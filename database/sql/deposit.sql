-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 06 2019 г., 11:33
-- Версия сервера: 5.7.20
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `deposit`
--

-- --------------------------------------------------------

--
-- Структура таблицы `banks`
--

CREATE TABLE `banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_uz` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_ru` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_uz` text COLLATE utf8_unicode_ci,
  `text_ru` text COLLATE utf8_unicode_ci,
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banks`
--

INSERT INTO `banks` (`id`, `name_uz`, `name_ru`, `phone`, `image`, `description_uz`, `description_ru`, `text_uz`, `text_ru`, `address`, `location`, `region_id`, `status`, `slug`, `views`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Trans Bank', NULL, '+998998762617', 'uploads/banks/1552770720.png', 'Trans bank', NULL, 'Trans bankTrans bankTrans bankTrans bankTrans bankTrans bankTrans bankTrans bankTrans bank', NULL, 'Chilonzor tumani', NULL, 0, 0, 'trans-bank', 0, NULL, '2019-02-14 05:08:05', '2019-03-17 02:12:00'),
(2, 'Turon bank', NULL, '+998711421524', 'uploads/banks/1552770515.png', NULL, NULL, NULL, NULL, 'Tashkent', NULL, 0, 0, 'turon-bank-31808', 0, NULL, '2019-02-18 15:10:24', '2019-03-17 02:08:35'),
(3, 'Aloqa bank', NULL, '+998998762610', 'uploads/banks/1552770484.png', NULL, NULL, NULL, NULL, 'Tuman', NULL, 0, 0, 'aloqa-bank-19215', 0, NULL, '2019-02-18 15:11:28', '2019-03-17 02:08:04'),
(4, 'ASIA ALLIANCE BANK', NULL, '+998998762610', 'uploads/banks/1552770431.png', NULL, NULL, NULL, NULL, 'Tuman', NULL, 0, 0, 'aab-27789', 0, NULL, '2019-02-18 15:11:58', '2019-03-23 19:37:01'),
(5, 'Asaka Bank', NULL, '+998998762610', 'uploads/banks/1552770392.png', NULL, NULL, NULL, NULL, 'Tuman', NULL, 0, 0, 'asaka-bank-70663', 0, NULL, '2019-02-18 15:12:24', '2019-03-17 02:06:32'),
(6, 'Orient Finans Bank', NULL, '+998998762610', 'uploads/banks/1552770294.png', NULL, NULL, NULL, NULL, 'Tuman', NULL, 0, 0, 'ofb-11277', 0, NULL, '2019-02-18 15:12:50', '2019-03-23 19:50:11'),
(7, 'Agro Bank', NULL, '998998762610', 'uploads/banks/1552770227.png', NULL, NULL, NULL, NULL, 'Tuman', NULL, 0, 0, 'agro-bank-98854', 0, NULL, '2019-02-18 15:13:17', '2019-03-17 02:03:47'),
(8, 'Turkiston Bank', NULL, '+998998762610', 'uploads/banks/1552770146.png', NULL, NULL, NULL, NULL, 'Tuman', NULL, 0, 0, 'turkiston-bank-72334', 0, NULL, '2019-02-18 15:13:41', '2019-03-17 02:02:26'),
(9, 'Sanoat Qurilish Bank', NULL, '+998998762610', 'uploads/banks/1552770046.png', NULL, NULL, NULL, NULL, 'Tuman', NULL, 1, 0, 'sqb-29780', 0, NULL, '2019-02-18 16:13:40', '2019-03-23 19:50:45'),
(10, 'O\'zMilliyBank', NULL, '+9999999999', 'uploads/banks/1553267851.png', NULL, NULL, NULL, NULL, 'Tashent shahar', NULL, 2, 0, 'ozmilliybank-33055', 0, NULL, '2019-03-22 20:17:31', '2019-03-22 20:17:31'),
(11, 'Xalq Banki', NULL, '+9999999999', 'uploads/banks/1553282170.png', NULL, NULL, NULL, NULL, 'Tashkent', NULL, 2, 0, 'xalq-banki-28942', 0, NULL, '2019-03-23 00:16:10', '2019-03-23 00:16:10'),
(12, 'MADAD INVEST BANK', NULL, '+99999999999', 'uploads/banks/1553352508.png', NULL, NULL, NULL, NULL, 'Manzil', NULL, 2, 0, 'madad-invest-bank-79339', 0, NULL, '2019-03-23 19:45:19', '2019-03-23 19:48:28'),
(13, 'Ipoteka bank', NULL, '+998998762617', 'uploads/banks/1553372140.png', NULL, NULL, NULL, NULL, 'Uzbekiston', NULL, 2, 0, 'ipoteka-bank-25946', 0, NULL, '2019-03-24 01:15:40', '2019-03-24 01:15:40'),
(14, 'Mikrokredit bank', NULL, '+998998762617', 'uploads/banks/1553454867.png', NULL, NULL, NULL, NULL, 'Tashkent', NULL, 2, 0, 'mikrokredit-bank-21046', 0, NULL, '2019-03-25 00:14:27', '2019-03-25 00:14:27'),
(15, 'Qishloq Qurilish bank', NULL, '+998998762617', 'uploads/banks/1553538036.png', NULL, NULL, NULL, NULL, 'Tashkent', NULL, 2, 0, 'qishloq-qurilish-bank-57613', 0, NULL, '2019-03-25 23:20:36', '2019-03-25 23:20:36');

-- --------------------------------------------------------

--
-- Структура таблицы `bank_services`
--

CREATE TABLE `bank_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_uz` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_ru` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_uz` text COLLATE utf8_unicode_ci,
  `text_ru` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `name`, `text`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Karimov Sharif Po\'latovich', 'Sayt menga yoqda, juda yaxshi tashkil qilingan', 1, '2019-02-28 10:50:43', '2019-02-28 10:50:43'),
(2, 'Karimov Sharif Po\'latovich', 'Saytning tuzilishi menga yoqdi, o\'ylaymanki sayt yaxshi yo\'lga qo\'yilgan, xizmatlaridan foydalanish ancha vaqyni va mablag\'ni tejab qoladi', 1, '2019-02-28 10:59:43', '2019-02-28 10:59:43');

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `phone`, `email`, `text`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Karimov Sharif', '+998998762610', NULL, 'Assalomu aleykum sayt administratoni menga yordam kekrak, siz menga QANDAY YORDAM BERA OLASIZ?', 0, '2019-02-28 08:56:11', '2019-02-28 08:56:11'),
(2, 'asdasdasdasd', 'asdasdas', NULL, 'asdasdasd', 0, '2019-02-28 08:57:32', '2019-02-28 08:57:32'),
(3, 'Ijejdjdjjdjd', '+998998762610', 'Sharif_260@bk.ru', 'Jssjhdhdhd', 0, '2019-03-31 10:46:12', '2019-03-31 10:46:12'),
(4, 'Hshdhdh', '35858', 'sharif_260@bk.ru', 'Djjdhdbdb', 0, '2019-05-03 21:31:24', '2019-05-03 21:31:24');

-- --------------------------------------------------------

--
-- Структура таблицы `credits`
--

CREATE TABLE `credits` (
  `id` int(10) UNSIGNED NOT NULL,
  `bank_id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `type_credit_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `name_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_credit` int(11) NOT NULL DEFAULT '0',
  `type_date` int(11) DEFAULT NULL,
  `date_privilege_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_privilege_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `percent` int(11) NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL DEFAULT '0',
  `text_amount_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_amount_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_compensation_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_compensation_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` int(11) DEFAULT NULL,
  `type_provision_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_provision_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `goal_credit_uz` text COLLATE utf8_unicode_ci,
  `goal_credit_ru` text COLLATE utf8_unicode_ci,
  `documents_uz` text COLLATE utf8_unicode_ci,
  `documents_ru` text COLLATE utf8_unicode_ci,
  `addition_rules_ru` text COLLATE utf8_unicode_ci,
  `addition_rules_uz` text COLLATE utf8_unicode_ci,
  `link` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `credits`
--

INSERT INTO `credits` (`id`, `bank_id`, `slug`, `status`, `type_credit_id`, `region_id`, `name_uz`, `name_ru`, `date_credit`, `type_date`, `date_privilege_uz`, `date_privilege_ru`, `percent`, `amount`, `text_amount_uz`, `text_amount_ru`, `first_compensation_uz`, `first_compensation_ru`, `currency`, `type_provision_uz`, `type_provision_ru`, `goal_credit_uz`, `goal_credit_ru`, `documents_uz`, `documents_ru`, `addition_rules_ru`, `addition_rules_uz`, `link`, `created_at`, `updated_at`) VALUES
(1, 9, 'istemoll-uchun-70727', 0, 1, NULL, 'Iste\'mol uchun', NULL, 36, 9, NULL, NULL, 30, 40000000, NULL, NULL, NULL, NULL, 1, 'Kafillik yoki boshqa ta\'minot', NULL, '- O\'zRda ishlab chiqarilgan tovar va xizmatlar xaridi uchun', NULL, '- Pasport nusxasi yoki uni o\'rnini bosuvchi xujjat nusxasi', NULL, NULL, 'Kredit miqdori EKIH 100 barobaridan ko\'p bo\'lganda ta\'minot sifatida mol-mulk garovi taqdim etilishi lozim', NULL, '2019-02-27 15:33:44', '2019-02-27 15:42:26'),
(2, 9, 'hamyonbop-44169', 0, 4, NULL, 'Hamyonbop', NULL, 12, 9, '3 oygacha', NULL, 32, 20000000, 'EKIH 100 barobarigacha', NULL, NULL, NULL, 1, 'Kafillik yoki boshqa ta\'mino', NULL, '- O\'zRda ishlab chiqarilgan tovar va xizmatlar xaridi uchun', NULL, '- Pasport nusxasi yoki uni o\'rnini bosuvchi xujjat nusxasi', NULL, NULL, 'Kredit miqdori EKIH 100 barobaridan ko\'p bo\'lganda ta\'minot sifatida mol-mulk garovi taqdim etilishi lozim', NULL, '2019-02-27 15:46:17', '2019-02-27 15:46:17'),
(3, 9, 'hamyonbop-54361', 0, 4, NULL, 'Hamyonbop', NULL, 24, 9, '3 oygacha', NULL, 34, 20000000, 'EKIH 100 barobarigacha', NULL, NULL, NULL, 1, 'Kafillik yoki boshqa ta\'minot', NULL, 'Maqsadsiz', NULL, '- Pasport nusxasi yoki uni o\'rnini bosuvchi xujjat nusxasi', NULL, NULL, NULL, NULL, '2019-02-27 15:48:17', '2019-02-27 15:48:17');

-- --------------------------------------------------------

--
-- Структура таблицы `credit_types`
--

CREATE TABLE `credit_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `text_uz` text COLLATE utf8_unicode_ci,
  `text_ru` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `credit_types`
--

INSERT INTO `credit_types` (`id`, `name_uz`, `name_ru`, `slug`, `status`, `text_uz`, `text_ru`, `created_at`, `updated_at`) VALUES
(1, 'Iste\'mol kreditlari', 'Потребительские кредиты', 'istemol-kreditlari-42499', 0, 'Iste\'mol uchun mo\'ljallangan pul mablag\'lari', 'Средства для потребления', '2019-02-27 13:58:05', '2019-06-01 20:14:30'),
(2, 'Avto kreditlar', 'Авто кредиты', 'avtokreditlar-63711', 0, 'Avto mobil sotib olish uchun beriladigan kreditlar', 'Кредиты на покупку автомобиля', '2019-02-27 13:58:36', '2019-06-01 20:12:18'),
(3, 'Ipoteka kreditlari', 'Ипотечные кредиты', 'ipoteka-kreditlari-64597', 0, 'Ko\'chmas mulk sotib olish uchun beriladigan kreditlar', 'Кредиты на приобретение недвижимости', '2019-02-27 13:59:03', '2019-06-01 20:12:49'),
(4, 'Mikro qarzlar', 'Микрозаймы', 'mikro-qarzlar-72982', 0, 'Miqro qarzlar- katta miqdorda bo\'lmagan pul mablag\'lari', 'Микрозаймы - большие суммы денег', '2019-02-27 13:59:26', '2019-06-01 20:13:30');

-- --------------------------------------------------------

--
-- Структура таблицы `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'UZS', 'Uzbekiston pul birligi', 0, '2019-02-17 16:01:12', '2019-02-17 16:01:12'),
(2, 'USD', 'Dollar', 0, '2019-02-17 16:01:50', '2019-02-17 16:01:50'),
(3, 'RUB', 'Russian', 0, '2019-02-17 16:02:10', '2019-02-17 16:02:10'),
(4, 'EUR', 'Yevro', 0, '2019-03-06 20:27:06', '2019-03-06 20:27:06');

-- --------------------------------------------------------

--
-- Структура таблицы `date_services`
--

CREATE TABLE `date_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `date_services`
--

INSERT INTO `date_services` (`id`, `name_uz`, `name_ru`, `service_id`, `status`, `position`, `description`, `created_at`, `updated_at`) VALUES
(8, 'Yillik', 'Годовой', 2, NULL, 0, 'Yillik kreditlar', '2019-02-27 14:09:34', '2019-06-06 07:39:13'),
(9, 'Oylik', 'Ежемесячно', 2, NULL, NULL, 'Oylik kreditlar', '2019-02-27 14:10:16', '2019-02-27 14:10:36'),
(10, 'Kunlik', 'Суточный', 2, NULL, NULL, 'Kunlik kreditlar', '2019-02-27 14:11:16', '2019-02-27 14:11:16'),
(11, '1 oy', '1 месяц', 1, NULL, 1, NULL, '2019-03-02 13:15:05', '2019-06-05 19:44:35'),
(12, '2 oy', '2 месяц', 1, NULL, 2, NULL, '2019-03-06 20:10:35', '2019-06-05 19:45:09'),
(13, '3 oy', '3 месяц', 1, NULL, 3, NULL, '2019-03-06 20:10:59', '2019-06-05 19:45:25'),
(14, '4 oy', '4 месяц', 1, NULL, 4, NULL, '2019-03-06 20:17:22', '2019-06-05 19:45:38'),
(15, '5 oy', '5 месяц', 1, NULL, 5, NULL, '2019-03-06 20:17:35', '2019-06-05 19:45:53'),
(16, '6 oy', '6 месяц', 1, NULL, 6, NULL, '2019-03-06 20:17:50', '2019-03-20 01:14:40'),
(17, '7 oy', '7 месяц', 1, NULL, 7, NULL, '2019-03-06 20:18:06', '2019-03-20 01:15:03'),
(18, '8 oy', '8 месяц', 1, NULL, 8, NULL, '2019-03-06 20:18:22', '2019-03-20 01:15:03'),
(19, '10 oy', '10 месяц', 1, NULL, 10, NULL, '2019-03-06 20:18:38', '2019-03-20 01:15:05'),
(20, '1 yil', '1 год', 1, NULL, 11, NULL, '2019-03-06 20:19:10', '2019-03-20 01:15:34'),
(21, '1,5 yil', '1,5 год', 1, NULL, 12, NULL, '2019-03-06 20:19:29', '2019-03-20 01:15:33'),
(22, '2 yil', '2 год', 1, NULL, 13, NULL, '2019-03-06 20:19:48', '2019-03-20 01:15:32'),
(23, '3 yil', '3 год', 1, NULL, 14, NULL, '2019-03-06 20:20:03', '2019-03-20 01:15:31'),
(24, '4 yil', '4 год', 1, NULL, 15, NULL, '2019-03-06 20:20:23', '2019-03-20 01:15:31'),
(25, '5 yil', '5 год', 1, NULL, 16, NULL, '2019-03-06 20:20:37', '2019-03-20 01:15:30'),
(26, '6 yil', '6 год', 1, NULL, 17, NULL, '2019-03-06 20:20:49', '2019-03-20 01:15:29'),
(27, '7 yil', '7 год', 1, NULL, 18, NULL, '2019-03-06 20:21:13', '2019-03-20 01:15:42'),
(28, '8 yil', '8 год', 1, NULL, 19, NULL, '2019-03-06 20:21:24', '2019-03-20 01:15:57'),
(29, '9 yil', '9 год', 1, NULL, 20, NULL, '2019-03-06 20:21:38', '2019-03-20 01:15:55'),
(30, '10 yil', '10 год', 1, NULL, 21, NULL, '2019-03-06 20:21:50', '2019-03-20 01:15:55'),
(31, '9 oy', '9 месяц', 1, NULL, 9, NULL, '2019-03-17 11:10:40', '2019-03-20 01:15:54'),
(32, 'Muddatsiz', 'Бессрочный', 1, NULL, 24, 'Muddatsiz', '2019-03-24 00:16:25', '2019-03-25 00:26:14'),
(33, '15 yil', '15 год', 1, NULL, 22, NULL, '2019-03-25 00:18:13', '2019-03-25 00:26:14'),
(34, '18 yil', '18 год', 1, NULL, 23, NULL, '2019-03-25 00:18:33', '2019-03-25 00:26:17');

-- --------------------------------------------------------

--
-- Структура таблицы `deposits`
--

CREATE TABLE `deposits` (
  `id` int(10) UNSIGNED NOT NULL,
  `bank_id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `name_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deposit_type_id` int(10) UNSIGNED DEFAULT NULL,
  `deposit_date` int(11) DEFAULT NULL,
  `deposit_date_type` int(11) DEFAULT NULL,
  `deposit_percent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_sum` int(11) DEFAULT '0',
  `type_sum` int(11) DEFAULT '0',
  `currency` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_fill` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `partly_take` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `percents_capitalization` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `percent_paid_period` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `close_before_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `link` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_ru` text COLLATE utf8_unicode_ci,
  `text_uz` text COLLATE utf8_unicode_ci,
  `region_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `deposits`
--

INSERT INTO `deposits` (`id`, `bank_id`, `slug`, `status`, `name_uz`, `name_ru`, `deposit_type_id`, `deposit_date`, `deposit_date_type`, `deposit_percent`, `min_sum`, `type_sum`, `currency`, `account_fill`, `partly_take`, `percents_capitalization`, `percent_paid_period`, `close_before_date`, `link`, `text_ru`, `text_uz`, `region_id`, `created_at`, `updated_at`) VALUES
(1, 9, 'avesto-26853', 0, 'Avesto', NULL, 1, 730, 22, '20', 2000000, NULL, '1', '0', '0', '0', 'Xar oyda', '1 yilgacha - 0% 1 yildan so\'ng 16%', 'https://www.uzpsb.uz/physical/deposits/', NULL, NULL, 1, '2019-02-18 16:19:19', '2019-03-22 20:02:32'),
(2, 9, 'platinum-22769', 0, 'Platinum', NULL, 1, 791, 22, '20', 200000, NULL, '1', '0', '0', '0', 'Xar 2 oyda', '1 yilgacha - 0% 1 yildan so\'ng 16%', NULL, NULL, NULL, 1, '2019-02-18 19:12:34', '2019-03-22 20:03:59'),
(3, 9, 'mega-31309', 0, 'Mega', NULL, 1, 777, 22, '19', 2000000, NULL, '1', '0', '0', '0', 'Xar oyda', '0', NULL, NULL, NULL, 1, '2019-02-18 19:16:55', '2019-03-22 20:05:20'),
(4, 9, 'komfort-21248', 0, 'Komfort', NULL, 1, 395, 20, '18', 100000, NULL, '1', '0', '0', '0', 'Xar oyda', '0', 'https://www.uzpsb.uz/physical/deposits', NULL, NULL, 1, '2019-02-18 19:25:56', '2019-03-22 20:07:14'),
(5, 9, 'online-exclusive-68516', 0, 'Online Exclusive', NULL, 2, 120, 14, '17', 100000, NULL, '1', '1', '0', '0', 'Xar oyda', '0', 'https://www.uzpsb.uz/physical/deposits', NULL, NULL, 1, '2019-02-18 19:28:11', '2019-03-22 20:08:43'),
(6, 9, 'online-sarmoya-55480', 0, 'Online Sarmoya', NULL, 2, 90, 13, '16', 100000, NULL, '1', '1', '0', '0', 'Xar oyda', '0', 'https://www.uzpsb.uz/physical/deposits', NULL, NULL, 1, '2019-02-18 19:29:46', '2019-03-22 20:09:41'),
(7, 9, 'universal-52747', 0, 'Universal', NULL, 1, 90, 13, '16', 100000, NULL, '1', '0', '0', '0', 'Xar oyda', '0', 'https://www.uzpsb.uz/physical/deposits', NULL, NULL, 1, '2019-02-18 19:30:59', '2019-03-22 20:10:42'),
(8, 9, 'bayramona-32229', 0, 'Bayramona', NULL, 2, 120, 14, '15', 10000, NULL, '1', '1', '0', '0', 'Xar oyda', '0', 'https://www.uzpsb.uz/physical/deposits', NULL, NULL, 1, '2019-02-18 19:32:02', '2019-03-22 20:11:47'),
(9, 9, 'oila-sarmoyasi-42445', 0, 'Oila Sarmoyasi', NULL, 2, 60, 12, '14', 100000, NULL, '1', '1', '0', '0', 'Xar oyda', '0', 'https://www.uzpsb.uz/physical/deposits', NULL, NULL, 1, '2019-02-18 19:32:53', '2019-03-22 20:13:00'),
(10, 9, 'imkon-79697', 0, 'Imkon', NULL, 2, 30, 11, '10', 100000, NULL, '1', '1', '0', '0', 'Xar oyda', '0', 'https://www.uzpsb.uz/physical/deposits', NULL, NULL, 1, '2019-02-18 19:33:50', '2019-03-22 20:14:06'),
(11, 2, 'istemol-73392', 0, 'Iste\'mol', NULL, 2, 365, 20, '2', 0, NULL, '1', '0', '0', '0', 'Istemol krediti uchun', '0', NULL, 'Средства принимаются от клиентов, которые хотят получить потребительский кредит на покупку автомобилей, произведенных на республиканском (первичном) и вторичном рынке.', NULL, 0, '2019-02-18 19:40:02', '2019-03-24 23:31:46'),
(12, 2, 'yangi-avto-44526', 0, 'Yangi avto', NULL, 2, 1095, 23, '7', 0, NULL, '1', '0', '0', '0', 'Avtokredit uchun', '0', NULL, 'Вклад предназначен для приобретения автокредита', NULL, 0, '2019-02-18 19:42:10', '2019-03-24 23:33:15'),
(13, 2, 'avto-omonat-81719', 0, 'Avto omonat', NULL, 2, 365, 20, '2', 0, NULL, '1', '1', '0', '0', 'Avtokredit uchun', '0', NULL, 'Вклад предназначен для приобретения потребительского кредита', NULL, 0, '2019-02-18 19:43:27', '2019-03-24 23:35:15'),
(14, 2, 'ipoteka-45558', 0, 'Ipoteka', NULL, 2, 365, 20, '7', 0, NULL, '1', '1', '0', '0', 'Har oyda', '0', NULL, 'Предназначен для приобретения ипотечного кредита, проценты выдаются по окончанию срока вклада', NULL, 0, '2019-02-18 20:13:12', '2019-03-24 23:36:38'),
(15, 2, 'sovga-26469', 0, 'Sovg\'a', NULL, 2, 365, 20, '7', 0, NULL, '1', '1', '0', '0', 'Har oyda', '0', NULL, 'Вклад предназначен для приобретения потребительского кредита', NULL, 0, '2019-02-18 20:14:25', '2019-03-24 23:38:03'),
(16, 2, 'meros-52130', 0, 'Meros', NULL, 2, 3650, 30, '9', 0, NULL, '1', '1', '0', '0', 'Har oyda', '0', NULL, 'Допольнительные взносы допускаются не менее 100 тыс сум , Проценты капитализируются.   Клиент имеет возможность приобрести сумму , накопившуюся во вкладе за срок не менее 2 лет в 4 кратном размере для оформления ипотечного , потребительского и образовательного кредита', NULL, 0, '2019-02-18 20:17:53', '2019-03-24 23:39:46'),
(17, 2, 'mobil-omonat-13881', 0, 'Mobil omonat', NULL, 1, 90, 13, '16', 100000, NULL, '1', '1', '0', '0', 'Har oyda', '0', NULL, 'Физ.лица (держатели) пластиковых карт UzCard открытые в сети АКБ \"Турон банк\",смогут внести сумму  через мобильную программу “Turon Mobile” или через “Личный кабинет” на сайте www.turonbank.uz. Дополнительные взносы допускаются. Минимальная сумма дополнительной оплаты - 100 000 сумов', NULL, 0, '2019-02-18 20:22:52', '2019-03-24 23:41:27'),
(18, 2, 'onlayn-omonat-88765', 0, 'Online', NULL, 1, 182, 16, '16', 0, NULL, '1', '1', '0', '0', 'Har oyda', '0', NULL, 'Физ.лица (держатели) пластиковых карт UzCard открытые в сети АКБ \"Турон банк\",смогут внести сумму  через мобильную программу “Turon Mobile” или через “Личный кабинет” на сайте www.turonbank.uz. Дополнительные взносы допускаются. Минимальная сумма дополнительной оплаты -  500 000 сумов', NULL, 0, '2019-02-18 20:24:23', '2019-03-24 23:43:27'),
(22, 2, 'imkoniyat-69327', 0, 'Imkoniyat', NULL, 1, 365, 20, '14', 0, NULL, '1', '0', '0', '0', 'Har oyda', '0', NULL, 'Проценты выплачиваются ежемесячно или по окончанию срока вклада', '15 kundan, 1, 3, 6, 9 va 12 oygacha', 0, '2019-02-18 21:02:15', '2019-03-24 23:54:36'),
(27, 10, 'mega-progress-75585', 0, 'Mega progress', NULL, 1, 1095, 23, '14', 1000000, NULL, '1', '1', '0', '0', 'Xar oyda yoki muddat ohirida', '6-24 oy - 14%,  1 yil  - 18%, 2 yil - 19%,   3 yil - 20 %', 'https://nbu.uz/uz/physical/deposits/mega-progress_uzb/', NULL, NULL, 2, '2019-03-22 20:22:11', '2019-03-22 20:51:50'),
(28, 10, 'investitsiya-63015', 0, 'Invеstitsiya', NULL, 1, 395, 20, '20', 1000000, NULL, '1', '1', '0', '0', '3 oygacha - 0%, 3-17 oyga - 16%.', NULL, 'https://nbu.uz/uz/physical/deposits/investitsiya-uz/', NULL, NULL, 2, '2019-03-22 20:27:43', '2019-03-22 20:27:43'),
(29, 10, 'investitsiya-89582', 0, 'Invеstitsiya', NULL, 1, 548, 22, '21', 1000000, NULL, '1', '1', '0', '0', '3 oygacha - 0%, 3-17 oyga - 16%.', NULL, 'https://nbu.uz/uz/physical/deposits/investitsiya-uz/', NULL, NULL, 2, '2019-03-22 20:30:27', '2019-03-22 20:30:27'),
(30, 10, 'progress-53819', 0, 'Progress', NULL, 1, 365, 20, '17.5', 500000, NULL, '1', '1', '0', '0', 'Xar oyda yoki muddat ohirida', '4-6 oyga yillik - 16.5%, 7-9 oyga yillik 17%, 10-12 oyga yillik 17.5% to‘lanadi', 'https://nbu.uz/uz/physical/deposits/progress_uzb/', NULL, NULL, 2, '2019-03-22 20:35:26', '2019-03-23 01:14:30'),
(31, 10, 'nihol-59307', 0, 'Nihol', NULL, 2, 1825, 25, '18', 0, 1, '1', '1', '0', '0', 'Muddat ohirida', NULL, 'https://nbu.uz/uz/physical/deposits/nikhol-uz/', NULL, NULL, 2, '2019-03-22 20:54:11', '2019-03-22 20:54:11'),
(32, 10, 'shinam-77782', 0, 'Shinam', NULL, 2, 180, 16, '16', 0, 1, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'https://nbu.uz/uz/physical/deposits/shinam-uz/', NULL, NULL, 2, '2019-03-22 20:56:18', '2019-03-22 20:56:18'),
(33, 10, 'pensiya-16394', 0, 'Pеnsiya', NULL, 2, 365, 20, '16', 0, 1, '1', '1', '0', '0', 'Xar oyda yoki muddat ohirida', '6 oy - 15%, 12 oy - 16%', 'https://nbu.uz/uz/physical/deposits/pensiya-uz/', NULL, NULL, 2, '2019-03-22 20:58:57', '2019-03-22 20:58:57'),
(34, 10, 'kopaysin-93884', 0, 'Ko‘paysin', NULL, 2, 365, 20, '16', 1000000, NULL, '1', '1', '0', '0', 'Muddat ohirida', NULL, 'https://nbu.uz/uz/physical/deposits/kopaysin-uz/', NULL, NULL, 2, '2019-03-22 21:00:39', '2019-03-22 21:00:39'),
(35, 10, 'xonadon-18336', 0, 'Xonadon', NULL, 2, 365, 20, '5', 0, NULL, '1', '1', '0', '0', 'Muddat ohirida', NULL, 'https://nbu.uz/uz/physical/deposits/xonadon/', NULL, 'uy-joy narxining 25%ga', 2, '2019-03-22 21:02:52', '2019-03-22 21:02:52'),
(36, 10, 'daromadli-48064', 0, 'Daromadli', NULL, 2, 90, 13, '13', 0, 1, '1', '0', '0', '0', 'Muddat ohirida', '1 oy - 11%, 2 oy - 12%, 3 oy - 13%', 'https://nbu.uz/uz/physical/deposits/daromadli-uz/', NULL, NULL, 2, '2019-03-22 21:04:50', '2019-03-22 21:04:50'),
(37, 10, 'innovatsiya-31882', 0, 'Innovatsiya', NULL, 2, 395, 20, '17', 0, 1, '1', '1', '0', '0', 'Muddat ohirida', NULL, 'https://nbu.uz/uz/physical/deposits/innovatsiya-uz/', NULL, NULL, 2, '2019-03-22 23:37:04', '2019-03-22 23:37:04'),
(38, 10, 'komfort-13780', 0, 'Komfort', NULL, 1, 760, 22, '4', 1000, NULL, '2', '1', '0', '0', '1 yildan so\'ng va muddat ohirida', NULL, 'https://nbu.uz/uz/physical/deposits/komfort-usd_uzb/', NULL, NULL, 2, '2019-03-22 23:58:18', '2019-03-23 00:08:15'),
(39, 10, 'komfort-98714', 0, 'Komfort', NULL, 1, 760, 22, '18', 5000000, NULL, '1', '1', '0', '0', '1 yildan so\'ng va muddat ohirida', NULL, 'https://nbu.uz/uz/physical/deposits/komfort_uzb/', NULL, NULL, 2, '2019-03-23 00:00:14', '2019-03-23 00:00:14'),
(40, 10, 'taraqqiyot-49163', 0, 'Taraqqiyot', NULL, 1, 395, 20, '4', 0, 1, '1', '1', '0', '0', 'Har chorakda yoki muddat ohirida', NULL, 'https://nbu.uz/uz/physical/deposits/taraqqiyot-uz/', NULL, NULL, 2, '2019-03-23 00:03:21', '2019-03-23 00:03:21'),
(41, 10, 'samarali-45528', 0, 'Samarali', NULL, 1, 60, 12, '3', 0, 1, '2', '1', '0', '0', 'Muddat ohirida', NULL, 'https://nbu.uz/uz/physical/deposits/samarali-uz/', NULL, NULL, 2, '2019-03-23 00:05:36', '2019-03-23 00:11:26'),
(42, 10, 'evro-40553', 0, 'EVRO', NULL, 1, 60, 12, '2', 0, 1, '2', '1', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'https://nbu.uz/uz/physical/deposits/evro-uz/', NULL, NULL, 2, '2019-03-23 00:07:43', '2019-03-23 00:07:43'),
(43, 10, 'barqaror-99760', 0, 'Barqaror', NULL, 1, 395, 20, '5', 0, 1, '2', '0', '0', '0', 'Har chorakda yoki muddat ohirida', NULL, 'https://nbu.uz/uz/physical/deposits/barqaror1/', NULL, NULL, 2, '2019-03-23 00:10:29', '2019-03-23 00:10:29'),
(44, 10, 'ishonch-64261', 0, 'Ishonch', NULL, 1, 182, 16, '4', 0, 1, '2', '1', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'https://nbu.uz/uz/physical/deposits/ishonch/', NULL, NULL, 2, '2019-03-23 00:13:13', '2019-03-23 00:13:13'),
(45, 11, 'xazina-1-54174', 0, 'Xazina-1', NULL, 1, 760, 22, '18', 500000, NULL, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://www.xb.uz/uz/info/xazina-1-0', NULL, NULL, 2, '2019-03-23 00:19:17', '2019-03-23 00:19:17'),
(46, 11, 'xazina-1-29357', 0, 'Xazina-1', NULL, 1, 395, 20, '17', 500000, NULL, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://www.xb.uz/uz/info/xazina-1', NULL, NULL, 2, '2019-03-23 00:21:04', '2019-03-23 00:21:04'),
(47, 11, 'barakali-96867', 0, 'Barakali', NULL, 1, 395, 20, '4', 0, 1, '2', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://www.xb.uz/uz/info/barakali', NULL, NULL, 2, '2019-03-23 00:22:48', '2019-03-23 00:22:48'),
(48, 11, 'online-kapital-97507', 0, 'Online Kapital', NULL, 1, 180, 16, '14', 100000, NULL, '1', '1', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://www.xb.uz/uz/info/online-kapital', NULL, NULL, 2, '2019-03-23 00:25:10', '2019-03-23 00:25:10'),
(49, 11, 'oila-jamgarmasi-53579', 0, 'Oila jamg\'armasi', NULL, 1, 150, 15, '14', 0, 1, '1', '1', '0', '0', 'Muddat ohirida', NULL, 'http://www.xb.uz/uz/info/oila-zhamgarmasi', NULL, NULL, 2, '2019-03-23 00:26:50', '2019-03-23 00:26:50'),
(50, 11, 'samarali-26069', 0, 'Samarali', NULL, 1, 365, 20, '3', 0, 1, '2', '0', '0', '0', 'Muddat ohirida', NULL, 'http://www.xb.uz/uz/node/2443', NULL, NULL, 2, '2019-03-23 00:28:41', '2019-03-23 00:28:41'),
(51, 11, 'farovon-hayot-14425', 0, 'Farovon hayot', NULL, 1, 1095, 23, '1', 0, NULL, '1', '0', '0', '0', 'Muddaat ohirida', NULL, 'http://www.xb.uz/uz/node/1876', NULL, '1 yil, 2 yil, 3 yil, iste\'mol krediti summasining 10 foizi', 2, '2019-03-23 00:31:40', '2019-03-23 00:44:52'),
(52, 11, 'yozgi-tatil-92307', 0, 'Yozgi ta’til', NULL, 1, 273, 31, '10', 0, 1, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://www.xb.uz/uz/node/144', NULL, NULL, 2, '2019-03-23 00:36:08', '2019-03-23 00:36:08'),
(53, 11, 'yangi-uy-poydevori-44728', 0, 'Yangi uy poydevori', NULL, 1, 365, 20, '1', 0, NULL, '1', '1', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://www.xb.uz/uz/node/132', NULL, 'ipoteka krediti summasining 25 %', 2, '2019-03-23 00:46:31', '2019-03-23 00:46:31'),
(54, 5, 'mobile-uz-65357', 0, 'Mobile-uz', NULL, 1, 91, 13, '15', 500000, NULL, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'https://www.asakabank.uz/uz/omonatlar/depozit_va_omonat_turlari', NULL, NULL, 0, '2019-03-23 00:49:19', '2019-03-23 00:49:19'),
(55, 5, 'mobile-uza-79127', 0, 'Mobile-uz.A', NULL, 1, 395, 20, '16', 100000, NULL, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'https://www.asakabank.uz/uz/omonatlar/depozit_va_omonat_turlari', NULL, NULL, 0, '2019-03-23 00:50:48', '2019-03-23 00:50:48'),
(56, 5, 'manfaatli-64349', 0, 'Manfaatli', NULL, 1, 543, 21, '16', 0, 1, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', '1 oygacha 0%,  13 oyga +1%, 18 oyga +2%', 'https://www.asakabank.uz/uz/omonatlar/depozit_va_omonat_turlari', NULL, NULL, 0, '2019-03-23 00:53:17', '2019-03-23 00:53:17'),
(57, 5, 'ziynat-42627', 0, 'Ziynat', NULL, 1, 180, 16, '15', 0, 1, '1', '0', '0', '0', 'Muddat ohirida', '1 oygacha 0%', 'https://www.asakabank.uz/uz/omonatlar/depozit_va_omonat_turlari', NULL, NULL, 0, '2019-03-23 00:54:48', '2019-03-23 00:54:48'),
(58, 5, 'umid-63630', 0, 'Umid', NULL, 1, 395, 20, '16', 0, 1, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'https://www.asakabank.uz/uz/omonatlar/depozit_va_omonat_turlari', NULL, NULL, 0, '2019-03-23 00:56:06', '2019-03-23 00:56:06'),
(59, 5, 'mangu-26989', 0, 'Mangu', NULL, 1, 365, 20, '14', 0, 1, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'https://www.asakabank.uz/uz/omonatlar/depozit_va_omonat_turlari', NULL, NULL, 0, '2019-03-23 00:57:12', '2019-03-23 00:57:12'),
(60, 5, 'elegant-18334', 0, 'Elegant', NULL, 1, 180, 16, '4', 0, 1, '2', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'https://www.asakabank.uz/uz/omonatlar/depozit_va_omonat_turlari', NULL, NULL, 0, '2019-03-23 00:59:34', '2019-03-23 00:59:34'),
(61, 5, 'madad-74301', 0, 'Madad', NULL, 1, 90, 13, '4', 0, 1, '2', '0', '0', '0', 'Muddat ohirida', NULL, 'https://www.asakabank.uz/uz/omonatlar/depozit_va_omonat_turlari', NULL, NULL, 0, '2019-03-23 01:01:01', '2019-03-23 01:01:01'),
(62, 5, 'mobile-usd-52066', 0, 'Mobile-usd', NULL, 1, 90, 13, '4', 100, NULL, '2', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'https://www.asakabank.uz/uz/omonatlar/depozit_va_omonat_turlari', NULL, NULL, 0, '2019-03-23 01:02:11', '2019-03-23 01:02:11'),
(63, 5, 'investisiya-99084', 0, 'Investisiya', NULL, 1, 760, 22, '5', 0, 1, '2', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'https://www.asakabank.uz/uz/omonatlar/depozit_va_omonat_turlari', NULL, NULL, 0, '2019-03-23 01:03:59', '2019-03-23 01:03:59'),
(64, 5, 'foydali-21767', 0, 'Foydali', NULL, 1, 365, 20, '4.5', 0, 1, '2', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'https://www.asakabank.uz/uz/omonatlar/depozit_va_omonat_turlari', NULL, NULL, 0, '2019-03-23 01:05:52', '2019-03-23 01:16:44'),
(65, 5, 'barqaror-32031', 0, 'Barqaror', NULL, 1, 365, 20, '4', 0, 1, '2', '0', '0', '0', 'Har chorakda yoki muddat ohirida', NULL, 'https://www.asakabank.uz/uz/omonatlar/depozit_va_omonat_turlari', NULL, NULL, 0, '2019-03-23 01:07:11', '2019-03-23 01:07:11'),
(66, 4, 'dostlik-onlayn-18198', 0, 'Do\'stlik-onlayn', NULL, 1, 455, 21, '19', 100000, NULL, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', '1 oygacha 0%,  1 oydan so\'ng 15 %', 'http://aab.uz/ru/private/deposit/dustlik-onlayn/', NULL, NULL, 0, '2019-03-23 19:03:49', '2019-03-23 19:03:49'),
(67, 4, 'dostlik-92195', 0, 'Do\'stlik', NULL, 1, 455, 21, '18', 500000, NULL, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', '1 oygacha 0%,  1 oydan so\'ng 14 %', 'http://aab.uz/ru/private/deposit/dustlik/', NULL, NULL, 0, '2019-03-23 19:05:42', '2019-03-23 19:05:42'),
(68, 4, 'istiqbol-onlayn-30382', 0, 'Istiqbol-onlayn', NULL, 1, 760, 22, '21', 100000, NULL, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', '6 oygacha 0%,  1 oydan so\'ng 17 %', 'http://aab.uz/ru/private/deposit/istikbol-onlayn/', NULL, NULL, 0, '2019-03-23 19:08:21', '2019-03-23 19:08:21'),
(69, 4, 'istiqbol-67927', 0, 'Istiqbol', NULL, 1, 760, 22, '21', 500000, NULL, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', '6 oygacha 0%,  1 oydan so\'ng 17 %', 'http://aab.uz/ru/private/deposit/istikbol/', NULL, NULL, 0, '2019-03-23 19:09:50', '2019-03-23 19:09:50'),
(70, 4, 'hamrox-onlayn-17290', 0, 'Hamrox-onlayn', NULL, 2, 545, 21, '17', 100000, NULL, '1', '1', '0', '0', 'Har chorakda yoki muddat ohirida', '1 oygacha 0%,  1 oydan so\'ng 14 %', 'http://aab.uz/ru/private/deposit/khamrokh-onlayn/', NULL, NULL, 0, '2019-03-23 19:12:32', '2019-03-23 19:14:51'),
(71, 4, 'hamrox-28991', 0, 'Hamrox', NULL, 2, 545, 21, '17', 500000, NULL, '1', '1', '0', '0', 'Har chorakda yoki muddat ohirida', '1 oygacha 0%,  1 oydan so\'ng 14 %', 'http://aab.uz/ru/private/deposit/khamrokh/', NULL, NULL, 0, '2019-03-23 19:14:15', '2019-03-23 19:14:15'),
(72, 4, 'yaxshilik-54752', 0, 'Yaxshilik', NULL, 1, 395, 20, '18', 500000, NULL, '1', '0', '0', '0', NULL, '% avans', 'http://aab.uz/ru/private/deposit/yakhshilik/', NULL, NULL, 0, '2019-03-23 19:16:40', '2019-03-23 19:16:40'),
(73, 4, 'omonlik-onlayn-32027', 0, 'Omonlik-onlayn', NULL, 1, 182, 16, '17', 100000, NULL, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://aab.uz/ru/private/deposit/omonlik-onlayn/', NULL, NULL, 0, '2019-03-23 19:18:09', '2019-03-23 19:18:09'),
(74, 4, 'omonlik-98667', 0, 'Omonlik', NULL, 1, 182, 16, '16', 500000, NULL, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://aab.uz/ru/private/deposit/omonlik/', NULL, NULL, 0, '2019-03-23 19:19:52', '2019-03-23 19:19:52'),
(75, 4, 'yaxshi-niyat-onlayn-71747', 0, 'Yaxshi niyat - onlayn', NULL, 1, 90, 13, '16', 100000, NULL, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://aab.uz/ru/private/deposit/yakhshi-niyat-onlayn/', NULL, NULL, 0, '2019-03-23 19:22:00', '2019-03-23 19:22:00'),
(76, 4, 'yaxshi-niyat-37202', 0, 'Yaxshi niyat', NULL, 1, 90, 13, '15', 500000, NULL, '1', '0', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://aab.uz/ru/private/deposit/yakhshi-niyat/', NULL, NULL, 0, '2019-03-23 19:23:31', '2019-03-23 19:24:05'),
(77, 4, 'alyans-yoshlik-39578', 0, 'Alyans yoshlik', NULL, 2, 3650, 30, '18', 0, 1, '1', '1', '0', '0', 'Muddat ohirida', NULL, 'http://aab.uz/ru/private/deposit/alyans-yeshlik/', NULL, NULL, 0, '2019-03-23 19:26:20', '2019-03-23 19:26:20'),
(78, 4, 'imkoniyat-46070', 0, 'Imkoniyat', NULL, 1, 182, 16, '4', 500, NULL, '2', '0', '1', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://aab.uz/ru/private/deposit/v-dollarakh-ssha-inom/', NULL, NULL, 0, '2019-03-23 19:28:18', '2019-03-23 19:28:18'),
(79, 4, 'manfaat-31279', 0, 'Manfaat', NULL, 2, 395, 20, '2', 500, NULL, '2', '1', '0', '0', 'Muddat ohirida', NULL, 'http://aab.uz/ru/private/deposit/v-dollarakh-ssha-manfaat-/', NULL, NULL, 0, '2019-03-23 19:30:14', '2019-03-23 19:30:14'),
(80, 4, 'hazina-63473', 0, 'Hazina', NULL, 1, 275, 31, '4.5', 500, NULL, '2', '0', '1', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://aab.uz/ru/private/deposit/v-dollarakh-ssha-fayz-/', NULL, NULL, 0, '2019-03-23 19:31:59', '2019-03-23 19:31:59'),
(81, 4, 'bolajon-88625', 0, 'Bolajon', NULL, 2, 3650, 30, '4', 500, NULL, '2', '1', '0', '0', 'Muddat ohirida', NULL, 'http://aab.uz/ru/private/deposit/v-dollarakh-ssha-bolazhon/', NULL, '15 yoshiga yetguncha', 0, '2019-03-23 19:34:24', '2019-03-23 19:34:24'),
(82, 4, 'barakali-49900', 0, 'Barakali', NULL, 1, 395, 20, '5.5', 500, NULL, '2', '0', '1', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://aab.uz/ru/private/deposit/stavka-na-uspekh/', NULL, NULL, 0, '2019-03-23 19:35:43', '2019-03-23 19:35:43'),
(83, 12, 'talab-qilib-olguncha-45843', 0, 'Talab qilib olguncha', NULL, 2, 0, 32, '1', 1000, NULL, '1', '1', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://www.madadinvestbank.uz/page/vidy-vkladov', NULL, NULL, 2, '2019-03-24 00:19:52', '2019-03-24 00:19:52'),
(84, 12, 'talab-qilib-olguncha-aqsh-dollari-64923', 0, 'Talab qilib olguncha AQSH dollari', NULL, 2, 0, 32, '0', 0, 1, '2', '1', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://www.madadinvestbank.uz/page/vidy-vkladov', NULL, NULL, 2, '2019-03-24 00:21:14', '2019-03-24 00:21:14'),
(85, 12, 'oila-madadkori-76569', 0, 'Oila madadkori', NULL, 1, 545, 21, '14', 1000, NULL, '1', '1', '0', '0', 'Xar oyda yoki muddat ohirida', '6 oy	-10% , 12 oy - 13%, 18 oy - 14%', 'http://www.madadinvestbank.uz/page/vidy-vkladov', NULL, '6 oy	-10% , 12 oy - 13%, 18 oy - 14,00%', 2, '2019-03-24 00:28:18', '2019-03-24 00:28:18'),
(86, 12, 'plastik-sarmoya-62162', 0, 'Plastik sarmoya', NULL, 1, 365, 20, '9', 1000, NULL, '1', '1', '0', '0', 'Xar oyda yoki muddat ohirida', '6 oy -7,00% ,	9 oy	 - 8,00%, 12 oy - 9,00%', 'http://www.madadinvestbank.uz/page/vidy-vkladov', NULL, '6 oy -7,00% ,	9 oy	 - 8,00%, 12 oy - 9,00%', 2, '2019-03-24 00:36:03', '2019-03-24 00:36:03'),
(87, 12, 'maqsadli-tolov-22985', 0, 'Maqsadli to’lov', NULL, 1, 365, 20, '12', 1000, NULL, '1', '1', '0', '0', 'Maqsadga ko\'ra to\'lov', NULL, 'http://www.madadinvestbank.uz/page/vidy-vkladov', NULL, NULL, 2, '2019-03-24 00:39:54', '2019-03-24 00:39:54'),
(88, 12, 'madadkorim-36941', 0, 'Madadkorim', NULL, 1, 760, 22, '20', 0, 1, '1', '1', '0', '0', 'Xar oyda', '12 oy- 18%,  18 oy - 19%,  25 oy - 20%', 'http://www.madadinvestbank.uz/page/vidy-vkladov', NULL, '12 oy- 18%,  18 oy - 19%,  25 oy - 20%', 2, '2019-03-24 00:44:26', '2019-03-24 00:51:26'),
(89, 12, 'sarmoya-87466', 0, 'Sarmoya', NULL, 2, 365, 20, '11', 1000, NULL, '1', '1', '0', '0', 'Xar oyda yoki muddat ohirida', '6 oy	 - 9%,  9 oy - 10%, 12 oy - 11%', 'http://www.madadinvestbank.uz/page/vidy-vkladov', NULL, '6 oy	 - 9%,  9 oy - 10%, 12 oy - 11%', 2, '2019-03-24 00:53:29', '2019-03-24 00:53:29'),
(90, 12, 'kelajak-avlodga-madad-20451', 0, 'Kelajak avlodga madad', NULL, 2, 3650, 30, '15', 1000, NULL, '1', '1', '0', '0', 'Muddat ohirida', NULL, 'http://www.madadinvestbank.uz/page/vidy-vkladov', NULL, NULL, 2, '2019-03-24 00:55:45', '2019-03-24 00:55:45'),
(91, 12, 'investitsiya-82318', 0, 'Investitsiya', NULL, 1, 760, 22, '19', 0, 1, '1', '1', '0', '0', 'Xar oyda', '12 oy - 17%,  18 oy - 18%,  25 oy - 19%', 'http://www.madadinvestbank.uz/page/vidy-vkladov', NULL, '12 oy - 17%,  18 oy - 18%,  25 oy - 19%', 2, '2019-03-24 00:57:17', '2019-03-24 00:58:21'),
(92, 12, 'gold-profit-aqsh-dollari-92564', 0, 'Gold Profit  AQSH dollari', NULL, 2, 365, 20, '5', 0, 1, '2', '1', '0', '0', 'Xar oyda yoki muddat ohirida', '3 oy	 - 3%, 6 oy - 4%, 9 oy- 4%, 12 oy - 5%,', 'http://www.madadinvestbank.uz/page/vidy-vkladov', NULL, '3 oy	 - 3%,\r\n6 oy	 - 4%,\r\n9 oy	 - 4%,\r\n12 oy - 5%,', 2, '2019-03-24 01:00:39', '2019-03-24 01:01:51'),
(93, 13, 'talab-qilib-olguncha-63521', 0, 'Talab qilib olguncha', NULL, 2, 0, 32, '0.01', 0, 1, '1', '1', '1', '0', 'ISTALGAN MUDDAT', '0.01%', 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, NULL, 2, '2019-03-24 18:00:07', '2019-03-24 18:00:07'),
(95, 13, 'plastkart-65643', 0, 'Plastkart', NULL, 2, 0, 32, '0.01', 0, 1, '1', '1', '1', '1', 'ISTALGAN MUDDAT', '0,01%', NULL, NULL, NULL, 2, '2019-03-24 18:02:09', '2019-03-24 18:02:09'),
(96, 13, 'navqiron-97987', 0, 'Navqiron', NULL, 2, 3650, 30, '16', 0, 1, '1', '1', '0', '0', 'Muddat ohirida', NULL, NULL, NULL, NULL, 2, '2019-03-24 18:04:32', '2019-03-24 18:04:32'),
(97, 13, 'mustaqillik-95623', 0, 'Mustaqillik', NULL, 1, 365, 20, '11', 0, 1, '1', '1', '0', '1', 'Xar oyda yoki muddat ohirida', NULL, NULL, NULL, NULL, 2, '2019-03-24 18:06:06', '2019-03-24 18:06:06'),
(98, 13, 'tinchlik-27727', 0, 'Tinchlik', NULL, 1, 730, 22, '13', 0, 1, '1', '1', '1', '1', 'Xar oyda yoki muddat ohirida', NULL, NULL, NULL, NULL, 2, '2019-03-24 18:07:53', '2019-03-24 18:07:53'),
(99, 13, 'ustoz-36146', 0, 'Ustoz', NULL, 1, 272, 31, '15', 0, 1, '1', '1', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, '3 oy,6 oy va 9 oy', 2, '2019-03-24 18:09:46', '2019-03-24 18:10:42'),
(100, 13, 'kommunal-imkon-86721', 0, 'Kommunal imkon', NULL, 1, 180, 16, '15', 0, 1, '1', '1', '0', '0', 'Kommunal muddatiga ko\'ra', NULL, 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, '3 oy va 6 oyga', 2, '2019-03-24 18:13:04', '2019-03-24 18:13:04'),
(101, 13, 'nafis-84623', 0, 'Nafis', NULL, 1, 60, 12, '14', 0, 1, '1', '1', '0', '0', 'Muddat ohirida', NULL, 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, '15, 45 kunga, 1 oy, 2 oyga', 2, '2019-03-24 18:15:36', '2019-03-24 18:15:36'),
(102, 13, 'qaldirgoch-17812', 0, 'Qaldirgoch', NULL, 1, 395, 20, '17', 0, 1, '1', '1', '0', '0', 'Xar oyda yoki muddat ohirida', '1 oygacha 0%, 1 oydan so\'ng 15%', 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, '1,3,5,6 oyga 	15,00%\r\n13 oyga	17,00%', 2, '2019-03-24 18:18:07', '2019-03-24 18:18:07'),
(103, 13, 'fayzli-xonadon-89587', 0, 'Fayzli xonadon', NULL, 1, 0, 32, '12', 0, 2, '1', '1', '0', '0', 'Muddat ohirida', NULL, 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, 'ipoteka krediti summasining 25 %', 2, '2019-03-24 18:20:18', '2019-03-24 18:20:18'),
(104, 13, 'obod-turmush-68498', 0, 'Obod turmush', NULL, 1, 270, 31, '13', 0, 1, '1', '1', '0', '0', 'Xar oyda yoki muddat ohirida', NULL, 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, '3 oy	15,00%\r\n6 oy	14,00%\r\n9 oy	13,00%', 2, '2019-03-24 18:22:13', '2019-03-24 18:22:13'),
(105, 13, 'umid-81941', 0, 'Umid', NULL, 1, 90, 13, '15', 0, 1, '1', '1', '0', '0', 'Muddat ohirida', NULL, 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, NULL, 2, '2019-03-24 18:24:03', '2019-03-24 18:24:03'),
(106, 13, 'muruvvat-38603', 0, 'Muruvvat', NULL, 1, 365, 20, '14', 0, 1, '1', '0', '0', '0', 'Xar oyda', NULL, 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, NULL, 2, '2019-03-24 18:25:07', '2019-03-24 18:25:07'),
(107, 13, 'nafosat-88695', 0, 'Nafosat', NULL, 1, 90, 13, '14', 0, 1, '1', '1', '0', '0', NULL, '3 oygacha 14%, 3 oydan 16%', 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, NULL, 2, '2019-03-24 18:26:53', '2019-03-24 18:26:53'),
(108, 13, 'nihol-91113', 0, 'Nihol', NULL, 1, 270, 31, '16', 0, 1, '1', '1', '0', '0', NULL, NULL, 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, '3 oydacha	14,00%\r\n4,5,6 oygacha	15,00%\r\n7,8,9 oygacha	16,00%', 2, '2019-03-24 18:29:05', '2019-03-24 18:29:05'),
(109, 13, 'farovon-90913', 0, 'Farovon', NULL, 1, 730, 22, '17', 0, 1, '1', '1', '0', '0', NULL, 'muddatdan oldin 14 % ga tushadi', 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, NULL, 2, '2019-03-24 18:30:47', '2019-03-24 18:30:47'),
(110, 13, 'makon-92495', 0, 'Makon', NULL, 1, 760, 22, '18', 0, NULL, '1', '0', '0', '0', NULL, 'muddatdan oldin yillik 1 %', 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, NULL, 2, '2019-03-24 18:32:33', '2019-03-24 18:32:33'),
(111, 13, 'kapital-lyuks-73490', 0, 'Kapital lyuks', NULL, 1, 365, 20, '4', 0, NULL, '2', '1', '0', '0', 'Xar oyda', NULL, 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, '1-6 oygacha - 3% ,\r\n6-12 oygacha -  3,5% 4%', 2, '2019-03-24 18:35:05', '2019-03-24 18:35:05'),
(112, 13, 'effektiv-43347', 0, 'Effektiv', NULL, 1, 90, 13, '4', 0, 1, '2', '1', '0', '0', 'Muddat ohirida', NULL, 'http://www.ipotekabank.uz/uz/individuals/deposit/about/', NULL, NULL, 2, '2019-03-24 18:36:09', '2019-03-24 18:36:09'),
(113, 7, 'kommunal-xizmatlar-21577', 0, 'Kommunal xizmatlar', NULL, 1, 90, 13, '17', 0, NULL, '1', '0', '0', '0', 'Maqsadga ko\'ra to\'lov', NULL, 'https://agrobank.uz/uz/page/deposits', NULL, NULL, 0, '2019-03-24 18:37:46', '2019-03-24 18:37:46'),
(114, 7, 'baxtli-bolalik-33673', 0, 'Baxtli bolalik', NULL, 1, 3650, 30, '16', 100000, NULL, '1', '1', '0', '1', 'Har oy kapitalizatsiya uchun o\'tkaziladi', NULL, 'https://agrobank.uz/uz/page/deposits', NULL, '36 oy	\r\n60 oy	\r\n120 oy	\r\nQMS - 16%', 0, '2019-03-24 18:40:27', '2019-03-24 18:41:24'),
(115, 7, 'farovon-84840', 0, 'Farovon', NULL, 1, 760, 22, '20', 1000000, NULL, '1', '0', '0', '0', 'Har oy naqt yoki plastik kartaga', '6 oydan ko\'p saqlangan omonatlarga yillik 7%', 'https://agrobank.uz/uz/page/deposits', NULL, '3 oy	 - 15,00%,\r\n6 oy	 - 16,00%,\r\n9 oy	 - 17,00%,\r\n13 oy - 18,00%,\r\n18 oy - 19,00%,\r\n25 oy - 20,00%,', 0, '2019-03-24 18:43:01', '2019-03-24 18:44:12'),
(116, 7, 'farovon-online-72925', 0, 'Farovon online', NULL, 1, 760, 22, '21', 1000000, NULL, '1', '0', '0', '0', 'Har oy faqat plastik kartaga', '6 oydan ko\'p saqlangan omonatlarga yillik 7%', 'https://agrobank.uz/uz/page/deposits', NULL, '3 oy - 16,00%,\r\n6 oy	 - 17,00%,\r\n9 oy	 - 18,00%,\r\n13 oy - 19,00%,\r\n18 oy - 20,00%,\r\n25 oy - 21,00%,', 0, '2019-03-24 18:46:06', '2019-03-24 18:46:06'),
(117, 7, 'mezon-84515', 0, 'Mezon', NULL, 2, 760, 22, '17', 1000000, NULL, '1', '0', '0', '0', 'Har oy naqt yoki plastik kartaga', '6 oydan ko\'p saqlangan omonatlarga yillik 7%', 'https://agrobank.uz/uz/page/deposits', NULL, '3 oy	, 6 oy, 9 oy - 15,00%,	\r\n13 oy - 16,00%\r\n25 oy - 17,00%', 0, '2019-03-24 23:10:38', '2019-03-24 23:11:44'),
(118, 7, 'mezon-online-61966', 0, 'Mezon online', NULL, 2, 760, 22, '18', 1000000, NULL, '1', '0', '0', '0', 'Har oy faqat plastik kartaga', '6 oydan ko\'p saqlangan omonatlarga yillik 7%', 'https://agrobank.uz/uz/page/deposits', NULL, '3 oy	, 6 oy, 9 oy - 16,00%,	\r\n13 oy - 17,00%\r\n25 oy - 18,00%', 0, '2019-03-24 23:13:59', '2019-03-24 23:13:59'),
(119, 7, 'bahor-2019-79860', 0, 'Bahor 2019', NULL, 1, 395, 20, '20', 1000000, NULL, '1', '0', '0', '0', 'Har oy naqt yoki plastik kartaga', NULL, 'https://agrobank.uz/uz/page/deposits', NULL, NULL, 0, '2019-03-24 23:15:29', '2019-03-24 23:15:29'),
(120, 7, 'bahor-2019-online-72775', 0, 'Bahor 2019 online', NULL, 1, 395, 20, '21', 1000000, NULL, '1', '0', '0', '0', 'Har oy faqat plastik kartaga', NULL, 'https://agrobank.uz/uz/page/deposits', NULL, NULL, 0, '2019-03-24 23:17:05', '2019-03-24 23:17:05'),
(121, 7, 'capital-va-capital-online-16473', 0, 'Capital va Capital online', NULL, 1, 545, 21, '6', 100, NULL, '2', '0', '0', '0', 'har oy naqt yoki plastik kartaga, (online) har oy faqat plastik kartaga', NULL, 'https://agrobank.uz/uz/page/deposits', NULL, '3 oy - 4,00%,\r\n6 oy	 - 4,50%,\r\n9 oy	 - 5,00%,\r\n13 oy - 5,50%,\r\n18 oy - 6,00%,', 0, '2019-03-24 23:19:34', '2019-03-24 23:20:54'),
(122, 7, 'express-va-express-online-92346', 0, 'Express va Express online', NULL, 2, 545, 21, '4.5', 100, NULL, '2', '0', '0', '0', 'har oy naqt yoki plastik kartaga,  (online) har oy faqat plastik kartaga', NULL, 'https://agrobank.uz/uz/page/deposits', NULL, '3 oy	 - 3,00%,\r\n9 oy	 - 3,50%,\r\n13 oy - 4,00%,\r\n18 oy - 4,50%,', 0, '2019-03-24 23:22:39', '2019-03-24 23:23:39'),
(123, 2, 'kelajak-80664', 0, 'Kelajak', NULL, 1, 1095, 23, '21', 0, NULL, '1', '0', '0', '0', 'Har oyda', NULL, NULL, NULL, '1 yil - 19,00%,\r\n2 yil - 20,00%,\r\n3 yil	- 21,00%,', 0, '2019-03-24 23:29:23', '2019-03-24 23:30:25'),
(124, 2, 'quvonch-90999', 0, 'Quvonch', NULL, 1, 90, 13, '9', 0, NULL, '1', '0', '0', '0', 'Har oyda', NULL, NULL, NULL, NULL, 0, '2019-03-25 00:01:48', '2019-03-25 00:01:48'),
(125, 2, 'xazina-78012', 0, 'Xazina', NULL, 1, 1095, 23, '20', 0, NULL, '1', '1', '0', '0', 'Har oyda', NULL, NULL, NULL, NULL, 0, '2019-03-25 00:03:16', '2019-03-25 00:03:16'),
(126, 2, 'orzu-13882', 0, 'Orzu', NULL, 1, 1095, 23, '19', 0, NULL, '1', '1', '0', '0', 'Har oyda', NULL, NULL, NULL, NULL, 0, '2019-03-25 00:04:58', '2019-03-25 00:04:58'),
(127, 2, 'eksklyuziv-55775', 0, 'Eksklyuziv', NULL, 1, 545, 21, '20', 0, NULL, '1', '0', '0', '0', 'Har oyda', NULL, NULL, NULL, NULL, 0, '2019-03-25 00:06:45', '2019-03-25 00:06:45'),
(128, 2, 'optimal-94477', 0, 'Optimal', NULL, 1, 1095, 23, '6', 0, NULL, '2', '1', '0', '0', 'Har oyda', NULL, NULL, NULL, NULL, 0, '2019-03-25 00:08:09', '2019-03-25 00:08:09'),
(129, 11, 'optimal-63727', 0, 'Optimal', NULL, 1, 370, 20, '6', 0, NULL, '2', '0', '0', '0', 'Har oyda', NULL, NULL, NULL, '90kun - 4,00%,\r\n180 kun - 5,00%,\r\n370 kun - 6,00%,', 2, '2019-03-25 00:09:43', '2019-03-25 00:10:32'),
(130, 2, 'meros-93972', 0, 'Meros', NULL, 2, 3650, 30, '4', 0, NULL, '2', '0', '0', '0', 'Har oyda', NULL, NULL, NULL, NULL, 0, '2019-03-25 00:12:05', '2019-03-25 00:12:05'),
(131, 14, 'manfaat-61448', 0, 'Manfaat', NULL, 2, 365, 20, '14', 0, NULL, '1', '1', '0', '0', 'Xohishiga ko\'ra har oyda', NULL, NULL, NULL, NULL, 2, '2019-03-25 00:16:44', '2019-03-25 00:16:44'),
(132, 14, 'baxtli-bolalik-44098', 0, 'Baxtli bolalik', NULL, 2, 5475, 33, '10', 0, NULL, '1', '1', '0', '0', 'Muddat ohirida', NULL, NULL, NULL, NULL, 2, '2019-03-25 00:21:01', '2019-03-25 00:21:01'),
(133, 14, 'yoshlik-96169', 0, 'Yoshlik', NULL, 2, 365, 20, '13', 0, NULL, '1', '1', '0', '0', NULL, NULL, 'Xohishiga ko\'ra har oyda', NULL, '6 va 12 oyga', 2, '2019-03-25 21:11:21', '2019-03-25 21:11:52'),
(134, 14, 'sarmoya-56242', 0, 'Sarmoya', NULL, 1, 1095, 23, '16', 500000, NULL, '1', '0', '0', '0', 'Xohishiga ko\'ra har oyda', NULL, 'https://mikrokreditbank.uz/retail/deposits/', NULL, '18, 24 va 36 oyga', 2, '2019-03-25 21:14:21', '2019-03-25 21:15:01'),
(135, 14, 'tadbirkor-34077', 0, 'Tadbirkor', NULL, 1, 365, 20, '14', 0, NULL, '1', '1', '0', '0', 'Xohishiga ko\'ra har oyda', NULL, 'https://mikrokreditbank.uz/retail/deposits/', NULL, '6 va 12 oyga', 2, '2019-03-25 21:17:05', '2019-03-25 21:18:51'),
(136, 14, 'istiqbol-96198', 0, 'Istiqbol', NULL, 1, 90, 13, '12', 0, NULL, '1', '1', '0', '0', 'Muddat ohirida', NULL, 'https://mikrokreditbank.uz/retail/deposits/', NULL, NULL, 2, '2019-03-25 21:20:38', '2019-03-25 21:20:38'),
(137, 14, 'oila-jamgarmasi-63669', 0, 'Oila jamg\'armasi', NULL, 1, 365, 20, '16', 0, NULL, '1', '1', '0', '0', NULL, NULL, 'https://mikrokreditbank.uz/retail/deposits/', NULL, '6 va 12 oyga', 2, '2019-03-25 21:22:23', '2019-03-25 21:22:23'),
(138, 14, 'kafolat-35171', 0, 'Kafolat', NULL, 1, 1095, 23, '18', 1000000, NULL, '1', '0', '0', '0', NULL, NULL, 'https://mikrokreditbank.uz/retail/deposits/', NULL, '13, 24 va 36 oyga', 2, '2019-03-25 21:23:48', '2019-03-25 21:23:48'),
(139, 14, 'barqaror-82673', 0, 'Barqaror', NULL, 1, 1095, 23, '20', 5000000, NULL, '1', '1', '0', '0', NULL, NULL, 'https://mikrokreditbank.uz/retail/deposits/', NULL, '12, 24 va 36 oyga', 2, '2019-03-25 21:25:14', '2019-03-25 21:25:14'),
(140, 14, 'eksklyuziv-23571', 0, 'Eksklyuziv', NULL, 1, 180, 16, '3', 500, NULL, '2', '1', '0', '0', 'Muddat ohirida', NULL, 'https://mikrokreditbank.uz/retail/deposits/', NULL, '3 va 6 oy', 2, '2019-03-25 21:30:15', '2019-03-25 21:30:15'),
(141, 14, 'tejamkor-online-33854', 0, 'Tejamkor online', NULL, 1, 365, 20, '10', 200000, NULL, '1', '1', '0', '0', 'Har oyda plastik kartaga', NULL, 'https://mikrokreditbank.uz/retail/deposits/', NULL, '6 va 12 oy', 2, '2019-03-25 21:32:01', '2019-03-25 21:32:01'),
(142, 14, 'tadbirkor-online-50478', 0, 'Tadbirkor online', NULL, 1, 365, 20, '14', 500000, NULL, '1', '0', '0', '0', 'Har oyda plastik kartaga', NULL, 'https://mikrokreditbank.uz/retail/deposits/', NULL, '6 va 12 oy', 2, '2019-03-25 21:33:50', '2019-03-25 21:33:50'),
(143, 14, 'sarmoya-online-79071', 0, 'Sarmoya online', NULL, 1, 1095, 23, '16', 500000, NULL, '1', '0', '0', '0', 'Har oyda plastik kartaga', NULL, 'https://mikrokreditbank.uz/retail/deposits/', NULL, '18, 24 va 36 oy', 2, '2019-03-25 21:35:27', '2019-03-25 21:35:27'),
(144, 14, 'istiqlol-online-26771', 0, 'Istiqlol online', NULL, 1, 365, 20, '17', 500000, NULL, '1', '0', '0', '0', 'Har oyda plastik kartaga', NULL, 'https://mikrokreditbank.uz/retail/deposits/', NULL, NULL, 2, '2019-03-25 21:37:04', '2019-03-25 21:37:04'),
(145, 14, 'barqaror-online-32126', 0, 'Barqaror online', NULL, 1, 1095, 23, '21', 1000000, NULL, '1', '1', '0', '0', 'Har oyda plastik kartaga', NULL, 'https://mikrokreditbank.uz/retail/deposits/', NULL, '12, 24 va 36 oy', 2, '2019-03-25 21:38:27', '2019-03-25 21:38:27'),
(146, 3, 'kelajak-uchun-19627', 0, 'Kelajak uchun', NULL, 1, 6570, 34, '14', 1000000, NULL, '1', '1', '0', '0', 'Muddat ohirida', NULL, 'http://aloqabank.uz/uz/page/interactive/omonat-va-depozitlar', NULL, '3-18 yil', 0, '2019-03-25 21:41:31', '2019-03-25 21:41:31'),
(147, 14, 'tadbirkor-85389', 0, 'Tadbirkor', NULL, 1, 2555, 27, '10', 0, 1, '1', '1', '0', '0', 'Har oyda', NULL, 'http://aloqabank.uz/uz/page/interactive/omonat-va-depozitlar', NULL, '2-7 yil', 2, '2019-03-25 21:43:28', '2019-03-25 21:44:22'),
(148, 3, 'qulay-imkoniyat-86407', 0, 'Qulay Imkoniyat', NULL, 1, 365, 20, '14', 1000000, NULL, '1', '1', '0', '0', 'Har oyda AT \"Aloqabank\"ning plastik kartasiga', NULL, 'http://aloqabank.uz/uz/page/interactive/omonat-va-depozitlar', NULL, '1, 2 va 3 oy - 10,00%,\r\n6, 9 va 12 oy - 14,00%', 0, '2019-03-25 21:46:30', '2019-03-25 21:46:30'),
(149, 3, 'kelajakka-sarmoya-60491', 0, 'Kelajakka sarmoya', NULL, 1, 1095, 23, '16', 0, NULL, '1', '1', '0', '0', 'Muddat ohirida AT \"Aloqabank\"ning plastik kartasiga', NULL, 'http://aloqabank.uz/uz/page/interactive/omonat-va-depozitlar', NULL, '2, 3 yil', 0, '2019-03-25 21:49:25', '2019-03-25 21:49:25'),
(150, 3, 'farovon-21153', 0, 'Farovon', NULL, 1, 1095, 23, '19', 0, NULL, '1', '1', '0', '0', 'Har oyda', NULL, 'http://aloqabank.uz/uz/page/interactive/omonat-va-depozitlar', NULL, '13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 oy - 18,00%;\r\n25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36 oy	 - 19,00%', 0, '2019-03-25 21:51:35', '2019-03-25 21:51:35'),
(151, 3, 'kafolat-mobil-54642', 0, 'Kafolat  (mobil)', NULL, 1, 365, 20, '14', 0, NULL, '1', '0', '0', '0', NULL, NULL, 'http://aloqabank.uz/uz/page/interactive/omonat-va-depozitlar', NULL, '1, 2, 3 oy - 10,00%;\r\n4, 5, 6 oy - 12,00%;\r\n7, 8, 9 oy - 13,00%;\r\n10, 11, 12 oy - 14,00%;', 0, '2019-03-25 21:55:03', '2019-03-25 21:55:03'),
(152, 3, 'joziba-mobil-75029', 0, 'Joziba (mobil)', NULL, 1, 730, 22, '17', 0, NULL, '1', '0', '0', '0', NULL, NULL, 'http://aloqabank.uz/uz/page/interactive/omonat-va-depozitlar', NULL, '18 oy -16,00%;\r\n25 oy -17,00%;', 0, '2019-03-25 21:57:17', '2019-03-25 21:57:17'),
(153, 3, 'online-sarmoya-obeydjing-kartalari-orqali-11417', 0, 'ONLINE SARMOYA - obeydjing kartalari orqali', NULL, 1, 1095, 23, '19', 100000, NULL, '1', '1', '0', '0', 'Har oyda', NULL, 'http://aloqabank.uz/uz/page/interactive/omonat-va-depozitlar', NULL, NULL, 0, '2019-03-25 21:59:57', '2019-03-25 21:59:57'),
(154, 3, 'active-99481', 0, 'Active', NULL, 1, 760, 22, '20', 5000000, NULL, '1', '0', '0', '0', 'Har oyda TQO hisobraqamiga ko\'chiriladi', '3 oygacha 0%', 'http://aloqabank.uz/uz/page/interactive/omonat-va-depozitlar', NULL, '13 oy	19,00%	1 000 000,00; \r\n25 oy	20,00%	5 000 000,00;', 0, '2019-03-25 22:02:26', '2019-03-25 22:02:26'),
(155, 15, 'talab-qilib-olinguncha-83301', 0, 'Talab qilib olinguncha', NULL, 2, 0, 32, '0', 0, 1, '1', '1', '0', '0', NULL, NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/talab.php', NULL, NULL, 2, '2019-03-25 23:23:04', '2019-03-25 23:23:04'),
(156, 15, 'kafola-34044', 0, 'Kafolat', NULL, 2, 395, 20, '16', 0, 1, '1', '1', '1', '1', 'Muddat ohirida', 'Omonat muddatidan oldin talab qilinganda, oldin hisoblab berilgan foiz to‘lovlar omonat summasidan ushlab qolinadi.', 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/jamgarma.php', NULL, '6-13 oyga', 2, '2019-03-25 23:26:49', '2019-03-25 23:28:36'),
(158, 15, 'kommunal-xizmat-82578', 0, 'Kommunal xizmat', NULL, 2, 760, 23, '17', 0, 1, '1', '1', '1', '0', 'Maqsadga ko\'ra to\'lov', '12 oydan keyin 50%', 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/jamgarma.php', NULL, '6-25 oyga', 2, '2019-03-25 23:31:03', '2019-03-25 23:31:03'),
(159, 15, 'obod-turmush-78829', 0, 'Obod turmush', NULL, 2, 1095, 23, '13.6', 0, 1, '1', '1', '0', '0', 'Maqsadga ko\'ra to\'lov', '12 oydan keyin 50%', 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/jamgarma.php', NULL, '13-36 oyga', 2, '2019-03-25 23:33:37', '2019-03-25 23:33:37'),
(160, 15, 'barkamol-avlod-75639', 0, 'Barkamol avlod', NULL, 2, 5840, 33, '14.4', 0, 1, '1', '1', '0', '1', 'Muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/jamgarma.php', NULL, '16 yoshgacha', 2, '2019-03-25 23:36:29', '2019-03-25 23:36:29'),
(161, 15, 'buyuk-kelajak-47832', 0, 'Buyuk kelajak', NULL, 2, 6570, 34, '15.2', 0, 1, '1', '1', '0', '1', 'Muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/jamgarma.php', NULL, '18 yoshgacha', 2, '2019-03-25 23:38:43', '2019-03-25 23:38:43'),
(162, 15, 'kafolatli-daromad-14397', 0, 'Kafolatli daromad', NULL, 2, 270, 21, '0', 0, 1, '1', '1', '0', '1', 'Muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/jamgarma.php', NULL, '13-16 oyga', 2, '2019-03-25 23:41:19', '2019-03-25 23:41:19'),
(163, 15, 'mustaqil-yurt-54325', 0, 'Mustaqil yurt', NULL, 2, 180, 16, '14', 0, 1, '1', '1', '0', '1', 'Muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/jamgarma.php', NULL, '2-6 oyga', 2, '2019-03-25 23:43:45', '2019-03-25 23:43:45'),
(164, 15, 'fidoiy-93155', 0, 'Fidoiy', NULL, 1, 90, 13, '15', 0, 1, '1', '0', '0', '0', 'Har oyda va muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/muddatli.php', NULL, '1-3 oygacha,', 2, '2019-03-25 23:45:45', '2019-03-25 23:45:45'),
(165, 15, 'istiqlol-60134', 0, 'Istiqlol', NULL, 1, 180, 16, '15', 0, 1, '1', '0', '0', '0', 'Muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/muddatli.php', NULL, '1-3 oygacha,', 2, '2019-03-25 23:47:43', '2019-03-25 23:47:43'),
(166, 15, 'barakali-sarmoya-97310', 0, 'Barakali sarmoya', NULL, 1, 180, 16, '16', 0, 1, '1', '0', '0', '0', 'Har oyda va muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/muddatli.php', NULL, '4-6 oyga', 2, '2019-03-25 23:49:45', '2019-03-25 23:49:45'),
(167, 15, 'yaxshi-niyat-22279', 0, 'Yaxshi niyat', NULL, 1, 395, 20, '14', 0, NULL, '1', '0', '0', '0', 'Har oyda va muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/muddatli.php', NULL, '3-13 oyga', 2, '2019-03-25 23:52:24', '2019-03-25 23:52:24'),
(168, 15, 'jonajon-vatan-40529', 0, 'Jonajon vatan', NULL, 1, 395, 20, '18', 0, 1, '1', '0', '0', '0', 'Har oyda va muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/muddatli.php', NULL, '7-13 oyga', 2, '2019-03-25 23:56:25', '2019-03-25 23:56:25'),
(169, 15, 'inson-manfaati-32365', 0, 'Inson manfaati', NULL, 1, 760, 22, '20', 0, 1, '1', '0', '0', '0', 'Har oyda va muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/muddatli.php', NULL, '13-25 oyga', 2, '2019-03-25 23:58:54', '2019-03-25 23:58:54'),
(170, 15, 'ishonch-75376', 0, 'Ishonch', NULL, 1, 180, 16, '12', 0, 1, '1', '0', '0', '0', 'Har oyda va muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/muddatli.php', NULL, '1-6 oyga', 2, '2019-03-26 00:07:06', '2019-03-26 00:07:06'),
(171, 15, 'kelajak-bunyodkori-27993', 0, 'Kelajak bunyodkori', NULL, 1, 1095, 23, '21', 0, 1, '1', '0', '0', '0', 'Har oyda va muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/muddatli.php', NULL, '25-36 oyga', 2, '2019-03-26 00:08:35', '2019-03-26 00:08:35'),
(172, 15, 'talab-qilib-olguncha-25349', 0, 'Talab qilib olguncha', NULL, 2, 0, 32, '0', 0, NULL, '2', '1', '1', '0', 'ISTALGAN MUDDAT', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/valyuta.php', NULL, NULL, 2, '2019-03-26 00:10:36', '2019-03-26 00:11:19'),
(173, 15, 'farovon-79271', 0, 'Farovon', NULL, 1, 90, 13, '3', 100, NULL, '1', '0', '0', '0', 'Har oyda va muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/valyuta.php', NULL, '1-3 oyga', 2, '2019-03-26 19:57:45', '2019-03-26 19:57:45'),
(174, 15, 'real-12656', 0, 'Real', NULL, 1, 273, 31, '3.5', 100, NULL, '2', '0', '0', '0', 'Har oyda va muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/valyuta.php', NULL, '4-9 oyga', 2, '2019-03-26 20:06:22', '2019-03-26 20:06:22'),
(175, 15, 'kapital-91991', 0, 'Kapital', NULL, 1, 760, 22, '4', 100, NULL, '2', '0', '0', '0', 'Har oyda va muddat ohirida', NULL, 'http://qishloqqurilishbank.uz/uz/individuals/omonatlar/valyuta.php', NULL, '13-25 oyga', 2, '2019-03-26 20:08:38', '2019-03-26 20:08:38');

-- --------------------------------------------------------

--
-- Структура таблицы `deposit_types`
--

CREATE TABLE `deposit_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_uz` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_ru` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `deposit_types`
--

INSERT INTO `deposit_types` (`id`, `name_uz`, `name_ru`, `description_uz`, `description_ru`, `created_at`, `updated_at`) VALUES
(1, 'Muddatli', 'Срочный депозит', 'Muddatli omonat turi', 'Тип срочного депозита', '2019-02-17 17:08:18', '2019-06-05 17:59:00'),
(2, 'Jamg\'arma', 'Фонд', 'Jamg\'arma omonat turi', 'Тип сберегательного депозита', '2019-02-17 17:10:10', '2019-06-05 18:00:20');

-- --------------------------------------------------------

--
-- Структура таблицы `exchange_rates`
--

CREATE TABLE `exchange_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `bank_id` int(11) NOT NULL,
  `currency` int(11) DEFAULT NULL,
  `take` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `exchange_rates`
--

INSERT INTO `exchange_rates` (`id`, `bank_id`, `currency`, `take`, `sale`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '8400', '8500', '2019-03-10 20:29:00', '2019-03-10 20:29:00'),
(2, 1, 3, '135', '136', '2019-03-10 20:31:54', '2019-06-05 07:06:02'),
(3, 2, 2, '8400,35', '8500,2', '2019-06-05 07:50:05', '2019-06-05 07:50:05'),
(4, 3, 3, '134,5', '135.4', '2019-06-05 07:50:43', '2019-06-05 07:50:43');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_02_05_192140_create_banks_table', 1),
(3, '2019_02_05_200726_create_contacts_table', 1),
(4, '2019_02_05_201255_create_bank_services_table', 1),
(5, '2019_02_05_202511_create_regions_table', 1),
(6, '2019_02_05_203939_create_deposits_table', 1),
(8, '2019_02_05_212503_create_deposit_types_table', 1),
(9, '2019_02_07_223656_create_services_table', 1),
(10, '2019_02_12_000114_create_date_services_table', 1),
(11, '2019_02_16_002153_create_currencies_table', 2),
(13, '2019_02_23_200445_create_banners_table', 3),
(14, '2019_02_05_204134_create_credits_table', 4),
(15, '2019_02_27_180026_create_credit_types_table', 5),
(16, '2019_02_28_152240_create_comments_table', 6),
(17, '2019_03_11_003352_create_exchange_rates_table', 7);

-- --------------------------------------------------------

--
-- Структура таблицы `regions`
--

CREATE TABLE `regions` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `regions`
--

INSERT INTO `regions` (`id`, `parent_id`, `name_uz`, `name_ru`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Xorazm viloyati', 'Хорезмская область', '2019-02-14 06:32:36', '2019-06-06 06:13:10'),
(2, NULL, 'Toshkent shahar', 'Город Ташкент', '2019-02-14 06:34:19', '2019-06-06 06:04:48'),
(3, 2, 'Chilonzor', 'Чиланзар', '2019-02-14 06:55:02', '2019-06-06 05:53:54'),
(4, NULL, 'Andijon viloyati', 'Андижанская область', '2019-06-06 05:56:08', '2019-06-06 06:13:23'),
(5, NULL, 'Buxoro viloyati', 'Бухарская область', '2019-06-06 05:56:39', '2019-06-06 06:13:34'),
(6, NULL, 'Farg\'ona viloyati', 'Ферганская область', '2019-06-06 05:57:09', '2019-06-06 06:14:17'),
(7, NULL, 'Jizzax viloyati', 'Джизакская область', '2019-06-06 05:57:23', '2019-06-06 06:14:30'),
(8, NULL, 'Namangan viloyati', 'Наманганская область', '2019-06-06 05:57:44', '2019-06-06 06:14:42'),
(9, NULL, 'Navoiy viloyati', 'Навоийская область', '2019-06-06 05:58:00', '2019-06-06 06:14:52'),
(10, NULL, 'Qashqadaryo viloyati', 'Кашкадарьинская область', '2019-06-06 05:58:16', '2019-06-06 06:15:04'),
(11, NULL, 'Qoraqalpog\'iston Respublikasi', 'Республика Каракалпакстан', '2019-06-06 05:58:44', '2019-06-06 06:15:14'),
(12, NULL, 'Samarqand viloyati', 'Самаркандская область', '2019-06-06 05:59:08', '2019-06-06 06:15:25'),
(13, NULL, 'Sirdaryo viloyati', 'Сырдарьинская область', '2019-06-06 05:59:22', '2019-06-06 06:15:36'),
(14, NULL, 'Surxondaryo viloyati', 'Сурхандарьинская область', '2019-06-06 06:00:15', '2019-06-06 06:15:46'),
(15, NULL, 'Toshkent viloyat', 'Ташкентская область', '2019-06-06 06:01:33', '2019-06-06 06:10:16');

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name_uz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`id`, `parent_id`, `name_uz`, `name_ru`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Omonatlar', 'Депозиты', 1, '2019-02-17 12:31:10', '2019-06-05 19:51:17'),
(2, NULL, 'Kreditlar', 'Кредиты', 1, '2019-02-17 12:31:24', '2019-06-05 19:52:31'),
(3, NULL, 'Kartalar', 'Карты', 1, '2019-02-17 12:31:39', '2019-06-05 19:53:42'),
(4, NULL, 'Ipoteka', 'Ипотека', 1, '2019-02-17 12:31:52', '2019-06-05 20:00:03'),
(5, NULL, 'Sug\'urta', 'Страховка', 1, '2019-02-17 12:32:07', '2019-06-05 20:00:22');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT '0',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role_id`, `email_verified_at`, `password`, `password_text`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Sharif Karimov', 'root_admin@mail.ru', 1, NULL, '$2y$10$8GdUOjXbrjVK7oPvtWcC0Ofu0UYjT1gs8VARHwolKioIoPJjvjwBS', 'root123456', 'FkDFdKSs4o86gMANBTqMzh1kQXzGPggM9keYq9F7BEGhrYpqTA6Y5n29fM2m', '2019-02-28 08:14:16', '2019-02-28 08:14:16');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `id` (`id`);

--
-- Индексы таблицы `bank_services`
--
ALTER TABLE `bank_services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bank_services_slug_unique` (`slug`);

--
-- Индексы таблицы `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `credits`
--
ALTER TABLE `credits`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `credits_slug_unique` (`slug`),
  ADD KEY `credits_bank_id_index` (`bank_id`);

--
-- Индексы таблицы `credit_types`
--
ALTER TABLE `credit_types`
  ADD PRIMARY KEY (`id`,`slug`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `date_services`
--
ALTER TABLE `date_services`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `deposits`
--
ALTER TABLE `deposits`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `deposits_slug_unique` (`slug`),
  ADD KEY `deposits_bank_id_index` (`bank_id`),
  ADD KEY `deposit_percent` (`deposit_percent`);

--
-- Индексы таблицы `deposit_types`
--
ALTER TABLE `deposit_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `exchange_rates`
--
ALTER TABLE `exchange_rates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `bank_services`
--
ALTER TABLE `bank_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `credits`
--
ALTER TABLE `credits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `credit_types`
--
ALTER TABLE `credit_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `date_services`
--
ALTER TABLE `date_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT для таблицы `deposits`
--
ALTER TABLE `deposits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT для таблицы `deposit_types`
--
ALTER TABLE `deposit_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `exchange_rates`
--
ALTER TABLE `exchange_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
