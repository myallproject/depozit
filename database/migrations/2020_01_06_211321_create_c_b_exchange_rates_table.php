<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCBExchangeRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_b_exchange_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code')->nullable();
            $table->string('key')->nullable();
            $table->string('name')->nullable();
            $table->integer('nominal')->default('1');
            $table->string('rate');
            $table->string('diff')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_b_exchange_rates');
    }
}
