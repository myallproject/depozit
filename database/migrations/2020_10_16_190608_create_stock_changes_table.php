<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('ordinary_stock_price_old')->nullable();
            $table->float('preferred_stock_price_old')->nullable();
            $table->float('ordinary_stock_price_new')->nullable();
            $table->float('preferred_stock_price_new')->nullable();
            $table->float('change_percentage_up')->nullable();
            $table->float('change_percentage_down')->nullable();
            $table->unsignedBigInteger('stock_id');
            $table->foreign('stock_id')->references('id')->on('stocks')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_changes');
    }
}
