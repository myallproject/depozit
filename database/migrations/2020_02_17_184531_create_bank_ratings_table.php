<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id');
            $table->string('moliyaviy')->nullable();
            $table->string('ommabop')->nullable();
            $table->string('urtacha')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_ratings');
    }
}
