<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditChildrenDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_children_dates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('credit_id')->unsigned();
            $table->foreign('credit_id')->references('id')->on('credits')->onDelete('cascade');
            $table->integer('date_id')->unsigned();
            $table->foreign('date_id')->references('id')->on('date_services')->onDelete('cascade');
            $table->integer('currency_id')->unsigned();
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->integer('date')->nullable();
            $table->integer('date_type')->unsigned();
            $table->foreign('date_type')->references('id')->on('date_services');
            $table->string('date_name_uz')->nullable();
            $table->string('date_name_ru')->nullable();
            $table->float('percent')->nullable();
            $table->float('sum')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_children_dates');
    }
}
