<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSaleColumnsInAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debit_cards', function (Blueprint $table) {
            $table->renameColumn('aktsiya_izoh_uz', 'aktsiya_izoh');
            $table->renameColumn('aktsiya_izoh_ru', 'aktsiya_link');
        });

        Schema::table('deposits', function (Blueprint $table) {
            $table->renameColumn('aktsiya_izoh_uz', 'aktsiya_izoh');
            $table->renameColumn('aktsiya_izoh_ru', 'aktsiya_link');
        });

        Schema::table('credits', function (Blueprint $table) {
            $table->renameColumn('aktsiya_izoh_uz', 'aktsiya_izoh');
            $table->renameColumn('aktsiya_izoh_ru', 'aktsiya_link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('debit_cards', function (Blueprint $table) {
            $table->renameColumn('aktsiya_izoh', 'aktsiya_izoh_uz');
            $table->renameColumn('aktsiya_linkaktsiya_izoh_ru', 'aktsiya_izoh_ru');
        });

        Schema::table('deposits', function (Blueprint $table) {
            $table->renameColumn('aktsiya_izoh', 'aktsiya_izoh_uz');
            $table->renameColumn('aktsiya_linkaktsiya_izoh_ru', 'aktsiya_izoh_ru');
        });

        Schema::table('credits', function (Blueprint $table) {
            $table->renameColumn('aktsiya_izoh', 'aktsiya_izoh_uz');
            $table->renameColumn('aktsiya_linkaktsiya_izoh_ru', 'aktsiya_izoh_ru');
        });
    }
}
