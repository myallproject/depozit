<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('bank_id')->index();
            
            $table->string('slug')->unique();
            
            $table->integer('status')->default(0);
            
            $table->string('name_uz')->nullable();
            
            $table->string('name_ru')->nullable();
            
            $table->unsignedInteger('deposit_type_id')->nullable();
            
            $table->integer('deposit_date')->nullable();
            
            $table->integer('deposit_date_type')->nullable();
            
            $table->string('deposit_percent',10)->nullable();
            
            $table->string('min_sum')->nullable();
            
            $table->string('currency',5)->nullable();
            
            $table->string('account_fill')->nullable();
            
            $table->string('partly_take')->nullable();
            
            $table->string('percents_capitalization')->nullable();
            
            $table->string('percent_paid_period')->nullable();
            
            $table->string('close_before_date')->nullable();
            
            $table->string('link',1000)->nullable();
            
            $table->text('text')->nullable();
            
            $table->integer('region_id')->nullable();
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposits');
    }
}
