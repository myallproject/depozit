<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreditsTalablar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credits', function (Blueprint $table) {
            $table->text('credit_borrower_possible_uz')->nullable();
            $table->text('credit_borrower_possible_ru')->nullable();
            
            $table->text('credit_borrower_additional_uz')->nullable();
            $table->text('credit_borrower_additional_ru')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
