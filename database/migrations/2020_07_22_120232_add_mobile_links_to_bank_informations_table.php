<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMobileLinksToBankInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_informations', function (Blueprint $table) {
            $table->string('mobile_app')->nullable();
            $table->string('app_store')->nullable();
            $table->string('play_market')->nullabel();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_informations', function (Blueprint $table) {
            $table->dropColumn('mobile_app');
            $table->dropColumn('app_store');
            $table->dropColumn('play_market');
        });
    }
}
