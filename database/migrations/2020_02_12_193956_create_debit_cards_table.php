<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebitCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debit_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id');
            $table->integer('bank_type_id');
            $table->string('name_uz')->nullable();
            $table->string('name_ru')->nullable();
            $table->integer('type_card_id');
            $table->integer('currency_id')->nullable();
            $table->integer('payment_id')->nullable();
            $table->integer('date_id')->nullable();
            $table->string('price')->nullable();
            $table->integer('status')->default(0);
            $table->integer('kamay_qoldiq')->default(0);
            $table->string('hisoblash_usuli')->nullable();
            $table->text('qushimcha_qulaylik_uz');
            $table->text('qushimcha_qulaylik_ru');
            $table->text('documents_uz');
            $table->text('documents_ru');
            $table->string('rasmiy_yuli_uz')->nullable();
            $table->string('rasmiy_yuli_ru')->nullable();
            $table->string('qushimcha_shart_uz')->nullable();
            $table->string('qushimcha_shart_ru')->nullable();
            $table->string('link')->nullable();
            $table->string('bepul_xizmat')->default(0);
            $table->string('bepul_naqdlash')->default(0);
            $table->string('bepul_tulov')->default(0);
            $table->string('kontaktsiz_tulov')->default(0);
            $table->string('cash_back')->default(0);
            $table->string('tuluv_komissia')->nullable();
            $table->string('naqdlash_komissia')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debit_cards');
    }
}
