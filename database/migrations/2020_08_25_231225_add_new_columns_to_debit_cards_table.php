<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToDebitCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debit_cards', function (Blueprint $table) {
            $table->string('tuluv_komissia_boshqa')->nullable();
            $table->string('tuluv_komissia_boshqa_izoh_uz')->nullable();
            $table->string('tuluv_komissia_boshqa_izoh_ru')->nullable();

            $table->string('naqdlash_komissia_terminal')->nullable();
            $table->string('naqdlash_komissia_terminal_izoh_uz')->nullable();
            $table->string('naqdlash_komissia_terminal_izoh_ru')->nullable();

            $table->string('urgent_opening')->nullable();
            $table->string('re_issue_card')->nullable();
            $table->string('sms_inform')->nullable();
            $table->string('service_uz')->nullable();
            $table->string('service_ru')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('debit_cards', function (Blueprint $table) {
            //
        });
    }
}
