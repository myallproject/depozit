<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('reviews', function (Blueprint $table) {
            $table->text('info_bank')->nullable();
         });
         Schema::table('bank_offices', function (Blueprint $table) {
            $table->integer('position')->default(0);
         });
         Schema::table('services', function (Blueprint $table) {
            $table->integer('position')->default(0);
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
