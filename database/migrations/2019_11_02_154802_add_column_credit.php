<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCredit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credits', function (Blueprint $table) {
            $table->integer('credit_data_string')->nullable();
            $table->integer('credit_goal_id')->nullable();
            $table->integer('credit_pledge_id')->nullable();
            $table->integer('credit_surety_id')->nullable();
            $table->integer('credit_proof_income_id')->nullable();
            $table->integer('credit_issuing_form_id')->nullable();
            $table->integer('credit_review_period_id')->nullable();
            $table->string('credit_borrower_age')->nullable();
            $table->integer('credit_borrower_category_id')->nullable();
            $table->integer('credit_personal_insurance')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credits', function (Blueprint $table) {
            //
        });
    }
}
