<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToBankInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_informations', function (Blueprint $table) {
            
            $table->renameColumn('phone', 'phone1');
            $table->string('phone2')->nullable();
            $table->string('phone3')->nullable();

            $table->renameColumn('site', 'site_link_uz');
            $table->string('site_link_ru')->nullable();

            $table->string('official_name_uz')->nullable();
            $table->string('official_name_ru')->nullable();

            $table->string('chairman_uz')->nullable();
            $table->string('chairman_ru')->nullable();

            $table->string('stockholders_uz')->nullable();
            $table->string('stockholders_ru')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_informations', function (Blueprint $table) {
            $table->renameColumn('phone1', 'phone');
            $table->dropColumn('phone2');
            $table->dropColumn('phone3');

            $table->renameColumn('site_link_uz', 'link');
            $table->dropColumn('site_link_ru');

            $table->dropColumn('official_name_uz');
            $table->dropColumn('official_name_ru');


            $table->dropColumn('chairman_uz');
            $table->dropColumn('chairman_ru');

            $table->dropColumn('stockholders_uz');
            $table->dropColumn('stockholders_ru');
        });
    }
}
