<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDataTypesAksiyaColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debit_cards', function (Blueprint $table) {
            $table->mediumText('aktsiya_izoh')->change();
            $table->mediumText('aktsiya_link')->change();
        });

        Schema::table('deposits', function (Blueprint $table) {
            $table->mediumText('aktsiya_izoh')->change();
            $table->mediumText('aktsiya_link')->change();
        });

        Schema::table('credits', function (Blueprint $table) {
            $table->mediumText('aktsiya_izoh')->change();
            $table->mediumText('aktsiya_link')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('debit_cards', function (Blueprint $table) {
            $table->string('aktsiya_izoh')->change();
            $table->string('aktsiya_link')->change();
        });

        Schema::table('deposits', function (Blueprint $table) {
            $table->string('aktsiya_izoh')->change();
            $table->string('aktsiya_link')->change();
        });

        Schema::table('credits', function (Blueprint $table) {
            $table->string('aktsiya_izoh')->change();
            $table->string('aktsiya_link')->change();
        });
    }
}
