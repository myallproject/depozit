<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsInDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->string('input_no_cash')->nullable ()->default (0)->after('close_before_date');
            $table->string('input_in_cash')->nullable ()->default (0)->after('close_before_date');
            $table->string('input_mobile_app')->nullable ()->default (0)->after('close_before_date');
            $table->string('foreign_bank')->nullable ()->default (0)->after('close_before_date');
            $table->string('private_bank')->nullable ()->default (0)->after('close_before_date');
            $table->string('state_bank')->nullable ()->default (0)->after('close_before_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deposits', function (Blueprint $table) {
            //
        });
    }
}
