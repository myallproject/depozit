<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('news');
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_uz',1000)->nullable();
            $table->string('title_ru',1000)->nullable();
            $table->string('description_uz',1000)->nullable();
            $table->string('description_ru',1000)->nullable();
            $table->string('image',1000)->nullable();
            $table->string('link',1000)->nullable();
            $table->string('slug',1000)->nullable();
            $table->string('new_category_id')->nullable();
            $table->text('text_uz');
            $table->text('text_ru');
            $table->integer('user_id')->nullable();
            $table->integer('views')->default(0);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
