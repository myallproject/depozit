<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('slug')->null();
            
            $table->string('name_uz')->nullable();
            $table->string('name_ru')->nullable();
            
            $table->string('phone')->nullable();
            
            $table->string('image')->nullable();
            
            $table->string('description_uz',1000)->nullable();
            
            $table->string('description_ru',1000)->nullable();
            
            $table->text('text_uz')->nullable();
            
            $table->text('text_ru')->nullable();
            
            $table->string('address',1000)->nullable();
            
            $table->string('location',1000)->nullable();
            
            $table->unsignedInteger('region_id')->nullable();
            
            $table->integer('status')->default(0);
            
            $table->integer('views')->default(0);
            
            $table->string('link')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
