<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeFieldsToDebitCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debit_cards', function (Blueprint $table) {
            $table->integer('type_card_id')->nullable()->change();
            $table->string('naqdlash_komissia_boshqa')->nullable();
            $table->string('kontaksiz_tolov_izoh_uz')->nullable();
            $table->string('kontaksiz_tolov_izoh_ru')->nullable();
            $table->string('cash_back_izoh_uz')->nullable();
            $table->string('cash_back_izoh_ru')->nullable();
            $table->string('uyga_yetkazish_izoh_uz')->nullable();
            $table->string('uyga_yetkazish_izoh_ru')->nullable();
            $table->string('tuluv_komissia_izoh_uz')->nullable();
            $table->string('tuluv_komissia_izoh_ru')->nullable();
            $table->string('naqdlash_komissia_izoh_uz')->nullable();
            $table->string('naqdlash_komissia_izoh_ru')->nullable();
            $table->string('naqdlash_komissia_boshqa_izoh_uz')->nullable();
            $table->string('naqdlash_komissia_boshqa_izoh_ru')->nullable();
            $table->string('price_izoh_uz')->nullable();
            $table->string('price_izoh_ru')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('debit_cards', function (Blueprint $table) {
            //
        });
    }
}
