<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id');
            $table->integer('bank_type_id');
            $table->integer('currency_id')->default(0);
            $table->string('name_uz')->nullable();
            $table->string('name_ru')->nullable();
            $table->integer('date_id')->nullable();
            $table->integer('status')->default(0);
            $table->string('imtoyozli_davr_uz')->nullable();
            $table->string('imtoyozli_davr_ru')->nullable();
            $table->string('percent')->nullable();
            $table->string('hisoblash_usul')->nullable();
            $table->string('max_sum')->default(0);
            $table->string('max_sum_text_uz')->nullable();
            $table->string('max_sum_text_ru')->nullable();
            $table->string('kurish_muddati')->nullable();
            $table->text('documents_uz');
            $table->text('documents_ru');
            $table->integer('taminot_turi_ids')->nullable();
            $table->text('taminot_text_uz');
            $table->text('taminot_text_ru');
            $table->string('rasmiy_yuli_uz',1000);
            $table->string('rasmiy_yuli_ru',1000);
            $table->string('ajratish_uz',1000);
            $table->string('ajratish_ru',1000);
            $table->text('sundirish_uz');
            $table->text('sundirish_ru');
            $table->text('talablar_uz');
            $table->text('talablar_ru');
            $table->text('qushim_shart_uz');
            $table->text('qushim_shart_ru');
            $table->string('link')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_cards');
    }
}
