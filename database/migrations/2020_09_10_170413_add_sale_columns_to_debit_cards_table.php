<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSaleColumnsToDebitCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debit_cards', function (Blueprint $table) {
            $table->integer('status')->default(1)->change();
            $table->dropColumn('hisoblash_usuli');

            $table->string('aksiya')->default(0);
            $table->string('aksiya_izoh_uz')->nullable();
            $table->string('aksiya_izoh_ru')->nullable();

            $table->string('qushimcha_karta_ochish')->nullable();

            $table->string('d_secure')->default(0);

            $table->string('lounge_key')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('debit_cards', function (Blueprint $table) {
            $table->integer('status')->default(0)->change();
            $table->string('hisoblash_usuli')->nullable();
            $table->dropColumn('aksiya');
            $table->dropColumn('aksiya_izoh_uz');
            $table->dropColumn('aksiya_izoh_ru');
            $table->dropColumn('qushimcha_karta_ochish');
            $table->dropColumn('d_secure');
            $table->dropColumn('lounge_key');
        });
    }
}
