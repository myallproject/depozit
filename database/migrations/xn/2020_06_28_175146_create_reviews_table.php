<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 1000);
            $table->text('fulltext');
            $table->tinyInteger('assessment');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedInteger('bank_id');
            $table->foreign('bank_id')->references('id')->on('banks')->onDelete('cascade');

            $table->unsignedInteger('branch_id')->nullable();
            $table->foreign('branch_id')->references('id')->on('bank_offices')->onDelete('cascade');

            $table->unsignedInteger('service_id')->nullable();
            $table->foreign('service_id')->references('id')->on('bank_services')->onDelete('cascade');

            $table->unsignedInteger('region_id')->nullable();
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('set null');

            $table->tinyInteger('status')->default(1)->comment('1 - waiting, 2 - accepted, 0 - rejected');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
