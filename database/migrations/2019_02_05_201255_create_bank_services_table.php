<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_services', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('bank_id')->nullable();
            
            $table->string('slug')->unique();
            
            $table->string('name_uz')->nullable();
            
            $table->string('name_ru')->nullable();
            
            $table->string('description_uz', 1000)->nullable();
            
            $table->string('description_ru',1000)->nullable();
            
            $table->text('text_uz')->nullable();
            
            $table->text('text_ru')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_services');
    }
}
