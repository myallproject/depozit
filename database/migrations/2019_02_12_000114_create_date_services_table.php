<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('date_services', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('name_uz')->nullable();
            $table->string('name_ru')->nullable();
            $table->integer('status')->nullable();
            $table->integer('child_service_id')->nullable();

            $table->string('description')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('date_services');
    }
}
