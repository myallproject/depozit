<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedColumnToAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banks', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('bank_informations', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('bank_offices', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('bank_ratings', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('bank_services', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('bank_types', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('banners', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('cards', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('cards_provision_types', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credits', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_borrower_ages', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_borrower_categories', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_bosh_badals', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_cards', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_children_dates', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_goals', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_issuing_forms', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_pledges', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_proof_of_incomes', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_provisions', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_review_periods', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_sureties', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('credit_types', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('currencies', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('c_b_exchange_rates', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('data_update_times', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('date_services', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('debit_cards', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('debit_card_types', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('deposits', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('deposits_dates', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('deposit_childrens', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('deposit_pay_percent_periods', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('deposit_types', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('documents', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('exchange_rates', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('kredit_kart_taminot_turis', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('minimal_payments', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('news', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('news_categories', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('organizations', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('other_exchange_rate_banks', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('payment_types', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('rate_exchange_types', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('regions', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('services', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('type_banks', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('type_open_deposits', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('user_roles', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('visitors', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });

        Schema::table('yes_or_nos', function (Blueprint $table) {
            $table->boolean('deleted')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banks', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('bank_informations', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('bank_offices', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('bank_ratings', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('bank_services', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('bank_types', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('banners', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('cards', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('cards_provision_types', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credits', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_borrower_ages', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_borrower_categories', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_bosh_badals', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_cards', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_children_dates', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_goals', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_issuing_forms', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_pledges', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_proof_of_incomes', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_provisions', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_review_periods', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_sureties', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('credit_types', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('currencies', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('c_b_exchange_rates', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('data_update_times', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('date_services', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('debit_cards', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('debit_card_types', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('deposits', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('deposits_dates', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('deposit_childrens', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('deposit_pay_percent_periods', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('deposit_types', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('documents', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('exchange_rates', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('kredit_kart_taminot_turis', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('minimal_payments', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('news_categories', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('organizations', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('other_exchange_rate_banks', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('payment_types', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('rate_exchange_types', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('regions', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('type_banks', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('type_open_deposits', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('user_roles', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('visitors', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });

        Schema::table('yes_or_nos', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });
    }
}
