<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositChildrensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_childrens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('deposit_id')->nullable();
            $table->integer('date_id')->nullable();
            $table->string('date_name_for_site')->nullable();
            $table->string('date')->nullable();
            $table->integer('date_type')->nullable();
            $table->integer('currency_id')->nullable();
            $table->float('percent')->nullable();
            $table->float('sum')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_childrens');
    }
}
