<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_terms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shop_telephone_id');
            $table->foreign('shop_telephone_id')->references('id')->on('shop_telephones')->onDelete('cascade');
            $table->integer('term');
            $table->bigInteger('monthly_payment');
            $table->bigInteger('total_payment');
            //1 for kun, 2 for oy, 3 for yil. default oy
            $table->unsignedTinyInteger('term_type')->default(2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_terms');
    }
}
