<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('car_brand_id');
            $table->foreign('car_brand_id')->references('id')->on('car_brands')->onDelete('cascade');
            $table->string('name_uz');
            $table->string('name_ru')->nullable();
            //1-pozitsiya 2-pozitsiya 3-pozitsiya 4-pozitsiya
            $table->tinyInteger('pozition')->default(1);
            $table->integer('price');
            //1-uzs 2-usd 3-rubl 4-euro
            $table->tinyInteger('price_category')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
