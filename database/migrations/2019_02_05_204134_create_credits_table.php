<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditsTable extends Migration
{
    public function down()
    {
        Schema::dropIfExists('credits');
    }
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credits', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('bank_id')->index();
    
            $table->string('slug')->unique();
    
            $table->integer('status')->default(0);
            
            $table->integer('type_credit_id')->nullable();
            
            $table->string('region_id')->nullable();
            
            $table->string('name_uz')->nullable();
    
            $table->string('name_ru')->nullable();
            
            $table->integer('date_credit')->default(0);
            
            $table->integer('type_date')->nullable();
            
            $table->string('date_privilege_uz')->nullable();
            $table->string('date_privilege_ru')->nullable();
            
            $table->integer('percent')->default(0);
            
            $table->integer('amount')->default(0);
            
            $table->string('text_amount_uz')->nullable();
            $table->string('text_amount_ru')->nullable();
            
            $table->string('first_compensation_uz')->nullable();
            $table->string('first_compensation_ru')->nullable();
            
            $table->integer('currency')->nullable();
            
            $table->string('type_provision_uz')->nullable();
            $table->string('type_provision_ru')->nullable();
            
            $table->text('goal_credit_uz')->nullable();
            $table->text('goal_credit_ru')->nullable();
            
            $table->text('documents_uz')->nullable();
            $table->text('documents_ru')->nullable();
            
            $table->text('addition_rules_ru')->nullable();
            $table->text('addition_rules_uz')->nullable();
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

}
