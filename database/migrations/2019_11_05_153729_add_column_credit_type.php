<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCreditType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credits', function (Blueprint $table) {
            $table->integer('type_date_filter')->nullable();
            $table->string('garov',1000)->default(0);
            $table->string('kafillik',1000)->default(0);
            $table->string('sugurta_polisi',1000)->default(0);
            $table->string('rasmiylashtirish',1000)->default(0);
            $table->string('ajratish_tartibi',1000)->default(0);
            $table->string('sundirish_tartibi',1000)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credits', function (Blueprint $table) {
            //
        });
    }
}
