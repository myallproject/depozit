<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyAPISTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_a_p_i_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('organization_id')->unsigned();
            $table->foreign('organization_id')->references('id')->on('banks')->onDelete('cascade');
            $table->string('api',10000)->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_a_p_i_s');
    }
}
