<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTelephonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('telephones');
        Schema::create('telephones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('smartphone_model');
            $table->string('smartphone_type')->nullable();
            $table->string('smartphone_body_type')->nullable();
            $table->string('smartphone_body_material')->nullable();
            $table->string('smartphone_sim_number')->nullable();
            $table->string('smartphone_sim_type')->nullable();
            $table->string('smartphone_sim_mode')->nullable();
            $table->string('smartphone_weight')->nullable();
            $table->string('smartphone_dimension')->nullable();
            $table->string('smartphone_display_type')->nullable();
            $table->string('smartphone_diagonal')->nullable();
            $table->string('smartphone_image_size')->nullable();
            $table->string('smartphone_pixels')->nullable();
            $table->string('smartphone_audio')->nullable();
            $table->string('smartphone_earphone')->nullable();
            $table->string('smartphone_standard')->nullable();
            $table->string('smartphone_interface')->nullable();
            $table->string('smartphone_inner_memory')->nullable();
            $table->string('smartphone_operation_memory')->nullable();
            $table->string('smartphone_memory_card')->nullable();
            $table->string('smartphone_battery_type')->nullable();
            $table->string('smartphone_battery_volume')->nullable();
            $table->string('smartphone_battery_lifetime_call')->nullable();
            $table->string('smartphone_battery_lifetime_sleep')->nullable();
            $table->string('smartphone_other')->nullable();
            $table->string('smartphone_internet_connection')->nullable();
            $table->string('smartphone_os')->nullable();
            $table->string('smartphone_os_beginning')->nullable();
            $table->string('smartphone_body_construction')->nullable();
            $table->string('smartphone_contactless_payment')->nullable();
            $table->string('smartphone_sensor_type')->nullable();
            $table->string('smartphone_proportion')->nullable();
            $table->integer('smartphone_main_camera_number')->nullable();
            $table->string('smartphone_main_camera_resolution')->nullable();
            $table->string('smartphone_main_camera_diafragma')->nullable();
            $table->string('smartphone_flashlight')->nullable();
            $table->string('smartphone_main_camera_functions')->nullable();
            $table->string('smartphone_video_recording')->nullable();
            $table->string('smartphone_front_camera')->nullable();
            $table->string('smartphone_processor')->nullable();
            $table->string('smartphone_core')->nullable();
            $table->string('smartphone_video_processor')->nullable();
            $table->string('smartphone_wireless_charging')->nullable();
            $table->string('smartphone_fast_charging')->nullable();
            $table->string('smartphone_loud_speaker')->nullable();
            $table->string('smartphone_managing')->nullable();
            $table->string('smartphone_detectors')->nullable();
            $table->string('smartphone_video_resolution')->nullable();
            $table->string('smartphone_video_shots')->nullable();
            $table->string('smartphone_lte')->nullable();
            $table->string('smartphone_battery')->nullable();
            $table->string('smartphone_battery_lifetime_music')->nullable();
            $table->string('smartphone_glass')->nullable();
            $table->string('smartphone_sales_beginning')->nullable();
            $table->string('smartphone_display_strength')->nullable();
            $table->string('smartphone_geo_tagging')->nullable();
            $table->string('smartphone_navigation')->nullable();
            $table->string('smartphone_gps')->nullable();
            $table->string('smartphone_profile')->nullable();
            $table->unsignedBigInteger('brand_id');
            $table->timestamps();
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
