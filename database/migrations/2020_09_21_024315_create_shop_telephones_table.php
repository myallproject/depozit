<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopTelephonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_telephones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shop_id');
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
            $table->unsignedBigInteger('telephone_id')->nullable();
            $table->foreign('telephone_id')->references('id')->on('telephones')->onDelete('cascade');     
            $table->unsignedBigInteger('brand_id');
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');         
            $table->string('url');
            $table->bigInteger('smartphone_price');
            $table->unsignedTinyInteger('status')->default(1);
            //1 active 2 ....
            $table->string('smartphone_model')->nullable();

            $table->string('smartphone_weight')->nullable();
            $table->string('smartphone_dimension')->nullable();

            $table->string('smartphone_inner_memory')->nullable();
            $table->string('smartphone_operation_memory')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_telephones');
    }
}
