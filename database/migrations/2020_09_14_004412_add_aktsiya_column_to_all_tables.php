<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAktsiyaColumnToAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debit_cards', function (Blueprint $table) {
            $table->renameColumn('aksiya', 'aktsiya');
            $table->renameColumn('aksiya_izoh_uz', 'aktsiya_izoh_uz');
            $table->renameColumn('aksiya_izoh_ru', 'aktsiya_izoh_ru');
        });

        Schema::table('deposits', function (Blueprint $table) {
            $table->string('aktsiya')->default(0);
            $table->string('aktsiya_izoh_uz')->nullable();
            $table->string('aktsiya_izoh_ru')->nullable();
        });

        Schema::table('credits', function (Blueprint $table) {
            $table->string('aktsiya')->default(0);
            $table->string('aktsiya_izoh_uz')->nullable();
            $table->string('aktsiya_izoh_ru')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('all_tables', function (Blueprint $table) {
            Schema::table('debit_cards', function (Blueprint $table) {
                $table->renameColumn('aktsiya', 'aksiya');
                $table->renameColumn('aktsiya_izoh_uz', 'aksiya_izoh_uz');
                $table->renameColumn('aktsiya_izoh_ru', 'aksiya_izoh_ru');
            });
    
            Schema::table('deposits', function (Blueprint $table) {
                $table->dropColumn('aktsiya');
                $table->dropColumn('aktsiya_izoh_uz');
                $table->dropColumn('aktsiya_izoh_ru');
            });
    
            Schema::table('credits', function (Blueprint $table) {
                $table->dropColumn('aktsiya');
                $table->dropColumn('aktsiya_izoh_uz');
                $table->dropColumn('aktsiya_izoh_ru');
            });
        });
    }
}
