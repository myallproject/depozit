<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('banks_id');
            $table->integer('type_bank')->nullable();
            $table->string('license')->nullable();
            $table->string('phone')->nullable();
            $table->string('helpline')->nullable();
            $table->string('site')->nullable();
            $table->string('telegram_channel')->nullable();
            $table->string('mail')->nullable();
            $table->string('address')->nullable();
            $table->integer('count_office')->default(0);
            $table->integer('count_min_office')->default(0);
            $table->string('inn')->nullable();
            $table->string('svift')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_informations');
    }
}
