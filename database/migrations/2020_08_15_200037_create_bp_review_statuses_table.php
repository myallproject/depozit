<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBpReviewStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bp_review_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedinteger('review_id');
            $table->integer('readstatus')->default(0);
            //0 o'qilmagan| 1 o'qilgan
            $table->foreign('review_id')->references('id')->on('reviews');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bp_review_statuses');
    }
}
