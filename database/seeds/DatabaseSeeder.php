<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
            /*CreditIssuingFormSeeder::class,
             BankInfoSeeder::class,
             BanksSeeder::class,
             CardsProvisionSeeder::class,
             CardsSeeder::class,
             CardTypeSeeder::class,
             CreditGoalsSeeder::class,
             CreditProofInComeSeeder::class,
             CreditProvisionSeeder::class,
             CreditTypeSeeder::class,
             CurrencySeeder::class,
             PaymentSeeder::class,
             TypeBankSeeder::class,
             ServicesSeeder::class,
             DebitCardSeeder::class,
             ServiceDateSeeder::class,
             CreditCardSeeder::class*/
             Organization::class

         ]);
    }
}
