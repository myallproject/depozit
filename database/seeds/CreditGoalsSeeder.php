<?php

use Illuminate\Database\Seeder;

class CreditGoalsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credit_goals')->truncate();

        DB::table('credit_goals')->insert([
            [
                'service_id' => 1,
                'name_uz' => 'Maqsadsiz – kartaga',
                'name_ru' => 'Без цели - на карту',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 1,
                'name_uz' => 'Maqsadli',
                'name_ru' => 'Целеустремленный',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 2,
                'name_uz' => 'Maqsadsiz – kartaga',
                'name_ru' => 'Без цели - на карту',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 2,
                'name_uz' => 'Maqsadli',
                'name_ru' => 'Целеустремленный',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 3,
                'name_uz' => 'Maqsadsiz – kartaga',
                'name_ru' => 'Без цели - на карту',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 3,
                'name_uz' => 'Maqsadli',
                'name_ru' => 'Целеустремленный',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 4,
                'name_uz' => 'Maqsadsiz – kartaga',
                'name_ru' => 'Без цели - на карту',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 4,
                'name_uz' => 'Maqsadli',
                'name_ru' => 'Целеустремленный',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 5,
                'name_uz' => 'Maqsadsiz – kartaga',
                'name_ru' => 'Без цели - на карту',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 5,
                'name_uz' => 'Maqsadli',
                'name_ru' => 'Целеустремленный',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
