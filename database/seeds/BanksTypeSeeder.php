<?php

use Illuminate\Database\Seeder;

class BanksTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank_types')->truncate();
        DB::table('bank_types')->insert([
            [
                'id' => 1,
                'name_uz'    => 'Davlat banki',
                'name_ru'	 => 'Государственные банки',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'name_uz'    => 'Xususiy bank',
                'name_ru'	 => 'Частные банки',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'name_uz'    => 'Xorijiy bank',
                'name_ru'	 => 'Иностранные банки',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
