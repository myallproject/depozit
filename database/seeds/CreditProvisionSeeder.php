<?php

use Illuminate\Database\Seeder;

class CreditProvisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credit_provisions')->truncate();

        DB::table('credit_provisions')->insert([
            [
                'service_id' => 1,
                'name_uz' => 'Mol-mulk garovi',
                'name_ru' => 'Залог имущества',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 1,
                'name_uz' => 'Kafillik',
                'name_ru' => 'Поручительство',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 1,
                'name_uz' => 'Sug’urta polisi',
                'name_ru' => 'Страховой полис',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 2,
                'name_uz' => 'Mol-mulk (sotib olinayotgan avtomobil)',
                'name_ru' => 'Залог имущества(машина покупается)',
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 2,
                'name_uz' => 'Kafillik',
                'name_ru' => 'Поручительство',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 2,
                'name_uz' => 'Sug’urta polisi',
                'name_ru' => 'Страховой полис',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 3,
                'name_uz' => 'Mol-mulk garovi',
                'name_ru' => 'Залог имущества',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 3,
                'name_uz' => 'Kafillik',
                'name_ru' => 'Поручительство',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 3,
                'name_uz' => 'Sug’urta polisi',
                'name_ru' => 'Страховой полис',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 4,
                'name_uz' => 'Mol-mulk garovi',
                'name_ru' => 'Залог имущества',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 4,
                'name_uz' => 'Kafillik',
                'name_ru' => 'Поручительство',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 4,
                'name_uz' => 'Sug’urta polisi',
                'name_ru' => 'Страховой полис',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 5,
                'name_uz' => 'Mol-mulk garovi',
                'name_ru' => 'Залог имущества',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 5,
                'name_uz' => 'Kafillik',
                'name_ru' => 'Поручительство',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 5,
                'name_uz' => 'Sug’urta polisi',
                'name_ru' => 'Страховой полис',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
