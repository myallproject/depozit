<?php

use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->truncate();

        DB::table('services')->insert([
            [
                'id'=> 1,
                'name_uz' => 'Omonatlar',
                'name_ru' => 'Депозиты',
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id'=> 2,
                'name_uz' => 'Kreditlar',
                'name_ru' => 'Кредиты',
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id'=> 3,
                'name_uz' => 'Kartalar',
                'name_ru' => 'Карты',
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id'=> 4,
                'name_uz' => 'Ipoteka',
                'name_ru' => 'Ипотека',
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id'=> 5,
                'name_uz' => 'Sug\'urta',
                'name_ru' => 'Sug\'urta',
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
