<?php

use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->truncate();
        DB::table('currencies')->insert([
        	[
        		'id' => 1,
        		'name' => 'UZS',
        		'description' => 'Uzbekiston pul birligi',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
        	],
        	[
        		'id' => 2,
        		'name' => 'USD',
        		'description' => 'USA',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
        	],
        	[
        		'id' => 3,
        		'name' => 'RUB',
        		'description' => 'Russian',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
        	],
        	[
        		'id' => 4,
        		'name' => 'EUR',
        		'description' => 'Europ',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
        	],
        ]);
    }
}
