<?php

use Illuminate\Database\Seeder;

class CardsProvisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cards_provision_types')->truncate();

        DB::table('cards_provision_types')->insert([
            [
                'cards_id' => 2,
                'name_uz' => 'Ta\'minotsiz',
                'name_ru' => 'Без обслуживания',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'cards_id' => 2,
                'name_uz' => 'Kafillik',
                'name_ru' => 'Поручительство',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'cards_id' => 2,
                'name_uz' => 'Sug\'urta po\'lisi',
                'name_ru' => 'Страховой полис',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
