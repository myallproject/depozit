<?php

use Illuminate\Database\Seeder;

class CreditProofInComeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credit_proof_of_incomes')->truncate();

        DB::table('credit_proof_of_incomes')->insert([
            [
                'service_id' => 1,
                'name_uz' => 'Mol-mulk garovi',
                'name_ru' => 'Залог имущества',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 1,
                'name_uz' => 'Kafillik',
                'name_ru' => 'Поручительство',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 1,
                'name_uz' => 'Sug’urta polisi',
                'name_ru' => 'Страховой полис',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 2,
                'name_uz' => 'Mol-mulk garovi',
                'name_ru' => 'Залог имущества',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 2,
                'name_uz' => 'Kafillik',
                'name_ru' => 'Поручительство',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 2,
                'name_uz' => 'Sug’urta polisi',
                'name_ru' => 'Страховой полис',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 3,
                'name_uz' => 'Mol-mulk garovi',
                'name_ru' => 'Залог имущества',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 3,
                'name_uz' => 'Kafillik',
                'name_ru' => 'Поручительство',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 3,
                'name_uz' => 'Sug’urta polisi',
                'name_ru' => 'Страховой полис',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 4,
                'name_uz' => 'Mol-mulk garovi',
                'name_ru' => 'Залог имущества',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 4,
                'name_uz' => 'Kafillik',
                'name_ru' => 'Поручительство',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 4,
                'name_uz' => 'Sug’urta polisi',
                'name_ru' => 'Страховой полис',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 5,
                'name_uz' => 'Mol-mulk garovi',
                'name_ru' => 'Залог имущества',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 5,
                'name_uz' => 'Kafillik',
                'name_ru' => 'Поручительство',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 5,
                'name_uz' => 'Sug’urta polisi',
                'name_ru' => 'Страховой полис',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
