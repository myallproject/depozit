<?php

use Illuminate\Database\Seeder;

class DebitCardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('debit_cards')->truncate();
        DB::table('debit_cards')->insert([
            [
                'bank_id' => 10,
                'bank_type_id' => 1,
                'name_uz' => 'Visa infinite',
                'name_ru' => 'Visa infinite',
                'type_card_id' => 1,
                'currency_id' => 2,
                'payment_id' => 3,
                'date_id' => 12,
                'price' => 'Bepul',
                'status' => 1,
                'kamay_qoldiq' => '$1000 / 8.000.000 so\'m',
                'hisoblash_usuli' => '',
                'qushimcha_qulaylik_uz' => '- Konsyerj xizmati - dunyoning turli burchaklarida 24/7 rеjimida xizmatlarini ko‘rsatishda yordam bеrish (mеhmonxona bron qilish, chiptalarni, tovarlar va xizmatlarni sotib olishda yordam bеrish, uchrashuvni tashkil qilish va boshqalar).
                        - To‘liq sayohat sug‘urtasi - karta egasi va uning oila a’zolaridan 5 nafari chеt eldagi barcha turdagi xatarlar uchun 1 million AQSh dollarigacha sug‘urta qilinadi.
                        - Lounge key - butun dunyo bo‘ylab xalqaro aeroportlarda (yiliga 6 ta bеpul tashrif) 1000 ta biznеs-zalga birinchi o‘ringa kirish. Aeroportda Loungekey zonasining mavjudligini vеb-sayti orqali ko‘rishingiz mumkin.
                        - Tibbiy-huquqiy yordam - tibbiy masalalar, vizalar olish, huquqiy xizmatlarning aloqalari va boshqalar.
                        - Aeroportlarda SpeedPass imtiyozlari (yQ Meet and Assist service) – aeroportda tеzkor o‘tishning bo‘yicha shaxsiy xizmat, kеlish/kеtishda hamrohlik qilish xizmati
                        - GCAS - Global qo‘llab-quvvatlash xizmati - mijozga favqulodda vaziyatlarda qaerda bo‘lishidan qat’iy nazar maksimal darajada yordam ko‘rsatish.
                        - Xarid qilish himoyasi - Sotib olingan kundan boshlab dastlabki 90 kun mobaynida zararni, haridning yo‘q bo‘lishi yoki o‘g‘irlashini sug‘urtalash.
                        - Dunyo aeroportlarida SpeedPass xizmati (yQ Meet and Assist service).
                        - Visa Luxury Hotel Collection Collection bilan mеhmonxona broni
                        - Agoda bilan 985 mingdan ziyod mеhmonxonalarda 12% chеgirma www.agoda.com/visacemea.
                        - Avis xizmati yordamida avtomashina ijarasiga chеgirmalar.
                        - Yevropa, Afrika, Yaqin Sharq va Osiyoda Avis avtomobillari ijarasiga 35% gacha chеgirmaga ega bo‘ling.
                        - Visa dasturi hamkorlari xizmatlari uchun chеgirmalar va maxsus takliflar',
                'qushimcha_qulaylik_ru' => '- Konsyerj xizmati - dunyoning turli burchaklarida 24/7 rеjimida xizmatlarini ko‘rsatishda yordam bеrish (mеhmonxona bron qilish, chiptalarni, tovarlar va xizmatlarni sotib olishda yordam bеrish, uchrashuvni tashkil qilish va boshqalar).
                        - To‘liq sayohat sug‘urtasi - karta egasi va uning oila a’zolaridan 5 nafari chеt eldagi barcha turdagi xatarlar uchun 1 million AQSh dollarigacha sug‘urta qilinadi.
                        - Lounge key - butun dunyo bo‘ylab xalqaro aeroportlarda (yiliga 6 ta bеpul tashrif) 1000 ta biznеs-zalga birinchi o‘ringa kirish. Aeroportda Loungekey zonasining mavjudligini vеb-sayti orqali ko‘rishingiz mumkin.
                        - Tibbiy-huquqiy yordam - tibbiy masalalar, vizalar olish, huquqiy xizmatlarning aloqalari va boshqalar.
                        - Aeroportlarda SpeedPass imtiyozlari (yQ Meet and Assist service) – aeroportda tеzkor o‘tishning bo‘yicha shaxsiy xizmat, kеlish/kеtishda hamrohlik qilish xizmati
                        - GCAS - Global qo‘llab-quvvatlash xizmati - mijozga favqulodda vaziyatlarda qaerda bo‘lishidan qat’iy nazar maksimal darajada yordam ko‘rsatish.
                        - Xarid qilish himoyasi - Sotib olingan kundan boshlab dastlabki 90 kun mobaynida zararni, haridning yo‘q bo‘lishi yoki o‘g‘irlashini sug‘urtalash.
                        - Dunyo aeroportlarida SpeedPass xizmati (yQ Meet and Assist service).
                        - Visa Luxury Hotel Collection Collection bilan mеhmonxona broni
                        - Agoda bilan 985 mingdan ziyod mеhmonxonalarda 12% chеgirma www.agoda.com/visacemea.
                        - Avis xizmati yordamida avtomashina ijarasiga chеgirmalar.
                        - Yevropa, Afrika, Yaqin Sharq va Osiyoda Avis avtomobillari ijarasiga 35% gacha chеgirmaga ega bo‘ling.
                        - Visa dasturi hamkorlari xizmatlari uchun chеgirmalar va maxsus takliflar',
                'rasmiy_yuli_uz' => 'Bank bo\'linmalari orqali ',
                'rasmiy_yuli_ru' => 'Bank bo\'linmalari orqali ',
                'qushimcha_shart_uz' => '',
                'qushimcha_shart_ru' => '',
                'link' => 'https://nbu.uz/uz/physical/cards/visa-infinite-uz/',
                'bepul_xizmat' => '',
                'bepul_naqdlash' => '',
                'bepul_tulov' => '',
                'kontaktsiz_tulov' => '',
                'cash_back' => '',
                'tuluv_komissia' => '',
                'naqdlash_komissia' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'bank_id' => 10,
                'bank_type_id' => 1,
                'name_uz' => 'Visa Platinum',
                'name_ru' => 'Visa Platinum',
                'type_card_id' => 1,
                'currency_id' => 12,
                'payment_id' => 3,
                'date_id' => 12,
                'price' => '200.000 so\'m',
                'status' => 1,
                'kamay_qoldiq' => '$1000 / 8.000.000 so\'m',
                'hisoblash_usuli' => '',
                'qushimcha_qulaylik_uz' => '- VISA terminallari mavjud barcha savdo muassasalarida to’lovni amalga oshirish.
                                            - Pul mablag’larini ixtiyoriy to’lov tizimi bankomatida naqd qilib olish, shu jumladan, chet elda ham.
                                            - Internet-do’konlarda xaridlar uchun to’lovlarni amalga oshirish.
                                            - Mehmonxonada xona broni uchun yoki transport vositasi ijarasi.
                                            - Savdo tashkilotlarining xududiy va dunyo bo’ylab chegirma va boshqa takliflari.
                                            - Xaridlarni himoya qilish.
                                            - Uzaytirilgan kafolat.
                                            - Visa Luxury Hotel Program.
                                            - Aeroportlarda kelish/ketish vaqtida kutib olish/kuzatish.
                                            ',
                'qushimcha_qulaylik_ru' => '- VISA terminallari mavjud barcha savdo muassasalarida to’lovni amalga oshirish.
                                            - Pul mablag’larini ixtiyoriy to’lov tizimi bankomatida naqd qilib olish, shu jumladan, chet elda ham.
                                            - Internet-do’konlarda xaridlar uchun to’lovlarni amalga oshirish.
                                            - Mehmonxonada xona broni uchun yoki transport vositasi ijarasi.
                                            - Savdo tashkilotlarining xududiy va dunyo bo’ylab chegirma va boshqa takliflari.
                                            - Xaridlarni himoya qilish.
                                            - Uzaytirilgan kafolat.
                                            - Visa Luxury Hotel Program.
                                            - Aeroportlarda kelish/ketish vaqtida kutib olish/kuzatish.
                                            ',
                'rasmiy_yuli_uz' => 'Bank bo\'linmalari orqali ',
                'rasmiy_yuli_ru' => 'Bank bo\'linmalari orqali ',
                'qushimcha_shart_uz' => '',
                'qushimcha_shart_ru' => '',
                'link' => 'https://nbu.uz/uz/physical/cards/visa-platinum-uz/',
                'bepul_xizmat' => '',
                'bepul_naqdlash' => '',
                'bepul_tulov' => '',
                'kontaktsiz_tulov' => '',
                'cash_back' => '',
                'tuluv_komissia' => '',
                'naqdlash_komissia' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'bank_id' => 10,
                'bank_type_id' => 1,
                'name_uz' => 'Visa Gold',
                'name_ru' => 'Visa Gold',
                'type_card_id' => 4,
                'currency_id' => 12,
                'payment_id' => 3,
                'date_id' => 12,
                'price' => '200.000 so\'m',
                'status' => 1,
                'kamay_qoldiq' => '$50 / 400.000 so\'m',
                'hisoblash_usuli' => '',
                'qushimcha_qulaylik_uz' => '- VISA terminallari mavjud barcha savdo muassasalarida to’lovni amalga oshirish.
                                            - Pul mablaglarini ixtiyoriy to’lov tizimi bankomatida naqd qilib olish, shu jumladan, chet elda ham
                                            - Internet-do’konlarda xaridlar uchun to’lovlarni amalga oshirish
                                            - Chet elda mehmonxona xonasi broni uchun yoki transport vositasi ijarasi uchun ishlatish.
                                            - Mijozlarni qo’llab-quvvatlash global xizmati.
                                            - Xalqaro tibbiy va sayyohlikni qo’llab-quvvatlash.',
                'qushimcha_qulaylik_ru' => '- VISA terminallari mavjud barcha savdo muassasalarida to’lovni amalga oshirish.
                                            - Pul mablaglarini ixtiyoriy to’lov tizimi bankomatida naqd qilib olish, shu jumladan, chet elda ham
                                            - Internet-do’konlarda xaridlar uchun to’lovlarni amalga oshirish
                                            - Chet elda mehmonxona xonasi broni uchun yoki transport vositasi ijarasi uchun ishlatish.
                                            - Mijozlarni qo’llab-quvvatlash global xizmati.
                                            - Xalqaro tibbiy va sayyohlikni qo’llab-quvvatlash.',
                'rasmiy_yuli_uz' => 'Bank bo\'linmalari orqali ',
                'rasmiy_yuli_ru' => 'Bank bo\'linmalari orqali ',
                'qushimcha_shart_uz' => '',
                'qushimcha_shart_ru' => '',
                'link' => 'https://nbu.uz/uz/physical/cards/visa-gold-uz/',
                'bepul_xizmat' => '',
                'bepul_naqdlash' => '',
                'bepul_tulov' => '',
                'kontaktsiz_tulov' => '',
                'cash_back' => '',
                'tuluv_komissia' => '',
                'naqdlash_komissia' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
