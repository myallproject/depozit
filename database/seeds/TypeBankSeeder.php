<?php

use Illuminate\Database\Seeder;

class TypeBankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_banks')->truncate();
        DB::table('type_banks')->insert([
        	[
        		'id' => 1,
        		'name_uz'    => 'Davlat banki',
        		'name_ru'	 => 'Государственные банки',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
        	],
        	[
        		'id' => 2,
        		'name_uz'    => 'Xususiy bank',
        		'name_ru'	 => 'Частные банки',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
        	],
        	[
        		'id' => 3,
        		'name_uz'    => 'Xorijiy bank',
        		'name_ru'	 => 'Иностранные банки',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
        	],
        ]);
    }
}
