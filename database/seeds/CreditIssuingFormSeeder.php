<?php

use Illuminate\Database\Seeder;

class CreditIssuingFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credit_issuing_forms')->truncate();

        DB::table('credit_issuing_forms')->insert([
            [
                'service_id' => 1,
                'name_uz' => 'Mobil ilova orqali',
                'name_ru' => 'Через мобильное приложение',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 1,
                'name_uz' => 'Bank bo\'limlari orqali',
                'name_ru' => 'Через отделения банка',
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 2,
                'name_uz' => 'Mobil ilova orqali',
                'name_ru' => 'Через мобильное приложение',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 2,
                'name_uz' => 'Bank bo\'limlari orqali',
                'name_ru' => 'Через отделения банка',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 2,
                'name_uz' => 'Pul o’tkazish orqali',
                'name_ru' => 'Переводя деньги',
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 3,
                'name_uz' => 'Pul o’tkazish yo’li bilan',
                'name_ru' => 'Переводом денег',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 3,
                'name_uz' => 'Bank kartasiga',
                'name_ru' => 'Банковская карта',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 4,
                'name_uz' => 'Mobil ilova orqali',
                'name_ru' => 'Через мобильное приложение',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 4,
                'name_uz' => 'Bank bo\'limlari orqali',
                'name_ru' => 'Через отделения банка',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 5,
                'name_uz' => 'Mobil ilova orqali',
                'name_ru' => 'Через мобильное приложение',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'service_id' => 5,
                'name_uz' => 'Bank bo\'limlari orqali',
                'name_ru' => 'Через отделения банка',
                'status' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
