<?php

use Illuminate\Database\Seeder;

class CreditCardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credit_cards')->truncate();

        DB::table('credit_cards')->insert([
            [
                'bank_id' => 9,
                'bank_type_id' => 1,
                'currency_id' => 1,
                'name_uz' => 'Kredit karta',
                'name_ru' => 'Kredit karta',
                'date_id' => 42,
                'status' => 1,
                'imtoyozli_davr_uz' => '',
                'imtoyozli_davr_ru' => '',
                'percent' => '28%',
                'hisoblash_usul' => '',
                'max_sum' => 22300000,
                'max_sum_text_uz' => 'Jismoniy shaxs o\'rtacha oylik ish haqining 3 baravari miqdorida lekin  22,3 mln. so\'mgacha.',
                'max_sum_text_ru' => 'Jismoniy shaxs o\'rtacha oylik ish haqining 3 baravari miqdorida lekin  22,3 mln. so\'mgacha.',
                'kurish_muddati' => '3 kun',
                'documents_uz' => '- Qarz oluvchi pasporti yoki pasport o‘rnini bosuvchi hujjat nusxasi;
                                - Qarz oluvchining oxirgi 12 oy mobaynidagi daromadlari to‘g‘risidagi ma’lumot
                                - Ta’minotga oid tegishli hujjatlar.',
                'documents_ru' => '- Qarz oluvchi pasporti yoki pasport o‘rnini bosuvchi hujjat nusxasi;
                                - Qarz oluvchining oxirgi 12 oy mobaynidagi daromadlari to‘g‘risidagi ma’lumot
                                - Ta’minotga oid tegishli hujjatlar.',
                'taminot_turi_ids' => 23,
                'taminot_text_uz'=> '- Uchinchi shaxslar kafillgi;
                                     - Boshqa ta\'minot',
                'taminot_text_ru' => '- Uchinchi shaxslar kafillgi;
                                     - Boshqa ta\'minot',
                'rasmiy_yuli_uz' => 'Bank bo\'linmalari orqali ',
                'rasmiy_yuli_ru'=> 'Bank bo\'linmalari orqali ',
                'ajratish_uz' => 'Kredit ajratilishida qarz oluvchiga UZCARD tizimida ishlovchi yangi plastik karta taqdim etiladi.',
                'ajratish_ru' => 'Kredit ajratilishida qarz oluvchiga UZCARD tizimida ishlovchi yangi plastik karta taqdim etiladi.',
                'sundirish_uz' => 'Mijozning oylik ish haqi kartasiga mablag\'lar kirim qilinganda, “Kredit Karta”dagi qarzdorlik avtomat ravishda so\'ndiriladi va “Kredit Karta”dagi limit miqdori mutanosib ravishda oshadi. Kredit muddatining oxirgi 4 oyi davomida mijozning kreditlash limiti miqdori 25%dan kamayib boradi va oxirgi 12-oyda kredit ajratilmaydi.',
                'sundirish_ru' => 'Mijozning oylik ish haqi kartasiga mablag\'lar kirim qilinganda, “Kredit Karta”dagi qarzdorlik avtomat ravishda so\'ndiriladi va “Kredit Karta”dagi limit miqdori mutanosib ravishda oshadi. Kredit muddatining oxirgi 4 oyi davomida mijozning kreditlash limiti miqdori 25%dan kamayib boradi va oxirgi 12-oyda kredit ajratilmaydi.',
                'talablar_uz' => '18 yoshga to’lgan O’zbekiston Respublikasi fuqarolari',
                'talablar_ru' => '18 yoshga to’lgan O’zbekiston Respublikasi fuqarolari',
                'qushim_shart_uz' => '“Kredit Karta” egasi belgilangan limit doirasida istalgan savdo shaxobchasidan to\'lovlarni amalga oshiradi yoki bankomatlardan naqd pul echishi mumkin.',
                'qushim_shart_ru' => '“Kredit Karta” egasi belgilangan limit doirasida istalgan savdo shaxobchasidan to\'lovlarni amalga oshiradi yoki bankomatlardan naqd pul echishi mumkin.',
                'link' => 'https://uzpsb.uz/uz/individuals/credits/kredit-kartasi/',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'bank_id' => 11,
                'bank_type_id' => 1,
                'currency_id' => 1,
                'name_uz' => 'Kredit karta',
                'name_ru' => 'Kredit karta',
                'date_id' => 46,
                'status' => 1,
                'imtoyozli_davr_uz' => '30 kun',
                'imtoyozli_davr_ru' => '30 kun',
                'percent' => 'Birinchi 30 kun foizsiz, keyin yillik 36%',
                'hisoblash_usul' => '',
                'max_sum' => 11150000,
                'max_sum_text_uz' => 'BHM 50 barobarigacha',
                'max_sum_text_ru' => 'BHM 50 barobarigacha',
                'kurish_muddati' => '3 kun',
                'documents_uz' => '- Qarz oluvchi pasporti yoki pasport o‘rnini bosuvchi hujjat nusxasi;- Qarz oluvchining oxirgi 12 oy mobaynidagi daromadlari to‘g‘risidagi ma’lumot',
                'documents_ru' => '- Qarz oluvchi pasporti yoki pasport o‘rnini bosuvchi hujjat nusxasi;- Qarz oluvchining oxirgi 12 oy mobaynidagi daromadlari to‘g‘risidagi ma’lumot',
                'taminot_turi_ids' => null,
                'taminot_text_uz'=> 'Ta\'minotsiz',
                'taminot_text_ru' => 'Ta\'minotsiz',
                'rasmiy_yuli_uz' => 'Bank bo\'linmalari orqali ',
                'rasmiy_yuli_ru'=> 'Bank bo\'linmalari orqali ',
                'ajratish_uz' => 'Kredit bank plastik kartasi orqali taqdim etiladi.',
                'ajratish_ru' => 'Kredit bank plastik kartasi orqali taqdim etiladi.',
                'sundirish_uz' => 'Ma\'lumot mavjud emas',
                'sundirish_ru' => 'Ma\'lumot mavjud emas',
                'talablar_uz' => 'Kredit kartalar O‘zbekiston Respublikasi fuqarolariga va fuqaroligi bo‘lmagan (O‘zbekiston Respublikasida doimiy ro\'xatdan o\'tgan) doimiy ish bilan band bo\'lgan jismoniy shaxslarga beriladi. Kredit kartalar oylik ish haqi to\'liq plastik kartasi orqali oladigan mijozlarga beriladi.',
                'talablar_ru' => 'Kredit kartalar O‘zbekiston Respublikasi fuqarolariga va fuqaroligi bo‘lmagan (O‘zbekiston Respublikasida doimiy ro\'xatdan o\'tgan) doimiy ish bilan band bo\'lgan jismoniy shaxslarga beriladi. Kredit kartalar oylik ish haqi to\'liq plastik kartasi orqali oladigan mijozlarga beriladi.',
                'qushim_shart_uz' => '',
                'qushim_shart_ru' => '',
                'link' => 'http://www.xb.uz/physical/credits/view?id=6',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'bank_id' => 7,
                'bank_type_id' => 1,
                'currency_id' => 1,
                'name_uz' => 'Kredit karta',
                'name_ru' => 'Kredit karta',
                'date_id' => 43,
                'status' => 1,
                'imtoyozli_davr_uz' => '',
                'imtoyozli_davr_ru' => '',
                'percent' => '28%',
                'hisoblash_usul' => '',
                'max_sum' => '6800000',
                'max_sum_text_uz' => 'Jismoniy shaxs o\'rtacha oylik ish haqining 4 baravarigacha.',
                'max_sum_text_ru' => 'Jismoniy shaxs o\'rtacha oylik ish haqining 4 baravarigacha.',
                'kurish_muddati' => '',
                'documents_uz' => '- ariza;
                         - qarz oluvchining pasport nusxasi;
                        - qarz oluvchining ish joyidan oxirgi 6 oy davri uchun ish haqi va unga tenglashtirilgan daromadlari to‘g‘risidagi ma\'lumotnomasi;
                        - ta’minotga oid tegishli hujjatlar.',
                'documents_ru' => '- ariza;
                         - qarz oluvchining pasport nusxasi;
                        - qarz oluvchining ish joyidan oxirgi 6 oy davri uchun ish haqi va unga tenglashtirilgan daromadlari to‘g‘risidagi ma\'lumotnomasi;
                        - ta’minotga oid tegishli hujjatlar.',
                'taminot_turi_ids' => 23,
                'taminot_text_uz'=> '- uchinchi shaxs kafilligi;- kredit qaytmasligi xatarini sug‘urta qilish.',
                'taminot_text_ru' => '- uchinchi shaxs kafilligi;- kredit qaytmasligi xatarini sug‘urta qilish.',
                'rasmiy_yuli_uz' => 'Bank bo\'linmalari orqali',
                'rasmiy_yuli_ru'=> 'Bank bo\'linmalari orqali',
                'ajratish_uz' => 'Kredit bank plastik kartasi orqali taqdim etiladi.',
                'ajratish_ru' => 'Kredit bank plastik kartasi orqali taqdim etiladi.',
                'sundirish_uz' => 'Ma\'lumot mavjud emas',
                'sundirish_ru' => 'Ma\'lumot mavjud emas',
                'talablar_uz' => 'Agrobankda  milliy valyutada plastik kartasi mavjud hamda doimiy daromad manbaiga (oylik ish haqi va unga tenglashtirilgan to‘lovlar, pensiya) ega bo‘lgan O‘zbekiston Respublikasi fuqarolari.',
                'talablar_ru' => 'Agrobankda  milliy valyutada plastik kartasi mavjud hamda doimiy daromad manbaiga (oylik ish haqi va unga tenglashtirilgan to‘lovlar, pensiya) ega bo‘lgan O‘zbekiston Respublikasi fuqarolari.',
                'qushim_shart_uz' => '',
                'qushim_shart_ru' => '',
                'link' => 'https://agrobank.uz/uz/page/news_credit_card_2020',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ]/*,[
                'bank_id' => '',
                'bank_type_id' => '',
                'currency_id' => '',
                'name_uz' => '',
                'name_ru' => '',
                'date_id' => '',
                'status' => '',
                'imtoyozli_davr_uz' => '',
                'imtoyozli_davr_ru' => '',
                'percent' => '',
                'hisoblash_usul' => '',
                'max_sum' => '',
                'max_sum_text_uz' => '',
                'max_sum_text_ru' => '',
                'kurish_muddati' => '',
                'documents_uz' => '',
                'documents_ru' => '',
                'taminot_turi_ids' => '',
                'taminot_text_uz'=> '',
                'taminot_text_ru' => '',
                'rasmiy_yuli_uz' => '',
                'rasmiy_yuli_ru'=> '',
                'ajratish_uz' => '',
                'ajratish_ru' => '',
                'sundirish_uz' => '',
                'sundirish_ru' => '',
                'talablar_uz' => '',
                'talablar_ru' => '',
                'qushim_shart_uz' => '',
                'qushim_shart_ru' => '',
                'link' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ]*/
        ]);
    }
}
