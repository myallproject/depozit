<?php

use Illuminate\Database\Seeder;

class ServiceDateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('date_services')->truncate();

        DB::table('date_services')->insert([
            [
                'id' => 8,
                'name_uz' => 'Yillik',
                'name_ru' => 'Годовой',
                'service_id' => 2,
                'position' => '',
                'status' => null,
                'description' => 'Yillik kreditlar',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 9,
                'name_uz' => 'Oylik',
                'name_ru' => 'Ежемесячно',
                'service_id' => 2,
                'position' => '',
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 10,
                'name_uz' => 'Kunlik',
                'name_ru' => 'Суточный',
                'service_id' => 2,
                'position' => '',
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 11,
                'name_uz' => '1 oy',
                'name_ru' => '1 месяц',
                'service_id' => 1,
                'position' => 1,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 12,
                'name_uz' => '2 oy',
                'name_ru' => '2 месяц',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 13,
                'name_uz' => '3 oy',
                'name_ru' => '3 месяц',
                'service_id' => 1,
                'position' => 3,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 14,
                'name_uz' => '4 oy',
                'name_ru' => '4 месяц',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 15,
                'name_uz' => '5 oy',
                'name_ru' => '5 месяц',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 16,
                'name_uz' => '6 oy',
                'name_ru' => '6 месяц',
                'service_id' => 1,
                'position' => 6,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 17,
                'name_uz' => '7 oy',
                'name_ru' => '7 месяц',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 18,
                'name_uz' => '8 oy',
                'name_ru' => '8 месяц',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 19,
                'name_uz' => '10 oy',
                'name_ru' => '10 месяц',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 20,
                'name_uz' => '1 yil',
                'name_ru' => '1 год',
                'service_id' => 1,
                'position' => 11,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 21,
                'name_uz' => '1,5 yil',
                'name_ru' => '1,5 год',
                'service_id' => 1,
                'position' => 12,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 22,
                'name_uz' => '2 yil',
                'name_ru' => '2 год',
                'service_id' => 1,
                'position' => 13,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 23,
                'name_uz' => '3 yil',
                'name_ru' => '3 год',
                'service_id' => 1,
                'position' => 14,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 24,
                'name_uz' => '4 yil',
                'name_ru' => '4 год',
                'service_id' => 1,
                'position' => 15,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 25,
                'name_uz' => '5 yil',
                'name_ru' => '5 год',
                'service_id' => 1,
                'position' => 16,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 26,
                'name_uz' => '6 yil',
                'name_ru' => '6 год',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 27,
                'name_uz' => '7 yil',
                'name_ru' => '7 год',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 28,
                'name_uz' => '8 yil',
                'name_ru' => '8 год',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 29,
                'name_uz' => '9 yil',
                'name_ru' => '9 год',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 30,
                'name_uz' => '10 yil',
                'name_ru' => '10 год',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 31,
                'name_uz' => '9 oy',
                'name_ru' => '9 месяц',
                'service_id' => 1,
                'position' => 9,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 32,
                'name_uz' => 'Muddatsiz',
                'name_ru' => 'Бессрочный',
                'service_id' => 1,
                'position' => 24,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 33,
                'name_uz' => '15 yil',
                'name_ru' => '15 год',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 34,
                'name_uz' => '18 yil',
                'name_ru' => '18 год',
                'service_id' => 1,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 35,
                'name_uz' => '3 oy',
                'name_ru' => '3 месяц',
                'service_id' => 2,
                'position' => null,
                'status' => 2,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 36,
                'name_uz' => '6 oy',
                'name_ru' => '6 месяц',
                'service_id' => 2,
                'position' => null,
                'status' => 2,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 37,
                'name_uz' => '9 oy',
                'name_ru' => '9 месяц',
                'service_id' => 2,
                'position' => null,
                'status' => 2,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 38,
                'name_uz' => '1 yil',
                'name_ru' => '1 год',
                'service_id' => 2,
                'position' => null,
                'status' => 2,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 39,
                'name_uz' => '2 yil',
                'name_ru' => '2 год',
                'service_id' => 2,
                'position' => null,
                'status' => 2,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 40,
                'name_uz' => '3 yil',
                'name_ru' => '3 год',
                'service_id' => 2,
                'position' => null,
                'status' => 2,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 41,
                'name_uz' => '5 yil',
                'name_ru' => '5 год',
                'service_id' => 2,
                'position' => null,
                'status' => 2,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 42,
                'name_uz' => '1 yil',
                'name_ru' => '1 год',
                'service_id' => 3,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 43,
                'name_uz' => '2 yil',
                'name_ru' => '2 год',
                'service_id' => 3,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 44,
                'name_uz' => '3 yil',
                'name_ru' => '3 год',
                'service_id' => 3,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 45,
                'name_uz' => '4 yil',
                'name_ru' => '4 год',
                'service_id' => 3,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 46,
                'name_uz' => '5 yil',
                'name_ru' => '5 год',
                'service_id' => 3,
                'position' => 0,
                'status' => null,
                'description' => '',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
