<?php

use Illuminate\Database\Seeder;

class CardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cards')->truncate();

        DB::table('cards')->insert([
            [
                'id' => 1,
                'name_uz' => 'Debit karta',
                'name_ru' => 'Дебетовая карта',
                'slug' => 'debit-karta',
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id' => 2,
                'name_uz' => 'Kredit karta',
                'name_ru' => 'Кредитная карта',
                'slug' => 'kredit-karta',
                'status' => 1,
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
