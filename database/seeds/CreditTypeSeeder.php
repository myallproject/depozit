<?php

use Illuminate\Database\Seeder;

class CreditTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credit_types')->truncate();

     	DB::table('credit_types')->insert([
     		[
     			'id' => 1,
     			'name_uz' => 'Iste\'mol kreditlari',
     			'name_ru' => 'Потребительские кредиты',
     			'slug' => 'istemol-kreditlari-42499',
     			'text_uz' => 'Iste\'mol kredit - bu jismoniy shaxsning iste\'mol ehtiyojlarini qondirish maqsadida tovarlar va xizmatlarni harid qilish uchun taqdim etiladigan kredit.',
     			'text_ru' => 'Потребительский кредит - это кредит, предоставляемый физическим лицом для покупки товаров и услуг для удовлетворения своих потребительских потребностей.',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s')
     		],
     		[
     			'id' => 2,
     			'name_uz' => 'Avto kreditlar',
     			'name_ru' => 'Авто кредиты',
     			'slug' => 'avtokreditlar-63711',
     			'text_uz' => 'Avto mobil sotib olish uchun beriladigan kreditlar',
     			'text_ru' => 'Кредиты на покупку автомобиля',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s')
     		],
     		[
     			'id' => 3,
     			'name_uz' => 'Ipoteka kreditlari',
     			'name_ru' => 'Ипотечные кредиты',
     			'slug' => 'ipoteka-kreditlari-64597',
     			'text_uz' => 'Ko\'chmas mulk sotib olish uchun beriladigan kreditlar',
     			'text_ru' => 'Кредиты на приобретение недвижимости',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s')
     		],
     		[
     			'id' => 4,
     			'name_uz' => 'Mikro qarzlar',
     			'name_ru' => 'Микрозаймы',
     			'slug' => 'mikro-qarzlar-72982',
     			'text_uz' => 'Miqro qarzlar- katta miqdorda bo\'lmagan pul mablag\'lari',
     			'text_ru' => 'Микрозаймы - большие суммы денег',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s')
     		],
     		[
     			'id' => 5,
     			'name_uz' => 'Ta\'lim kreditlari',
     			'name_ru' => 'Образовательные кредиты',
     			'slug' => 'talim-krediti-17292',
     			'text_uz' => 'O‘zbekiston Respublikasi oliy ta’lim muassasalarining kunduzgi bo‘limlariga to‘lov-kontrakt asosida qabul qilingan O‘zbekiston Respublikasi fuqarosi bo‘lgan talabalarning o‘qishi uchun talabalarning o‘zlariga, ularning ota-onalariga yoki vasiylariga beriladi.',
     			'text_ru' => 'Студенты, являющиеся гражданами Республики Узбекистан, зачисленные на платной основе для студентов дневного отделения высших учебных заведений Республики Узбекистан, будут предоставлены студентам, их родителям или опекунам.',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s')
     		],[
     			'id' => 6,
     			'name_uz' => 'Overdraft kreditlari',
     			'name_ru' => 'Овердрафтные кредиты',
     			'slug' => 'overdraft-kreditlari-42249',
     			'text_uz' => 'Overdraft kreditlari',
     			'text_ru' => 'Овердрафтные кредиты',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s')
     		],
     	]);   
    }
}
