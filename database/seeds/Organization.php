<?php

use Illuminate\Database\Seeder;

class Organization extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    	
        DB::table('organizations')->truncate();

        DB::table('organizations')->insert([
        	[
                'id' => 1,
        		'name_uz' => 'Bank',
        		'name_ru' => 'Банк',
        	],
        	[
                'id' => 2,
        		'name_uz' => 'Boshqa tashkilotlar',
        		'name_ru' => 'Другие организации',
        	],
        ]);
    }
}
