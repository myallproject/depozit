<?php
    return [
        'admins' => [1,2,3,10],
        'ymd' => [8,9,10],
        
        'status' =>[
            '1' => 'active',
            '0' => 'block',
        ],
        'type_sum' => [
            'unlimited'  => 1,
            'limited'    => 2
        ],
        
        'yes_or_no' => [
            'no'    => 0,
            'yes'   => 1,
        ],
        
        'bank_type' => [
            'state_bank' => 1,
            'private_bank' => 2,
            'foreign_bank' => 3
        ],

        'credits' => [
            'type_id' => [
                1=>'consume',  
                2=>'auto', 
                3=>'mortgage',
                4=>'micro',   
                5=>'edu',
                6=>'overdraft',
            ],
            'all'       => 'all',
            'consume'   => 'istemol-kreditlari-42499',
            'auto'      => 'avtokreditlar-63711',
            'micro'     => 'mikro-qarzlar-72982',
            'mortgage'  => 'ipoteka-kreditlari-64597',
            'edu'       => 'talim-krediti-17292',
            'overdraft' => 'overdraft-kreditlari-42249',

            'activity' => [
                '1' => 'credit_active',
                '2' => 'credit_suspended'
            ],

            'status' =>[
                '1' => 'active',
                '0' => 'block',
            ]
        ],

        'services' =>  [
            'deposit' => 1,
            'credit'  => 2,
            'card'    => 3,
            'ipoteka'   => 4,
            'sugurta'   => 5
        ],

        'currency' => [
            '2' => 'USD',
            '3' => 'RUB',
            '4' => 'EUR',
            'USD'=> '2', 
            'RUB'=> '3', 
            'EUR'=> '4' 
        ],

        'date_type'=> [
            [
                'k' => 1,
                'name' => 'Kun',
                'name_lang' =>'day'
            ],
            [
                'k' => 2,
                'name' => 'Oy',
                'name_lang' => 'month'
            ],
            [
                'k' => 3,
                'name' => 'Yil',
                'name_lang' => 'year'
            ]
        ],

        'percent_consider' => [
        
            ['k' => 1,'name_uz' => 'Turli muddatda va turli foizda' ],
            ['k' => 2,'name_uz' => 'Differensial muddatli' ],
            ['k' => 3,'name_uz' => 'Banklar orqali ochish' ],
            ['k' => 4,'name_uz' => 'Mobil ilova orqali ochish' ],
        ],
        'percent_consider_id' => [
            'id' => [
                'different_date' => 1,
                'differensial_date' => 2,
            ],
        ],
        'pages' => [
            'home' => 1,
            'deposits' => 2,
            'deposit' => 3,
            'credit' => 4,
            'credits' => 5,
            'credit_card' => 6,
            'debit_card' => 7,
            'about_us' => 8,
            'contact' => 9,
            'search' => 10,
            'comparison' => 11,
            'cb_rate' => 12,
            'bank_rate' => 13,
            'bank' => 14,
            'banks' => 15,
            'news' => 16,
            'all_news' => 17,
            'micro_credits' => 18,
            'auto_credits' => 19,
            'consume_credits' => 20,
            'mortgage_credits' => 21,
            'edu_credits' => 22,
            'bank_services' => 23,
            'overdraft' => 24,
            'reviews' => 25,
            'bank_question' => 26,
            'ranking' => 27,
            'login' => 28,
            'register' => 29,
            'add_review' => 30,
            'service_question' => 31
        ],
        'review' => [
            'status' => [
                0 => 'review_rejected',
                1 => 'review_waiting',
                2 => 'review_accepted',
            ],
            'assessment' =>[
                0 => 0,
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => 5
            ],
            'answer_status' =>[
                1 => 'new',
                2 => 'answered',
                3 => 'checked',
                4 => 'problem_solved',
            ],
            'answer_status_color'=> [
                1 => '#0764ff29',
                2 => '#0764ff00',
                3 => '#28a745',
                4 => '#00800052',
            ],
            'assessment_text' => [45,123,5,4,3,2,1]
          
        ],
        'organization' => [
            'bank'  => 1,
            'other' => 2
        ],

        'letters_uz' => ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
        'letters_ru' => ["А", "Б", "В", "Г", "Д", 'Е', "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я"], 

        'gender' => [
            1 => 'male',
            2 => 'female'
        ]
    ];
