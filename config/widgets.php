<?php
/**
 * Created by PhpStorm.
 * User: Physics
 * Date: 27.10.2019
 * Time: 15:55
 */

return [
    'currency' => 'App\Widgets\BestCurrencyWidget',
    'credit_filter' => 'App\Widgets\CreditFilterWidget'
];