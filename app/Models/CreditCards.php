<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

use App\Models\Currency;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class CreditCards extends Model implements Searchable
{   
    use Cachable;
    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class,'bank_id');
    }

    public function name()
    {
        $name = 'name_'.App::getLocale();
        return $this->$name;
    }

    public function imtoyozli_davr(){
        $lang = 'imtoyozli_davr_'.App::getLocale();
        return $this->$lang;
    }

    public function max_sum_text(){
        $lang = 'max_sum_text_'.App::getLocale();
        return $this->$lang;
    }

    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class,'currency_id');
    }

    public function getSearchResult(): SearchResult
    {
        // TODO: Implement getSearchResult() method.

         $url = route('credit-cards',['locale'=>App::getLocale()]);
         return new SearchResult(
             $this,$this->name(),$url
         );
    }

    public function link($lang)
    {
        if($this->link){
            $array = json_decode($this->link,true);
            return $array[$lang];
        }
    }
}
