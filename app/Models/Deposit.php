<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\App;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use App\Models\DepositChildren;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Deposit extends Model implements Searchable
{
    
    use Cachable;

    protected $fillable = [
        'bank_id',
        'slug',
        'status',
        'name_uz',
        'name_ru',
        'deposit_type_id',
        'deposit_date',
        'deposit_date_type',
        'deposit_percent',
        'min_sum',
        'currency',
        'account_fill',
        'partly_take',
        'percents_capitalization',
        'percent_paid_period',
        'close_before_date'
    ];
    
    public function name() {
        $name = 'name_'.App::getLocale();
        return $this->$name;
    }
    public function text() {
        $text = 'text_'.App::getLocale();
        return $this->$text;
    }
    
    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class,'bank_id');
    }
    
    public function depositType(): BelongsTo
    {
        return $this->belongsTo(DepositType::class,'deposit_type_id');
    }
    
    public function depositDateType(): BelongsTo
    {
        return $this->belongsTo(DateService::class,'deposit_date_type');
    }
    
    public function priceCurrency(): BelongsTo {
        return $this->belongsTo(Currency::class,'currency');
    }
    
    public function a_fill(): string
    {
        if ($this->account_fill == config('global.yes_or_no.yes')) {
            return __('lang.yes');
        } elseif ($this->account_fill == config('global.yes_or_no.no')) {
            return __('lang.no');
        }
    }

    public function p_take(): string
    {
        if ($this->partly_take == config('global.yes_or_no.yes')) {
            return __('lang.yes');
        } elseif ($this->partly_take == config('global.yes_or_no.no')) {
            return __('lang.no');
        }
    }
    
    public function capitalization(): string
    {
        if ($this->percents_capitalization == config('global.yes_or_no.yes')) {
            return __('lang.yes');
        } elseif ($this->percents_capitalization == config('global.yes_or_no.no')) {
            return __('lang.no');
        }
    }

    public function other_deposits() {
        return Deposit::where('bank_id',$this->bank_id)->where('id','!=',$this->id)->where('currency',$this->currency)->count();
    }

    public function depositPercentDate(): HasMany
    {
        return $this->hasMany(DepositsDate::class,'deposit_id');
    }


    public function deposit_percent_date()
    {
        $model = DepositsDate::where('deposit_id',$this->id)->whereIn('percent_consider',[1,2,3])->get();
        return $model;
    }

    public function onMobileApp()
    {
       $model = DepositsDate::where('deposit_id',$this->id)->where('percent_consider',4)->get();
       return $model;
    }

    public function payPercentPeriod(): BelongsTo
    {
        return $this->belongsTo(DepositPayPercentPeriod::class,'percent_paid_period');
    }

    public function openType()
    {
        $ids = explode(',', $this->type_open);
        $model = [];
        foreach($ids as $k => $id){
            $model[]= TypeOpenDeposit::find($id);
        }
        return $model;
    }

    public function getSearchResult(): SearchResult
    {
        // TODO: Implement getSearchResult() method.

        $url = route('view-deposit',['locale' => App::getLocale(),'slug'=>$this->slug]);

        return new SearchResult(
            $this,
            $this->name_uz,
            $url
        );
    } 

    public function otherDate($id_date){
        
        $child = DepositChildren::where(['date_id'=>$id_date,'deposit_id'=>$this->id])->first();
        $date = '';
        $date_name = '';
        if($child){
           $d =  DateService::find($child['date_type']);
            $date_name = $child['date'].' '.$d->name();
        } else {
         
            $model = DateService::find($this->date_type_id);
            $date_name = $this->deposit_date.' '.$model->name();
        }

        return $date_name;
    } 

    public function otherPercent($id_date){
        $child = DepositChildren::where(['date_id'=>$id_date,'deposit_id'=>$this->id])->first();
        $date = '';
        $date_percent = '';
        if($child){
            $date_percent = $child->percent;
        } else {
            $date_percent = $this->deposit_percent;
        }

        return $date_percent;
    }
    
    public function otherDateType($id_date){
        $child = DepositChildren::where(['date_id'=>$id_date,'deposit_id'=>$this->id])->first();
        $date = '';
        $return = '';
        if($child){
            $return = $child->date_type;
        } else {
            $return = $this->date_type_id;
        }

        return $return;
    }

    public function otherDateTypeDate($id_date){
        $child = DepositChildren::where(['date_id'=>$id_date,'deposit_id'=>$this->id])->first();
        $date = '';
        $return = '';
        if($child){
            $return = $child->date;
        } else {
            $return = $this->deposit_date;
        }

        return $return;
    } 

    public function dateStringLang($lang)
    {
        if($this->deposit_date_string){
            $array = json_decode($this->deposit_date_string,true);
        return $array[$lang];
        }
    }
    
    public function accountFillRules($lang)
    {
        if($this->account_fill_rules){
            $array = json_decode($this->account_fill_rules,true);
            return $array[$lang];
        }
    }

    public function partlyTakeRules($lang)
    {
        if($this->partly_take_rules){
            $array = json_decode($this->partly_take_rules,true);
            return $array[$lang];
        }
    }
    
    public function percentsCapitalizationRules($lang)
    {
        if($this->percents_capitalization_rules){
            $array = json_decode($this->percents_capitalization_rules,true);
            return $array[$lang];
        }
    }

    public function privilegeTakeText($lang)
    {
        if($this->privilege_take_text){
            $array = json_decode($this->privilege_take_text,true);
            return $array[$lang];
        }
    }
    
    public function openTypeText($lang)
    {
        if($this->open_type_text){
            $array = json_decode($this->open_type_text,true);
            return $array[$lang];
        }
    }

    public function link()
    {
        $lang = App::getLocale();
        if($this->link){
            $array = json_decode($this->link,true);
            return $array[$lang];
        }
    }


    public function dateMonth($date_type)
    {
        $type = $this->otherDateType($date_type);

        $date = $this->otherDateTypeDate($date_type);
        $result = '';
        if($type == 8){
            $result = $date * 12;
        } 
        else if($type == 9){
            $result = $date;
        }
        else if($type == 10){
            $result = $date / 30;
        }

        return $result;
    }

    public function aktsiya_link($lang)
    {
        if($this->aktsiya_link){
            $array = json_decode($this->aktsiya_link,true);
            return $array[$lang];
        }
    }

    public function aktsiya_izoh($lang)
    {
        if($this->aktsiya_izoh){
            $array = json_decode($this->aktsiya_izoh,true);
            return $array[$lang];
        }
    }

}
