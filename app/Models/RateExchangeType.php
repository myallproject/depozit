<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class RateExchangeType extends Model
{
	//use Cachable;
	
    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class,'bank_id');
    }

    public function name()
    {
        $name = 'name_'.App::getLocale();
        return $this->$name;
    }
}
