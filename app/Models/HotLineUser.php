<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\User;

class HotLineUser extends Model
{
    protected $fillable = ['org_id', 'user_id', 'status'];

    protected $dates = [
        'updated_at',
        'created_at'
    ];
    protected $casts = [
    	'id' => 'integer',
    	'user_id' => 'integer',
    	'status' => 'integer'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
