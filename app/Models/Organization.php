<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use App;

class Organization extends Model
{
    public function name($lang=false)
    {
    	if(!$lang){
    		$lang = App::getLocale();
    	}
    	$name = 'name_'.$lang;
    	return $this->$name;
    }
}
