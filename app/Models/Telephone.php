<?php

namespace App\Models;

use App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Throwable;

class Telephone extends Model implements ToModel, WithHeadingRow, SkipsOnError
{
    use Importable,SkipsErrors;
    protected $guarded = ['id'];
    public function brand() : BelongsTo {
        return $this->belongsTo(Brand::class, 'brand_id', 'id');
    }

    public function telephone_urls() : HasMany {
        return $this->hasMany(TelephoneUrl::class, 'telephone_id', 'id');
    }

    public function model(array $row) {
        if($row['smartphone_brand'] === null) {
            $brand = Brand::where('name', 'like', 'Brendsiz')->first();
        } else $brand = Brand::where('name', 'like', $row['smartphone_brand'])->first();

        if($brand === null) {
            $brand = new Brand();
            if($row['smartphone_brand'] === null) {
                $brand->name = 'Brendsiz';
                $brand->save();
                $brand = Brand::where('name', 'like', 'Brendsiz')->first();
            }               
            else {
                $brand->name = $row['smartphone_brand'];
                $brand->save();
                $brand = Brand::where('name', 'like', $row['smartphone_brand'])->first();
            }                
        }
        return new Telephone([
            'smartphone_model'  => $row['smartphone_model'],
            'smartphone_type'   => $row['smartphone_type'],
            'smartphone_body_type'  =>  $row['smartphone_body_type'],
            'smartphone_body_material'  =>  $row['smartphone_body_material'],
            'smartphone_sim_number'  =>  $row['smartphone_sim_number'],
            'smartphone_sim_mode'  =>  $row['smartphone_sim_mode'],
            'smartphone_weight'  =>  $row['smartphone_weight'],
            'smartphone_dimension'  =>  $row['smartphone_dimension'],
            'smartphone_display_type'  =>  $row['smartphone_display_type'],
            'smartphone_diagonal'  =>  $row['smartphone_diagonal'],
            'smartphone_image_size'  =>  $row['smartphone_image_size'],
            'smartphone_pixels'  =>  $row['smartphone_pixels'],
            'smartphone_audio'  =>  $row['smartphone_audio'],
            'smartphone_earphone'  =>  $row['smartphone_earphone'],
            'smartphone_standard'  =>  $row['smartphone_standard'],
            'smartphone_interface'  =>  $row['smartphone_interface'],
            'smartphone_inner_memory'  =>  $row['smartphone_inner_memory'],
            'smartphone_operation_memory'  =>  $row['smartphone_operation_memory'],
            'smartphone_memory_card'  =>  $row['smartphone_memory_card'],
            'smartphone_battery_type'  =>  $row['smartphone_battery_type'],
            'smartphone_battery_volume'  =>  $row['smartphone_battery_volume'],
            'smartphone_battery_lifetime_call'  =>  $row['smartphone_battery_lifetime_call'],
            'smartphone_battery_lifetime_sleep'  =>  $row['smartphone_battery_lifetime_sleep'],
            'smartphone_other'  =>  $row['smartphone_other'],
            'smartphone_internet_connection'  =>  $row['smartphone_internet_connection'],
            'smartphone_os'  =>  $row['smartphone_os'],
            'smartphone_os_beginning'  =>  $row['smartphone_os_beginning'],
            'smartphone_body_construction'  =>  $row['smartphone_body_construction'],
            'smartphone_contactless_payment'  =>  $row['smartphone_contactless_payment'],
            'smartphone_sensor_type'  =>  $row['smartphone_sensor_type'],
            'smartphone_proportion'  =>  $row['smartphone_proportion'],
            'smartphone_main_camera_number'  =>  $row['smartphone_main_camera_number'],
            'smartphone_main_camera_resolution'  =>  $row['smartphone_main_camera_resolution'],
            'smartphone_main_camera_diafragma'  =>  $row['smartphone_main_camera_diafragma'],
            'smartphone_flashlight'  =>  $row['smartphone_flashlight'],
            'smartphone_main_camera_functions'  =>  $row['smartphone_main_camera_functions'],
            'smartphone_video_recording'  =>  $row['smartphone_video_recording'],
            'smartphone_front_camera'  =>  $row['smartphone_front_camera'],
            'smartphone_processor'  =>  $row['smartphone_processor'],
            'smartphone_core'  =>  $row['smartphone_core'],
            'smartphone_video_processor'  =>  $row['smartphone_video_processor'],
            'smartphone_wireless_charging'  =>  $row['smartphone_wireless_charging'],
            'smartphone_fast_charging'  =>  $row['smartphone_fast_charging'],
            'smartphone_loud_speaker'  =>  $row['smartphone_loud_speaker'],
            'smartphone_managing'  =>  $row['smartphone_managing'],
            'smartphone_detectors'  =>  $row['smartphone_detectors'],
            'smartphone_video_resolution'  =>  $row['smartphone_video_resolution'],
            'smartphone_video_shots'  =>  $row['smartphone_video_shots'],
            'smartphone_lte'  =>  $row['smartphone_lte'],
            'smartphone_battery'  =>  $row['smartphone_battery'],
            'smartphone_battery_lifetime_music'  =>  $row['smartphone_battery_lifetime_music'],
            'smartphone_glass'  =>  $row['smartphone_glass'],
            'smartphone_sales_beginning'  =>  $row['smartphone_sales_beginning'],
            'smartphone_display_strength'  =>  $row['smartphone_display_strength'],
            'smartphone_geo_tagging'  =>  $row['smartphone_geo_tagging'],
            'smartphone_navigation'  =>  $row['smartphone_navigation'],
            'smartphone_gps'  =>  $row['smartphone_gps'],
            'smartphone_profile'  =>  $row['smartphone_profile'],
            'brand_id'  =>  $brand->id
        ]);
    }


}
