<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\App;

use App\Models\CreditChildrenDate;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Credit extends Model implements Searchable
{
    use Cachable;
    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id');
    }

    public function creditDate()
    {
        return $this->belongsTo(DateService::class, 'type_date');
    }

    public function currencyName()
    {
        return $this->belongsTo(Currency::class, 'currency');
    }

    public function reviewPeriod()
    {
        return $this->belongsTo(CreditReviewPeriod::class, 'credit_review_period_id');
    }

    public function name()
    {
        $name = 'name_' . App::getLocale();
        return $this->$name;
    }

    public function privilege()
    {
        $name = 'date_privilege_' . App::getLocale();
        return $this->$name;
    }

    public  function textAmount()
    {
        $name = 'text_amount_' . App::getLocale();
        return $this->$name;
    }

    public function compensation()
    {
        $name = 'first_compensation_' . App::getLocale();
        return $this->$name;
    }

    public function creditBoshBadal(): BelongsTo
    {
        return $this->belongsTo(CreditBoshBadal::class, 'compensation_status');
    }

    public function typeProvision()
    {
        $name = 'type_provision_' . App::getLocale();
        return $this->$name;
    }


    public function goal()
    {
        $goal = 'goal_credit_' . App::getLocale();

        $column = $this->$goal;
        $arr = [];
        if ($column) {
            $arr = explode(';', $column);
        }
        return $arr;
    }

    public function creditGoal(): BelongsTo
    {
        return $this->belongsTo(CreditGoal::class, 'credit_goal_id');
    }

    public function creditGoalString()
    {
        $string = '';
        if ($this->credit_goal_id) {
            $ids = explode(',', $this->credit_goal_id);
            foreach ($ids as $k => $v) {
                $goal = CreditGoal::find($v);
                $string .= ', ' . $goal->name();
            }
        }

        return substr($string, 1);
    }

    public function legitimation($lang)
    {
        if ($this->rasmiylashtirish)
            $array = json_decode($this->rasmiylashtirish, true);
        return $array[$lang];
    }

    public function documents()
    {
        $langName = 'documents_' . App::getLocale();
        return $this->$langName;
    }

    public function additionRules()
    {
        $langName = 'addition_rules_' . App::getLocale();
        return $this->$langName;
    }

    public function parent(): HasOne
    {
        return $this->hasOne(CreditType::class, 'id', 'type_credit_id');
    }

    public function provision(): BelongsTo
    {
        return $this->belongsTo(CreditProvision::class, 'provision_id');
    }

    public function provisionName()
    {
        $ids  = explode(',', $this->provision_id);
        $name = '';
        $d = [];
        $br = "<br>";
        foreach ($ids as $key) {
            $p = CreditProvision::find($key);
            $name .= ',' . $br . $p->name();
        }

        return substr($name, 5);
    }

    public function citizenship(): BelongsTo
    {
        return $this->belongsTo(CreditBorrowerCategory::class, 'credit_borrower_category_id');
    }

    public function getSearchResult(): SearchResult
    {
        // TODO: Implement getSearchResult() method.

        $url = route('view-credit', ['locale' => App::getLocale(), 'slug' => $this->slug]);

        return new SearchResult(
            $this,
            $this->name_uz,
            $url
        );
    }

    public function otherCreditDatePercent($date, $compensation = false)
    {

        $datename = [
            'date' => '',
            'percent' => ''
        ];


        $other = CreditChildrenDate::where('credit_id', $this->id);
        if ($date) {
            $other = $other->where('date_id', $date);
        }
        if ($compensation) {
            $other = $other->where('compensation_id', $compensation);
        }

        $other = $other->first();
        if ($other) {
            $datename = [
                'date' => $other->datename(),
                'percent' => $other->percent,
                'compensation' => $other->compensation_id > 0 ?  $other->compensation->name() : '--',
            ];
        } else {
            $c = '';
            if (isset($this->creditBoshBadal)) $c = $this->creditBoshBadal->name();
            else $c = "--";
            $datename = [
                'date' => $this->date_credit . " " . $this->creditDate->name(),
                'percent' => $this->percent,
                'compensation' => $c,
            ];
        }

        return $datename;
    }

    // public function 

    public function otherCreditList()
    {

        $other = CreditChildrenDate::where('credit_id', $this->id)->orderBy('date')->get();

        if (count($other) > 0) {
            return $other;
        } else {
            return false;
        }
    }

    public function otherCreditUniqueSumma($amount, $singlePrint = false) {
        if($singlePrint) {            
            if($this->type_date == 8) {//yil
                $date = (float)$this->date_credit * 12;
            } else if($this->type_date == 9) {//oy
                $date = (float)$this->date_credit;
            } else {//kun
                $date = (float) $this->date_credit / 30;
            }
            $c = 0;
            if (isset($this->creditBoshBadal)) $c = $this->creditBoshBadal->name();
            $total = ($amount * (100 - (float) $c)) / 100;
            $per_month = $total / (float)$date + $total * ((float) $this->percent) / 1200;
            $total = number_format($total);
            $per_month = number_format($per_month);
            $total = $total . ' ' . $this->currencyName->name();
            $per_month = $per_month . ' '.$this->currencyName->name();
        } else {
            $compensation = CreditChildrenDate::where('credit_id', $this->id)->orderBy('percent', 'desc')->first();
            $compensation_min = CreditChildrenDate::where('credit_id', $this->id)->orderBy('percent', 'asc')->first();
            $percent = $compensation;
            $percent_min = $compensation_min;
            $date = CreditChildrenDate::where('credit_id', $this->id)->orderBy('date', 'desc')->first();
            $date_min = CreditChildrenDate::where('credit_id', $this->id)->orderBy('date', 'asc')->first();
            if($date->date_type == 8) {//yil
                $date = (float)$date->date * 12;
            } else if($date->date_type == 9) {//oy
                $date = (float)$date->date;
            } else {//kun
                $date = (float) $date->date / 30;
            }

            if($date_min->date_type == 8) {//yil
                $date_min = (float)$date_min->date * 12;
            } else if($date_min->date_type == 9) {//oy
                $date_min = (float)$date_min->date;
            } else {//kun
                $date_min = (float) $date_min->date / 30;
            }

            if(isset($compensation->compensation)) $compensation = $compensation->compensation->name();
                else $compensation = "--";
            if(isset($compensation_min->compensation)) $compensation_min = $compensation_min->compensation->name();
                else $compensation_min = "--";
            $total = 0;
            $per_month = 0;
            if(is_numeric($compensation[0])) {
                $total = (float)$amount * (100 - (float)$compensation) / 100;                          
                $total_min = (float)$amount * (100 - (float)$compensation_min) / 100; 
                $total_out = max($total, $total_min);
                $total = min($total, $total_min); 
                $per_total =(float) $total / (float)$date +  $total * ((float) $percent->percent) / 1200;          
                $per_total_min = $total_min / (float)$date_min +  $total * ((float) $percent_min->percent) / 1200; 
                       
                $per_month = min($per_total, $per_total_min);
                $total = number_format($total_out);
                $per_month = number_format($per_month);
                if(App::isLocale('uz')) {
                    $total = $total . ' ' . $this->currencyName->name() . 'gacha';
                    $per_month = $per_month . ' ' . $this->currencyName->name() . 'dan';
                } else {
                    $total = 'До ' . ' ' .$total . ' '.$this->currencyName->name();
                    $per_month = 'От ' . ' ' .$per_month . ' '. $this->currencyName->name();
                }
            } else {
                $per_month = $amount / (float) $date + $amount * ((float) $this->percent) / 1200;
                $total = number_format($amount);
                $per_month = number_format($per_month);
                $total = $total . ' ' . $this->currencyName->name();
                $per_month = $per_month . ' '.$this->currencyName->name();
            }
        }
        return [
            'total' => $total,
            'per_month' => $per_month
        ];
    }

    public function otherCreditUnique($amount = 0)
    {
        $datename = 'date_name_' . App::getLocale();
        $date = CreditChildrenDate::where('credit_id', $this->id)->orderBy('date', 'desc')->first()->$datename;
        $percent = CreditChildrenDate::where('credit_id', $this->id)->orderBy('percent', 'asc')->first()->percent;
        $compensation = CreditChildrenDate::where('credit_id', $this->id)->orderBy('percent', 'desc')->first();
        if (isset($compensation->compensation)) $compensation = $compensation->compensation->name();
        else $compensation = "--";
        if($amount == 0) {
            if (is_numeric($compensation[0])) {
                if (App::isLocale('uz')) {
                    $compensation = $compensation . " dan";
                } else {
                    $compensation = "От " . $compensation;
                }
            }
        } else {
            if (is_numeric($compensation[0])) {
                $numeric_compensation = floor((float)$compensation * $amount / 100);
                $compensation = number_format($numeric_compensation);
                if (App::isLocale('uz')) {
                    $compensation = $compensation .' '.$this->currencyName->name() . "dan";
                } else {
                    $compensation = "От " . $compensation .' '.$this->currencyName->name();
                }
            }
        }
       
        if (App::isLocale('uz')) {
            $percent = $percent . "%* dan";
        } else {
            $percent = "От " . $percent . "%*";
        }

        return [
            'date' => $date,
            'percent' => $percent,
            'compensation' => $compensation
        ];
    }

    public function link($lang)
    {
        if ($this->link) {
            $array = json_decode($this->link, true);
            return $array[$lang];
        }
    }
    public function credit_borrower_possible() {
        $var = "credit_borrower_possible_" . App::getLocale();
        return $this->$var;
    }
    public function credit_borrower_additional() {
        $var = "credit_borrower_additional_" . App::getLocale();
        return $this->$var;
    }
    
    public function aktsiya_link($lang)
    {
        if($this->aktsiya_link){
            $array = json_decode($this->aktsiya_link,true);
            return $array[$lang];
        }
    }

    public function aktsiya_izoh($lang)
    {
        if($this->aktsiya_izoh){
            $array = json_decode($this->aktsiya_izoh,true);
            return $array[$lang];
        }
    }
    public function type_of_output($case, $comdate) {
        if($case == 1) {
            $credit_children = CreditChildrenDate::where('credit_id', $this->id)->where('compensation_id', $comdate)->count();
            if($credit_children > 1) return true;
        } else if($case == 2) {
            $credit_children = CreditChildrenDate::where('credit_id', $this->id)->where('date_id', $comdate)->count();
            if($credit_children > 1) return true;
        }
        return false;
    }
}
