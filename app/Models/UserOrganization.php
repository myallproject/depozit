<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\User;
use App;
use App\Models\Bank;


class UserOrganization extends Model
{
	// public function banker(): HasOne
    // {
    //     return $this->hasOne(Bank::class,'id','child_org_id');
    // }
    public function position($lang=false){
    	if(!$lang){
    		$lang = App::getLocale();
    	}
    	$jsonR = ['uz','ru'];
    	if($this->position){
    		$jsonR = json_decode($this->position,true);
    	}

    	return $jsonR[$lang];
    }
    
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function organisation():BelongsTo
    {
        return $this->belongsTo(Bank::class,'child_org_id','id');
    }

}
