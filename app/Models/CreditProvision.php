<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class CreditProvision extends Model
{
	//use Cachable;
    public function serviceType(): BelongsTo
    {
        return $this->belongsTo(CreditType::class,'service_id');
    }

     public function name() {
        $name = 'name_'.App::getLocale();
        return $this->$name;
    }
}
