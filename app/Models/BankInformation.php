<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\App;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class BankInformation extends Model
{
    //use Cachable;
    protected $cacheCooldownSeconds = 300;
     
    protected $table ='bank_informations';

    public function type(): BelongsTo
    {
        return $this->belongsTo(BankType::class,'type_bank','id');
    }

    public function ownership(): BelongsTo
    {
        return $this->belongsTo(TypeBanks::class,'type_bank');
    }

    public function bank(): BelongsTo
    {
    	return $this->belongsTo(Bank::class,'banks_id');
    }

    public function licenseReplace(){
        $replace = ['Лицензия','ЦБ','РУз'];
        $search = ['MB','litsenziyasi','raqami'];
        $array = [
           'ru' => str_replace($search,$replace,$this->license),
           'uz' => $this->license
        ];
        $key = App::getLocale();
        $result = $array[$key];
        return $result;
    }

    public function name(){
        $attr = 'official_name_'.App::getLocale();
        return $this->$attr;
    }

    public function address(){
        $attr = 'address_'.App::getLocale();
        return $this->$attr;
    }

    public function site(){
        $attr = 'site_link_'.App::getLocale();
        return $this->$attr;
    }

    public function chairman(){
        $attr = 'chairman_'.App::getLocale();
        return $this->$attr;
    }

    public function stockholders(){
        $attr = 'stockholders_'.App::getLocale();
        return $this->$attr;
    }

    public function telegram_link(){
        return str_replace('@', 'https://t.me/', $this->telegram_channel);
    }
}
