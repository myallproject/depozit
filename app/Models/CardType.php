<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class CardType extends Model
{
	//use Cachable;
    protected $table = 'debit_card_types';

    public function name()
    {
        $name = 'name_'.App::getLocale();
        return $this->$name;
    }
}
