<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class Visitors extends Model
{
	//use Cachable;
    protected $fillable = ['id','ip'];

    protected $dates = ['updated_at','created_at'];

    protected $casts = [
        'id' => 'integer',
        'ip' => 'string'
    ];
}
