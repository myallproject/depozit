<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class DataUpdateTime extends Model
{
	//use Cachable;
    public function service(): BelongsTo
    {
        return $this->belongsTo(Service::class,'service_id');
    }
}
