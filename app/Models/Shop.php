<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\App;

class Shop extends Model
{
    protected $guarded =['id'];
    public function name() {
        $name ='name_'.App::getLocale();
        return $this->$name;
    }

    public function shop_telephones(): HasMany {
        return $this->hasMany(ShopTelephone::class, 'shop_id', 'id');
    }

    public function telefon_a_p_i_ss(): HasMany {
        return $this->hasMany(TelefonAPI::class, 'shop_id', 'id');
    }
}
