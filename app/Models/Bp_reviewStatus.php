<?php

namespace App\Models;

use App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Bp_reviewStatus extends Model
{
    protected $fillable = [
        'review_id','readstatus'
    ];
    public function name() {
        if($this->readstatus) {
            return __('lang.yesread');
        } else {
            return __('lang.noread');
        }
    }
    public function review(): BelongsTo {
        return $this->belongsTo(Review::class, 'review_id','id');
    }
}
