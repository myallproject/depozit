<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class Currency extends Model
{
	//use Cachable;
	protected $fillable = [
		'name', 'description', 'status',
	];

	protected $dates = [
		'created_at','updated_at'
	];

	public function name(): string
	{
		if($this->name === 'UZS'){
			if(App::isLocale('uz')){
				$name = 'so\'m';
			} elseif(App::isLocale('ru')){
				$name = 'сум';
			}
		} else {
			$name = $this->name;
		}

		return $name;
	}

}
