<?php

namespace App\Models;

use App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TelephoneUrl extends Model
{
    public function telephone() :BelongsTo {
        return $this->belongsTo(Telephone::class, 'telephone_id', 'id');
    }
}
