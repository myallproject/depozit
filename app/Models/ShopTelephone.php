<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ShopTelephone extends Model
{
    protected $guarded = ['id'];

    public function shop(): BelongsTo {
        return $this->belongsTo(Shop::class, 'shop_id', 'id');
    }
    public function telephone(): BelongsTo {
        return $this->belongsTo(Telephone::class, 'telephone_id', 'id');
    }
    public function shop_terms(): HasMany {
        return $this->hasMany(ShopTerm::class, 'shop_telephone_id', 'id');
    }
    public function brand(): BelongsTo {
        return $this->belongsTo(Brand::class, 'brand_id','id');
    }
}
