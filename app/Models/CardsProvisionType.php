<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class CardsProvisionType extends Model
{
	//use Cachable;
	
    public function name()
    {
        $name = 'name_'.App::getLocale();
        return $this->$name;
    }
}
