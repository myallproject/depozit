<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockBuyRequest extends Model
{
    protected $guarded = [];
}
