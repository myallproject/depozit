<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class Service extends Model
{
    //use Cachable;
    protected $fillable = [
      'id','parent_id', 'name_uz', 'name_ru', 'status'
    ];
    
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    
    protected $casts = [
        'id' => 'integer',
        'parent_id' => 'integer',
        'name_uz' => 'string',
        'name_ru' => 'string',
        'status' => 'integer'
    ];
    
    public function parent(): BelongsTo
    {
        return $this->belongsTo($this,'parent_id');
    }
    
    public function getName() {
        $name = 'name_'.App::getLocale();
        return $this->$name;
    }

    public function children()
    {
        return Service::where('parent_id',$this->id)->where('status',1)->orderBy('name_'.App::getLocale())->get();
    }

   
}



