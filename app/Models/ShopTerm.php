<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ShopTerm extends Model
{
    //
    protected $guarded = ['id'];

    public function shop_telephone(): BelongsTo {
        return $this->belongsTo(ShopTelephone::class, 'shop_telephone_id', 'id');
    }
}
