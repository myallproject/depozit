<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class DepositChildren extends Model
{
	//use Cachable;
    public function dateType(): BelongsTo
    {
    	return $this->belongsTo(DateService::class,'date_id');
    }
}
