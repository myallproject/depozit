<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class Comment extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public static function commentsTree($review, $response = null)
    {   

        if(is_null($response)) {
            $arr = self::where('review_id', $review->id)
            ->with('user')->whereStatus(1)->orderBy('created_at')->get();
        } else {
            $arr = self::where('review_id', $review->id)->where('id','!=',$response->id)
            ->with('user')->whereStatus(1)->orderBy('created_at')->get();
        }

        return self::buildTree($arr, 0);
    }

    // Сама функция рекурсии
    public static function buildTree($arr, $pid = 0) {
        // Находим всех детей раздела
        $found = $arr->filter(function($item) use ($pid){
            return $item->parent_id == $pid;
        });

        // Каждому детю запускаем поиск его детей
        foreach ($found as $key => $cat) {
            $sub = self::buildTree($arr, $cat->id);
            $cat->sub = $sub;
        }

        return $found;
    }
}
