<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class DepositType extends Model
{
    //use Cachable;
    protected $fillable = [
        'name_uz', 'name_ru', 'description_uz', 'description_ru'
    ];
    
    protected  $dates = [
        'created_at',
        'updated_at'
    ];
    
    protected $casts = [
        'id' => 'integer',
        'name_uz' => 'string',
        'name_ru' =>  'string',
        'description_uz' => 'string',
        'description_ru' => 'string'
    ];
    
    public function id(): int
    {
        return $this->id;
    }
   
    public function name()
    {
        if(App::getLocale() == 'uz' ) {
            return $this->name_uz;
        } elseif(App::getLocale() == 'ru'){
            return $this->name_ru;
        }
    }
    
    public function description()
    {
        if(App::getLocale() == 'uz' ){
            return $this->description_uz;
        } elseif(App::getLocale() == 'ru'){
            return $this->description_ru;
        }
    }
    
}
