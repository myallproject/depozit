<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Cache;
use App;

define('BANKS', Config::get('global.organization.bank'));

class Ranking extends Model
{
    public function rankedBanks($date='', $service_id=0){
        $banks = Bank::where('organization_id',BANKS)->get();
        $rankedBanks=[];
        foreach($banks as $bank){
            $temp=[
                'bank' => $bank,
                'score' => $bank->ranking($date, $service_id)
            ];
            array_push($rankedBanks,$temp);
        }
        usort($rankedBanks, function($a,$b){
            return ($a['score']>=$b['score']) ? -1 : 1;
        });

        return $rankedBanks;
    }

    public function topServices(){
        $services = Service::where('status','=',1)->get();
        $top_services = [];
        foreach($services as $service){
            $sorted = $this->rankedBanks('',$service->id);
            $temp = [
                'bank_name' => $sorted[0]['bank']->name(),
                'bank_slug' => $sorted[0]['bank']->slug,
                'service_name' => $service->getName(),
                'mark' => $sorted[0]['bank']->formatedAvgMark('', $service->id)
            ];
            array_push($top_services,$temp);
        }
        usort($top_services, function($a,$b){
            return ($a['mark']>=$b['mark']) ? -1 : 1;
        });
        return $top_services;
    }

    public function cacheBankPositions(){
        $rankedBanks = $this->rankedBanks();
        $position = 1;
        foreach($rankedBanks as $bank){
            $key = 'bank_'.$bank['bank']->id.'_position';
            Cache::put($key, $position, 3600);
            $position++;
        }
    }

    public function rankBanksByAvg(){
        $banks = Bank::where('organization_id',BANKS)->get();
        $rankedBanks=[];
        foreach($banks as $bank){
            $temp=[
                'bank' => $bank,
                'avg' => $bank->avgMark()
            ];
            array_push($rankedBanks,$temp);
        }
        usort($rankedBanks, function($a,$b){
            return ($a['avg']>=$b['avg']) ? -1 : 1;
        });

        return $rankedBanks;
    }

    public function searchBanks(string $bank_name){
        if(App::getLocale()=='uz'){
            $banks = Bank::where('organization_id',BANKS)->where('name_uz','LIKE','%'.$bank_name.'%')->get();
        }else if(App::getLocale()=='ru'){
            $banks = Bank::where('organization_id',BANKS)->where('name_ru','LIKE','%'.$bank_name.'%')->get();
        }
        $rankedBanks=[];
        foreach($banks as $bank){
            $temp=[
                'bank' => $bank,
                'score' => $bank->ranking()
            ];
            array_push($rankedBanks,$temp);
        }
        usort($rankedBanks, function($a,$b){
            return ($a['score']>=$b['score']) ? -1 : 1;
        });

        return $rankedBanks;
    }
}
