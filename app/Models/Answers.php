<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use App\User;

class Answers extends Model
{
    //use Cachable;
	protected $fillable = ['question_id', 'user_id', 'answer_text', 'status'];

    protected $dates = [
        'updated_at',
        'created_at'
    ];
    protected $casts = [
    	'id' => 'integer',
    	'user_id' => 'integer',
    	'question_id' => 'integer',
    	'answer_text' => 'string',
    	'status' => 'integer'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
