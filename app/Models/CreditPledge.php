<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class CreditPledge extends Model
{
	//use Cachable;
    public function serviceType(): BelongsTo
    {
        return $this->belongsTo(CreditType::class,'service_id');
    }
}
