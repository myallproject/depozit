<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class OtherExchangeRateBank extends Model
{
	//use Cachable;
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class,'currency_id');
    }

    public function typeExchange(): BelongsTo
    {
        return $this->belongsTo(RateExchangeType::class,'type_id');
    }
}
