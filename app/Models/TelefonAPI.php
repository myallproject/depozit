<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TelefonAPI extends Model
{
    //
    protected $guarded = ['id'];
    public function organization():BelongsTo
    {
    	return $this->belongsTo(Shop::class,'shop_id');
    }
    public function api() {
        $project_token = $this->project_token;
        $api_key = $this->api_key;  
        $api = "https://www.parsehub.com/api/v2/projects/$project_token/last_ready_run/data?api_key=$api_key";
        return $api;
    }
}
