<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\User;
use Illuminate\Support\Facades\App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class News extends Model
{
    //use Cachable;
    public function user(): BelongsTo
    {
    	return $this->belongsTo(User::class,'user_id');
    }

    public function title(){
    	$name = 'title_'.App::getLocale();
    	return $this->$name;
    }

    public function text(){
    	$name = 'text_'.App::getLocale();
    	return $this->$name;
    }

    public function link(){
    	$name = 'link_'.App::getLocale();
    	return $this->$name;
    }

    public function getKeywords()
    {
        $name = 'keywords_'.App::getLocale();
        return $this->$name;
    }
    
    public function getDescription()
    {
        $name = 'description_'.App::getLocale();
        return $this->$name;
    }

    public function categories()
    {   
        $cats = [];
        if($this->new_category_id){
            $ids = explode(',', $this->new_category_id);
            foreach($ids as $k => $v){
                 $cats[] = NewsCategory::find($v);
            }
        }
         return $cats;
    }
}
