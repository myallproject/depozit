<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ExchangeRate extends Model
{

    use Cachable;

	protected $fillable = [
		'take', 'sale', 'bank_id','currency'
	];

    public function bank(): BelongsTO
    {
        return $this->belongsTo(Bank::class,'bank_id');
    }
    
    public function currencyName(): BelongsTo
    {
        return $this->belongsTo(Currency::class,'currency');
    }

    public function calc($action,$quantum){
        $result = (float)$this->$action * (float)$quantum;
        return $result;
    }

    public function otherRate(): HasMany
    {
        return $this->hasMany(OtherExchangeRateBank::class,'currency_id','currency');
    }

    public function otherRates()
    {
        $other  = OtherExchangeRateBank::where('bank_id',$this->bank_id)->where('currency_id',$this->currency)->get();
        return $other;
    }
}
