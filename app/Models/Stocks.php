<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\StockChanges;
use Carbon\Carbon;

class Stocks extends Model
{
    protected $fillable = [
        'name',
        'branch_id',
        'dividend',
        'foreign',
        'ordinary_stock_price',
        'preferred_stock_price'
    ];

    public function  getPercentageChange($oldNumber, $newNumber)
    {
        $decreaseValue = $oldNumber - $newNumber;
        return ($decreaseValue / $oldNumber) * 100;
    }

    public function getDailyChangeAttribute()
    {
        $yesterday = StockChanges::where('stock_id', $this->attributes['id'])
            ->where('created_at', Carbon::yesterday())
            ->first();
        if ($yesterday) {
            $result = $this->getPercentageChange($yesterday->ordinary_stock_price_new, $this->attributes['ordinary_stock_price']);
            if ($yesterday->ordinary_stock_price_new < $this->attributes['ordinary_stock_price']) {
                return "-" . $result . '%';
            } else {
                return "+" . $result . '%';
            }
        } else {
            return '0%';
        }
    }

    public function branch()
    {
        return $this->belongsTo(\App\Models\Stocks::class, 'branch_id', 'id');
    }
}
