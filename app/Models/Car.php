<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\App;

class Car extends Model
{
    const IMAGE_STORE = '/uploads/cars';
    protected $fillable = [
        'name_uz', 'name_ru', 'pozition', 'price', 'image'
    ];
    public function brand(): BelongsTo {
        return $this->belongsTo(CarBrand::class, 'car_brand_id', 'id');
    }

    public function name() {
        $name = 'name_'. App::getLocale();
        return $this->$name;
    }
}
