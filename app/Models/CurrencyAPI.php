<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CurrencyAPI extends Model
{
    public function organization():BelongsTo
    {
    	return $this->belongsTo(Bank::class,'organization_id');
    }
}
