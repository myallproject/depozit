<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\App;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class DebitCards extends Model implements Searchable
{

    use Cachable;

    protected $fillable = ['id','bank_id','type_bank_id','currency_id','payment_id','date_id','name_uz','name_ru'];

    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class,'bank_id');
    }

    public function cardType(): BelongsTo
    {
        return $this->belongsTo(CardType::class,'type_card_id','id');
    }

    public function currencies()
    {
        $ids = explode(',',$this->currency_id);
        $currencies = [];
        foreach($ids as $id){
            $currencies[] = Currency::find($id);
        }
        return $currencies;
    }

    public function payments()
    {
        $ids = explode(',',$this->payment_id);
        $payments = [];
        foreach($ids as $id){
            $payment = PaymentType::find($id);
            array_push($payments, $payment->name);
        }
        return implode("-",$payments);
    }

    public function dateCard(): BelongsTo
    {
        return $this->belongsTo(DateService::class,'date_id');
    }

    public function name()
    {
        $name = 'name_'.App::getLocale();
        return $this->$name;
    }

    public function getSearchResult(): SearchResult
    {
        // TODO: Implement getSearchResult() method.

        $url = route('debit-cards',['locale'=>App::getLocale()]);
        return new SearchResult(
            $this,$this->name(),$url
        );
    }

    public function link($lang)
    {
        if($this->link){
            $array = json_decode($this->link,true);
            return $array[$lang];
        }
    }

    public function aktsiya_link($lang)
    {
        if($this->aktsiya_link){
            $array = json_decode($this->aktsiya_link,true);
            return $array[$lang];
        }
    }

    public function aktsiya_izoh($lang)
    {
        if($this->aktsiya_izoh){
            $array = json_decode($this->aktsiya_izoh,true);
            return $array[$lang];
        }
    }

    public function kontaksiz_tolov_izoh(){
        $name = 'kontaksiz_tolov_izoh_'.App::getLocale();
        return $this->$name;
    }

    public function cash_back_izoh(){
        $name = 'cash_back_izoh_'.App::getLocale();
        return $this->$name;
    }

    public function uyga_yetkazish_izoh(){
        $name = 'uyga_yetkazish_izoh_'.App::getLocale();
        return $this->$name;
    }

    public function tuluv_komissia_izoh(){
        $name = 'tuluv_komissia_izoh_'.App::getLocale();
        return $this->$name;
    }

    public function tuluv_komissia_boshqa_izoh(){
        $name = 'tuluv_komissia_boshqa_izoh_'.App::getLocale();
        return $this->$name;
    }

    public function naqdlash_komissia_izoh(){
        $name = 'naqdlash_komissia_izoh_'.App::getLocale();
        return $this->$name;
    }

    public function naqdlash_komissia_boshqa_izoh(){
        $name = 'naqdlash_komissia_boshqa_izoh_'.App::getLocale();
        return $this->$name;
    }

    public function price_izoh(){
        $name = 'price_izoh_'.App::getLocale();
        return $this->$name;
    }

    public function service(){
        $name = 'service_'.App::getLocale();
        return $this->$name;
    }

    public function qushimcha_qulaylik(){
        $name = 'qushimcha_qulaylik_'.App::getLocale();
        echo str_replace(';','<br>',$this->$name);
    }

    public function qushimcha_shartlar(){
        $name = 'qushimcha_shart_'.App::getLocale();
        return $this->$name;
    }

    public function rasmiy_yuli(){
        $name = 'rasmiy_yuli_'.App::getLocale();
        return $this->$name;
    }
}
