<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class CreditChildrenDate extends Model
{
	//use Cachable;
    public function dateId(): BelongsTo
    {
    	return	$this->belongsTo(DateService::class, 'date_id');
    }

    public function compensation(): BelongsTo
    {
        return $this->belongsTo(CreditBoshBadal::class,'compensation_id');
    }

    public function datename()
    {
    	$name = 'date_name_'.App::getLocale();
    	return $this->$name;
    }
}
