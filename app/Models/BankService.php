<?php

namespace App\Models;

use App\Models\Bank;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use App;
class BankService extends Model
{
    //use Cachable;
    protected $fillable = [
        'id','bank_id', 'slug', 'name_uz', 'name_ru', 'description_uz','description_ru','text_uz','text_ru'
    ];
    
    protected $dates = [
        'updated_at',
        'created_at'
    ];
    
    protected $casts = [
        'id' => 'integer',
        'bank_id' => 'integer',
        'slug' => 'string',
        'name_uz' => 'string',
        'name_ru' => 'string',
        'description_uz' => 'string',
        'description_ru' => 'string',
        'text_uz' => 'string',
        'text_ru' => 'string'
    ];
    
    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class,'bank_id');
    }
    
    public function name() {
        $name = 'name_'.App::getLocale();
        return $this->$name;
    }
}
