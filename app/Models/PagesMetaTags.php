<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PagesMetaTags extends Model
{
    public function metaData($lang)
    {
        $data=['uz' => '','ru' => ''];
        if($this->data){
    	   $data = json_decode($this->data,true);
        }
    	return $data[$lang];
    }
    
    public function attribute()
    {
    	$child = PagesMetaTags::where('parent_id',$this->id)->get();

    	return $child;
    }

    public function tagName()
    {  
        $tag = '';
        $tag = $this->tag;
        $open = '';
        $close = '';
        $html = [
            'open' => $this->tag,
            'close' => $close
        ];
        //$t = [1=> '', 2=>''];

        if($tag and strpos($tag, '|')){

            $t = explode('|',$tag);
        //dd($t);
             $open = $t[0];  $close = $t[1];
            $html = [
                'open' => $open,
                'close' => $close
            ];
        }
        return $html;
    }

    public function text($lang)
    {
        $text=[
            'uz' => '',
            'ru' => ''
        ];
        if($this->text){
            $text = json_decode($this->text,true);
        }
        return $text[$lang];
    }
}
