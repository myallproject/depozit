<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class Contact extends Model
{
	//use Cachable;
    protected $fillable = [
        'name', 'phone', 'email',' text', 'status'
    ];
    
    protected $dates = [
      'created_at', 'updated_at'
    ];
}
