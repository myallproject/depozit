<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class DateService extends Model
{
    //use Cachable;
    
    protected $fillable = ['name_uz', 'name_ru', 'status', 'service_id','description'];
    
    protected $dates = [
      'created_at','updated_at'
    ];
    
    
    public function service(): BelongsTo
    {
        return $this->belongsTo(Service::class,'service_id');
    }
    
    public function serviceChild(): BelongsTo
    {
        return $this->belongsTo(CreditType::class,'child_service_id');
    }
    
    public function name($x = 0): string 
    {
        if($x == 0 || App::getLocale() == "uz") {
            $langName = 'name_'.App::getLocale();
            return mb_strtolower($this->$langName);
        } else {
            if($x % 10 >= 5 || ($x >=5 && $x <=20) || $x % 10 == 0) $version = 2;
            else if($x % 10 == 1) $version = 0; else $version = 1;
            $dateVersion = $this->id;
            if($dateVersion == 9) {
                return trans_choice('lang.month', $version);
            } else if($dateVersion == 8) {
                return trans_choice('lang.year', $version);
            } else {
                return trans_choice('lang.day', $version);
            }
        }
        
    }
}
