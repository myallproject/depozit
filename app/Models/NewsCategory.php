<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class NewsCategory extends Model
{
	//use Cachable;
    public function name(){
    	$name = 'name_'.App::getLocale();
    	return $this->$name;
    }
}
