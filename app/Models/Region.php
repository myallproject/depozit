<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Region extends Model
{
   // use Cachable;
    protected  $fillable = [
        'name_uz' , 'name_ru', 'parent_id',
    ];
    
    protected $dates = [
        'created_at',
        'update_at'
    ];
    
    public function name() {
      $name = 'name_'.App::getLocale();
       return $this->$name;
    }
    
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Region::class,'parent_id');
    }

    public function children(): HasMany
    {
        $name = 'name_'.App::getLocale();
        return $this->hasMany(Region::class,'parent_id')->orderBy($name,'ASC');
    }
}
