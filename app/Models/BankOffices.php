<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App;

class BankOffices extends Model
{
    protected $fillable = ['bank_id','mfo','services','address','name','region_id','lang','lat','stir','phone'];

    public function region(): BelongsTo
    {
    	return $this->belongsTo(Region::class,'region_id','id');
    }

    public function getName($lang=false)
    {
    	return self::jsonDecode($this->name,$lang);
    }
   
    public function getAddress($lang=false)
    {
    	return self::jsonDecode($this->address,$lang);
    }

    public function getServices($lang=false)
    {
    	return self::jsonDecode($this->services,$lang);
    }

    public function getMFO($lang=false)
    {
    	return $this->mfo;
    }

    protected function jsonDecode($attribute,$lang=false){
    	$arr = ['uz' => '','ru' => ''];
    	if(!$lang){
    		$lang = App::getLocale();
    	}
    	if($attribute){
    		$arr = json_decode($attribute,true);
    	}
    	return $arr[$lang];
    }

}
