<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
class DepositsDate extends Model
{
    //use Cachable;
    public function dateName($x= '')
    {
		$x = intval($x);
    	$name = '';
    	foreach(config('global.date_type') as $k => $v){
    		if($this->date_type == $v['k']){
				if(($x >=5 && $x <=20)) {
					$x = 2;
					$name = trans_choice('lang.'.$v['name_lang'].'', $x);
				} else if($x % 10 == 1) {
					$x = 0;
					$name = trans_choice('lang.'.$v['name_lang'].'', $x);
				} else if(($x % 10 >=2) && ($x % 10 <= 4)) {
					$x = 1;
					$name = trans_choice('lang.'.$v['name_lang'].'', $x);
				} else {
					$x = 2;
					$name = trans_choice('lang.'.$v['name_lang'].'', $x);
				}				
    		}
    	}

    	return strtolower($name);
    }

    public function onMobileApp($id,$dpId,$date,$cid){
    	return $model = self::where('id','!=',$id)->where('deposit_id',$dpId)->where('date',$date)->where('percent_consider',$cid)->first();
    }

}
