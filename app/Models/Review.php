<?php

namespace App\Models;

use App\Models\Bp_reviewStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use App\User;
use App\Models\Region;
use App\Models\Notification;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Review extends Model
{   
    /**
    * answer_status = 1 new, 2 answered, 3 checked, 4 problem_solved , 4 answered , problem_solved
    *
    **/
    protected $table ='reviews';

    protected $fillable = ['title', 'fulltext', 'assessment', 'user_id', 'bank_id', 'branch_id', 'service_id', 'region_id','info_bank','answer_status'];


    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class,'review_id');
    }

    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class, 'bank_id');
    }

    public function service(): BelongsTo
    {
        return $this->belongsTo(Service::class, 'service_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function region(): BelongsTo
    {
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function limitedFulltext()
    {
        return Str::limit($this->fulltext, 400, ' ...');
    }

    public function assessmentClass()
    {
        if($this->assessment == 1 || $this->assessment == 2){
            return '--red';
        } elseif($this->assessment == 3) {
            return '--yellow';
        } elseif($this->assessment == 4 || $this->assessment == 5) {
            return '--green';
        } else {
            return '--yellow';
        }
    }

    public function assessmentColor()
    {
        switch ($this->assessment) {
                case 1:
                    return 'blackred';
                case 2:
                    return 'whitered'; 
                case 3:
                    return 'yellow';
                case 4:
                    return 'whitegreen';
                case 5:
                    return 'blackgreen';               
           }   
    }

    public function statusClass()
    {
        switch ($this->status) {
            case 0:
                return 'rejected';
            case 1:
                return 'waiting';
            case 2:
                return 'accepted';
            default:
                return '';
        }
    } 
    public function starClass()
    {
        switch ($this->status) {
            case 0:
                return 'star-rejected';
            case 1:
                return 'star-waiting tooltip';
            case 2:
                return '';
            default:
                return '';
        }
    }

    public function statusText()
    {
        switch ($this->status) {
            case 0:
                return 'review_rejected';
            case 1:
                return 'review_waiting';
            case 2:
                return 'review_accepted';
            default:
                return '';
        }
    }

    public function countComments(){
        return count($this->comments);
    }

    public function notifications()
    {
        $not = Notification::where('table_name',$this->getTable())->where('row_id',$this->id)->get(); 

        return $not;
    }

    public function moderatorComment($id){
        $return = ['id' => '', 'text' => ''];
        $c = Comment::where('review_id',$id)->get();
        if($c){
            foreach($c as $v){
                $user = User::find($v->user_id);

                if(in_array($user->role_id, [1,2,3,10])){
                    $return=['id'=>$v->id,'text'=>$v->text];
                }
            }            
        }

        return $return;
    }

    public function bp_reviewStatus(): HasOne {
        return $this->hasOne(Bp_reviewStatus::class, 'review_id', 'id');
    }

    public function bank_office(): BelongsTo {
        return $this->belongsTo(BankOffices::class, 'branch_id', 'id');
    }
}
