<?php

namespace App\Models;

use App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Brand extends Model
{
    protected $fillable = [
        'name'
    ];
    public function telephones() : HasMany
    {
        return $this->hasMany(Telephone::class, 'brand_id', 'id');
    }
    public function count_model() {
        $count = Telephone::where('brand_id', $this->id)->count();
        return $count;
    }
}
