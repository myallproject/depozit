<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\App;

class CarBrand extends Model
{
    public function cars():HasMany {
        return $this->hasMany(Car::class, 'car_brand_id', 'id');
    }
    public function name() {
        $lang = App::getLocale();
        $name = 'name_'.$lang;
        return $this->$name;
    }

    public function count_model() {
        $count = Car::where('car_brand_id', $this->id)->count();
        return $count;
    }
}
