<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\App;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use App\Models\ExchangeRate;
use App\Models\HotLineUser;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Bank extends Model implements Searchable{
    //use Cachable;
    protected $fillable = [
        'slug', 'name_uz', 'name_ru','phone','image','description_uz','description_ru','text_uz','text_ru','address','location','region_id','status','view','link','work_time_uz','work_time_ru'
    ];
    
    protected $dates = [
        'updated_at',
        'created_at'
    ];
    
    protected $casts = [
        'id' => 'integer',
        'slug'=>'string',
        'name_uz'=>'string',
        'name_ru'=>'string',
        'phone'=>'string',
        'image' => 'string',
        'description_uz' => 'string',
        'description_ru' => 'string',
        'text_uz' => 'string',
        'text_ru' => 'string',
        'address' => 'string',
        'location' => 'string',
        'region_id'=>'string',
        'status' => 'integer',
        'view' => 'integer',
        'link' => 'string',
        'work_time_uz' => 'string',
        'work_time_ru' => 'string'
    ];
    
    public function name() {
        $name ='name_'.App::getLocale();
        return $this->$name;
    }

    public function address() {
        $add ='address_'.App::getLocale();
        return $this->$add;
    }

    public function description()
    {
        $langname ='description_'.App::getLocale();
        return $this->$langname;
    }
    
    public function text()
    {
        $langname ='text_'.App::getLocale();
        return $this->$langname;
    }
    
    public function regions()
    {
        $result = [];
        $r_id_arr = explode(',',$this->region_id);
        foreach($r_id_arr as $v){
            $result[] = Region::find($v);
        }

        if(count($result) <=  0){
            $result = [];
        }

        return $result;
    }

    public function rate(): HasMany
    {
        return $this->hasMany (ExchangeRate::class,'bank_id','id');
    }

    public function workTime()
    {
        $langname = 'work_time_'.App::getLocale();
        return $this->$langname;
    }

    public function deposits(): HasMany
    {
        return $this->hasMany (Deposit::class,'bank_id','id');
    }

    public function credits(): HasMany
    {
        return $this->hasMany (Credit::class,'bank_id','id');
    }

    public function credit_cards(): HasMany
    {
        return $this->hasMany (CreditCards::class,'bank_id','id');
    }

    public function debit_cards(): HasMany
    {
        return $this->hasMany (DebitCards::class,'bank_id','id');
    }

    public function cards(){
        return $this->credit_cards() + $this->debit_cards();
    }

    public function information(): BelongsTo
    {
        return $this->belongsTo(BankInformation::class,'id','banks_id');
    }

    public function ratings(): BelongsTo
    {
        return $this->belongsTo(BankRating::class,'id','bank_id');
    }

    public function getSearchResult(): SearchResult
    {
        // TODO: Implement getSearchResult() method.

        $url = route('info-bank',['locale'=>App::getLocale(),'slug'=>$this->slug]);
        return new SearchResult(
            $this,$this->name_uz,$url
        );
    }

    public function ownership(): BelongsTo
    {
        return $this->belongsTo(TypeBanks::class,'bank_type_id');
    }

    public function bank_hodimi(): HasMany
    {
        return $this->hasMany(UserOrganization::class,'child_org_id','id');
    }

    //local uchun ozgartirildi
    public function rateUpdateTime(){
        $rate = ExchangeRate::select('updated_at')->where('bank_id',$this->id)->orderBy('updated_at','DESC')->first();

        return $rate; 
    }

    public function reviews(): HasMany{
        return $this->hasMany (Review::class,'bank_id','id');
    }

    public function hotLineUserOne(): BelongsTo
    {
        return $this->belongsTo(HotLineUser::class,'id','org_id');
    }


    public function avgMark($date='', $service_id=0){
        if($date && $service_id)
            $avg=$this->reviews->where('status',2)->where('service_id', $service_id)->where('created_at', '<', $date)->avg('assessment');
        elseif(!$date && $service_id)
            $avg=$this->reviews->where('status',2)->where('service_id', $service_id)->avg('assessment');
        elseif($date && !$service_id)
            $avg=$this->reviews->where('status',2)->where('created_at', '<', $date)->avg('assessment');
        else
            $avg=$this->reviews->where('status',2)->avg('assessment');
        return $avg?$avg:0;
    }

    public function formatedAvgMark($date='', $service_id=0){
        return number_format($this->avgMark($date,$service_id), 1);
    }
    public function countAllReviews($date='', $service_id=0){
        if($date && $service_id)
            return $this->reviews->where('service_id', $service_id)->where('created_at', '<', $date)->count();
        elseif(!$date && $service_id)
            return $this->reviews->where('service_id', $service_id)->count();
        elseif($date && !$service_id)
            return $this->reviews->where('created_at', '<', $date)->count();
        else
            return $this->reviews->count();
    }

    public function countAcceptedReviews($date='', $service_id=0){
        if($date && $service_id)
            return $this->reviews->where('status',2)->where('service_id', $service_id)->where('created_at', '<', $date)->count();
        elseif(!$date && $service_id)
            return $this->reviews->where('status',2)->where('service_id', $service_id)->count();
        elseif($date && !$service_id)
            return $this->reviews->where('status',2)->where('created_at', '<', $date)->count();
        else
            return $this->reviews->where('status',2)->count();
    }

    public function countSolvedReviews($date='', $service_id=0){
        if($date && $service_id)
            return $this->reviews->where('answer_status',4)->where('service_id', $service_id)->where('created_at', '<', $date)->count();
        elseif(!$date && $service_id)
            return $this->reviews->where('answer_status',4)->where('service_id', $service_id)->count();
        elseif($date && !$service_id)
            return $this->reviews->where('answer_status',4)->where('created_at', '<', $date)->count();
        else
            return $this->reviews->where('answer_status',4)->count();
    }

    public function countAnsweredReviews($date='', $service_id=0){
        $count=0;
        $reviews='';
        $bankers = $this->bank_hodimi;
        if($bankers){
            foreach($bankers as $banker){
                $banker_id = $banker->user_id;
                if($date && $service_id){
                    $reviews = $this->reviews->where('service_id', $service_id)->where('created_at', '<', $date);
                    foreach($reviews as $review){
                        $temp = ($review->comments->where('user_id', $banker_id)->count())?1:0;
                        $count += $temp;
                    }
                }
                elseif(!$date && $service_id){
                    $reviews = $this->reviews->where('service_id', $service_id);
                    foreach($reviews as $review){
                        $temp = ($review->comments->where('user_id', $banker_id)->count())?1:0;
                        $count += $temp;
                    }
                }
                elseif($date && !$service_id){
                    $reviews = $this->reviews->where('created_at', '<', $date);
                    foreach($reviews as $review){
                        $temp = ($review->comments->where('user_id', $banker_id)->count())?1:0;
                        $count += $temp;
                    }
                }
                else{
                    $reviews = $this->reviews;
                    foreach($reviews as $review){
                        $temp = ($review->comments->where('user_id', $banker_id)->count())?1:0;
                        $count += $temp;
                    }
                }
            }
        }
        return $count;
    }

    public function ranking($date='', $service_id=0){
        $solved=$this->countSolvedReviews($date, $service_id);
        $accepted=$this->countAcceptedReviews($date, $service_id);
        $all=$this->countAllReviews($date, $service_id);
        $score=0;
        if($all){
            $score = $this->countAnsweredReviews($date, $service_id)*20/$this->countAllReviews($date, $service_id) + $this->avgMark($date, $service_id)*10;
            if($accepted)
                $score += $solved*30/$accepted;
        }
        return number_format($score,1);
    }

    public function returnStars($date='', $service_id=0){
        $rating=$this->avgMark($date, $service_id);
        $fullStar = "<i class = 'fa fa-star'></i>";
        $halfStar = "<i class = 'fa fa-star-half-o'></i>";
        $emptyStar = "<i class = 'fa fa-star-o'></i>";
        $rating = $rating <= 5?$rating:5;

        $fullStarCount = (int)$rating;
        $halfStarCount = ceil($rating)-$fullStarCount;
        $emptyStarCount = 5 -$fullStarCount-$halfStarCount;

        $html = str_repeat($fullStar,$fullStarCount);
        $html .= str_repeat($halfStar,$halfStarCount);
        $html .= str_repeat($emptyStar,$emptyStarCount);

        return $html;
    }
    public function stars($date='', $service_id=0){
        
        echo $this->returnStars($date, $service_id);
    }

    public function questions(): HasMany
    {
        return $this->hasMany(Questions::class,'bank_id','id');
    }

    public function topServicesForSingleBank() {
        $services = Service::where('status','=',1)->get();
        $top_services = [];
        foreach($services as $service){
            $temp = [
                'service_name' => $service->getName(),
                'rank' => $this->formatedAvgMark('', $service->id)
            ];
            array_push($top_services,$temp);
        }
        usort($top_services, function($a,$b){
            return ($a['rank']>=$b['rank']) ? -1 : 1;
        });
        return $top_services;
    }
    
    public function citizenRankingKey(){
        return 'bank_'.$this->id.'_position';
    }
    
}