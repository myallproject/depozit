<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    // product_type_id => 1-deposit, 2-credit, 3-debet_card
    protected $fillable = [
        'id', 'user_name', 'phone_number', 'email', 'region_id', 'product_id', 'product_type_id', 'bank_id', 'agree'
    ];

    public function getUserName(){
        return $this->user_name;
    }

    public function getPhoneNumber(){
        return $this->phone_number;
    }

    public function getEmail(){
        return $this->email;
    }

    public function region() : BelongsTo
    {
        return $this->belongsTo(Region::class);
    }

    public function product() : BelongsTo
    {
        if($this->product_type_id == 1){
            return $this->belongsTo(Deposit::class, 'product_id');
        }
        else if($this->product_type_id == 2){
            return $this->belongsTo(Credit::class, 'product_id');
        }
        else if($this->product_type_id == 3){
            return $this->belongsTo(DebitCards::class, 'product_id');
        }
    }

    public function bank() : BelongsTo
    {
        return $this->belongsTo(Bank::class, 'bank_id');
    }
}
