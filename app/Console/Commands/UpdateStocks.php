<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\Services\UpdateStocksService;
use Exception;
use Illuminate\Support\Facades\Log;

class UpdateStocks extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Getting stock data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            (new UpdateStocksService())();
            $this->info('Stocks date is parsed');
        } catch (Exception $e) {
            Log::alert($e->getMessage());
            $this->error('Stocks are not parsed, something went wrong');
        }
    }
}
