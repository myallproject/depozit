<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;

class MakeView extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "make:view {view}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new blade template.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $view = $this->argument("view");
		//$extends = $this->argument("extends");
        
        $path = $this->viewPath($view);
        
        $this->createDir($path);
  
        if(File::exists($path)) {
            $this->error("View {$path} already exists!");
                return;
        }
		
		//$extends = '@extends(\''.$extends.'\')';
        
        File::put($path,$path);
        
        $this->info("View created successfully.");
        
    }
    
    public function viewPath($view) {
        $view = str_replace(".","/", $view) . ".blade.php";
        
        $path = "resources/views/{$view}";
        
        return $path;
    }
    
    public function createDir($path) {
        $dir = dirname($path);
        
        if(!file_exists($dir)){
            mkdir($dir,0777,true);
        }
    }
}
