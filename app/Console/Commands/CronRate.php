<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\CBExchangeRate;

class CronRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:rate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron Rate Exchange';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $get = curl_init('https://cbu.uz/oz/arkhiv-kursov-valyut/json');
        
        curl_setopt($get, CURLOPT_HEADER,true);
        curl_setopt($get, CURLOPT_NOBODY,true);
        curl_setopt($get, CURLOPT_RETURNTRANSFER ,true); 
        curl_exec($get);
        $code = curl_getinfo($get,CURLINFO_HTTP_CODE);
        //curl_close($get);

        if($code == 301){
            //$xml = 'http://cbu.uz/oz/arkhiv-kursov-valyut/xml/';
        
        $xml = 'https://cbu.uz/oz/arkhiv-kursov-valyut/json/all/'.date("Y-m-d").'/';
        $file_get_content = file_get_contents ($xml);
        
        //$simplexml = simplexml_load_string($file_get_content);
       // $json = json_encode($simplexml);
       
        $rateList = json_decode ($file_get_content, true);
        //dd($rateList['CcyNtry']);
         //dd($rateList);
        //CBExchangeRate::query()->delete();
         foreach($rateList as $row){ 
                $model = CBExchangeRate::where('key',$row['Ccy'])->first();

                if(!$model){ 
                    $model = new CBExchangeRate();
                }

                $model->code = $row['Code'];
                $model->key = $row['Ccy'];
                $model->name = $row['CcyNm_EN'];
                $model->nominal = $row['Nominal'];
                $model->rate = $row['Rate'];
                $model->diff = $row['Diff'];
                $model->updated_at = date('Y-m-d H:i:s',strtotime($row["Date"]));
                $model->save();
            }
            // foreach($rateList['CcyNtry'] as $row){
            //     $code = $row['@attributes']['ID'];
            //     $model = CBExchangeRate::where('key',$row['Ccy'])->first();
            //     $diff_rate = 0;
            //     if($model){
            //         $old_rate = $model->rate;
            //         $diff_rate = (float)$row['Rate'] - (float)$old_rate;
            //     }
                
                
            //     if(!$model){ 
            //         $model = new CBExchangeRate();
            //     }

            //     $model->code = $code;
            //     $model->key = $row['Ccy'];
            //     $model->name = $row['CcyNm_EN'];
            //     $model->nominal = $row['Nominal'];
            //     $model->rate = $row['Rate'];
                
            //     if(date('Y-m-d',strtotime($model->updated_at)) != date('Y-m-d')){
            //         $model->diff = $diff_rate;
            //     }

            //     $model->updated_at = date('Y-m-d H:i:s');
            //     $model->save();
            // }
        $this->info('CB rate run successfully at '.date('d-m-Y H:i:s'));

        }
        

    }
}
