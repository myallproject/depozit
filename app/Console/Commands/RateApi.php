<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Components\BankExchangeRateApi;

class RateApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rate:api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rate api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        BankExchangeRateApi::parseHubCurrencyApi();
        //BankExchangeRateApi::tengeBank();
        BankExchangeRateApi::psb();
        BankExchangeRateApi::nbu();
        BankExchangeRateApi::poytaxtBank();
        $this->info('This command has done successfully at '.date('d-m-Y H:i:s'));
    }
}
