<?php

namespace App\Console\Commands;

use App\Components\RassrochkaTelefonApi;
use App\TelefonAPI;
use Illuminate\Console\Command;

class UpdateTelfonePrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateTelefonePrice:api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update shops telefone prices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        RassrochkaTelefonApi::parseHubCurrencyApi();
        $this->info('This command has done successfully at ' . date('d-m-Y H:i:s'));
    }
}
