<?php
// Samandar Mirakhmedov
namespace App\Repositories;

use App\Models\StockChanges;
use App\Models\Stocks;
use ErrorException;
use Exception;
use Illuminate\Support\Facades\Log;

class StocksRepository
{
    public function deleteAllRecords()
    {
        return Stocks::query()->delete();
    }

    /**
     * Creating or refreshing date accroding to record in DB
     *
     * @param [object] $item
     * @return boolean
     */

    public function create($item)
    {
        try {
            Stocks::create([
                'name' => $item->name,
                'ticker' => (isset($item->ticker)) ? $item->ticker : null,
                'ordinary_stock_price' => (isset($item->ordinary_stock_price)) ? $item->ordinary_stock_price : null,
                'preferred_stock_price' => (isset($item->preferred_stock_price)) ? $item->preferred_stock_price : null,
            ]);
            return true;
        } catch (ErrorException $e) {
            Log::alert($e->getMessage());
        }
    }

    /**
     * Creating or refreshing date accroding to record in DB
     *
     * @param [object] $item
     * @return boolean
     */
    public function refresh($item)
    {
        if (!isset($item->ticker)) {
            return false;
        }
        $stock_element = Stocks::where('ticker', $item->ticker)
            ->first();
        if ($stock_element) {
            $stock_element->ordinary_stock_price = (isset($item->ordinary_stock_price)) ? $item->ordinary_stock_price : null;
            $stock_element->preferred_stock_price = (isset($item->preferred_stock_price)) ? $item->preferred_stock_price : null;
            $stock_element->save();

            $this->recordChanges($stock_element, $item);
        } else {
            return $this->create($item);
        }
    }

    public function refreshItems($stocks)
    {
        if (!empty($stocks)) {
            foreach ($stocks->company as $key => $stock) {
                $refresh = $this->refresh($stock);
                if (!$refresh) {
                    continue;
                }
            }
        }
    }

    public function recordChanges($current, $new)
    {
        try {
            if ($current->ordinary_stock_price !== $new->ordinary_stock_price) {
                return $this->createRecordChange($current, $new);
            } elseif ($current->preferred_stock_price !== $new->preferred_stock_price) {
                return $this->createRecordChange($current, $new);
            }
        } catch (ErrorException $e) {
            Log::info($e->getMessage());
        }
    }

    public function createRecordChange($current, $new)
    {
        $data = [
            'stock_id' => $current->id,
            'ordinary_stock_price_old' => $current->ordinary_stock_price,
            'preferred_stock_price_old' => $current->preferred_stock_price,
            'ordinary_stock_price_new' => $new->ordinary_stock_price,
            'preferred_stock_price_new' => $new->preferred_stock_price
        ];

        if ($current->ordinary_stock_price < $new->ordinary_stock_price && $current->preferred_stock_price < $new->preferred_stock_price) {
            $change = (($current->ordinary_stock_price - $new->ordinary_stock_price) / $current->ordinary_stock_price) * 100;
            $data['change_percentage_up'] = $change;
            $data['change_percentage_down'] = '0';
        } else {
            $change = (($new->ordinary_stock_price - $current->ordinary_stock_price) / $current->ordinary_stock_price) * 100;
            $data['change_percentage_down'] = $change;
            $data['change_percentage_up'] = '0';
        }

        return StockChanges::create($data);
    }
}
