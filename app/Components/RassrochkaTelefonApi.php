<?php 

namespace App\Components;

use App\Models\Brand;
use App\Models\ExchangeRate;
use App\Models\Currency;
use App\Models\CBExchangeRate;
use App\Models\OtherExchangeRateBank;
use App\Models\Notification;
use App\Models\Shop;
use App\Models\ShopTelephone;
use App\Models\ShopTelephones;
use App\Models\ShopTerm;
use App\Models\TelefonAPI;
use App\Models\Telephone;
use App\Models\TelephoneUrl;

class RassrochkaTelefonApi {
	public static function parseHubFileGetContent($api)
	{	$get_content = '';
		$arr = [];
		if(!empty($api)){
			$get_content = file_get_contents($api);
		}
		if($get_content){
		 	$gzdecode =  gzdecode($get_content);
			$arr = json_decode($gzdecode,true);
		}
		return  $arr;
	}

	protected static function file_post_contents($url, $data = false) {

		$options = [
			'http' => ['method'=>'POST', 'header' => 'Content-type: application/json', 'content' => $data],
			'ssl'  => [ 'verify_peer'=>false, 'verify_peer_name'=>false, ],
		];
		$context = stream_context_create($options);

		return file_get_contents($url, false, $context);
	}
	//for checking telefone model and excel import
	public static function telefonFix() {
		$brands = Brand::orderBy('name')->get(['id','name']);
		$brands_to_send=[];
		$problem_with_brand = [];
		$deleted = [];
		foreach($brands as $brand) {
			$problem = [];
			$telephones = $brand->telephones()->get(['id', 'smartphone_model', 'smartphone_weight', 'smartphone_dimension', 'smartphone_operation_memory', 'smartphone_inner_memory']);
			$i = 0;
			$done = [];
			foreach($telephones as $t) {
				$j = 0;
				$eachrow = [$t];
				if(!array_key_exists($t->id, $deleted)) {
					foreach($telephones as $ct) {
						if($j > $i && !array_key_exists($ct->id, $deleted) && !array_key_exists($ct->id, $done)) {
							$model = false; 
							$physics = false;
							$memory = false;
							$physicsNull = false;
							$memoryNull = false;
							if($t->smartphone_model == $ct->smartphone_model) $model = true;
							if(($t->smartphone_weight == null || $ct->smartphone_weight == null || $t->smartphone_weight == $ct->smartphone_weight) && ($t->smartphone_dimension == null || $ct->smartphone_dimension == null || $t->smartphone_dimension == $ct->smartphone_dimension)) $physics = true;
							if(($t->smartphone_weight == null || $ct->smartphone_weight == null ) && ($t->smartphone_dimension == null || $ct->smartphone_dimension == null)) $physics = false;
							if(($t->smartphone_weight == null && $ct->smartphone_weight == null ) && ($t->smartphone_dimension == null && $ct->smartphone_dimension == null)) $physicsNull = true;
							if(($t->smartphone_operation_memory == null || $ct->smartphone_operation_memory == null || $t->smartphone_operation_memory == $ct->smartphone_operation_memory) && ($t->smartphone_inner_memory == null || $ct->smartphone_inner_memory == null || $t->smartphone_inner_memory == $ct->smartphone_inner_memory)) $memory = true;
							if(($t->smartphone_operation_memory == null || $ct->smartphone_operation_memory == null ) && ($t->smartphone_inner_memory == null || $ct->smartphone_inner_memory == null)) $memory = false;
							if(($t->smartphone_operation_memory == null && $ct->smartphone_operation_memory == null ) && ($t->smartphone_inner_memory == null && $ct->smartphone_inner_memory == null)) $memoryNull = true;
							if($model && ($physics || $physicsNull) && ($memory || $memoryNull)) {
								$deleted[$ct->id] = 1;
								Telephone::find($ct->id)->delete();
							} else if(((!$model && $physics) || ($model && !$physics)) && ($memory || $memoryNull)) {
								$done[$ct->id] = 1;
								array_push($eachrow, $ct);
							}
						}		
						$j++;							
					}
				}				
				if(sizeof($eachrow) > 1) {
					array_push($problem, $eachrow);
				}
				$i++;
			}
			if(count($problem)>0) {
				$problem_with_brand[$brand->id] = $problem;
				$brands_to_send[$brand->id]= $brand;
			}
		}
		$deleted_number = count($deleted);
		$success  = "$deleted_number ta telefon o'chirildi";
		return compact('problem_with_brand', 'brands_to_send','success');
	}

	public static function removeSpace($x) {
		$x = str_replace(' ', '', $x);
		return $x;
	}


	public static function parseHubAsaxiy() {

	}

	public static function addBrand($term_brand) {
		$brand = Brand::where('name','=',$term_brand);
		if($brand->exists()) {
			$brand = $brand->first();
		} else {
			$brand = new Brand();
			$brand->name = $term_brand;
			$brand->save();
		}
		return $brand;
	}




	public static function parseHubTelefonApi()
	{	
		$apis = TelefonAPI::all();
		$deleted = 0;
		$problem = 0;
		$iteration = 0;
		foreach($apis as $api){
			$api_array =  self::parseHubFileGetContent($api->api());
			// $iteration++;
			// if($iteration == 2) {
			// 	dd($api_array);
			// }		
			if(array_key_exists('smartphone', $api_array)){
				foreach($api_array['smartphone'] as $term){
					//checkin for brand
					if(!array_key_exists('brand',$term)) $term['brand'] = "Brendsiz";
					$brand = self::addBrand($term['brand']);
					//checking for terms properties
					//if it is texnomart
					if($api->organization->name_uz == "Texnomart") {
						if(array_key_exists('installment_price', $term)) {
							if(array_key_exists('weight', $term)) $term['weight'] = $term['weight'] ." г";
							if(array_key_exists('dimension', $term)) $term['dimension'] = $term['dimension'] . " мм";
							$term['installment_price'] = self::removeSpace($term['installment_price']);
							$texno_monthly_payment = (int)($term['installment_price'] / 12);
							$texno_term = ["term" => 12, 
											"monthly_payment" => $texno_monthly_payment, 
											"total_payment" => $term['installment_price']
							];
							$term['term'] = [0 => $texno_term];
						}						
					}					
					//if it is texnomart end
					if(array_key_exists('url', $term) && array_key_exists('model', $term) 
					&& array_key_exists('price', $term) && (array_key_exists('term', $term))) {
						//checking if additional properties are coming
						if(!array_key_exists('weight', $term)) $term['weight'] = null;
						if(!array_key_exists('dimension', $term)) $term['dimension'] = null;
						if(!array_key_exists('inner_memory', $term)) $term['inner_memory'] = null;
						if(!array_key_exists('operation_memory', $term)) $term['operation_memory'] = null;
						//checking for this term is already exist
						$shop_telefon = ShopTelephone::where([
							['smartphone_model', '=', $term['model']],
							['brand_id','=',$brand->id]							
						]);

						$telefon = Telephone::where([
							['smartphone_model', '=', $term['model']],
							['brand_id','=',$brand->id]									
						]);
						///////////
						$match_telefon = ShopTelephone::where([
							['brand_id','=',$brand->id]		
						]);
						
						if(is_null($term['weight'])) {
							$shop_telefon = $shop_telefon->whereNull('smartphone_weight');
							$telefon = $telefon->whereNull('smartphone_weight');
							$match_telefon = $match_telefon->whereNull('smartphone_weight');
						} else {
							$shop_telefon = $shop_telefon->where('smartphone_weight','=',$term['weight']);
							$telefon = $telefon->where('smartphone_weight','=',$term['weight']);
							$match_telefon = $match_telefon->where('smartphone_weight','=',$term['weight']);
						}
						if(is_null($term['dimension'])) {
							$shop_telefon = $shop_telefon->whereNull('smartphone_dimension');
							$telefon = $telefon->whereNull('smartphone_dimension');
							$match_telefon = $match_telefon->whereNull('smartphone_dimension');
						} else {
							$shop_telefon = $shop_telefon->where('smartphone_dimension','=',$term['dimension']);
							$telefon = $telefon->where('smartphone_dimension','=',$term['dimension']);
							$match_telefon = $match_telefon->where('smartphone_dimension','=',$term['dimension']);
						}
						if(is_null($term['inner_memory'])) {
							$shop_telefon = $shop_telefon->whereNull('smartphone_inner_memory');
							$telefon = $telefon->whereNull('smartphone_inner_memory');
							$match_telefon = $match_telefon->whereNull('smartphone_inner_memory');
						} else {
							$shop_telefon = $shop_telefon->where('smartphone_inner_memory','=',$term['inner_memory']);
							$telefon = $telefon->where('smartphone_inner_memory','=',$term['inner_memory']);
							$match_telefon = $match_telefon->where('smartphone_inner_memory','=',$term['inner_memory']);
						}
						if(is_null($term['operation_memory'])) {
							$shop_telefon = $shop_telefon->whereNull('smartphone_operation_memory');
							$telefon = $telefon->whereNull('smartphone_operation_memory');
							$match_telefon = $match_telefon->whereNull('smartphone_operation_memory');
						} else{
							$shop_telefon = $shop_telefon->where('smartphone_operation_memory','=',$term['operation_memory']);
							$telefon = $telefon->where('smartphone_operation_memory','=',$term['operation_memory']);
							$match_telefon = $match_telefon->where('smartphone_operation_memory','=',$term['operation_memory']);
						}

						$shop_telefon = $shop_telefon->first();
						$telefon = $telefon->first();
						$match_telefon = $match_telefon->first();


						///////

						if($match_telefon != null && $shop_telefon == null && $telefon == null) {
							$deleted++;
						} else {
							if(!is_null($shop_telefon)) {
								if(ShopTerm::where('shop_telephone_id','=',$shop_telefon->id)->exists()) {
									$shop_terms = ShopTerm::where('shop_telephone_id','=',$shop_telefon->id)->get();
									foreach($shop_terms as $val) {
										$val->delete();
									}	
								}
													
							} else {
								$shop_telefon = new ShopTelephone();	
								$shop_telefon->brand_id = $brand->id;						
								$shop_telefon->smartphone_model = $term['model'];
								$shop_telefon->smartphone_weight = $term['weight'];
								$shop_telefon->smartphone_dimension = $term['dimension'];
								$shop_telefon->smartphone_inner_memory = $term['inner_memory'];
								$shop_telefon->smartphone_operation_memory = $term['operation_memory'];
							}	
							
							if($telefon != null) {
								$shop_telefon->telephone_id = $telefon->id;
							} 
							$shop_telefon->shop_id = $api->shop_id;
							$shop_telefon->url = $term['url'];
							$shop_telefon->smartphone_price = self::removeSpace($term['price']);
							$shop_telefon->save();
							foreach($term['term'] as $val) {
								$obj = new ShopTerm();
								$obj->shop_telephone_id = $shop_telefon->id;
								$obj->term = $val['term'];
								$obj->monthly_payment = self::removeSpace($val['monthly_payment']);
								$obj->total_payment = self::removeSpace($val['total_payment']);
								$obj->save();
							}
						}						
					}					
				}
			} else {
				$problem++;
				$not = Notification::where('table_name', $api->getTable())->where('status', 1)->where('row_id',$api->id)->first();
				
				if(empty($not)){
					$not = new Notification();
				}

				$not->table_name = 'telefon_a_p_i_s';
				$not->row_id = $api->id;
				$not->row_column = $api->api;
				$not->row_column_value = 'Please check this api';
				$not->save();
			}
		}
		return compact('deleted', 'problem');
	}
//agar keyinroq telefon qo'shilgan avvalgi xatolar match bo'lganligini tekshirish
	public function telefonShopMatch() {
		$shop_telefones = ShopTelephone::where('telephone_id','=', null);
		foreach($shop_telefones as $shop_telefon) {
			$telefon = Telephone::where([
				['smartphone_model', '=', $shop_telefon->smartphone_model],
				['smartphone_weight','=', $shop_telefon->smartphone_weight],
				['smartphone_dimension', '=', $shop_telefon->smartphone_dimension],
				['smartphone_inner_memory','=',$shop_telefon->smartphone_inner_memory],
				['smartphone_operation_memory','=', $shop_telefon->smartphone_operation_memory]							
			]);
			if($telefon->exists()) {
				$telefon = $telefon->first();
				$shop_telefon->telephone_id = $telefon->id;
			}
		}
	}
	//magazinlarda kelayotgan telefonlardagi telephone_id si null bo'lganlari
	public static function telefonAPIFix() {
		$shops = Shop::orderBy('name_uz')->get(['id','name_uz']);
		$shops_to_send=[];
		$problem_with_shop = [];
		$created = 0;
		$done=false;
		foreach($shops as $shop) {
			$problem = [];
			$shop_telefones = ShopTelephone::where('shop_id','=', $shop->id)->whereNull('telephone_id')->get(['id', 'smartphone_model', 'smartphone_weight', 'smartphone_dimension', 'smartphone_operation_memory', 'smartphone_inner_memory','brand_id']);
			$telephones = Telephone::all(['id', 'smartphone_model', 'smartphone_weight', 'smartphone_dimension', 'smartphone_operation_memory', 'smartphone_inner_memory','brand_id']);
			
			foreach($shop_telefones as $sh_t) {
				$eachrow = [$sh_t];
				foreach($telephones as $ct) {				
					$model = false; 
					$physics = false;
					$memory = false;
					$physicsNull = false;
					$memoryNull = false;
					if($sh_t->smartphone_model == $ct->smartphone_model) $model = true;
					if(($sh_t->smartphone_weight == null || $ct->smartphone_weight == null || $sh_t->smartphone_weight == $ct->smartphone_weight) && ($sh_t->smartphone_dimension == null || $ct->smartphone_dimension == null || $sh_t->smartphone_dimension == $ct->smartphone_dimension)) $physics = true;
					if(($sh_t->smartphone_weight == null || $ct->smartphone_weight == null ) && ($sh_t->smartphone_dimension == null || $ct->smartphone_dimension == null)) $physics = false;
					if(($sh_t->smartphone_weight == null && $ct->smartphone_weight == null ) && ($sh_t->smartphone_dimension == null && $ct->smartphone_dimension == null)) $physicsNull = true;
					if(($sh_t->smartphone_operation_memory == null || $ct->smartphone_operation_memory == null || $sh_t->smartphone_operation_memory == $ct->smartphone_operation_memory) && ($sh_t->smartphone_inner_memory == null || $ct->smartphone_inner_memory == null || $sh_t->smartphone_inner_memory == $ct->smartphone_inner_memory)) $memory = true;
					if(($sh_t->smartphone_operation_memory == null || $ct->smartphone_operation_memory == null ) && ($sh_t->smartphone_inner_memory == null || $ct->smartphone_inner_memory == null)) $memory = false;
					if(($sh_t->smartphone_operation_memory == null && $ct->smartphone_operation_memory == null ) && ($sh_t->smartphone_inner_memory == null && $ct->smartphone_inner_memory == null)) $memoryNull = true;
					if($model && ($physics || $physicsNull) && ($memory || $memoryNull)) {
						$sh_t->telephone_id = $ct->id;
						$sh_t->save();
						$done=true;
						break;
					} else if(((!$model && $physics) || ($model && !$physics)) && $memory) {
						array_push($eachrow, $ct);
					} else if($model && ($physics || $physicsNull) && !$memory) {
						$newTelefone = Telephone::where([
							['smartphone_model', '=', $sh_t->smartphone_model]
						]);
						if(is_null($sh_t['weight'])) {
							$newTelefone = $newTelefone->whereNull('smartphone_weight');
						} else {
							$newTelefone = $newTelefone->where('smartphone_weight','=',$sh_t['weight']);
						}
						if(is_null($sh_t['dimension'])) {
							$newTelefone = $newTelefone->whereNull('smartphone_dimension');
						} else {
							$newTelefone = $newTelefone->where('smartphone_dimension','=',$sh_t['dimension']);
						}
						if(is_null($sh_t['inner_memory'])) {
							$newTelefone = $newTelefone->whereNull('smartphone_inner_memory');
						} else {;
							$newTelefone = $newTelefone->where('smartphone_inner_memory','=',$sh_t['inner_memory']);
						}
						if(is_null($sh_t['operation_memory'])) {
							$newTelefone = $newTelefone->whereNull('smartphone_operation_memory');
						} else{
							$newTelefone = $newTelefone->where('smartphone_operation_memory','=',$sh_t['operation_memory']);
						}
						if(!$newTelefone->exists()) {
							$tel = new Telephone();
							$tel->smartphone_model = $sh_t->smartphone_model;
							$tel->smartphone_weight = $sh_t->smartphone_weight;
							$tel->smartphone_dimension = $sh_t->smartphone_dimension;
							$tel->smartphone_operation_memory = $sh_t->smartphone_operation_memory;
							$tel->smartphone_inner_memory = $sh_t->smartphone_inner_memory;
							$tel->brand_id = $ct->brand_id;
							$tel->save();
							$sh_t->telephone_id = $tel->id;
							$sh_t->save();
							$created++;
							$done=true;
							break;
						}							
					} 											
				}
				
				if(sizeof($eachrow) == 1) {
						$tel = new Telephone();
						$tel->smartphone_model = $sh_t->smartphone_model;
						$tel->smartphone_weight = $sh_t->smartphone_weight;
						$tel->smartphone_dimension = $sh_t->smartphone_dimension;
						$tel->smartphone_operation_memory = $sh_t->smartphone_operation_memory;
						$tel->smartphone_inner_memory = $sh_t->smartphone_inner_memory;
						$tel->brand_id = $ct->brand_id;
						$tel->save();
						$sh_t->telephone_id = $tel->id;
						$sh_t->save();
						$created++;
						$done=true;
				}
				if(sizeof($eachrow) > 1 || $done == false) {
					array_push($problem, $eachrow);
				}
			}
			if(count($problem)>0) {
				$problem_with_shop[$shop->id] = $problem;
				$shops_to_send[$shop->id]= $shop;
			}
		}
		if($created > 0) $success  = "$created ta telefon qo'shildi";
		else $success = null;
		return compact('problem_with_shop', 'shops_to_send','success');
	}

	public static function parseHubTelefonCharApi() {
		$project_token = "t6Xq6vTfKWue";
        $api_key = "tUSd8dE28pgn";  
		$api = "https://www.parsehub.com/api/v2/projects/$project_token/last_ready_run/data?api_key=$api_key";
		$problem = 0;
		$added = 0;
		$already_added = 0;
		$api_array =  self::parseHubFileGetContent($api);
		if(array_key_exists('smartphone', $api_array)){
			$telefones = $api_array['smartphone'];
			$all = count($telefones);	
			foreach($telefones as $telefon) {
				if(array_key_exists('model', $telefon)) {
					if(!array_key_exists('weight', $telefon)) $telefon['weight'] = null;
					if(!array_key_exists('dimension', $telefon)) $telefon['dimension'] = null;
					if(!array_key_exists('inner_memory', $telefon)) $telefon['inner_memory'] = null;
					if(!array_key_exists('operation_memory', $telefon)) $telefon['operation_memory'] = null;
					$match_telefon = Telephone::where([
						['smartphone_model', '=', $telefon['model']],
					]);
					if(is_null($telefon['weight'])) {
						$match_telefon = $match_telefon->whereNull('smartphone_weight');
					} else {
						$match_telefon = $match_telefon->where('smartphone_weight','=',$telefon['weight']);
					}
					if(is_null($telefon['dimension'])) {
						$match_telefon = $match_telefon->whereNull('smartphone_dimension');
					} else {
						$match_telefon = $match_telefon->where('smartphone_dimension','=',$telefon['dimension']);
					}
					if(is_null($telefon['inner_memory'])) {
						$match_telefon = $match_telefon->whereNull('smartphone_inner_memory');
					} else {;
						$match_telefon = $match_telefon->where('smartphone_inner_memory','=',$telefon['inner_memory']);
					}
					if(is_null($telefon['operation_memory'])) {
						$match_telefon = $match_telefon->whereNull('smartphone_operation_memory');
					} else{
						$match_telefon = $match_telefon->where('smartphone_operation_memory','=',$telefon['operation_memory']);
					}
					if(!$match_telefon->exists()) {
						$newTelefon = new Telephone();
						$newTelefon->smartphone_model = $telefon['model'];
						$newTelefon->smartphone_weight = $telefon['weight'];						
						$newTelefon->smartphone_dimension = $telefon['dimension'];
						$newTelefon->smartphone_inner_memory = $telefon['inner_memory'];
						$newTelefon->smartphone_operation_memory = $telefon['operation_memory'];
						if(array_key_exists('type', $telefon)) {
							$newTelefon->smartphone_type = $telefon['type'];
						}
						if(array_key_exists('body_type', $telefon)) {
							$newTelefon->smartphone_body_type = $telefon['body_type'];
						}
						if(array_key_exists('body_material', $telefon)) {
							$newTelefon->smartphone_body_material = $telefon['body_material'];
						}
						if(array_key_exists('sim_number', $telefon)) {
							$newTelefon->smartphone_sim_number = $telefon['sim_number'];
						}
						if(array_key_exists('sim_mode', $telefon)) {
							$newTelefon->smartphone_sim_mode = $telefon['sim_mode'];
						}
						if(array_key_exists('display_type', $telefon)) {
							$newTelefon->smartphone_display_type = $telefon['display_type'];
						}
						if(array_key_exists('diagonal', $telefon)) {
							$newTelefon->smartphone_diagonal = $telefon['diagonal'];
						}
						if(array_key_exists('image_size', $telefon)) {
							$newTelefon->smartphone_image_size = $telefon['image_size'];
						}
						if(array_key_exists('pixels', $telefon)) {
							$newTelefon->smartphone_pixels = $telefon['pixels'];
						}
						if(array_key_exists('audio', $telefon)) {
							$newTelefon->smartphone_audio = $telefon['audio'];
						}
						if(array_key_exists('earphone', $telefon)) {
							$newTelefon->smartphone_earphone = $telefon['earphone'];
						}
						if(array_key_exists('standard', $telefon)) {
							$newTelefon->smartphone_standard = $telefon['standard'];
						}
						if(array_key_exists('interface', $telefon)) {
							$newTelefon->smartphone_interface = $telefon['interface'];
						}
						if(array_key_exists('memory_card', $telefon)) {
							$newTelefon->smartphone_memory_card = $telefon['memory_card'];
						}
						if(array_key_exists('battery_type', $telefon)) {
							$newTelefon->smartphone_battery_type = $telefon['battery_type'];
						}
						if(array_key_exists('battery_volume', $telefon)) {
							$newTelefon->smartphone_battery_volume = $telefon['battery_volume'];
						}
						if(array_key_exists('battery_lifetime_call', $telefon)) {
							$newTelefon->smartphone_battery_lifetime_call = $telefon['battery_lifetime_call'];
						}
						if(array_key_exists('battery_lifetime_sleep', $telefon)) {
							$newTelefon->smartphone_battery_lifetime_sleep = $telefon['battery_lifetime_sleep'];
						}
						if(array_key_exists('other', $telefon)) {
							$newTelefon->smartphone_other = $telefon['other'];
						}
						if(array_key_exists('internet_connection', $telefon)) {
							$newTelefon->smartphone_internet_connection = $telefon['internet_connection'];
						}
						if(array_key_exists('os', $telefon)) {
							$newTelefon->smartphone_os = $telefon['os'];
						}
						if(array_key_exists('os_beginning', $telefon)) {
							$newTelefon->smartphone_os_beginning = $telefon['os_beginning'];
						}
						if(array_key_exists('body_construction', $telefon)) {
							$newTelefon->smartphone_body_construction = $telefon['body_construction'];
						}
						if(array_key_exists('contactless_payment', $telefon)) {
							$newTelefon->smartphone_contactless_payment = $telefon['contactless_payment'];
						}
						if(array_key_exists('sensor_type', $telefon)) {
							$newTelefon->smartphone_sensor_type = $telefon['sensor_type'];
						}
						if(array_key_exists('proportion', $telefon)) {
							$newTelefon->smartphone_proportion = $telefon['proportion'];
						}
						if(array_key_exists('main_camera_number', $telefon)) {
							$newTelefon->smartphone_main_camera_number = $telefon['main_camera_number'];
						}
						if(array_key_exists('main_camera_resolution', $telefon)) {
							$newTelefon->smartphone_main_camera_resolution = $telefon['main_camera_resolution'];
						}
						if(array_key_exists('main_camera_diafragma', $telefon)) {
							$newTelefon->smartphone_main_camera_diafragma = $telefon['main_camera_diafragma'];
						}
						if(array_key_exists('flashlight', $telefon)) {
							$newTelefon->smartphone_flashlight = $telefon['flashlight'];
						}
						if(array_key_exists('main_camera_functions', $telefon)) {
							$newTelefon->smartphone_main_camera_functions = $telefon['main_camera_functions'];
						}
						if(array_key_exists('video_recording', $telefon)) {
							$newTelefon->smartphone_video_recording = $telefon['video_recording'];
						}
						if(array_key_exists('front_camera', $telefon)) {
							$newTelefon->smartphone_front_camera = $telefon['front_camera'];
						}
						if(array_key_exists('processor', $telefon)) {
							$newTelefon->smartphone_processor = $telefon['processor'];
						}
						if(array_key_exists('core', $telefon)) {
							$newTelefon->smartphone_core = $telefon['core'];
						}
						if(array_key_exists('video_processor', $telefon)) {
							$newTelefon->smartphone_video_processor = $telefon['video_processor'];
						}
						if(array_key_exists('wireless_charging', $telefon)) {
							$newTelefon->smartphone_wireless_charging = $telefon['wireless_charging'];
						}
						if(array_key_exists('fast_charging', $telefon)) {
							$newTelefon->smartphone_fast_charging = $telefon['fast_charging'];
						}
						if(array_key_exists('loud_speaker', $telefon)) {
							$newTelefon->smartphone_loud_speaker = $telefon['loud_speaker'];
						}
						if(array_key_exists('managing', $telefon)) {
							$newTelefon->smartphone_managing = $telefon['managing'];
						}
						if(array_key_exists('detectors', $telefon)) {
							$newTelefon->smartphone_detectors = $telefon['detectors'];
						}
						if(array_key_exists('video_resolution', $telefon)) {
							$newTelefon->smartphone_video_resolution = $telefon['video_resolution'];
						}
						if(array_key_exists('video_shots', $telefon)) {
							$newTelefon->smartphone_video_shots = $telefon['video_shots'];
						}
						if(array_key_exists('lte', $telefon)) {
							$newTelefon->smartphone_lte = $telefon['lte'];
						}
						if(array_key_exists('battery', $telefon)) {
							$newTelefon->smartphone_battery = $telefon['battery'];
						}
						if(array_key_exists('battery_lifetime_music', $telefon)) {
							$newTelefon->smartphone_battery_lifetime_music = $telefon['battery_lifetime_music'];
						}
						if(array_key_exists('glass', $telefon)) {
							$newTelefon->smartphone_glass = $telefon['glass'];
						}
						if(array_key_exists('sales_beginning', $telefon)) {
							$newTelefon->smartphone_sales_beginning = $telefon['sales_beginning'];
						}
						if(array_key_exists('display_strength', $telefon)) {
							$newTelefon->smartphone_display_strength = $telefon['display_strength'];
						}
						if(array_key_exists('geo_tagging', $telefon)) {
							$newTelefon->smartphone_geo_tagging = $telefon['geo_tagging'];
						}
						if(array_key_exists('navigation', $telefon)) {
							$newTelefon->smartphone_navigation = $telefon['navigation'];
						}
						if(array_key_exists('gps', $telefon)) {
							$newTelefon->smartphone_gps = $telefon['gps'];
						}
						if(array_key_exists('profile', $telefon)) {
							$newTelefon->smartphone_profile = $telefon['profile'];
						}
						
						//brand
						if(!array_key_exists('brand', $telefon)) $telefon['brand'] = "Brendsiz"; 
						$brand = Brand::where('name','=', $telefon['brand']);
						if(!$brand->exists()) {
							$brand = new Brand();
							$brand->name = $telefon['brand'];
							$brand->save();
						} else {
							$brand = $brand->first();
						}
						$newTelefon->brand_id = $brand->id;
						$newTelefon->save();

						if(array_key_exists('url', $telefon)) {
							$newUrl = new TelephoneUrl();
							$newUrl->url = $telefon['url'];
							$newUrl->telephone_id = $newTelefon->id;
							$newUrl->save();
						}

						$added++;
					} else {
						$already_added++;
					}
				} else {
					$problem++;
				}				
			}
		} else {
			$problem++;
		}
		return compact('all','added','already_added','problem');
	}
		
}