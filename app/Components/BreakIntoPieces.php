<?php

namespace App\Components;

class BreakIntoPieces
{
	public static function getNumberOfWords($text){
		$result = 0;
		if($text){
			$text = preg_replace('/\s+/', ' ', trim($text));
			$words = explode(" ", $text);
			$result = count($words);
		}
		return $result;
	}

	public static function bisectionTextWord($text,$rows){

	}
	public static function bisectionTextPhrase($text,$rows){
		$array = self::explodePoint($text);
		$first = '';
		$second = '';

		foreach($array as $k => $v){
			if($k <= $rows){
				$first .= $v.'.';
			} else {
				if($v){	
					$second .= $v.'.';
				}
			}
		}
		$data = ['first'=> $first, 'second' => $second];
		return $data;
	}


	public static function explodePoint($text){
		$result = [];
		if($text){
			$text = preg_replace('/\s+/', ' ', trim($text));
			$phrase = explode(".", $text);
			$result = $phrase;
		}
		return $result;
	}
	public static function explodeSpace($text){
		$result = [];
		if($text){
			$text = preg_replace('/\s+/', ' ', trim($text));
			$result = explode(" ", $text);
			
		}
		return $result;
	}
}
