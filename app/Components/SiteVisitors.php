<?php


namespace App\Components;


use App\Models\Visitors;
use Illuminate\Support\Facades\File;


class SiteVisitors
{
    public static function count()
    {
        $visitors = Visitors::all();
        $count = 0;
        foreach ($visitors as $visitor){
            $count += $visitor->count;
        }
        return $count;
    }

    public static function monthly()
    {
        $this_month = date('Y-m');
        $visitors = Visitors::where('updated_at','like','%'.$this_month.'%')->get();
        $count = 0;
        foreach ($visitors as $visitor){
            $count +=$visitor->count;
        }
        return $count;
    }

    public static function daily(){

        $today = date('Y-m-d');

        $visitors = Visitors::where('updated_at','like','%'.$today.'%')->get();
        $count=0;
        foreach ($visitors as $visitor){
            $count +=$visitor->count;
        }
        return $count;
    }
}