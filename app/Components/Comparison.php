<?php


namespace App\Components;


use Illuminate\Support\Facades\Session;

class Comparison
{
    public static function setComparison($id,$url)
    {
        if($url == 'deposit') {
            $key = 'comparison-deposit';
            Session::remove ('comparison-credit','comparison-card-credit','comparison-card-debit');
        }
        if($url == 'credit'){
            $key = 'comparison-credit';
            Session::remove ('comparison-deposit','comparison-card-credit','comparison-card-debit');
        }
        if($url == 'card_credit'){
            $key = 'comparison-card-credit';
            Session::remove ('comparison-deposit','comparison-credit','comparison-card-debit');
        }
        if($url == 'card_debit'){
            $key = 'comparison-card-debit';
            Session::remove ('comparison-deposit','comparison-credit','comparison-card-credit');
        }
        $has = Session::has($key);
        if($has){
            $old = Session::get ($key);
        } else {
            $old = [];
        }

        $comparison = [$id];
        $arr = array_merge ($old,$comparison);
        Session::put ($key,$arr);

        return count($arr);
    }

    public static function removeComparison($id,$url)
    {
        if($url == 'deposit') {
            $key = 'comparison-deposit';
        }
        if($url == 'credit'){
            $key = 'comparison-credit';
        }
        if($url == 'card_credit'){
            $key = 'comparison-card-credit';
        }
        if($url == 'card_debit'){
            $key = 'comparison-card-debit';
        }
        $session = Session::get($key);
        if(isset($session)){
            $session = array_diff ($session,[$id]);
        }
        Session::put($key,$session);

        $count = count($session);
        if($count == 0){
            $count = '';
        }
        return $count;
    }

    public static function btn($buttun){
        $btn = '<a class="'.$buttun['class'].'"';
        foreach($buttun['attr'] as $k => $v){
            $btn.= $k.'='.'"'.$v.'"';
        }
        $btn.='style="'.$buttun['style'].'"';
        $btn.= '> <i class="fa fa-balance-scale btn-balance-icon" style="cursor:pointer;" aria-hidden="true"></i>';
        $btn.='</a>';
        echo $btn;
    }

    public static function count(){
        $session1 = Session::get('comparison-deposit');
        $session2 = Session::get('comparison-credit');
        $session3 = Session::get('comparison-card-credit');
        $session4 = Session::get('comparison-card-debit');
        if($session1) {
            $comparison = count($session1);
            $url = 'deposit';
        }
        if($session2) {
            $comparison = count($session2);
            $url = 'credit';
        }
        if($session3) {
            $comparison = count($session3);
            $url = 'card_credit';
        }
        if($session4) {
            $comparison = count($session4);
            $url = 'card_debit';
        }
        if(!$session1 and !$session2 and !$session3 and !$session4){
            $url = 'service';
            $comparison = '';
        }
        return ['url' => $url,'count' => $comparison];
    }
}