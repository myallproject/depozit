<?php 

namespace App\Components;

use App\Models\ExchangeRate;
use App\Models\Currency;
use App\Models\CurrencyAPI;
use App\Models\CBExchangeRate;
use App\Models\OtherExchangeRateBank;
use App\Models\Notification;

class BankExchangeRateApi {

	public static function psb(){
		$id = 9; //sanoat qurilish bank
		$api = 'http://uzpsb.uz/uz/exchange-rate/json';
		$get = curl_init($api);
        curl_setopt($get, CURLOPT_HEADER,true);
        curl_setopt($get, CURLOPT_NOBODY,true);
        curl_setopt($get, CURLOPT_RETURNTRANSFER ,true); 
        curl_exec($get);
        $code = curl_getinfo($get,CURLINFO_HTTP_CODE);
        if($code == 301){
        	$get_content = file_get_contents($api);
			$json = json_decode($get_content);
			if($json){
				foreach($json as $j){
					$rate = ExchangeRate::where('bank_id',$id)->where('code',$j->code)->first();
					if($rate){
						$rate->take = $j->buy_price;
						$rate->sale = $j->cell_price;
						$rate->updated_at = date('Y-m-d H:i:s');
						$rate->save();
					} else {
						$currency = Currency::where('name',$j->code)->first();
						if($currency){
							$rate = new ExchangeRate();
							$rate->currency = $currency->id;
							$rate->bank_id = $id;
							$rate->take = $j->buy_price;
							$rate->sale = $j->cell_price;
							$rate->code = $currency->name;
							$rate->save();
						}
					}
				}
			}
        }
	}

	public static function poytaxtBank() {
		$id = 23;
		$api = 'https://api.poytaxtbank.uz/currency';
		$options = [
			'http' => ['method'=>'GET', 'header' => 'Content-type: application/json', 'content' => false],
			'ssl'  => [ 'verify_peer'=>false, 'verify_peer_name'=>false, ],
		];
		$context = stream_context_create($options);
		$get_content = '';
		if(isset($api)){
			$get_content = file_get_contents($api, false, $context);
		}
		if($get_content){
			$json = json_decode($get_content,true);
			$local_array = $json['local'];
			foreach ($local_array as $lk => $lv) {
				$rate = ExchangeRate::where('bank_id',$id)->where('code',$lk)->first();
				$currency_id = Currency::where('name',$lk)->first();
				if(empty($rate)){
					if($currency_id){
						$rate = new ExchangeRate();
						$rate->currency = $currency_id->id;
						$rate->bank_id = $id;
						$rate->code = $lk;
						$rate->take = $lv[0]['buy'];
						$rate->sale = $lv[0]['sell'];
						$rate->save();
					}
				} else {
					$rate->take = $lv[0]['buy'];
					$rate->sale = $lv[0]['sell'];
					$rate->save();
				}
			}
		}
	}

	public static function tengeBank(){
		$id = 30;
		$api = 'http://195.158.11.180:8088/get-all';
		$get = curl_init($api);
        curl_setopt($get, CURLOPT_HEADER,true);
        curl_setopt($get, CURLOPT_NOBODY,true);
        curl_setopt($get, CURLOPT_RETURNTRANSFER ,true); 
        curl_exec($get);
        $code = curl_getinfo($get,CURLINFO_HTTP_CODE);
        if($code == 200){
			$get_content = file_get_contents($api,'');
			$json = json_decode($get_content,true);
			$new = [];
			foreach ($json as $key => $value) {
				if($value['operationType'] == 1){$type = 'sell';}
				if($value['operationType'] == 2){$type = 'buy';}
				$new[$value['codVal']][$type] = $value['rate'];
			}
			//dd($new);
			foreach($new as $k => $v)
			{
				$cb = CBExchangeRate::where('code',$k)->first();
				if($cb){
					$currency = Currency::where('name',$cb['key'])->first();

					if($currency){
						$rate = ExchangeRate::where(['bank_id'=> $id, 'code' => $cb['key']])->first();
						if(!$rate){
							$rate = new ExchangeRate();
							
							$rate->currency = $currency->id;
							
							$rate->bank_id = $id;
							
							$rate->code = $cb['key'];
							if(in_array('buy', $v)){
								$rate->take = $v['buy'];
							}
							if(in_array('sell', $v)){
								$rate->sale = $v['sell'];
							}
							
							$rate->save();
						} else {
							
							if(in_array('buy', $v)){
								$rate->take = $v['buy'];
							}
							if(in_array('sell', $v)){
								$rate->sale = $v['sell'];
							}
							
							$rate->save();
						}
					}
				}
			}
        }
	}

	public static function trastBank(){
		return 'trastBnak';
	} 

	protected static function file_post_contents($url, $data = false) {

		$options = [
			'http' => ['method'=>'POST', 'header' => 'Content-type: application/json', 'content' => $data],
			'ssl'  => [ 'verify_peer'=>false, 'verify_peer_name'=>false, ],
		];
		$context = stream_context_create($options);

		return file_get_contents($url, false, $context);
	}

	public static function parseHubCurrencyApi()
	{	
		$apis = CurrencyAPI::all();
		foreach($apis as $api){
			$api_array =  self::parseHubFileGetContent($api->api);
			$b_currencies = ExchangeRate::where('bank_id',$api->organization_id)->get();
			if(array_key_exists('currency', $api_array)){
				foreach($api_array['currency'] as $cr){
					if(array_key_exists('name',$cr)){
						/*if( $cr['name'] == '$'){
							$cr = array_merge($cr , ['name' => 'USD']);
						}if( $cr['name'] == '€'){
							$cr = array_merge($cr , ['name' => 'EUR']);
						}if( $cr['name'] == '£'){
							$cr = array_merge($cr , ['name' => 'GBP']);
						}if( $cr['name'] == '¥'){
							$cr = array_merge($cr , ['name' => 'JPY']);
						}*/
						$currency = ExchangeRate::where(['bank_id'=> $api->organization_id, 'code' => $cr['name']])->first();
						if(empty($currency)){
							$currency_id = Currency::where('name',$cr['name'])->first();
							if(!$currency_id){
								$currency_id = new Currency(); 
								$currency_id->name = $cr['name'];
								$currency_id->save();
							}
							$currency = new ExchangeRate();
							$currency->currency = $currency_id->id;
							$currency->bank_id = $api->organization_id;
							$currency->code = $cr['name'];		
						}

						$currency->take = preg_replace('/\s+/','',$cr['buy']);
						$currency->sale = preg_replace('/\s+/','',$cr['sell']);
						$currency->save();

						if(array_key_exists('atm_buy', $cr)){
							$type = 1;
							$other = OtherExchangeRateBank::where(['bank_id' => $api->organization_id, 'type_id' => $type, 'currency_id' => $currency->currency])->first();
							if(empty($other)){
								$other = new OtherExchangeRateBank();
								$other->bank_id = $api->organization_id;
								$other->type_id = $type;
								$other->currency_id = $currency->currency;
							}
							$other->take = preg_replace('/\s+/','',$cr['atm_buy']);
							$other->sale = preg_replace('/\s+/','',$cr['sell']);

							$other->save();
						}
					}
				}
			} else {
				$not = Notification::where('table_name', $api->getTable())->where('status', 1)->where('row_id',$api->id)->first();
				
				if(empty($not)){
					$not = new Notification();
				}
				$not->table_name = 'currency_a_p_i_s';
				$not->row_id = $api->id;
				$not->row_column = $api->api;
				$not->row_column_value = 'Please check this api';
				$not->save();
			}
			//atm
			/*if(array_key_exists('atm', $api_array)){
				$currency = ExchangeRate::where(['bank_id'=> $api->organization_id, 'code' => $api_array['atm']])->first();
				$type = 1;
				if($currency){
					$other = OtherExchangeRateBank::where(['bank_id' => $api->organization_id, 'type_id' => $type, 'currency_id' => $currency->currency])->first();
				if(empty($other)){
					$other = new OtherExchangeRateBank();
					$other->bank_id = $api->organization_id;
					$other->type_id = $type;
					$other->currency_id = $currency->currency;
				}
				$other->take = preg_replace('/\s+/','',$api_array['buy']);
				$other->sale = preg_replace('/\s+/','',$api_array['sell']);
				$other->save();
				}
			}*/
		}
	}

	public static function parseHubFileGetContent($api)
	{	$get_content = '';
		$arr = [];
		if(!empty($api)){
			$get_content = file_get_contents($api);
		}
		if($get_content){
		 	$gzdecode =  gzdecode($get_content);
			$arr = json_decode($gzdecode,true);
		}
		return  $arr;
	}

	public static function nbu()
	{
		$id = 10; 
		$api = 'https://nbu.uz/uz/exchange-rates/json/';
		$get = curl_init($api);
        curl_setopt($get, CURLOPT_HEADER,true);
        curl_setopt($get, CURLOPT_NOBODY,true);
        curl_setopt($get, CURLOPT_RETURNTRANSFER ,true); 
        curl_exec($get);
        $code = curl_getinfo($get,CURLINFO_HTTP_CODE);
       	if($code == 200){
       		$get_content = file_get_contents($api);
			$array = json_decode($get_content);
			foreach ($array as $row) {
				$rate = ExchangeRate::where('bank_id',$id)->where('code',$row->code)->first();
				$currency_id = Currency::where('name',$row->code)->first();
				if(empty($rate)){
					if($currency_id){
						$rate = new ExchangeRate();
						$rate->currency = $currency_id->id;
						$rate->bank_id = $id;
						$rate->code = $row->code;
						$rate->take = $row->nbu_buy_price;
						$rate->sale = $row->nbu_cell_price;
						$rate->save();
					}
				} else {
					$rate->take = $row->nbu_buy_price;
					$rate->sale = $row->nbu_cell_price;
					$rate->save();
				}
			}
       }
	}
}