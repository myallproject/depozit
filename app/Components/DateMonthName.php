<?php

namespace App\Components;

class DateMonthName {

    public static function updateDateTime($date) {
       $datetime = '';
       if($date){
           $day   = date('d',strtotime($date));
           $month = date('m',strtotime($date));
           $year  = date('Y',strtotime($date));
           $time  = date('H:i',strtotime($date));
           
           $month_name = self::month($month);
           $datetime = $day.'/'.$month_name.'/'.$year.' '.$time;
       } 
       return $datetime;
    } 
    public static function updateDateName($date) {
       $datename = '';
       if($date){
           $day   = date('d',strtotime($date));
           $month = date('m',strtotime($date));
           $year  = date('Y',strtotime($date));
           
           $month_name = self::month($month);
           $datename = $day.'/'.$month_name.'/'.$year;
       } 
       return $datename;
    }
    
    public static function month($month) {
        
        switch ($month) {
            case 1:
                return __('lang.month_name.january.short');
                break;
            case 2:
                return __('lang.month_name.february.short');
                break;
            case 3:
                return __('lang.month_name.march.short');
                break;
            case 4:
                return __('lang.month_name.april.short');
                break;
            case 5:
                return __('lang.month_name.may.short');
                break;
            case 6:
                return __('lang.month_name.june.short');
                break;
            case 7:
                return __('lang.month_name.july.short');
                break;
            case 8:
                return __('lang.month_name.august.short');
                break;
            case 9:
                return __('lang.month_name.september.short');
                break;
            case 10:
                return __('lang.month_name.october.short');
                break;
            case 11:
                return __('lang.month_name.november.short');
                break;
            case 12:
                return __('lang.month_name.december.short');
                break;
        }
    }
}