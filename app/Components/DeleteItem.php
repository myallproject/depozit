<?php 


namespace App\Components;


class DeleteItem {

	public static function model($model, $id)
	{
		$m = $model::find($id);

		if($m){
			$m->delete = 1;
			$m->save();
		}
		return redirect()->back();
	}
}