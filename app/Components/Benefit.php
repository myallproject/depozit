<?php
    /**
     * Created by PhpStorm.
     * User: Karimov SHarif
     * Date: 24.02.2019
     * Time: 16:56
     */
    
    namespace App\Components;
    
    
    use App\Models\Deposit;
    use App\Models\DateService;
    /*
     * 1, 5 year
     * 3 month
     * 4 day
     * id=9 month
     * id=8 year
     * id=10 day
     * */
    class Benefit
    {
        public static function benefit($id, $percent, $date_type = flase, $date_type_date, $r_price)
        {
            $allSum = 0;
            $deposit = Deposit::find($id);
            $dateS = DateService::find($deposit->deposit_date_type);
            $inDay = strstr($dateS->name_uz,'kun');
            $inMonth = strstr($dateS->name_uz,'oy');
            $inYear = strstr($dateS->name_uz,'yil');
            if(!$date_type or !$date_type_date){
                if($inMonth){
                    $date = str_replace('oy','',$dateS->name_uz);
                    if(strpos($date,',')){
                        list($o,$t) = explode(',', $date);
                        $date = $o.'.'.$t;
                    }
                    $perMonthPercent = $percent / 12;
                    $sumPerMonth = ((float)$perMonthPercent*(float)$r_price) / 100;
                    $allSum = (float)$date * $sumPerMonth;
                } 
                if($inYear){
                    $date = str_replace('yil','',$dateS->name_uz);
                    if(strpos($date,',')){
                        list($o,$t) = explode(',', $date);
                        $date = $o.'.'.$t;
                    }
                    $perYear = ($percent * (float)$r_price) /100;
                    $allSum = (float)$date * $perYear;
                }
                if($inDay){
                    $date = str_replace('kun','',$dateS->name_uz);
                    if(strpos($date,',')){
                        list($o,$t) = explode(',', $date);
                        $date = $o.'.'.$t;

                        $perDayPercent = $percent / 365;
                        $sumPerDay = ((float)$perDayPercent * (float)$r_price );
                        $allSum = $sumPerDay * (float)$date;
                    } 
                }
            } else {
                if($date_type == 9){
                    
                    $perMonthPercent = $percent / 12;
                    $sumPerMonth = ((float)$perMonthPercent*(float)$r_price) / 100;
                    $allSum = (float)$date_type_date * $sumPerMonth;
                } 
                if($date_type == 8){
                   
                    $perYear = ($percent * (float)$r_price) /100;
                    $allSum = (float)$date_type_date * $perYear;
                }

                if($date_type == 10){
                    $perDayPercent = $percent / 365;
                    $sumPerDay = ((float)$perDayPercent * (float)$r_price ) / 100;
                    $allSum = $sumPerDay * (float)$date_type_date;
                }
            }
            


            return $allSum;
        }
    }   
