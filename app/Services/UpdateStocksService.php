<?php

namespace App\Services;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Models\Stocks;
use App\Repositories\StocksRepository;

class UpdateStocksService
{
    const API_KEY = 'tUSd8dE28pgn';
    const PROJECT_KEY = 'tWKXJwqh0tbY';
    const URL = 'https://www.parsehub.com/';

    public $repository;

    public function __invoke()
    {
        $this->refreshTheDatabase();
    }

    public function __construct()
    {
        $this->repository = new StocksRepository;
    }

    /**
     * Requesting the data from the api
     *
     * @return object
     */

    public function requestDataFromApi()
    {
        $client = new Client([
            'base_uri' => self::URL,
            'timeout'  => 2.0,
        ]);
        try {
            $response = $client->request('GET', 'api/v2/projects/' . self::PROJECT_KEY . '/last_ready_run/data', [
                'query' => ['api_key' => self::API_KEY]
            ]);
        } catch (Exception $e) {
            Log::alert('Cant get data' . $e->getMessage());
            return [];
        }

        $response = json_decode($response->getBody());

        return $response;
    }

    public function refreshTheDatabase()
    {
        $stocks = $this->requestDataFromApi();
        $this->repository->refreshItems($stocks);
    }
}
