<?php

namespace App;

use App\Models\PhoneVerification;
use App\Models\Review;
use App\Models\UserRole;
use App\Models\UserOrganization;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id', 'name', 'email', 'phone', 'role_id', 'password', 'verified','facebook_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
    ];

    public function id(): int
    {
       return (int) $this->id;
    }

    public function getName(){
        return $this->name;
    }
    
    public function getLocalName()
    {
        if(!preg_match('/[А-Яа-яЁё]/u', $this->name) && App::getLocale() == 'ru'){
            return $this->latinToCyrilic($this->name);
        }
        return $this->name;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getGender()
    {
        $class = '';
        switch ($this->gender) {
            case 1:
                $class = 'fa-male';
                break;
            case 2:
                $class = 'fa-female';
                break;
            default:
                $class = 'fa-question';
                break;
        }
        return $class;
    }

    public function verification()
    {
        return $this->hasOne(PhoneVerification::class, 'user_id')->latest()->withDefault();
    }
	
    public function verifications()
    {
        return $this->hasOne(PhoneVerification::class, 'user_id');
    }

    public function phoneVerified()
    {
        return is_null($this->phone_verified_at) ? false : true;
    }

    public function isVerified(){
        return $this->verified;
    }
    
    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class, 'user_id');
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(UserRole::class,'role_id');
    }

    public function organization(): BelongsTo
    {
        return $this->belongsTo(UserOrganization::class,'id','user_id');
    }

    public function latinToCyrilic($latinString){
        $lat = [
            'dj','kh','yo','ts','ch','sh','yu','ya','ye','e',
            'Dj','Kh','Yo','Ts','Ch','Sh','Yu','Ya','Ye','E',
            'a','b','v','g','d','j','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','x',
            'A','B','V','G','D','J','Z','I','Y','K','L','M','N','O','P',
            'R','S','T','U','F','H','X'
        ];
        $cyr = [
            'ж','х','ё','ц','ч','ш','ю','я','е','е',
            'Ж','Х','Ё','Ц','Ч','Ш','Ю','Я','Е','Е',
            'а','б','в','г','д','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','ҳ','х',
            'А','Б','В','Г','Д','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Ҳ','Х',
        ];
        $latinString = str_replace($lat, $cyr, $latinString);

        return $latinString;
    }
}
