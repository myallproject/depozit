<?php

namespace App\Exceptions;

use Exception;

class NotFoundHttpException extends Exception
{
    public function report() {
    
    }
    
    public function render() {
    
    }
}
