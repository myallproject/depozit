<style>
    .new-width .td:nth-child(1),  .new-width .th:nth-child(1){
        width: 10% !important;
    }
    .new-width .td:nth-child(2),  .new-width .th:nth-child(2){
        width: 20% !important;
    }
     .new-width .td:nth-child(3),  .new-width .th:nth-child(3){
        width: 35% !important;
    }
     .new-width .td:nth-child(4),  .new-width .th:nth-child(4){
        width: 30% !important;
    }
</style>
<div class="table best-currency-table">
    <div class="thead">
        <div class="tr new-width">
            <div class="th pl-15">@lang('lang.currency')</div>
            <div class="th pl-15">@lang('lang.cb_course')</div>
            <div class="th pl-15">@lang('lang.take')</div>
            <div class="th pl-15">@lang('lang.sale')</div>
        </div>
    </div>
    <div class="tbody">
        <div class="tr new-width">
            <div class="td">
                <div class="font-800">USD</div>
            </div>
            <div class="td">
                <div> {{ number_format($cd_rate['usd']['rate'],2)}} <span style="font-size:50%">UZS</span></div>
                <div class="small">{{ $cd_rate['usd']['diff'] }} <span style="font-size:50%">UZS</span></div>
            </div>

            <div class="td" style="display: flex">
                @php
                    $usd_best_count = count($best_take_usd) + 1;
                    $bank_ids = [];
                    $bank_names = '';
                @endphp
                <div>
                    @if($usd_best_count == 0)
                        @if($usd_take['bank']['image'])
                            <img class="widget-bank-logo" src="/{{ $usd_take['bank']['image'] }}" />
                        @else
                            <img class="widget-bank-logo" src="/temp/images/bank.png" />
                        @endif
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>

                <div>
                    <div>{{ number_format($usd_take['take'],2) }} <span style="font-size:50%">UZS</span></div>
                    <div class="small">
                        <?php $bank_names .= $usd_take['bank']['name_uz']; ?>
                        
						@if($usd_best_count <= 3)
                         {{ $usd_take['bank']['name_uz']}}@if($usd_best_count)@foreach($best_take_usd as $best_usd_bank),&nbsp;{{ $best_usd_bank->bank->name_uz }}@endforeach @endif
                        @else

                        @foreach($best_take_usd as $best_usd_take_bank)
                            <?php $bank_ids[] = $best_usd_take_bank->bank->id;
                              $bank_names .=', '.$best_usd_take_bank->bank->name_uz; ?>
                        @endforeach
                            <?php //$bank_ids = implode('|', $bank_ids).'|'.$usd_take['id']; ?>
                            <div style="display: initial; position: relative; font-size: 13px !important">
                                   <a class='tooltip-down' data-tooltip="{{ $bank_names }}" {{-- href="{{ route('on-bank-exchange-rate',[
                                   'cr' => 2,
                                   'ac' => 'take',
                                   'id' => $bank_ids
                               ]) }}" --}}>@lang('lang.best_cr_bank',['count' => $usd_best_count])</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="td" style="display: flex">
                    @php
                        $usd_best_sale_count = count($best_sale_usd) + 1;
                        $bank_ids = [];
                    @endphp
                <div>
                    @if($usd_best_sale_count == 0)
                        @if($usd_sale['bank']['image'])
                            <img class="widget-bank-logo" src="/{{ $usd_sale['bank']['image'] }}" />
                        @else
                            <img class="widget-bank-logo" src="/temp/images/bank.png" />
                        @endif
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($usd_sale['sale'],2) }} <span style="font-size:50%">UZS</span></div>
                    <div class="small">
                         <?php $bank_names = '';  $bank_names .= $usd_sale['bank']['name_uz']; ?>
                        @if($usd_best_sale_count <= 3)
                        {{ $usd_sale['bank']['name_uz']}}@if($usd_best_sale_count)@foreach($best_sale_usd as $best_usd_sale_bank),&nbsp;{{ $best_usd_sale_bank->bank->name_uz }}@endforeach @endif
                        @else

                        @foreach($best_sale_usd as $best_usd_sale_bank)
                            <?php $bank_ids[] = $best_usd_sale_bank->bank->id;  $bank_names .=', '.$best_usd_sale_bank->bank->name_uz;  ?>
                        @endforeach
                            <?php //$bank_ids = implode('|', $bank_ids).'|'.$usd_sale['id']; ?>
                            <div style="display: initial; position: relative;font-size: 13px !important">
                               <a class='tooltip-down' data-tooltip="{{ $bank_names }}">@lang('lang.best_cr_bank',['count' => $usd_best_sale_count])</a>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
        <div class="tr new-width">
            <div class="td">
                <div class="font-800">RUB</div>
            </div>
            <div class="td">
                <div>{{ number_format($cd_rate['rub']['rate'],2) }} <span style="font-size:50%">UZS</span></div>
                <div class="small">{{ $cd_rate['rub']['diff'] }} <span style="font-size:50%">UZS</span></div>
            </div>
            <div class="td" style="display: flex">
                @php
                    $rub_best_count = count($best_take_rub) + 1;
                @endphp
                <div>
                    @if($rub_best_count == 0)
                       @if($rub_take['bank']['image'])
                            <img class="widget-bank-logo" src="/{{ $rub_take['bank']['image'] }}" />
                        @else
                            <img class="widget-bank-logo" src="/temp/images/bank.png" />
                        @endif
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>

                <div>
                    <div>{{ number_format($rub_take['take'],2) }} <span style="font-size:50%">UZS</span></div>
                    <div class="small">
                        <?php $bank_names = ''; $bank_names .= $rub_take['bank']['name_uz']; ?>
                        @if($rub_best_count <= 3)
                        {{ $rub_take['bank']['name_uz']}}@if($rub_best_count)@foreach($best_take_rub as $best_rub_take_bank),&nbsp;{{ $best_rub_take_bank->bank->name_uz }}@endforeach @endif
                        @else

                        @foreach($best_take_rub as $best_rub_take_bank)
                            <?php $bank_ids[] = $best_rub_take_bank->bank->id;
                            $bank_names .=', '.$best_rub_take_bank->bank->name_uz; ?>
                        @endforeach
                            <?php //$bank_ids = implode('|', $bank_ids).'|'.$rub_take['id']; ?>
                            <div style="display: initial; position: relative;font-size: 13px !important">
                               <a class='tooltip-down' data-tooltip="{{ $bank_names }}">@lang('lang.best_cr_bank',['count' => $rub_best_count])</a>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
            <div class="td" style="display: flex">
                 @php
                    $rub_best_count_sale = count($best_sale_rub) + 1;
                @endphp
                 <div>
                    @if($rub_best_count_sale == 0)
                       @if($rub_sale['bank']['image'])
                            <img class="widget-bank-logo" src="/{{ $rub_sale['bank']['image'] }}" />
                        @else
                            <img class="widget-bank-logo" src="/temp/images/bank.png" />
                        @endif
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>

                <div>
                    <div>{{ number_format($rub_sale['sale'],2) }} <span style="font-size:50%">UZS</span></div>
                    <div class="small">
                        <?php $bank_names = ''; $bank_names .= $rub_sale['bank']['name_uz']; ?>
                    @if($rub_best_count_sale <= 3)
                        {{ $rub_sale['bank']['name_uz']}}@if($rub_best_count_sale)@foreach($best_sale_rub as $best_rub_sale_bank),&nbsp;{{ $best_rub_sale_bank->bank->name_uz }}@endforeach @endif
                    @else

                    @foreach($best_sale_rub as $best_rub_sale_bank)
                        <?php $bank_ids[] = $best_rub_sale_bank->bank->id;
                        $bank_names .=', '.$best_rub_sale_bank->bank->name_uz; ?>
                    @endforeach
                        <?php // $bank_ids = implode('|', $bank_ids).'|'.$rub_sale['id'];  ?>
                       <div style="display: initial; position: relative;font-size: 13px !important">
                               <a class='tooltip-down' data-tooltip="{{ $bank_names }}">@lang('lang.best_cr_bank',['count' => $rub_best_count_sale])</a>
                        </div>
                    @endif
                </div>
                </div>

            </div>
        </div>
        <div class="tr new-width">
            <div class="td">
                <div class="font-800">EUR</div>
            </div>
            <div class="td">
                <div>{{ number_format($cd_rate['eur']['rate'],2) }} <span style="font-size:50%">UZS</span></div>
                <div class="small">{{ $cd_rate['eur']['diff'] }} <span style="font-size:50%">UZS</span></div>
            </div>
            <div class="td" style="display: flex">
                @php
                    $eur_best_count = count($best_take_eur) + 1;
                @endphp
                <div>
                    @if($eur_best_count == 0)
                       @if($eur_take['bank']['image'])
                            <img class="widget-bank-logo" src="/{{ $eur_take['bank']['image'] }}" />
                        @else
                            <img class="widget-bank-logo" src="/temp/images/bank.png" />
                        @endif
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>

                <div>
                    <div>{{ number_format($eur_take['take'],2) }} UZS</div>
                    <div class="small">
                        <?php $bank_names = ''; $bank_names .= $eur_take['bank']['name_uz']; ?>
                        @if($eur_best_count <= 3)
                        {{ $eur_take['bank']['name_uz']}}@if($eur_best_count)@foreach($best_take_eur as $best_take_eur_row),&nbsp;{{ $best_take_eur_row->bank->name_uz }}@endforeach @endif
                        @else

                        @foreach($best_take_eur as $best_take_eur_row)
                            <?php $bank_ids[] = $best_take_eur_row->bank->id;
                             $bank_names .=', '.$best_take_eur_row->bank->name_uz; ?>
                        @endforeach
                            <?php //$bank_ids = implode('|', $bank_ids).'|'.$eur_take['id'];  ?>
                           <div style="display: initial; position: relative;font-size: 13px !important">
                               <a class='tooltip-down' data-tooltip="{{ $bank_names }}">@lang('lang.best_cr_bank',['count' => $eur_best_count])</a>
                            </div>
                        @endif

                    </div>
                </div>

            </div>
            <div class="td" style="display: flex">
                 @php
                    $eur_best_countsa_sale = count($best_sale_eur) + 1;
                @endphp
                <div>
                    @if($eur_best_countsa_sale == 0)
                       @if($eur_sale['bank']['image'])
                            <img class="widget-bank-logo" src="/{{ $eur_sale['bank']['image'] }}" />
                        @else
                            <img class="widget-bank-logo" src="/temp/images/bank.png" />
                        @endif
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>



                <div>
                    <div>{{ number_format($eur_sale['sale'],2) }} UZS</div>

                    <div class="small">
                         <?php $bank_names = ''; $bank_names .= $eur_sale['bank']['name_uz']; ?>
                        @if($eur_best_countsa_sale <= 3)
                        {{ $eur_sale['bank']['name_uz']}}@if($eur_best_countsa_sale)@foreach($best_sale_eur as $best_sale_eur_row),&nbsp;{{ $best_sale_eur_row->bank->name_uz }}@endforeach @endif
                        @else
                        @foreach($best_sale_eur as $best_sale_eur_row)
                            <?php
                            $bank_ids[] = $best_sale_eur_row->bank->id;
                            $bank_names .=','.$best_sale_eur_row->bank->name_uz;
                             ?>
                        @endforeach
                            <?php //$bank_ids = implode('|', $bank_ids).'|'.$eur_sale['id'];  ?>
                            <div style="display: initial; position: relative;font-size: 13px !important">
                               <a class='tooltip-down' data-tooltip="{{ $bank_names }}">@lang('lang.best_cr_bank',['count' => $eur_best_countsa_sale])</a>
                            </div>
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>