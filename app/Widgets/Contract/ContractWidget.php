<?php
/**
 * Created by PhpStorm.
 * User: Physics
 * Date: 27.10.2019
 * Time: 15:48
 */

namespace App\Widgets\Contract;


interface ContractWidget
{

    public function execute();
}

