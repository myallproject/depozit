<?php
/**
 * Created by PhpStorm.
 * User: Physics
 * Date: 27.10.2019
 * Time: 16:07
 */

namespace App\Widgets;


use App\Widgets\Contract\ContractWidget;
use App\Models\ExchangeRate;
use App\Components\ExchangeRateClass;

/*  '2' => 'USD',
	'3' => 'RUB',
    '4' => 'EUR'*/

class BestCurrencyWidget implements ContractWidget
{
    public function execute()
    {
        // TODO: Implement execute() method.

        $usd_rate_exchange_take =  ExchangeRate::where('deleted',0)->where('currency','=','2')->orderBy('take','DESC')->first();
        $rub_rate_exchange_take =  ExchangeRate::where('deleted',0)->where('currency','=','3')->orderBy('take','DESC')->first();
        $eur_rate_exchange_take =  ExchangeRate::where('deleted',0)->where('currency','=','4')->orderBy('take','DESC')->first();
        
        $usd_rate_exchange_sale =  ExchangeRate::where('deleted',0)->where('currency','=','2')->orderBy('sale','ASC')->first();
        $rub_rate_exchange_sale =  ExchangeRate::where('deleted',0)->where('currency','=','3')->orderBy('sale','ASC')->first();
        $eur_rate_exchange_sale =  ExchangeRate::where('deleted',0)->where('currency','=','4')->orderBy('sale','ASC')->first();

        /*take*/
        $best_take_usd = ExchangeRate::where('deleted',0)->where('id','!=',$usd_rate_exchange_take['id'])
			->where('bank_id','!=',$usd_rate_exchange_take['bank_id'])
			->where('currency','=','2')
			->where('take','LIKE','%'.$usd_rate_exchange_take['take'].'%')->get();
        $best_take_rub = ExchangeRate::where('deleted',0)->where('id','!=',$rub_rate_exchange_take['id'])
			->where('bank_id','!=',$rub_rate_exchange_take['bank_id'])
			->where('currency','=','3')
			->where('take','LIKE','%'.$rub_rate_exchange_take['take'].'%')->get();
        $best_take_eur = ExchangeRate::where('deleted',0)->where('id','!=',$eur_rate_exchange_take['id'])
			->where('bank_id','!=',$eur_rate_exchange_take['bank_id'])
			->where('currency','=','4')
			->where('take','LIKE','%'.$eur_rate_exchange_take['take'].'%')->get();
       
       /*sale*/
        $best_sale_usd = ExchangeRate::where('deleted',0)->where('id','!=',$usd_rate_exchange_sale['id'])
			->where('bank_id','!=',$usd_rate_exchange_sale['bank_id'])
			->where('currency','=','2')->where('sale','LIKE','%'.$usd_rate_exchange_sale['sale'].'%')->get();
        $best_sale_rub = ExchangeRate::where('deleted',0)->where('id','!=',$rub_rate_exchange_sale['id'])
			->where('bank_id','!=',$rub_rate_exchange_sale['bank_id'])
			->where('currency','=','3')->where('sale','LIKE','%'.$rub_rate_exchange_sale['sale'].'%')->get();
        $best_sale_eur = ExchangeRate::where('deleted',0)->where('id','!=',$eur_rate_exchange_sale['id'])
			->where('bank_id','!=',$eur_rate_exchange_sale['bank_id'])
			->where('currency','=','4')->where('sale','LIKE','%'.$eur_rate_exchange_sale['sale'].'%')->get();
       

		//dd($best_take_usd, $usd_rate_exchange_take['take'], $best_take_rub, $best_take_eur, $best_sale_usd, $best_sale_rub, $best_sale_eur );
        
		$cb_rate = [
        	'usd' => ExchangeRateClass::selectOne('USD'),
        	'rub' => ExchangeRateClass::selectOne('RUB'),
        	'eur' => ExchangeRateClass::selectOne('EUR')
        ];
		
		
        return view('Widgets::best_currency',array(
        	'usd_take' => $usd_rate_exchange_take,
        	'rub_take' => $rub_rate_exchange_take,
        	'eur_take' => $eur_rate_exchange_take,
        	'usd_sale' => $usd_rate_exchange_sale,
        	'rub_sale' => $rub_rate_exchange_sale,
        	'eur_sale' => $eur_rate_exchange_sale,
        	'cd_rate'  => $cb_rate,
            'best_take_usd' => $best_take_usd,
            'best_sale_usd' => $best_sale_usd,
            'best_take_rub' => $best_take_rub,
            'best_sale_rub' => $best_sale_rub,
            'best_take_eur' => $best_take_eur,
            'best_sale_eur' => $best_sale_eur,
        ));
    }
}