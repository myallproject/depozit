<?php


namespace App\Imports;

use App\Models\Deposit;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class BankDepositImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        $deposits = new Deposit([
            'bank_id' => '',
            'slug' => str_slug($row['name']." ".rand(10000,99999),"-"),
            'status' => 0,
            'name_uz' => $row['name'],
            'deposit_type_id' => $row['deposit_type_id'],
            'deposit_date' => $row['deposit_date'],
            'deposit_date_type' => $row['deposit_date_type'],
            'deposit_percent' => $row['deposit_percent'],
            'min_sum' => $row['min_sum'],
            'type_sum' => $row['type_row'],
            'currency' => $row['currency'],
            'account_fill' => $row['account_fill'],
            'partly_take' => $row['partly_take'],
            'percents_capitalization' => $row['percents_capitalization'],
            'percent_paid_period' => $row['percent_paid_period'],
            'close_before_date' => $row['close_before_date'],
            'link' => $row['link'],
            'text_ru' => $row['text_ru'],
            'text_uz' => $row['text_uz'],
            'region_id' => $row['region_id'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        return $deposits;
    }

    public function headingRow(): int
    {
        return 1;
    }
}