<?php

namespace App\Imports;

use App\Models\BankOffices;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class BankBranchesImport implements ToModel, WithHeadingRow
{
	 public function model(array $row)
	{
		$address_arr = ['uz'=>'','ru'=>''];
		$address_arr=[
			'uz' => $row['address_uz'],
			'ru' => $row['address_ru']
		];
		$address_json = json_encode($address_arr);

		$name_arr = ['uz'=>'','ru'=>''];
		$name_arr=[
			'uz' => $row['name_uz'],
			'ru' => $row['name_ru']
		];
		$name_json = json_encode($name_arr);

		$lat_lang = $row['lat_long'];
		$lat = '';
		$lang = '';
		if($lat_lang){
			$lat_lang = preg_replace('/\s+/','',$lat_lang);
			$lat_lang_arr = explode(',', $lat_lang);
				foreach($lat_lang_arr as $k => $v){
					if($k == 0)
						$lat = $v;
					if($k == 1)
						$lang = $v;
				}
		}

		$branches = new BankOffices([
			'bank_id' => $row['bank_id'],
			'region_id' => $row['region_id'],
			'mfo' => $row['mfo'],
			'stir' => $row['stir'],
			'name' => $name_json,
			'address' => $address_json,
			'lat' => $lat,
			'lang' => $lang,
		]);

		return $branches;
	}
	
	public function headingRow(): int
    {
        return 1;
    }
	
}
?>