<?php


namespace App\CacheData;

use App\Models\ExchangeRate;
use Carbon\Carbon;

class ExchangeRateCache {

	CONST CACHE_KEY = 'EXCHANGE_RATE';

	public function all($orderBy)
	{
		$key = "all.{$orderBy}";
		
		$cacheKey = $this->getCacheKey($key);
		/*
		* addMinutes()
		* addDays()
		* addHours()
		*/
		return cache()->remember($cacheKey, Carbon::now()->addMinutes(5), function() use($orderBy){
			return ExchangeRate::orderBy($orderBy)->get();
		});
		
	}

	public function getId($id)
	{
		$key = "getId.{$id}";
		
		$cacheKey = $this->getCacheKey($key);

		return cache()->remember($cacheKey, Carbon::now()->addMinutes(5), function() use($id){
			return ExchangeRate::find($id);
		});
	}

	public function getCacheKey($key)
	{
		$key = strtoupper($key);
		return self::CACHE_KEY.".$key";
	}

	public function getWhere($where)
	{

	}

}
