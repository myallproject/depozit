<?php

namespace App\Spider;

use Goutte\Client;

class SpiderCore
{
    public function getData()
    {
        $client = new Client();
        $crawler = $client->request('GET', 'https://uzse.uz/isu_infos');
        return $crawler->filter('.table');
    }
}
