<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class WidgetServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::singleton('widget',function(){
            return new \App\Widgets\Widget();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('widget',function($name){
           return "<?php echo app('widget')->show($name); ?>";
        });

        $this->loadViewsFrom(app_path().'/Widgets/view','Widgets');
    }

}
