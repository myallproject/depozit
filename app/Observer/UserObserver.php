<?php

namespace App\Observer;

use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Cache;

class UserObserver 
{
	public function saved(User $user)
	{
		Cache::put("user.{$user->id}",$user, 60);
	}
	
	public function deleted(User $user)
    {
        Cache::forget("user.{$user->id}");
    }

    public function restored(User $user)
    {
        Cache::put("user.{$user->id}", $user, 60);
    }

    public function retrieved(User $user)
    {
        Cache::add("user.{$user->id}", $user, 60);
    }
    
}

?>