<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() and (Auth::user()->role_id == 2 or Auth::user()->role_id == 1 or Auth::user()->role_id == 3 or Auth::user()->role_id == 10) ) {
            return $next($request);
        }
        return redirect()->route('home',App::getLocale());
    }
}
