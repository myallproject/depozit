<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Root
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(Auth::check() and (Auth::user()->role_id == 10) ) {
            return $next($request);
        }
        return back()->with(['msg-root-validate'=>'You have no right!']);
    }
}
