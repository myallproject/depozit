<?php
    
    namespace App\Http\Middleware;
    
    use Closure;
    use Carbon\Carbon;
    
    use Illuminate\Support\Facades\App;
    use Illuminate\Support\Facades\Session;
    use Illuminate\Support\Facades\Config;
    use Illuminate\Support\Facades\Route;
    
    class Localization
    {
        public function handle($request, Closure $next)
        {
            if($request->segment(1)){
                if(in_array($request->segment(1), Config::get('app.available_locales'))){
                    Session::put('locale',$request->segment(1));
                } else {
                    Session::put('locale','uz');
                }
                
                $lang = Session::get('locale');
                
                if(Session::has('locale')){
                    App::setLocale($lang);
                    Carbon::setLocale($lang);
                } else {
                    App::setLocale($request->segment(1));
                    Carbon::setLocale($request->segment(1));
                } 

            } else {
                $lang = Session::get('locale');
                if(Session::has('locale')){
                    App::setLocale($lang);
                    Carbon::setLocale($lang);
                } else {
                    App::setLocale('uz');
                    Carbon::setLocale('uz');
                } 
            }
          return $next($request);

        }
    }
