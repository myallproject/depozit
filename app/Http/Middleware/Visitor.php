<?php

namespace App\Http\Middleware;

use App\Models\Visitors;
use Closure;
use Illuminate\Support\Facades\File;

class Visitor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        $data[$ipaddress] = [
                'count' => 1,
                'time' => date("H:i:s")
        ];
        $dateMonth = date('m');
        $dateDay = date('d');
        $dateYear = date('Y');
        $path = '../storage/visitors/'.$dateYear.'/'.$dateMonth;
        if(File::exists($path.'/'.$dateDay.'.json')){
            $in_file = utf8_encode(File::get($path.'/'.$dateDay.'.json'));
            $in_file_array = json_decode($in_file, true);
            
            if(!$in_file_array){$in_file_array = [];}
            
            if(array_key_exists($ipaddress, $in_file_array)){
                $old_time = strtotime($in_file_array[$ipaddress]['time']);
                $new_time = strtotime(date("H:i:s"));

                $diff_time = (time() - $old_time) / 60;
                if( $diff_time >= 50 ){        
                    $in_file_array[$ipaddress]['count'] +=1; 
                    $in_file_array[$ipaddress]['time'] = date("H:i:s"); 
                    $new_json = json_encode($in_file_array);
                    File::put($path.'/'.$dateDay.'.json',$new_json);
                }
            }  else {
                $new_array = array_merge($data,$in_file_array);
                $new_json = json_encode($new_array);
                File::put($path.'/'.$dateDay.'.json',$new_json);
            }
        }
        if(!File::exists($path.'/'.$dateDay.'.json')) {
            $json = json_encode($data);
            File::makeDirectory($path,0755,true,true);
            File::put($path.'/'.$dateDay.'.json',$json);
        }


       /* $today = date('Y-m-d');
        $t_visitor = Visitors::where('ip',$ipaddress)->where('updated_at','like','%'.$today.'%')->first();

        if($t_visitor)
        {
            $update_time = strtotime($t_visitor->updated_at);
            $now = strtotime(date('Y-m-d H:i:s'));
            $check_time = date('i',mktime($now - $update_time));
            
            if($check_time > 50){
                $t_visitor->count += 1;
                $t_visitor->save();
            }

        } else {
            $newClient = new Visitors();
            $newClient->ip = $ipaddress;
            $newClient->save();
        }*/
       /* $v = Visitors::all();
        foreach ($v as $r){
            if($r->count == 0){
                $r->count =1;
                $r->save();
            }
        }*/
        return $next($request);
    }
}