<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class BankProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() and (Auth::user()->role_id == 10 or Auth::user()->role_id == 11) ) {
            return $next($request);
        }
        return redirect()->route('home',App::getLocale());
    }
}
