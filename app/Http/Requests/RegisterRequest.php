<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'phone' => 'required|digits:12|numeric|unique:users,phone',
            'email' => 'nullable|email|unique:users,email',
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ];
    }
}
