<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_id' => 'required|not_in:0',
            'assessment' => 'required',
            'service_id' => 'required|not_in:0'
            //'region_id' => 'required|not_in:0',
        ];
    }

    public function messages()
    {
        return[
            'bank_id.*' => 'Iltimos Bankni tanlang!',
            'assessment.*' => 'Iltimos bahoni tanlang!',
            'service_id.*' => 'Iltimos xizmat turini tanlang!'
            //'region_id.*' => 'Iltimos Hududni tanlang',
        ];
    }
}
