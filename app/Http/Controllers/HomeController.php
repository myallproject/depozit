<?php

namespace App\Http\Controllers;

use App\Components\Comparison;
use App\Components\ExchangeRateClass;
use App\Models\Bank;
use App\Models\BankOffices;
use App\Models\CBExchangeRate;
use App\Models\Comment;
use App\Models\Contact;
use App\Models\Credit;
use App\Models\CreditCards;
use App\Models\CreditType;
use App\Models\Currency;
use App\Models\DataUpdateTime;
use App\Models\DebitCards;
use App\Models\Deposit;
use App\Models\Documents;
use App\Models\ExchangeRate;
use App\Models\News;
use App\Models\NewsCategory;
use App\Models\Notification;
use App\Models\PagesMetaTags;
use App\Models\Ranking;
use App\Models\RateExchangeType;
use App\Models\Region;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

define('USD_ID', Config::get('global.currency.USD'));
define('BANK', Config::get('global.organization.bank'));
define('OTHER_ORGANIZATION', Config::get('global.organization.other'));

class HomeController extends Controller {

	/* public function __construct()
		    {
		        $this->middleware('auth');
	*/

	public function index() {
		$ids = [1, 3, 8, 10, 12]; //id for online credits
		$meta_tags = PagesMetaTags::where('page', config('global.pages.home'))->where('parent_id', 0)->get();

		$banks = Bank::where('organization_id', BANK)->get();
		$organizations = Bank::where('organization_id', OTHER_ORGANIZATION)->get();

		$deposits = Deposit::orderBy('deposit_percent', 'DESC')->where('status', 0)->limit(3)->get();
		$online_deposits = Deposit::orderBy('deposit_percent', 'DESC')->where('status', 0)->where('type_open', 'LIKE', '%' . '1' . '%')->limit(3)->get();

		$deposits_usd = Deposit::orderBy('deposit_percent', 'DESC')->where(['currency' => USD_ID, 'status' => 0])->limit(3)->get();
		$online_deposits_usd = Deposit::orderBy('deposit_percent', 'DESC')->where(['currency' => USD_ID, 'status' => 0])->where('type_open', 'LIKE', '%' . '1' . '%')->limit(3)->get();
		//dd($deposits_usd, USD_ID);
		$credits_istemol = Credit::where('type_credit_id', 1)->where('status', 1)->orderBy('percent', 'ASC')->limit(3)->get();
		$online_credits_istemol = Credit::where('type_credit_id', 1)->where('status', 1)->whereIn('credit_issuing_form_id', $ids)->orderBy('percent', 'ASC')->limit(3)->get();

		$credits_auto = Credit::where('type_credit_id', 2)->where('status', 1)->orderBy('percent', 'ASC')->limit(3)->get();
		$online_credits_auto = Credit::where('type_credit_id', 2)->where('status', 1)->whereIn('credit_issuing_form_id', $ids)->orderBy('percent', 'ASC')->limit(3)->get();

		$credits_ipoteka = Credit::where('type_credit_id', 3)->where('status', 1)->orderBy('percent', 'ASC')->limit(3)->get();
		$online_credits_ipoteka = Credit::where('type_credit_id', 3)->where('status', 1)->whereIn('credit_issuing_form_id', $ids)->orderBy('percent', 'ASC')->limit(3)->get();

		$credits_micro = Credit::where('type_credit_id', 4)->where('status', 1)->orderBy('percent', 'ASC')->limit(3)->get();
		$online_credits_micro = Credit::where('type_credit_id', 4)->where('status', 1)->whereIn('credit_issuing_form_id', $ids)->orderBy('percent', 'ASC')->limit(3)->get();

		$credits_talim = Credit::where('type_credit_id', 5)->where('status', 1)->orderBy('percent', 'ASC')->limit(3)->get();
		$online_credits_talim = Credit::where('type_credit_id', 5)->where('status', 1)->whereIn('credit_issuing_form_id', $ids)->orderBy('percent', 'ASC')->limit(3)->get();

		$debet_cards = DebitCards::where('tuluv_komissia', '0%')->inRandomOrder()->limit(3)->get();
		$delivered_debet_cards = DebitCards::where([
			['tuluv_komissia', '0%'],
			['uyga_yetkazish', 1],
		])->inRandomOrder()->limit(3)->get();

		$exchange_rate = CBExchangeRate::whereIn('key', ['USD', 'EUR', 'RUB'])->get();
		$best_rate_update_time = DataUpdateTime::where('service_id', 6)->first();

		$news = News::orderBy('created_at', 'DESC')->limit(4)->get();
		return view('home', compact(
			'exchange_rate',
			'deposits',
			'deposits_usd',
			'credits_istemol',
			'credits_auto',
			'credits_ipoteka',
			'credits_micro',
			'credits_talim',
			'online_deposits',
			'online_deposits_usd',
			'online_credits_istemol',
			'online_credits_auto',
			'online_credits_ipoteka',
			'online_credits_micro',
			'online_credits_talim',
			'banks',
			'news',
			'best_rate_update_time',
			'meta_tags',
			'organizations',
			'debet_cards',
			'delivered_debet_cards'
		));
	}

	public function contactUs() {
		$meta_tags = PagesMetaTags::where('page', config('global.pages.contact'))->where('parent_id', 0)->get();
		return view('frontend.pages.contact', compact('meta_tags'));
	}

	public function contact(Request $request) {

		$request->validate([
			'name' => 'required|string|max:256',
			'phone' => 'required|max:13',
			'email' => 'required|email',
			'text' => 'required|max:1000',
			'captcha' => 'required|captcha',
		], ['captcha.captcha' => 'Invalid Captcha Code']);

		$model = new Contact();
		$model->name = $request->name;
		$model->phone = $request->phone;
		$model->email = $request->email;
		$model->text = $request->text;

		$model->save();
		$msg = ['success' => __('lang.text_success_contact')];
		return redirect()->back()->with($msg);
	}

	public function captchaRefresh() {
		return response()->json(['captcha' => captcha_img()]);
	}

	public function exchangeRate() {
		$meta_tags = PagesMetaTags::where('page', config('global.pages.cb_rate'))->where('parent_id', 0)->get();
		$cb_rate = CBExchangeRate::all();
		$best_rate_update_time = DataUpdateTime::where('service_id', 6)->first();
		return view('frontend.pages.exchange-rate', compact('cb_rate', 'best_rate_update_time', 'meta_tags'));
	}

	public function bankExchangeRate(Request $request) {
		$banks = Bank::all();
		$currencies = Currency::all();
		$best_rate_update_time = DataUpdateTime::where('service_id', 6)->first();
		$currency = 2;
		$action = 'take';
		$quantum = 100;

		if ($request->input('cr')) {
			$currency = $request->input('cr');
		}

		if ($request->input('quantum')) {
			$quantum = $request->input('quantum');
		}
		$orderBy = 'DESC';
		if ($request->input('ac') == 'take') {
			$action = $request->input('ac');
			$orderBy = 'DESC';
		}
		if ($request->input('ac') == 'sale') {
			$action = $request->input('ac');
			$orderBy = 'ASC';
		}

		$exchange = ExchangeRate::where('deleted', 0)->where('currency', $currency);
		if ($request->input('id')) {
			$ids = explode('|', $request->input('id'));
			$exchange = $exchange->whereIn('bank_id', $ids);
		}

		$exchange = $exchange->orderBy($action, $orderBy);

		$exchange = $exchange->paginate(3);

		$data = [
			'error' => '',
			'exchange' => $exchange,
		];
		$currencyName = Currency::find($currency);
		$otherExchangeType = RateExchangeType::all();
		$meta_tags = PagesMetaTags::where('page', config('global.pages.bank_rate'))->where('parent_id', 0)->get();
		return view('frontend.pages.on-banks-exchange', compact(
			'exchange',
			'banks',
			'currencies',
			'best_rate_update_time',
			'currencyName',
			'action',
			'data',
			'quantum',
			'otherExchangeType',
			'meta_tags'
		));
	}

	public function exchangeAjaxContent(Request $request) {
		$validate = Validator::make($request->all(), [
			'quantum' => 'required|numeric',
		]);
		$otherExchangeType = RateExchangeType::all();
		if ($validate->fails()) {
			return response()->json(['error' => $validate->errors()]);
		} else {
			$orderBy = 'DESC';
			if ($request->input('action') == 'take') {
				$orderBy = 'DESC';
			}
			if ($request->input('action') == 'sale') {
				$orderBy = 'ASC';
			}
			$exchange = ExchangeRate::where('deleted', 0)->where('currency', $request->input('currency'));

			$exchange = $exchange->orderBy($request->input('action'), $orderBy);

			$exchange = $exchange->paginate(3);

			$data = [
				'error' => '',
				'exchange' => $exchange,
			];
			$action = $request->input('action');
			$quantum = $request->input('quantum');
			$currencyName = Currency::find($request->input('currency'));
			return Response::json(view('frontend.pages.exchange-ajax-content', compact(
				'currencyName',
				'action',
				'data',
				'quantum',
				'otherExchangeType'
			))->render());
		}

	}

	public function aboutUs() {
		$meta_tags = PagesMetaTags::where('page', config('global.pages.about_us'))->where('parent_id', 0)->get();
		$creditsT = CreditType::all();
		return view('frontend.pages.about-us', compact('creditsT', 'meta_tags'));
	}

	public function comments() {
		//$meta_tags = PagesMetaTags::where('page',config('global.pages.bank_rate'))->where('parent_id',0)->get();
		$comments = Comment::where('status', 1)->paginate(10);
		return view('frontend.pages.comments', compact(
			'comments'
		));
	}

	public function comment(Request $request) {
		$model = new Comment();

		$model->name = $request->name;
		$model->text = $request->text;
		$model->status = 1;
		$model->save();

		return redirect()->back()->with(['success' => true]);
	}

	public function moreComment() {
		// $meta_tags = PagesMetaTags::where('page',config('global.pages.bank_rate'))->where('parent_id',0)->get();
		$comments = Comment::all();

		return view('frontend.pages.more-comments', compact('comments'))->render();
	}

	public function addComparison(Request $request) {
		$comp = Comparison::setComparison($request->id, $request->url_type);
		return response()->json($comp);
	}

	public function removeComparison(Request $request) {
		$comp = Comparison::removeComparison($request->id, $request->url_type);
		return response()->json($comp);
	}

	public function listComparison() {
		$deposits = [];
		$credits = [];
		$debit_cards = [];
		$credit_cards = [];
		$url = '';
		$sessiond = Session::get('comparison-deposit');
		if ($sessiond and count($sessiond) != 0) {
			foreach ($sessiond as $k => $v) {
				$deposits[] = Deposit::find($v);
			}
			$url = 'deposit';
		}
		$sessionc = Session::get('comparison-credit');
		if ($sessionc and count($sessionc) != 0) {
			foreach ($sessionc as $k => $v) {
				$credits[] = Credit::find($v);
			}
			$url = 'credit';
		}
		$session_credit = Session::get('comparison-card-credit');
		if ($session_credit and count($session_credit) != 0) {
			foreach ($session_credit as $k => $v) {
				$credit_cards[] = CreditCards::find($v);
			}
			$url = 'card_credit';
		}
		$session_debit = Session::get('comparison-card-debit');
		if ($session_debit and count($session_debit) != 0) {
			foreach ($session_debit as $k => $v) {
				$debit_cards[] = DebitCards::find($v);
			}
			$url = 'card_debit';
		}
		$meta_tags = PagesMetaTags::where('page', config('global.pages.comparison'))->where('parent_id', 0)->get();
		return view('frontend.pages.comparison', compact(
			'deposits',
			'credits',
			'url',
			'debit_cards',
			'credit_cards',
			'meta_tags'
		));
	}

	public function infoBank($lang = false, $slug) {

		$bank = Bank::where('slug', $slug)->first();
		$deposits = $bank->deposits->where('status', '0')->count();

		$credits = $bank->credits;
		$consumer_credits = $credits->where('type_credit_id', 1)->where('status', '1')->count();
		$auto_credits = $credits->where('type_credit_id', 2)->where('status', '1')->count();
		$mortgage_credits = $credits->where('type_credit_id', 3)->where('status', '1')->count();
		$micro_credits = $credits->where('type_credit_id', 4)->where('status', '1')->count();
		$education_credits = $credits->where('type_credit_id', 5)->where('status', '1')->count();
		$overdraft_credits = $credits->where('type_credit_id', 6)->where('status', '1')->count();
		$credits = $credits->where('status', '1')->count();

		$debit_cards = $bank->debit_cards->where('status', '1')->count();
		$credit_cards = $bank->credit_cards->where('status', '1')->count();
		$cards = $debit_cards + $credit_cards;

		$reviews = $bank->reviews->count();

		$meta_tags = PagesMetaTags::where('page', config('global.pages.bank'))->where('parent_id', 0)->get();

		return view('frontend.banks.bank', compact(
			'bank',
			'deposits',
			'credits',
			'consumer_credits',
			'auto_credits',
			'mortgage_credits',
			'micro_credits',
			'education_credits',
			'cards',
			'debit_cards',
			'credit_cards',
			'meta_tags',
			'overdraft_credits',
			'reviews'
		));
	}

	public function bankDeposits($lang = false, $slug) {

		$bank = Bank::where('slug', $slug)->first();
		$meta_tags = PagesMetaTags::where('page', config('global.pages.bank'))->where('parent_id', 0)->get();
		return view('frontend.banks.deposits', compact('bank', 'meta_tags'));
	}

	public function banks() {
		$ranking = new Ranking;
		$banks = $ranking->rankedBanks();
		$meta_tags = PagesMetaTags::where('page', config('global.pages.banks'))->where('parent_id', 0)->get();
		return view('frontend.banks.banks', compact('banks', 'meta_tags'));
	}

	public function loader() {
		return view('frontend.components.loader');
	}

	public function news(Request $request) {
		$new = News::orderBy('created_at', 'DESC')->first();
		$news = News::where('id', '!=', $new->id)->orderBy('created_at', 'DESC')->paginate(4)->onEachSide(2);

		if ($request->id) {
			$new = News::find($request->id);
			$news = News::where('id', '!=', $request->id)->orderBy('created_at', 'DESC')->paginate(4)->onEachSide(2);
		}
		if ($request->category) {
			$new = News::where('new_category_id', 'LIKE', '%' . $request->category . '%')->orderBy('created_at', 'DESC')->first();
			$news = News::where('new_category_id', 'LIKE', '%' . $request->category . '%')->where('id', '!=', $new->id)->orderBy('created_at', 'DESC')->paginate(4)->onEachSide(2);
		}
		$categories = [];

		$newCategory = NewsCategory::all();
		$categories = [];
		foreach ($newCategory as $row) {
			$new_cat = News::where('new_category_id', 'LIKE', '%' . $row->id . '%')->get();
			if (count($new_cat) > 0) {
				$categories[] = NewsCategory::find($row->id);
			}
		}
		$new->views += 1;
		$new->save();

		$meta_tags = PagesMetaTags::where('page', config('global.pages.news'))->where('parent_id', 0)->get();
		return view('frontend.pages.news', compact('new', 'news', 'categories', 'meta_tags'));
	}

	public function newsAll(Request $request) {
		$news = News::paginate(12)->onEachSide(2);

		if ($request->category) {
			$news = News::where('new_category_id', 'LIKE', '%' . $request->category . '%')->paginate(12)->onEachSide(2);
		}

		$categories = NewsCategory::all();
		$meta_tags = PagesMetaTags::where('page', config('global.pages.all_news'))->where('parent_id', 0)->get();
		return view('frontend.pages.news-all', compact('news', 'categories', 'meta_tags'));
	}

	public function banksServicesTable() {
		$doc = Documents::find(1);
		$service_table = Excel::toArray('', '/' . $doc->file);

		$meta_tags = PagesMetaTags::where('page', config('global.pages.bank_services'))->where('parent_id', 0)->get();
		return view('frontend.pages.bank-services-table', compact('service_table', 'meta_tags'));
	}

	public function rateForTelegram(Request $request) {
		$otherExchangeType = RateExchangeType::all();

		$exchange_take = ExchangeRate::where('currency', $request->currency)->orderBy('take', 'DESC')->get();
		$exchange_sale = ExchangeRate::where('currency', $request->currency)->orderBy('sale', 'ASC')->get();

		$currencyName = Currency::find($request->currency);
		$best_rate_update_time = DataUpdateTime::where('service_id', 6)->first();

		return view('frontend.pages.telegram.rate-for-telegram', compact(
			'otherExchangeType',
			'exchange_take',
			'exchange_sale',
			'best_rate_update_time',
			'currencyName'
		));
	}

	public function bestTelegram() {

		$usd_rate_exchange_take = ExchangeRate::where('currency', '=', '2')->orderBy('take', 'DESC')->first();
		$rub_rate_exchange_take = ExchangeRate::where('currency', '=', '3')->orderBy('take', 'DESC')->first();
		$eur_rate_exchange_take = ExchangeRate::where('currency', '=', '4')->orderBy('take', 'DESC')->first();
		$gbp_rate_exchange_take = ExchangeRate::where('currency', '=', '5')->orderBy('take', 'DESC')->first();
		$chf_rate_exchange_take = ExchangeRate::where('currency', '=', '6')->orderBy('take', 'DESC')->first();
		$jpy_rate_exchange_take = ExchangeRate::where('currency', '=', '7')->orderBy('take', 'DESC')->first();
		$kzt_rate_exchange_take = ExchangeRate::where('currency', '=', '8')->orderBy('take', 'DESC')->first();

		$usd_rate_exchange_sale = ExchangeRate::where('currency', '=', '2')->orderBy('sale', 'ASC')->first();
		$rub_rate_exchange_sale = ExchangeRate::where('currency', '=', '3')->orderBy('sale', 'ASC')->first();
		$eur_rate_exchange_sale = ExchangeRate::where('currency', '=', '4')->orderBy('sale', 'ASC')->first();
		$gbp_rate_exchange_sale = ExchangeRate::where('currency', '=', '5')->orderBy('sale', 'ASC')->first();
		$chf_rate_exchange_sale = ExchangeRate::where('currency', '=', '6')->orderBy('sale', 'ASC')->first();
		$jpy_rate_exchange_sale = ExchangeRate::where('currency', '=', '7')->orderBy('sale', 'ASC')->first();
		$kzt_rate_exchange_sale = ExchangeRate::where('currency', '=', '8')->orderBy('sale', 'ASC')->first();

		$cb_rate = [
			'usd' => ExchangeRateClass::selectOne('USD'),
			'rub' => ExchangeRateClass::selectOne('RUB'),
			'eur' => ExchangeRateClass::selectOne('EUR'),
			'gbp' => ExchangeRateClass::selectOne('GBP'),
			'chf' => ExchangeRateClass::selectOne('CHF'),
			'jpy' => ExchangeRateClass::selectOne('JPY'),
			'kzt' => ExchangeRateClass::selectOne('KZT'),
		];

		$best_rate_update_time = DataUpdateTime::where('service_id', 6)->first();

		return view('frontend.pages.telegram.best-rate-for-telegram', [
			'usd_take' => $usd_rate_exchange_take,
			'rub_take' => $rub_rate_exchange_take,
			'eur_take' => $eur_rate_exchange_take,
			'gbp_take' => $gbp_rate_exchange_take,
			'chf_take' => $chf_rate_exchange_take,
			'jpy_take' => $jpy_rate_exchange_take,
			'kzt_take' => $kzt_rate_exchange_take,
			'usd_sale' => $usd_rate_exchange_sale,
			'rub_sale' => $rub_rate_exchange_sale,
			'eur_sale' => $eur_rate_exchange_sale,
			'gbp_sale' => $gbp_rate_exchange_sale,
			'chf_sale' => $chf_rate_exchange_sale,
			'jpy_sale' => $jpy_rate_exchange_sale,
			'kzt_sale' => $kzt_rate_exchange_sale,
			'cd_rate' => $cb_rate,
			'best_rate_update_time' => $best_rate_update_time,
		]);
	}

	public function testPage() {

		return view('test');
	}

	public function downloadCommunicationRules($lang) {
		$file = 'uz/Depozit.uz veb-saytida muloqot qilish qoidalari.docx';
		if ($lang == 'ru') {
			$file = 'ru/Правила общения на сайте Depozit.uz.docx';
		} elseif ($lang == 'uz') {
			$file = 'uz/Depozit.uz veb-saytida muloqot qilish qoidalari.docx';
		}
		$path = public_path('/doc-templates/doc/communication-rules/' . $file);
		return Response::download($path);
	}

	public function selectRegionBanks(Request $request) {
		$option = '';
		$options = '';
		if ($request->region_id) {
			$banks = Bank::where(['deleted' => 0, 'organization_id' => 1])->where('region_id', 'LIKE', '%' . $request->region_id . '%')->orderBy('name_' . App::getLocale())->get();
			$option .= '<option value="0">' . __("lang.banks") . '</option>';
			foreach ($banks as $row) {
				$option .= '<option value="' . $row->id . '">' . $row->name() . '</option>';
			}
			$options = $option;
		} else {
			$banks = Bank::where(['deleted' => 0, 'organization_id' => 1])->orderBy('name_' . App::getLocale())->get();
			$option .= '<option value="0">' . __("lang.banks") . '</option>';
			foreach ($banks as $row) {
				$option .= '<option value="' . $row->id . '">' . $row->name() . '</option>';
			}
			$options = $option;
		}

		return Response($options);
	}

	public function selectBankBranches(Request $request) {
		return self::selectOption(BankOffices::class, $request, 'bank_id');
	}

	protected function selectOption($class, $request, $attribute) {
		$ids_region = [];
		if ($request->region_id) {
			$regions = Region::find($request->region_id);
			if (empty($regions->parent_id)) {
				foreach ($regions->children as $child) {
					$ids_region[] = $child->id;
				}
			}
		}
		$option = '';
		$options = '';
		if ($request->id) {
			$model = $class::where($attribute, $request->id)->orderBy('position', 'DESC');
			if ($request->region_id) {
				if (count($ids_region) <= 0) {
					$model = $model->where('region_id', $request->region_id);
				} else {
					$model = $model->whereIn('region_id', $ids_region);
				}
			}
			$model = $model->where('deleted', 0)->get() /*->sortBy('name')*/;
			foreach ($model as $row) {
				$option .= '<option value="' . $row->id . '">' . $row->getName(App::getLocale()) . '</option>';
			}
			$options = '<option value"">' . __('lang.choose') . '</option>' . $option;
		}
		return Response($options);
	}

	public function notifications(Request $request) {
		$class_name = $request->input('class');
		$class = app("App\Models" . '\\' . "$class_name");
		$column = $request->input('table_column_name');
		$class = $class::find($request->input('id'));
		$class->$column = $request->input('column_value');
		$class->save();

		$model = Notification::where('table_name', $request->input('table_name'))
			->where('row_column', $request->input('column'))
			->where('row_id', $request->input('id'))->first();
		if (empty($model)) {
			$model = new Notification();
		}

		$model->table_name = $request->input('table_name');
		$model->row_id = $request->input('id');
		$model->row_column = $request->input('column');
		$model->row_column_value = $request->input('column_value');
		$model->status = 1;
		$model->save();

		return response()->json(['status' => true]);
	}

	public function delNotification($id) {
		if ($id) {
			$model = Notification::find($id);
			$model->delete();
		}

		return redirect()->back();
	}

	public function checkNotification($id) {
		if ($id) {
			$model = Notification::find($id);
			$model->status = 0;
			$model->save();
		}

		return redirect()->back();
	}

	public function visitorJson() {
		if (getenv('HTTP_CLIENT_IP')) {
			$ipaddress = getenv('HTTP_CLIENT_IP');
		} else if (getenv('HTTP_X_FORWARDED_FOR')) {
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		} else if (getenv('HTTP_X_FORWARDED')) {
			$ipaddress = getenv('HTTP_X_FORWARDED');
		} else if (getenv('HTTP_FORWARDED_FOR')) {
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		} else if (getenv('HTTP_FORWARDED')) {
			$ipaddress = getenv('HTTP_FORWARDED');
		} else if (getenv('REMOTE_ADDR')) {
			$ipaddress = getenv('REMOTE_ADDR');
		} else {
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		}

		$data[$ipaddress] = [
			'count' => 1,
			'time' => date("H:i:s"),
		];
		$dateMonth = date('m');
		$dateDay = date('d');
		$dateYear = date('Y');
		$path = '../storage/visitors/' . $dateYear . '/' . $dateMonth;
		if (File::exists($path . '/' . $dateDay . '.json')) {
			$in_file = utf8_encode(File::get($path . '/' . $dateDay . '.json'));
			$in_file_array = json_decode($in_file, true);
			//dd($in_file_array);

			if (!$in_file_array) {$in_file_array = [];}

			if (array_key_exists($ipaddress, $in_file_array)) {
				$old_time = strtotime($in_file_array[$ipaddress]['time']);
				$new_time = strtotime(date("H:i:s"));

				$diff_time = (time() - $old_time) / 60;
				if ($diff_time >= 50) {
					$in_file_array[$ipaddress]['count'] += 1;
					$in_file_array[$ipaddress]['time'] = date("H:i:s");
					$new_json = json_encode($in_file_array);
					File::put($path . '/' . $dateDay . '.json', $new_json);
				}
			} else {
				$new_array = array_merge($data, $in_file_array);
				$new_json = json_encode($new_array);
				File::put($path . '/' . $dateDay . '.json', $new_json);
			}
		}
		if (!File::exists($path . '/' . $dateDay . '.json')) {
			$json = json_encode($data);
			File::makeDirectory($path, 0755, true, true);
			File::put($path . '/' . $dateDay . '.json', $json);
		}
	}

	public function sendToChannel() {

		$apiToken = "1285276388:AAEtju1Uo42UbMUSjWb3L0s8diTcPGr2MMQ";
		$chat_id = "@rootadmindepozituz";
		$message = "Test send new message to channel";
		$data = [
			'chat_id' => $chat_id,
			'text' => $message,
		];

		$response = file_get_contents("https://api.telegram.org/bot$apiToken/sendMessage?" . http_build_query($data));
	}
}
