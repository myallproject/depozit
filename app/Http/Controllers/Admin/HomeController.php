<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use App\Models\Documents;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Excel;

use Auth;

const PATH_DOC = 'uploads/documents/banks/services_table/';
class HomeController extends Controller
{
	public function contact(){
	    $contacts = Contact::orderBy('id','desc')->get();
	    return view('admin.contact.list',compact ('contacts'));
	}

	public function bankServicesTable()
	{
		$documents = Documents::all();

		return view('admin.banks.services.table-services',compact('documents'));
	}

	public function bankServicesTableEdit(Request $request)
	{
		if($request->id){
			$model = Documents::find($request->id);
		} else {
			$model = new Documents();
		}

    	$model->user_id = Auth::id();
    	$model->comments = $request->comments;

		if($request->file('file')){
			if (is_file($model->file)) {
				unlink(public_path('/').$model->file);
			}
			$fileName = time().'.'.$request->file('file')->getClientOriginalExtension();
			$model->file = PATH_DOC.$fileName;
			$request->file('file')->move(PATH_DOC,$fileName);
		}

    	$model->save();

    	return redirect()->back();
    }

    public function download($id){
    	$model = Documents::find($id);
    	$path = public_path('/'.$model->file);

    	return response()->download($path);
    }
    
    public function deleteFile($id){
    	$model = Documents::find($id);
    	if (is_file($model->file)) {
			unlink(public_path('/').$model->file);
		}
    	//$model->delete();

    	return redirect()->back();
    }
}
