<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\UserRole;
use App\Models\Bank;
use App\Models\Organization;
use App\Models\UserOrganization;
use App\Models\HotLineUser;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

const USER_PATH = "uploads/users/";
class UserController extends Controller
{
    public function users() {
    	$users = User::all();
		return view('admin.users.list',compact('users'));
	}

	public function usersRoles()
	{
		$list = UserRole::all();
		return view('admin.users.roles.list',compact('list'));
	}

	public function editUserRole(Request $request)
	{
		if(!empty($request->id)){
            $model = UserRole::find($request->id);
        }if(empty($request->id)){
            $model = new UserRole();
        }

        $model->name = $request->input('name');
        $model->status = $request->input('status');
        $model->save();

        return redirect()->back();
	}

	public function deleteRole($id)
	{
		if($id){
			$model = UserRole::find($id);
			$model->status = 0;
			$model->delete = 1;
			$model->save();
		}

		return redirect()->back();
	}

	public function userUpdate($id)
	{
		$user = User::find($id);
		$userrole = UserRole::all();
		return view('admin.users.update',compact('user','userrole'));
	}

	public function userAdd(){
		$userrole = UserRole::all();
		return view('admin.users.add',compact('userrole'));
	}

	public function userEdit(Request $request)
	{
		$ruleEmail = null;
		if($request->input('id')){
			$model = User::find($request->input('id'));
			$ruleEmail = $request->has('email') ? Rule::unique('users')->ignore($model->id) : '';
		} else {
			$model = new User();
		}
		
		if($request->input('email'))
		{
			$request->validate([
	    		'email' => [$ruleEmail]
	    	]);
		}
		$model->name = $request->input('name');
		$model->email = $request->input('email');
		$model->role_id = $request->input('role');
		if($request->input('password')){
			$model->password = Hash::make($request->input('password'));
			$model->password_text = $request->input('password');
		}
		if($request->file('image')){
            if (is_file($model->image)) {
                unlink(public_path('/').$model->image);
            }
            $fileName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $model->user_image = USER_PATH.$fileName;
            $request->file('image')->move(USER_PATH,$fileName);
        }
        $model->save();
        if($request->input('organization')){
			$user_org = UserOrganization::where('user_id',$model->id)->first();
			if(!$user_org){
				$user_org = new UserOrganization();
			}
			$user_org->user_id = $model->id;
			$user_org->organization_id = $request->input('organization');
			$user_org->child_org_id = $request->input('child_oranization');
			$position = [
				'uz' => $request->input("position_uz"),
				'ru' => $request->input("position_ru")
			];
			$jsonP = json_encode($position);
			$user_org->position = $jsonP;
			$model->verified = 1;
			
			$model->save();
			$user_org->save();

			if($request->input('hot_line')){
				$hotLine = HotLineUser::where('org_id', $request->input('organization'))->first();
				if(!$hotLine){
					$hotLine = new HotLineUser();
				}

				$hotLine->user_id = $model->id;
				$hotLine->org_id = $request->input('child_oranization');
				$hotLine->save();
			} else {
				$hotLine = HotLineUser::where('org_id', $request->input('child_oranization'))->first();
				if($hotLine){
					$hotLine->delete();
				}
			}
		} else {
			$user_org = UserOrganization::where('user_id',$model->id)->first();
			if($user_org){
				$user_org->delete();
			}
		}
        return redirect()->route('users-list');
	}

	public function userOtherContent(Request $request)
	{
		$banks = Bank::orderBy('name_uz')->get();
		$organizations = Organization::all();
		$user = [];
		if($request->id){
			$user = User::find($request->id);
		}
		return view('admin.users.user-other-input',compact('banks','organizations','user'))->render();
	}

	public function selectOrganization(Request $request)
	{
		$option = '';
		$options = '';

		if($request->input('org_type')){
			$list = Bank::where('organization_id',$request->input('org_type'))->orderBy('name_uz','ASC')->get();
			foreach($list as $row){
				$selected = '';
				if($request->input('child_org') == $row->id){
					$selected = 'selected';
				}
				$option .='<option '.$selected.' value="'.$row->id.'">'.$row->name().'</option>';
			}
			$options = $option;

		}
		return Response($options);
	}
}
