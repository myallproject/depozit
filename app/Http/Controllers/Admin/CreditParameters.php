<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CreditBorrowerCategory;
use App\Models\CreditBoshBadal;
use App\Models\CreditGoal;
use App\Models\CreditIssuingForm;
use App\Models\CreditPledge;
use App\Models\CreditProofOfIncome;
use App\Models\CreditProvision;
use App\Models\CreditReviewPeriod;
use App\Models\CreditSurety;
use App\Models\CreditType;
use Illuminate\Http\Request;

class CreditParameters extends Controller
{
    public function delete($model,$id){
        $m = $model::find($id);
        $m->delete();
    }

    public function edit($model,$request)
    {
        $model->name_uz = $request->name_uz;
        $model->name_ru = $request->name_ru;
        $model->service_id = $request->input('credit_type_id');

        $model->save();
    }

    /** Kredit oluvchi toifasi **/
    public function borrowerCategoryList(){

        $creditType = CreditType::all();
        $model = CreditBorrowerCategory::all();

        return view('admin.credit-parameter.borrower-category.list',compact(
            'model',
            'creditType'
        ));
    }

    public function borrowerCategoryEdit(Request $request){
        if(!empty($request->id)){
            $model = CreditBorrowerCategory::find($request->id);
        }if(empty($request->id)){
            $model = new  CreditBorrowerCategory();
        }

        self::edit($model,$request);

        return redirect()->back();
    }

    public function borrowerCategoryDelete($id){
        if($id){
            self::delete(CreditBorrowerCategory::class,$id);
        }
        return redirect()->back();
    }

    public function borrowerCategorySelectType(Request $request){

        $type = CreditType::all();

        $option='';
        foreach ($type as $row){
            $select = '';
            if($row->id == $request->id_type){
                $select = 'selected="selected"';
            }
            $option.='<option '.$select.' value="'.$row->id.'">'.$row->name().'</option>';
        }
        $options ='<option value="">-'.__('lang.choose').'-</option>'.$option;
        return Response($options);
    }

    /** End Kredit oluvchi toifasi **/

    /** Kredit maqsadi **/
    public function goalCredit(){
        $creditType = CreditType::all();
        $model = CreditGoal::all();
        return view('admin.credit-parameter.credit-goal.list',compact(
            'model',
            'creditType'
        ));
    }

    public function goalCreditEdit(Request $request){
        if(!empty($request->id)){
            $model = CreditGoal::find($request->id);
        }if(empty($request->id)){
            $model = new CreditGoal();
        }

        self::edit($model,$request);

        return redirect()->back();
    }

    public function goalCreditDelete($id){
        if($id){
           self::delete(CreditGoal::class,$id);
        }
        return redirect()->back();
    }

    /** End Kredit maqsadi **/

    /** Kredit berish shakli **/

    public function issuingFormCreditList(){
        $creditType = CreditType::all();
        $model = CreditIssuingForm::all();
        return view('admin.credit-parameter.credit-issuing-form.list',compact(
            'model',
            'creditType'
        ));
    }

    public function issuingFormCreditEdit(Request $request){
        if(!empty($request->id)){
            $model = CreditIssuingForm::find($request->id);
        }if(empty($request->id)){
            $model = new CreditIssuingForm();
        }

        self::edit($model,$request);

        return redirect()->back();
    }

    public function issuingFormCreditDelete($id){
        if($id){
            self::delete(CreditIssuingForm::class,$id);
        }
        return redirect()->back();
    }
    /** End Kredit berish shakli**/

    /** Kredit garovi **/
    public function pledgeCreditList(){
        $creditType = CreditType::all();
        $model = CreditPledge::all();
        return view('admin.credit-parameter.credit-pledge.list',compact(
            'model',
            'creditType'
        ));
    }

    public function pledgeCreditEdit(Request $request){
        if(!empty($request->id)){
            $model = CreditPledge::find($request->id);
        }if(empty($request->id)){
            $model = new CreditPledge();
        }

        self::edit($model,$request);

        return redirect()->back();
    }
    public function pledgeCreditDelete($id){
        if($id){
            self::delete(CreditPledge::class,$id);
        }
        return redirect()->back();
    }

    /** End Kredit garovi **/

    /** Daromadni tasdiqlovci hujjat **/
    public function proofIncomeCreditList(){
        $creditType = CreditType::all();
        $model = CreditProofOfIncome::all();
        return view('admin.credit-parameter.proof-income.list',compact(
            'model',
            'creditType'
        ));
    }

    public function proofIncomeCreditEdit(Request $request){
        if(!empty($request->id)){
            $model = CreditProofOfIncome::find($request->id);
        }if(empty($request->id)){
            $model = new CreditProofOfIncome();
        }

        self::edit($model,$request);

        return redirect()->back();
    }
    public function proofIncomeCreditDelete($id){
        if($id){
            self::delete(CreditProofOfIncome::class,$id);
        }
        return redirect()->back();
    }
    /** End Daromadni tasdiqlovci hujjat **/

    /** Kreditni ko'rib chiqish vaqti**/
    public function reviewPeriodCreditList(){
        $creditType = CreditType::all();
        $model = CreditReviewPeriod::all();
        return view('admin.credit-parameter.review-period.list',compact(
            'model',
            'creditType'
        ));
    }

    public function reviewPeriodCreditEdit(Request $request){
        if(!empty($request->id)){
            $model = CreditReviewPeriod::find($request->id);
        }if(empty($request->id)){
            $model = new CreditReviewPeriod();
        }

        self::edit($model,$request);

        return redirect()->back();
    }
    public function reviewPeriodCreditDelete($id){
        if($id){
            self::delete(CreditReviewPeriod::class,$id);
        }
        return redirect()->back();
    }
    /** End Kreditni ko'rib chiqish **/

    /** Kredit uchun kafillik**/
    public function suretyCreditList(){
        $creditType = CreditType::all();
        $model = CreditSurety::all();
        return view('admin.credit-parameter.surety.list',compact(
            'model',
            'creditType'
        ));
    }

    public function suretyCreditEdit(Request $request){
        if(!empty($request->id)){
            $model = CreditSurety::find($request->id);
        }if(empty($request->id)){
            $model = new CreditSurety();
        }

        self::edit($model,$request);

        return redirect()->back();
    }
    public function suretyCreditDelete($id){
        if($id){
            self::delete(CreditSurety::class,$id);
        }
        return redirect()->back();
    }
    /** End Kredit uchun kafillik **/

    /** Kredit uchun taminot**/
    public function provisionCreditList(){
        $creditType = CreditType::all();
        $model = CreditProvision::all();
        return view('admin.credit-parameter.provision.list',compact(
            'model',
            'creditType'
        ));
    }

    public function provisionCreditEdit(Request $request){
        if(!empty($request->id)){
            $model = CreditProvision::find($request->id);
        }if(empty($request->id)){
            $model = new CreditProvision();
        }

        self::edit($model,$request);

        return redirect()->back();
    }
    public function provisionCreditDelete($id){
        if($id){
            self::delete(CreditProvision::class,$id);
        }
        return redirect()->back();
    }
    /** End Kredit uchun taminot **/

    /** boshlang'ich badal **/
    public function compensationCreditList(){
        $creditType = CreditType::all();
        $model = CreditBoshBadal::all();
        return view('admin.credit-parameter.compensation.list',compact(
            'model',
            'creditType'
        ));
    }

    public function compensationCreditEdit(Request $request){
        
        if(!empty($request->id)){
            $model = CreditBoshBadal::find($request->id);
        }if(empty($request->id)){
            $model = new CreditBoshBadal();
        }
            $model->status = $request->status;
            $model->name_uz = $request->name_uz;
            $model->name_ru = $request->name_ru;
            $model->service_id = $request->input('credit_type_id');
            $model->save();
        return redirect()->back();
    }
    public function compensationCreditDelete($id){
        if($id){
            self::delete(CreditBoshBadal::class,$id);
        }
        return redirect()->back();
    }
    /** end boshlang'ich badal **/
}
