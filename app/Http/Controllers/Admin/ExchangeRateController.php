<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Currency;
use App\Models\ExchangeRate;
use App\Models\OtherExchangeRateBank;
use App\Models\RateExchangeType;
use Illuminate\Http\Request;

class ExchangeRateController extends Controller
{
    public function rateList() {
        
        $list = ExchangeRate::all();

        $banks = Bank::all();
        $other_rate = OtherExchangeRateBank::all();
        return view('admin.exchange.list',compact('list','banks','other_rate'));
    }
    
    public function addForm() {
        $bank = Bank::all();
        $currency = Currency::all();
        $exchange = null;
        $typeRateExchange = RateExchangeType::all();
        return view('admin.exchange.edit',compact('exchange','bank','currency','typeRateExchange'));
    }
    
    public function add(Request $request) {
        $name_curr = Currency::find($request->currency);
         $exist = ExchangeRate::where('bank_id',$request->bank)->where('currency',$request->currency)->first();

        if($exist){
            $c =  ExchangeRate::where('bank_id',$request->bank)->where('currency',$request->currency)->first();
            $c->take = $request->take;
            $c->sale = $request->sale;
            $c->code = $name_curr->name;
        } else {
            $c = new ExchangeRate();
            $c->bank_id = $request->input('bank');
            $c->currency = $request->input('currency');
            $c->take = $request->take;
            $c->sale = $request->sale;
            $c->code = $name_curr->name;
        }
        $c->save();

        if($request->input('type_exchange')){
            foreach($request->input('type_exchange') as $k => $v){
                $otherExist =  OtherExchangeRateBank::where('bank_id',$request->bank)->where('currency_id',$request->currency)->where('type_id',$v)->first();
                if($otherExist){
                    $other_take = 'other_take_'.$v;
                    $other_sale = 'other_sale_'.$v;
                    $otherExist->take = $request->$other_take;
                    $otherExist->sale = $request->$other_sale;
                    $otherExist->save();
                } else {
                    $other = new OtherExchangeRateBank();
                    $other->bank_id = $request->input('bank');
                    $other->currency_id = $request->input('currency');
                    $other->type_id = $v;
                    $other_take = 'other_take_'.$v;
                    $other_sale = 'other_sale_'.$v;
                    $other->take = $request->$other_take;
                    $other->sale = $request->$other_sale;
                    $other->save();
                }
            }
        }


        return redirect()->route('rate-list');
    }
    
    public function updateForm($id) {
        ;
        $bank = Bank::all();
        
        $currency = Currency::all();
        
        $exchange = ExchangeRate::find($id);

        $typeRateExchange = RateExchangeType::all();
        
        return view('admin.exchange.edit',compact('exchange','bank','currency','typeRateExchange'));
    }
    
    public function update(Request $request) {
        
        $e = ExchangeRate::find($request->id);
        
        $e->bank_id = $request->input('bank');
        $e->currency = $request->input('currency');
        $e->take = $request->take;
        $e->sale = $request->sale;
        $e->updated_at = date('Y-m-d H:m:i');
        $e->save();


        
        return redirect()->route('rate-list');
    }
    
    public function delete($id) {
        $e = ExchangeRate::find($id);
        
        $e->delete();
        
        return redirect()->route('rate-list');
    }

    public function deleteOther($id){
        $d = OtherExchangeRateBank::find($id);

        $d->delete();

        return redirect()->back();
    }

    public function updateCurrenciesForm($bankId){

        $bank = Bank::find($bankId);
        $model = ExchangeRate::where('bank_id','=',$bankId)->orderBy('bank_id','ASC')->get();
        $typeRateExchange = RateExchangeType::all();
        $otherRateExchange = OtherExchangeRateBank::where('bank_id','=',$bankId)->get();

        return view('admin.exchange.bank-currencies',compact('model','bank','typeRateExchange','otherRateExchange'));
    }

    public function updateCurrenciesEdit(Request $request){

        foreach($request->ids as $k => $v ){
            $take = 'take_'.$v;
            $sale = 'sale_'.$v;
            $model = ExchangeRate::find($v);
            $name_curr = Currency::find($model->currency);
            $model->take = $request->$take;
            $model->sale = $request->$sale;
            $model->code = $name_curr->name;

            $model->save();
        }
        //dd($request->other_ids);
        if($request->other_ids){
            foreach ($request->other_ids as $k_oth => $v_oth){
                $other = OtherExchangeRateBank::find($v_oth);
                $take_other = 'other_take_'.$v_oth;
                $sale_other = 'other_sale_'.$v_oth;
               // dd($take_other);
                $other->take = $request->$take_other;
                $other->sale = $request->$sale_other;
                $other->save();
            }
        }

        return redirect()->route('rate-list');
    }

    /**
     * valyuta ayriboshlash turlari
     **/
    public function listTypeExchange(){
        $model = RateExchangeType::all();
        $banks = Bank::all();
        return view('admin.exchange.type.list',compact('model','banks'));
    }

    public function editTypeExchange(Request $request){
        if(!empty($request->id)){
            $model = RateExchangeType::find($request->id);
        } else {
            $model = new RateExchangeType();
        }
        $model->bank_id = $request->bank_id;
        $model->name_uz = $request->name_uz;
        $model->name_ru = $request->name_ru;
        $model->save();
        return redirect()->back();
    }

    public function deleteTypeExchange($id){
        if($id){
            $model = RateExchangeType::find($id);
            $model->delete();
        }
        return redirect()->back();
    }
}
