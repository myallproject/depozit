<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Models\News;
use App\Models\NewsCategory;

const PATHIMG = 'uploads/news/';

class NewsController extends Controller
{
	protected $attributes = [
		'title_uz'=>'',
		'title_ru'=>'',
		'link_ru'=>'',
		'link_uz'=>'',
		'description_uz'=>'',
		'description_ru'=>'',
		'text_uz'=>'',
		'text_ru'=>'',
		'image'=>''
	];
    public function newsList(){
    	$news = News::all();
    	return view('admin.news.list',compact('news'));
    }

    public function add(Request $request)
    {
    	$return = '';
    	if($request->post()){
    		$validate = Validator::make($request->all(),[
			 	'new_category_id'=>'required',
			 	'title_uz'=>'required',
			 	'title_ru'=>'required',
			 	'text_uz'=>'required',
			 	'text_ru'=>'required',
			 	'link_uz'=>'required',
			 	'link_ru'=>'required',
			 	'image' => 'required',
			 	'keywords_uz' => 'required',
			 	'keywords_ru' => 'required'
			]);
		  	
			if ($validate->fails()) {
				$return = redirect()->back()->withErrors($validate)->withInput();
			} else {
				$model = new News();
				$model->new_category_id = implode(',', $request->new_category_id);
				$model->title_uz = $request->title_uz;
				$model->title_ru = $request->title_ru;
				$model->link_uz = $request->link_uz;
				$model->link_ru = $request->link_ru;
				$model->description_uz = $request->description_uz;
				$model->description_ru = $request->description_ru;
				$model->keywords_uz = $request->keywords_uz;
				$model->keywords_ru = $request->keywords_ru;
				$model->text_uz = $request->text_uz;
				$model->text_ru = $request->text_ru;
				$model->slug = str_slug(mb_substr ($request->title_uz,'0',80, 'UTF-8'));
				$model->user_id = Auth::id();

				if($request->file('image')){
					$fileName = time().'.'.$request->file('image')->getClientOriginalExtension();
					$model->image = PATHIMG.$fileName;
					$request->file('image')->move(PATHIMG,$fileName);
				}

				$model->save();
				$return = redirect()->route('news-list');
			}
    	}else {
    			$categories = NewsCategory::all();
				$return = view('admin.news.add',compact('categories'));
    	}
    	return  $return;
    }

    public function update(Request $request){

    	$return = '';

    	if($request->post()){
    		$model = News::find($request->id);
			$model->new_category_id = implode(',', $request->new_category_id);
    		$model->title_uz = $request->title_uz;
			$model->title_ru = $request->title_ru;
			$model->link_uz = $request->link_uz;
			$model->link_ru = $request->link_ru;
			$model->description_uz = $request->description_uz;
			$model->description_ru = $request->description_ru;
			$model->text_uz = $request->text_uz;
			$model->text_ru = $request->text_ru;
			$model->keywords_uz = $request->keywords_uz;
			$model->keywords_ru = $request->keywords_ru;
			
			if($model->slug){
				$model->slug = str_slug(mb_substr ($request->title_uz,'0',80, 'UTF-8'));
			}

			if($request->file('image')){
	            if (is_file($model->image)) {
	                unlink(public_path('/').$model->image);
	            }
	            $fileName = time().'.'.$request->file('image')->getClientOriginalExtension();
	            $model->image = PATHIMG.$fileName;
	            $request->file('image')->move(PATHIMG,$fileName);
        	}

			$model->user_id = Auth::id();
			$model->save();
			$return = redirect()->route('news-list');

    	} else {
    		$model = News::find($request->id);
    		$categories = NewsCategory::all();
    		$return = view('admin.news.update',compact('model','categories'));
    	}
    	
    	return  $return;
    }

	public function delete($id){
		if($id){
			$model = News::find($id);
			$model->delete();
		}
		return redirect()->back();
	}

	public function categoryList(){
		$model = NewsCategory::all();
		return view('admin.categories.list',compact('model'));
	}

	public function categoryEdit(Request $request)
	{
		if($request->id){
			$model = NewsCategory::find($request->id);
		} else {
			$model = new NewsCategory();
		}
		if(empty($model->slug)){
			$model->slug = str_slug($request->name_uz);
		}
		$model->name_uz = $request->name_uz;
		$model->name_ru = $request->name_ru;
		$model->description_uz = $request->description_uz;
		$model->description_ru = $request->description_ru;

		$model->save();

		return redirect()->back();
	}

	public function categoryDelete($id)
	{
		if($id){
			$model = NewsCategory::find($id);
			$model->delete();
		}
		return redirect()->back();
	}


}
