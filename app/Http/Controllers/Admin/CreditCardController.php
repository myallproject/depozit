<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CreditCards;
use App\Models\Bank;
use App\Models\Currency;
use App\Models\DateService;
use App\Models\KreditKartTaminotTuri;
use App\Models\BankInformation;

use Illuminate\Http\Request;

const PATH_IMG = 'uploads/cards/credit/';

class CreditCardController extends Controller
{
	protected $attributes = [
		'id' => '',
		'bank_id'=>'',
		'bank_type_id'=>'',
		'currency_id'=>'',
		'name_uz'=>'',
		'name_ru'=>'',
		'image'=>'',
		'date_id'=>'',
		'imtoyozli_davr_uz'=>'',
		'imtoyozli_davr_ru'=>'',
		'percent'=>'',
		'status'=>'',
		'hisoblash_usul'=>'',
		'max_sum'=>'',
		'max_sum_text_uz'=>'',
		'max_sum_text_ru'=>'',
		'kurish_muddati'=>'',
		'documents_uz'=>'',
		'documents_ru'=>'',
		'taminot_turi_ids'=>'',
		'taminot_text_uz'=>'',
		'taminot_text_ru'=>'',
		'rasmiy_yuli_uz'=>'',
		'rasmiy_yuli_ru'=>'',
		'ajratish_uz'=>'',
		'ajratish_ru'=>'',
		'sundirish_uz'=>'',
		'sundirish_ru'=>'',
		'talablar_uz'=>'',
		'talablar_ru'=>'',
		'qushim_shart_uz'=>'',
		'qushim_shart_ru'=>'',
		'link'=>'',
	];
    public function cardList(){
    	$model = CreditCards::orderBy('status', 'desc')->get();
    	return view('admin.cards.credit.list',compact('model'));
    }
    

    public function addCard(Request $request){

    	if($request->isMethod('POST')){
    		$action = self::edit(new CreditCards, $request); 
    	} else {
    		$model = $this->attributes;
    		$edit = 'add';
    		$route = 'credit-card-add';
    		$banks = Bank::orderBy('name_uz')->get();
    		$currencies = Currency::all();
    		$dates = DateService::where(['service_id' =>3,'child_service_id' => 2, 'status' => 1])->get(); /** service_id = 3 kartalar, child_service_id = 1 kredit kartalar **/
    		$taminots = KreditKartTaminotTuri::all();

    		$action = view('admin.cards.credit.edit',compact(
    			'model',
    			'edit',
    			'route',
    			'banks',
    			'currencies',
    			'dates',
    			'taminots'
    		));
    	}

    	return $action;
    }

    public function updateCard(Request $request){
    	if($request->isMethod('POST')){
    		$action = self::edit(CreditCards::find($request->input('id')), $request); 
    	} else {
    		$model = CreditCards::find($request->input('id'));
    		$edit = 'add';
    		$route = 'credit-card-update';
    		$banks = Bank::orderBy('name_uz')->get();
    		$currencies = Currency::all();
    		$dates = DateService::where(['service_id' =>3,'child_service_id' => 2, 'status' => 1])->get(); /** service_id = 3 kartalar, child_service_id = 1 kredit kartalar **/
    		$taminots = KreditKartTaminotTuri::all();

    		$action = view('admin.cards.credit.edit',compact(
    			'model',
    			'edit',
    			'route',
    			'banks',
    			'currencies',
    			'dates',
    			'taminots'
    		));
    	}

    	return $action;
    }

    protected function edit($model,$request){
    	$model->bank_id = $request->input('bank_id');
    	$typeBan = BankInformation::where('banks_id',$request->input('bank_id'))->first();
    	if($typeBan){
    		$model->bank_type_id = $typeBan->type_bank;
    	}
  
    	$model->currency_id = $request->input('currency_id');
    	$model->name_uz = $request->input('name_uz');
    	$model->name_ru = $request->input('name_ru');
    	$model->date_id = $request->input('date_id');
    	$model->imtoyozli_davr_uz = $request->input('imtoyozli_davr_uz');
    	$model->imtoyozli_davr_ru = $request->input('imtoyozli_davr_ru');
    	$model->percent = $request->input('percent');
    	$model->hisoblash_usul = $request->input('hisoblash_usul');
    	$model->max_sum = $request->input('max_sum');
    	$model->max_sum_text_uz = $request->input('max_sum_text_uz');
    	$model->max_sum_text_ru = $request->input('max_sum_text_ru');
    	$model->kurish_muddati = $request->input('kurish_muddati');
    	$model->documents_uz = $request->input('documents_uz');
    	$model->documents_ru = $request->input('documents_ru');
    	$model->taminot_turi_ids = $request->input('taminot_turi_ids');
    	$model->taminot_text_uz = $request->input('taminot_text_uz');
    	$model->taminot_text_ru = $request->input('taminot_text_ru');
    	$model->rasmiy_yuli_uz = $request->input('rasmiy_yuli_uz');
    	$model->rasmiy_yuli_ru = $request->input('rasmiy_yuli_ru');
    	$model->ajratish_uz = $request->input('ajratish_uz');
    	$model->ajratish_ru = $request->input('ajratish_ru');
    	$model->sundirish_uz = $request->input('sundirish_uz');
    	$model->sundirish_ru = $request->input('sundirish_ru');
    	$model->talablar_uz = $request->input('talablar_uz');
    	$model->talablar_ru = $request->input('talablar_ru');
    	$model->qushim_shart_uz = $request->input('qushim_shart_uz');
    	$model->qushim_shart_ru = $request->input('qushim_shart_ru');
    	
        $link=[
            'uz' => $request->input('link_uz'),
            'ru' => $request->input('link_ru'),
        ];
        $model->link = json_encode($link);
    	
    
    	if($request->file('image')){
    		if (is_file($model->image)) {
                unlink(public_path('/').$model->image);
            }
            $fileName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $model->image = PATH_IMG.$fileName;
            $request->file('image')->move(PATH_IMG,$fileName);
    	}
    	//$model->image = $request->input('image');
    	//dd($request->file('image'));
    	$model->save();


    	return redirect()->route('credit-card-list');
    }

     public function cardStatus(Request $request){
        $model = CreditCards::find($request->id);
        $model->status = $request->status;
        $model->save();
        
        return response()->json(['status'=>$request->status]);
    }

    public function deleteCard($id){
        if($id){
            $model = CreditCards::find($id);
            $model->delete();
        }
        return redirect()->back();
    }

}
