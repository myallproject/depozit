<?php

namespace App\Http\Controllers\Admin;

use App;
use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\BankInformation;
use App\Models\Deposit;
use App\Models\Region;
use App\Models\TypeBanks;
use App\Models\BankOffices;
use App\Models\Organization;
use App\Imports\BankBranchesImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Config;

const BANK_PATH = "uploads/banks/";
define('A_BANK', Config::get('global.organization.bank'));
define('A_OTHER_ORGANIZATION', Config::get('global.organization.other'));

class BanksController extends Controller
{
    public function listBanks()
    {
        $organizations = Bank::where('organization_id',A_BANK)->orderBy('created_at', 'desc')->get();
        
        return view('admin.banks.list', compact('organizations'));
    }

    public function listOtherOrganization()
    {
        $organizations = Bank::where('organization_id',A_OTHER_ORGANIZATION)->orderBy('created_at', 'desc')->get();
        
        return view('admin.banks.other-organization.list', compact('organizations'));
    }
    
    public function view($id)
    {
        $bank = Bank::find($id);
        return view('admin.banks.view', compact('bank'));
    }
    
    public function updateView($id)
    {
        $bank = Bank::find($id);
        $bank_info = BankInformation::where('banks_id', $id)->first();
        $regions = Region::all();
        $type_bank = TypeBanks::all();
        $organization = Organization::all();
        return view('admin.banks.edit',compact('bank','bank_info','regions','type_bank','organization'));
    }
    
    public function update(Request $request)
    {
        $bank = Bank::find($request->id);
        
        if(empty($bank->slug)) {
            $bank->slug = str_slug($request->name_uz." ".rand(10000,99999),"-");
        }

        $bank->bank_type_id = $request->type;
        $bank->organization_id = $request->organization_id;

        $bank->name_uz = $request->name_uz;
        $bank->name_ru = $request->name_ru;

        $bank->phone = $request->phone1;

        $bank->description_uz = $request->description_uz;
        $bank->description_ru = $request->description_ru;

        $bank->text_uz = $request->text_uz;
        $bank->text_ru = $request->text_ru;

        $bank->work_time_uz = $request->work_time_uz;
        $bank->work_time_ru = $request->work_time_ru;

        $bank->address_uz = $request->address_uz;
        $bank->address_ru = $request->address_ru;

        $bank->location = $request->location;

        if($request->has('region')){
            $bank->region_id = implode(',',$request->input('region'));
        }
    
        if($request->file('image')){
            if (is_file($bank->image)) {
                unlink(public_path('/').$bank->image);
            }
            $fileName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $bank->image = BANK_PATH.$fileName;
            $request->file('image')->move(BANK_PATH,$fileName);
        }
     
        $bank->save();

        $bank_info = BankInformation::find($request->bank_info_id);

        $bank_info->type_bank = $request->type;

        $bank_info->official_name_uz = $request->official_name_uz;
        $bank_info->official_name_ru = $request->official_name_ru;

        $bank_info->mail = $request->email;

        $bank_info->phone1 = $request->phone1;
        $bank_info->phone2 = $request->phone2;
        $bank_info->phone3 = $request->phone3;

        $bank_info->site_link_uz = $request->link_uz;
        $bank_info->site_link_ru = $request->link_ru;
        $bank_info->telegram_channel = $request->telegram_channel;
        $bank_info->mobile_app = $request->mobile_app;
        $bank_info->app_store = $request->app_store;
        $bank_info->play_market = $request->play_market;

        $bank_info->chairman_uz = $request->chairman_uz;
        $bank_info->chairman_ru = $request->chairman_ru;

        $bank_info->stockholders_uz = $request->stockholders_uz;
        $bank_info->stockholders_ru = $request->stockholders_ru;

        $bank_info->address_uz = $request->official_address_uz;
        $bank_info->address_ru = $request->official_address_ru;
        $bank_info->count_office = count($request->region);

        $bank_info->inn = $request->stir;
        $bank_info->mfo = $request->mfo;
        $bank_info->svift = $request->swift;
        $bank_info->license = $request->licence;
        
        $bank_info->save();
        
        return redirect()->route('banks-view',[$bank->id]);
    }
    
    public function addView()
    {
        $bank = null;
        $regions = Region::all();
        $type_bank = TypeBanks::all();
        $organization = Organization::all();
        return view('admin.banks.edit',compact('bank','regions','type_bank','organization'));
    }
    
    
    public function add(Request $request) {
        
        $bank = new Bank();
        
        $bank->slug = str_slug($request->name_uz." ".rand(10000,99999),"-");
        $bank->bank_type_id = $request->type;

        $bank->name_uz = $request->name_uz;
        $bank->name_ru = $request->name_ru;

        $bank->phone = $request->phone1;
        $bank->organization_id = $request->organization_id;

        $bank->description_uz = $request->description_uz;
        $bank->description_ru = $request->description_ru;

        $bank->text_uz = $request->text_uz;
        $bank->text_ru = $request->text_ru;

        $bank->work_time_uz = $request->work_time_uz;
        $bank->work_time_ru = $request->work_time_ru;

        $bank->address_uz = $request->address_uz;
        $bank->address_ru = $request->address_ru;

        $bank->location = $request->location;

        if($request->has('region')){
            $bank->region_id = implode(',',$request->input('region'));
        }

        if($request->file('image')){
            $fileName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $bank->image = BANK_PATH.$fileName;
            $request->file('image')->move(BANK_PATH,$fileName);
        }

        $bank->save();

        $bank_info = new BankInformation();

        $bank_info->banks_id = $bank->id;
        $bank_info->type_bank = $request->type;

        $bank_info->official_name_uz = $request->official_name_uz;
        $bank_info->official_name_ru = $request->official_name_ru;

        $bank_info->mail = $request->email;

        $bank_info->phone1 = $request->phone1;
        $bank_info->phone2 = $request->phone2;
        $bank_info->phone3 = $request->phone3;

        $bank_info->site_link_uz = $request->link_uz;
        $bank_info->site_link_ru = $request->link_ru;
        $bank_info->telegram_channel = $request->telegram_channel;
        $bank_info->mobile_app = $request->mobile_app;
        $bank_info->app_store = $request->app_store;
        $bank_info->play_market = $request->play_market;

        $bank_info->chairman_uz = $request->chairman_uz;
        $bank_info->chairman_ru = $request->chairman_ru;

        $bank_info->stockholders_uz = $request->stockholders_uz;
        $bank_info->stockholders_ru = $request->stockholders_ru;

        $bank_info->address_uz = $request->official_address_uz;
        $bank_info->address_ru = $request->official_address_ru;
        $bank_info->count_office = count($request->region);

        $bank_info->inn = $request->stir;
        $bank_info->mfo = $request->mfo;
        $bank_info->svift = $request->swift;
        $bank_info->license = $request->licence;
        
        $bank_info->save();

        return redirect()->route('banks-list');
    }
    
    public function delete($id) {
        
        $b = Bank::find($id);
        $b->delete();
        return redirect()->back();
    }

    public function depositList($id)
    {
        $bank = Bank::find($id);
        $list = Deposit::where('bank_id',$id)->get();
        return view('admin.banks.deposits',compact ('list','bank'));
    }

    public function typeBanks(){
        $banksType = TypeBanks::all();
        return view('admin.banks.type-banks.list',compact ('banksType'));
    }

    public function typeBanksAdd(Request $request){
        if(empty($request->banks_type_id)){
            $model = new TypeBanks();
        }
        if(!empty($request->banks_type_id)){
            $model = TypeBanks::find($request->banks_type_id);
        }
            $model->name_uz = $request->name_uz;
            $model->name_ru = $request->name_ru;
            $model->save();

        return redirect ()->back ();
    }

    public function typeBanksDelete($id){
        $model = TypeBanks::find($id);
        $model->delete();
        return redirect ()->back ();
    }

    public function informations($id){
        
        $model = BankInformation::where('banks_id',$id)->first();
        
        if(!$model){
            $model = new BankInformation();
            $model->banks_id = $id;
            $model->save();

            $model = BankInformation::find($model->id);
        }
        $type_bank = TypeBanks::all();

        return view('admin.banks.information.view',compact('model','type_bank'));
    }

    public function EditInformations(Request $request){
         $model = BankInformation::find($request->id);
         $model->license = $request->license;
         $model->type_bank = $request->type;
         $model->phone = $request->phone;
         $model->helpline = $request->helpline;
         $model->site = $request->site;
         $model->telegram_channel = $request->telegram_channel;
         $model->mail = $request->mail;
         $model->address_uz = $request->address_uz;
         $model->address_ru = $request->address_ru;
         $model->count_office = $request->count_office;
         $model->count_min_office = $request->count_min_office;
         $model->inn = $request->inn;
         $model->svift = $request->svift;

         $model->save();

        return redirect ()->route('bank-information',['id'=>$model->banks_id]);
    }

    public function organizationBranches($id){
        $branches = BankOffices::where('bank_id',$id)->get(); 
        $organization = Bank::find($id);
        return view('admin.organization.branches.list',compact('branches','organization'));
    }

    public function branchesAdd($id){
        $organization = Bank::find($id);
        $name = 'name_'.App::getLocale();
        $regions = Region::whereNull('parent_id')->orderBy($name,'ASC')->get();
        return view('admin.organization.branches.add',compact('organization','regions'));
    }


    public function branchesUpdate($id)
    {
        $branch = BankOffices::find($id);
        $organization = Bank::find($branch->bank_id);
        $name = 'name_'.App::getLocale();
        $regions = Region::whereNull('parent_id')->orderBy($name,'ASC')->get();
        return view('admin.organization.branches.update',compact('organization','regions','branch'));
    }


    public function branchesEdit(Request $request)
    {
        //dd($request);
        if($request->input('id')){
            $model = BankOffices::find($request->input('id'));
        } else {
            $model = new BankOffices();
        }

        $model->bank_id = $request->input('org_id');
        $model->phone = $request->input('phone');
        $model->mfo = $request->input('mfo');
        $model->region_id = $request->input('region_id');
        $model->region_id = $request->input('region_id');
        $model->lang = $request->input('lang');
        $model->lat = $request->input('lat');

        $model->name = self::jsonEncode($request->input('name_uz'),$request->input('name_ru'));
        $model->address = self::jsonEncode($request->input('address_uz'),$request->input('address_ru'));
        $model->services = self::jsonEncode($request->input('services_uz'),$request->input('services_ru'));
        
        $model->save();

        return redirect()->route('organization-branches',[$request->input('org_id')]);
    }

    protected function jsonEncode($attr_uz,$attr_ru)
    {
        $arr = ['uz' => '', 'ru' => ''];
        $arr = [
            'uz' => $attr_uz,
            'ru' => $attr_ru
        ];
        
        $json = json_encode($arr);

        return $json;
    }  

    public function importBranches(Request $request) 
    {
        Excel::import(new BankBranchesImport,$request->file('file'));
           
        return back();
    }
}
