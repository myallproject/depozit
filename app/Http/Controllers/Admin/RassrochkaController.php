<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Components\RassrochkaTelefonApi;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Shop;
use App\Models\ShopTelephone;
use App\Models\TelefonAPI;
use App\Models\Telephone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Facades\Excel;

class RassrochkaController extends Controller {
    public function telephonesList() {
        $telephones = Telephone::orderBy('smartphone_model')
        ->get(['id','brand_id','smartphone_model','smartphone_inner_memory','smartphone_operation_memory','smartphone_dimension','smartphone_weight']);
        $brands = Brand::orderBy('name_uz')->get();
        return view('admin.rassrochka.telephones.list',
                    compact('brands', 'telephones'));
    }
    public function telephoneTypeView($brand_id) {
        $telephones = Telephone::where('brand_id','=',$brand_id)->orderBy('smartphone_model')
        ->get(['id','brand_id', 'smartphone_model','smartphone_inner_memory','smartphone_operation_memory','smartphone_dimension','smartphone_weight']);
        $brands = Brand::orderBy('name_uz')->get();
        return view('admin.rassrochka.telephones.telephone-type.list', 
                    compact('brands', 'telephones'));
    }
    public function importTelephones(Request $request) {
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx,ods'
        ]);
        $file = $request->file('file');
        $import = new Telephone;
        $import->import($file);
        if($import->errors() !== null){
            $count = 0;            
            foreach($import->errors()->toArray() as $error) {
                $count++;
            }
            return back()->with('myErrors',$count . ' ta qatorda model nomi kiritlmagan');
        } else {
            return back()->with('success', 'ma\'lumot to\'g\'ri qo\'shildi');
        }
        
    }
    public function deleteTelephone($id) {
        $telephone = Telephone::find($id);
        $telephone->delete();
        return redirect()->back();
    }
    public function telephoneEdit($telephone, $request) {
        $array = Lang::get('lang.rassrochka.telephone');
        $this->validate($request,[
            'model' => 'required'
        ]);
        $telephone->smartphone_model = $request->input('model');
        $telephone->brand_id = $request->input('brand_id'); 
        unset($array['model']);
        unset($array['brand']);
       
        foreach($array as $key =>$value) {
            $telephone->{"smartphone_$key"} = $request->input($key);
        }
        $telephone->save();        
    }
    public function addTelephone(Request $request) {
        $telephone = new Telephone();
        $this->telephoneEdit($telephone, $request);
        $id = $telephone->brand_id;
        return redirect()->route('admin-telephone-type', $id);
    }
    public function updateTelephone(Request $request) {
        $telephone = Telephone::find($request->id);
        $this->telephoneEdit($telephone, $request);
        $id = $telephone->brand_id;
        return redirect()->route('admin-telephone-type', $id);
    }

    public function updateTelephoneFromShop($shopTelefonId) {
        $shop_telefone = ShopTelephone::find($shopTelefonId);
        $telephone = new Telephone();
        $telephone->smartphone_model = $shop_telefone->smartphone_model;
        $telephone->smartphone_weight = $shop_telefone->smartphone_weight;
        $telephone->smartphone_dimension = $shop_telefone->smartphone_dimension;
        $telephone->smartphone_inner_memory = $shop_telefone->smartphone_inner_memory;
        $telephone->smartphone_operation_memory = $shop_telefone->smartphone_operation_memory;
        $telephone->brand_id = $shop_telefone->brand_id;
        $telephone->save();
        $id = $telephone->id;
        return redirect()->route('telephone-update-form', $id);
    }

    public function addTelephoneView() {
        $telephone = null;
        $brand = Brand::where('name_uz','like','Brendsiz')->first();
        $brands = Brand::where('name_uz', 'not like', 'Brendsiz')->orderBy('name_uz')->get();
        
        return view('admin.rassrochka.telephones.edit', compact(
            'telephone',
            'brands',
            'brand'
        ));
    }
    public function updateTelephoneView($id) {
        $telephone = Telephone::find($id);
        $brand = Brand::where('name_uz','like','Brendsiz')->first();
        $brands = Brand::where('name_uz', 'not like', 'Brendsiz')->orderBy('name_uz')->get();
        $key = 'model';
        return view('admin.rassrochka.telephones.edit', compact(
            'telephone',
            'brands',
            'brand'
        ));
    }

    public function telefoneAPIList() {
        $api = TelefonAPI::all();
        $organizations = Shop::orderBy('name_uz')->get();
        $table_nots = Notification::where('table_name', 'telefon_a_p_i_s')->where('status', 1)->get();
        return view('admin.rassrochka.telephones.telephone-api.list', compact('api','organizations','table_nots'));
    }
    
    public function telefoneAPIEdit(Request $request) {
        if($request->input('id')) {
            $model = TelefonAPI::find($request->input('id'));
        } else {
            $model = new TelefonAPI();
        }
        $model->shop_id = $request->input('shop_id');
        $model->project_token = $request->input('project_token');
        if($request->input('run_token')) $model->run_token = $request->input('run_token');
        $model->api_key = $request->input('api_key');
        $model->save();
        return redirect()->back();
    }

    public function telefoneAPIDelete($id) {
        $api = TelefonAPI::find($id);
        $api->delete();
        return redirect()->back();
    }
    public function telefoneAPIRunParse() {
        $delPro = RassrochkaTelefonApi::parseHubTelefonApi();
        $deleted = $delPro['deleted'];
        $problem = $delPro['problem'];
        if($deleted == 0 && $problem == 0) {
            return redirect()->back()->with("status", "done successfully");
        } else {
            if($problem == 0) {
                return redirect()->back()->with('status', $deleted . " ta bog'lanish avval bo'lganligi tufayli hisobga olinmadi." );
            } else {
                return redirect()->back()->with('status', $problem . " ta apida xatolik bo'ldi va xatoliklar Notification table saqlandi." );
            }            
        }
        
    }
    public function telefonAPIProblems() {
        $problems = RassrochkaTelefonApi::telefonFix();
        return view('admin.rassrochka.telephones.tproblem',$problems);
    }
    public function telefonShopAPIProblems() {
        $problems = RassrochkaTelefonApi::telefonAPIFix();
        return view('admin.rassrochka.telephones.tshopproblem',$problems)->with('status', $problems['success']);
    }
    public function telefoneModelEdit(Request $request) {
        $i = 0;
        while($request->{"id_".$i} != null) {
            $telephone = Telephone::find($request->{"id_".$i});
            $telephone->smartphone_model = $request->input('model_'.$i); 
            $telephone->save();
            $i++;
        }
        return redirect()->back();
    }
    public function telefonCharAPIRunParse() {
       $info = RassrochkaTelefonApi::parseHubTelefonCharApi();
       return redirect()->back()->with('success', $info['all'].' ta telefon keldi, shundan '.$info['already_added'].' ta telefon avval kiritilgan. '. $info['added'] .' ta telefon yangi qo\'shildi va ' .$info['problem'].' ta telefonda muammom aniqlandi.');
    }
    public function updateTelephoneShopMatch($shop_id, $id) {
        $shop_telefon = ShopTelephone::find($shop_id);
        $shop_telefon->telephone_id = $id;
        $shop_telefon->save();
        return redirect()->back();
    }
    public function brandsIndex() {
        $brands = Brand::all();
        return view('admin.rassrochka.telephones.telephoneBrand.list', compact('brands'));
    }

    public function brandsStore(Request $request) {
        if($request->id !== null) {
            $brand = Brand::findOrFail($request->id);
        } else {
            $brand = new Brand();
        }
        if($request->input('name_uz') !== null) $brand->name_uz = $request->input('name_uz');
        else return redirect()->back()->with('error', 'Nomi kiritilmagan');
        if($request->input('name_ru') !== null) $brand->name_ru = $request->input('name_ru');

        $brand->save();
        return redirect()->back()->with('success', 'Brand qo\'shildi');
    }
    
    public function brandsDestroy($id) {        
        $brand = Brand::findOrFail($id);
        $brand->delete();
        return redirect()->back()->with('success', 'Brand o\'chirildi');
    }
}
