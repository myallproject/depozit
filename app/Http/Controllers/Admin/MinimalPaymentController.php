<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MinimalPayment;
use App\Models\Currency;

class MinimalPaymentController extends Controller
{
    public function listQuantity() {
    	$model = MinimalPayment::all();
    	$currencies = Currency::all();
    	return view('admin.quantities.list',compact('model','currencies'));
    }

    public function editQuantity(Request $request) {
    	//dd($request);
    	if(!$request->id) {
    		return self::edit(new MinimalPayment, $request);
    	} else {
			return self::edit(MinimalPayment::find($request->id),$request);
    	}
    }

    protected function edit($model, $request) {
    	$model->name_uz = $request->name_uz;
    	$model->name_ru = $request->name_ru;
    	$model->description_uz = $request->description_uz;
    	$model->description_ru = $request->description_ru;
    	$model->currency_id = $request->currency_id;
    	$model->summa = $request->summa;
    	$model->status = $request->status;
    	$model->save();

    	return redirect()->back()->with(['msg'=>'Muvofaqiyatli saqlandi!']);
    }

    public function destroyQuantity($id){
    	
    	if($id) {
    		$model = MinimalPayment::find($id);

    		$model->delete();

    		return redirect()->back();
    	}

    	return redirect()->back()->with(['msg'=>'Mavjud emas']);
    }
}
