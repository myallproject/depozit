<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Currency;
use App\Models\ExchangeRate;
use App\Models\Bank;
use App\Models\UserRole;
use App\User;
use App\Http\Controllers\Controller;
use App\Components\BankExchangeRateApi;

class DashboardController extends Controller
{
 /*   public function __construct()
    {
        $this->middleware('auth');
    }*/
    
    public function dashboard()
    {
    	$currencies = Currency::where('id','!=',1)->get();
    	$banks = Bank::orderBy('name_uz','ASC')->get();
    	$users = User::all();
        return view('admin.dashboard',compact(
        	'currencies',
        	'banks',
        	'users'
        ));
    }

	public function rateCurrencyName()
	{
		$currency = Currency::all();
		$rates = ExchangeRate::all();

		foreach($rates as $r){
			$currency = Currency::find($r->currency);
			
			$rate = ExchangeRate::find($r->id);
			
			$rate->code = $currency->name;
			
			$rate->save();
		}

		return redirect()->back();
	}

	
}
