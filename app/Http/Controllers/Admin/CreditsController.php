<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\CarBrand;
use App\Models\Credit;
use App\Models\CreditBorrowerCategory;
use App\Models\CreditBoshBadal;
use App\Models\CreditGoal;
use App\Models\CreditIssuingForm;
use App\Models\CreditProvision;
use App\Models\CreditReviewPeriod;
use App\Models\CreditType;
use App\Models\Currency;
use App\Models\DateService;
use App\Models\TypeBanks;
use App\Models\CreditChildrenDate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use YandexTranslate;

use Illuminate\Support\Facades\Config;

define('G_CREDIT_ID',Config::get('global.services.credit'));

define('YMD',Config::get('global.ymd'));

class CreditsController extends Controller
{
    public function creditsList() {
        //$type = CreditType::where('slug',$slug)->first();
       // $list = Credit::where('type_credit_id',$type['id'])->get();
        $credits = Credit::orderBy('status', 'desc')->get();
        $credit_types = CreditType::all();
        return view('admin.credits.list',compact('credits','credit_types'));
    }
    
    public function creditTypeView($slug) {
        $type_c = CreditType::where('slug',$slug)->first();
        $list = Credit::where('type_credit_id',$type_c['id'])->orderBy('status', 'desc')->get();
        $credit_types = CreditType::all();

        return view('admin.credits.type',compact('credit_types','list','type_c'));
    }

    protected function makeJson($model, $attr)
    {
        foreach($model::all() as $list)
        {
            $array = ['uz'=>$list->$attr,'ru'=>$list->$attr];

            $json = json_encode($array);

            $list->$attr = $json;
            $list->save();
        }
    }

    public function addCreditView() {
        $credit = null;

        $credit_types = CreditType::all();
        $banks = Bank::orderBy('name_uz','ASC')->get();
        $banks_type = TypeBanks::all();

        $date_type = DateService::whereIn('id',[8,9,10])->get();
        $currency = Currency::all();
        $review_period = CreditReviewPeriod::all();

        $car_brands = CarBrand::all();
        
        return view('admin.credits.edit',compact(
            'car_brands',
            'credit',
            'banks',
            'date_type',
            'currency',
            'credit_types',
            'review_period',
            'banks_type'
        ));
    }

    public function creditEdit($c,$request){
        if(empty($request->input('id'))){
            $c->slug = str_slug($request->name." ".rand(10000,99999),"-");
        }
        
        $c->status= $request->input('status');
        $c->status_suspended = $request->input('status_suspended');
        
        $c->type_bank_id = $request->input('type_bank');
        $c->bank_id = $request->input('bank');
        $c->type_credit_id = $request->input('type_credit');
        $c->date_credit = $request->date;
        
        $c->type_date = $request->input('type_date');
        
        $c->amount = $request->amount;
        
        $c->currency = $request->input('currency');
        $c->percent = $request->percent;

        if($request->car_brand != 0) $c->car_brand_id = $request->car_brand;
       

        $link=[
            'uz' => $request->input('link_uz'),
            'ru' => $request->input('link_ru'),
        ];
        $c->link = json_encode($link);
        if($request->input('type_date_filter')){
            $dates =  implode(',', $request->input('type_date_filter'));
        }
        
        $c->type_date_filter = $dates;
      

        $c->credit_review_period_id = $request->input('review_period');
       
        $c->credit_issuing_form_id = $request->input('credit_issuing_form');
        if($request->input('provision')){
            $c->provision_id = implode(',',$request->input('provision')); 
        }

        $c->credit_borrower_category_id = $request->input('credit_borrower_category');
        if($request->input('credit_goal_select')){
            $c->credit_goal_id = implode(',',$request->input('credit_goal_select'));
        }

        $rasmiylashtirish = ['uz'=> $request->rasmiylashtirish_uz, 'ru' => $request->rasmiylashtirish_ru];
        $rasmiylashtirish_json = json_encode( $rasmiylashtirish);
        //dd($rasmiylashtirish_json);
        $c->rasmiylashtirish = $rasmiylashtirish_json ? : 0;

        $c->credit_data_string = $request->credit_data_string ? : 0;
        $c->garov = $request->garov_taminoti ? : 0;
        $c->kafillik = $request->kafillik ? : 0;
        $c->sugurta_polisi = $request->sugurta_polisi ? : 0;

        $c->ajratish_tartibi = $request->ajratish_tartibi ? : 0;
        $c->sundirish_tartibi = $request->sundirish_tartibi ? : 0;
        $c->credit_borrower_age = $request->credit_borrower_age_uz ? : 0;
        $c->credit_borrower_age_ru = $request->credit_borrower_age_ru ? : 0;
        $c->credit_borrower_possible_uz = $request->credit_borrower_possible_uz ? : 0;
        $c->credit_borrower_possible_ru = $request->credit_borrower_possible_ru ? : 0;
        $c->credit_borrower_additional_uz = $request->credit_borrower_additional_uz ? : 0;
        $c->credit_borrower_additional_ru = $request->credit_borrower_additional_ru ? : 0;
        if($request->input('compensation_status')){
            $c->compensation_status = implode(',', $request->input('compensation_status'));
        }

        $c->text_amount_uz = $request->text_amount_uz;
        $c->text_amount_ru = $request->text_amount_ru;
        
        $c->first_compensation_uz = $request->first_compensation_uz;
        $c->first_compensation_ru = $request->first_compensation_ru;
        $c->date_privilege_uz = $request->date_privilege_uz;
        $c->date_privilege_ru = $request->date_privilege_ru;
        $c->goal_credit_uz = $request->goal_credit_uz;
        $c->goal_credit_ru = $request->goal_credit_ru;
        $c->documents_uz = $request->documents_uz;
        $c->documents_ru = $request->documents_ru;
        $c->addition_rules_uz = $request->addition_rules_uz;
        $c->addition_rules_ru = $request->addition_rules_ru;
        $c->type_provision_uz = $request->type_provision_uz;
        $c->type_provision_ru = $request->type_provision_ru;
        $c->percent_table = $request->percent_table;
        $c->name_uz = $request->name_uz;
        $c->name_ru = $request->name_ru;

        $c->aktsiya = $request->aktsiya > 0 ? 1 : 0;
        $link = [
            'uz' => $request->input('aktsiya_link_uz'),
            'ru' => $request->input('aktsiya_link_ru')
        ];
        $c->aktsiya_link = json_encode($link);
        $izoh = [
            'uz' => $request->input('aktsiya_izoh_uz'),
            'ru' => $request->input('aktsiya_izoh_ru')
        ];
        $c->aktsiya_izoh = json_encode($izoh);
        $c->save();

        if(count($request->input('type_date_filter')) > 1){
            $sorted = [];
            foreach($request->input('type_date_filter') as $key => $value){
                $sorted += [$value => $request->input('percent_'.$value)];
            }
            asort($sorted);
            foreach($sorted as $value => $key){
                $childDate = CreditChildrenDate::where(['credit_id'=> $c->id, 'date_id'=> $value])->first();
                if(!$childDate){
                    $childDate = new CreditChildrenDate();
                } 

                $childDate->credit_id = $c->id;
                $childDate->date_id = $value;
                $childDate->currency_id = $request->input('currency');
                $childDate->compensation_id = $request->input('first_compensation_'.$value);
                $childDate->date = $request->input('date_'.$value);
                $childDate->date_type = $request->input('date_type_'.$value);
                $childDate->date_name_uz = $request->input('date_name_uz_'.$value);
                $childDate->date_name_ru = $request->input('date_name_ru_'.$value);
                $childDate->percent = $request->input('percent_'.$value);
                $childDate->status = 1;
                $childDate->save();

            }

        }
		if($request->input('compensation_status')){
			if(count($request->input('compensation_status')) > 1){
				$sorted = [];
				foreach($request->input('compensation_status') as $key => $value){
					$sorted += [$value => $request->input('percent_'.$value)];
				}
				asort($sorted);
				foreach($sorted as $value => $key){
					$childDate = CreditChildrenDate::where(['credit_id'=> $c->id, 'compensation_id'=> $value])->first();
					if(!$childDate){
						$childDate = new CreditChildrenDate();
					} 
					$childDate->credit_id = $c->id;
					$childDate->date_id = $request->input('type_date_filter')[0];
					$childDate->currency_id = $request->input('currency');
					$childDate->compensation_id = $value;
					$childDate->date = $request->input('date_'. $value);
					$childDate->date_type = $request->input('date_type_'.$value);
					$childDate->date_name_uz = $request->input('date_name_uz_'.$value);
					$childDate->date_name_ru = $request->input('date_name_ru_'.$value);
					$childDate->percent = $request->input('percent_'.$value);
					$childDate->status = 1;
					$childDate->save();
				}
			}
		
		}

    }

    public function addCredit(Request $request) {

        $c = new Credit();

        self::creditEdit($c,$request);

        $slug = $c->parent->slug;
        return redirect()->route('credits-type-view', $slug);
    }
    
    public function updateCreditView($id) {

        $credit = Credit::find($id);
        
        $credit_types = CreditType::all();
        $banks = Bank::all();
        $date_type = DateService::whereIn('id',[8,9,10])->get();
        $currency = Currency::all();
        $review_period = CreditReviewPeriod::all();
        $banks_type = TypeBanks::all();
        $car_brands = CarBrand::all();
        $car_brand_id = $credit->car_brand_id;
        return view('admin.credits.edit',compact(
            'car_brands',
            'car_brand_id',
            'credit',
            'banks',
            'date_type',
            'currency',
            'credit_types',
            'review_period',
            'banks_type'
        ));
    }
    
    public function updateCredit(Request $request) {
        $c =  Credit::find($request->id);

        self::creditEdit($c,$request);

        $slug = $c->parent->slug;
        return redirect()->route('credits-type-view', $slug);
    }

    
    public function creditView($id) {

        $credit = Credit::find($id);

        return view('admin.credits.view',compact('credit'));
    }
    public function deleteCredit($id) {

        $credit = Credit::find($id);
        $credit->delete();
        return redirect()->back();
    }
    
    //credit type
    
    public function creditTypeList() {
        
        $list = CreditType::all();
        
        return view('admin.credits.credit-types.list',compact('list'));
    }
    
    public function addTypeView() {
        
        $model = null;
        
        return view('admin.credits.credit-types.edit',compact('model'));
    }
    
    public function addType(Request $request) {
        
        $model = new CreditType();
        if(App::getLocale() == 'uz'){
            $model->name_uz = $request->name;
            $model->text_uz = $request->text;
            $model->slug = str_slug($request->name." ".rand(10000,99999),'-');
        } elseif(App::getLocale() == 'ru') {
            $model->name_ru = $request->name;
            $model->text_ru = $request->text;
            $model->slug = str_slug($request->name." ".rand(10000,99999),'-');
        }
        
        $model->save();
        
        return redirect()->route('credit-type-list');
    }
    
    public function updateTypeView($id) {

        $model = CreditType::find($id);
    
        return view('admin.credits.credit-types.edit',compact('model'));
    }
    
    public function updateType(Request $request) {
        $model = CreditType::find($request->id);
        if(App::getLocale() == 'uz'){
            $model->name_uz = $request->name;
            $model->text_uz = $request->text;
        } elseif(App::getLocale() == 'ru') {
            $model->name_ru = $request->name;
            $model->text_ru = $request->text;
        }
        $model->save();
        return redirect()->route('credit-type-list');
    }

    public function deleteType($id) {
        
        $model = CreditType::find($id);
        
        $model->delete();
        
        return redirect()->route('credit-type-list');
    }
    public function status(Request $request){

        $cr = Credit::find($request->id);

        $cr->status = $request->status;

        $cr->save();
    }

    public function translate(Request $request){
        foreach($request->input ('credits') as $key => $v){
            $c = Credit::find($v);
            $c->name_ru = $this->YTranslate($c->name_uz,'uz',$request->input ('lang'));
            $c->date_privilege_ru = $this->YTranslate($c->date_privilege_uz,'uz',$request->input ('lang'));
            $c->text_amount_ru = $this->YTranslate($c->text_amount_uz,'uz',$request->input ('lang'));
            $c->first_compensation_ru = $this->YTranslate($c->first_compensation_uz,'uz',$request->input ('lang'));
            $c->type_provision_ru = $this->YTranslate($c->type_provision_uz,'uz',$request->input ('lang'));
            $c->goal_credit_ru = $this->YTranslate($c->goal_credit_uz,'uz',$request->input ('lang'));
            $c->documents_ru = $this->YTranslate($c->documents_uz,'uz',$request->input ('lang'));
            $c->addition_rules_ru = $this->YTranslate($c->addition_rules_uz,'uz',$request->input ('lang'));
            $c->save();
        }
       return  back ();
    }

    public function YTranslate($attr, $langdf, $langtr){
        if($attr){
            return YandexTranslate::translate($attr,$langdf,$langtr);
        } else{
            return null;
        }
    }

    public function creditReviewList(Request $request){
        return self::responseSelect(CreditReviewPeriod::class,$request,'service_id');
    }

    public function creditIssuingForm(Request $request){
        return self::responseSelect(CreditIssuingForm::class,$request,'service_id');
    }

    public function creditGoalList(Request $request){
        return self::responseSelectGoals(CreditGoal::class,$request,'service_id');
    }

    public function creditCompensationList(Request $request){
        return self::responseSelectCompensations(CreditBoshBadal::class,$request,'service_id');
    }

    public function creditDateTypeList(Request $request){
        return self::responseSelectDate(DateService::class,$request,'service_id');
    }

    public function creditProvisionList(Request $request){
        return self::responseSelectProvision(CreditProvision::class,$request,'service_id');
    }

    public function creditBorrowCategory(Request $request){

        return self::responseSelect(CreditBorrowerCategory::class,$request,'service_id');
    }

    public function responseSelect($model,$request,$where){
        $option = '';
        $options = '';
        if($request->id){
            $list = $model::where($where,$request->id)->orderBy('name_uz','ASC')->get();

            foreach ($list as $item) {
                $select = '';
                if($request->review_id == $item->id){
                    $select = 'selected="selected"';
                }
                $option .='<option '.$select.' value="'.$item->id.'">'.$item->name().'</option>';
            }
            $options ='<option value="">-'.__('lang.choose').'-</option>'.$option;
        }

        return Response($options);
    }

    public function responseSelectDate($model,$request,$where){
        $option = '';
        $options = '';
        $date_ids = [];
        if($request->credit_id){
            $credit = Credit::find($request->credit_id);
           $date_ids = explode(',',$credit->type_date_filter); 
        }
        //dd($date_ids);
        if($request->id){
            $list = $model::where([$where => G_CREDIT_ID,'status' => 1])->where('child_service_id',$request->id)->get();

            foreach ($list as $item) {
                $select = '';
                if(in_array($item->id, $date_ids)){
                    $select = 'selected="selected"';
                }
                $option .='<option '.$select.' value="'.$item->id.'">'.$item->name().'</option>';
            }
            $options = $option;
        }

        return Response($options);
    }

    public function responseSelectGoals($model,$request,$where){
        $option = '';
        $options = '';
        $date_ids = [];
        if($request->credit_id){
            $credit = Credit::find($request->credit_id);
            $date_ids = explode(',',$credit->credit_goal_id); 
        }
        //dd($date_ids);
        if($request->id){
            $list = $model::where($where,$request->id)->where('status',1)->get();

            foreach ($list as $item) {
                $select = '';
                if(in_array($item->id, $date_ids)){
                    $select = 'selected="selected"';
                }
                $option .='<option '.$select.' value="'.$item->id.'">'.$item->name().'</option>';
            }
            $options = $option;
        }

        return Response($options);
    }

    public function responseSelectCompensations($model,$request,$where){
        $option = '';
        $options = '';
        $date_ids = [];
        if($request->credit_id){
            $credit = Credit::find($request->credit_id);
            $date_ids = explode(',',$credit->compensation_status); 
        }
        //dd($date_ids);
        if($request->id){
            $list = $model::where($where,$request->id)->get();

            foreach ($list as $item) {
                $select = '';
                if(in_array($item->id, $date_ids)){
                    $select = 'selected="selected"';
                }
                $option .='<option '.$select.' value="'.$item->id.'">'.$item->name().'</option>';
            }
            $options = $option;
        }

        return Response($options);
    }

    public function responseSelectProvision($model,$request,$where)
    {
        $option = '';
        $options = '';
        if($request->id){
            $ids = explode(',', $request->review_id);
            $list = $model::where($where,$request->id)->get();

            foreach($list as $row){
                $select= '';
                if(in_array($row->id, $ids)){
                    $select = 'selected="selected"';

                }
                $option .='<option '.$select.' value="'.$row->id.'">'.$row->name().'</option>';
            }
            $options ='<option value="">-'.__('lang.choose').'-</option>'.$option;
        }
        return Response($options);
    }

    public function credit_children_date(Request $request)
    {
        
        $dates = [];
        $model = [];
        $mod = [];

        if($request->delete_id){
            $mod = CreditChildrenDate::find($request->delete_id);
            $mod->delete();
            $request->date_ids = array_diff($request->date_ids, [$request->delete_id]);
        }
        
        if(count($request->date_ids) > 1){
            if($request->credit_id){
                $mod = CreditChildrenDate::where('credit_id',$request->credit_id)->get();
            }
            if(count($mod) <= count($request->date_ids)){
                foreach($request->date_ids as $k => $v){
                    if($request->credit_id){
                        $m = CreditChildrenDate::where('credit_id',$request->credit_id)->where('date_id',$v)->first();
                        if($m){
                            $model[]= $m;
                        } else {
                            $dates [] = DateService::find($v); 
                        }
                    } else {
                        $dates [] = DateService::find($v);
                    }
                }
            } else {
                $model = $mod;
            }
        }
        $date_type_id = DateService::find(YMD);
        //$credit = Credit::find($request->credit_id);
        $compensation = CreditBoshBadal::where('service_id',$request->cr_type_id)->get();
        $isDate = true;
        return view('admin.credits.components.child_date_form',compact(
            'dates', 'model','date_type_id','compensation','isDate'
        ))
        ->render();
    }
    public function credit_children_compensation_date(Request $request)
    {
        
        $compensations = [];
        $model = [];
        $mod = [];
        if($request->delete_id){
            $mod = CreditChildrenDate::destroy($request->delete_id);
            $request->compensation_ids = array_diff($request->compensation_ids, [$request->delete_id]);
        }
        if(isset($request->compensation_ids) && count($request->compensation_ids) > 1){
            if($request->credit_id){
                $mod = CreditChildrenDate::where('credit_id',$request->credit_id)->get();
            }
            if(count($mod) <= count($request->compensation_ids)){
                foreach($request->compensation_ids as $k => $v){
                    if($request->credit_id){
                        $m = CreditChildrenDate::where('credit_id',$request->credit_id)->where('compensation_id',$v)->first();
                        if($m){
                            $model[]= $m;
                        } else {
                            $compensations [] = CreditBoshBadal::find($v); 
                        }
                    } else {
                        $compensations [] = CreditBoshBadal::find($v); 
                    }
                }
            } else {
                $model = $mod;
            }
        }
        $date_type_id = DateService::find(YMD);
        //$credit = Credit::find($request->credit_id);
        if(isset($request->date_id[0]))
        $date = DateService::find($request->date_id[0]);
        $isDate = false;
        return view('admin.credits.components.child_date_form',compact(
            'date', 'model','date_type_id','compensations', 'isDate'
        ))
        ->render();
    }

}
