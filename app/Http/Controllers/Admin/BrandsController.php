<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CarBrand;
use Illuminate\Http\Request;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $car_brands = CarBrand::all();
        return view('admin.cars.carBrands.list', compact('car_brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->id !== null) {
            $carBrand = CarBrand::findOrFail($request->id);
        } else {
            $carBrand = new CarBrand();
        }
        if($request->input('name_uz') !== null) $carBrand->name_uz = $request->input('name_uz');
        else return redirect()->back()->with('error', 'Nomi kiritilmagan');
        if($request->input('name_ru') !== null) $carBrand->name_ru = $request->input('name_ru');

        $carBrand->save();
        return redirect()->back()->with('success', 'Brand qo\'shildi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carBrand = CarBrand::findOrFail($id);
        $carBrand->delete();
        return redirect()->back()->with('success', 'Brand o\'chirildi');
    }
}
