<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Currency;
use App\Models\DateService;
use App\Models\Deposit;
use App\Models\DepositPayPercentPeriod;
use App\Models\DepositsDate;
use App\Models\DepositType;
use App\Models\TypeOpenDeposit;
use App\Models\DepositChildren;
use App\Models\Credit;
use App\Models\CreditCards;
use App\Models\DebitCards;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use YandexTranslate;



class DepositsController extends Controller
{

    public function dataTranslate(Request $request){
        $all = Deposit::all();

        foreach ($all as $row) {
            $column = $request->input('column');
            
            if($row->$column){
               // dd($row->$column);
                $row->for_json = YandexTranslate::translate($row->$column,'uz','ru');
                $row->save();
            }
        }
        return back();
    }

    public function dataLang(Request $request){
        
        $model = $request->model;
        $all = self::$model();
        foreach($all as $row){
             $column = $request->input('column');
            if($row->$column){
                $lang = [
                    'uz' => $row->$column,
                    'ru' => $row->$column
                ];
                $row->$column = json_encode($lang);
                //$row->for_json = '';
                $row->save();
            }
        }
        return back();
    }

    public function Deposit()
    {
        return Deposit::all();
    }
    public function Credit()
    {
        return Credit::all();
    }
    public function CreditCards()
    {
        return CreditCards::all();
    }
    public function DebitCards()
    {
        return DebitCards::all();
    }

    public function translatePage()
    {
        return view('admin.translate-column.deposits');
    }
    /** end column translate **/



    /** Omonat turlari**/
    public function depositTypeList() {
        
        $list = DepositType::all();
        return view('admin.deposits.deposit-types.list',compact('list'));
    }

    public function typeEdit(Request $request) {
        if(!empty($request->id)){
            $type = DepositType::find($request->id);
        }if(empty($request->id)){
            $type = new DepositType();
        }
        $type->name_uz = $request->name_uz;
        $type->name_ru = $request->name_ru;
        $type->description_uz = $request->description_uz;
        $type->description_ru = $request->description_ru;

        $type->save();
        return redirect()->route('deposit-type-list');
    }

    public function typeDelete($id) {
        $d = DepositType::find($id);
        $d->delete();
        return redirect()->route('deposit-type-list');
    }
    /** End Omonat turlari**/

    /** Omonatlar**/
    public function depositList() {
        $list = Deposit::orderBy('status', 'desc')->get();
        return view('admin.deposits.list',[
            'list' => $list,
        ]);
    }
    
    public function addView() {
        $deposit = '';
        if(App::getLocale() == 'uz') {
         $name = "name_uz";
        } elseif(App::getLocale() == 'ru') {
            $name = "name_ru";
        }
        
        $sumType = config('global.type_sum');
        $banks = Bank::all()->sortBy($name);
        $deposit_type = DepositType::all();
        $date_type = DateService::where('service_id',config ('global.services.deposit'))->where('status',1)->get();
        $currency = Currency::all();
        $paid_period = DepositPayPercentPeriod::all();
        $type_open = TypeOpenDeposit::all();
        $date_type_id = DateService::find([8,9,10]);
        return view('admin.deposits.edit',compact(
            'deposit',
            'banks',
            'deposit_type',
            'date_type',
            'currency',
            'sumType',
            'paid_period',
            'type_open',
            'date_type_id'
        ));
    }
    
    public function add(Request $request) {
        $d = new Deposit();

        if($request->input('bank')){
            $bank = Bank::find($request->input('bank'));
            $d->region_id = $bank->region_id;
            $d->bank_type_id = $bank->information->type_bank;
        }
        $d->name_uz = $request->name_uz;
        $d->text_uz = $request->text_uz;
        $d->name_ru = $request->name_ru;
        $d->text_ru = $request->text_ru;
        $d->bank_id = $request->input('bank');
        
       
        $d->other_percent = $request->input('other_percent');
    

        $d->slug = str_slug($request->name_uz." ".rand(10000,99999),"-");
       
        $d->deposit_type_id = $request->input('deposit_type');
        
        $date_lang = [
            'uz' => $request->input('deposit_date_string_uz'),
            'ru' => $request->input('deposit_date_string_ru')
        ];
        $d->deposit_date_string = json_encode($date_lang);
        
        $dates =  implode(',', $request->input('date_type'));
        $d->deposit_date_type = $dates;
        

        $d->deposit_date = $request->input('deposit_date');
        
        $d->date_type_id = $request->input('date_type_id');
        $d->currency = $request->input('currency');

        if(!empty($request->input('min_sum'))){
            $d->min_sum = $request->min_sum;
        } else {
            $d->min_sum = 0;
        }
        $d->type_sum = $request->type_sum;
        $d->deposit_percent = $request->percent;

        $d->aktsiya = $request->input('aktsiya') > 0 ? 1 : 0;
        $izoh = [
            'uz' => $request->input('aktsiya_izoh_uz'),
            'ru' => $request->input('aktsiya_izoh_ru')
        ];
        $d->aktsiya_izoh = json_encode($izoh);
        $link = [
            'uz' => $request->input('aktsiya_link_uz'),
            'ru' => $request->input('aktsiya_link_ru')
        ];
        $d->aktsiya_link = json_encode($link);

        $d->account_fill = $request->input('account_fill');
        $afr = [
            'uz' => $request->input('account_fill_rules_uz'),
            'ru' => $request->input('account_fill_rules_ru')
        ];
        $d->account_fill_rules = json_encode($afr);

        $d->partly_take = $request->input('partly_take');
        $ptr = [
            'uz' => $request->input('partly_take_rules_uz'),
            'ru' => $request->input('partly_take_rules_ru')
        ];
        $d->partly_take_rules = json_encode($ptr);
        
        $d->percents_capitalization = $request->input('percents_capitalization');
        $pcr = [
            'uz' => $request->input('percents_capitalization_rules_uz'),
            'ru' => $request->input('percents_capitalization_rules_ru'),
        ];
        $d->percents_capitalization_rules = json_encode($pcr);

        if($request->input('privilege_take')){
            $d->privilege_take = $request->input('privilege_take');
        }
        $ptt = [
            'uz' => $request->input('privilege_take_text_uz'),
            'ru' => $request->input('privilege_take_text_ru')
        ];
        $d->privilege_take_text = json_encode($ptt);

        
        if($request->input('type_open')){
            $openT = implode(',', $request->input('type_open'));
            $d->type_open = $openT;
        }
        $ott=[
            'uz' => $request->input('open_type_text_uz'),
            'ru' => $request->input('open_type_text_ru'),
        ];
        $d->open_type_text = json_encode($ott);

        $d->percent_paid_period = $request->input('percent_paid_period');
        //$d->close_before_date = $request->close_before_date ? : 0;

        $link=[
            'uz' => $request->input('link_uz'),
            'ru' => $request->input('link_ru'),
        ];
        $d->link = json_encode($link);

        $d->save();

        if(count($request->input('date_type')) > 1){
            foreach ($request->input('date_type') as $key => $value) {
                $model = new DepositChildren();
                $model->deposit_id = $d->id;
                $model->date_id = $value;
                $model->currency_id = $request->input('currency');
                $model->percent = $request->input('deposit_date_type_percet_'.$value);
                $model->date_name_for_site = $request->input('date_name_for_site_'.$value);
                $model->date_type = $request->input('deposit_date_type_'.$value);
                $model->date = $request->input('deposit_date_'.$value);
                $model->save();
            }  
        }
        $amount_input = $request->diff_date_amount_input;
        if((int)$amount_input > 0){
            for($i = 1; $i <= (int)$amount_input; $i++){
                $request_date_name = 'diff_date_input_'.$i;
                $request_percent_name = 'diff_percent_input_'.$i;
                $request_date_type_name = 'diff_date_type_input_'.$i;
                $request_percent_consider_name = 'diff_percent_consider_input_'.$i;

                $deposit_date = new DepositsDate();
                $deposit_date->deposit_id = $d->id;
                $deposit_date->date = $request->$request_date_name;
                $deposit_date->percent = $request->$request_percent_name;
                $deposit_date->date_type = $request->$request_date_type_name;
                $deposit_date->percent_consider = $request->$request_percent_consider_name;
                $deposit_date->save();
            }
        }
        return redirect()->route('bank-deposits',$request->input('bank'));
    }
    
    public function updateView($id) {

        $deposit = Deposit::find($id);

        $sumType = config('global.type_sum');
        $banks = Bank::all()->sortBy('name_uz');
        $deposit_type = DepositType::all();
        $date_type = DateService::where('service_id',config ('global.services.deposit'))->where('status',1)->get();
        $currency = Currency::all();
        $paid_period = DepositPayPercentPeriod::all();
        $type_open = TypeOpenDeposit::all();
         $date_type_id = DateService::find([8,9,10]);
        return view('admin.deposits.edit',compact(
            'deposit',
            'banks',
            'deposit_type',
            'date_type',
            'currency',
            'sumType',
            'paid_period',
            'type_open',
            'date_type_id'
        ));
    }
    
    public function update(Request $request) {
        
        $d = Deposit::find($request->id);
        //dd($d);
        if($request->input('bank')){
            $bank = Bank::find($request->input('bank'));
            $d->region_id = $bank->region_id;
            $d->bank_type_id = $bank->information->type_bank;
        }
        
        $d->bank_id = $request->input('bank');
    
        if(!$d->slug) {
            $d->slug = str_slug($request->name_uz." ".rand(10000,99999),"-");
        }
        $d->name_uz = $request->name_uz;
        $d->text_uz = $request->text_uz;
        $d->name_ru = $request->name_ru;
        $d->text_ru = $request->text_ru;

        
        $d->other_percent = $request->input('other_percent');
       

        $date_lang = [
            'uz' => $request->input('deposit_date_string_uz'),
            'ru' => $request->input('deposit_date_string_ru')
        ];
        $d->deposit_date_string = json_encode($date_lang);

        $d->deposit_type_id = $request->input('deposit_type');

        $dates =  implode(',', $request->input('date_type'));
        $d->deposit_date_type = $dates;

        if(count($request->input('date_type')) > 1){
            foreach ($request->input('date_type') as $key => $value) {
                $model = DepositChildren::where(['deposit_id'=> $d->id,'date_id' => $value])->first();
                if(!$model){
                    $model = new DepositChildren();
                }

                $model->deposit_id = $d->id;
                $model->date_id = $value;
                $model->currency_id = $request->input('currency');
                $model->percent = $request->input('deposit_date_type_percet_'.$value);
                $model->date_name_for_site = $request->input('date_name_for_site_'.$value);
                $model->date_type = $request->input('deposit_date_type_'.$value);
                $model->date = $request->input('deposit_date_'.$value);
                $model->save();
            }  
        }
        
        $d->deposit_date = $request->input('deposit_date');
        $d->date_type_id = $request->input('date_type_id');
        $d->currency = $request->input('currency');
      
        if(!empty($request->input('min_sum'))){
            $d->min_sum = $request->min_sum;
        } else {
            $d->min_sum = 0;
        }
        $d->type_sum = $request->type_sum;
        $d->deposit_percent = $request->percent;

        $d->aktsiya = $request->input('aktsiya') > 0 ? 1 : 0;
        $izoh = [
            'uz' => $request->input('aktsiya_izoh_uz'),
            'ru' => $request->input('aktsiya_izoh_ru')
        ];
        $d->aktsiya_izoh = json_encode($izoh);
        $link = [
            'uz' => $request->input('aktsiya_link_uz'),
            'ru' => $request->input('aktsiya_link_ru')
        ];
        $d->aktsiya_link = json_encode($link);

        $d->account_fill = $request->input('account_fill');
        $afr = [
            'uz' => $request->input('account_fill_rules_uz'),
            'ru' => $request->input('account_fill_rules_ru')
        ];
        $d->account_fill_rules = json_encode($afr);

        $d->partly_take = $request->input('partly_take');
        $ptr = [
            'uz' => $request->input('partly_take_rules_uz'),
            'ru' => $request->input('partly_take_rules_ru')
        ];
        $d->partly_take_rules = json_encode($ptr);
        
        $d->percents_capitalization = $request->input('percents_capitalization');
        $pcr = [
            'uz' => $request->input('percents_capitalization_rules_uz'),
            'ru' => $request->input('percents_capitalization_rules_ru'),
        ];
        $d->percents_capitalization_rules = json_encode($pcr);

        
        $d->privilege_take = $request->input('privilege_take');
        
        $ptt = [
            'uz' => $request->input('privilege_take_text_uz'),
            'ru' => $request->input('privilege_take_text_ru')
        ];
        $d->privilege_take_text = json_encode($ptt);

        
        if($request->input('type_open')){
            $openT = implode(',', $request->input('type_open'));
            $d->type_open = $openT;
        }
        $ott=[
            'uz' => $request->input('open_type_text_uz'),
            'ru' => $request->input('open_type_text_ru'),
        ];
        $d->open_type_text = json_encode($ott);

        if($request->input('percent_paid_period') or $request->input('percent_paid_period') != 0){
            $d->percent_paid_period = $request->input('percent_paid_period');
        }
        /*$d->close_before_date = $request->close_before_date ? : 0;
        $d->close_before_date_rules = $request->input('close_before_date_rules');*/

        $link=[
            'uz' => $request->input('link_uz'),
            'ru' => $request->input('link_ru'),
        ];
        $d->link = json_encode($link);

        $amount_input = $request->diff_date_amount_input;
        if((int)$amount_input > 0){
            for($i = 1; $i <= (int)$amount_input; $i++){
                $request_date_name = 'diff_date_input_'.$i;
                $request_percent_name = 'diff_percent_input_'.$i;
                $request_date_type_name = 'diff_date_type_input_'.$i;
                $request_percent_consider_name = 'diff_percent_consider_input_'.$i;
                $request_deposit_type_id = 'diff_deposit_date_type_input_'.$i;
                if($request->input($request_deposit_type_id)){
                    $deposit_date = DepositsDate::find($request->$request_deposit_type_id);

                }else{
                    $deposit_date = new DepositsDate();
                }
                $deposit_date->deposit_id = $d->id;
                $deposit_date->date = $request->$request_date_name;
                $deposit_date->percent = $request->$request_percent_name;
                $deposit_date->date_type = $request->$request_date_type_name;
                $deposit_date->percent_consider = $request->$request_percent_consider_name;
                $deposit_date->save();
            }
        }
        $d->save();

        return redirect()->route('bank-deposits',$request->input('bank'));
    }
    
    public function delete($id) {
        $d = Deposit::find($id);
        $d->delete();
        return redirect()->route('deposit-list');
    }

    public function status(Request $request){
        $dp = Deposit::find($request->id);
        $dp->status = $request->status;
        $dp->save();
    }

    public function depositDifferDate(Request $request){
        $d = DepositsDate::find($request->d_date_id);
        $d->delete();
        return response()->json('Successfully');
    }

    /** End Omonatlar**/

    public function openTypeDepositList(){
        $list = TypeOpenDeposit::all();
        return view('admin.deposits.open-type.list',compact('list'));
    }

    public function openTypeDepositEdit(Request $request){
        if(!empty($request->id)){
            $model = TypeOpenDeposit::find($request->id);
        }if(empty($request->id)){
            $model = new TypeOpenDeposit();
        }
        $model->name_uz = $request->name_uz;
        $model->name_ru = $request->name_ru;
        $model->save();
        return redirect()->back();
    }
    public function openTypeDepositDelete($id){
        if($id) {
            $model = TypeOpenDeposit::find($id);
            $model->delete();
        }
        return redirect()->back();
    }

    public function paidPercentDepositList(){
        $list = DepositPayPercentPeriod::all();
        return view('admin.deposits.period-percent.list',compact('list'));
    }

    public function paidPercentDepositEdit(Request $request){
        if(!empty($request->id)){
            $model = DepositPayPercentPeriod::find($request->id);
        }if(empty($request->id)){
            $model = new DepositPayPercentPeriod();
        }
        $model->name_uz = $request->name_uz;
        $model->name_ru = $request->name_ru;
        $model->save();
        return redirect()->back();
    }
    public function paidPercentDepositDelete($id){
        if($id) {
            $model = DepositPayPercentPeriod::find($id);
            $model->delete();
        }
        return redirect()->back();
    }

    public function depositView($id){
        $deposit = Deposit::find($id);
        return view('admin.deposits.view',compact('deposit'));
    }

    public function deposit_date_type_percent_input(Request $request){
        
        $dates=[];
        $model = [];
        $mod = [];
        if($request->delete_id){
            $d = DepositChildren::find($request->delete_id);
            $d->delete();
            $request->date_type_id = array_diff($request->date_type_id, [$request->delete_id]);

        }
        if(count($request->date_type_id) > 1){
            if($request->id_deposit){
                $mod = DepositChildren::where('deposit_id',$request->id_deposit)->get();
            }

            if(count($mod) <= count($request->date_type_id)){
                foreach($request->date_type_id as $k => $v){
                    if($request->id_deposit){
                        $m = DepositChildren::where('deposit_id',$request->id_deposit)->where('date_id',$v)->first();
                        if($m){
                            $model []= $m;
                        } else {
                            $dates[] = DateService::find($v);
                        }
                    } else {
                        $dates[] = DateService::find($v);
                    }
                }
            } else {
                $model = $mod;
            }

        }

        //dd($dates);
        $date_type_id = DateService::find([8,9,10]);
        return view('admin.deposits.components.deposit-date-type-percent',
        compact(
            'dates','model','date_type_id'
        ))->render();  
    }

}
