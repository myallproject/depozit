<?php

namespace App\Http\Controllers\Admin;

use App;
use App\Http\Controllers\Controller;
use App\Models\Card;
use App\Models\CreditType;
use App\Models\DateService;
use App\Models\Service;
use Illuminate\Http\Request;

class DateServiceController extends Controller
{
    public function dateList() {
        $list = DateService::all();
        return view('admin.date-service.list',compact('list'));
    }
    
    public function addView() {
        
        $date_type = null;
        $services = Service::all();
        return view('admin.date-service.edit',compact('date_type','services'));
    }

    public function edit($model, $request){

        if($request->input('service') == 2 or $request->input('service') == 3){
            $model->child_service_id = $request->input('child_service');
        }
        $model->status = 0;
        $model->description = $request->description;
        $model->service_id = $request->input('service');

        $model->name_uz = $request->input('name_uz');
        $model->name_ru = $request->input('name_ru');

        $model->save();
    }
    public function add(Request $request) {
        $model = new DateService();

        self::edit($model,$request);

        return redirect()->route('date-type-list');
    }
    
    public function updateView($id) {
        $date_type = DateService::find($id);
        $services = Service::all();
        return view('admin.date-service.edit',compact('date_type','services'));
    }
    
    public function update(Request $request) {
    
        $model = DateService::find($request->id);

        self::edit($model,$request);

        return redirect()->route('date-type-list');
    }
    
    public function delete($id) {
        
        $d = DateService::find($id);
        
        $d->delete();
        
        return redirect()->back();
    }
    
    public function position(Request $request){
        $d = DateService::find($request->id);

        $d->position = $request->input('position');

        $d->save();
        
        return response()->json(['name'=>$d->name(),'position' => $d->position,'id' => $d->id]);
    }


    public function creditChildren(Request $request){
        $children = [];
        if($request->id == 2)
            $children = CreditType::all();
        if($request->id == 3)
            $children = Card::all();

        $option = '';

        foreach($children as $child){
            $select = '';
            if($child->id == $request->id_child){
                $select = 'selected="selected"';
            }
            $option .='<option '.$select.' value="'.$child->id.'">'.$child->name().'</option>';
        }
        return Response($option);
    }

    public function changeStatus(Request $request)
    {
        $model = DateService::find($request->id);

        if($model){
            $model->status = $request->status;
            $model->save();
        }

        return response()->json(['error'=>false,'msg'=>'Success!', 'status' => $request->status,'id'=>$request->id]);
    }

    public function changeStatusAll($status)
    {
        $all = DateService::all();

        foreach ($all as $row) {
            $row->status = $status;
            $row->save();
        }

        return redirect()->back();
    }
}
