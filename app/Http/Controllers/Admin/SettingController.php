<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PagesMetaTags;
use App\Models\CurrencyAPI;
use App\Models\Bank;
use App\Models\Notification;
use Artisan;


class SettingController extends Controller
{
    public function metaTagsList()
    {
    	$model = PagesMetaTags::where('parent_id',0)->get();

    	return view('admin.setting.meta-tag.page-meta-tag',compact('model'));
    }
    

    public function metaTagInputs(Request $request)
    {
    	$model = [];
        $parent = [];
    	if($request->input('id')){
           $parent = PagesMetaTags::find($request->input('id')); 
			$model = PagesMetaTags::where('parent_id',$request->input('id'))->get();
    	} else {
    		$model = [];
    	}
    	//dd($request);
    	return view('admin.setting.meta-tag.inputs',compact('model','parent'))->render();
    }

    public function metaTagEdit(Request $request)
    {
    	if(!$request->input('id')){
    		$model = new PagesMetaTags();
    		self::metaAdd($model, $request);
    	} else {
			$model = PagesMetaTags::find($request->input('id'));
    		self::metaUpdate($model, $request);
    	}	
    	return redirect()->back();
    }

    protected function metaAdd($model, $request){

    	$model->page = $request->page;
    	$model->tag = $request->tag;
    	$model->save();    	
    }

    protected function metaUpdate($model, $request){
        for($i = 1; $i <= (int)$request->input('input_amount'); $i++){
			$attribute = 'attribute_'.$i;  			
			$data_uz = 'data_uz_'.$i;
			$child_id = 'child_id_'.$i;
			$data_ru = 'data_ru_'.$i;
            $text_uz = 'text_uz_'.$i;
            $text_ru = 'text_ru_'.$i;

			if($request->$child_id){
				$child = PagesMetaTags::find($request->$child_id);
                if(!$child){
                    $child = new PagesMetaTags();
                }
			} else {
				$child = new PagesMetaTags();
			}
           
			$child->page = $model->page;
			$child->parent_id = $model->id;
            
            if($model->tag and strpos($model->tag, '|')){
                $arraytext = [
                    'uz' => $request->text_uz,
                    'ru' => $request->text_ru
                ];
                $jsontext = json_encode($arraytext);
                $model->text = $jsontext;
                $model->save();
            }

			$child->attribute = $request->$attribute;

			$arraydata = [
				'uz' => $request->$data_uz,
				'ru' => $request->$data_ru
			];
			$json = json_encode($arraydata);
			$child->data = $json;

            

			$child->save();
    	}

        if($request->text_uz or $request->text_ru){
            $child = PagesMetaTags::find($request->id);
            $arraytext = [
                'uz' => $request->text_uz,
                'ru' => $request->text_ru
            ];
            $jsontext = json_encode($arraytext);
            $child->text = $jsontext;
            $child->save();
        }
    }

    public function metaDelete($id)
    {
    	if($id){
    		$model = PagesMetaTags::find($id);
    		if($model){
    			$childen = PagesMetaTags::where('parent_id',$model->id)->get();
    			$model->delete();

    			foreach ($childen as $child) {
    				$child->delete();
    			}
    		}
    	}

    	return redirect()->back();
    } 

    public function metaDeleteOne(Request $request)
    {
        if($request->id){
            $model = PagesMetaTags::find($request->id);
            if($model){
                $model->delete();
            }
        }
    }

    public function currencyAPIlist(){
        
       $api = CurrencyAPI::all();
        $organizations = Bank::orderBy('name_uz')->get();
       
        $table_nots = Notification::where('table_name','currency_a_p_i_s')->where('status',1)->get();

        return view('admin.setting.currency-api.list',compact('api','organizations','table_nots'));
    }

    public function currencyAPIEdit(Request $request)
    {
        if($request->input('id')){
            $model = CurrencyAPI::find($request->input('id'));
        } else {
            $model = new CurrencyAPI();
        }   
        $model->organization_id = $request->input('org_id');
        $model->api = $request->input('api');
        $model->save();

        return redirect()->back();
    }

    public function rowPosition(Request $request)
    {
        $data = [
            'status' => false,
            'id' => false,
            'position' => false
        ];
        if($request->input('model')){
            $model_name  = $request->input('model');
            $model = app("App\Models".'\\'."$model_name");

            $model = $model::find($request->input('id'));

            $model->position = $request->input('position');
            $model->save();

            $data = [
                'status' => true,
                'id' => $model->id,
                'position' => $model->position
            ];
        }

        return response()->json($data);
    }


    public function caching()
    {
        return view('admin.setting.caching');
    }

    public function artisanCommand($command){
        if($command){
            Artisan::call($command);
        }
        return redirect()->back(); 
    }

    public function downloadCronResult(){
        $path = public_path('../crontab.txt');
        return response()->download($path);
    }

}