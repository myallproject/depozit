<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\PlayMobileService;
use App\Notifications\ReviewNotification;
use App\Models\Review;
use App\Models\Region;
use App\Models\Bank;
use App\Models\Comment;
use App\Models\Service;
use App\Models\Notification;
use App\User;
use App;
use Auth;

class ReviewController extends Controller
{
    protected $playMobileService;

    public function __construct(PlayMobileService $playMobileService){
        $this->playMobileService = $playMobileService;
    }

    public function reviewList(Request $request)
    {  
        $list = Review::orderBy('updated_at','DESC')->get();
       /* foreach($list as $row){
            dd($row->moderatorComment($row->id));
        }*/
    	return view('admin.review.list',compact('list'));
    }
    
    public function reviewAdd()
    {
    	$lang = App::getLocale();

        $regions = Region::orderBy('name_'.App::getLocale(),'ASC')->get();

    	$banks = Bank::orderBy('name_'.App::getLocale(),'ASC')->get();

    	$users = User::all();

        $services = Service::whereNull('parent_id')->where('status',1)->orderBy('name_'.$lang)->get();
    	
        return view('admin.review.add',compact('model','regions','banks','users','services'));
    }
    
    public function reviewUpdate($id)
    {
    	$model = Review::find($id);
   		
    	$regions = Region::orderBy('name_'.App::getLocale(),'ASC')->get();

    	$banks = Bank::orderBy('name_'.App::getLocale(),'ASC')->get();

        $services = Service::whereNull('parent_id')->where('status',1)->orderBy('name_'.App::getLocale())->get();

    	return view('admin.review.update',compact('model','regions','banks','services'));
    }

    public function reviewEdit(Request $request)
    {
    	if($request->id){
    		$model = Review::find($request->id);
    	} else {
    		$model = new Review();
    	}

        //sending notification
        $this->notifyMe($request->input('status'), $request->input('comment_site'), $model);

    	$model->title = $request->input('title');
    	$model->fulltext = $request->input('fulltext');
    	$model->assessment = $request->input('assessment');
        $model->info_bank = $request->input('info_bank');
   
    	$model->user_id = $request->input('user');
    	$model->bank_id = $request->input('bank');
    	$model->branch_id = $request->input('branch');
    	$model->service_id = $request->input('service');
    	$model->region_id = $request->input('region');
    	$model->status = $request->input('status');
        $model->answer_status = $request->input('answer_status');


        if($request->input('comment_site')){
            if(!empty($model->comment_site)){
                $comment = Comment::find($request->comment_site_id);
            } else{
                $comment = new Comment();
            }
           // dd($comment);
            $comment->text = $request->input('comment_site');
            $comment->review_id = $model->id;
            $comment->user_id = Auth::user()->id;
            $comment->status = 1;

            $comment->save();
            $model->comment_site = $comment->id;
        } else {
            $comment = Comment::find($request->comment_site_id);
            if($comment){
                $compact->delete();
            }
            $model->comment_site = null;
        }

        $model->save();

        $not = Notification::where('row_id', $model->id)->where('table_name',$model->getTable())->first();
        if($not){
            $not->delete();
        }
        return redirect()->route('admin-review-list');
    }

    public function reviewDelete($id)
    {
    	if($id){
    		$model = Review::find($id);
    		if($model){
    			$model->delete();
    		}
    	} 

    	return redirect()->back();
    }

    public function reviewCommentEdit(Request $request)
    {
        $data = ['text'=>'','id'=>''];
        if($request->input('id')){
            $model = Comment::find($request->input('id'));
            $model->text = $request->text;
            $model->status = 1;
            $model->save();
            $data = [
                'text' => $request->text,
                'id' => $request->id
            ];
        }
        return response()->json($data);
    }
    public function reviewCommentDelete($id)
    {
        if($id){
            $model = Comment::find($id);       
            $model->delete();
        }
        return redirect()->back();
    }

    protected function notifyMe($status, $comment, Review $review){
            $msg='';
            $notf='';
            if($status != $review->status){
                if($status == 0){
                    $msg.='Assalomu alaykum hurmatli mijoz! Siz qoldirgan fikr bahosi moderator tomonidan rad etildi. ';
                    $notf.='Siz qoldirgan fikr bahosi moderator tomonidan rad etildi. ';
                }
                if($status == 1){
                    $msg.='Assalomu alaykum hurmatli mijoz! Sizning izohingiz sayt moderatori tomonidan tekshirilmoqda. ';
                    $notf.='Sizning izohingiz sayt moderatori tomonidan tekshirilmoqda. ';
                }
                if($status ==2){
                    $msg.='Assalomu alaykum hurmatli mijoz! Siz qoldirgan fikr bahosi qabul qilindi. Bank hodimining javobini kuting. ';
                    $notf.='Siz qoldirgan fikr bahosi qabul qilindi. Bank hodimining javobini kuting. ';
                }
            }
            if(!($comment === $review->moderatorComment($review->id)['text'])){
                $msg.='Depozit.uz moderatori: ';
                $msg.=$comment;
                $notf.='Depozit.uz moderatori: ';
                $notf.=$comment;
            }
            $user = User::find($review->user_id);
            $statusCode=0;
            if($msg != ''){
                if($user->phone){
                    $statusCode = $this->playMobileService->sendMessage($user->getPhone(), $msg, 'https://depozit.uz/uz/reviews/'.$review->id);
                }
                if($statusCode!=200 && $user->email){
                    $user->notify(new ReviewNotification('https://depozit.uz/uz/reviews/'.$review->id, $notf));
                }
            }
    }
}
