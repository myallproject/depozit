<?php

namespace App\Http\Controllers\Admin;

use App;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    public function currencyList() {
        
        $list = Currency::paginate(20);
        
        return view('admin.currency.list',compact('list'));
    }
    
    public function addView() {
        
        $currency = null;
        
        return view('admin.currency.edit',compact('currency'));
    }
    
    public function add(Request $request) {
        $c = new Currency();
        
        $c->name = $request->name;
        $c->description = $request->description;
        
        $c->save();
        
        return redirect()->route('currency-list');
    }
    
    public function updateView($id) {
        $currency = Currency::find($id);
    
        
        return view('admin.currency.edit',compact('currency'));
    }
    
    public function update(Request $request) {
        $c = Currency::find($request->id);

        $c->name = $request->name;
        $c->description = $request->description;

        $c->save();
        
        return redirect()->route('currency-list');
    }
    
    public function delete($id) {
        $c = Currency::find($id);
        
        $c->delete();
        
        return redirect()->route('currency-list');
    }
}