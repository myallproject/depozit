<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\CardType;
use App\Models\Currency;
use App\Models\DateService;
use App\Models\DebitCards;
use App\Models\PaymentType;
use Illuminate\Http\Request;

const CARD_PATH = "uploads/cards/debit/";

class DebitCardController extends Controller
{
    protected $attributes = [
        'id' => '',
        'bank_id' => '',
        'name_uz' => '',
        'name_ru' => '',
        'type_card_id' => '',
        'currency_id' => [],
        'payment_id' => '',
        'date_id' => '',
        'price' => '',
        'kamay_qoldiq' => '',
        'qushimcha_qulaylik_uz' => '',
        'qushimcha_qulaylik_ru' => '',
        'rasmiy_yuli_uz' => '',
        'rasmiy_yuli_ru' => '',
        'qushimcha_shart_uz' => '',
        'qushimcha_shart_ru' => '',
        'link' => '',
        'bepul_xizmat' => '',
        'bepul_naqdlash' => '',
        'bepul_tulov' => '',
        'kontaktsiz_tulov' => '',
        'cash_back' => '',
        'uyga_yetkazish' => '',
        'tuluv_komissia' => '',
        'naqdlash_komissia' => '',
        'documents_ru' => '',
        'documents_uz' => '',
        'naqdlash_komissia_boshqa' => '',
        'kontaksiz_tolov_izoh_uz' => '',
        'kontaksiz_tolov_izoh_ru' => '',
        'cash_back_izoh_uz' => '',
        'cash_back_izoh_ru' => '',
        'uyga_yetkazish_izoh_uz' => '',
        'uyga_yetkazish_izoh_ru' => '',
        'tuluv_komissia_izoh_uz' => '',
        'tuluv_komissia_izoh_ru' => '',
        'naqdlash_komissia_izoh_uz' => '',
        'naqdlash_komissia_izoh_ru' => '',
        'naqdlash_komissia_boshqa_izoh_uz' => '',
        'naqdlash_komissia_boshqa_izoh_ru' => '',
        'price_izoh_uz' => '',
        'price_izoh_ru' => '',
        'tuluv_komissia_boshqa' => '',
        'tuluv_komissia_boshqa_izoh_uz' => '',
        'tuluv_komissia_boshqa_izoh_ru' => '',
        'naqdlash_komissia_terminal' => '',
        'naqdlash_komissia_terminal_izoh_uz' => '',
        'naqdlash_komissia_terminal_izoh_ru' => '',
        'urgent_opening' => '',
        're_issue_card' => '',
        'sms_inform' => '',
        'service_uz' => '',
        'service_ru' => '',
        'aktsiya' => '',
        'aktsiya_izoh' => '',
        'aktsiya_link' => '',
        'qushimcha_karta_ochish' => '',
        'd_secure' => '',
        'lounge_key' => ''
        ];

    public function debitCardList()
    {
        $cards = DebitCards::orderBy('status', 'desc')->get();

        return view('admin.cards.debit.list',compact('cards'));
    }
    
    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $action = self::edit(new DebitCards(),$request);
        } else {
            $model = $this->attributes;
            $edit = 'add';
            $route = 'debit-cards-add';

            $banks = Bank::all();
            $card_types = CardType::all();
            $currencies = Currency::all();
            $payments = PaymentType::all();
            $dates = DateService::where(['service_id' =>3, 'child_service_id' => 1,'status' => 1])->get(); /** service_id = 3 kartalar, child_service_id = 1 debit kartalar **/

            $action = view('admin.cards.debit.edit',compact(
                'edit',
                'model',
                'banks',
                'card_types',
                'currencies',
                'payments',
                'dates',
                'route'
            ));
        }
        return $action;
    }
    public function update(Request $request)
    {
        if($request->isMethod('post'))
        {
            $action = self::edit(DebitCards::find($request->id),$request);
        } else {
            $model = DebitCards::find($request->id);
            $edit = 'update';
            $route = 'debit-cards-update-add';

            $banks = Bank::all();
            $card_types = CardType::all();
            $currencies = Currency::all();
            $payments = PaymentType::all();
            $dates = DateService::where(['service_id' =>3, 'child_service_id' => 1, 'status' => 1])->get();  /** service_id = 3 kartalar, child_service_id = 1 debit kartalar **/

            $action = view('admin.cards.debit.edit',compact(
                'edit',
                'model',
                'banks',
                'card_types',
                'currencies',
                'payments',
                'dates',
                'route'
                ));
        }
        return $action;
    }
    protected function edit($model,$request){
        $model->bank_id = $request->bank_id;
        $model->name_uz = $request->name_uz;
        $model->name_ru = $request->name_ru;
        $model->type_card_id = $request->type_card_id;

        if($request->currency_id){
            $model->currency_id = implode(',',$request->currency_id);
        }
        if($request->payment_id){
            $model->payment_id = implode(',',$request->payment_id);
        }
        $model->date_id = $request->date_id;
        $model->price = $request->price;
        $model->kamay_qoldiq = $request->kamay_qoldiq;
        $model->qushimcha_qulaylik_uz = $request->qushimcha_qulaylik_uz;
        $model->qushimcha_qulaylik_ru = $request->qushimcha_qulaylik_ru;
        $model->documents_uz = $request->documents_uz;
        $model->documents_ru = $request->documents_ru;
        $model->rasmiy_yuli_uz = $request->rasmiy_yuli_uz;
        $model->rasmiy_yuli_ru = $request->rasmiy_yuli_ru;
        $model->qushimcha_shart_uz = $request->qushimcha_shart_uz;
        $model->qushimcha_shart_ru = $request->qushimcha_shart_ru;
        
        $link=[
            'uz' => $request->input('link_uz'),
            'ru' => $request->input('link_ru'),
        ];
        $model->link = json_encode($link);

        $model->bepul_xizmat = $request->bepul_xizmat > 0 ? 1: 0;

        $model->bepul_naqdlash = $request->bepul_naqdlash > 0 ? 1: 0;

        $model->bepul_tulov = $request->bepul_tulov > 0 ? 1: 0;

        $model->kontaktsiz_tulov = $request->kontaktsiz_tulov > 0 ? 1: 0;

        $model->cash_back = $request->cash_back > 0 ? 1: 0;

        $model->uyga_yetkazish = $request->uyga_yetkazish > 0 ? 1: 0;

        $model->tuluv_komissia = $request->tuluv_komissia;

        $model->naqdlash_komissia = $request->naqdlash_komissia;
        $model->naqdlash_komissia_boshqa = $request->naqdlash_komissia_boshqa ;
        $model->kontaksiz_tolov_izoh_uz = $request->kontaksiz_tolov_izoh_uz;
        $model->kontaksiz_tolov_izoh_ru = $request->kontaksiz_tolov_izoh_ru;
        $model->cash_back_izoh_uz = $request->cash_back_izoh_uz;
        $model->cash_back_izoh_ru = $request->cash_back_izoh_ru;
        $model->uyga_yetkazish_izoh_uz = $request->uyga_yetkazish_izoh_uz;
        $model->uyga_yetkazish_izoh_ru = $request->uyga_yetkazish_izoh_ru;
        $model->tuluv_komissia_izoh_uz = $request->tuluv_komissia_izoh_uz;
        $model->tuluv_komissia_izoh_ru = $request->tuluv_komissia_izoh_ru;
        $model->naqdlash_komissia_izoh_uz = $request->naqdlash_komissia_izoh_uz;
        $model->naqdlash_komissia_izoh_ru = $request->naqdlash_komissia_izoh_ru;
        $model->naqdlash_komissia_boshqa_izoh_uz = $request->naqdlash_komissia_boshqa_izoh_uz;
        $model->naqdlash_komissia_boshqa_izoh_ru = $request->naqdlash_komissia_boshqa_izoh_ru;
        $model->price_izoh_uz = $request->price_izoh_uz;
        $model->price_izoh_ru = $request->price_izoh_ru;

        $model->tuluv_komissia_boshqa = $request->tuluv_komissia_boshqa;
        $model->tuluv_komissia_boshqa_izoh_uz = $request->tuluv_komissia_boshqa_izoh_uz;
        $model->tuluv_komissia_boshqa_izoh_ru = $request->tuluv_komissia_boshqa_izoh_ru;
        $model->naqdlash_komissia_terminal = $request->naqdlash_komissia_terminal;
        $model->naqdlash_komissia_terminal_izoh_uz = $request->naqdlash_komissia_terminal_izoh_uz;
        $model->naqdlash_komissia_terminal_izoh_ru = $request->naqdlash_komissia_terminal_izoh_ru;
        $model->urgent_opening = $request->urgent_opening;
        $model->re_issue_card = $request->re_issue_card;
        $model->sms_inform = $request->sms_inform;
        $model->service_uz = $request->service_uz;
        $model->service_ru = $request->service_ru;

        $model->aktsiya = $request->aktsiya > 0 ? 1 : 0;
        $izoh = [
            'uz' => $request->input('aktsiya_izoh_uz'),
            'ru' => $request->input('aktsiya_izoh_ru'),
        ];
        $model->aktsiya_izoh = json_encode($izoh);
        $link = [
            'uz' => $request->input('aktsiya_link_uz'),
            'ru' => $request->input('aktsiya_link_ru'),
        ];
        $model->aktsiya_link = json_encode($link);

        $model->qushimcha_karta_ochish = $request->qushimcha_karta_ochish;
        $model->d_secure = $request->d_secure > 0 ? 1 : 0;
        $model->lounge_key = $request->lounge_key > 0 ? 1 : 0;

        if($request->file('image')){
            if (is_file($model->image)) {
                unlink(public_path('/').$model->image);
            }
            $fileName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $model->image = CARD_PATH.$fileName;
            $request->file('image')->move(CARD_PATH,$fileName);
        }

        $model->save();

        return redirect()->route('debit-cards-list');
    }
    public function debitCardDelete($id)
    {
        $d = DebitCards::find($id);

        $d->delete();

        return redirect()->back();
    }

    public function listType(){
        $model = CardType::all();
        return view('admin.cards.debit.parameters.type.list',compact('model'));
    }

    public function editCardType(Request $request){
        if(!empty($request->id)){
            $model = CardType::find($request->id);
        }if(empty($request->id)){
            $model = new CardType();
        }
        $model->name_uz = $request->name_uz;
        $model->name_ru = $request->name_ru;
        $model->save();

        return redirect()->back();
    }
    public function deleteCardType($id){
        if($id){
            $model = CardType::find($id);
            $model->delete();
        }
        return redirect()->back();
    }

    public function cardStatus(Request $request){
        $model = DebitCards::find($request->id);
        $model->status = $request->status;
        $model->save();
        
        return response()->json(['status'=>$request->status]);
    }
}