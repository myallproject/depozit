<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;

class OrderController extends Controller
{
    public function index(){
        $orders = Order::all();
        return view('admin.orders.list',compact('orders'));
    }

    public function edit($id){
        $order = Order::find($id);
        return view('admin.orders.edit',compact('order'));
    }

    public function update(Request $request){

        DB::beginTransaction();
        try {
            $order = Order::find($request->order_id);
            $order->region_id = $request->region_id;
            $order->product_id = $request->product_id;
            $order->bank_id = $request->bank_id;
            $order->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return 500;
        }
        
        return 200;
    }
}
