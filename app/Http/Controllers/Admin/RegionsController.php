<?php

namespace App\Http\Controllers\Admin;

use App;
use App\Models\Region;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use YandexTranslate;

class RegionsController extends Controller
{
    public function regionList() {
        
        $regions = Region::all();
        
        return view('admin.regions.list',compact('regions'));
    }
   
    public function updateView($id) {
        
        $region = $this->findModel($id);
        
        $parent = Region::where('parent_id',null)->get();
        return view('admin.regions.edit',compact('region','parent'));
    }
    
    public function update(Request $request) {
        
        $r = $this->findModel($request->id);        
        $r->name_uz = $request->name_uz;
        $r->name_ru = $request->name_ru;

        //if($request->input('parent')){
        $r->parent_id = $request->parent;
        //}
        
        $r->save();
        
        return redirect()->route('region-list');
    }
    
    public function addView() {
        
        $region = null;
        $parent = Region::where('parent_id',null)->get();
        return view('admin.regions.edit',compact('region','parent'));
    }
    
    public function add(Request $request) {
        
        $r = new Region();
    
        $r->name_uz = $request->name_uz;
        $r->name_ru = $request->name_ru;
        
        if($request->input('parent')){
            $r->parent_id = $request->parent;
        }
        
        $r->save();
        
        return redirect()->route('region-list');
    
    }
    
    public function findModel($id) {
        
        if(($model = Region::find($id)) !==null) {
            return $model;
        }
    }
    
    public function delete($id) {
        $r = $this->findModel($id);
        
        $r->delete();
    
        return redirect()->route('region-list');
    }

    public function translateName($name)
    {
        return YandexTranslate::translate($name,'uz','ru');
    }
}
