<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\CarBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = CarBrand::all();
        $cars = Car::all();
        return view('admin.cars.list', compact('cars','brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'brand_id' => ['bail','required'],
            'price' => ['required','numeric'],
            'name_uz' =>['required'],
            'car_image' => ['image']
        ]);

        if($request->id !== null) {
            $car = Car::findOrFail($request->id);
            $message = 'Mashina muvaffaqiyatli yangilandi';
        } else {
            $car = new Car();
            $message = 'Mashina muvaffaqiyatliqo\'shildi';
        }
        $car->car_brand_id = $request->brand_id;
        $car->name_uz = $request->name_uz;
        if($request->name_ru !== null) $car->name_ru = $request->name_ru;
        $car->pozition = $request->pozition;
        $car->price = $request->price;
        $car->price_category = $request->currency;
        if($request->hasFile('car_image')) {
            //delete previous file
            if($car->image != null) {
                $previousImage = public_path("uploads/cars/{$car->image}");
                if(File::exists($previousImage)) {
                    unlink($previousImage);                    
                }
            }            
            //
            $path = $request->car_image->store("/uploads/cars");
            $length = count(explode('/',$path));
            $filename = explode('/', $path)[$length - 1];
            $car->image = $filename;
        }
        $car->save();
        return redirect()->back()->with('success', $message );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Car::findOrFail($id);
        $car->delete();
        return redirect()->back()->with('success','Moshina o\'chirildi');
    }

    public function carAPIRunParse() {

    }

    public function carTypeList($id) {

    }
}
