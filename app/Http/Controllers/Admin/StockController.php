<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StockBranches;
use App\Models\Stocks;
use App\Http\Requests\Admin\StockRequest;

class StockController extends Controller
{
    public function index()
    {
        $stocks = Stocks::orderBy('name')->with('branch')->get();
        return view('admin.stocks.list', compact('stocks'));
    }

    public function form($id = null)
    {
        $stock = Stocks::find($id);
        $branches = StockBranches::orderBy('title_uz')
            ->pluck('title_uz', 'id');
        if (!$stock) {
            $stock = [];
        }
        return view('admin.stocks.form', compact('stock', 'branches'));
    }

    public function delete($id = null)
    {
        if ($id) {
            $stock = Stocks::find($id);
            $stock->delete();
        }

        return redirect()->back()->with('success', 'Ochirildi');
    }

    public function save(StockRequest $request, $id = null)
    {
        $data = $request->except('_token');

        // return $data;

        if ($id) {
            $stock = Stocks::findOrFail($id);
            $stock->update($data);
        } else {
            $stock = new Stocks;
            $stock->name = $data['name'];
            $stock->ordinary_stock_price = $data['ordinary_stock_price'];
            $stock->preferred_stock_price = $data['preferred_stock_price'];
            $stock->branch_id = $data['branch_id'];
            $stock->dividend = $data['dividend'];
            $stock->foreign = $data['foreign'];
            $stock->save();
        }

        return redirect()->back()->with('success', 'Saqlandi');
    }

    public function requests()
    {
    }
}
