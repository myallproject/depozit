<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaymentType;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ServiceController extends Controller
{
    public function servicesList() {
        
        $services = Service::paginate(20);
        return view('admin.services.list',compact('services'));
    }
    
    public function addView() {
        
        $service = null;
        $parent = Service::whereNull('parent_id')->get();
        return view('admin.services.edit',compact('service','parent'));
    }
    
    public function add(Request $request) {
        
        $service = new Service();
       
        $service->name_uz = $request->name_uz;

        $service->name_ru = $request->name_ru;
      
        if($request->input('parent')){
            $service->parent_id = $request->input('parent');
        }
        $service->status = 1;
        
        $service->save();
        
        return redirect()->route('services-list');
    }
    
    public function updateView($id) {
        
         $service = Service::find($id);
         $parent = Service::whereNull('parent_id')->get();
         
         return view('admin.services.edit',compact('service','parent'));
    }
    
    public function update(Request $request) {
        
        $service = Service::find($request->id);
    
        $service->name_uz = $request->name_uz;

        $service->name_ru = $request->name_ru;

        if($request->input('parent')){
            $service->parent_id = $request->input('parent');
        }
        $service->status = 1;
    
        $service->save();
        
        return redirect()->route('services-list');
    }

    public function changeStatus(Request $request)
    {
         $model = Service::find($request->id);

        if($model){
            $model->status = $request->status;
            $model->save();
        }

        return response()->json(['error'=>false,'msg'=>'Success!', 'status' => $request->status,'id'=>$request->id]);
    }
    
    public function paymentTypeList(){
        $model = PaymentType::all();

        return view('admin.payment-type.list',compact('model'));
    }

    public function paymentTypeEdit(Request $request){
        if(!empty($request->id)){
            $model = PaymentType::find($request->id);
        }if(empty($request->id)){
            $model = new PaymentType();
        }
        $model->name = $request->name;
        $model->save();
        return redirect()->back();
    }
    
    public function paymentTypeDelete($id){
        $model = PaymentType::find($id);
        $model->deleted = 1;
        $model->save();
        return redirect()->back();
    }
    
    public function delete($id){
        $model = Service::find($id);
        $model->deleted = 1;
        $model->save();
        return redirect()->back();
    }


    
}
