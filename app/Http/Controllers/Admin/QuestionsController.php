<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Questions;
use App\Models\Service;
use App\Models\Answers;

class QuestionsController extends Controller
{
    public function listQuestions()
    {
    	$questions = Questions::all();
    	return view('admin.questions.list',compact('questions'));
    }

    public function editQuestionView($id)
    {
    	$services = Service::all();
    	$question = Questions::findOrFail($id);
    	return view('admin.questions.edit',compact('question','services'));
    }

    public function edit(Request $request)
    {
		if($request->input('id')){
			$question = Questions::find($request->input('id'));
			$question->service_id = $request->input('service');
			$question->question = $request->input('question');
			$question->info_bank = $request->input('info_bank');
			$question->save();
		}

		return redirect()->route('admin-question-list');
    } 

    public function editAnswer(Request $request)
    {
		if($request->input('id')){
			$answer = Answers::find($request->input('id'));
			$answer->answer_text = $request->input('answer_text');
			
			$answer->save();
		}

		return redirect()->back();
    }

    public function deleteQuestion($id)
    {
    	$question = Questions::findOrFail($id);
    	$question->delete();
    	return redirect()->back();
    }

    public function deleteAnswer($id)
    {
    	$answer = Answers::findOrFail($id);
    	$answer->delete();
    	return redirect()->back();
    }
}
