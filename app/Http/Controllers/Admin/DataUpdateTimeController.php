<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DataUpdateTime;
use App\Models\Service;
use Illuminate\Http\Request;

class DataUpdateTimeController extends Controller
{
    public function listDate()
    {
        $model = DataUpdateTime::all();
        $services = Service::all();
        return view('admin.update-time.list',compact('model','services'));
    }

    public function edit(Request $request){
        if($request->id){
           return self::save(DataUpdateTime::find($request->id),$request);
        } else {
           return self::save(new DataUpdateTime(),$request);
        }
    }

    protected function save($model,$request){

        $model->service_id = $request->service_id;
        if($model->data_update_time){
            $model->last_data_update_time = $model->data_update_time;
        }
        $model->data_update_time = $request->update_time;
        $model->save();

        return redirect()->back();
    }

    public function delete($id)
    {
        if($id){
            $model = DataUpdateTime::find($id);
            $model->delete();
        }

        return redirect()->back();
    }
}
