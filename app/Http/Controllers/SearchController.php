<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Credit;
use App\Models\CreditCards;
use App\Models\DebitCards;
use App\Models\Deposit;
use App\Models\PagesMetaTags;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class SearchController extends Controller
{
    public function search(Request $request){

        $variable = $request->input('variable');
        $resultSearch = [];

        if($variable){
            $resultSearch = (new Search())
                ->registerModel(Deposit::class,'name_uz','name_ru','deposit_date_string','min_sum','deposit_percent')
                ->registerModel(Credit::class,'name_uz','name_ru','date_privilege_uz','date_privilege_ru','amount','percent')
                ->registerModel(CreditCards::class,'name_uz','name_ru','percent','max_sum')
                ->registerModel(DebitCards::class,'name_uz','name_ru','price')
                ->registerModel(Bank::class,'name_uz','name_ru','description_uz','description_ru')
                ->perform($variable);

            //dd($resultSearch);
        }
        $meta_tags = PagesMetaTags::where('page',config('global.pages.search'))->where('parent_id',0)->get();
        return view('frontend.pages.search',compact('resultSearch','meta_tags'));
    }

    public function searchPage(){
         $meta_tags = PagesMetaTags::where('page',config('global.pages.search'))->where('parent_id',0)->get();
        return view('frontend.pages.search',compact('meta_tags'));
    }
}
