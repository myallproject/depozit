<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Models\Bank;
use App\Models\Currency;
use App\Models\ExchangeRate;

class CurrencyApiController extends Controller
{
    public function getCurrency(Request $request)
    {
    	$banks = Bank::all();
        $currencies = Currency::all();

        $action = 'take';

        $orderBy = 'DESC';
        if($request->input('action') == 'take'){
            $action = $request->input('action');
            $orderBy = 'DESC';
        }
        
        if($request->input('action') == 'sale')
        {
            $action = $request->input('action');
            $orderBy = 'ASC';
        }

        $exchange = ExchangeRate::where('deleted',0);
      
	 	if($request->input('code')){
	    	$exchange = $exchange->where('code','LIKE','%'.$request->input('code').'%');
	    }

        $exchange = $exchange->orderBy($action,$orderBy);

       	$exchange = $exchange->get();
       	$data=[];
       	if(count($exchange) > 0){
   			foreach($exchange as $ex){
				$data[] = [
					'bank' => $ex->bank->name(),
					'code' => $ex->code,
					'take'=> $ex->take,
					'sale'=> $ex->sale,
					'updated_at'=> date('Y-m-d H:i:s',strtotime($ex->updated_at)),
					'deleted'=> $ex->deleted
				];
	       	}
	       	$status = 200;
       	} else {
       		$status = 404;
       	}
       

        return Response::json($data, $status);
    }
}
