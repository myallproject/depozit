<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function deposits()
    {
    	
    	return 'Deposit Test Function';
    }

   public function putget()
    {
    	return view('test.page');
    }

    public function put(Request $request)
    {
    	dd($request->id);;
    }
}
