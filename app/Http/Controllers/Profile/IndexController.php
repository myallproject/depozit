<?php

namespace App\Http\Controllers\Profile;

use App\Rules\MatchOldPassword;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

use App\Models\Review;

class IndexController extends Controller
{
    public function index()
    {
        return view('profile.index');
    }

    public function reviews()
    {
        $user = Auth::user();
        $reviews = [];
        if (!empty($user->reviews)) {
            $reviews = Review::where('user_id',$user->id())->paginate(10);
 
        }
        return view('profile.reviews', compact('reviews'));
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ], [
            'current_password.required' => 'Поле текущий пароль обязательно для заполнению',
            'new_password.required' => 'Поле пароль обязательно для заполнению',
            'new_confirm_password.same' => 'Неправильно указан подтверждающий пароль'
        ]);

        User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);

        return redirect()->back()->with('success', 'Ваш пароль успешно изменён');
    }


    public function editUser(Request $request)
    {   
        $ruleEmail = null;
        $rulePhone = null;
        if($request->input('email')){
            $ruleEmail = $request->has('email') ? Rule::unique('users')->ignore($request->id) : '';

        }
        if($request->input('phone')){
            $rulePhone = $request->has('phone') ? Rule::unique('users')->ignore($request->id) : '';

        }

        $request->validate([
            'email' => [$ruleEmail],
            'phone' => [$rulePhone]
        ]);

        $user = User::find($request->id);

        if($request->input('email')){
            $user->email = $request->input('email');
        }
        if($request->input('phone')){
            $user->phone = $request->input('phone');
        }

        $user->name = $request->name;
        $user->gender = $request->gender;

        $user->save();

        return redirect()->back()->with('success', __('lang.success'));;
    }
}
