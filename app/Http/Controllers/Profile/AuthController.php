<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Services\PlayMobileService;
use App\Models\PagesMetaTags;
use App\Models\PhoneVerification;
use App\User;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Socialite;
use Hash;
use Illuminate\Support\Str;
use Session;
use Socialite;

class AuthController extends Controller
{
    protected $playMobileService;

    public function __construct(PlayMobileService $playMobileService)
    {
        $this->playMobileService = $playMobileService;
    }

    public function testSMS()
    {
        return $this->playMobileService->sendCode('998911146761', 'Test message!');
    }
    public function login()
    {
        $add_review_url = App::getLocale() . '/reviews/form/add';

        if ($add_review_url != session('intended_url'))
            Session::put('intended_url', url()->previous());

        $meta_tags = PagesMetaTags::where('page', config('global.pages.login'))->where('parent_id', 0)->get();
        return view('profile.auth.login', compact('meta_tags'));
    }

    public function postLogin(LoginRequest $request)
    {
        if (preg_match('/.+\@.+/', $request->login)) {
            $request->validate(['login' => 'required|email']);
        } elseif (preg_match('/(9989).+/', $request->login)) {
            $request->validate(['login' => 'required|digits:12|numeric']);
        }
        $data = $request->except(['_token']);

        if (
            Auth::attempt(['phone' => $data['login'], 'password' => $data['password']]) ||
            Auth::attempt(['email' => $data['login'], 'password' => $data['password']])
        ) {
            return redirect(session('intended_url', '/'))->with('success', 'Вы успешно вошли в систему!');
        } else {
            return redirect()->back()->with('error', 'Неправильный логин или пароль!');
        }
    }

    public function register()
    {
        $meta_tags = PagesMetaTags::where('page', config('global.pages.register'))->where('parent_id', 0)->get();
        return view('profile.auth.register', compact('meta_tags'));
    }

    public function postRegister(RegisterRequest $request)
    {
        $data = $request->all();

        $user = User::create([
            'name' => $data['username'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        Auth::login($user);

        return redirect()->route('profile.confirm', App::getLocale());
    }

    public function confirm()
    {
        $user = Auth::user();
        $phone = $user->getPhone();
        if ($phone) {
            if (!$user->isVerified()) {
                $code = $this->playMobileService->generateCode();

                DB::beginTransaction();
                try {
                    $sendCodeStatus = $this->playMobileService->sendCode($phone, $code);
                    if ($sendCodeStatus == 200) {
                        $verification = new PhoneVerification();
                        $verification->user_id = $user->getAuthIdentifier();
                        $verification->sent_at = Carbon::now();
                        $verification->code = $code;
                        $verification->save();
                    } else {
                        throw new \DomainException("Ошибка при отправке код подтверждение!");
                    }
                    DB::commit();
                } catch (\DomainException $e) {
                    DB::rollBack();
                    return redirect()->back()->with('error', $e->getMessage());
                }
                return view('profile.auth.confirm');
            }
            return redirect(session('intended_url', '/'))->with('success', 'Номер мобильного телефона подтвержден!');
        }
        return redirect()->route('phone.verify', App::getLocale())->with('error', 'Мобильный номер не найден. Пожалуйста, введите повторно');
    }

    public function postConfirm(Request $request)
    {
        $code = $request->input('code');
        $user = Auth::user();
        $verification = $user->verification;
        if ($code && $verification) {
            if ($verification->code == $code) {
                $user->verifications()->where('id', '!=', $verification->id)->delete();
                $verification->verified_at = Carbon::now();
                $verification->save();

                $user->phone_verified_at = $verification->verified_at;
                $user->verified = true;
                $user->save();
                return redirect(session('intended_url', '/'))->with('success', 'Ваш телефон успешно подтверждена!');
            } else {
                return redirect()->route('profile.confirm', App::getLocale())->with('error', 'Вы ввели неправильный код, повторите попытку!');
            }
        } else {
            return redirect(session('intended_url', '/'))->with('error', 'Неправильный код подтверждения!');
        }
    }

    public function resetPassword()
    {
        return view('profile.auth.password-reset');
    }

    public function postResetPassword(Request $request)
    {
        $request->validate([
            'phone_number' => 'required|digits:12|numeric',
            'new_password' => 'required'
        ]);

        $data = $request->all();
        $user = User::where('phone', '=', $data['phone_number'])->first();

        if ($user) {
            $user->password = bcrypt($data['new_password']);
            $user->phone_verified_at = null;
            $user->verified = false;
            $user->save();

            $verification = $user->verification;
            $verification->verified_at = null;
            $verification->save();

            Auth::login($user);

            return redirect()->route('profile.confirm', App::getLocale());
        }

        return redirect()->back()->with('error', 'Номер телефона недоступен в системе!');
    }

    public function verifyPhone()
    {
        return view('profile.auth.ask-phone');
    }

    public function postVerifyPhone(Request $request)
    {
        $request->validate(['phone_number' => 'required|digits:12|numeric']);
        if (Auth::check()) {
            $user = Auth::user();
            if (strcasecmp($request->phone_number, $user->getPhone()) != 0) {
                $user->phone = $request->phone_number;
                $user->save();
            }
            return redirect()->route('profile.confirm', App::getLocale());
        }
        return redirect()->route('profile.login', App::getLocale());
    }

    public function google()
    {
        $redirect = '';
        try {
            $redirect = Socialite::driver('google')->redirect();
        } catch (\Exception $e) {
            return redirect()->route('profile.login', App::getLocale())->with('error', 'Не удалось подключиться к Google!');
        }
        return $redirect;
    }

    public function googleRedirect()
    {
        try {
            $google_user = Socialite::driver('google')->stateless()->user();
            $user = User::firstOrCreate([
                'email' => $google_user->email
            ], [
                'name' => $google_user->name,
                'password' => Hash::make(Str::random(24)),
                'verified' => true
            ]);

            Auth::login($user, true);
        } catch (\Exception $e) {
            return redirect()->route('profile.login', App::getLocale())->with('error', 'Возникла проблема с вашей учетной записью Google!');
        }

        return redirect(session('intended_url', '/'))->with('success', 'Вы успешно вошли в систему!');
    }

    public function facebook()
    {
        $redirect = '';
        try {
            $redirect = Socialite::driver('facebook')->redirect();
        } catch (\Exception $e) {
            return redirect()->route('profile.login', App::getLocale())->with('error', 'Не удалось подключиться к Facebook!');
        }
        return $redirect;
    }

    public function facebookRedirect()
    {
        try {
            $facebook_user = Socialite::driver('facebook')->stateless()->user();
            if ($facebook_user->email) {
                $user = User::firstOrCreate([
                    'email' => $facebook_user->email
                ], [
                    'name' => $facebook_user->name,
                    'password' => Hash::make(Str::random(24)),
                    'verified' => true
                ]);
            } else {
                $user = User::firstOrCreate([
                    'facebook_id' => $facebook_user->id
                ], [
                    'name' => $facebook_user->name,
                    'password' => Hash::make(Str::random(24)),
                    'verified' => true
                ]);
            }
            Auth::login($user, true);
        } catch (\Exception $e) {
            return redirect()->route('profile.login', App::getLocale())->with('error', 'Возникла проблема с вашей учетной записью Facebook!');
        }

        return redirect(session('intended_url', '/'))->with('success', 'Вы успешно вошли в систему!');
    }
}
