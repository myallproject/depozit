<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Models\Deposit;
use App\Models\Order;
use App\Models\UserOrganization;
use DB;
use Auth;

class OrderController extends Controller
{
    public function orderDeposit(OrderRequest $request){
        $deposit = Deposit::find($request->product_id);
        $data = [
            'user_name' => $request->user_name,
            'phone_number' => $request->phone,
            'email' => $request->email,
            'region_id' => $request->city,
            'product_id' => $request->product_id,
            'product_type_id' => 1,
            'bank_id' => $deposit->bank->id,
            'agree' => 1,
        ];

        if($this->save($data) == 500){
            return redirect()->back()->with('errors', __('lang.v2.order.sent'));
        }
        return redirect()->back()->with('success', __('lang.v2.order.sent'));
    }

    protected function save($model){

        DB::beginTransaction();
        try {
            Order::create($model);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return 500;
        }
        
        return 200;
    }

    public function bankDeposits(){
        $bank = $this->bank();
        $orders_done = Order::where([
            ['bank_id', $bank->id],
            ['product_type_id', 1],
            ['done', 1]
        ])->get();

        $orders_unread = Order::where([
            ['bank_id', $bank->id],
            ['product_type_id', 1],
            ['done', 0]
        ])->get();

        return view('bank-profile.order.deposits', compact(
            'orders_unread',
            'orders_done'
        ));
    }

    public function bankCredits(){
        return view('bank-profile.order.credits');
    }

    public function bankDebetCards(){
        return view('bank-profile.order.debet-cards');
    }

    protected function bank(){
        return (UserOrganization::where('user_id', Auth::id())->first())->organisation;
    }

    public function markDone(Request $request){
        dd($request->arr);
        $orders = Order::whereIn('id', $request->ids)->get();

        foreach($orders as $order){
            $order->done = 1;
            $order->save();
        }

        $bank = $this->bank();
        $orders_done = Order::where([
            ['bank_id', $bank->id],
            ['product_type_id', 1],
            ['done', 1]
        ])->get();

        $orders_unread = Order::where([
            ['bank_id', $bank->id],
            ['product_type_id', 1],
            ['done', 0]
        ])->get();

        return Response::json(view('bank-profile.order.deposits', compact(
            'orders_unread',
            'orders_done'
        ))->render());
    }
}
