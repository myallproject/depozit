<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Questions;
use App\Models\Answers;
use App\User;

class BankProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('bankprofile');
    }

    public function profile()
    {
    	$user = User::find(Auth::user()->id);
    	return view('bank-profile.profile',compact('user'));
    }

    public function newQuestions()
    {
    	$questions = [];
    	$user = User::find(Auth::user()->id);
    	$org_id = $user->organization->child_org_id;
    	if($org_id){
    		$questions = Questions::where(['bank_id' => $org_id, 'status' => 0])->paginate(10)->onEachSide(1);
    	}
    	return view('bank-profile.question-answer',compact('questions','user'));
    }

    public function answeredQuestions()
    {
    	$questions = [];
    	$user = User::find(Auth::user()->id);
    	$org_id = $user->organization->child_org_id;
    	if($org_id){
    		$questions = Questions::where(['bank_id' => $org_id, 'status' => 1])->paginate(10)->onEachSide(1);
    	}
    	return view('bank-profile.answered-question',compact('questions','user'));
    }

    public function writeAnswer(Request $request)
    {
        $request->validate([
			'answer_text' => 'required',
			'id' => 'required',
		]);
		$questions = Questions::find($request->id);
		$questions->status = 1;
		$questions->save();

		$user_id = Auth::user()->id;
		$model = new Answers();
		$model->user_id = $user_id;
		$model->question_id = $request->id;
		$model->answer_text = $request->answer_text;
		$model->save();

		return redirect()->back()->with('success', 'Siz savolga javob berdingiz!');
    } 

    public function editAnswer(Request $request)
    {
        $request->validate([
			'answer_text' => 'required',
			'id' => 'required',
		]);
		
		$model = Answers::find($request->id);
		$model->answer_text = $request->answer_text;
		$model->save();

		return redirect()->back()->with('success', 'Siz savolga bergan javobingizni o\'zgartirdingiz!');
    }



    public function editUser(Request $request)
    {   
        $ruleEmail = null;
        $rulePhone = null;
        if($request->input('email')){
            $ruleEmail = $request->has('email') ? Rule::unique('users')->ignore($request->id) : '';

        }
        if($request->input('phone')){
            $rulePhone = $request->has('phone') ? Rule::unique('users')->ignore($request->id) : '';

        }

        $request->validate([
            'email' => [$ruleEmail],
            'phone' => [$rulePhone]
        ]);

        $user = User::find($request->id);

        if($request->input('email')){
            $user->email = $request->input('email');
        }
        if($request->input('phone')){
            $user->phone = $request->input('phone');
        }

        $user->name = $request->name;
        $user->gender = $request->gender;

        $user->save();

        return redirect()->back()->with('success', __('lang.success'));;
    }





}
