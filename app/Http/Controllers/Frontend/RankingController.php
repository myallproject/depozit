<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Review;
use App\Models\Service;
use App\Models\PagesMetaTags;
use Illuminate\Support\Facades\App;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;

define('R_BANK', Config::get('global.organization.bank'));

class RankingController extends Controller
{
    public function citizen(){
        $lang = App::getLocale();
        $filter_date='';
        $service_id=0;
        $date=array(
            Carbon::today()->toDateString(),
            Carbon::today()->subDays(7)->toDateString(),
            Carbon::today()->subDays(30)->toDateString(),
            Carbon::today()->subDays(90)->toDateString(),
            Carbon::today()->subDays(180)->toDateString(),
            Carbon::today()->subDays(365)->toDateString()
        );
        $data = $this->rankedBanks($filter_date,$service_id);
        $top_services = $this->topServices();
        $services = Service::whereNull('parent_id')->where('status',1)->orderBy('name_'.$lang)->get();
        $meta_tags = PagesMetaTags::where('page',config('global.pages.bank_question'))->where('parent_id',0)->get();
        
        return view('frontend.ranking.citizen-ranking', compact(
            'date',
            'data',
            'services',
            'filter_date',
            'service_id',
            'top_services',
            'meta_tags'
        ));
    }

    public function citizenFilter(Request $request){
        $request->date ? $filter_date = $request->date : $filter_date='';
        $request->services ? $service_id = $request->services : $service_id=0;
        $data = $this->rankedBanks($filter_date,$service_id);

        return Response::json(view('frontend.ranking.citizen-ranking-ajax',compact(
            'filter_date',
            'service_id',
            'data'
            ))->render());
    }

    public function rankedBanks($date='', $service_id=0){
        $banks = Bank::where('organization_id',R_BANK)->get();
        $rankedBanks=[];
        foreach($banks as $bank){
            $temp=[
                'bank' => $bank,
                'score' => $bank->ranking($date, $service_id)
            ];
            array_push($rankedBanks,$temp);
        }
        usort($rankedBanks, function($a,$b){
            return ($a['score']>=$b['score']) ? -1 : 1;
        });

        return $rankedBanks;
    }

    public function topServices(){
        $services = Service::where('status','=',1)->get();
        $top_services = [];
        foreach($services as $service){
            $sorted = $this->rankedBanks('',$service->id);
            $temp = [
                'bank_name' => $sorted[0]['bank']->name(),
                'bank_slug' => $sorted[0]['bank']->slug,
                'service_name' => $service->getName(),
                'mark' => $sorted[0]['bank']->formatedAvgMark('', $service->id)
            ];
            array_push($top_services,$temp);
        }
        usort($top_services, function($a,$b){
            return ($a['mark']>=$b['mark']) ? -1 : 1;
        });
        return $top_services;
    }
}
