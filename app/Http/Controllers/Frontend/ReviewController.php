<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Bp_reviewStatus;
use App\Http\Requests\AddCommentRequest;
use App\Http\Requests\AddReviewRequest;
use App\User;
use App\Models\Bank;
use App\Models\BankService;
use App\Models\Comment;
use App\Models\Region;
use App\Models\Review;
use App\Models\BankOffices;
use App\Models\Service;
use App\Models\Ranking;
use App\Models\PagesMetaTags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserOrganization;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use App\Http\Services\PlayMobileService;
use App\Notifications\ReviewNotification;

class ReviewController extends Controller
{
    protected $playMobileService;

    public function __construct(PlayMobileService $playMobileService){
        $this->playMobileService = $playMobileService;
    }

    protected function notifyUser($comment, $review_id){
        $status=0;
        $msg='';
        $notf='';
        $user = Review::find($review_id)->user;
        if(Auth::id() != $user->id){
            if(Auth::user()->role_id == 11){
                $msg='Assalomu alaykum hurmatli mijoz. Sizning izohingiz bank tomonidan ko\'rib chiqildi. Bank hodimining javobi: ';
                $notf='Sizning izohingiz bank tomonidan ko\'rib chiqildi.<br>Bank hodimining javobi: ';
            }
            else{
                $msg='Assalomu alaykum hurmatli mijoz. Sizning izohingizga saytimiz foydalanuvchisi '.Auth::user()->name.' sharh qoldirdi: ';
                $notf='Sizning izohingizga saytimiz foydalanuvchisi '.Auth::user()->name.' sharh qoldirdi: ';
            }
    
            $msg.=$comment;
            $notf.=$comment;
    
            if($user->phone){
                $status = $this->playMobileService->sendMessage($user->getPhone(), $msg, 'https://depozit.uz/uz/reviews/'.$review_id);
            }
            if($status!=200 && $user->email){
                $user->notify(new ReviewNotification('https://depozit.uz/uz/reviews/'.$review_id, $notf));
            }
        }
    }

    protected function notifyBanker(Bank $bank, $review_id, $type){
        $status=0;
        $msg='';
        $bankers = $bank->bank_hodimi;
        if(count($bankers)){
            foreach($bankers as $banker){
                if(Auth::id() != $banker->user_id){
                    $banker = User::find($banker->user_id);
                    if($type==1){
                        $msg='Assalomu alaykum '.$banker->name.'. Depozit.uz sayti foydalanuvchisi '.Auth::user()->name.' '.$bank->name_uz.' haqida fikr bildirdi.';
                    }elseif($type==2){
                        $msg='Assalomu alaykum '.$banker->name.'. Depozit.uz sayti foydalanuvchisi '.Auth::user()->name.' '.$bank->name_uz.' kommentariya qoldirdi.';
                    }
            
                    if($banker->phone){
                        $status = $this->playMobileService->sendMessage($banker->getPhone(), $msg, 'https://depozit.uz/uz/reviews/'.$review_id);
                    }
                    if($status!=200 && $banker->email){
                        $banker->notify(new ReviewNotification('https://depozit.uz/uz/reviews/'.$review_id, $msg));
                    }
                }
            }
        }
    }

    public function list(Request $request, $slug='')
    {
        $lang = App::getLocale();
        $single_bank = [];
        if($request){
            $reviews = Review::orderBy('created_at', 'DESC')
                ->with('service','bank')
                ->withCount('comments');
            if($request->input('region')){
                $reviews = $reviews->where('region_id',$request->input('region'));
            }
            if($request->input('bank')){
                $reviews = $reviews->where('bank_id',$request->input('bank'));
            } 
            if($request->input('services')){
                $reviews = $reviews->where('service_id',$request->input('services'));
            }
            if($request->input('answer_status')){
                $reviews = $reviews->where('answer_status',$request->input('answer_status'));
            }

            if($request->input('assessment')){
                if(intval($request->input('assessment')) <= 5){
                    $reviews = $reviews->where('assessment',$request->input('assessment'));
                } else {
                    $ns = array_map('intval', str_split($request->input('assessment')));
                    $reviews = $reviews->whereIn('assessment',$ns);
                }
            }
            $reviews = $reviews->paginate(10)->onEachSide(2);
        }else{
            $reviews = Review::orderBy('created_at', 'DESC')
                ->with('service','bank')
                ->withCount('comments')
                ->paginate(10)->onEachSide(2); 
        }

        if($request->ajax()){
            return Response::json(view('frontend.reviews.reviews-content',compact('reviews'))->render());
        }

        $banks = Bank::where('organization_id',config('global.organization.bank'))->orderBy('name_'.$lang)->get();
        $regions = Region::where('id','!=',224)->orderBy('name_'.$lang)->get();
        $regions_list = self::regionList();
       // dd($regions_list);
        $services = Service::whereNull('parent_id')->where('status',1)->orderBy('position','ASC')->orderBy('name_'.$lang)->get();
        $ranking = new Ranking;
        $top_services = $ranking->topServices();
        $meta_tags = PagesMetaTags::where('page',config('global.pages.reviews'))->where('parent_id',0)->get();
        return view('frontend.reviews.index', compact(
            'reviews',
            'banks',
            'services',
            'regions', 
            'regions_list', 
            'top_services', 
            'single_bank',
            'meta_tags'));
    }

    public function singleBankReviews($local, $slug){
        $lang = $local;
        $bank = Bank::where('slug',$slug)->first();
        $single_bank = [$bank->id, $bank->name()];
        $reviews = Review::where('bank_id', $bank->id)
                        ->orderBy('created_at', 'DESC')
                        ->with('service','bank')
                        ->withCount('comments')
                        ->paginate(10)->onEachSide(2);
        
        $banks = Bank::where('organization_id',config('global.organization.bank'))->orderBy('name_'.$lang)->get();
        $regions = Region::where('id','!=',224)->orderBy('name_'.$lang)->get();
        $regions_list = self::regionList();
        $services = Service::whereNull('parent_id')->where('status',1)->orderBy('position','ASC')->orderBy('name_'.$lang)->get();
        $ranking = new Ranking;
        $top_services = $ranking->topServices();
        $meta_tags = PagesMetaTags::where('page',config('global.pages.reviews'))->where('parent_id',0)->get();
        return view('frontend.reviews.index', compact('reviews','banks','services','regions', 'regions_list', 'top_services', 'single_bank', 'meta_tags'));
    }

    protected function regionList(){
         $lang = App::getLocale();
         $lts = config('global.letters_'.App::getLocale());
         $regions = Region::where('id','!=',224)->orderBy('name_'.$lang)->get();
         $new = [];
         foreach($lts as $ltk => $ltv){
             foreach ($regions as $region) {
                 $fl = mb_substr($region->name(),0,1,'UTF-8');
                 if($ltv == $fl){
                    $new[$ltv][] = ['id'=> $region->id ,'name'=>$region->name()]; 
                 }
             }            
         }

         return $new;
    }
    public function view(Request $request)
    {        
        $review = Review::whereId($request->id)->withCount('comments')->first();
        if(is_null($review)) abort(404);
        if(Auth::check() && Auth::user()->role_id == 11) {
            $bpstatus = Bp_reviewStatus::where('review_id',$request->id)->first();
            $bpstatus->readstatus = 1;
            $bpstatus->save();
        }
        $review->views +=1;
        $review->save();        
        $response = null;
        $respondedUsers = UserOrganization::where('child_org_id', '=', $review->bank->id)->get();
        $respondedUsersId = [];
        foreach($respondedUsers as $respondedUser) {
            array_push($respondedUsersId, $respondedUser->user_id);
        }
        if(count($respondedUsersId) > 0) {
            $response = Comment::where('review_id','=', $review->id)->whereNull('parent_id')->whereIn('user_id', $respondedUsersId)->orderBy('created_at', 'ASC')->first();
        }
        if(is_null($response)) $comments = Comment::commentsTree($review);
        else $comments = Comment::commentsTree($review, $response);
        
        return view('frontend.reviews.view', compact('review', 'comments','response'));
    }

    public function addReview()
    {
        $meta_tags = PagesMetaTags::where('page',config('global.pages.add_review'))->where('parent_id',0)->get();
        
        $lang = App::getLocale();
        $cities = Region::whereNull('parent_id')->with('children')->orderBy('name_'.$lang)->get();
        $banks = Bank::where('organization_id',config('global.organization.bank'))->orderBy('name_'.$lang)->get();
        $services = Service::whereNull('parent_id')->where('status',1)->orderBy('position','ASC')->orderBy('name_'.$lang)->get();
        return view('frontend.reviews.add', compact('cities', 'banks','services','meta_tags'));
    }

    public function addReviewForm(AddReviewRequest $request)
    {
        $data = $request->except(['_token']);
        $data['user_id'] = Auth::user()->getAuthIdentifier();
        //$data['branch_id'] = null;
        //$data['service_id'] = null;
        $review = new Review();
        if($request->input('branch_id') && is_numeric($request->input('branch_id'))){
            $bank_b = BankOffices::find($request->input('branch_id'));
            if($bank_b->region_id != 224){
                $data['region_id'] = $bank_b->region_id; 
                $review->region_id = $bank_b->region_id;
            }
        } else {
           $data['branch_id']  = null; 
        }
        $review->branch_id = $data['branch_id'];
        $review->bank_id = $request->input('bank_id');
        $review->assessment = $request->input('assessment');
        $review->service_id = $request->input('service_id');
        $review->title = $request->input('title');
        $review->fulltext = $request->input('fulltext');
        $review->info_bank = $request->input('info_bank');
        $bfstatus = new Bp_reviewStatus();
		
        DB::beginTransaction();
        try {
            $review->user_id = Auth::user()->getAuthIdentifier();
            $review->save();
            DB::commit();
            $bfstatus->review_id = Review::orderBy('created_at','desc')->first()->id;
            $bfstatus->readstatus = 0;
            $bfstatus->save();
            
            if($request->input('bank_id')){
                $bank = Bank::find($request->input('bank_id'));
                $this->notifyBanker($bank, $bfstatus->review_id, 1);
            }
        
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage());
        }

        return redirect()->route('review.list',['locale' => App::getLocale()])->with(
            [
                'success'=> 'Sizning fikringiz qabul qilindi.',
                'newReview'=>[
                    'review_id' =>  $review->id,
                    'text'=>$review->fulltext
                ]
            ]);        
    }

    public function addCommentForm(AddCommentRequest $request)
    {
        $data = $request->except(['_token']);
        $data['user_id'] = Auth::user()->getAuthIdentifier();;
        $data['status'] = 1;

        $comment = new Comment();
        
        if(is_null($data['parent_id']) && Auth::user()->role_id == 11) {
            
            $revi = Review::whereId($data['review_id'])->first();
            $revi->answer_status = 2;
            $revi->save();
        }
        DB::beginTransaction();
        try {
            $comment->create($data);
            DB::commit();
            
            $this->notifyUser($data['text'], $data['review_id']);
            $review = Review::find($data['review_id']);
            if($review){
                $bank = $review->bank;
                if($bank)
                    $this->notifyBanker($bank, $review->id, 2);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Sizning izohingiz qoldirildi.');
    }

    
    public function selectBankBranches(Request $request)
    {
        return self::selectOption(BankOffices::class, $request, 'bank_id');
    }

    protected function selectOption($class, $request, $attribute)
    {
        $option = '';
        $options = '';
        if($request->id){
            $model = $class::where($attribute,$request->id)->where('deleted',0)->get()->sortBy('name');
            foreach($model as $row){
                $option .= '<option value="'.$row->id.'">'.$row->getName().'</option>';
            }
            $options = '<option value"">'.__('lang.v2.add_form.bank_branches').'</option>'.$option;
        }

        return Response($options);
    }

    //Bank-Profile
    public function reviewEditAnsStatus($ans_status) {
        
        // if($request->frombp) {
        //     Bp_reviewStatus::where('review_id',$request->id)->update(['readstatus' => 1]);
        // }
    }
    public function toggleReviewRead(Request $request) {
        $bpreview = Bp_reviewStatus::where('review_id',$request->id)->first();
        $status = $bpreview->readstatus;
        if($status == 0) $status = 1;
        else $status = 0;
        $bpreview->readstatus = $status;
        $bpreview->save();
        return;             
    }
    public function bplist(Request $request)
    {
        $lang = App::getLocale();
        $reviews = Review::where('bank_id','=', $request->id)->orderBy('created_at', 'asc')
        ->with('service','bank')
        ->withCount('comments');
        if($request->ajax()){           
            if($request->input('region')){
                $reviews = $reviews->where('region_id',$request->input('region'));
            }
            if($request->input('branch')){
                $reviews = $reviews->where('branch_id',$request->input('branch'));
            } 
            if($request->input('services')){
                $reviews = $reviews->where('service_id',$request->input('services'));
            }
            if($request->input('assessment')){
                if(intval($request->input('assessment')) <= 5){
                    $reviews = $reviews->where('assessment',$request->input('assessment'));
                } else {
                    $ns = array_map('intval', str_split($request->input('assessment')));
                    $reviews = $reviews->whereIn('assessment',$ns);
                }
            }
            $reviews = $reviews->paginate(10);
            if($request->input('answer_status')){
                $opt = $request->input('answer_status');
                if($opt == 1) {
                    foreach($reviews as $key=>$value) {
                        if($value->bp_reviewStatus->readstatus == 0) {
                            unset($reviews[$key]);
                        }
                    }
                } else if($opt == 2) {
                    foreach($reviews as $key=>$value) {
                        if($value->bp_reviewStatus->readstatus == 1) {
                            unset($reviews[$key]);
                        }
                    }
                } else if($opt == 3) {
                    foreach($reviews as $key=>$value) {
                        if($value->answer_status != 2) {
                            unset($reviews[$key]);
                        }
                    }
                } else if($opt == 4) {
                    foreach($reviews as $key=>$value) {
                        if($value->answer_status == 2) {
                            unset($reviews[$key]);
                        }
                    }
                } else if($opt == 5) {
                    foreach($reviews as $key=>$value) {
                        if($value->status != 2) {
                            unset($reviews[$key]);
                        }
                    }
                } else if($opt == 6) {
                    foreach($reviews as $key=>$value) {
                        if($value->status == 2) {
                            unset($reviews[$key]);
                        }
                    }
                } else if($opt == 7) {
                    foreach($reviews as $key=>$value) {
                        if($value->status != 4) {
                            unset($reviews[$key]);
                        }
                    }
                } else if($opt == 8) {
                    foreach($reviews as $key=>$value) {
                        if($value->status == 4) {
                            unset($reviews[$key]);
                        }
                    }
                }      
            }
        }
            
        if($request->ajax()){                
           return Response::json(view('frontend.reviews.bp_reviews-content',compact('reviews'))->render());
        }
        $reviews = $reviews->paginate(10);
        //0-umumiy count 1-o'qilgan 2-javob berilgan 3-tekshirilgan 4-muammo yechilgan
        $counter = array(0,0,0,0,0,0,0,0,0);
        if(count($reviews) > 0) {
            $counter[0] = count($reviews);
            $isRead = 0;
            foreach($reviews as $review) {
                if(!isset($review->bp_reviewStatus)) {
                    $bfstatus = new Bp_reviewStatus();
                    $bfstatus->review_id = $review->id;
                    $bfstatus->readstatus = 0;
                    $bfstatus->save();
                    $review->bp_reviewStatus = $bfstatus;
                }
                if($review->bp_reviewStatus->readstatus == 1) {
                    $isRead++;
                }
            }
            $counter[1] = $isRead;
            $counter[2] = $counter[0] - $counter[1];
            $counter[3] = count($reviews->where('answer_status', '=', 2));
            $counter[4] = $counter[0] - $counter[3];
            $counter[5] = count($reviews->where('status', '=', 2));
            $counter[6] = $counter[0] - $counter[5];
            $counter[7] = count($reviews->where('answer_status', '=', 4));
            $counter[8] = $counter[0] - $counter[7]; 
        }

        $bank = Bank::find($request->id);
        $ranked_banks = app('App\Http\Controllers\Frontend\RankingController')->rankedBanks();
        $score = 0;
        $rank = 0;
        $allCo = count(Bank::all());
        $com = 1;
        foreach($ranked_banks as $key => $value) {
            if($value['bank']->id == $bank->id) {
                $score = $value['score'];
                $rank = $com;
                break;
            }
            $com++;
        }
        $reyting = [$score, $rank, $allCo];
        $bank_offices = BankOffices::where('deleted','=',0)->where('bank_id','=', $request->id)->get();
        $regions = Region::orderBy('name_'.$lang)->get();
        $regions_list = self::regionList();
       // dd($regions_list);
        $top_services_for_single_bank = $bank->topServicesForSingleBank();
        $services = Service::whereNull('parent_id')->where('status',1)->orderBy('name_'.$lang)->get();
        return view('frontend.reviews.bp_index', compact('top_services_for_single_bank','counter','reviews','bank_offices','services','regions', 'regions_list','bank','reyting'));
    }

    public function bpResponseEditPage(Request $request) {
        $review = Review::findOrFail($request->id);
        $response = null;
        $respondedUsers = UserOrganization::where('child_org_id', '=', $review->bank->id)->get();
        $respondedUsersId = [];
        foreach($respondedUsers as $respondedUser) {
            array_push($respondedUsersId, $respondedUser->user_id);
        }
        if(count($respondedUsersId) > 0) {
            $response = Comment::where('review_id','=', $review->id)->whereNull('parent_id')->whereIn('user_id', $respondedUsersId)->orderBy('created_at', 'ASC')->first();
        }
        $bank_id = $review->bank->id;
        if(is_null($response)) abort(404); 
        return view('frontend.reviews.bp_edit', compact('response', 'bank_id'));
    }

    public function bpResponseEdit(Request $request) {
        $validateData = $request->validate([
            'text' => 'required',
            'response_id' => 'required',
            'bank_id' => 'required'
        ]);
        $response = Comment::findOrFail($request->response_id);
        $response->text = $request->text;
        $response->save();
        return redirect()->route('review.bp_list', ['locale' => App::getLocale(), 'id' => $request->bank_id])->with(['success' => 'Javob muvaffaqiyatli o\'zgartirildi']);        
    }


}