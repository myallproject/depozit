<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Currency;
use App\Models\DataUpdateTime;
use App\Models\DateService;
use App\Models\Deposit;
use App\Models\DepositPayPercentPeriod;
use App\Models\Region;
use App\Models\TypeBanks;
use App\Models\TypeOpenDeposit;
use App\Models\PagesMetaTags;
use App\Models\Ranking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Session;

define('BANK_TYPE',Config::get('global.bank_type'));

//1 deposit
class DepositsController extends Controller
{

    public function deposits() {
        //dd(Route::current()->parameters());
        $single_bank_id = 0;
        $meta_tags = PagesMetaTags::where('page',config('global.pages.deposits'))->where('parent_id',0)->get(); 
        $update_time = DataUpdateTime::where('service_id',1)->first();
        $currency = Currency::whereIn('name',['UZS','USD','EUR'])->get();
        $name = 'name_'.App::getLocale();
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $type_date = DateService::where(['service_id'=>1,'status' => 1])->where('position','>',0)->orderBy('position','asc')->get();
        $banks = Bank::orderBy('name_uz')->get();
        $open_type = TypeOpenDeposit::all();
        $paid_pariod  = DepositPayPercentPeriod::all();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();

        return view('frontend.deposits.deposits',compact(
            'currency',
            'type_date',
            'cities',
            'banks',
            'open_type',
            'paid_pariod',
            'update_time',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function depositsOfSingleBank($local, $slug){
        $single_bank_id = 0;
        if($slug){
            $bank = Bank::where('slug', $slug)->first();
            $single_bank_id = $bank->id;
        }
        $meta_tags = PagesMetaTags::where('page',config('global.pages.deposits'))->where('parent_id',0)->get(); 
        $update_time = DataUpdateTime::where('service_id',1)->first();
        $currency = Currency::whereIn('name',['UZS','USD','EUR'])->get();
        $name = 'name_'.App::getLocale();
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $type_date = DateService::where(['service_id'=>1,'status' => 1])->where('position','>',0)->orderBy('position','asc')->get();
        $banks = Bank::orderBy('name_uz')->get();
        $open_type = TypeOpenDeposit::all();
        $paid_pariod  = DepositPayPercentPeriod::all();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();
        
        return view('frontend.deposits.deposits',compact(
            'currency',
            'type_date',
            'cities',
            'banks',
            'open_type',
            'paid_pariod',
            'update_time',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function filter(Request $request) {
        $single_bank_id = $request->single_bank_id;

        $filter_type = $request->type_filter;
        $date_dp = '';
        $date_dp = $request->date_deposit_type > 0 ? $request->date_deposit_type: '';
        $request_price = 100000000000;
        $attr = [
            'status' => 0
        ];

        if($request->price){
            $request_price = $request->price;
        }
        if($request->input('currency') > 0){
            $attr = array_merge($attr,['currency'=>$request->input('currency')]);
        }
           
               
        if($single_bank_id>0){
            $data_d = Deposit::where('bank_id', $single_bank_id)->orderBy('deposit_percent', 'DESC')->orderBy('deposit_date','ASC');
        }
        else{
            $data_d = Deposit::orderBy('deposit_percent', 'DESC')->orderBy('deposit_date','ASC');
        }

        $data_d = $data_d->where('min_sum', '<=', $request_price);  
        $data_d = $data_d->where($attr);

        if($request->input ('date_deposit_type')){
            $data_d = $data_d->where('deposit_date_type','LIKE','%'.$request->input('date_deposit_type').'%');
        }
        if($request->input('online_deposit')){
            $data_d = $data_d->where('type_open','LIKE','%'.'1'.'%');
        }

        $data_d = $data_d->get();
        $r_price = $request->price;
        
        if(!$request->price and !$request->input ('date_deposit_type')){
            $r_price = 0;
        }

        if($single_bank_id>0){
            $data = $data_d;
        }else{
            $data = $this->groupBy($data_d,'bank_id',$request);
        }
        //dd($data);

        $type_bank = TypeBanks::all();
        return view('frontend.deposits.ajax-content',compact(
            'data',
            'r_price',
            'date_dp',
            'filter_type',
            'type_bank',
            'single_bank_id'
        ))->render();
    }

    public function groupBy($array,$key,$request){
        $request_price = $request->price;
        if(!$request->price) {
            $request_price = 10000000000;
        }
        $return = [];

        /*foreach($array as $val){
            $b_r = explode(',',$val->bank->region_id);
            if($request->input('region')){
                if(count($request->input('region')) > 1){
                        if(in_array($v,$b_r)){
                            $return[$val[$key]][] = $val;
                        }
                }else{
                    $return[$val[$key]][] = $val;
                }
            } else {
                $return[$val[$key]][] = $val;
            }
        }*///dd($return);

        foreach($array as $val){
            $b_r = explode(',',$val->bank->region_id);
            if($request->input('region')){
                if(in_array($request->input('region'),$b_r)){
                    $return[$val[$key]][] = $val;
                }
            }else{
                $return[$val[$key]][] = $val;
            }
        }

        $tp_banks = $request->type_banks;

        // if($request->type_banks[0] == null){
        //     $tp_banks =  [];
        // }
        $bank_type = [];
        if(!$request->input('type_banks_btn')){
            if($tp_banks){
                if(in_array('state_bank',$tp_banks)){
                    $bank_type = array_merge($bank_type,[1]);
                }
                if(in_array('private_bank',$tp_banks)){
                    $bank_type = array_merge($bank_type,[2]);
                }
                if (in_array('foreign_bank',$tp_banks)){
                    $bank_type = array_merge($bank_type,[3]);
                }
            }
        } else {
            if($request->input('type_banks_btn') == 'all_banks'){
                $bank_type = [1,2,3,4,5];
            } else {
                if($request->input('type_banks_btn') == 'state_bank'){ $bank_type = [1]; }
                if($request->input('type_banks_btn') == 'private_bank'){ $bank_type = [2]; }
                if($request->input('type_banks_btn') == 'foreign_bank'){ $bank_type = [3]; }
            }
        }
        

        $add_rules = $request->input('additional_rule');
        $account_fill = 0;
        $partly_take = 0;
        $privilege_take = 0;
        if($add_rules){
            if(in_array('account_fill',$add_rules)){$account_fill = strval(1);}
            if(in_array('partly_take',$add_rules)){ $partly_take = strval(1);}
            if(in_array('privilege_take',$add_rules)){$privilege_take = strval(1);}
        }


        $max_percent = [];
        $count = [];
        
        $attr = [
            'status' => 0
        ];
        if($request->input('currency') > 0){
            $attr = array_merge($attr,['currency'=>$request->input('currency')]);
        }

        if($request->input('capitalization')){
            $attr = array_merge($attr,['percents_capitalization'=>$request->input('capitalization')]);
        }
        //dd($state_bank);
        foreach ($return as $k => $v){
            $dp = Deposit::where('bank_id',$k);

                $dp = $dp->where($attr);

                if($request->input('input_type_dp')){
                      $dp = $dp->where('type_open','LIKE','%'.$request->input('input_type_dp').'%');
                }
                
                if($request->input ('date_deposit_type')){
                    $dp = $dp->where('deposit_date_type','LIKE','%'.$request->input('date_deposit_type').'%');
                }
                
                if($request->input('online_deposit')) {
                   $dp = $dp->where('type_open','LIKE','%'.'1'.'%');
                }
                if($request->percent_period){
                    $dp = $dp->whereIn('percent_paid_period', $request->percent_period);
                }
                $dp = $dp->where('min_sum', '<=', $request_price);
                $dp = $dp->where('account_fill', '>=', $account_fill);
                $dp = $dp->where('partly_take', '>=', $partly_take);
                $dp = $dp->where('privilege_take', '>=', $privilege_take);
                if($bank_type){
                    $dp = $dp->whereIn('bank_type_id', $bank_type);
                }
                
                $dp = $dp->orderBy('deposit_percent','DESC');
                $dp = $dp->first();
                if($dp){
                    $max_percent[] = $dp;
                }
            $count[$k] = count($v);
        }
        //dd($max_percent);
        $data = [
            'max_dp_id' => $max_percent,
            'data'      => $return,
            'count'     => $count
        ];

        return $data;
    }

    public function largeSearchDeposits(Request $request) {
        //dd($request->type_banks);
        $single_bank_id = 0;
        $attr = ['status' => 0];
        $request_price = $request->price;
        
        if(!$request->price){$request_price = 10000000000;}
       
       

        $add_rules = $request->additional_rule;
        
        if($request->additional_rule==null) {$add_rules = [];}

        $ids=range(1,100);

        $tp_banks = $request->type_banks;
        if($request->type_banks==null) {$tp_banks = [];}

        $bank_type = [];
        if(!$request->input('type_banks_btn')){
            if($tp_banks){
                if(in_array('state_bank',$tp_banks)) {
                    $tp_banks = array_diff($tp_banks,['state_bank']);
                    $bank_type = array_merge($bank_type,[1]);
                }
                if(in_array('private_bank',$tp_banks)) {
                    $bank_type = array_merge($bank_type,[2]);
                    $tp_banks = array_diff($tp_banks,['private_bank']);
                }
                if (in_array('foreign_bank',$tp_banks)) {
                    $bank_type = array_merge($bank_type,[3]);
                    $tp_banks = array_diff($tp_banks,['foreign_bank']);
                }
                if(count($tp_banks) > 0){
                    $ids = $tp_banks;
                }
            }
        } else {
            if($request->input('type_banks_btn') == 'state_bank'){ $bank_type = [1]; }
            if($request->input('type_banks_btn') == 'private_bank'){ $bank_type = [2]; }
            if($request->input('type_banks_btn') == 'foreign_bank'){ $bank_type = [3]; }
        }
        $account_fill = 0;
        $partly_take = 0;
        $privilege_take = 0;
        if($add_rules){
            if(in_array('account_fill',$add_rules)){$account_fill = strval(1);}
            if(in_array('partly_take',$add_rules)){ $partly_take = strval(1);}
            if(in_array('privilege_take',$add_rules)){ $privilege_take = strval(1);}
        }

        $filter_type = $request->type_filter;
        $date_dp = $request->date_deposit_type;

        if($request->input('capitalization')){
            $attr = array_merge($attr,['percents_capitalization'=>$request->input('capitalization')]);
        }

        if($request->input('currency') > 0){
            $attr = array_merge($attr,['currency'=>$request->input('currency')]);
        }

        if(!$request->price and !$request->input ('date_deposit_type')){
            $data_d = Deposit::where('status',0);
            $data_d = $data_d->where($attr);
            
            $data_d = $data_d->whereIn('bank_id',$ids);

            if($account_fill){$data_d = $data_d->where('account_fill', '>=', $account_fill);}
            
            if($partly_take){$data_d = $data_d->where('partly_take', '>=', $partly_take);}
            
            if($privilege_take){$data_d = $data_d->where('privilege_take', '>=', $privilege_take);}
            
            if($request->percent_period){$data_d = $data_d->whereIn('percent_paid_period', $request->percent_period);}
            
            if($request->input('input_type_dp')) {$data_d = $data_d->where('type_open','LIKE','%'.$request->input('input_type_dp').'%');}
            
            if($request->input('online_deposit')) {$data_d = $data_d->where('type_open','LIKE','%'.'1'.'%');}
            
            if($bank_type) {$data_d = $data_d->whereIn('bank_type_id', $bank_type);}
            
            $data_d = $data_d->orderBy('deposit_percent', 'DESC');
            $data_d = $data_d->get();
            $r_price = 0;
        } else {
            $data_d = Deposit::where($attr);
            $data_d = $data_d->whereIn('bank_id',$ids);
            $data_d = $data_d->where('min_sum', '<=', $request_price);
            
            if($account_fill){$data_d = $data_d->where('account_fill', '>=', $account_fill);}
            
            if($partly_take){$data_d = $data_d->where('partly_take', '>=', $partly_take);}
            
            if($privilege_take){$data_d = $data_d->where('privilege_take', '>=', $privilege_take);}
            
            if($request->percent_period){$data_d = $data_d->whereIn('percent_paid_period', $request->percent_period);}
            
            if($request->input('input_type_dp')){$data_d = $data_d->where('type_open','LIKE','%'.$request->input('input_type_dp').'%');}

            if($request->input ('date_deposit_type')){$data_d = $data_d->where('deposit_date_type','LIKE','%'.$request->input('date_deposit_type').'%');}
            
            if($request->input('online_deposit')){$data_d = $data_d->where('type_open','LIKE','%'.'1'.'%');}
            
            if($bank_type){$data_d = $data_d->whereIn('bank_type_id', $bank_type);}
            
            $data_d = $data_d->orderBy('deposit_percent', 'DESC');
            //->groupBy('bank_id')
            $data_d = $data_d->get();
        
            $r_price = $request->price;
        }

        $data = $this->groupBy($data_d,'bank_id',$request);

        $type_bank = TypeBanks::all();
        return view('frontend.deposits.ajax-content',compact(
            'data',
            'r_price',
            'date_dp',
            'filter_type',
            'type_bank',
            'single_bank_id'
        ))->render();
    }

    public function viewDeposit($locale=false, $slug){

        $meta_tags = PagesMetaTags::where('page',config('global.pages.deposit'))->where('parent_id',0)->get();

        $deposit = Deposit::where('slug',$slug)->first();

        $list = Deposit::where('status',0)->where('bank_id',$deposit->bank_id)->limit(5)->orderBY('created_at','desc')->get();

        $deposit_dates = $deposit->deposit_date_type;
        
        if($deposit_dates == 32) {
            $date_list = DateService::where(['service_id'=>1,'status' => 1])->where('position','>',0)->where('id','!=', 32)->orderBy('position','ASC')->get();
        } else {
            $date_list = DateService::where(['service_id'=>1,'status' => 1])->whereIn('id',explode(",",$deposit_dates))->orderBy('position','ASC')->get();
        }

        $regions = Region::where([
            ['parent_id', '=', null],
            ['id', '!=', 224]
        ])->orderBy('name_uz')->get();

        return view('frontend.deposits.deposit',compact(
            'deposit',
            'list',
            'date_list',
            'meta_tags',
            'regions'
        ));

    }

    public function removeNull(){

        $d = Deposit::all();
        foreach($d as $r){
            if(!$r->percent_paid_period){
                $r->percent_paid_period = 0;
            }
            if(!$r->close_before_date){
                $r->close_before_date = 0;
            }
            $r->save();
        }

        return 'Successfully';
    }

    public function calculationDeposit(Request $request) {
        // oylik capitalizatsiya x * (1+(foiz/12))^oy
        
        $validator =  Validator::make($request->all(),[
            'date'=>'required',
            'sum'=>'required',
            //'capitalization'=>'required',
        ]);
        if($validator->fails()){
            $data = [
                'error' => $validator->errors()
            ];
        } else {
            $inMonth = strstr($request->input('date'),'oy');
            $inYear = strstr($request->input('date'),'yil');

            $currency = Deposit::find($request->deposit_id);
            if($inMonth){
                $date = str_replace('oy','',$request->input('date'));
                if(strpos($date,',')){
                    list($o,$t) = explode(',', $date);
                    $date = $o.'.'.$t;
                }
                //dd($date);
                //if($request->capitalization == '0'){ // capitalizatsiya yoq
                    $percent_pr_month = $request->percent / 12;
                    $sum_percent_month =  ($percent_pr_month * (float)$request->input('sum')) / 100;
                    $all_percent_sum = (float)$date * $sum_percent_month;
                    $result_cap = $all_percent_sum + (float)$request->input('sum');
                //}
                //dd($percent_pr_month, $sum_percent_month, $all_percent_sum, $result_cap);
               /* if($request->capitalization == '1'){ // capitalizatsiya bor
                    $result_cap = (float)$request->input('sum') *  pow((1 + ($request->percent/100)/12),(float)$date);
                }*/

                //$all_percent_sum = (float)$result_cap -  (float)$request->input('sum');
                
                $data = [
                    'error' => '',
                    'month_sum' => number_format($all_percent_sum),
                    'all_sum'   => number_format($result_cap),
                    'currency'  =>  $currency->priceCurrency->name
                ];
            }


            if($inYear){
                $date = str_replace('yil','',$request->input('date'));
                if(strpos($date,',')){
                    list($o,$t) = explode(',', $date);
                    $date = $o.'.'.$t;
                }
                    
                //if($request->capitalization == '0'){ // capitalizatsiya yoq
                    $sum_percent_year = ($request->percent * (float)$request->input('sum')) / 100;
                    $all_percent_sum = (float)$date * $sum_percent_year;
                    $result_cap = $all_percent_sum + (float)$request->input('sum');
                //}

                /*if($request->capitalization == '1'){ // capitalizatsiya bor
                    $result_cap = (float)$request->input('sum') *  pow((1 + (($request->percent / 100)/12)),((float)$date)*12);
                }*/

                $all_percent_sum = (float)$result_cap -  (float)$request->input('sum');
               

                $data = [
                    'error' => '',
                    'month_sum' => number_format($all_percent_sum),
                    'all_sum'   => number_format($result_cap),
                    'currency'  =>  $currency->priceCurrency->name
                ];


            }
        }

        return response()->json($data);
    }

    public function otherDepositList(Request $request) {
        $request_price = $request->price;
        if(!$request->price){
            $request_price = 10000000000;
        }
        $r_price = 0;
        $this_id = $request->this_id;
        $date_dp = '';
        $date_dp = $request->date_deposit_type;
        if($request->type_filter === 'small'){
            $attr = [
                'status' => 0
            ];
            if($request->input('currency') > 0){
                $attr = array_merge($attr,['currency'=>$request->input('currency')]);
            }

            if($request->input('price')){
                $r_price = $request->price;
            }

            $data = Deposit::where('bank_id',$request->bank_id);
            $data = $data->where($attr);

            if($request->input ('date_deposit_type')){
                $data = $data->where('deposit_date_type','LIKE','%'.$request->input('date_deposit_type').'%');
            }
            if($request->input('online_deposit')){
               $data = $data->where('type_open','LIKE','%'.'1'.'%');
            }

            $data = $data->where('min_sum', '<=', $request->input('price') > 0 ? $request->input('price') : 1000000000);
            $data = $data->orderBy('deposit_percent', 'DESC');
            $data = $data->get();}
        if($request->type_filter === 'large'){
            $r_price = 0;
            $tp_banks = $request->type_banks;
            $add_rules = $request->additional_rule;
            $cap = $request->capitalization;
            $percent_period = $request->percent_period;
            if($request->type_banks==null){$tp_banks =  [];}
            if($request->additional_rule==null){$add_rules = [];}
            if($request->percent_period==null){$percent_period = [];}
            
            $ids=range(1,100);
            $bank_type = [];
            if(!$request->input('type_banks_btn')){
                if($tp_banks){
                    if(in_array('state_bank',$tp_banks)){
                        $bank_type = array_merge($bank_type,[1]);
                        $tp_banks = array_diff($tp_banks,['state_bank']);
                    }
                    if(in_array('private_bank',$tp_banks)){
                         $bank_type = array_merge($bank_type,[2]);
                        $tp_banks = array_diff($tp_banks,['private_bank']);
                    }
                    if (in_array('foreign_bank',$tp_banks)  ){
                        $bank_type = array_merge($bank_type,[3]);
                        $tp_banks = array_diff($tp_banks,['foreign_bank']);
                    }
                    if(count($tp_banks) > 0){
                        $ids = $tp_banks;
                    }
                }
            } else {
                if($request->input('type_banks_btn') == 'state_bank'){ $bank_type = [1]; }
                if($request->input('type_banks_btn') == 'private_bank'){ $bank_type = [2]; }
                if($request->input('type_banks_btn') == 'foreign_bank'){ $bank_type = [3]; }
            }

            $account_fill = 0;
            $partly_take = 0;
            $privilege_take = 0;
            if($add_rules){
                if(in_array('account_fill',$add_rules)){$account_fill = strval(1);}
                if(in_array('partly_take',$add_rules)){ $partly_take = strval(1);}
                if(in_array('privilege_take',$add_rules)){$privilege_take = strval(1);}
            }

            $capitalization = [0,1];
            if($cap){$capitalization = $request->capitalization;}
            
            $pay_percent_month = 0;
            if($percent_period){if(in_array('pay_percent_month',$percent_period)){$pay_percent_month = strval(1);}}

            $filter_type = $request->type_filter;
            $date_dp = $request->date_deposit_type;
                $attr = [];
                
                if($request->input('currency')){
                    $attr = array_merge($attr,['currency' => $request->input('currency')]);
                }
                if($request->input('capitalization')){
                    $attr = array_merge($attr,['percents_capitalization'=>$request->input('capitalization')]);
                }
            if(!$request->price and !$request->input ('date_deposit_type')){
                $data = Deposit::where('status',0);

                $data = $data->where('bank_id',$request->bank_id);

                $data = $data->where('currency',$request->input('currency'));

                if($account_fill){$data = $data->where('account_fill', '>=', $account_fill);}
                if($partly_take){$data = $data->where('partly_take', '>=', $partly_take);}
                if($privilege_take){$data = $data->where('privilege_take', '>=', $privilege_take);}
                if($request->percent_period){$data = $data->whereIn('percent_paid_period', $request->percent_period);}
                if($request->input('online_deposit')){$data = $data->where('type_open','LIKE','%'.'1'.'%');}
                if($bank_type){$data = $data->whereIn('bank_type_id', $bank_type);}
                $data = $data->orderBy('deposit_percent', 'DESC');
                $data = $data->get();
                $r_price = 0;
            } else {
                if($percent_period or $add_rules) {
                    $data = Deposit::where('status',0);
                    $data = $data->where('bank_id',$request->bank_id);
                    $data = $data->where('min_sum', '<=', $request_price);
                    if($account_fill){$data = $data->where('account_fill', '>=', $account_fill);}
                    if($partly_take){$data = $data->where('partly_take', '>=', $partly_take);}
                    if($privilege_take){
                        $data = $data->where('privilege_take', '>=', $privilege_take);
                    }
                    if($request->percent_period){
                        $data = $data->whereIn('percent_paid_period', $request->percent_period);
                    }

                    if($request->input('input_type_dp')){
                         $data = $data->where('type_open','LIKE','%'.$request->input('input_type_dp').'%');
                    }

                    if($request->input ('date_deposit_type')){
                        $data = $data->where('deposit_date_type','LIKE','%'.$request->input('date_deposit_type').'%');
                    }

                    if($request->input('online_deposit')) 
                    {
                       $data = $data->where('type_open','LIKE','%'.'1'.'%');
                    }
                    if($bank_type){
                        $data = $data->whereIn('bank_type_id', $bank_type);
                    }
                    $data = $data->where($attr);
                    $data = $data->orderBy('deposit_percent', 'DESC');
                    //->groupBy('bank_id')
                    $data = $data->get();
                } else {
                    $data = Deposit::where('status',0);
                    $data = $data->where('bank_id',$request->bank_id);
                    $data = $data->where('min_sum', '<=', $request_price);
                    if($request->input('online_deposit')) {$data = $data->where('type_open','LIKE','%'.'1'.'%');}
                    if($request->input('input_type_dp')) {$data = $data->where('type_open','LIKE','%'.$request->input('input_type_dp').'%');}
                    if($request->input ('date_deposit_type')){$data = $data->where('deposit_date_type','LIKE','%'.$request->input('date_deposit_type').'%');}
                    $data = $data->where($attr);
                    $data = $data->orderBy('deposit_percent', 'DESC');
                    $data = $data->get();
                }
                $r_price = $request->price;
            }
            /*if($request->input('price') and $request->input('date_deposit_type')){
                $r_price = $request->price;
            }*/
        }
        $type_bank = TypeBanks::all();
        return view('frontend.deposits.others-deposits-content',compact(
            'data',
            'r_price',
            'date_dp',
            'this_id',
            'type_bank'
        ))->render();
    }

   
    public function selectCurrency(Request $request){
        $currency = Currency::all();
        $id = $request->id;
        return view('frontend.components.select-currency',compact('currency','id'))->render();
    }


}
