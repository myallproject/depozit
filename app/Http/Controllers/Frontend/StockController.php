<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StockRequest;
use App\Models\StockBranches;
use App\Models\StockBuyRequest;
use App\Models\Stocks;
use Exception;
use Yajra\DataTables\Contracts\DataTable;
// StockBuyRequest

class StockController extends Controller
{

    public function index(Request $request)
    {
        // Отрасль компаний
        $branches = StockBranches::orderBy('title_' . app()->getLocale())->pluck('title_' . app()->getLocale(), 'id');

        $stocks = Stocks::whereNotNull('ordinary_stock_price')
            ->when($request->has('foreign'), function ($query) use ($request) {
                $query->where('foreign', $request->foreign);
            })->when($request->has('branch'), function ($query) use ($request) {
                $query->where('branch_id', $request->branch);
            })->get();

        // return $stocks;

        return view('frontend.stocks.index', compact('stocks', 'branches'));
    }

    /**
     * Client Submit
     * @return json
     */

    public function submit(StockRequest $request)
    {
        try {
            $data = $request->only('email', 'phone', 'stock_id');
            $data['name'] = $request->name . " " . $request->surname;
            $create = StockBuyRequest::create($data);
        } catch (Exception $e) {
            info($e->getMessage());
        }
        return response()->json(['success' => '1']);
    }
}
