<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Questions;
use App\Models\Service;
use App\Models\UserOrganization;
use App\Models\PagesMetaTags;

const PAGINATE_QUESTIONS = 10;

class QuestionsController extends Controller
{
	public function index()
	{
		$questions = Questions::paginate(PAGINATE_QUESTIONS)->onEachSide(1);
		$banks = $this->sortBanksByQuestions();
		$meta_tags = PagesMetaTags::where('page',config('global.pages.bank_question'))->where('parent_id',0)->get();
		return view('frontend.questions.index',compact('questions','banks','meta_tags'));
	}

	public function questionTypeList(Request $request)
	{
		$questions = Questions::paginate(PAGINATE_QUESTIONS)->onEachSide(1);
		$banks = Bank::where('organization_id',1)->get();
		$services = Service::all();
		$meta_tags = PagesMetaTags::where('page',config('global.pages.bank_question'))->where('parent_id',0)->get();
		
		return view('frontend.questions.questions-type-list',compact('questions','banks','services','meta_tags'));
	}
	public function giveQuestion(Request $request)
	{
		if($request->slug){
			$bank = Bank::where('slug',$request->slug)->first();
			if($request->tagid){
				$questions = Questions::where(['bank_id' => $bank->id, 'service_id'=>$request->tagid])->orderBy('created_at','DESC')->paginate(PAGINATE_QUESTIONS)->onEachSide(1);
			} else {
				$questions = Questions::where('bank_id',$bank->id)->orderBy('created_at','DESC')->paginate(PAGINATE_QUESTIONS)->onEachSide(1);
			}
			$staff = UserOrganization::where('child_org_id',$bank->id)->get();
		}
		$services = Service::all();

		return view('frontend.questions.users-questions',compact('bank','services','questions','staff'));
	}

	public function fAQuestions(Request $request){
		if($request->slug){
			$bank = Bank::where('slug',$request->slug)->first();
			if($request->tagid){
				$questions = Questions::where(['bank_id' => $bank->id, 'service_id'=>$request->tagid, 'faq' => 1])->orderBy('created_at','DESC')->paginate(PAGINATE_QUESTIONS)->onEachSide(1);
			} else {
				$questions = Questions::where(['bank_id'=>$bank->id, 'faq' => 1])->orderBy('created_at','DESC')->paginate(PAGINATE_QUESTIONS)->onEachSide(1);
			}
			$staff = UserOrganization::where('child_org_id',$bank->id)->get();
		}

		$services = Service::all();

		return view('frontend.questions.fa-questions',compact('bank','services','questions','staff'));
	}

	public function addQuestion(Request $request)
	{
		//dd($request->service_id);
		$request->validate([
			'service_id' => 'required',
			'question' => 'required',
			'bank_id' => 'required'
		]);

		$user_id = Auth::user()->id;
		$model = new Questions();
		$model->user_id = $user_id;
		$model->service_id = $request->input('service_id');
		$model->bank_id = $request->input('bank_id');
		$model->question = $request->input('question');
		$model->info_bank = $request->input('info_bank');
		$model->save();

		return redirect()->back()->with('success', 'Sizning savolingiz qabul qilindi. Iltimos javobni kuting!');
	}

	public function listAnswer(Request $request){
		if($request->slug){
			$bank = Bank::where('slug',$request->slug)->first();
			if($request->tagid){
				$questions = Questions::where(['bank_id' => $bank->id, 'service_id'=>$request->tagid])->orderBy('created_at','DESC')->paginate(PAGINATE_QUESTIONS)->onEachSide(1);
			} else {
				$questions = Questions::where('bank_id',$bank->id)->orderBy('created_at','DESC')->paginate(PAGINATE_QUESTIONS)->onEachSide(1);
			}
			$staff = UserOrganization::where('child_org_id',$bank->id)->get();
		}
		$services = Service::all();

		return view('frontend.questions.answer',compact('bank','services','questions','staff'));
	}
	public function listFaqAnswer(Request $request){
		if($request->slug){
			$bank = Bank::where('slug',$request->slug)->first();
			if($request->tagid){
				$questions = Questions::where(['bank_id' => $bank->id, 'service_id'=>$request->tagid, 'faq' => 1])->orderBy('created_at','DESC')->paginate(PAGINATE_QUESTIONS)->onEachSide(1);
			} else {
				$questions = Questions::where(['bank_id'=>$bank->id, 'faq' => 1])->orderBy('created_at','DESC')->paginate(PAGINATE_QUESTIONS)->onEachSide(1);
			}
			$staff = UserOrganization::where('child_org_id',$bank->id)->get();
		}

		$services = Service::all();

		return view('frontend.questions.faq-answer',compact('bank','services','questions','staff'));
	}

	protected function sortBanksByQuestions(){
		$banks = Bank::where('organization_id',1)->get();
		$ranked = [];
		foreach($banks as $bank){
			$temp = [
				'bank' => $bank,
				'questions' => count($bank->questions),
			];
			array_push($ranked, $temp);
		}

		usort($ranked, function($a,$b){
            return ($a['questions']>=$b['questions']) ? -1 : 1;
		});
		
		return $ranked;
	}
}
