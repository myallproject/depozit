<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\CardsProvisionType;
use App\Models\CardType;
use App\Models\CreditCards;
use App\Models\Currency;
use App\Models\DateService;
use App\Models\DebitCards;
use App\Models\PaymentType;
use App\Models\PagesMetaTags;
use App\Models\Ranking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CardsController extends Controller
{
    /**
     * debit cards functions
     **/
    public function debitCard(){
        $single_bank_id = 0;
        $currencies = Currency::whereIn('name',['UZS','USD'])->get();
        $payments = PaymentType::all();
        $card_types = CardType::all();
        $banks = Bank::all();
        $meta_tags = PagesMetaTags::where('page',config('global.pages.debit_card'))->where('parent_id',0)->get();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();

        return view('frontend.cards.debit',compact(
            'currencies',
            'payments',
            'card_types',
            'banks',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function debitCardsOfSingleBank($local, $slug){
        $single_bank_id = 0;
        if($slug){
            $bank = Bank::where('slug', $slug)->first();
            $single_bank_id = $bank->id;
        }
        $currencies = Currency::whereIn('name',['UZS','USD'])->get();
        $payments = PaymentType::all();
        $card_types = CardType::all();
        $banks = Bank::all();
        $meta_tags = PagesMetaTags::where('page',config('global.pages.debit_card'))->where('parent_id',0)->get();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();

        return view('frontend.cards.debit',compact(
            'currencies',
            'payments',
            'card_types',
            'banks',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function debitCardSmallFilter(Request $request){
        $single_bank_id = $request->single_bank_id;
        $attr = [
            'status' => 1
        ];

        if($request->input('type_card'))
            $attr = array_merge($attr,['type_card_id' => $request->input('type_card')]);
        if($request->input('only_delivered_cards')=="on")
            $attr = array_merge($attr,['uyga_yetkazish' => 1]);

        if($single_bank_id>0){
            $d = DebitCards::where('bank_id', $single_bank_id)->where($attr);
        }else{
            $d = DebitCards::where($attr);
        }

        if($request->input('currency') === 'multi'){
            $d = $d->where('currency_id','LIKE','%'.','.'%');
        } else {
            $d = $d->where('currency_id','LIKE','%'.$request->input('currency').'%');
        }
        if($request->input('payment'))
        {
            $d = $d->whereIn('payment_id',$request->input('payment'));
        }

        $d = $d->get();

        if($single_bank_id>0){
            $data = $d;
        }else{
            $data = $this->groupBy($d,'bank_id',$request);
        }

        $filter_type = $request->filter_type;

        return  view('frontend.cards.debit-card-content',compact(
            'data',
            'filter_type',
            'single_bank_id'
        ))->render();
    }

    public function groupBy($array,$key,$request){
        $return = [];
        $max_percent = [];
        $count = [];
        foreach($array as $val){
            $return[$val[$key]][] = $val;
        }
        //dd($return);
        $attr = [
            'status' => 1
        ];
        $filter_type = $request->filter_type;
        if($filter_type=='small'){
            if($request->input('type_card'))
                $attr = array_merge($attr,['type_card_id' => $request->input('type_card')]);
            if($request->input('only_delivered_cards')=="on")
                $attr = array_merge($attr,['uyga_yetkazish' => 1]);
        }elseif ($filter_type=='large') {
            if($request->input('type_card'))
                $attr = array_merge($attr,['type_card_id' => $request->input('type_card')]);
            if($request->input('bepul_xizmat')=="on")
                $attr = array_merge($attr,['bepul_xizmat' => 1]);
            if($request->input('bepul_naqdlash')=="on")
                $attr = array_merge($attr,['bepul_naqdlash' => 1]);
            if($request->input('bepul_tulov')=="on")
                $attr = array_merge($attr,['bepul_tulov' => 1]);
            if($request->input('kontaktsiz_tulov')=="on")
                $attr = array_merge($attr,['kontaktsiz_tulov' => 1]);
            if($request->input('cash_back')=="on")
                $attr = array_merge($attr,['cash_back' => 1]);
            if($request->input('delivery_service')=="on")
                $attr = array_merge($attr,['uyga_yetkazish' => 1]);
            if($request->input('aktsiya')=="on")
                $attr = array_merge($attr,['aktsiya' => 1]);
            if($request->input('d_secure')=="on")
                $attr = array_merge($attr,['d_secure' => 1]);
            if($request->input('lounge_key')=="on")
                $attr = array_merge($attr,['lounge_key' => 1]);
        }

        $tp_banks =  [];
        if($request->type_banks){
            $tp_banks = $request->type_banks;
        }
        /*1 davlat bank, 2 xususiy bank, 3 xorijiy bank*/
        if($tp_banks){
            if(!is_numeric($tp_banks[0])){
                if(in_array('state_bank',$tp_banks)){
                    array_push($tp_b_id,1);
                    $tp_banks = array_diff($tp_banks,['state_bank']);
                }
                if(in_array('private_bank',$tp_banks)){
                    array_push($tp_b_id,2);
                    $tp_banks = array_diff($tp_banks,['private_bank']);
                }
                if (in_array('foreign_bank',$tp_banks)  ){
                    array_push($tp_b_id,3);
                    $tp_banks = array_diff($tp_banks,['foreign_bank']);
                }
            } else {
                $tp_b_id = [0,1,2,3];
            }
            //dd($tp_b_id);
        } else {
            $tp_b_id = [0,1,2,3];
        }
        //dd($tp_b_id);
        foreach ($return as $k => $v){
            $d = DebitCards::where('bank_id',$k);
            $d = $d->where($attr);

            if($request->input('currency') == 'multi'){
                $d = $d->where('currency_id','LIKE','%'.','.'%');
            } else {
                $d = $d->where('currency_id','LIKE','%'.$request->input('currency').'%');
            }

            if($request->input('payment'))
            {
                $d = $d->whereIn('payment_id',$request->input('payment'));
            }

            $d = $d->whereIn('bank_type_id',$tp_b_id);

            $d = $d->first();

            $max_percent[] = $d;
            $count[$k] = count($v);
        }

        $data = [
            'max_cr_id' => $max_percent,
            'data'      => $return,
            'count'     => $count
        ];
        //dd($data);
        return $data;
    }

    public function debitCardLargeFilter(Request $request){
        $single_bank_id = 0;
        $attr = [
            'status' => 1
        ];

        if($request->input('type_card'))
            $attr = array_merge($attr,['type_card_id' => $request->input('type_card')]);
        if($request->input('bepul_xizmat')=="on")
            $attr = array_merge($attr,['bepul_xizmat' => 1]);
        if($request->input('bepul_naqdlash')=="on")
            $attr = array_merge($attr,['bepul_naqdlash' => 1]);
        if($request->input('bepul_tulov')=="on")
            $attr = array_merge($attr,['bepul_tulov' => 1]);
        if($request->input('kontaktsiz_tulov')=="on")
            $attr = array_merge($attr,['kontaktsiz_tulov' => 1]);
        if($request->input('cash_back')=="on")
            $attr = array_merge($attr,['cash_back' => 1]);
        if($request->input('delivery_service')=="on")
            $attr = array_merge($attr,['uyga_yetkazish' => 1]);
        if($request->input('aktsiya')=="on")
            $attr = array_merge($attr,['aktsiya' => 1]);
        if($request->input('d_secure')=="on")
            $attr = array_merge($attr,['d_secure' => 1]);
        if($request->input('lounge_key')=="on")
            $attr = array_merge($attr,['lounge_key' => 1]);

        $tp_banks = $request->type_banks;
        $tp_b_id = [];
        /*1 davlat bank, 2 xususiy bank, 3 xorijiy bank*/
        $ids= range(1,100);
        if($tp_banks){
            if(!is_numeric($tp_banks[0])){
                if(in_array('state_bank',$tp_banks)){
                    array_push($tp_b_id,1);
                    $tp_banks = array_diff($tp_banks,['state_bank']);
                }
                if(in_array('private_bank',$tp_banks)){
                    array_push($tp_b_id,2);
                    $tp_banks = array_diff($tp_banks,['private_bank']);
                }
                if (in_array('foreign_bank',$tp_banks)  ){
                    array_push($tp_b_id,3);
                    $tp_banks = array_diff($tp_banks,['foreign_bank']);
                }
            } else{
                $tp_b_id = [0,1,2,3];
            }
            if(count($tp_banks) > 0){
                $ids = $tp_banks;
            }
        }else{
            $tp_b_id = [0,1,2,3];
        }

        $d = DebitCards::whereIn('bank_id',$ids);
        
        if($request->input('payment'))
        {
            $d = $d->whereIn('payment_id',$request->input('payment'));
        }

        $d = $d->where($attr);

        if($request->input('currency') == 'multi'){
            $d = $d->where('currency_id','LIKE','%'.','.'%');
        } else {
            $d = $d->where('currency_id','LIKE','%'.$request->input('currency').'%');
        }
        
        $d = $d->whereIn('bank_type_id',$tp_b_id);
        
        $d = $d->get();

        $data = $this->groupBy($d,'bank_id',$request);

        $filter_type = $request->filter_type;

        return  view('frontend.cards.debit-card-content',compact(
            'data',
            'filter_type',
            'single_bank_id'
        ))->render();
    }

    public function otherDebitCards(Request $request){
        if($request->filter_type == 'small'){
            $attr = [
                'status' => 1,
                'bank_id' => $request->bank_id
            ];
           
            if($request->input('type_card'))
                $attr = array_merge($attr,['type_card_id' => $request->input('type_card')]);
            if($request->input('only_delivered_cards')=="on")
                $attr = array_merge($attr,['uyga_yetkazish' => 1]);

            $data = DebitCards::where($attr)->where('id','!=',$request->this_id);
            if($request->input('payment'))
            {
                $data = $data->whereIn('payment_id',$request->input('payment'));
            }
            if($request->input('currency') == 'multi'){
                $data = $data->where('currency_id','LIKE','%'.','.'%');
            } else {
                $data = $data->where('currency_id','LIKE','%'.$request->input('currency').'%');
            }
           

            $data = $data->get();
        }
        if($request->filter_type == 'large'){
            $attr = [
                'status' => 1,
                'bank_id' => $request->bank_id
            ];
           
            if($request->input('type_card'))
                $attr = array_merge($attr,['type_card_id' => $request->input('type_card')]);
            if($request->input('bepul_xizmat')=="on")
                $attr = array_merge($attr,['bepul_xizmat' => 1]);
            if($request->input('bepul_naqdlash')=="on")
                $attr = array_merge($attr,['bepul_naqdlash' => 1]);
            if($request->input('bepul_tulov')=="on")
                $attr = array_merge($attr,['bepul_tulov' => 1]);
            if($request->input('kontaktsiz_tulov')=="on")
                $attr = array_merge($attr,['kontaktsiz_tulov' => 1]);
            if($request->input('cash_back')=="on")
                $attr = array_merge($attr,['cash_back' => 1]);
            if($request->input('delivery_service')=="on")
                $attr = array_merge($attr,['uyga_yetkazish' => 1]);
            if($request->input('aktsiya')=="on")
                $attr = array_merge($attr,['aktsiya' => 1]);
            if($request->input('d_secure')=="on")
                $attr = array_merge($attr,['d_secure' => 1]);
            if($request->input('lounge_key')=="on")
                $attr = array_merge($attr,['lounge_key' => 1]);
                
            $tp_banks = $request->type_banks;
            $tp_b_id = [];
            /*1 davlat bank, 2 xususiy bank, 3 xorijiy bank*/

            if($tp_banks){
                if(!is_numeric($tp_banks[0])){
                    if(in_array('state_bank',$tp_banks)){
                        array_push($tp_b_id,1);
                    }
                    if(in_array('private_bank',$tp_banks)){
                        array_push($tp_b_id,2);
                    }
                    if (in_array('foreign_bank',$tp_banks)  ){
                        array_push($tp_b_id,3);
                    }
                } else{
                    $tp_b_id = [0,1,2,3];
                }
            }else{
                $tp_b_id = [0,1,2,3];
            }

            $data = DebitCards::where($attr)->where('id','!=',$request->this_id);
            
            if($request->input('payment'))
            {
                $data = $data->whereIn('payment_id',$request->input('payment'));
            }

            if($request->input('currency') == 'multi'){
                $data = $data->where('currency_id','LIKE','%'.','.'%');
            } else {
                $data = $data->where('currency_id','LIKE','%'.$request->input('currency').'%');
            }

            $data = $data->whereIn('bank_type_id',$tp_b_id);
            $data = $data->get();

        }
        $filter_type = $request->filter_type;

        return view('frontend.cards.other.debit',compact('data','filter_type'))->render();
    }

    /**
     * credit cards functions
     **/
    public function creditCard(){
        $single_bank_id = 0;
        $currencies = Currency::whereIn('name',['UZS','USD'])->get();
        $provisions = CardsProvisionType::all();
        $card_types = CardType::all();
        $banks = Bank::all();
        $dates = DateService::where(['service_id' => 3, 'child_service_id' => 2])->get();
        $meta_tags = PagesMetaTags::where('page',config('global.pages.credit_card'))->where('parent_id',0)->get();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();
        
        return view('frontend.cards.credit',compact(
            'card_types',
            'provisions',
            'currencies',
            'banks',
            'dates',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function creditCardsOfSingleBank($local, $slug){
        $single_bank_id = 0;
        if($slug){
            $bank = Bank::where('slug', $slug)->first();
            $single_bank_id = $bank->id;
        }
        $currencies = Currency::whereIn('name',['UZS','USD'])->get();
        $provisions = CardsProvisionType::all();
        $card_types = CardType::all();
        $banks = Bank::all();
        $dates = DateService::where(['service_id' => 3, 'child_service_id' => 2])->get();
        $meta_tags = PagesMetaTags::where('page',config('global.pages.credit_card'))->where('parent_id',0)->get();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();
        
        return view('frontend.cards.credit',compact(
            'card_types',
            'provisions',
            'currencies',
            'banks',
            'dates',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function creditCardSmallFilter(Request $request){
        $single_bank_id = $request->single_bank_id;
        $validator = '';

        if($request->max_sum) {
            $validator =  Validator::make($request->all(),[
                'max_sum'=>'numeric',
            ]);
        }

        $attr = [
            'status' => 1
        ];

        if($validator and $validator->fails()){
            return ['error' => $validator->errors()];
        } else {

            if($request->input('date'))
                $attr = array_merge($attr,['date_id' =>$request->input('date')]);

            if($single_bank_id>0){
                $d = CreditCards::where('bank_id', $single_bank_id)->where($attr);
            }else{
                $d = CreditCards::where($attr);
            }
            
            if($request->input('max_sum')){
                $d = $d->where('max_sum','<=',$request->input('max_sum'));
            }

            if($request->input('provision')){
               $d = $d->where('taminot_turi_ids','LIKE','%'.$request->input('provision').'%');
            }
            $d = $d->get();
           
            if($single_bank_id>0){
                $data = $d;
            }else{
                $data = $this->groupByCreditCard($d,'bank_id',$request);
            }
            
            $filter_type = $request->filter_type;

            return  view('frontend.cards.credit-card-content',compact(
                'data',
                'filter_type',
                'single_bank_id'
            ))->render();
        }
    }

    public function creditCardLargeFilter(Request $request){
        return abort(404);
    }

    public function groupByCreditCard($array,$key,$request){
        $return = [];
        $max_percent = [];
        $count = [];
        foreach($array as $val){
            $return[$val[$key]][] = $val;
        }
        //dd($array);
        $attr = [
            'status' => 1
        ];
        if($request->input('date'))
            $attr = array_merge($attr,['date_id' =>$request->input('date')]);

        foreach ($return as $k => $v){
            $card = CreditCards::where('bank_id',$k);
                $card = $card->where($attr);
                
                if($request->input('max_sum')){
                    $card = $card->where('max_sum','<=',$request->input('max_sum'));
                }

                if($request->input('provision')){
                    $card = $card->where('taminot_turi_ids','LIKE','%'.$request->input('provision').'%');
                }
                $card = $card->first();
            $max_percent[] = $card;
            $count[$k] = count($v);
        }
        //dd($max_percent);
        $data = [
            'max_cr_id' => $max_percent,
            'data'      => $return,
            'count'     => $count
        ];
        //dd($data);
        return $data;
    }

    public function otherCreditCards(Request $request){
        if($request->filter_type == 'small'){

            $validator = '';

            if($request->max_sum) {
                $validator =  Validator::make($request->all(),[
                    'max_sum'=>'numeric',
                ]);
            }

            $attr = [
                'status' => 1,
                'bank_id' => $request->bank_id
            ];

            if($validator and $validator->fails()){
                return ['error' => $validator->errors()];
            } else {

                if($request->input('date'))
                    $attr = array_merge($attr,['date_id' =>$request->input('date')]);

                $d = CreditCards::where('id','!=',$request->this_id);
                $d = $d->where($attr);
                if($request->input('max_sum')){
                    $d = $d->where('max_sum','<=',$request->input('max_sum'));
                }
                
                if($request->input('provision')){
                   $d = $d->where('taminot_turi_ids','LIKE','%'.$request->input('provision').'%');
                }
                $d = $d->get();
               
                $data = $d;
                
                $filter_type = $request->filter_type;
                return  view('frontend.cards.other.credit',compact(
                    'data','filter_type'
                ))->render();
            }

        }

        if($request->filter_type == 'large'){

        }
    }
}
