<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Brand;
use App\Http\Controllers\Controller;
use App\Models\DateService;
use App\Models\Telephone;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\App;

class RassrochkaController extends Controller
{
    public function list() {
        $brand = Brand::all();
        $telephones = Telephone::where('brand_id', 101)->orderBy('created_at', 'DESC')->first();
        return view('frontend.rassrochka.telephones.list', compact('telephones', 'brand'));
        
    }

    public function telephones() {
        $brands = Brand::all();
        $telephones = Telephone::orderBy('created_at', 'DESC')->get();
        $type_date = DateService::where(['service_id'=>1,'status' => 1])->where('position','>',0)->orderBy('position','asc')->get();
        return view('frontend.rassrochka.telephones.telephones', 
            compact('telephones', 
                    'brands',
                    'type_date'
                ));
        
    }
    
}
