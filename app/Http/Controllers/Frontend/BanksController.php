<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;
use App\Models\Ranking;
use App;

class BanksController extends Controller
{
    public function search(Request $request){
        $banks=[];
        $ranking = new Ranking;
        if($request->bank_name){
            $banks = $ranking->searchBanks($request->bank_name);
        } else {
            $banks = $ranking->rankedBanks();
        }
        return view('frontend.banks.ajax-banks-content',compact('banks'))->render();
    }
}
