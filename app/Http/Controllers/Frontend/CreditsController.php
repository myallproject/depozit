<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Car;
use App\Models\CarBrand;
use App\Models\Credit;
use App\Models\CreditBoshBadal;
use App\Models\CreditGoal;
use App\Models\CreditIssuingForm;
use App\Models\CreditProvision;
use App\Models\CreditReviewPeriod;
use App\Models\CreditType;
use App\Models\Currency;
use App\Models\DataUpdateTime;
use App\Models\DateService;
use App\Models\Region;
use App\Models\TypeBanks;
use App\Models\MinimalPayment;
use App\Models\PagesMetaTags;
use App\Models\Ranking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Config;

define('CREDIT_ID', Config::get('global.services.credit'));
define('CONSUME_CREDIT', Config::get('global.credits.consume'));
//const CREDIT_ID = Config::get('global.services.credit');

class CreditsController extends Controller
{
    public function credits(Request $request)
    {
    }

    public function creditConsumer()
    {
        $single_bank_id = 0;
        $meta_tags = PagesMetaTags::where('page', config('global.pages.consume_credits'))->where('parent_id', 0)->get();
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', CONSUME_CREDIT)->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();

        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->get();

        $goals = CreditGoal::where('service_id', $credit_type->id)->where('status', 1)->get();
        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $provisions = CreditProvision::where('service_id', $credit_type->id)->get();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();
        
        return view('frontend.credits.consumer',compact(
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'update_time',
            'meta_tags',
            'single_bank_id'

        ));
    }

    public function consumerCreditsOfBank($local, $slug)
    {
        $single_bank_id = 0;
        if ($slug) {
            $bank = Bank::where('slug', $slug)->first();
            $single_bank_id = $bank->id;
        }
        $meta_tags = PagesMetaTags::where('page', config('global.pages.consume_credits'))->where('parent_id', 0)->get();
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', CONSUME_CREDIT)->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();

        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->get();

        $goals = CreditGoal::where('service_id', $credit_type->id)->where('status', 1)->get();
        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $provisions = CreditProvision::where('service_id', $credit_type->id)->get();


        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();

        return view('frontend.credits.consumer', compact(
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'update_time',
            'meta_tags',
            'single_bank_id'

        ));
    }

    public function creditAuto()
    {
        $single_bank_id = 0;
        $meta_tags = PagesMetaTags::where('page', config('global.pages.auto_credits'))->where('parent_id', 0)->get();
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', config('global.credits.auto'))->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();
        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->get();

        $goals = CreditGoal::where('service_id', $credit_type->id)->get();

        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $provisions = CreditProvision::where('service_id', $credit_type->id)->get();

        $bosh_badal = CreditBoshBadal::where(['service_id'=>$credit_type->id,'status'=>1])->orderBy('name_uz','ASC')->get();
        
        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();

        return view('frontend.credits.auto',compact(
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'update_time',
            'bosh_badal',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function autoCreditsOfBank($local, $slug)
    {
        $single_bank_id = 0;
        if ($slug) {
            $bank = Bank::where('slug', $slug)->first();
            $single_bank_id = $bank->id;
        }
        $meta_tags = PagesMetaTags::where('page', config('global.pages.auto_credits'))->where('parent_id', 0)->get();
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', config('global.credits.auto'))->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();
        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->get();

        $goals = CreditGoal::where('service_id', $credit_type->id)->get();

        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $provisions = CreditProvision::where('service_id', $credit_type->id)->get();

        $bosh_badal = CreditBoshBadal::where(['service_id'=>$credit_type->id,'status'=>1])->orderBy('name_uz','ASC')->get();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();

        return view('frontend.credits.auto',compact(
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'update_time',
            'bosh_badal',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function creditEducation()
    {
        $single_bank_id = 0;
        $meta_tags = PagesMetaTags::where('page', config('global.pages.edu_credits'))->where('parent_id', 0)->get();
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', 'talim-krediti-17292')->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();
        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->get();
        $goals = CreditGoal::where('service_id', $credit_type->id)->get();
        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $provisions = CreditProvision::where('service_id',$credit_type->id)->get();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();
        
        return view('frontend.credits.education',compact(
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'update_time',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function educationCreditsOfBank($local, $slug)
    {
        $single_bank_id = 0;
        if ($slug) {
            $bank = Bank::where('slug', $slug)->first();
            $single_bank_id = $bank->id;
        }
        $meta_tags = PagesMetaTags::where('page', config('global.pages.edu_credits'))->where('parent_id', 0)->get();
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', 'talim-krediti-17292')->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();
        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->get();
        $goals = CreditGoal::where('service_id', $credit_type->id)->get();
        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $provisions = CreditProvision::where('service_id',$credit_type->id)->get();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();

        return view('frontend.credits.education',compact(
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'update_time',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function creditMicroCredit()
    {
        $single_bank_id = 0;
        $meta_tags = PagesMetaTags::where('page', config('global.pages.micro_credits'))->where('parent_id', 0)->get();
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', 'mikro-qarzlar-72982')->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();
        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->orderBy('position', 'ASC')->get();
        $goals = CreditGoal::where('service_id', $credit_type->id)->get();
        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $provisions = CreditProvision::where('service_id', $credit_type->id)->get();

        $bhmModel = MinimalPayment::where(['status' => 1, 'id' => 1])->first();
        $bhm = 223000 * 100;
        if ($bhmModel) {
            $bhm = $bhmModel['summa'] * 100;
        } else {
            $bhm = 223000 * 100;
        }

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();

        return view('frontend.credits.micro-credit',compact(
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'update_time',
            'bhm',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function microCreditsOfBank($local, $slug)
    {
        $single_bank_id = 0;
        if ($slug) {
            $bank = Bank::where('slug', $slug)->first();
            $single_bank_id = $bank->id;
        }
        $meta_tags = PagesMetaTags::where('page', config('global.pages.micro_credits'))->where('parent_id', 0)->get();
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', 'mikro-qarzlar-72982')->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();
        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->orderBy('position', 'ASC')->get();
        $goals = CreditGoal::where('service_id', $credit_type->id)->get();
        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $provisions = CreditProvision::where('service_id', $credit_type->id)->get();

        $bhmModel = MinimalPayment::where(['status' => 1, 'id' => 1])->first();
        $bhm = 223000 * 100;
        if ($bhmModel) {
            $bhm = $bhmModel['summa'] * 100;
        } else {
            $bhm = 223000 * 100;
        }

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();

        return view('frontend.credits.micro-credit',compact(
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'update_time',
            'bhm',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function creditMortgage()
    {
        $single_bank_id = 0;
        $meta_tags = PagesMetaTags::where('page', config('global.pages.mortgage_credits'))->where('parent_id', 0)->get();
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', 'ipoteka-kreditlari-64597')->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();
        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->get();
        $goals = CreditGoal::where('service_id', $credit_type->id)->get();
        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $badals = CreditBoshBadal::where('service_id',$credit_type_id)->get();
        $provisions = CreditProvision::where('service_id',$credit_type->id)->get();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();

        return view('frontend.credits.mortgage',compact(
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'badals',
            'update_time',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function mortgageCreditsOfBank($local, $slug)
    {
        $single_bank_id = 0;
        if ($slug) {
            $bank = Bank::where('slug', $slug)->first();
            $single_bank_id = $bank->id;
        }
        $meta_tags = PagesMetaTags::where('page', config('global.pages.mortgage_credits'))->where('parent_id', 0)->get();
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', 'ipoteka-kreditlari-64597')->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();
        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->get();
        $goals = CreditGoal::where('service_id', $credit_type->id)->get();
        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $badals = CreditBoshBadal::where('service_id',$credit_type_id)->get();
        $provisions = CreditProvision::where('service_id',$credit_type->id)->get();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();

        return view('frontend.credits.mortgage',compact(
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'badals',
            'update_time',
            'meta_tags',
            'single_bank_id'
        ));
    }

    public function creditOverdraft(){
        $meta_tags = PagesMetaTags::where('page',config('global.pages.overdraft'))->where('parent_id',0)->get();
        $single_bank_id = 0;
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', 'overdraft-kreditlari-42249')->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();
        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->get();
        $goals = CreditGoal::where('service_id', $credit_type->id)->get();
        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $badals = CreditBoshBadal::where('service_id',$credit_type_id)->get();
        $provisions = CreditProvision::where('service_id',$credit_type->id)->get();

        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();

        return view('frontend.credits.overdraft',compact(
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'badals',
            'update_time',
            // 'meta_tags',
            'single_bank_id'
        ));
    }

    public function overdraftCreditsOfBank($local, $slug){
        $single_bank_id = 0;
        if ($slug) {
            $bank = Bank::where('slug', $slug)->first();
            $single_bank_id = $bank->id;
        }
        $meta_tags = PagesMetaTags::where('page', config('global.pages.overdraft'))->where('parent_id', 0)->get();
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', 'overdraft-kreditlari-42249')->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();

        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->get();

        $goals = CreditGoal::where('service_id', $credit_type->id)->where('status', 1)->get();
        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $provisions = CreditProvision::where('service_id', $credit_type->id)->get();


        //Caching bank positions for 60 min
        $ranking = new Ranking;
        $ranking->cacheBankPositions();
        
        return view('frontend.credits.overdraft',compact(
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'update_time',
            'meta_tags',
            'single_bank_id'

        ));
    }

    /*Filter consumer credit */
    public function filterConsumer(Request $request)
    {
        $single_bank_id = $request->single_bank_id;
        $filter_type = $request->type_filter;
        $credit_date = '';
        $attr = [
            'type_credit_id' => $request->credit_type_id,
            'status' => 1
        ];

        if ($single_bank_id > 0) {
            $data_c = Credit::where('bank_id', $single_bank_id)->where($attr);
        } else {
            $data_c = Credit::where($attr);
        }

        if ($request->input('amount')) {
            $data_c = $data_c->where('amount', '>=', $request->amount);
        }
        $singlePrint = false;
        if ($request->input('date')) {
            $data_c =  $data_c->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
            $credit_date = $request->input('date');
        } else {
            $singlePrint = true;
        }

        if ($request->input('provision')) {
            $data_c =  $data_c->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
        }

        if ($request->input('online_credit')) {
            $data_c = $data_c->where('credit_issuing_form_id', 1);
        }

        $data_c = $data_c->orderBy('percent', 'ASC');

        $data_c  = $data_c->get();
        if ($single_bank_id > 0) {
            $data = $data_c;
        } else {
            $data = $this->groupBy($data_c, 'bank_id', $request);
        }

        $type_bank = TypeBanks::all();
        return view('frontend.credits.ajax-content.consumer', compact(
            'singlePrint',
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'single_bank_id'
        ))->render();
    }

    public function groupBy($array, $key, $request)
    {

        $return = [];
        $max_percent = [];
        $count = [];

        foreach ($array as $val) {
            $b_r = explode(',', $val->bank->region_id);
            if ($request->input('region')) {
                if (in_array($request->input('region'), $b_r)) {
                    $return[$val[$key]][] = $val;
                }
            } else {
                $return[$val[$key]][] = $val;
            }
        }

        $arrAttr = [
            'status' => 1,
            'type_credit_id' => $request->credit_type_id,
        ];

        if ($request->type_filter == 'large') {
            if ($request->input('currency')) {
                $arrAttr = array_merge($arrAttr, ['currency' => $request->input('currency')]);
            }
        }

        if ($request->input('goal'))
            $arrAttr = array_merge($arrAttr, ['credit_goal_id' => $request->input('goal')]);

        if ($request->input('review_period'))
            $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);
        if ($request->input('credit_issuing_form'))
            $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

        $tp_banks = $request->type_banks;

        $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

        $tp_b_id  = $bank_type['tp_b_id'];

        foreach ($return as $k => $v) {
            $cr = Credit::where('bank_id', $k);
            $cr = $cr->where($arrAttr);

            if ($request->input('amount')) {
                $cr = $cr->where('amount', '>=', $request->amount);
            }

            $cr = $cr->whereIn('type_bank_id', $tp_b_id);

            if ($request->input('date')) {
                $cr =  $cr->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
            }

            if ($request->input('compensation')) {
                $cr =  $cr->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }

            if ($request->input('online_credit')) {
                $cr = $cr->where('credit_issuing_form_id', 1);
            }

            if ($request->input('provision')) {
                $cr = $cr->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
            }

            $cr = $cr->orderBy('percent', 'ASC');

            $cr = $cr->first();

            $max_percent[] = $cr;
            $count[$k] = count($v);
        }

        $data = [
            'max_cr_id' => $max_percent,
            'data'      => $return,
            'count'     => $count
        ];
        return $data;
    }

    public function largeConsumerFilter(Request $request)
    {
        $single_bank_id = 0;
        $filter_type = $request->type_filter;
        $credit_date = '';
        $arrAttr = [
            'status' => 1,
            'type_credit_id' => $request->credit_type_id
        ];
        $singlePrint = true;
        if ($request->input('currency'))
            $arrAttr = array_merge($arrAttr, ['currency' => $request->input('currency')]);

        if ($request->input('goal'))
            $arrAttr = array_merge($arrAttr, ['credit_goal_id' => $request->input('goal')]);

        if ($request->input('review_period'))
            $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);

        if ($request->input('credit_issuing_form'))
            $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

        $tp_banks = $request->type_banks;

        /*1 davlat bank, 2 xususiy bank, 3 xorijiy bank*/
        $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

        $tp_b_id  = $bank_type['tp_b_id'];

        $ids = $bank_type['ids'];

        $data_c = Credit::whereIn('bank_id', $ids);

        $data_c = $data_c->where($arrAttr);

        if ($request->input('amount')) {
            $data_c = $data_c->where('amount', '>=', $request->amount);
        }

        if ($request->input('date')) {
            $singlePrint = false;
            $data_c =  $data_c->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
            $credit_date = $request->input('date');
        }

        if ($request->input('online_credit')) {
            $data_c = $data_c->where('credit_issuing_form_id', 1);
        }
        if ($request->input('compensation')) {
            $data_c = $data_c->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
        }
        $data_c = $data_c->whereIn('type_bank_id', $tp_b_id);

        if ($request->input('provision')) {
            $data_c = $data_c->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
        }

        $data_c = $data_c->get();

        $data = $this->groupBy($data_c, 'bank_id', $request);
        $type_bank = TypeBanks::all();

        return view('frontend.credits.ajax-content.consumer', compact(
            'singlePrint',
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'single_bank_id'
        ))->render();
    }

    public function otherConsumerCredit(Request $request)
    {
        $data = [];
        $credit_date = '';
        if ($request->type_filter == 'small') {
            if ($request) {
                $attr = [
                    'bank_id' => $request->bank_id,
                    'type_credit_id' => $request->credit_type_id,
                    'status' => 1
                ];
                $singlePrint = true;
                $data = Credit::where($attr);

                if ($request->input('date')) {
                    $singlePrint = false;
                    $data =  $data->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                    $credit_date = $request->input('date');
                }

                if ($request->input('online_credit')) {
                    $data = $data->where('credit_issuing_form_id', 1);
                }

                $data = $data->where('id', '!=', $request->this_id);

                if ($request->input('amount')) {
                    $data = $data->where('amount', '>=', $request->amount);
                }

                if ($request->input('provision')) {
                    $data =  $data->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
                }

                $data = $data->orderBy('percent', 'ASC')->get();
            }
        }
        if ($request->type_filter == 'large') {
            $arrAttr = [
                'status' => 1,
                'type_credit_id' => $request->credit_type_id,
                'bank_id' => $request->bank_id
            ];

            if ($request->input('currency'))
                $arrAttr = array_merge($arrAttr, ['currency' => $request->input('currency')]);

            if ($request->input('goal'))
                $arrAttr = array_merge($arrAttr, ['credit_goal_id' => $request->input('goal')]);

            if ($request->input('review_period'))
                $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);

            if ($request->input('credit_issuing_form'))
                $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

            $tp_banks = $request->type_banks;

            $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

            $tp_b_id  = $bank_type['tp_b_id'];

            $data = Credit::where($arrAttr);

            $data =  $data->where('id', '!=', $request->this_id);

            if ($request->input('amount')) {
                $data = $data->where('amount', '>=', $request->amount);
            }

            if ($request->input('online_credit')) {
                $data = $data->where('credit_issuing_form_id', $request->input('online_credit'));
            }

            if ($request->input('compensation')) {
                $data = $data->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }

            if ($request->input('date')) {
                $data =  $data->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }

            $data =  $data->whereIn('type_bank_id', $tp_b_id);

            if ($request->input('provision')) {
                $data =  $data->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
            }

            $data =  $data->get();
        }
        $type_bank = TypeBanks::all();
        return view('frontend.credits.other.consumer', compact(
            'singlePrint',
            'data',
            'type_bank',
            'credit_date'
        ))->render();
    }
    /*End consumer credit filter*/

    /*Auto credit filter*/
    public function filterAuto(Request $request)
    {
        $single_bank_id = $request->single_bank_id;
        $filter_type = $request->type_filter;
        $credit_date = '';
        if ($request) {
            $attr = [
                'type_credit_id' => $request->credit_type_id,
                'status' => 1
            ];
            $singlePrint = true;

            if ($single_bank_id > 0) {
                $data_c = Credit::where('bank_id', $single_bank_id)->where($attr);
            } else {
                $data_c = Credit::where($attr);
            }

            if ($request->input('online_credit')) {
                $data_c = $data_c->where('credit_issuing_form_id', 3);
            }

            if ($request->input('date')) {
                $data_c =  $data_c->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
                $singlePrint = false;
            }
            if ($request->input('amount')) {
                $data_c = $data_c->where('amount', '>=', $request->amount);
            }
            if ($request->input('compensation')) {
                $data_c = $data_c->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
                $singlePrint = false;
            }

            $data_c = $data_c->orderBy('percent', 'ASC')->get();
        }

        if ($single_bank_id > 0) {
            $data = $data_c;
        } else {
            $data = $this->groupByAuto($data_c, 'bank_id', $request);
        }
        //dd($data);
        $compensation = $request->input('compensation');
        $type_bank = TypeBanks::all();
        return view('frontend.credits.ajax-content.auto', compact(
            'singlePrint',
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'compensation',
            'single_bank_id'
        ))->render();
    }

    public function largeAutoFilter(Request $request)
    {
        $single_bank_id = 0;
        $filter_type = $request->type_filter;
        $credit_date = '';
        $arrAttr = [
            'type_credit_id' => $request->credit_type_id,
            'status' => 1
        ];
        $singlePrint = true;
        if (!$request->input('online_credit')) {
            if ($request->input('credit_issuing_form')) {
                $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);
            }
        }

        $tp_banks = $request->type_banks;
        $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

        $tp_b_id  = $bank_type['tp_b_id'];

        $ids = $bank_type['ids'];

        $data_c = Credit::whereIn('bank_id', $ids);
        $data_c = $data_c->where($arrAttr);
        if ($request->input('online_credit')) {
            $data_c = $data_c->where('credit_issuing_form_id', 3);
        }
        if ($request->input('date')) {
            $singlePrint = false;
            $data_c =  $data_c->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
            $credit_date = $request->input('date');
        }
        if ($request->input('goal')) {
            $data_c = $data_c->where('credit_goal_id', 'LIKE', '%' . $request->input('goal') . '%');
        }
        if ($request->input('provision')) {
            $data_c = $data_c->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
        }
        if ($request->input('compensation')) {
            $singlePrint = false;
            $data_c = $data_c->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
        }
        if ($request->input('amount')) {
            $data_c = $data_c->where('amount', '>=', $request->amount);
        }

        $data_c = $data_c->whereIn('type_bank_id', $tp_b_id);
        $data_c = $data_c->get();

        $data = $this->groupByAuto($data_c, 'bank_id', $request);
        $type_bank = TypeBanks::all();
        $compensation = $request->input('compensation');
        return view('frontend.credits.ajax-content.auto', compact(
            'singlePrint',
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'compensation',
            'single_bank_id'
        ))->render();
    }

    public function groupByAuto($array, $key, $request)
    {

        $return = [];
        $max_percent = [];
        $count = [];

        foreach ($array as $val) {
            $b_r = explode(',', $val->bank->region_id);
            if ($request->input('region')) {
                if (in_array($request->input('region'), $b_r)) {
                    $return[$val[$key]][] = $val;
                }
            } else {
                $return[$val[$key]][] = $val;
            }
        }

        $arrAttr = [
            'type_credit_id' => $request->credit_type_id,
            'status' => 1
        ];

        if ($request->input('credit_issuing_form'))
            $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

        $tp_banks = $request->type_banks;

        $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

        $tp_b_id  = $bank_type['tp_b_id'];

        foreach ($return as $k => $v) {
            $cr = Credit::where('bank_id', $k);
            if ($request->input('online_credit')) {
                $cr = $cr->where('credit_issuing_form_id', 3);
            }
            if ($request->input('date')) {
                $cr = $cr->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
            }
            if ($request->input('goal')) {
                $cr = $cr->where('credit_goal_id', 'LIKE', '%' . $request->input('goal') . '%');
            }
            if ($request->input('provision')) {
                $cr = $cr->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
            }
            if ($request->input('compensation')) {
                $cr = $cr->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }
            $cr = $cr->where($arrAttr);
            if ($request->input('amount')) {
                $cr = $cr->where('amount', '>=', $request->amount);
            }
            $cr = $cr->whereIn('type_bank_id', $tp_b_id);
            $cr = $cr->orderBy('percent', 'ASC');
            $cr = $cr->first();
            $max_percent[] = $cr;
            $count[$k] = count($v);
        }

        $data = [
            'max_cr_id' => $max_percent,
            'data'      => $return,
            'count'     => $count
        ];
        return $data;
    }

    public function otherAutoCredit(Request $request)
    {
        $data = [];
        $credit_date = '';
        $singlePrint = true;
        if ($request->type_filter == 'small') {
            if ($request) {
                $attr = [
                    'bank_id' => $request->bank_id,
                    'type_credit_id' => $request->credit_type_id,
                    'status' => 1
                ];

                $data = Credit::where($attr);
                if ($request->input('online_credit')) {
                    $data = $data->where('credit_issuing_form_id', 3);
                }
                if ($request->input('date')) {
                    $singlePrint = false;
                    $data =  $data->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                    $credit_date = $request->input('date');
                }

                if ($request->input('compensation')) {
                    $singlePrint = false;
                    $data =  $data->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
                }
                $data = $data->where('id', '!=', $request->this_id);

                if ($request->input('amount')) {
                    $data = $data->where('amount', '>=', $request->amount);
                }

                $data = $data->orderBy('percent', 'ASC')->get();
            }
        }
        if ($request->type_filter == 'large') {
            $arrAttr = [
                'bank_id' => $request->bank_id,
                'type_credit_id' => $request->credit_type_id,
                'status' => 1
            ];
            if ($request->input('date'))
                $arrAttr = array_merge($arrAttr, ['type_date_filter' => $request->input('date')]);
            if ($request->input('review_period'))
                $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);
            if ($request->input('credit_issuing_form'))
                $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

            $tp_banks = $request->type_banks;

            $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

            $tp_b_id  = $bank_type['tp_b_id'];

            $data = Credit::where($arrAttr);
            if ($request->input('online_credit')) {
                $data = $data->where('credit_issuing_form_id', 3);
            }
            if ($request->input('date')) {
                $data =  $data->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }
            if ($request->input('goal')) {
                $data = $data->where('credit_goal_id', 'LIKE', '%' . $request->input('goal') . '%');
            }
            if ($request->input('provision')) {
                $data = $data->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
            }
            if ($request->input('compensation')) {
                $data =  $data->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }
            $data = $data->where('id', '!=', $request->this_id);
            if ($request->input('amount')) {
                $data = $data->where('amount', '>=', $request->amount);
            }
            $data = $data->whereIn('type_bank_id', $tp_b_id);
            $data = $data->get();
        }
        $compensation = $request->input('compensation');
        $type_bank = TypeBanks::all();
        return view('frontend.credits.other.auto', compact(
            'singlePrint',
            'data',
            'type_bank',
            'credit_date',
            'compensation'
        ))->render();
    }
    /*End Auto Credit filter*/

    /*Education credit filter*/
    public function filterEducation(Request $request)
    {
        $single_bank_id = $request->single_bank_id;
        $filter_type = $request->type_filter;
        $credit_date = '';
        if ($request) {
            $attr = [
                'type_credit_id' => $request->credit_type_id,
                'status' => 1,
                'currency' => $request->input('currency')
            ];
            $date = [];

            $attrs = array_merge($attr, $date);
            $singlePrint = true;
            if ($single_bank_id > 0) {
                $data_c = Credit::where('bank_id', $single_bank_id)->where($attrs);
            } else {
                $data_c = Credit::where($attrs);
            }

            if ($request->input('online_credit')) {
                $data_c = $data_c->where('credit_issuing_form_id', 10);
            }
            if ($request->input('date')) {
                $singlePrint = false;
                $data_c =  $data_c->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }
            if ($request->input('amount')) {
                $data_c = $data_c->where('amount', '>=', $request->amount);
            }

            $data_c = $data_c->orderBy('percent', 'ASC')->get();
        }

        if ($single_bank_id > 0) {
            $data = $data_c;
        } else {
            $data = $this->groupByEducation($data_c, 'bank_id', $request);
        }
        //dd($data);
        $type_bank = TypeBanks::all();
        return view('frontend.credits.ajax-content.education', compact(
            'singlePrint',
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'single_bank_id'
        ))->render();
    }

    public function largeEducationFilter(Request $request)
    {
        $single_bank_id = 0;
        $filter_type = $request->type_filter;
        $credit_date = '';
        $arrAttr = [
            'currency' => $request->input('currency'),
            'status' => 1,
        ];
        $singlePrint = true;
        if ($request->input('review_period'))
            $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);
        if ($request->input('credit_issuing_form'))
            $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);


        $tp_banks = $request->type_banks;

        $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

        $tp_b_id  = $bank_type['tp_b_id'];

        $ids = $bank_type['ids'];

        $data_c = Credit::whereIn('bank_id', $ids);
        if ($request->input('online_credit')) {
            $data_c = $data_c->where('credit_issuing_form_id', 10);
        }
        if ($request->input('date')) {
            $singlePrint = false;
            $data_c =  $data_c->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
            $credit_date = $request->input('date');
        }
        if ($request->input('compensation')) {
            $singlePrint = false;
            $data_c =  $data_c->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
        }
        if ($request->input('goal')) {
            $data_c =  $data_c->where('credit_goal_id', 'LIKE', '%' . $request->input('goal') . '%');
        }
        if ($request->input('provision')) {
            $data_c =  $data_c->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
        }
        $data_c = $data_c->where($arrAttr);
        if ($request->input('amount')) {
            $data_c = $data_c->where('amount', '>=', $request->amount);
        }
        $data_c = $data_c->where('type_credit_id', $request->credit_type_id);
        $data_c = $data_c->whereIn('type_bank_id', $tp_b_id);
        $data_c = $data_c->get();

        $data = $this->groupByEducation($data_c, 'bank_id', $request);
        $type_bank = TypeBanks::all();

        return view('frontend.credits.ajax-content.education', compact(
            'singlePrint',
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'single_bank_id'
        ))->render();
    }

    public function groupByEducation($array, $key, $request)
    {

        $return = [];
        $max_percent = [];
        $count = [];

        foreach ($array as $val) {
            $b_r = explode(',', $val->bank->region_id);
            if ($request->input('region')) {
                if (in_array($request->input('region'), $b_r)) {
                    $return[$val[$key]][] = $val;
                }
            } else {
                $return[$val[$key]][] = $val;
            }
        }

        $arrAttr = [
            'currency' => $request->input('currency'),
            'status' => 1,
        ];

        if ($request->input('review_period'))
            $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);
        if ($request->input('credit_issuing_form'))
            $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

        $tp_banks = $request->type_banks;

        $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

        $tp_b_id  = $bank_type['tp_b_id'];

        foreach ($return as $k => $v) {
            $cr = Credit::where('bank_id', $k);
            if ($request->input('online_credit')) {
                $cr = $cr->where('credit_issuing_form_id', 10);
            }
            if ($request->input('date')) {
                $cr =  $cr->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }
            if ($request->input('compensation')) {
                $cr =  $cr->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }
            if ($request->input('goal')) {
                $cr =  $cr->where('credit_goal_id', 'LIKE', '%' . $request->input('goal') . '%');
            }
            if ($request->input('provision')) {
                $cr =  $cr->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
            }
            $cr = $cr->where($arrAttr);
            if ($request->input('amount')) {
                $cr = $cr->where('amount', '>=', $request->amount);
            }
            $cr = $cr->where('type_credit_id', $request->credit_type_id);
            $cr = $cr->whereIn('type_bank_id', $tp_b_id);
            $cr = $cr->orderBy('percent', 'ASC');
            $cr = $cr->first();
            $max_percent[] = $cr;
            $count[$k] = count($v);
        }

        $data = [
            'max_cr_id' => $max_percent,
            'data'      => $return,
            'count'     => $count
        ];
        return $data;
    }

    public function otherEducationCredit(Request $request)
    {
        $credit_date = '';
        $data = [];
        $singlePrint = true;
        if ($request->type_filter == 'small') {
            if ($request) {
                $attr = [
                    'type_credit_id' => $request->credit_type_id,
                    'status' => 1,
                    'currency' => $request->input('currency'),
                    'bank_id' => $request->bank_id
                ];
                $date = [];
                if ($request->input('date')) {
                    $singlePrint = false;
                    $date = ['type_date_filter' => $request->input('date')];
                }
                $attrs = array_merge($attr, $date);

                $data = Credit::where($attrs);
                if ($request->input('online_credit')) {
                    $data = $data->where('credit_issuing_form_id', 10);
                }
                if ($request->input('date')) {
                    $data =  $data->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                    $credit_date = $request->input('date');
                }
                $data = $data->where('id', '!=', $request->this_id);
                if ($request->input('amount')) {
                    $data = $data->where('amount', '>=', $request->amount);
                }
                $data = $data->orderBy('percent', 'ASC')->get();
            }
        }
        if ($request->type_filter == 'large') {
            $arrAttr = [
                'bank_id' => $request->bank_id,
                'currency' => $request->input('currency'),
            ];

            if ($request->input('review_period'))
                $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);
            if ($request->input('credit_issuing_form'))
                $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

            $tp_banks = $request->type_banks;

            $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

            $tp_b_id  = $bank_type['tp_b_id'];

            $data = Credit::where($arrAttr);
            if ($request->input('online_credit')) {
                $data = $data->where('credit_issuing_form_id', 10);
            }
            if ($request->input('date')) {
                $singlePrint = false;
                $data =  $data->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }
            if ($request->input('compensation')) {
                $singlePrint = false;
                $data =  $data->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }
            if ($request->input('goal')) {
                $data =  $data->where('credit_goal_id', 'LIKE', '%' . $request->input('goal') . '%');
            }
            if ($request->input('provision')) {
                $data =  $data->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
            }
            $data = $data->where('id', '!=', $request->this_id);
            if ($request->input('amount')) {
                $data = $data->where('amount', '>=', $request->amount);
            }
            $data = $data->where('type_credit_id', $request->credit_type_id);
            $data = $data->whereIn('type_bank_id', $tp_b_id);
            $data = $data->get();
        }

        $type_bank = TypeBanks::all();

        return view('frontend.credits.other.education', compact(
            'singlePrint',
            'data',
            'type_bank',
            'credit_date'
        ))->render();
    }
    /*End Education credit filter*/

    /*Micro credit filter*/
    public function filterMicro(Request $request)
    {
        $single_bank_id = $request->single_bank_id;
        $credit_date = '';
        $filter_type = $request->type_filter;
        if ($request) {
            $attr = [
                'type_credit_id' => $request->credit_type_id,
                'status' => 1
            ];
            $singlePrint = true;
            /* if($request->input('compensation'))
                $attr = array_merge($attr,['compensation_status' => $request->input('compensation')]);*/

            if ($single_bank_id > 0) {
                $data_c = Credit::where('bank_id', $single_bank_id)->where($attr);
            } else {
                $data_c = Credit::where($attr);
            }
            if ($request->input('online_credit')) {
                $data_c = $data_c->where('credit_issuing_form_id', 'LIKE', '%' . '8' . '%');
            }
            if ($request->input('date')) {
                $singlePrint = false;
                $data_c =  $data_c->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }
            if ($request->amount) {
                $data_c = $data_c->where('amount', '>=', $request->amount);
            }
            $data_c = $data_c->orderBy('percent', 'ASC')->get();
        }

        if ($single_bank_id > 0) {
            $data = $data_c;
        } else {
            $data = $this->groupByMicro($data_c, 'bank_id', $request);
        }

        $type_bank = TypeBanks::all();
        return view('frontend.credits.ajax-content.micro', compact(
            'singlePrint',
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'single_bank_id'
        ))->render();
    }

    public function largeMicroFilter(Request $request)
    {
        $single_bank_id = 0;
        $filter_type = $request->type_filter;
        $credit_date = '';
        $arrAttr = [
            'status' => 1,
            'type_credit_id' => $request->credit_type_id
        ];
        $singlePrint = true;
        /*if($request->input('compensation'))
            $arrAttr = array_merge($arrAttr,['compensation_status' => $request->input('compensation')]);*/
        if ($request->input('review_period'))
            $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);
        if ($request->input('credit_issuing_form'))
            $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

        $tp_banks = $request->type_banks;

        $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

        $tp_b_id  = $bank_type['tp_b_id'];

        $ids = $bank_type['ids'];

        $data_c = Credit::whereIn('bank_id', $ids);
        if ($request->input('online_credit')) {
            $data_c = $data_c->where('credit_issuing_form_id', 'LIKE', '%' . '8' . '%');
        }
        if ($request->input('date')) {
            $singlePrint = false;
            $data_c =  $data_c->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
            $credit_date = $request->input('date');
        }
        if ($request->input('provision')) {
            $data_c =  $data_c->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
        }
        $data_c = $data_c->where($arrAttr);
        if ($request->amount) {
            $data_c = $data_c->where('amount', '>=', $request->amount);
        }
        $data_c = $data_c->whereIn('type_bank_id', $tp_b_id);
        $data_c = $data_c->get();
        $data = $this->groupByMicro($data_c, 'bank_id', $request);
        $type_bank = TypeBanks::all();
        //dd($data);
        return view('frontend.credits.ajax-content.micro', compact(
            'singlePrint',
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'single_bank_id'
        ))->render();
    }

    public function groupByMicro($array, $key, $request)
    {

        $return = [];
        $max_percent = [];
        $count = [];

        foreach ($array as $val) {
            $b_r = explode(',', $val->bank->region_id);
            if ($request->input('region')) {
                if (in_array($request->input('region'), $b_r)) {
                    $return[$val[$key]][] = $val;
                }
            } else {
                $return[$val[$key]][] = $val;
            }
        }

        $arrAttr = [
            'status' => 1,
            'type_credit_id' => $request->credit_type_id
        ];

        /*if($request->input('compensation'))
            $arrAttr = array_merge($arrAttr,['compensation_status' => $request->input('compensation')]);*/
        if ($request->input('review_period'))
            $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);
        if ($request->input('credit_issuing_form'))
            $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

        $tp_banks = $request->type_banks;

        $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

        $tp_b_id  = $bank_type['tp_b_id'];

        foreach ($return as $k => $v) {

            $cr = Credit::where('bank_id', $k);
            if ($request->input('online_credit')) {
                $cr = $cr->where('credit_issuing_form_id', 'LIKE', '%' . '8' . '%');
            }
            if ($request->input('date')) {
                $cr =  $cr->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }

            if ($request->input('provision')) {
                $cr =  $cr->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
            }
            $cr = $cr->where($arrAttr);
            if ($request->amount) {
                $cr = $cr->where('amount', '>=', $request->amount);
            }

            $cr = $cr->whereIn('type_bank_id', $tp_b_id);
            $cr = $cr->orderBy('percent', 'ASC');
            $cr = $cr->first();
            $max_percent[] = $cr;
            $count[$k] = count($v);
        }
        // return dd($max_percent);
        $data = [
            'max_cr_id' => $max_percent,
            'data'      => $return,
            'count'     => $count
        ];
        return $data;
    }

    public function otherMicroCredit(Request $request)
    {
        $credit_date = '';
        $data = [];
        $singlePrint = true;
        if ($request->type_filter == 'small') {
            $attr = [
                'type_credit_id' => $request->credit_type_id,
                'status' => 1,
                'bank_id' => $request->bank_id
            ];


            $data = Credit::where($attr)->where('id', '!=', $request->this_id);
            if ($request->input('online_credit')) {
                $data = $data->where('credit_issuing_form_id', 8);
            }
            if ($request->input('date')) {
                $singlePrint = false;
                $data =  $data->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }
            if ($request->amount) {
                $data = $data->where('amount', '>=', $request->amount);
            }
            if ($request->input('compensation')) {
                $singlePrint = false;
                $data =  $data->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }
            $data = $data->orderBy('percent', 'ASC')->get();
        }
        if ($request->type_filter == 'large') {
            $arrAttr = [
                'type_credit_id' => $request->credit_type_id,
                'status' => 1,
                'bank_id' => $request->bank_id
            ];

            if ($request->input('review_period'))
                $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);
            if ($request->input('credit_issuing_form'))
                $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

            $tp_banks = $request->type_banks;

            $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

            $tp_b_id  = $bank_type['tp_b_id'];

            $data = Credit::where($arrAttr)->where('id', '!=', $request->this_id);
            if ($request->input('online_credit')) {
                $data = $data->where('credit_issuing_form_id', 8);
            }
            if ($request->input('date')) {
                $data =  $data->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }
            if ($request->input('provision')) {
                $data =  $data->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
            }
            if ($request->amount) {
                $data = $data->where('amount', '>=', $request->amount);
            }
            if ($request->input('compensation')) {
                $data =  $data->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }
            $data = $data->whereIn('type_bank_id', $tp_b_id);
            $data = $data->get();
        }
        $type_bank = TypeBanks::all();
        return view('frontend.credits.other.micro', compact(
            'singlePrint',
            'data',
            'type_bank',
            'credit_date'
        ))->render();
    }
    /*End Micro credit filter*/

    /*Mortgage credit filter*/
    public function filterMortgage(Request $request)
    {
        $single_bank_id = $request->single_bank_id;
        $filter_type = $request->type_filter;
        $singlePrint = true;
        $credit_date = '';
        if ($request) {
            $attrs = [
                'type_credit_id' => $request->credit_type_id,
                'status' => 1,
            ];

            if ($single_bank_id > 0) {
                $data_c = Credit::where('bank_id', $single_bank_id)->where($attrs);
            } else {
                $data_c = Credit::where($attrs);
            }

            if ($request->input('online_credit')) {
                $data_c = $data_c->where('credit_issuing_form_id', 6);
            }
            if ($request->input('date')) {
                $singlePrint = false;
                $data_c =  $data_c->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }
            if ($request->amount) {
                $data_c = $data_c->where('amount', '>=', $request->amount);
            }
            if ($request->input('compensation')) {
                $singlePrint = false;
                $data_c =  $data_c->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }
            $data_c = $data_c->orderBy('percent', 'ASC')->get();
        }

        if ($single_bank_id > 0) {
            $data = $data_c;
        } else {
            $data = $this->groupByMortgage($data_c, 'bank_id', $request);
        }
        $compensation = $request->input('compensation');
        $type_bank = TypeBanks::all();
        return view('frontend.credits.ajax-content.mortgage', compact(
            'singlePrint',
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'compensation',
            'single_bank_id'
        ))->render();
    }

    public function largeMortgageFilter(Request $request)
    {
        $single_bank_id = 0;
        $singlePrint = true;
        $filter_type = $request->type_filter;
        $credit_date = '';
        $arrAttr = [
            'status' => 1,
            'type_credit_id' => $request->credit_type_id
        ];
        if ($request->input('currency'))
            $arrAttr = array_merge($arrAttr, ['currency' => $request->input('currency'),]);

        if ($request->input('review_period'))
            $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);
        if ($request->input('credit_issuing_form'))
            $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);


        $tp_banks = $request->type_banks;

        $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

        $tp_b_id  = $bank_type['tp_b_id'];

        $ids = $bank_type['ids'];

        $data_c = Credit::whereIn('bank_id', $ids);

        if ($request->input('online_credit')) {
            $data_c = $data_c->where('credit_issuing_form_id', 6);
        }
        if ($request->input('date')) {
            $singlePrint = false;
            $data_c =  $data_c->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
            $credit_date = $request->input('date');
        }
        if ($request->input('compensation')) {
            $data_c =  $data_c->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
        }
        if ($request->input('goal')) {

            $data_c =  $data_c->where('credit_goal_id', 'LIKE', '%' . $request->input('goal') . '%');
        }
        if ($request->input('provision')) {
            $data_c =  $data_c->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
        }

        $data_c = $data_c->where($arrAttr);
        if ($request->amount) {
            $data_c = $data_c->where('amount', '>=', $request->amount);
        }
        $data_c = $data_c->whereIn('type_bank_id', $tp_b_id);
        $data_c = $data_c->get();

        $data = $this->groupByMortgage($data_c, 'bank_id', $request);
        $type_bank = TypeBanks::all();
        $compensation = $request->input('compensation');
        return view('frontend.credits.ajax-content.mortgage', compact(
            'singlePrint',
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'compensation',
            'single_bank_id'
        ))->render();
    }

    public function groupByMortgage($array, $key, $request)
    {

        $return = [];
        $max_percent = [];
        $count = [];

        foreach ($array as $val) {
            $b_r = explode(',', $val->bank->region_id);
            if ($request->input('region')) {
                if (in_array($request->input('region'), $b_r)) {
                    $return[$val[$key]][] = $val;
                }
            } else {
                $return[$val[$key]][] = $val;
            }
        }

        $arrAttr = [
            'status' => 1,
            'type_credit_id' => $request->credit_type_id
        ];

        if ($request->input('currency'))
            $arrAttr = array_merge($arrAttr, ['currency' => $request->input('currency'),]);
        if ($request->input('review_period'))
            $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);
        if ($request->input('credit_issuing_form'))
            $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

        $tp_banks = $request->type_banks;

        $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

        $tp_b_id  = $bank_type['tp_b_id'];

        foreach ($return as $k => $v) {
            $cr = Credit::where('bank_id', $k);
            if ($request->input('online_credit')) {
                $cr = $cr->where('credit_issuing_form_id', 8);
            }
            if ($request->input('date')) {
                $cr =  $cr->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }
            if ($request->input('goal')) {
                $cr = $cr->where('credit_goal_id', 'LIKE', '%' . $request->input('goal') . '%');
            }
            if ($request->input('provision')) {
                $cr = $cr->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
            }
            $cr = $cr->where($arrAttr);
            if ($request->amount) {
                $cr = $cr->where('amount', '>=', $request->amount);
            }
            if ($request->input('compensation')) {
                $cr =  $cr->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }
            $cr = $cr->whereIn('type_bank_id', $tp_b_id);
            $cr = $cr->orderBy('percent', 'ASC');
            $cr = $cr->first();
            $max_percent[] = $cr;
            $count[$k] = count($v);
        }

        $data = [
            'max_cr_id' => $max_percent,
            'data'      => $return,
            'count'     => $count
        ];
        return $data;
    }

    public function otherMortgageCredit(Request $request)
    {
        $singlePrint = true;
        $credit_date = '';
        $data = [];
        if ($request->type_filter == 'small') {
            $attrs = [
                'type_credit_id' => $request->credit_type_id,
                'status' => 1,
                'bank_id' => $request->bank_id
            ];

            $data = Credit::where($attrs);
            if ($request->input('online_credit')) {
                $data = $data->where('credit_issuing_form_id', 6);
            }
            if ($request->input('date')) {
                $singlePrint = false;
                $data =  $data->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }
            if ($request->input('compensation')) {
                $singlePrint = false;
                $data =  $data->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }
            $data = $data->where('id', '!=', $request->this_id);
            if ($request->amount) {
                $data = $data->where('amount', '>=', $request->amount);
            }
            $data = $data->orderBy('percent', 'ASC')->get();
        }
        if ($request->type__filter == 'large') {
            $arrAttr = [
                'status' => 1,
                'type_credit_id' => $request->credit_type_id,
                'bank_id' => $request->bank_id
            ];
            if ($request->input('currency'))
                $arrAttr = array_merge($arrAttr, ['currency' => $request->input('currency'),]);
            if ($request->input('review_period'))
                $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);
            if ($request->input('credit_issuing_form'))
                $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

            $tp_banks = $request->type_banks;

            $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

            $tp_b_id  = $bank_type['tp_b_id'];

            $data = Credit::where($arrAttr)->where('id', '!=', $request->this_id);
            if ($request->input('online_credit')) {
                $data = $data->where('credit_issuing_form_id', 6);
            }
            if ($request->input('date')) {
                $singlePrint = false;
                $data =  $data->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }
            if ($request->input('goal')) {
                $data = $data->where('credit_goal_id', 'LIKE', '%' . $request->input('goal') . '%');
            }
            if ($request->input('provision')) {
                $data = $data->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
            }
            if ($request->amount) {
                $data = $data->where('amount', '>=', $request->amount);
            }
            if ($request->input('compensation')) {
                $singlePrint = false;
                $data =  $data->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }
            $data = $data->whereIn('type_bank_id', $tp_b_id);
            $data = $data->get();
        }
        $type_bank = TypeBanks::all();
        $compensation = $request->input('compensation');
        return view('frontend.credits.other.mortgage', compact(
            'singlePrint',
            'data',
            'type_bank',
            'credit_date',
            'compensation'
        ))->render();
    }
    /*End Mortgage credit filter*/

    public function filterOverdraft(Request $request)
    {
        $single_bank_id = $request->single_bank_id;
        $filter_type = $request->type_filter;
        $credit_date = '';
        $attr = [
            'type_credit_id' => $request->credit_type_id,
            'status' => 1
        ];

        if ($single_bank_id > 0) {
            $data_c = Credit::where('bank_id', $single_bank_id)->where($attr);
        } else {
            $data_c = Credit::where($attr);
        }

        if ($request->input('amount')) {
            $data_c = $data_c->where('amount', '>=', $request->amount);
        }

        /* if($request->input('date')){
            $data_c =  $data_c->where('type_date_filter','LIKE','%'.$request->input('date').'%');
            $credit_date = $request->input('date');
        }*/

        if ($request->input('provision')) {
            $data_c =  $data_c->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
        }

        if ($request->input('online_credit')) {
            $data_c = $data_c->where('credit_issuing_form_id', 1);
        }

        $data_c = $data_c->orderBy('percent', 'ASC');

        $data_c  = $data_c->get();
        if ($single_bank_id > 0) {
            $data = $data_c;
        } else {
            $data = $this->groupByOverdraft($data_c, 'bank_id', $request);
        }

        $type_bank = TypeBanks::all();
        return view('frontend.credits.ajax-content.overdraft', compact(
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'single_bank_id'
        ))->render();
    }

    public function largeOverdraftFilter(Request $request)
    {
        $single_bank_id = 0;
        $filter_type = $request->type_filter;
        $credit_date = '';
        $arrAttr = [
            'status' => 1,
            'type_credit_id' => $request->credit_type_id
        ];

        /*if($request->input('currency'))
            $arrAttr = array_merge($arrAttr,['currency' => $request->input('currency')]);*/

        if ($request->input('goal'))
            $arrAttr = array_merge($arrAttr, ['credit_goal_id' => $request->input('goal')]);

        if ($request->input('review_period'))
            $arrAttr = array_merge($arrAttr, ['credit_review_period_id' => $request->input('review_period')]);

        if ($request->input('credit_issuing_form'))
            $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

        $tp_banks = $request->type_banks;

        /*1 davlat bank, 2 xususiy bank, 3 xorijiy bank*/
        $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

        $tp_b_id  = $bank_type['tp_b_id'];

        $ids = $bank_type['ids'];

        $data_c = Credit::whereIn('bank_id', $ids);

        $data_c = $data_c->where($arrAttr);

        if ($request->input('amount')) {
            $data_c = $data_c->where('amount', '>=', $request->amount);
        }

        /* if($request->input('date')){
            $data_c =  $data_c->where('type_date_filter','LIKE','%'.$request->input('date').'%');
            $credit_date = $request->input('date');
        }*/

        if ($request->input('online_credit')) {
            $data_c = $data_c->where('credit_issuing_form_id', 1);
        }
        if ($request->input('compensation')) {
            $data_c = $data_c->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
        }
        $data_c = $data_c->whereIn('type_bank_id', $tp_b_id);

        if ($request->input('provision')) {
            $data_c = $data_c->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
        }

        $data_c = $data_c->get();

        $data = $this->groupByOverdraft($data_c, 'bank_id', $request);
        $type_bank = TypeBanks::all();

        return view('frontend.credits.ajax-content.overdraft', compact(
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'single_bank_id'
        ))->render();
    }

    public function groupByOverdraft($array, $key, $request)
    {

        $return = [];
        $max_percent = [];
        $count = [];

        foreach ($array as $val) {
            $b_r = explode(',', $val->bank->region_id);
            if ($request->input('region')) {
                if (in_array($request->input('region'), $b_r)) {
                    $return[$val[$key]][] = $val;
                }
            } else {
                $return[$val[$key]][] = $val;
            }
        }

        $arrAttr = [
            'status' => 1,
            'type_credit_id' => $request->credit_type_id,
        ];

        /*if($request->type_filter == 'large'){
            if($request->input('currency')){
                $arrAttr = array_merge($arrAttr,['currency' => $request->input('currency')]);
            }
        }*/

        if ($request->input('goal')) {
            $arrAttr = array_merge($arrAttr, ['credit_goal_id' => $request->input('goal')]);
        }

        /* if($request->input('review_period'))
            $arrAttr = array_merge($arrAttr,['credit_review_period_id' => $request->input('review_period')]);*/

        if ($request->input('credit_issuing_form')) {
            $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);
        }

        $tp_banks = $request->type_banks;

        $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

        $tp_b_id  = $bank_type['tp_b_id'];

        foreach ($return as $k => $v) {
            $cr = Credit::where('bank_id', $k);
            $cr = $cr->where($arrAttr);

            if ($request->input('amount')) {
                $cr = $cr->where('amount', '>=', $request->amount);
            }

            $cr = $cr->whereIn('type_bank_id', $tp_b_id);

            /* if($request->input('date')){
                $cr =  $cr->where('type_date_filter','LIKE','%'.$request->input('date').'%');
            }*/

            /*if($request->input('compensation')){
                $cr =  $cr->where('compensation_status','LIKE','%'.$request->input('compensation').'%');
            }*/

            if ($request->input('online_credit')) {
                $cr = $cr->where('credit_issuing_form_id', 1);
            }

            if ($request->input('provision')) {
                $cr = $cr->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
            }

            $cr = $cr->orderBy('percent', 'ASC');

            $cr = $cr->first();

            $max_percent[] = $cr;
            $count[$k] = count($v);
        }

        $data = [
            'max_cr_id' => $max_percent,
            'data'      => $return,
            'count'     => $count
        ];
        return $data;
    }

    public function otherOverdraftCredit(Request $request)
    {
        $data = [];
        $credit_date = '';
        if ($request->type_filter == 'small') {
            if ($request) {
                $attr = [
                    'bank_id' => $request->bank_id,
                    'type_credit_id' => $request->credit_type_id,
                    'status' => 1
                ];

                $data = Credit::where($attr);

                if ($request->input('date')) {
                    $data =  $data->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                    $credit_date = $request->input('date');
                }

                if ($request->input('online_credit')) {
                    $data = $data->where('credit_issuing_form_id', 1);
                }

                $data = $data->where('id', '!=', $request->this_id);

                if ($request->input('amount')) {
                    $data = $data->where('amount', '>=', $request->amount);
                }

                if ($request->input('provision')) {
                    $data =  $data->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
                }

                $data = $data->orderBy('percent', 'ASC')->get();
            }
        }
        if ($request->type_filter == 'large') {
            $arrAttr = [
                'status' => 1,
                'type_credit_id' => $request->credit_type_id,
                'bank_id' => $request->bank_id
            ];

            if ($request->input('currency'))
                $arrAttr = array_merge($arrAttr, ['currency' => $request->input('currency')]);

            if ($request->input('goal'))
                $arrAttr = array_merge($arrAttr, ['credit_goal_id' => $request->input('goal')]);

            if ($request->input('credit_issuing_form'))
                $arrAttr = array_merge($arrAttr, ['credit_issuing_form_id' => $request->input('credit_issuing_form')]);

            $tp_banks = $request->type_banks;

            $bank_type = self::filterBankType($request->input('type_banks_btn'), $tp_banks);

            $tp_b_id  = $bank_type['tp_b_id'];

            $data = Credit::where($arrAttr);

            $data =  $data->where('id', '!=', $request->this_id);

            if ($request->input('amount')) {
                $data = $data->where('amount', '>=', $request->amount);
            }

            if ($request->input('online_credit')) {
                $data = $data->where('credit_issuing_form_id', $request->input('online_credit'));
            }

            if ($request->input('compensation')) {
                $data = $data->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
            }

            if ($request->input('date')) {
                $data =  $data->where('type_date_filter', 'LIKE', '%' . $request->input('date') . '%');
                $credit_date = $request->input('date');
            }

            $data =  $data->whereIn('type_bank_id', $tp_b_id);

            if ($request->input('provision')) {
                $data =  $data->where('provision_id', 'LIKE', '%' . $request->input('provision') . '%');
            }

            $data =  $data->get();
        }
        $type_bank = TypeBanks::all();
        return view('frontend.credits.other.overdraft', compact(
            'data',
            'type_bank',
            'credit_date'
        ))->render();
    }

    protected function filterBankType($type_banks_btn = false, $tp_banks = false)
    {

        /*1 davlat bank, 2 xususiy bank, 3 xorijiy bank*/
        $tp_b_id = [];
        $ids = range(1, 100);
        if (!$type_banks_btn) {
            if ($tp_banks) {
                if (!is_numeric($tp_banks[0])) {

                    if (in_array('state_bank', $tp_banks)) {
                        array_push($tp_b_id, 1);
                        $tp_banks = array_diff($tp_banks, ['state_bank']);
                    }
                    if (in_array('private_bank', $tp_banks)) {
                        array_push($tp_b_id, 2);
                        $tp_banks = array_diff($tp_banks, ['private_bank']);
                    }
                    if (in_array('foreign_bank', $tp_banks)) {
                        array_push($tp_b_id, 3);
                        $tp_banks = array_diff($tp_banks, ['foreign_bank']);
                    }
                } else {
                    $tp_b_id = [1, 2, 3, 4, 5, 6];
                }
                if (count($tp_banks) > 0) {
                    $ids = $tp_banks;
                }
            } else {
                $tp_b_id = [1, 2, 3, 4, 5, 6];
            }
        } else {
            if ($type_banks_btn == 'all_banks') {
                $tp_b_id = [1, 2, 3, 4, 5];
            } else {
                if ($type_banks_btn == 'state_bank') {
                    $tp_b_id = [1];
                }
                if ($type_banks_btn == 'private_bank') {
                    $tp_b_id = [2];
                }
                if ($type_banks_btn == 'foreign_bank') {
                    $tp_b_id = [3];
                }
            }
        }
        $data = [
            'ids'     => $ids,
            'tp_b_id' => $tp_b_id,
        ];
        return $data;
    }

    public function viewCredit($lang = false, $slug)
    {

        $meta_tags = PagesMetaTags::where('page', config('global.pages.credit'))->where('parent_id', 0)->get();
        $credit = Credit::where('slug', $slug)->first();

        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit->type_credit_id])->get();

        $list = Credit::where('status', 0)->where('bank_id', $credit->bank_id)->limit(5)->orderBY('created_at', 'desc')->get();

        $type = __('lang.' . config('global.credits.type_id.' . $credit->type_credit_id . '') . '');
        //dd($type);

        return view('frontend.credits.credit', compact('credit', 'list', 'dates', 'type', 'meta_tags'));
    }

    public function typeCredit()
    {
        $meta_tags = PagesMetaTags::where('page', config('global.pages.credit'))->where('parent_id', 0)->get();
        return view('frontend.credits.type-credits', compact('meta_tags'));
    }

    public function calcCreditPercent(Request $request)
    {

        $data = ['error' => false, 'data' => false];
        $validator =  Validator::make($request->all(), [
            'percent' => 'required|numeric',
            'quantum' => 'required|numeric',
            'date'    => 'required'
        ]);
        if ($validator->fails()) {
            $data = [
                'error' => $validator->errors()
            ];
        } else {

            $inMonth = strstr($request->input('date'), 'oy');
            $inYear = strstr($request->input('date'), 'yil');
            $all_percent = 0;
            $all_summa = 0;
            $pay_first_month = 0;

            if ($inMonth) {
                $date = str_replace('oy', '', $request->input('date'));
                //$percent_pr_month = $request->percent / 12 / 10;
                $month = (float)$date;
                //$sum_percent_month =  ($percent_pr_month * (float)$request->input('quantum'));
                $summa = $request->input('quantum');
                for ($i = 1; $i <= $month; $i++) {
                    if ($i <= $month) {
                        $data['data'][$i] = [
                            'per_month' => (float)$request->input('quantum') / $month
                        ];

                        $i == 1 ? $residue = $summa : $residue = $summa - ((float)$request->input('quantum') / $month);

                        $data['data'][$i] = array_merge($data['data'][$i], ['residue' => $residue]);

                        $pay_percent = $residue * $request->percent / 12 / 100;

                        $all_summa += $pay_percent + ((float)$request->input('quantum') / $month);

                        $all_percent += $pay_percent;

                        if ($i === 1) {
                            $pay_first_month = $pay_percent + ((float)$request->input('quantum') / $month);
                        }
                        $summa = $residue;
                    }
                }

                $data = array_merge($data, [
                    'month_sum' => number_format(ceil($all_percent)),
                    'all_sum' => number_format(ceil($all_summa)),
                    'currency'  =>  'UZS',
                    'percent'   => $request->percent,
                    'pay_first_month'   => number_format(ceil($pay_first_month))
                ]);
            }

            if ($inYear) {
                $date = str_replace('yil', '', $request->input('date'));
                $month = (float)$date * 12;

                $summa = $request->input('quantum');

                for ($i = 1; $i <= $month; $i++) {
                    if ($i <= $month) {
                        $data['data'][$i] = [
                            'per_month' => (float)$request->input('quantum') / $month,
                        ];

                        $i == 1 ? $residue = $summa : $residue = $summa - (float)$request->input('quantum') / $month;

                        $data['data'][$i] = array_merge($data['data'][$i], ['residue' => $residue]);

                        $pay_percent = $residue * $request->percent / 12 / 100;

                        $all_summa += $pay_percent + ((float)$request->input('quantum') / $month);
                        $all_percent += $pay_percent;

                        if ($i === 1) {
                            $pay_first_month = $pay_percent + ((float)$request->input('quantum') / $month);
                        }

                        $summa = $residue;
                    }
                }

                $data = array_merge($data, [
                    'month_sum' => number_format(ceil($all_percent)),
                    'all_sum' => number_format(ceil($all_summa)),
                    'currency'  =>  'UZS',
                    'percent'   => $request->percent,
                    'pay_first_month'   => number_format(ceil($pay_first_month))
                ]);
            }
        }
        return response()->json($data);
    }

    public function payGraph(Request $request)
    {
        $inMonth = strstr($request->input('date'), 'oy');
        $inYear = strstr($request->input('date'), 'yil');
        $data = ['error' => false, 'data' => false];

        $validator =  Validator::make($request->all(), [
            'percent' => 'required|numeric',
            'quantum' => 'required|numeric',
            'date'    => 'required'
        ]);
        if ($validator->fails()) {
            $data = [
                'error' => $validator->errors()
            ];

            return response()->json($data);
        } else {
            if ($request->input('type_calc') == 'simple') {
                if ($inMonth) {
                    $date = str_replace('oy', '', $request->input('date'));
                    //$percent_pr_month = $request->percent / 12 / 10;
                    $month = (float)$date;
                    //$sum_percent_month =  ($percent_pr_month * (float)$request->input('quantum'));
                    $summa = $request->input('quantum');
                    for ($i = 1; $i <= $month; $i++) {
                        if ($i <= $month) {
                            $data['data'][$i] = [
                                'per_month' => (float)$request->input('quantum') / $month
                            ];

                            $i == 1 ? $residue = $summa : $residue = $summa - ((float)$request->input('quantum') / $month);

                            $data['data'][$i] = array_merge($data['data'][$i], ['residue' => $residue]);

                            $pay_percent = $residue * $request->percent / 12 / 100;

                            $data['data'][$i] = array_merge($data['data'][$i], ['pay_percent' => $pay_percent]);

                            $data['data'][$i] = array_merge($data['data'][$i], ['pay_all' => $pay_percent + ((float)$request->input('quantum') / $month)]);

                            $summa = $residue;
                        }
                    }
                }
                if ($inYear) {
                    $date = str_replace('yil', '', $request->input('date'));
                    $month = (float)$date * 12;

                    $sum_percent_year = ($request->percent * (float)$request->input('quantum')) / 100;
                    $all_percent_sum = (float)$date * $sum_percent_year;
                    $all_sum = $all_percent_sum + (float)$request->input('quantum');

                    $per_month_sum = $all_sum / $month;
                    $summa = $request->input('quantum');
                    for ($i = 1; $i <= $month; $i++) {
                        if ($i <= $month) {
                            $data['data'][$i] = [
                                'per_month' => (float)$request->input('quantum') / $month,
                            ];

                            $i == 1 ? $residue = $summa : $residue = $summa - (float)$request->input('quantum') / $month;

                            $data['data'][$i] = array_merge($data['data'][$i], ['residue' => $residue]);

                            $pay_percent = $residue * $request->percent / 12 / 100;

                            $data['data'][$i] = array_merge($data['data'][$i], ['pay_percent' => $pay_percent]);

                            $data['data'][$i] = array_merge($data['data'][$i], ['pay_all' => $pay_percent + ((float)$request->input('quantum') / $month)]);

                            $summa = $residue;
                        }
                    }
                }
            }

            if ($request->input('type_calc') == 'anuitet') {
                $data = [];
            }

            return view('frontend.credits.credit-graph', compact('data'))->render();
        }
    }

    // new page

    public function creditNewPage()
    {
        //my custom
        $car_brands = CarBrand::orderBy("name_uz", "ASC")->get();
        $cars = Car::all();
        //
        $single_bank_id = 0;
        $meta_tags = PagesMetaTags::where('page', config('global.pages.auto_credits'))->where('parent_id', 0)->get();
        $update_time = DataUpdateTime::where('service_id', CREDIT_ID)->first();
        $name = 'name_' . App::getLocale();
        $credit_type = CreditType::where('slug', config('global.credits.auto'))->first();
        $credit_type_id = $credit_type->id;
        $cities = Region::whereNull('parent_id')->orderBy($name)->get();
        $currency = Currency::whereIn('name', ['UZS', 'USD', 'EUR'])->get();
        $dates = DateService::where(['service_id' => CREDIT_ID, 'child_service_id' => $credit_type->id, 'status' => 1])->get();

        $goals = CreditGoal::where('service_id', $credit_type->id)->get();

        $issuing_forms = CreditIssuingForm::where('service_id', $credit_type->id)->get();
        $review_period = CreditReviewPeriod::where('service_id', $credit_type->id)->get();
        $banks = Bank::orderBy('name_uz')->get();
        $provisions = CreditProvision::where('service_id', $credit_type->id)->get();

        $bosh_badal = CreditBoshBadal::where(['service_id' => $credit_type->id, 'status' => 1])->orderBy('name_uz', 'ASC')->get();
        // dd(CREDIT_ID);
        return view('frontend.credits.newPage', compact(
            'car_brands',
            'cars',
            'cities',
            'currency',
            'credit_type_id',
            'dates',
            'goals',
            'issuing_forms',
            'review_period',
            'banks',
            'provisions',
            'update_time',
            'bosh_badal',
            'meta_tags',
            'single_bank_id'
        ));
    }
    public function filterNewAuto(Request $request){
        $border_case = 0;
        $single_bank_id = $request->single_bank_id;
        $filter_type = $request->type_filter;
        $credit_date = '';
        if ($request) {
            $attr = [
                'type_credit_id' => $request->credit_type_id,
                'status' => 1
            ];
            $singlePrint = true;

            if ($single_bank_id > 0) {
                $data_c = Credit::where('bank_id', $single_bank_id)->where($attr);
            } else {
                $data_c = Credit::where($attr);
            }

            if ($request->input('online_credit')) {
                $data_c = $data_c->where('credit_issuing_form_id', 3);
            }
            $dateEntered = false;
            $compEntered = false;
            if($request->input('date')){
                $data_c =  $data_c->where('type_date_filter','LIKE','%'.$request->input('date').'%');
                $credit_date = $request->input('date');
                $singlePrint = false;
                $dateEntered = true;
            }
            $amount = 0;
            if ($request->input('car_brand') && $request->input('car')) {
                $amount = Car::findOrFail($request->input('car'))->price;
                $data_c = $data_c->where(function ($query) use ($amount) {
                    $query->where('amount', '>=', $amount)->orWhere('amount', '=', 0);
                });
                $amount = (int)$amount;
            }
            if ($request->input('compensation')) {
                $data_c = $data_c->where('compensation_status', 'LIKE', '%' . $request->input('compensation') . '%');
                $singlePrint = false;
                $compEntered = true;;
            }
            if($compEntered && !$dateEntered) $border_case = 1;//only compensation entered
            if($dateEntered && !$compEntered) $border_case = 2;//only date entered
            $data_c = $data_c->orderBy('percent','ASC')->get();
        }

        if ($single_bank_id > 0) {
            $data = $data_c;
        } else {
            $data = $this->groupByAuto($data_c, 'bank_id', $request);
        }
        //dd($data);
        $compensation = $request->input('compensation');
        $type_bank = TypeBanks::all();
        return view('frontend.credits.ajax-content.newauto',compact(
            'border_case',
            'amount',
            'singlePrint',
            'data',
            'filter_type',
            'type_bank',
            'credit_date',
            'compensation',
            'single_bank_id'
        ))->render();
    }

    public function selectCar(Request $request)
    {
        if ($request->id) {
            $model = Car::where('car_brand_id', '=', $request->id)->orderBy('name_uz')->get();
            $option = '';
            foreach ($model as $row) {
                if (is_null($row->image)) {
                    $image = 'defaultcar.webp';
                } else {
                    $image = $row->image;
                }
                $option .= '<option value="' . $row->id . '" data-image="' . $image . '">' . $row->name() . '</option>';
            }
            $options = '<option value"" data-image="' . $image . '">' . __('lang.choose') . '</option>' . $option;
            return response($options);
        }
    }
}
