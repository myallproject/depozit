<?php


namespace App\Http\Services;


use GuzzleHttp\Client;

class PlayMobileService
{

    protected $login;
    protected $password;

    public function __construct() {
        $this->login = env('','depozituz');
        $this->password = env('', '7si4L6RJ');
    }

    public function sendCode($phone, $code)
    {
        $date = new \DateTime();
        $messageId = $this->generateMessageId();

        $url = 'http://91.204.239.44/broker-api/send';
        $headers = [
            'Accept' => 'application/json',
            'Authorization' => $this->basicAuth(),
            'X-Timestamp' => $date->format(\DateTime::ATOM),
        ];
        $json = [
            'messages' => [
                'recipient' => $phone,
                'message-id' => $messageId,
                'sms' => [
                    'originator' => '3700',
                    'content' => [
                        'text' => 'Confirmation code '.$code
                    ]
                ],
            ]
        ];

        $client = new Client(['http_errors' => false]);

        $response = $client->post($url, [
            'headers' => $headers,
            'json' => $json,
        ]);

        return json_decode($response->getStatusCode());
    }

    public function sendMessage($phone, $msg, $r_url)
    {
        $date = new \DateTime();
        $messageId = $this->generateMessageId();

        $url = 'http://91.204.239.44/broker-api/send';
        $headers = [
            'Accept' => 'application/json',
            'Authorization' => $this->basicAuth(),
            'X-Timestamp' => $date->format(\DateTime::ATOM),
        ];
        $json = [
            'messages' => [
                'recipient' => $phone,
                'message-id' => $messageId,
                'sms' => [
                    'originator' => '3700',
                    'content' => [
                        'text' => $msg.' '.$r_url
                    ]
                ],
            ]
        ];

        $client = new Client(['http_errors' => false]);

        $response = $client->post($url, [
            'headers' => $headers,
            'json' => $json,
        ]);

        return json_decode($response->getStatusCode());
    }

    protected function basicAuth()
    {
        $login = $this->login;
        $password = $this->password;
        return 'Basic ' . base64_encode($login . ':' . $password);
    }

    public function generateMessageId($length = 9)
    {
        $characters = '123456789';
        $charactersLength = strlen($characters);
        $randomNumber = '';
        for ($i = 0; $i < $length; $i++) {
            $randomNumber .= $characters[rand(0, $charactersLength - 1)];
        }
        return 'dep'.$randomNumber;
    }

    public function generateCode($length = 4)
    {
        $characters = '123456789';
        $charactersLength = strlen($characters);
        $randomNumber = '';
        for ($i = 0; $i < $length; $i++) {
            $randomNumber .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomNumber;
    }
}
