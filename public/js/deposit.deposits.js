var currentAjax = null;
$(document).ready(function() {
    function printPlural(x) {
        let a = x.split(" ");
        let op = parseInt(a[1]);
        if(op > 1) {
            a[2] = "депозита)"
        }
        return a.join(" ");
    }
    $('#f-el-price').on('keyup',function(){
        let v = $(this).val();
        if($(this).val().length != 0){
            if(!$.isNumeric(v)){
                $(this).attr('data-validate',false);
                $('#price-error').text($(this).attr('data-error-text')).show();
            }
            if($.isNumeric(v)){
                $(this).attr('data-validate',true);
                $('#price-error').hide();
                let val_cur = $('#f-el-currency').val();
                if(val_cur == 1){
                    if($(this).val().length >= 6){
                        let check_form = $('#small-search-form');
                        
                        if(currentAjax){
                            currentAjax.abort();
                            $('#content-result-filter').remove('.loader');
                        }
                         $('#content-result-filter').html('<div class="loader"><div class="ajax-preloader loader__wrap"><svg class="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 270.93 275.47"><g><path fill="#0876a2" d="M213.35,167.42q0,20.42-13.22,34A43.37,43.37,0,0,1,167.83,215H59.6V120.27H92.31v59.62h75.75q12.57,0,12.58-12.52v-55q0-12.53-12.57-12.52H59.6V64.75H167.83a43.41,43.41,0,0,1,32.3,13.56q13.22,13.56,13.22,34Z" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M258.76,0H11.7a.56.56,0,0,0-.51.41.58.58,0,0,0,.13.48l24.35,26H234.8l24.35-26a.58.58,0,0,0,.13-.48.56.56,0,0,0-.52-.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M11.7,275.47H258.76a.54.54,0,0,0,.52-.41.55.55,0,0,0-.13-.47l-24.35-26H35.67l-24.36,26a.59.59,0,0,0-.12.47.54.54,0,0,0,.51.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M273.14,262.22V15.16a.54.54,0,0,0-.41-.52.55.55,0,0,0-.47.13l-26,24.35V238.25l26,24.36a.59.59,0,0,0,.47.12.54.54,0,0,0,.41-.51" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M2.22,14.16V261.22a.54.54,0,0,0,.41.51.59.59,0,0,0,.47-.12l26-24.36V38.12L3.1,13.77a.55.55,0,0,0-.47-.13.54.54,0,0,0-.41.52"/></g></svg><svg class="progress" width="120" height="120" viewBox="0 0 120 120"><circle class="progress__value" cx="60" cy="60" r="50" fill="none" stroke="#fff" stroke-width="100" /></svg></div></div>');
                        currentAjax = $.ajax({
                            type:'GET',
                            url:check_form.attr('action'),
                            data: check_form.serialize()+'&type_filter=small',
                            success:function (data) {
                               currentAjax = null;
                        
                                $('#content-result-filter').html(data);
                                $('#count-result').text(printPlural($('#count-depozit').attr('data-text')));
                                price  = $('#f-el-price').val();
                                date = $('#f-el-date-type').val();
                                if(!price || !date){
                                    $('#text-table-td').text($('#text-table-td').attr('data-text-sum'));
                                } else {
                                    $('#text-table-td').text($('#text-table-td').attr('data-text-benefit'));
                                }
                                $('#online-deposit-input').attr('data-form-id','#small-search-form');
                                sort_desc();
                            }
                        });
                    }
                }if(val_cur == 2 || val_cur == 4){
                    if($(this).val().length == 4 || $(this).val().length == 5 || $(this).val().length == 6){
                        let check_form = $('#small-search-form');
                        
                        if(currentAjax){
                            currentAjax.abort();
                            $('#content-result-filter').remove('.loader');
                        }
                        $('#content-result-filter').html('<div class="loader"><div class="ajax-preloader loader__wrap"><svg class="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 270.93 275.47"><g><path fill="#0876a2" d="M213.35,167.42q0,20.42-13.22,34A43.37,43.37,0,0,1,167.83,215H59.6V120.27H92.31v59.62h75.75q12.57,0,12.58-12.52v-55q0-12.53-12.57-12.52H59.6V64.75H167.83a43.41,43.41,0,0,1,32.3,13.56q13.22,13.56,13.22,34Z" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M258.76,0H11.7a.56.56,0,0,0-.51.41.58.58,0,0,0,.13.48l24.35,26H234.8l24.35-26a.58.58,0,0,0,.13-.48.56.56,0,0,0-.52-.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M11.7,275.47H258.76a.54.54,0,0,0,.52-.41.55.55,0,0,0-.13-.47l-24.35-26H35.67l-24.36,26a.59.59,0,0,0-.12.47.54.54,0,0,0,.51.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M273.14,262.22V15.16a.54.54,0,0,0-.41-.52.55.55,0,0,0-.47.13l-26,24.35V238.25l26,24.36a.59.59,0,0,0,.47.12.54.54,0,0,0,.41-.51" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M2.22,14.16V261.22a.54.54,0,0,0,.41.51.59.59,0,0,0,.47-.12l26-24.36V38.12L3.1,13.77a.55.55,0,0,0-.47-.13.54.54,0,0,0-.41.52"/></g></svg><svg class="progress" width="120" height="120" viewBox="0 0 120 120"><circle class="progress__value" cx="60" cy="60" r="50" fill="none" stroke="#fff" stroke-width="100" /></svg></div></div>');
                        currentAjax = $.ajax({
                            type:'GET',
                            url:check_form.attr('action'),
                            data: check_form.serialize()+'&type_filter=small',
                            success:function (data) {
                                currentAjax = null;
                        
                                $('#content-result-filter').html(data);
                                $('#count-result').text(printPlural($('#count-depozit').attr('data-text')));
                                price  = $('#f-el-price').val();
                                date = $('#f-el-date-type').val();
                                if(!price || !date){
                                    $('#text-table-td').text($('#text-table-td').attr('data-text-sum'));
                                } else {
                                    $('#text-table-td').text($('#text-table-td').attr('data-text-benefit'));
                                }
                                 $('#online-deposit-input').attr('data-form-id','#small-search-form');
                                 sort_desc();
                            }
                        });
                    }
                }

            }
        }
        if($(this).val().length == 0){
            $(this).attr('data-validate',true);
            $('#price-error').hide();
        }
    });

    $('#large-f-el-price').on('keyup',function(){
        let v = $(this).val();
        if($(this).val().length != 0){
            if(!$.isNumeric(v)){
                $(this).attr('data-validate',false);
                $('#large-price-error').text($(this).attr('data-error-text')).show();
            }
            if($.isNumeric(v)){
                $(this).attr('data-validate',true);
                $('#large-price-error').hide();
            }
        }
        if($(this).val().length == 0){
            $(this).attr('data-validate',true);
            $('#large-price-error').hide();
        }
    });

    $('#small-search-form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $('.menu-deposit').addClass('current');

    let check_form = $('#small-search-form');
    /*$('.icheck-helper').click(function(e){
        e.preventDefault();
        if($('#f-el-price').attr('data-validate') == 'true') {
            //$('#content-result-filter').html("<div class='card-body loader position-inherit bg-white border-r-8'><span></span><span></span><span></span></div>");
            $.ajax({
                type: 'GET',
                url:
                data: check_form.serialize()
                , success: function (data) {
                    $('#content-result-filter').html(data);
                    $('#count-result').text($('#count-depozit').val()+' '+"");
                }, error: function () {
                    alert('error');
                }
            });
        }
    });*/

    // auto send
    //$('#content-result-filter').html("<div class='card-body loader position-inherit bg-white border-r-8'><span></span><span></span><span></span></div>");
    $.ajax({
        type:'GET',
        url:check_form.attr('action'),
        data: check_form.serialize()+'&type_filter=small',
        success:function (data) {
            $('#content-result-filter').html(data);
            $('#count-result').text(printPlural($('#count-depozit').attr('data-text')));
            price  = $('#f-el-price').val();
            date = $('#f-el-date-type').val();
                if(!price || !date){
                    $('#text-table-td').text($('#text-table-td').attr('data-text-sum'));
                } else {
                    $('#text-table-td').text($('#text-table-td').attr('data-text-benefit'));
                }
                $('#online-deposit-input').attr('data-form-id','#small-search-form');
                sort_desc();
            }
    });

    $('.btn-filter-deposit-ajax').on('click',function (e) {
        $('#open-content').attr('data-status',false).attr('data-content-id','');
        if($('#f-el-price').attr('data-validate') == 'true'){
            e.preventDefault();
            
            if(currentAjax){
                currentAjax.abort();
                $('#content-result-filter').remove('.loader');
            }
            $('#content-result-filter').html('<div class="loader"><div class="ajax-preloader loader__wrap"><svg class="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 270.93 275.47"><g><path fill="#0876a2" d="M213.35,167.42q0,20.42-13.22,34A43.37,43.37,0,0,1,167.83,215H59.6V120.27H92.31v59.62h75.75q12.57,0,12.58-12.52v-55q0-12.53-12.57-12.52H59.6V64.75H167.83a43.41,43.41,0,0,1,32.3,13.56q13.22,13.56,13.22,34Z" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M258.76,0H11.7a.56.56,0,0,0-.51.41.58.58,0,0,0,.13.48l24.35,26H234.8l24.35-26a.58.58,0,0,0,.13-.48.56.56,0,0,0-.52-.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M11.7,275.47H258.76a.54.54,0,0,0,.52-.41.55.55,0,0,0-.13-.47l-24.35-26H35.67l-24.36,26a.59.59,0,0,0-.12.47.54.54,0,0,0,.51.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M273.14,262.22V15.16a.54.54,0,0,0-.41-.52.55.55,0,0,0-.47.13l-26,24.35V238.25l26,24.36a.59.59,0,0,0,.47.12.54.54,0,0,0,.41-.51" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M2.22,14.16V261.22a.54.54,0,0,0,.41.51.59.59,0,0,0,.47-.12l26-24.36V38.12L3.1,13.77a.55.55,0,0,0-.47-.13.54.54,0,0,0-.41.52"/></g></svg><svg class="progress" width="120" height="120" viewBox="0 0 120 120"><circle class="progress__value" cx="60" cy="60" r="50" fill="none" stroke="#fff" stroke-width="100" /></svg></div></div>');

            currentAjax = $.ajax({
                type:'GET',
                url:check_form.attr('action'),
                data: check_form.serialize()+'&type_filter=small',
                success:function (data) {
                    currentAjax = null;
            
                    $('#content-result-filter').html(data);
                    $('#count-result').text(printPlural($('#count-depozit').attr('data-text')));
                    price  = $('#f-el-price').val();
                    date = $('#f-el-date-type').val();

                    if( !price || !date){
                        $('#text-table-td').text($('#text-table-td').attr('data-text-sum'));
                    } else {
                        $('#text-table-td').text($('#text-table-td').attr('data-text-benefit'));
                    }
                    $('#online-deposit-input').attr('data-form-id','#small-search-form');
                    sort_desc();
                }
            });
        }
    });

    //input focus and change data-focus to true
    $('.select2-selection').focus(function () {
        $('.select2-f-el-currency-container').attr('data-focus',true);
        $('.select2-f-el-date-type-container').attr('data-focus',true);
    });
    /*class filter-focus-ajax on change and get ajax else data-focus is true*/
        $('.filter-focus-ajax').on('change',function (e) {
            $('#open-content').attr('data-status',false).attr('data-content-id','');
            if($(this).attr('data-focus') == 'true'){
                if($('#f-el-price').attr('data-validate') == 'true'){
                    e.preventDefault();
                    
                    if(currentAjax){
                       currentAjax.abort();
                       $('#content-result-filter').remove('.loader');
                    }

                    $('#content-result-filter').html('<div class="loader"><div class="ajax-preloader loader__wrap"><svg class="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 270.93 275.47"><g><path fill="#0876a2" d="M213.35,167.42q0,20.42-13.22,34A43.37,43.37,0,0,1,167.83,215H59.6V120.27H92.31v59.62h75.75q12.57,0,12.58-12.52v-55q0-12.53-12.57-12.52H59.6V64.75H167.83a43.41,43.41,0,0,1,32.3,13.56q13.22,13.56,13.22,34Z" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M258.76,0H11.7a.56.56,0,0,0-.51.41.58.58,0,0,0,.13.48l24.35,26H234.8l24.35-26a.58.58,0,0,0,.13-.48.56.56,0,0,0-.52-.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M11.7,275.47H258.76a.54.54,0,0,0,.52-.41.55.55,0,0,0-.13-.47l-24.35-26H35.67l-24.36,26a.59.59,0,0,0-.12.47.54.54,0,0,0,.51.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M273.14,262.22V15.16a.54.54,0,0,0-.41-.52.55.55,0,0,0-.47.13l-26,24.35V238.25l26,24.36a.59.59,0,0,0,.47.12.54.54,0,0,0,.41-.51" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M2.22,14.16V261.22a.54.54,0,0,0,.41.51.59.59,0,0,0,.47-.12l26-24.36V38.12L3.1,13.77a.55.55,0,0,0-.47-.13.54.54,0,0,0-.41.52"/></g></svg><svg class="progress" width="120" height="120" viewBox="0 0 120 120"><circle class="progress__value" cx="60" cy="60" r="50" fill="none" stroke="#fff" stroke-width="100" /></svg></div></div>');
                    currentAjax = $.ajax({
                        type:'GET',
                        url: check_form.attr('action'),
                        data: check_form.serialize()+'&type_filter=small',
                        success:function (data) {
                            currentAjax = null;
                    
                            $('#content-result-filter').html(data);
                            $('#count-result').text(printPlural($('#count-depozit').attr('data-text')));
                            price  = $('#f-el-price').val();
                            date = $('#f-el-date-type').val();

                            if( !price || !date){
                                $('#text-table-td').text($('#text-table-td').attr('data-text-sum'));
                            } else {
                                $('#text-table-td').text($('#text-table-td').attr('data-text-benefit'));
                            }
                            $('#online-deposit-input').attr('data-form-id','#small-search-form');
                            sort_desc();
                        }
                    });
                }
            }
        });

    $('.filter-deposit-ajax').on('change',function (e) {
        if($('#f-el-price').attr('data-validate') == 'true'){
            e.preventDefault();
            if(currentAjax){
               currentAjax.abort();
               $('#content-result-filter').remove('.loader');
            }
            $('#content-result-filter').html('<div class="loader"><div class="ajax-preloader loader__wrap"><svg class="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 270.93 275.47"><g><path fill="#0876a2" d="M213.35,167.42q0,20.42-13.22,34A43.37,43.37,0,0,1,167.83,215H59.6V120.27H92.31v59.62h75.75q12.57,0,12.58-12.52v-55q0-12.53-12.57-12.52H59.6V64.75H167.83a43.41,43.41,0,0,1,32.3,13.56q13.22,13.56,13.22,34Z" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M258.76,0H11.7a.56.56,0,0,0-.51.41.58.58,0,0,0,.13.48l24.35,26H234.8l24.35-26a.58.58,0,0,0,.13-.48.56.56,0,0,0-.52-.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M11.7,275.47H258.76a.54.54,0,0,0,.52-.41.55.55,0,0,0-.13-.47l-24.35-26H35.67l-24.36,26a.59.59,0,0,0-.12.47.54.54,0,0,0,.51.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M273.14,262.22V15.16a.54.54,0,0,0-.41-.52.55.55,0,0,0-.47.13l-26,24.35V238.25l26,24.36a.59.59,0,0,0,.47.12.54.54,0,0,0,.41-.51" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M2.22,14.16V261.22a.54.54,0,0,0,.41.51.59.59,0,0,0,.47-.12l26-24.36V38.12L3.1,13.77a.55.55,0,0,0-.47-.13.54.54,0,0,0-.41.52"/></g></svg><svg class="progress" width="120" height="120" viewBox="0 0 120 120"><circle class="progress__value" cx="60" cy="60" r="50" fill="none" stroke="#fff" stroke-width="100" /></svg></div></div>');

            currentAjax = $.ajax({
                type:'GET',
                url: check_form.attr('action'),
                data: check_form.serialize()+'&type_filter=small',
                success:function (data) {
                    currentAjax = null;
            
                    $('#content-result-filter').html(data);
                    $('#count-result').text(printPlural($('#count-depozit').attr('data-text')));
                    price  = $('#f-el-price').val();
                    date = $('#f-el-date-type').val();

                    if( !price || !date){
                        $('#text-table-td').text($('#text-table-td').attr('data-text-sum'));
                    } else {
                        $('#text-table-td').text($('#text-table-td').attr('data-text-benefit'));
                    }
                    $('#online-deposit-input').attr('data-form-id','#small-search-form');
                    sort_desc();
                }
            });
        }
    });

    //lagre search
    let large_form = $('#large-search-form');
    $('.search-btn-modal').click(function (e) {
        $('#open-content').attr('data-status',false).attr('data-content-id','');
        e.preventDefault();
        $('.modal-ui__close-btn').click();
        $('#f-el-price').val($('#large-f-el-price').val());
        $('#f-el-currency').attr('data-focus',false);
        $('#f-el-currency').val($('#large-f-el-currency').val()).trigger('change');
        $('#f-el-date-type').attr('data-focus',false);
        $('#f-el-date-type').val($('#large-f-el-date').val()).trigger('change');
        if(currentAjax){
           currentAjax.abort();
           $('#content-result-filter').remove('.loader');
        }
        $('#content-result-filter').html('<div class="loader"><div class="ajax-preloader loader__wrap"><svg class="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 270.93 275.47"><g><path fill="#0876a2" d="M213.35,167.42q0,20.42-13.22,34A43.37,43.37,0,0,1,167.83,215H59.6V120.27H92.31v59.62h75.75q12.57,0,12.58-12.52v-55q0-12.53-12.57-12.52H59.6V64.75H167.83a43.41,43.41,0,0,1,32.3,13.56q13.22,13.56,13.22,34Z" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M258.76,0H11.7a.56.56,0,0,0-.51.41.58.58,0,0,0,.13.48l24.35,26H234.8l24.35-26a.58.58,0,0,0,.13-.48.56.56,0,0,0-.52-.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M11.7,275.47H258.76a.54.54,0,0,0,.52-.41.55.55,0,0,0-.13-.47l-24.35-26H35.67l-24.36,26a.59.59,0,0,0-.12.47.54.54,0,0,0,.51.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M273.14,262.22V15.16a.54.54,0,0,0-.41-.52.55.55,0,0,0-.47.13l-26,24.35V238.25l26,24.36a.59.59,0,0,0,.47.12.54.54,0,0,0,.41-.51" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M2.22,14.16V261.22a.54.54,0,0,0,.41.51.59.59,0,0,0,.47-.12l26-24.36V38.12L3.1,13.77a.55.55,0,0,0-.47-.13.54.54,0,0,0-.41.52"/></g></svg><svg class="progress" width="120" height="120" viewBox="0 0 120 120"><circle class="progress__value" cx="60" cy="60" r="50" fill="none" stroke="#fff" stroke-width="100" /></svg></div></div>');
        currentAjax =  $.ajax({
            type:$(large_form).attr('method'),
            url: $(large_form).attr('action'),
            data: large_form.serialize()+'&type_filter=large',
            success:function (data) {
                currentAjax = null;
        
                $('#content-result-filter').html(data);
                $('#count-result').text(printPlural($('#count-depozit').attr('data-text')));

                price  = $('#f-el-price').val();
                date = $('#f-el-date-type').val();
                if( !price || !date){
                    $('#text-table-td').text($('#text-table-td').attr('data-text-sum'));
                } else {
                    $('#text-table-td').text($('#text-table-td').attr('data-text-benefit'));
                }
                $('#online-deposit-input').attr('data-form-id','#large-search-form');
                sort_desc();

            }
        });
    });



    $('.iCheck-helper').on('click',function(e){
        
        let parent = $('#online-deposit-input').parent('.icheckbox_minimal-blue');
        status = $(parent).attr('aria-checked');
        if(status == 'true') {
            input_status = 1;
        } else if (status == 'false'){
            input_status = 0;
        }

        e.preventDefault();
        
        let online_input = $('#online-deposit-input').attr('data-form-id');
        
        form = $(online_input);
        
        let form_data = form.serialize()+'&type_filter=small';
        
        if(online_input == '#large-search-form'){
            form_data = form.serialize()+'&type_filter=small'+'&online_deposit='+input_status; 
        }
        if(online_input == '#small-search-form'){
            form_data = form.serialize()+'&type_filter=large'+'&online_deposit='+input_status; 
        }

        $('#progress-bar').show();
        var i = 0;
        if(i==0){
            i = 1;
            var elem = document.getElementById('bar');
            var width = 1;
            var id = setInterval(frame,90);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                    i = 0;
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }
        if(currentAjax){
           currentAjax.abort();
        }
       currentAjax =  $.ajax({
            type:'GET',
            url: form.attr('action'),
            data: form_data,
            success:function (data) {
                currentAjax = null;
                $('#content-result-filter').html(data);
                $('#count-result').text(printPlural($('#count-depozit').attr('data-text')));
                price  = $('#f-el-price').val();
                date = $('#f-el-date-type').val();

                if( !price || !date){
                    $('#text-table-td').text($('#text-table-td').attr('data-text-sum'));
                } else {
                    $('#text-table-td').text($('#text-table-td').attr('data-text-benefit'));
                }

                $('#progress-bar').hide();
                 clearInterval(id);
                 sort_desc();
            }
        });
    });
  /*  (function( $ ){
        $.fn.selectOptionCurrency = function() {

            return this;
        };
    })( jQuery );*/

    function sort_desc(){
        $('#content-result-filter .item-section').sort(div_desc).appendTo('#content-result-filter');
        if($('#sort-percent').attr('data-sort') == 'asc'){
            $('#sort-percent').toggleClass('fa-sort-amount-asc').toggleClass('fa-sort-amount-desc');
            $('#sort-percent').attr('data-sort','desc')
        }
    }

    function sort_asc(){

    }

   function div_desc(a,b){ 
        return ($(b).data('percent')) > ($(a).data('percent')) ? 1 : -1;
   }
   function div_asc(a,b){
        return ($(b).data('percent')) < ($(a).data('percent')) ? 1 : -1;
   }
});


