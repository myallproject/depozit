$('.add-comparison-debit-other').on('click',function() {
    $(this).toggleClass('added-to-balance');
    let status = $(this).attr('data-status');
    let id = $(this).data('id');

    if (status == false || status == 'false') {
        $(this).attr('data-status', true);
        $.ajax({
            url: $(this).attr('data-add-route'),
            data: {
                id: id,
                url_type: 'card_debit'
            },
            success: function (data) {
                $(".comparison-amount").text(data);
            }
        });
    }
    if (status == "true") {
        $(this).attr('data-status', false);
        $.ajax({
            url: $(this).attr('data-remove-route'),
            data: {
                id: id,
                url_type: 'card_debit'
            },
            success: function (data) {
                $(".comparison-amount").text(data);
            }
        });
    }
});

$('.add-comparison-credit-other').on('click',function() {
    $(this).toggleClass('added-to-balance');
    let status = $(this).attr('data-status');
    let id = $(this).data('id');

    if (status == false || status == 'false') {
        $(this).attr('data-status', true);
        $.ajax({
            url: $(this).attr('data-add-route'),
            data: {
                id: id,
                url_type: 'card_credit'
            },
            success: function (data) {
                $(".comparison-amount").text(data);
            }
        });
    }
    if (status == "true") {
        $(this).attr('data-status', false);
        $.ajax({
            url: $(this).attr('data-remove-route'),
            data: {
                id: id,
                url_type: 'card_credit'
            },
            success: function (data) {
                $(".comparison-amount").text(data);
            }
        });
    }
});