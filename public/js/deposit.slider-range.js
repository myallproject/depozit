
  var currentAjax = null;
  rageInputSmall('myRange','f-el-price','price-error');
  rageInput('myRangeLarge','large-f-el-price','large-price-error');
  
  function rageInput (rangeId, inputId, errorId=false){
    var slider = document.getElementById(rangeId);
    var output = document.getElementById(inputId);
    if(slider.value == 0){
      output.value = '';
    } else {
      output.value = slider.value;
      
    }

    backgroundColor(rangeId,inputId);

    slider.oninput = function() {
      if(this.value == 0){
        output.value = '';
      } else{
        output.value = this.value;
          
        backgroundColor(rangeId,inputId);

        if($.isNumeric(output.value)){
            $('#'+inputId).attr('data-validate',true);
            $('#'+errorId).hide();
          } else {
            $('#'+inputId).attr('data-validate',false);
          }
      }
    }
  
    $('#'+inputId).on('keyup',function(){
      let all =  $('#'+rangeId).attr('max');
      backgroundColor(rangeId,inputId);

      if(!$(this).val() || !$.isNumeric($(this).val())){
        $('#'+rangeId).val(0).trigger('change');
         backgroundColor(rangeId,inputId);
      } else {
        if($(this).val() > parseInt(all)){
          $(this).val(all);
          $('#'+rangeId).val($(this).val()).trigger('change');
        } else {
          $('#'+rangeId).val($(this).val()).trigger('change');
        }
      }
      if($.isNumeric($(this).val())){
        $(this).attr('data-validate',true);
        $('#'+errorId).hide();
      } else {
        $(this).attr('data-validate',false);
      }
    });
  }
    
  function backgroundColor(rangeId, inputId){
    let all =  $('#'+rangeId).attr('max');
    let piece = $('#'+inputId).val();
    if($.isNumeric(piece)){
      piece_percent = (parseInt(piece) * 100 ) / parseInt(all);
    } else {
      piece_percent = 0;
    }
    $('#'+rangeId).css('background','linear-gradient(90deg, #2196f3 '+piece_percent+'%, #00ffff00 0%)');
  }

function sendAjaxRequest(){

  if(currentAjax){
    currentAjax.abort();
  }
  
  $('#content-result-filter').html('<div class="loader"><div class="ajax-preloader loader__wrap"><svg class="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 270.93 275.47"><g><path fill="#0876a2" d="M213.35,167.42q0,20.42-13.22,34A43.37,43.37,0,0,1,167.83,215H59.6V120.27H92.31v59.62h75.75q12.57,0,12.58-12.52v-55q0-12.53-12.57-12.52H59.6V64.75H167.83a43.41,43.41,0,0,1,32.3,13.56q13.22,13.56,13.22,34Z" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M258.76,0H11.7a.56.56,0,0,0-.51.41.58.58,0,0,0,.13.48l24.35,26H234.8l24.35-26a.58.58,0,0,0,.13-.48.56.56,0,0,0-.52-.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M11.7,275.47H258.76a.54.54,0,0,0,.52-.41.55.55,0,0,0-.13-.47l-24.35-26H35.67l-24.36,26a.59.59,0,0,0-.12.47.54.54,0,0,0,.51.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M273.14,262.22V15.16a.54.54,0,0,0-.41-.52.55.55,0,0,0-.47.13l-26,24.35V238.25l26,24.36a.59.59,0,0,0,.47.12.54.54,0,0,0,.41-.51" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M2.22,14.16V261.22a.54.54,0,0,0,.41.51.59.59,0,0,0,.47-.12l26-24.36V38.12L3.1,13.77a.55.55,0,0,0-.47-.13.54.54,0,0,0-.41.52"/></g></svg><svg class="progress" width="120" height="120" viewBox="0 0 120 120"><circle class="progress__value" cx="60" cy="60" r="50" fill="none" stroke="#fff" stroke-width="100" /></svg></div></div>');
  
  let filter_form = $('#small-search-form');
  
  currentAjax=$.ajax({
        type:filter_form.attr('method'),
        url:filter_form.attr('action'),
        data: filter_form.serialize()+'&type_filter=small',
        success:function (data) {
          currentAjax = null;
            $('#content-result-filter').html(data);
            $('#count-result').text($('#count-credit').attr('data-text'));
            $('#online-deposit-input').attr('data-form-id','#small-search-form');
        },
  });


}
 
function rageInputSmall (rangeId, inputId, errorId=false){
    var slider = document.getElementById(rangeId);
    var output = document.getElementById(inputId);
    if(slider.value == 0){
      output.value = '';

    } else {
      output.value = slider.value;
    }

    backgroundColor(rangeId,inputId);

    slider.oninput = function() {
      if(this.value == 0){
        output.value = '';
        // sendAjaxRequest();
      } else{
        output.value = this.value;
          
        backgroundColor(rangeId,inputId);

        if($.isNumeric(output.value)){
            sendAjaxRequest();
            $('#'+inputId).attr('data-validate',true);
            $('#'+errorId).hide();
          } else {
            $('#'+inputId).attr('data-validate',false);
          }
      }
    }
  
    $('#'+inputId).on('keyup',function(){
      let all =  $('#'+rangeId).attr('max');
      backgroundColor(rangeId,inputId);

      if(!$(this).val() || !$.isNumeric($(this).val())){
        $('#'+rangeId).val(0).trigger('change');
         backgroundColor(rangeId,inputId);
      } else {
        if($(this).val() > parseInt(all)){
          $(this).val(all);
          $('#'+rangeId).val($(this).val()).trigger('change');
        } else {
          $('#'+rangeId).val($(this).val()).trigger('change');
        }
      }
      if($.isNumeric($(this).val())){
        sendAjaxRequest();
        $(this).attr('data-validate',true);
        $('#'+errorId).hide();
      } else {
        $(this).attr('data-validate',false);
      }
      if(!$(this).val()){
        sendAjaxRequest();
      }
    });
  }