var currentAjax = null;
$(document).ready(function() {
   //my new auto logic
    $('#car-brand-small').on('change',function(){
        $('#car-small-div').find('i').addClass('fa fa-refresh fa-spin fa-3x fa-fw');
        $.ajax({
            method:'GET',
            url:$(this).attr('data-childred-url'),
            data:{
                id: $(this).val()
            }
        }).done(function(data,){
            $('#car-small-div').find('i').removeClass('fa fa-refresh fa-spin fa-3x fa-fw');
            $('#car-small').removeAttr('disabled');
            $('#car-small').html(data);
        });
    });
    $('#car-small').on('change', function() {
        $('#credit-result').html('<div class="loader"><div class="ajax-preloader loader__wrap"><svg class="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 270.93 275.47"><g><path fill="#0876a2" d="M213.35,167.42q0,20.42-13.22,34A43.37,43.37,0,0,1,167.83,215H59.6V120.27H92.31v59.62h75.75q12.57,0,12.58-12.52v-55q0-12.53-12.57-12.52H59.6V64.75H167.83a43.41,43.41,0,0,1,32.3,13.56q13.22,13.56,13.22,34Z" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M258.76,0H11.7a.56.56,0,0,0-.51.41.58.58,0,0,0,.13.48l24.35,26H234.8l24.35-26a.58.58,0,0,0,.13-.48.56.56,0,0,0-.52-.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M11.7,275.47H258.76a.54.54,0,0,0,.52-.41.55.55,0,0,0-.13-.47l-24.35-26H35.67l-24.36,26a.59.59,0,0,0-.12.47.54.54,0,0,0,.51.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M273.14,262.22V15.16a.54.54,0,0,0-.41-.52.55.55,0,0,0-.47.13l-26,24.35V238.25l26,24.36a.59.59,0,0,0,.47.12.54.54,0,0,0,.41-.51" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M2.22,14.16V261.22a.54.54,0,0,0,.41.51.59.59,0,0,0,.47-.12l26-24.36V38.12L3.1,13.77a.55.55,0,0,0-.47-.13.54.54,0,0,0-.41.52"/></g></svg><svg class="progress" width="120" height="120" viewBox="0 0 120 120"><circle class="progress__value" cx="60" cy="60" r="50" fill="none" stroke="#fff" stroke-width="100" /></svg></div></div>');
        let element = $(this).find('option:selected'); 
        let imagePath = '/uploads/cars/'+ element.attr('data-image');
        $("#car-image").attr('src', imagePath);
        currentAjax = $.ajax({
            type: filter_form.attr('method'),
            url:  filter_form.attr('action'),
            data: filter_form.serialize()+'&type_filter=small',
            success: function (data) {
                currentAjax = null;
                $('#credit-result').html(data);
                $('#count-result').text($('#count-credit').attr('data-text'));
                $('#online-credit-input').attr('data-form-id','#credit-filter-form-small');
                sort_asc();
            }
        });
    })



   //end

    $('#large-search-amount-id').on('keyup',function(){
        let v = $(this).val();
        if($(this).val().length != 0){
            if(!$.isNumeric(v)){
                $(this).attr('data-validate',false);
                $('#large-amount-error').text($(this).attr('data-error-text')).show();
            }
            if($.isNumeric(v)){
                $(this).attr('data-validate',true);
                $('#large-amount-error').hide();
            }
        }
        if($(this).val().length == 0){
            $(this).attr('data-validate',true);
            $('#large-amount-error').hide();
        }
    });

    $('#large-search-age').on('keyup',function(){
        let v = $(this).val();
        if($(this).val().length != 0){
            if(!$.isNumeric(v)){
                $(this).attr('data-validate',false);
                $('#search-borrow-age-error').text($(this).attr('data-error-text')).show();
            }
            if($.isNumeric(v)){
                $(this).attr('data-validate',true);
                $('#search-borrow-age-error').hide();
            }
        }
        if($(this).val().length == 0){
            $(this).attr('data-validate',true);
            $('#search-borrow-age-error').hide();
        }
    });



    $('#credit-filter-form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $('.menu-credit').addClass('current');

    let filter_form = $('#credit-filter-form-small');

    $('#credit-result').html('<div class="loader"><div class="ajax-preloader loader__wrap"><svg class="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 270.93 275.47"><g><path fill="#0876a2" d="M213.35,167.42q0,20.42-13.22,34A43.37,43.37,0,0,1,167.83,215H59.6V120.27H92.31v59.62h75.75q12.57,0,12.58-12.52v-55q0-12.53-12.57-12.52H59.6V64.75H167.83a43.41,43.41,0,0,1,32.3,13.56q13.22,13.56,13.22,34Z" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M258.76,0H11.7a.56.56,0,0,0-.51.41.58.58,0,0,0,.13.48l24.35,26H234.8l24.35-26a.58.58,0,0,0,.13-.48.56.56,0,0,0-.52-.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M11.7,275.47H258.76a.54.54,0,0,0,.52-.41.55.55,0,0,0-.13-.47l-24.35-26H35.67l-24.36,26a.59.59,0,0,0-.12.47.54.54,0,0,0,.51.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M273.14,262.22V15.16a.54.54,0,0,0-.41-.52.55.55,0,0,0-.47.13l-26,24.35V238.25l26,24.36a.59.59,0,0,0,.47.12.54.54,0,0,0,.41-.51" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M2.22,14.16V261.22a.54.54,0,0,0,.41.51.59.59,0,0,0,.47-.12l26-24.36V38.12L3.1,13.77a.55.55,0,0,0-.47-.13.54.54,0,0,0-.41.52"/></g></svg><svg class="progress" width="120" height="120" viewBox="0 0 120 120"><circle class="progress__value" cx="60" cy="60" r="50" fill="none" stroke="#fff" stroke-width="100" /></svg></div></div>');
    $.ajax({
        type:filter_form.attr('method'),
        url:filter_form.attr('action'),
        data: filter_form.serialize()+'&type_filter=small',
        success:function (data) {
            $('#credit-result').html(data);
            $('#count-result').text($('#count-credit').attr('data-text'));
            $('#online-credit-input').attr('data-form-id','#credit-filter-form-small');
            sort_asc();
        }
    });

    $('.filter-credit-btn-small').on('click',function(e) {
        e.preventDefault();
        
        if(currentAjax){
            currentAjax.abort();
            $('#credit-result').remove('.loader');
        }
        $('#credit-result').html('<div class="loader"><div class="ajax-preloader loader__wrap"><svg class="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 270.93 275.47"><g><path fill="#0876a2" d="M213.35,167.42q0,20.42-13.22,34A43.37,43.37,0,0,1,167.83,215H59.6V120.27H92.31v59.62h75.75q12.57,0,12.58-12.52v-55q0-12.53-12.57-12.52H59.6V64.75H167.83a43.41,43.41,0,0,1,32.3,13.56q13.22,13.56,13.22,34Z" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M258.76,0H11.7a.56.56,0,0,0-.51.41.58.58,0,0,0,.13.48l24.35,26H234.8l24.35-26a.58.58,0,0,0,.13-.48.56.56,0,0,0-.52-.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M11.7,275.47H258.76a.54.54,0,0,0,.52-.41.55.55,0,0,0-.13-.47l-24.35-26H35.67l-24.36,26a.59.59,0,0,0-.12.47.54.54,0,0,0,.51.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M273.14,262.22V15.16a.54.54,0,0,0-.41-.52.55.55,0,0,0-.47.13l-26,24.35V238.25l26,24.36a.59.59,0,0,0,.47.12.54.54,0,0,0,.41-.51" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M2.22,14.16V261.22a.54.54,0,0,0,.41.51.59.59,0,0,0,.47-.12l26-24.36V38.12L3.1,13.77a.55.55,0,0,0-.47-.13.54.54,0,0,0-.41.52"/></g></svg><svg class="progress" width="120" height="120" viewBox="0 0 120 120"><circle class="progress__value" cx="60" cy="60" r="50" fill="none" stroke="#fff" stroke-width="100" /></svg></div></div>');
        // if($('#f-el-amount').attr('data-validate') == 'true') {
            currentAjax = $.ajax({
                type: filter_form.attr('method'),
                url:  filter_form.attr('action'),
                data: filter_form.serialize()+'&type_filter=small',
                success: function (data) {
                    currentAjax = null;
                    $('#credit-result').html(data);
                    $('#count-result').text($('#count-credit').attr('data-text'));
                    $('#online-credit-input').attr('data-form-id','#credit-filter-form-small');
                    sort_asc();
                }
            });
        // }
    });

    //large search
    let filter_form_large = $('#credit-filter-form-large');

    $('.filter-credit-btn-large').on('click',function(e) {
        
        e.preventDefault();
        
        if(currentAjax){
            currentAjax.abort();
            $('#credit-result').remove('.loader');
        }
        $('#credit-result').html('<div class="loader"><div class="ajax-preloader loader__wrap"><svg class="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 270.93 275.47"><g><path fill="#0876a2" d="M213.35,167.42q0,20.42-13.22,34A43.37,43.37,0,0,1,167.83,215H59.6V120.27H92.31v59.62h75.75q12.57,0,12.58-12.52v-55q0-12.53-12.57-12.52H59.6V64.75H167.83a43.41,43.41,0,0,1,32.3,13.56q13.22,13.56,13.22,34Z" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M258.76,0H11.7a.56.56,0,0,0-.51.41.58.58,0,0,0,.13.48l24.35,26H234.8l24.35-26a.58.58,0,0,0,.13-.48.56.56,0,0,0-.52-.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M11.7,275.47H258.76a.54.54,0,0,0,.52-.41.55.55,0,0,0-.13-.47l-24.35-26H35.67l-24.36,26a.59.59,0,0,0-.12.47.54.54,0,0,0,.51.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M273.14,262.22V15.16a.54.54,0,0,0-.41-.52.55.55,0,0,0-.47.13l-26,24.35V238.25l26,24.36a.59.59,0,0,0,.47.12.54.54,0,0,0,.41-.51" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M2.22,14.16V261.22a.54.54,0,0,0,.41.51.59.59,0,0,0,.47-.12l26-24.36V38.12L3.1,13.77a.55.55,0,0,0-.47-.13.54.54,0,0,0-.41.52"/></g></svg><svg class="progress" width="120" height="120" viewBox="0 0 120 120"><circle class="progress__value" cx="60" cy="60" r="50" fill="none" stroke="#fff" stroke-width="100" /></svg></div></div>');

        $('#cr-type-provision').attr('data-focus',false);
        $('#f-el-date').attr('data-focus',false);
        if($('#f-el-amount').attr('data-validate') == 'true') {
            $('.modal-ui__close-btn').click();
           currentAjax = $.ajax({
                type: filter_form_large.attr('method'),
                url:  filter_form_large.attr('action'),
                data: filter_form_large.serialize()+'&type_filter=large',
                success: function (data) {
                    currentAjax = null;
                    $('#credit-result').html(data);
                    $('#count-result').text($('#count-credit').attr('data-text'));
                    $('#online-credit-input').attr('data-form-id','#credit-filter-form-large');
                    sort_asc();
                }
            });
        }
    });

     $('.iCheck-helper').on('click',function(e){
        
        let parent = $('#online-credit-input').parent('.icheckbox_minimal-blue');
        
        status = $(parent).attr('aria-checked');
        
        if(status == 'true') {
            input_status = 1;
        } else if (status == 'false'){
            input_status = 0;
        }

        e.preventDefault();
        
        let online_input = $('#online-credit-input').attr('data-form-id');
        
        form = $(online_input);
        
        let form_data = form.serialize()+'&type_filter=small';
        
        if(online_input == '#credit-filter-form-small'){
            form_data = form.serialize()+'&type_filter=small'+'&online_credit='+input_status; 
        }
        if(online_input == '#credit-filter-form-large'){
            form_data = form.serialize()+'&type_filter=large'+'&online_credit='+input_status; 
        }

        $('#progress-bar').show();
        var i = 0;
        if(i==0){
            i = 1;
            var elem = document.getElementById('bar');
            var width = 1;
            var id = setInterval(frame,90);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                    i = 0;
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }

        if(currentAjax){
            currentAjax.abort();
        }

        currentAjax = $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form_data,
            success:function (data) {
                currentAjax = null;
                $('#credit-result').html(data);
                $('#count-result').text($('#count-credit').attr('data-text'));

                $('#progress-bar').hide();
                clearInterval(id);
                sort_asc();
            }
        });
    });

    /*   $('.filter-credit').keyup(function(e) {
           e.preventDefault();
           $('#credit-result').html("<div class='card-body loader position-inherit bg-white border-r-8'><span></span><span></span><span></span></div>");
           setTimeout(function(){
               $.ajax({
                   method:'get',
                   url:"",
                   data: filter_form.serialize(),
                   success:function (data) {
                       $('#credit-result').html(data);
                   },
                   error:function(){
                       alert('error');
                   }
               });
           },3000);
       });*/

        $('.select2-selection').focus(function () {
            $('#first-compensation-small').attr('data-focus',true);
            $('#f-el-date-small').attr('data-focus',true);
        });  

        $('.on-change-filter').on('change',function(e) {

        if($(this).attr('data-focus') == 'true'){
    
            e.preventDefault();
            
            if(currentAjax){
                currentAjax.abort();
                $('#credit-result').remove('.loader');
            }
             $('#credit-result').html('<div class="loader"><div class="ajax-preloader loader__wrap"><svg class="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 270.93 275.47"><g><path fill="#0876a2" d="M213.35,167.42q0,20.42-13.22,34A43.37,43.37,0,0,1,167.83,215H59.6V120.27H92.31v59.62h75.75q12.57,0,12.58-12.52v-55q0-12.53-12.57-12.52H59.6V64.75H167.83a43.41,43.41,0,0,1,32.3,13.56q13.22,13.56,13.22,34Z" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M258.76,0H11.7a.56.56,0,0,0-.51.41.58.58,0,0,0,.13.48l24.35,26H234.8l24.35-26a.58.58,0,0,0,.13-.48.56.56,0,0,0-.52-.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M11.7,275.47H258.76a.54.54,0,0,0,.52-.41.55.55,0,0,0-.13-.47l-24.35-26H35.67l-24.36,26a.59.59,0,0,0-.12.47.54.54,0,0,0,.51.41" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M273.14,262.22V15.16a.54.54,0,0,0-.41-.52.55.55,0,0,0-.47.13l-26,24.35V238.25l26,24.36a.59.59,0,0,0,.47.12.54.54,0,0,0,.41-.51" transform="translate(-2.22 0)"/><path fill="#0876a2" d="M2.22,14.16V261.22a.54.54,0,0,0,.41.51.59.59,0,0,0,.47-.12l26-24.36V38.12L3.1,13.77a.55.55,0,0,0-.47-.13.54.54,0,0,0-.41.52"/></g></svg><svg class="progress" width="120" height="120" viewBox="0 0 120 120"><circle class="progress__value" cx="60" cy="60" r="50" fill="none" stroke="#fff" stroke-width="100" /></svg></div></div>');
            
            currentAjax = $.ajax({
                type: filter_form.attr('method'),
                url:  filter_form.attr('action'),
                data: filter_form.serialize()+'&type_filter=small',
                success: function (data) {
                    currentAjax = null;
                    $('#credit-result').html(data);
                    $('#count-result').text($('#count-credit').attr('data-text'));
                    $('#online-credit-input').attr('data-form-id','#credit-filter-form-small');
                    sort_asc();
                },
            });
            
        }
       

    });

function sort_desc(){
    $('#credit-result .item-section').sort(div_desc).appendTo('#credit-result');
    if($('#sort-percent').attr('data-sort') == 'asc'){
        $('#sort-percent').toggleClass('fa-sort-amount-asc').toggleClass('fa-sort-amount-desc');
        $('#sort-percent').attr('data-sort','desc')
    }
}

    function sort_asc(){
        $('#credit-result .item-section').sort(div_asc).appendTo('#credit-result');
        
        if($('#sort-percent').attr('data-sort') == 'desc'){
            $('#sort-percent').toggleClass('fa-sort-amount-desc').toggleClass('fa-sort-amount-asc');
            $('#sort-percent').attr('data-sort','asc')
        }
    }

   function div_desc(a,b){ 
        return ($(b).data('percent')) > ($(a).data('percent')) ? 1 : -1;
   }
   function div_asc(a,b){
        return ($(b).data('percent')) < ($(a).data('percent')) ? 1 : -1;
   }

});