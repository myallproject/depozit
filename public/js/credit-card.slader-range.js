
rageInputSmall('myRange','f-el-price','max_sum-error');
    
function backgroundColor(rangeId, inputId){
    let all =  $('#'+rangeId).attr('max');
    let piece = $('#'+inputId).val();
    if($.isNumeric(piece)){
      piece_percent = (parseInt(piece) * 100 ) / parseInt(all);
    } else {
      piece_percent = 0;
    }
    $('#'+rangeId).css('background','linear-gradient(90deg, #2196f3 '+piece_percent+'%, #00ffff00 0%)');
}
 
function rageInputSmall (rangeId, inputId, errorId=false){
    var slider = document.getElementById(rangeId);
    var output = document.getElementById(inputId);
    if(slider.value == 0){
      output.value = '';

    } else {
      output.value = slider.value;
    }

    backgroundColor(rangeId,inputId);

    slider.oninput = function() {
      if(this.value == 0){
        output.value = '';
      } else{
        output.value = this.value;
          
        backgroundColor(rangeId,inputId);

        if($.isNumeric(output.value)){
            $('#'+inputId).attr('data-validate',true);
            $('#'+errorId).hide();
          } else {
            $('#'+inputId).attr('data-validate',false);
          }
      }
    }
  
    $('#'+inputId).on('keyup',function(){
      let all =  $('#'+rangeId).attr('max');
      backgroundColor(rangeId,inputId);

      if(!$(this).val() || !$.isNumeric($(this).val())){
        $('#'+rangeId).val(0).trigger('change');
         backgroundColor(rangeId,inputId);
      } else {
        if($(this).val() > parseInt(all)){
          $(this).val(all);
          $('#'+rangeId).val($(this).val()).trigger('change');
        } else {
          $('#'+rangeId).val($(this).val()).trigger('change');
        }
      }
      if($.isNumeric($(this).val())){
        $(this).attr('data-validate',true);
        $('#'+errorId).hide();
      } else {
        $(this).attr('data-validate',false);
      }
      if(!$(this).val()){
      }
    });
  }