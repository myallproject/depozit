$(document).ready(function(){

    $('#orders-done-form').submit(function(e){
        e.preventDefault();
        var form = $(this);
        var selected = new Array();
        $("#orders-table input[type=checkbox]:checked").each(function () {
            selected.push(this.value);
        });
        ids = selected.join(',');

        var isDone = confirm("Bajarilganlarga qo'shmoqchmisiz?");
        if(isDone){
            $.ajax({
                url: form.attr('action'),
                method: form.attr('method'),
                data: {ids:ids}
            }).done(function(data){
                $('#orders-table').html(data)
            });
        }
    });
});