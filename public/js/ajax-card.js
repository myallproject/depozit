$('.add-comparison-debit').on('click',function(){
    $(this).toggleClass('added-to-balance');
    let status = $(this).attr('data-status');
    let id = $(this).data('id');

    if(status == false || status == 'false'){
        $(this).attr('data-status',true);
        $.ajax({
            url: $(this).attr('data-add-route'),
            data: {
                id: id,
                url_type: 'card_debit'
            },
            success:function(data){
                $(".comparison-amount").text(data);
            }
        });
    }
    if(status == "true") {
        $(this).attr('data-status', false);
        $.ajax({
            url: $(this).attr('data-remove-route'),
            data: {
                id: id,
                url_type: 'card_debit'
            },
            success:function(data){
                $(".comparison-amount").text(data);
            }
        });
    }
});

$('.btn-others-cards').on('click',function(){
    let status_btn =  $(this).attr('data-click-status');
    let this_id = $(this).attr('data-this-id');
    let bank_id = $(this).attr('data-bank-id');
    let form = $('#small-search-form');
    let filter = $(this).attr('data-filter-type');
    let open_content = $('#open-content');
    if(filter == 'small'){
         form = $('#small-search-form');
    }
    if(filter == 'large'){
         form = $('#large-search-form');
    }
    if(status_btn == 'false') {
        $.ajax({
            type: "GET",
            url: $(this).attr('data-list-url'),
            data: form.serialize()+'&bank_id='+bank_id+'&this_id='+this_id,
            success:function(data){
                $('#others-card-content-'+this_id).html(data);
            }
        });
        $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
        $(this).attr('data-click-status','true');
        $('#others-card-content-'+this_id).slideToggle(500);
        if($(open_content).attr('data-status') == 'true'){
            let content = $(open_content).attr('data-content-id');
            $('#'+content).attr('data-click-status','false');
            $('#others-card-content-'+$('#'+content).attr('data-this-id')).slideToggle(500);
            $('#'+content).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
            $(open_content).attr('data-content-id',$(this).attr('id'));
        } else if ($(open_content).attr('data-status') == 'false'){
            $(open_content).attr('data-status','true');
            $(open_content).attr('data-content-id',$(this).attr('id'));
        }
    }
    if(status_btn == 'true') {
        $(open_content).attr('data-status','false');
        $(open_content).attr('data-content-id','');
        $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
        $(this).attr('data-click-status','false');
        $('#others-card-content-'+this_id).slideToggle(500);
    }
});