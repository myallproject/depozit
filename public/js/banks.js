$('.search-banks').on('click',function(){
    $.ajax({
        method: $('#search-input').attr('data-method'),
        url: $('#search-input').attr('data-url'),
        data: {
            bank_name: $('#search-input').val()
        },success:function(data){
            $('#result-search-bank').html(data);
        },error:function(){
            console.log()
        }
    });
});

$('#search-input').on('keyup',function(){
    if($(this).val().length >=3 ){
        $.ajax({
            method: $('#search-input').attr('data-method'),
            url: $('#search-input').attr('data-url'),
            data: {
                bank_name: $('#search-input').val()
            }
        }).done(function(data){
            $('#result-search-bank').html(data);
        }); 
    } 
    if($(this).val().length == 0){
        $.ajax({
            method: $('#search-input').attr('data-method'),
            url: $('#search-input').attr('data-url'),
            data: {
                bank_name: $('#search-input').val()
            }
        }).done(function(data){
            $('#result-search-bank').html(data);
        }); 
    } 
});

$('body').on('click', '.bank-object .btn--arrow', function (e) {
    e.preventDefault();
    var parent = $(this).parent().parent().parent().parent();
    $(this).toggleClass('active');
    parent.parent().toggleClass('opened');
    parent.next().slideToggle(300);
});