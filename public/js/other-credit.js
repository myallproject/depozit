$('.add-comparison-other').on('click',function(){
    $(this).toggleClass('added-to-balance');
    let status = $(this).attr('data-status');
    let id = $(this).data('id');

    if(status == false || status == 'false'){
        $(this).attr('data-status',true);
        $.ajax({
            url: $(this).attr('data-add-route'),
            data: {
                id: id,
                url_type: 'credit'
            },
            success:function(data){
                $(".comparison-amount").text(data);
            }
        });
    }

    if(status == "true") {
        $(this).attr('data-status', false);
        $.ajax({
            url: $(this).attr('data-remove-route'),
            data: {
                id: id,
                url_type: 'credit'
            },
            success:function(data){
                $(".comparison-amount").text(data);
            }
        });
    }
});

$('[data-toggle="tooltip"]').tooltip({
    track: true
});

$.fn.extend({
    toggleText:function(a,b){
        return this.text(this.text() == b ? a :b);
    }
});
$('.btn-more-info').on('click',function(){
    $(this).find('span').toggleText("@lang('lang.detailed')","@lang('lang.close')");
    $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
    $('#content-more-information-'+$(this).attr('data-id')).slideToggle(500);
});

$('.show-info-content-one').click(function(){
    id = $(this).attr('data-id');
    $('#content-one-'+id).show();
    $('#content-two-'+id).hide();
    $(this).addClass('tab-active-btn');
    $('#show-info-content-two-'+id).removeClass('tab-active-btn')

});
$('.show-info-content-two').click(function(){
    id = $(this).attr('data-id');
    $('#show-info-content-one-'+id).removeClass('tab-active-btn');
    $(this).addClass('tab-active-btn');
    $('#content-one-'+id).hide();
    $('#content-two-'+id).show();
});
$('.btn-default').click(function(){
    $(this).toggleClass('btn-default-active');
});