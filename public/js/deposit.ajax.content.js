$('.add-comparison').on('click',function(){
    $(this).toggleClass('added-to-balance');
    let status = $(this).attr('data-status');
    let id = $(this).data('id');

    if(status == false || status == 'false'){
        $(this).attr('data-status',true);
        $.ajax({
            url:  $(this).attr('data-add-route'),
            data: {
                id: id,
                url_type: 'deposit'
            },
            success:function(data){
                $(".comparison-amount").text(data);
            }
        });
    }
    if(status == "true") {
        $(this).attr('data-status', false);
        $.ajax({
            url: $(this).attr('data-remove-route'),
            data: {
                id: id,
                url_type: 'deposit'
            },
            success:function(data){
                $(".comparison-amount").text(data);
            }
        });
    }
});

$('[data-toggle="tooltip"]').tooltip({
    track: true
});

$.fn.extend({
    toggleText:function(a,b){
        return this.text(this.text() == b ? a :b);
    }
});

$('.btn-more-info').on('click',function(){
    $(this).find('span').toggleText($(this).attr('data-text-detailed'),$(this).attr('data-text-close'));
    $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
    $('#content-more-information-'+$(this).attr('data-id')).slideToggle(500);
});

$('.show-info-content-one').click(function(){
    id = $(this).attr('data-id');
    $('#content-one-'+id).show();
    $('#content-two-'+id).hide();
    $('#content-three-'+id).hide();
    $(this).addClass('tab-active-btn');
    $('#show-info-content-two-'+id).removeClass('tab-active-btn');
    $('#show-info-content-three-'+id).removeClass('tab-active-btn');
});

$('.show-info-content-two').click(function(){
    id = $(this).attr('data-id');
    $('#show-info-content-one-'+id).removeClass('tab-active-btn');
    $('#show-info-content-three-'+id).removeClass('tab-active-btn');
    $(this).addClass('tab-active-btn');
    $('#content-one-'+id).hide();
    $('#content-two-'+id).show();
    $('#content-three-'+id).hide();

});

$('.show-info-content-three').click(function(){
    id = $(this).attr('data-id');
    $('#show-info-content-one-'+id).removeClass('tab-active-btn');
    $('#show-info-content-two-'+id).removeClass('tab-active-btn');
    $(this).addClass('tab-active-btn');
    $('#content-one-'+id).hide();
    $('#content-two-'+id).hide();
    $('#content-three-'+id).show();

});

$('.btn-default').click(function(){
    $(this).toggleClass('btn-default-active');
});


$('.btn-others-deposits').click(function(){
    let status_btn =  $(this).attr('data-click-status');
    let this_id = $(this).attr('data-this-id');
    let bank_id = $(this).attr('data-bank-id');
    let filter = $(this).attr('data-filter-type');
    let count = $(this).attr('data-count');
    let open_content = $('#open-content');
    if(parseInt(count,10) > 0) {

        if(filter == 'small'){
            let form = $('#small-search-form');
             if(status_btn == 'false') {
                /*$('#others-deposits-content-'+this_id).load('loader');*/
                $.ajax({
                    type: "GET",
                    url: $(this).attr('data-dp-list-url'),
                    data: form.serialize()+'&type_filter='+$(this).attr('data-filter-type')+'&bank_id='+bank_id+'&this_id='+this_id,
                    success:function(data){
                        $('#others-deposits-content-'+this_id).html(data);
                        if($('#sort-percent').attr('data-sort') == 'desc'){
                            $('#others-deposits-content-'+this_id+' .item-section').sort(other_div_desc).appendTo('#others-deposits-content-'+this_id);
                        }if($('#sort-percent').attr('data-sort') == 'asc'){
                            $('#others-deposits-content-'+this_id+' .item-section').sort(other_div_asc).appendTo('#others-deposits-content-'+this_id);
                        }
                    }
                });

                $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
                $(this).attr('data-click-status','true');
                $('#others-deposits-content-'+this_id).slideToggle(500);
                if($(open_content).attr('data-status') == 'true'){
                   let content = $(open_content).attr('data-content-id');
                   $('#'+content).attr('data-click-status','false');
                   $('#others-deposits-content-'+$('#'+content).attr('data-this-id')).slideToggle(500);
                   $('#'+content).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
                   $(open_content).attr('data-content-id',$(this).attr('id'));
                } else if ($(open_content).attr('data-status') == 'false'){
                   $(open_content).attr('data-status','true');
                   $(open_content).attr('data-content-id',$(this).attr('id'));
                }
            }
        }
        if(filter == 'large'){
            let form = $('#large-search-form');
           if(status_btn == 'false') {
                // $('#others-deposits-content-'+this_id).load('loader');
                $.ajax({
                    type: "GET",
                    url: $(this).attr('data-dp-list-url'),
                    data: form.serialize()+'&type_filter='+$(this).attr('data-filter-type')+'&bank_id='+bank_id+'&this_id='+this_id,
                    success:function(data){
                        $('#others-deposits-content-'+this_id).html(data);
                        if($('#sort-percent').attr('data-sort') == 'desc'){
                            $('#others-deposits-content-'+this_id+' .item-section').sort(other_div_desc).appendTo('#others-deposits-content-'+this_id);
                        }if($('#sort-percent').attr('data-sort') == 'asc'){
                            $('#others-deposits-content-'+this_id+' .item-section').sort(other_div_asc).appendTo('#others-deposits-content-'+this_id);
                        }

                    }
                });

                $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
                $(this).attr('data-click-status','true');
                $('#others-deposits-content-'+this_id).slideToggle(500);
               if($(open_content).attr('data-status') == 'true'){
                   let content = $(open_content).attr('data-content-id');
                   $('#'+content).attr('data-click-status','false');
                   $('#others-deposits-content-'+$('#'+content).attr('data-this-id')).slideToggle(500);
                   $('#'+content).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
                   $(open_content).attr('data-content-id',$(this).attr('id'));
               } else if ($(open_content).attr('data-status') == 'false'){
                   $(open_content).attr('data-status','true');
                   $(open_content).attr('data-content-id',$(this).attr('id'));
               }
            }
        }
    }

    if(status_btn == 'true') {
        $(open_content).attr('data-status','false');
        $(open_content).attr('data-content-id','');
        $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
        $(this).attr('data-click-status','false');
        $('#others-deposits-content-'+this_id).slideToggle(500);
    }
});



$('.btn-more-info-child').on('click',function(){
    $(this).find('span').toggleText($(this).attr('data-text-detailed'),$(this).attr('data-text-close'));
    $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
    $('#content-more-information-'+$(this).attr('data-id')).slideToggle(500);
});

$('.show-info-content-one-child').click(function(){
    id = $(this).attr('data-id');
    $('#content-one-'+id).show();
    $('#content-two-'+id).hide();
    $(this).addClass('tab-active-btn');
    $('#show-info-content-two-'+id).removeClass('tab-active-btn')

});
$('.show-info-content-two-child').click(function(){
    id = $(this).attr('data-id');
    $('#show-info-content-one-'+id).removeClass('tab-active-btn')
    $(this).addClass('tab-active-btn');
    $('#content-one-'+id).hide();
    $('#content-two-'+id).show();
});

$('.btn-default').click(function(){
    $(this).toggleClass('btn-default-active');
});

function other_div_desc(a,b){ 
   return ($(b).data('percent')) > ($(a).data('percent')) ? 1 : -1;
}
function other_div_asc(a,b){
    return ($(b).data('percent')) < ($(a).data('percent')) ? 1 : -1;
}