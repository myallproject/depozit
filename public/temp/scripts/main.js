(function () {
    new Swiper('.min-banner .swiper-container', {
        slidesPerView: 1,
        speed: 1500,
        loop: true,
        loopedSlides: 1,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        keyboard: {
            enabled: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    $('.select2__js').select2({
        //closeOnSelect : false,
        placeholder: $('.any-text').attr('data-text'),
        width: '100%',
    });

    let search = $('.select2__js');
    $.each($(search),function(i,v){
        if( $(this).attr('data-dropdown') == 'open'){
            $(this).select2({
                closeOnSelect : false,
                placeholder: $(this).attr('data-empty'),
                width: '100%',
            });
        } else {
            $(this).select2({
                placeholder: $(this).attr('data-empty'),
                width: '100%',
            });
        }
    });

    $('.select2__js').on("select2:select", function(e){
        var id = e.params.data.id;

        if (id == 'any') {
            $(this).val(null).trigger('change');
            $(this).select2('close');
        }
    });

    /*if ($('.select2__js').length > 0) {
        $('.select2__js').select2({
            closeOnSelect : false,
            placeholder: 'Select',
            width: '100%',
        })
    }
    */
    /*$('.select2__js').select2({
        placeholder: 'Select',
        width: '100%',
        //allowClear: true
    });*/
    $('[data-modal]').click(function (e) {

        e.preventDefault();

        $('body').addClass('modal-open');

        var value = $(this).attr('data-modal');

        $(`[data-modal-name = ${value}]`).fadeIn(200).addClass('show');

    });

    $('.modal-ui').click(function (e) {

        console.log(e.target.className);

        if(e.target.className === 'modal-ui show'){

            $('[data-modal-name]').fadeOut(200).removeClass('show');

            $('body').removeClass('modal-open');

        }

    });

    $('.modal-ui .modal-ui__close-btn').click(function () {

        $('[data-modal-name]').fadeOut(200).removeClass('show');

        $('body').removeClass('modal-open');

    });

    /*FILTER RESULT*/

    $('body').on('click', '.filter-item .btn--arrow', function (e) {
        e.preventDefault();

        var parent = $(this).parent().parent();

        $(this).toggleClass('active');

        parent.parent().find('.aktsiya-tab').removeClass('active');
        parent.parent().find('.main-tab').addClass('active');

        parent.parent().toggleClass('opened');

        parent.next().slideToggle(300);

    });

    $('body').on('click', '.filter-item .promotion', function (e) {
        e.preventDefault();

        var parent = $(this).parent().parent().parent();

        parent.parent().find('.active').removeClass('active');
        parent.parent().find('.aktsiya-tab').addClass('active');

        parent.parent().toggleClass('opened');

        parent.next().slideToggle(300);

    });

    $('body').on('click', '.tr-more .tab-nav a', function (e) {

        e.preventDefault();

        var parent = $(this).parent().parent();

        parent.find('.active').removeClass('active');

        $(this).addClass('active');

        parent.find('.tab-pane').eq($(this).index()).addClass('active');

    });

    const nav = $('.tab-nav');

    if (nav.length > 0) {

        nav.each(function () {

            var nav = $(this),
                caption = nav.next();

            nav.find('a').first().addClass('active');
            caption.find('.tab-pane').first().addClass('active');

            nav.find('a').click(function (e) {

                e.preventDefault();

                var link = $(this);

                nav.find('.active').removeClass('active');

                caption.find('.active').removeClass('active');

                link.addClass('active');

                caption.find('.tab-pane').eq(link.index()).addClass('active')

            });

        });

    }

    $('.search-icon--toggle').click(function () {

      $(this).closest('.header__search').toggleClass('open')

    });

  $('.menu-toggle').click(function () {

    $('#navbar-toggle').find('ul').addClass('open')


  });


  $('.header__nav .close-icon').click(function () {

    $(this).parent().parent().removeClass('open')

  })

})();

