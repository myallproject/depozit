<?php

return [
    'name' => 'Kompaniya nomi',
    'share_cost' => 'Bitta aksiya bahosi',
    'changes' => 'Kunlik o\'zgarish',
    'dividend' => 'Dividend',
    'action' => 'Harakatlar',
    'shares' => 'Aksiyalar',
    'branches' => 'Sanoat',
    'local' => 'O\'zbekiston',
    'foreign' => 'Xorijiy',
    'buy' => 'Sotib olish',
    'surname' => 'Familayngiz',
    'email' => 'Elektron pochta',
    'phone' => 'Telefon',
    'cancel' => 'Bekor qilish',
    'request' => 'J\'onatish'
];
