<?php

return  [
		'home' => [
			'title' => 'O\'zbekistondagi eng foydali ssudalar va omonatlar',
			'keywords' => 'foydali kredit, depozit, valyuta kursi',
			'description' => '30 dan ortiq banklar. O\'zingiz uchun eng yaxshi kredit, omonat, mikrokredit va valyuta kursini tanlang. Biz eng yaxshi banklar bilan hamkorlik qilamiz. Kiring va taqqoslang.' 
		],
		'deposits' => [
			'title' => 'O\'zbekistonda omonat oching. Toshkentdagi eng yaxshi sharoitlar',
			'keywords' => 'Omonatlar, bank omonatlari, dipazit Toshkent',
			'description' => 'Siz tanlagan 200 ta omonat. Biz sizga eng yaxshi bankni va munosib omonatni tanlashda yordam beramiz. Eng yaxshi qiziqish bilan ishonchli hissa. Kiring va o\'zingizni ko\'ring.' 
		],
		'deposit' => [
			'title' => '',
			'keywords' => '',
			'description' => '' 
		],
		'credit' => [
			'title' => '',
			'keywords' => '',
			'description' => '' 
		],
		'credits' => [
			'title' => '',
			'keywords' => '',
			'description' => '' 
		],
		'credit_card' => [
			'title' => 'O\'zbekistonda kredit kartalari. O\'zingiz uchun kredit kartasini tanlang',
			'keywords' => 'bank kredit kartalari, kredit kartalari, bank kredit kartalari',
			'description' => '15 ta kredit tanlab olindi, shundan o\'zingizga eng mos keladigan kredit kartasini olishingiz mumkin. Bizning veb-saytimizga keling va tanlang' 
		],
		'debit_card' => [
			'title' => 'O\'zbekistonda to\'lov kartasini oling. Eng yaxshi plastik kartalarga ega bo\'lgan bankni tanlang',
			'keywords' => 'O\'zbekistonda debet kartalari, plastik kartalar',
			'description' => 'Turli xil shartlarga ega 81 debet karta, ular orasida sizning ham bor! Saytga tashrif buyuring va biz sizga mos keladigan eng yaxshi kartani tanlashda sizga yordam beramiz!' 
		],
		'about_us' => [
			'title' => 'Depozit.uz sayti. Bizning faoliyatimiz tafsilotlari',
			'keywords' => 'Deposit.uz haqida batafsil ma\'lumot, depozit.uz sayti haqida, depozit.uz haqida ma\'lumot',
			'description' => 'Depozit.uz - bu odamlarga to\'g\'ri qaror qabul qilishga yordam beradigan qulay xizmatlar va maslahatlar beradigan moliyaviy supermarket.' 
		],
		'contact' => [
			'title' => 'Depozit.uz | Biz bilan bog\'lanish',
			'keywords' => 'omonat haqida ma\'lumot oling, Toshkentdagi kredit haqida ma\'lumot oling, qaerdan kredit olish kerak',
			'description' => 'Ushbu sahifada siz biz bilan bog\'lanishingiz, qiziqtirgan har qanday savolingizni berishingiz mumkin va bizning menejerlarimiz sizga yaqin kelajakda javob berishadi.' 
		],
		'search' => [
			'title' => 'Sizni qiziqtirgan ma\'lumotni bizning veb-saytimizda qidiring',
			'keywords' => 'Toshkentdagi omonatlar, O\'zbekistondagi kreditlar, Toshkentdagi valyuta kurslari',
			'description' => 'Bu erda siz kerakli ma\'lumotni bir marta bosishingiz mumkin va qo\'lda qidirishga vaqt sarflamaysiz. Sizning vaqtingiz haqida qayg\'uramiz.' 
		],
		'comparison' => [
			'title' => '',
			'keywords' => '',
			'description' => '' 
		],
		'cb_rate' => [
			'title' => 'Turli banklarni taqqoslaganda eng yaxshi almashuv kurslari',
			'keywords' => 'ayirboshlash kursi, O‘zbekistondagi ayirboshlash kursi, bugungi kun uchun kurs',
			'description' => 'Bu erda siz O\'zbekiston Respublikasi Markaziy banki va boshqa banklarda valyuta kurslarini topishingiz, o\'zingiz uchun qulay bo\'lgan bankni tanlashingiz mumkin' 
		],
		'bank_rate' => [
			'title' => 'Turli banklarni taqqoslaganda eng yaxshi almashuv kurslari',
			'keywords' => 'ayirboshlash kursi, O‘zbekistondagi ayirboshlash kursi, bugungi kun uchun kurs',
			'description' => 'Bu erda siz O\'zbekiston Respublikasi Markaziy banki va boshqa banklarda valyuta kurslarini topishingiz, o\'zingiz uchun qulay bo\'lgan bankni tanlashingiz mumkin' 
		],
		'bank' => [
			'title' => ' O\'zbekistondagi eng yaxshi kreditlar, omonatlar, foizlar va takliflar',
			'keywords' => ' Toshkent banklari, Toshkent kreditlari, O\'zbekiston banklari, O\'zbekistondagi omonatlar',
			'description' => 'Eng yaxshi narxlar! Omonatlar, kreditlar, mikrokreditlar, ipoteka kreditlari va boshqa ko\'p narsalar! Kiring va tanlang. Biz siz uchun taklifni shaxsan tanlaymiz!.' 
		],
		'banks' => [
			'title' => 'O\'zbekiston banklari. O\'zbekistonda ixcham xizmatlar va ma\'lumotlar ro\'yxati',
			'keywords' => 'valyuta kursi O\'zbekistondagi, bugunga kelib rasmiy kurslar',
			'description' => 'O\'zbekistonda banklar, ularning manzillari va telefon raqamlari. O\'zbekistonning xorijiy, xususiy va davlat banklari bizning veb-saytimizda. Kiring va tanlang.' 
		],
		'news' => [
			'title' => 'O\'zbekiston banklarining so\'nggi yangiliklari. Foydali taklif!',
			'keywords' => 'O\'zbekiston banklari yangiliklari, banklar yangiliklari, O\'zbekiston banklari yangiliklari',
			'description' => 'O\'zbekistonning barcha banklari yangiliklari bitta to\'plamda to\'planadi. Baxtsiz hodisalar va eng yaxshi takliflar va so\'nggi o\'zgarishlar haqida bilib oling.' 
		],
		'all_news' => [
			'title' => 'O\'zbekiston banklarining so\'nggi yangiliklari. Foydali taklif!',
			'keywords' => 'O\'zbekiston banklari yangiliklari, banklar yangiliklari, O\'zbekiston banklari yangiliklari',
			'description' => 'O\'zbekistonning barcha banklari yangiliklari bitta to\'plamda to\'planadi. Baxtsiz hodisalar va eng yaxshi takliflar va so\'nggi o\'zgarishlar haqida bilib oling.' 
		],
		'micro_credits' => [
			'title' => 'O\'zbekistonda mikrokreditlar. Mikrokreditlar uchun eng yaxshi sharoitlar',
			'keywords' => 'mikrokredit, mikrokredit, mikrokreditlar onlayn',
			'description' => 'O\'zbekistonning turli banklarida mikrokredit berishning 42 varianti tanlandi. Sizga eng mos mikrokreditni tanlashda yordam beramiz. Kiring va siz pushaymon bo\'lmaysiz.' 
		],
		'auto_credits' => [
			'title' => 'O\'zbekistonda avtomobil kreditlari. Avtomobil kreditini tanlang',
			'keywords' => 'avtoulov krediti, avtoulov krediti 2020, O\'zbekiston avtokredit krediti',
			'description' => 'Siz tanlagan turli xil avtomobil kreditlari! Biz uchun va Zaxodi men uchun orzu qilingan avtomobil uchun eng yaxshi daromadli xaklarga avtoulov kreditini tanladi.' 
		],
		'consume_credits' => [
			'title' => 'O\'zbekistonda iste\'mol kreditlari. Sizga baxtli bo\'lishga yordam beramiz',
			'keywords' => 'Iste\'mol krediti, Toshkent shahridagi iste\'mol krediti, iste\'mol krediti banklari',
			'description' =>'Biz O\'zbekistonning turli banklarining 28 ta taklifidan munosib iste\'mol kreditini tanlaymiz. Eng qulay kreditlarni biz bilan topish mumkin!' 
		],
		'mortgage_credits' =>[
			'title' => 'O\'zbekistonda ipoteka kreditini oling. Eng yaxshi ipoteka.',
			'keywords' => 'banklar ipoteka, ipoteka, kredit ipotekasi',
			'description' =>'26 ta taklif orasida eng foydali ipoteka. Turli xil shartlarga ega bo\'lgan O\'zbekistondagi ipotekalar, tanlang va yashang. Bizning sahifamizga tashrif buyuring.' 
		],
		'edu_credits' => [
			'title' => 'Ta\'lim kreditlari. O\'zbekistonda ta\'lim krediti',
			'keywords' => 'Talaba krediti, talaba krediti, talaba krediti',
			'description' => 'O\'zbekistonda ta\'lim kreditlari. Ta\'lim kreditini olish juda oson, ayniqsa tanlov juda ko\'p bo\'lsa. Bizga keling va ishonch hosil qiling.' 
		],
		'bank_services' => [
			'title' => '',
			'keywords' => '',
			'description' => '' 
		],
		'overdraft_credit' => [
			'title' => 'Overdraft',
			'keywords' => '',
			'description' => '' 
		],
];