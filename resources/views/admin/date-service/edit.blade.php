@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('date-type-list') }}">
            <i class="fa fa-calendar"></i>
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="">
            {!! !empty($date_type) ? trans('lang.edit') : trans('lang.add_new') !!}
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{!! !empty($date_type) ? trans('lang.edit') : trans('lang.add_new') !!}</h5>
                                    @if($date_type)
                                        <div class="card-header-right">
                                            <a href="{{ route('date-type-add-view') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-12">
                                    @if($date_type)
                                        @include('admin.date-service.form.update')
                                    @else
                                        @include('admin.date-service.form.add')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#menu-addition').addClass('active pcoded-trigger');
            $('#menu-date-type').addClass('active');


            $('#parent-service-select').on('change',function(){
                /*2 kreditlar, 3 kartalar*/
                if($(this).val() == '2' || $(this).val() == '3'){
                    $('#child-service-select-div').show();
                    $('#child-service-select').attr('required','required');
                    $.ajax({
                        url:'{{ route("select-credit-children") }}',
                        type: 'GET',
                        data:{id:$(this).val(),type:'add',id_child:''},
                        success:function(data){
                            $('#child-service-select').html(data)
                        }
                    });
                } else {
                    $('#child-service-select-div').hide();
                    $('#child-service-select').removeAttr('required');
                }
            });
            setTimeout(function(){
                if($('#parent-service-select').val() == '2' || $('#parent-service-select').val() == '3'){
                    $('#child-service-select-div').show();
                    $('#child-service-select').attr('required','required');
                    $.ajax({
                        url:'{{ route("select-credit-children") }}',
                        type: 'GET',
                        data:{
                            id:$('#parent-service-select').val(),
                            type:'update',
                                id_child: $('#parent-service-select').attr('data-select-option')},
                        success:function(data){
                            $('#child-service-select').html(data)
                        }
                    });

                } else {

                    $('#child-service-select-div').hide();
                    $('#child-service-select').removeAttr('required');
                }

            },2000);

        });
    </script>
@endsection
