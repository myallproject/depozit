@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#!">@lang('lang.date_types')</a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.date_types')</h5>
                                    <div class="card-header-right">
                                        <a href="{{ route('date-service-status-change-all',[1]) }}" class="btn waves-effect waves-light all-status-btn hor-grd pl-2 p-1  pr-3 btn-grd-success" data-status="1">All active</a>
                                        <a href="{{ route('date-service-status-change-all',[0]) }}" class="btn waves-effect waves-light all-status-btn hor-grd pl-2 p-1  pr-3 btn-grd-danger" data-status="0">All block</a>
                                        <a class="btn btn-info p-1 text-white pl-2 pr-3" href="{{ route('date-type-add-view') }}">
                                            <i class="fa fa-plus-circle"> </i>
                                        @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table id="date-table" class="table-responsive-xl table-borderless table table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('lang.name') Uz</th>
                                            <th>@lang('lang.name') Ru</th>
                                            <th>@lang('lang.service')</th>
                                            <th>@lang('lang.position')</th>
                                            <th>@lang('lang.service') sub</th>
                                            <th>@lang('lang.status')</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($list as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td>{{ $row->name_uz }}</td>
                                                <td>{{ $row->name_ru }}</td>
                                                <td>{{ $row->service->getName() }}</td>
                                                <td >
                                                    <form id="position-date-{{ $row->id }}" action="{{ route('date-position') }}" method="POST" style="display: flex !important;">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $row->id }}">
                                                        <input type="text" name="position" class="form-control col-md-2" value="{{ $row->position }}">
                                                        <a onclick="position({{ $row->id }})" class="btn p-1 pl-2  btn-info">
                                                            <i class="fa fa-hand-o-up"></i>
                                                        </a>
                                                    </form>
                                                    <div id="alert-{{ $row->id }}" style="display: none; font-size: 10px" class="alert alert-success"></div>
                                                </td>
                                                <td>
                                                    @if($row->child_service_id)
                                                        {{ $row->serviceChild->name() }}
                                                    @endif
                                                </td>
                                                <td>
                                                    <select name="active" data-id="{{ $row->id }}" data-action="{{ route('date-service-status-change') }}" class="form-control change-status">
                                                       
                                                        <option @if($row->status == 1) selected @endif value="1">Active</option>
                                                        <option @if($row->status == 0) selected @endif value="0">Block</option>
                                                    </select>
                                                    <div id="status-{{ $row->id }}" style="display: none" class="alert alert-success"></div>
                                                </td>
                                                <td class="text-right">
                                                    <a href="{{ route('date-type-update-view',[$row->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('date-type-delete',[$row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-footer"></div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.delete').on('click', function() {
            return confirm("@lang('lang.confirmDel')");
        });
        $('.all-status-btn').on('click',function(){
            return confirm('Hamma muddatlarning statusi '+$(this).attr('data-status'))
        });
       $('#menu-addition').addClass('active pcoded-trigger');
       $('#menu-date-type').addClass('active');

       $(function () {
            $('#date-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });

     });

    function position(id){
        var frm = $('#position-date-'+id);

            $.ajax({
                method: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success:function(data){
                    $('#alert-'+data.id).text('Positsiya: '+data.position).show();
                    setTimeout(function(){$('#alert-'+data.id).hide();},5000);
                }
        });
    }

    $('.change-status').on('change',function(){
        $.ajax({
            method: 'GET',
            url: $(this).attr('data-action'),
            data:{
                'status':$(this).val(),
                'id': $(this).attr('data-id')
            }
        }).done(function(data){
            $('#status-'+data.id).text('Msg: '+data.msg+', Status:'+data.status+', Id:'+data.id).show();
            setTimeout(function(){$('#status-'+data.id).hide();},5000);
        });
    });
</script>
@endsection
