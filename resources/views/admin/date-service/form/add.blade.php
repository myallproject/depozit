<div class="row ml-5">
    <div class="col-sm-8 pt-4 ">
        <form action="{{ route('date-type-add') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.name')UZ</b></label>
                    <input required="required" type="text" name="name_uz" class="form-control form-control-lg">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.name') RU</b></label>
                    <input required="required" type="text" name="name_ru" class="form-control form-control-lg">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.description')</b></label>
                    <input type="text" name="description" class="form-control form-control-lg">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.service')</b></label>
                    <select type="text" name="service" class="form-control form-control-lg" required="required" id="parent-service-select">
                        <option value="">-- @lang('lang.choose') --</option>
                           @foreach($services as $row)
                               <option value="{{ $row->id }}"> {{ $row->getName() }}</option>
                           @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row" style="display: none" id="child-service-select-div" >
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.service')</b></label>
                    <select type="text" name="child_service" id="child-service-select" class="form-control form-control-lg">

                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-info ">
                        @lang('lang.save')
                    </button>
                </div>
            </div>
        </form>
    </div>

</div>

