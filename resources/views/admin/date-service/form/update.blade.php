<div class="row ml-5">
    <div class="col-sm-8 pt-4 ">
        <form action="{{ route('date-type-update') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ $date_type->id }}">
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.name') UZ</b></label>
                    <input required="required" type="text" value="{{ $date_type->name_uz }}" name="name_uz" class="form-control form-control-lg">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.name') RU</b></label>
                    <input required="required" type="text" name="name_ru" value="{{ $date_type->name_ru }}" class="form-control form-control-lg">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.description')</b></label>
                    <input type="text" name="description" value="{{ $date_type->description }}" class="form-control form-control-lg">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.service')</b></label>
                    <select type="text" name="service" class="form-control form-control-lg" required="required" id="parent-service-select" data-select-option="{{ $date_type->child_service_id }}">
                        <option value="">-- @lang('lang.choose') --</option>
                        @foreach($services as $row)
                            <option @if($date_type->service_id == $row->id) selected="selected" @endif value="{{ $row->id }}"> {{ $row->getName() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row" style="display: none" id="child-service-select-div" >
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.service')</b></label>
                    <select name="child_service" id="child-service-select" class="form-control form-control-lg" required="">
                        <option value=""></option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-info ">
                        @lang('lang.save')
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

