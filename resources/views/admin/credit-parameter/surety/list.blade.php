@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">
            <i class="fa fa-bank"></i>
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{{ Yt::trans('Kredit uchun kafillik','uz') }}</h5>
                                    <div class="card-header-right">
                                        <a id="add-new-btn" data-toggle="modal" data-target="#myModal" class="btn btn-info p-1 text-white pl-2 pr-3" href="">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table id="data-table" class="table-responsive-xl table-borderless table table-hover" style="font-size: 13px !important;">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('lang.name') UZ</th>
                                            <th>@lang('lang.name') RU</th>
                                            <th>Kredit turi</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($model as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td id="name-uz{{ $row->id }}">{{ $row->name_uz }}</td>
                                                <td id="name-ru{{ $row->id }}">{{ $row->name_ru }}</td>
                                                <td >{{ $row->serviceType->name() }}</td>

                                                <td class="text-right">
                                                    <a data-toggle="modal" data-target="#myModal" data-id="{{ $row->id }}" data-service-id="{{ $row->service_id }}" class="update btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('credit-surety-delete',[$row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form action="{{ route('credit-surety-edit') }}" method="POST">
                @csrf
                <input type="hidden" id="id" name="id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kredit turlari</label>
                                <select required="required" class="form-control form-control-lg" name="credit_type_id" id="credit-type-id">
                                    <option value="">@lang('lang.choose')</option>
                                    @foreach($creditType as $t_row)
                                        <option value="{{ $t_row->id }}">{{ $t_row->name() }}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>@lang('lang.name') UZ</label>
                                <input type="text" autocomplete="off" id="name-uz" name="name_uz" required class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>@lang('lang.name') RU</label>
                                <input type="text" autocomplete="off" id="name-ru" name="name_ru" required class="form-control">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning" data-dismiss="modal" >{{ Yt::trans('Yopish','uz') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });

            $('#menu-credit-parent').addClass('active pcoded-trigger');

            $('#add-new-btn').click(function(){
                $('#id').val('');
                $('#name-uz').val('');
                $('#name-ru').val('');
                $('#modal-title').text("@lang('lang.add_new')")
            });

            $('.update').click(function(){
                let id = $(this).attr('data-id');
                id_type = $(this).attr('data-service-id');

                $.ajax({
                    type:'GET',
                    url: "{{ route('credit-select-type') }}",
                    data: {
                        'id_type': id_type
                    },success:function(data){
                        $('#credit-type-id').html(data)
                    }
                });

                n_uz = $('#name-uz'+id).text();
                n_ru = $('#name-ru'+id).text();
                $('#name-uz').val(n_uz);
                $('#name-ru').val(n_ru);
                $('#id').val(id);
                $('#modal-title').text("@lang('lang.edit')")

            });
        });

        $(function () {
            $('#data-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });

    </script>
@endsection
