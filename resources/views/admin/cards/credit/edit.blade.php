@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="">@lang('lang.credit_cards')</a>
    </li>
@endsection
@section('content')

 <link rel="stylesheet" href="/plugins/dropify/dist/css/dropify.min.css">
	<div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                	    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5> @lang('lang.'.$edit.''). @lang('lang.credit_cards')</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-info p-1 text-white pl-2 pr-3" href="{{ route('credit-card-add-page') }}">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>

                                <div class="card-block">
                                    <div class="row pl-4">
                                        <div class="col-md-8">
                                            <form action="{{ route($route) }}" method="POST" enctype="multipart/form-data">
                                            
                                            	@csrf
                                                <input type="hidden" name="id" value="{{ $model['id'] }}">
                                                <div class="form-group">
                                                	<label><b>@lang('lang.bank')</b></label>
                                                	<select name="bank_id" required="required"  class="form-control">
                                                		<option value="">-@lang('lang.choose')-</option>
                                                		@foreach($banks as $bank)
                                                			<option value="{{ $bank->id }}" @if($model['bank_id'] == $bank->id) selected @endif>{{ $bank->name_uz }}</option>
                                                		@endforeach
                                                	</select>
                                                </div>
                                                <div class="form-group">
                                                	<label><b>@lang('lang.name') uz</b></label>
                                                	<input type="text" required name="name_uz" value="{{ $model['name_uz'] }}" class="form-control" />
                                                </div>

                                                <div class="form-group">
                                                	<label><b>@lang('lang.name') ru</b></label>
                                                	<input type="text" required name="name_ru" value="{{ $model['name_ru'] }}"  class="form-control"/>
                                                </div>

                                                <div class="form-group">
                                                	<label><b>@lang('lang.currency')</b></label>
                                                	<select name="currency_id" required="required"  class="form-control">
                                                		<option value="">-@lang('lang.choose')-</option>
                                                		@foreach($currencies as $currency)
                                                			<option value="{{ $currency->id }}" @if($model['currency_id'] == $currency->id) selected @endif>{{ $currency->name }}</option>
                                                		@endforeach
                                                	</select>
                                                </div>

                                                <div class="form-group">
                                                	<label><b>@lang('lang.date')</b></label>
                                                	<select required class="form-control" name='date_id' required>                                                	
                                                		<option value="">-@lang('lang.choose')-</option>
                                                		@foreach($dates as $date)
                                                			<option value="{{ $date->id }}" @if($model['date_id'] == $date->id) selected @endif>{{ $date->name() }}</option>
                                                		@endforeach
                                                	</select>
                                                </div>

                                                <div class="form-group">
                                                	<label><b>Imtiyozli davr uz</b></label>
                                                	<input type="text" required name="imtoyozli_davr_uz" value="{{ $model['imtoyozli_davr_uz'] }}" class="form-control">
                                                </div>

  												<div class="form-group">
                                                	<label><b>Imtiyozli davr ru</b></label>
                                                	<input type="text" required name="imtoyozli_davr_ru" value="{{ $model['imtoyozli_davr_ru'] }}" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                	<label><b>@lang('lang.percent')(raqamlar)</b></label>
                                                	<input type="text" required name="percent" value="{{ $model['percent'] }}" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                	<label for=""><b>Hisoblash usuli</b></label>
                                                	<input type="text" name="hisoblash_usul" value="{{ $model['hisoblash_usul'] }}" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                	<label><b>max sum (raqamlar)</b></label>
                                                	<input name='max_sum' required="required" value="{{ $model['max_sum'] }}" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                	<label><b>max sum matn uz</b></label>
                                                	<input name='max_sum_text_uz' required="required" value="{{ $model['max_sum_text_uz'] }}" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                	<label><b>max sum matn ru</b></label>
                                                	<input name='max_sum_text_ru' required="required" value="{{ $model['max_sum_text_ru'] }}" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                	<label><b>kurish_muddati</b></label>
                                                	<input name='kurish_muddati' required="required" value="{{ $model['kurish_muddati'] }}" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                	<label><b>documents_uz</b></label>
                                                	<textarea class="form-control" rows="6" name="documents_uz" >{{ $model['documents_uz'] }}</textarea> 
                                                </div> 

                                                <div class="form-group">
                                                	<label><b>documents_ru</b></label>
                                                	<textarea  class="form-control" rows="6" name="documents_ru" >{{ $model['documents_ru'] }}</textarea> 
                                                </div>

                                                <div class="form-group">
                                                	<label for=""><b>Taminot turi</b></label>
                                                	<select required="required" name='taminot_turi_ids' class="form-control">
                                                		<option value="">-@lang('lang.choose')-</option>
                                                		@foreach($taminots as $taminot)
                                                		 <option value="{{ $taminot->id }}" @if($model['taminot_turi_ids'] == $taminot->id) selected="selected" @endif>{{ $taminot['name_uz'] }}</option>
                                                		@endforeach
                                                	</select>
                                                </div>

                                                <div class="form-group">
                                                	<label><b>taminot_text_uz</b></label>
                                                	<textarea class="form-control" rows="6" name="taminot_text_uz" >{{ $model['taminot_text_uz'] }}</textarea> 
                                                </div> 

                                                <div class="form-group">
                                                	<label><b>taminot_text_ru</b></label>
                                                	<textarea  class="form-control" rows="6" name="taminot_text_ru" >{{ $model['taminot_text_ru'] }}</textarea> 
                                                </div>

                                                <div class="form-group">
                                                	<label><b>rasmiy_yuli_uz</b></label>
                                                	<input name='rasmiy_yuli_uz' required="required" value="{{ $model['rasmiy_yuli_uz'] }}" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                	<label><b>rasmiy_yuli_ru</b></label>
                                                	<input name='rasmiy_yuli_ru' required="required" value="{{ $model['rasmiy_yuli_ru'] }}" class="form-control">
                                                </div> 

                                                <div class="form-group">
                                                	<label><b>ajratish_uz</b></label>
                                                	<input name='ajratish_uz'  value="{{ $model['ajratish_uz'] }}" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                	<label><b>ajratish_ru</b></label>
                                                	<input name='ajratish_ru' value="{{ $model['ajratish_ru'] }}" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                	<label><b>sundirish_uz</b></label>
                                                	<textarea class="form-control" rows="6" name="sundirish_uz" >{{ $model['sundirish_uz'] }}</textarea> 
                                                </div> 

                                                <div class="form-group">
                                                	<label><b>sundirish_ru</b></label>
                                                	<textarea  class="form-control" rows="6" name="sundirish_ru" >{{ $model['sundirish_ru'] }}</textarea> 
                                                </div>

                                                <div class="form-group">
                                                	<label><b>talablar_uz</b></label>
                                                	<textarea class="form-control" rows="6" name="talablar_uz" >{{ $model['talablar_uz'] }}</textarea> 
                                                </div> 

                                                <div class="form-group">
                                                	<label><b>talablar_ru</b></label>
                                                	<textarea  class="form-control" rows="6" name="talablar_ru" >{{ $model['talablar_ru'] }}</textarea> 
                                                </div> 
                                                <div class="form-group">
                                                	<label><b>qushim_shart_uz</b></label>
                                                	<textarea class="form-control" rows="6" name="qushim_shart_uz" >{{ $model['qushim_shart_uz'] }}</textarea> 
                                                </div> 

                                                <div class="form-group">
                                                	<label><b>qushim_shart_ru</b></label>
                                                	<textarea  class="form-control" rows="6" name="qushim_shart_ru" >{{ $model['qushim_shart_ru'] }}</textarea> 
                                                </div>

                                                <div class="form-group">
                                                	<label><b>@lang('lang.link') uz</b></label>
                                                	<input name='link_uz' required value="@if($model['id']){{ $model->link('uz') }}@endif" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label><b>@lang('lang.link') ru</b></label>
                                                    <input name='link_ru' required value="@if($model['id']){{ $model->link('ru') }}@endif" class="form-control">
                                                </div>

                                                <div class="form-group d-flex col-sm-12 pr-0 pl-0 mb-3" >
		                                            <div class="col-sm-6">
		                                                <label >@lang('lang.image') (yangi)</label>
		                                                <input type="file" id="input-file-now-custom-2" value="{{ $model['image'] }}" name="image" class="dropify" data-default-file="" />
		                                            </div>
		                                            <div class="col-md-6">
		                                            	<label>Yuklangan</label>
		                                            	@if($model['image'])
		                                            	 <img width="250" src="/{{ $model['image'] }}">
		                                            	@endif
		                                            </div>
		                                        </div> 

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success">@lang('lang.save')</button>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
 <script src="/plugins/dropify/dist/js/dropify.min.js"></script>

  <script>
        $(document).ready(function () {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#card-parent').addClass('active pcoded-trigger');
            $('#card-child2').addClass('active');

           
        // Basic
        $('.dropify').dropify();

         $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

 		
        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })

    });

    </script>
 @endsection