@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="">@lang('lang.debit_cards')</a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.debit_cards')</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-info p-1 text-white pl-2 pr-3" href="{{ route('debit-cards-add-page') }}">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table id="debit-card-table" class="table-responsive-xl table-borderless table table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('lang.name')</th>
                                            <th>@lang('lang.bank')</th>
                                            <th>@lang('lang.card_type')</th>
                                            <th>@lang('lang.currency')</th>
                                            <th>@lang('lang.status')</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($cards as $row)
                                            <tr>
                                               <td>{{ $row->id }}</td>
                                               <td>{{ $row->name() }}</td>
                                               <td>{{ $row->bank->name() }}</td>
                                               <td>@if($row->type_card_id){{ $row->cardType->name() }}@endif</td>
                                               <td>
                                                @if(!$row->currency_id) @lang('lang.multi_currency') 
                                                @else
                                                    @foreach($row->currencies() as $row_c) 
                                                    {{ $row_c->name }} 
                                                    @endforeach
                                                    @endif
                                                </td>
                                                <td>
                                                    <?php
                                                    if($row->status == 1){
                                                        $statusClass = 'alert-success';
                                                        $statusText = 'Active';
                                                    }elseif($row->status == 0){
                                                        $statusClass = 'alert-warning';
                                                        $statusText = 'Block';
                                                    }
                                                    ?>
                                                    <div id="alert-status-{{ $row->id }}" class="alert {{ $statusClass }} p-1 text-center" role="alert">
                                                        {{ $statusText }}
                                                    </div>
                                                </td>
                                                <td class="text-right">
                                                    <a  data-active-id='{{ $row->id }}'
                                                        data-status="{{ $row->status }}"
                                                        id="active-{{ $row->id }}"
                                                        class="active-dep btn @php if($row->status == 1){ echo 'btn-success';} if($row->status == 0){ echo 'btn-warning';} @endphp  p-1 pl-2 text-white "
                                                    ><i id="status-icon-{{ $row->id }}" class="fa @php if($row->status == 1){ echo 'fa-check';} if($row->status == 0){ echo 'fa-close';} @endphp "></i>
                                                    </a>

                                                    <a href="{{ route('debit-cards-update-page',['id' => $row->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>

                                                    <a href="{{ route('debit-card-delete',[$row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
<script>
    $(document).ready(function(){

        $('.delete').on('click', function() {
            return confirm("@lang('lang.confirmDel')");
        });

        $('.active-dep').on('click',function(){
            let status = $(this).attr('data-status');
            let id = $(this).attr('data-active-id');
            if(status == 0){
                $.ajax({
                    type:'GET',
                    url:'{{ route("debit-card-status") }}',
                    data:{
                        'id':id,
                        '_token': $('meta[name=csrf-token]').val(),
                        'status':1
                    },success:function(data){
                    if(data.status == 1){
                        $('#active-'+id).removeClass('btn-warning').addClass('btn-success');
                        $('#active-'+id).attr('data-status',1);
                        $('#status-icon-'+id).removeClass('fa-close').addClass('fa-check');
                        $('#alert-status-'+id).removeClass('alert-warning').addClass('alert-success').text('Active');
                      
                    }
                        
                    },error:function(){
                        alert('error')
                    }
                });

            } else if (status == 1){
                $.ajax({
                    type:'GET',
                    url:'{{ route("debit-card-status") }}',
                    data:{
                        'id':id,
                        '_token': $('meta[name=csrf-token]').val(),
                        'status':0
                    },success:function(data){
                        if(data.status == 0){
                            $('#active-'+id).removeClass('btn-success').addClass('btn-warning');
                            $('#active-'+id).attr('data-status',0);
                            $('#status-icon-'+id).removeClass('fa-check').addClass('fa-close');
                            $('#alert-status-'+id).removeClass('alert-success').addClass('alert-warning').text('Block');
                        }
                    },error:function(){
                    
                        alert('error')
                    }
                });
            }
        });

        $('#card-parent').addClass('active pcoded-trigger');
        $('#card-child1').addClass('active');

        $(function () {
            $('#debit-card-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });
    });

</script>
@endsection