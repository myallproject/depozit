@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="">@lang('lang.debit_cards')</a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5> @lang('lang.'.$edit.''). @lang('lang.debit_cards')</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-info p-1 text-white pl-2 pr-3" href="{{ route('debit-cards-add-page') }}">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="row pl-4">
                                        <div class="col-12">
                                            <form action="{{ route($route) }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $model['id'] }}">
                                                <div class="form-group ">
                                                    <label><b>@lang('lang.bank')</b></label>
                                                    <select name="bank_id" required="required" class="form-control">
                                                        <option value="">- @lang('lang.choose') -</option>
                                                        @foreach($banks as $bank)
                                                         <option @if($model['bank_id'] == $bank->id) selected @endif value="{{ $bank->id }}">{{ $bank->name_uz }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.name') uz</b></label>
                                                    <input name="name_uz" value="{{ $model['name_uz'] }}" type="text" required  class="form-control"/>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.name') ru</b></label>
                                                    <input name="name_ru" type="text" value="{{ $model['name_ru'] }}" required  class="form-control"/>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.type_card')</b></label>
                                                    <select name="type_card_id" class="form-control">
                                                        <option value="">-@lang('lang.choose')-</option>
                                                        @foreach($card_types as $card_type)
                                                        <option @if($model['type_card_id'] == $card_type->id) selected @endif value="{{ $card_type->id }}">{{ $card_type->name() }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.payment')</b></label>
                                                    <select name="payment_id[]" required class="form-control select2" multiple>
                                                        @foreach($payments as $payment)
                                                            @php if($model['payment_id']){$pay = explode(',',$model['payment_id']);}else {$pay = [];}  @endphp
                                                            <option @foreach($pay as $k => $v) @if($payment->id == $v ) selected @endif @endforeach value="{{ $payment->id }}">{{ $payment->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.currency')</b></label>
                                                    <select name="currency_id[]" required class="form-control select2" multiple>
                                                        @foreach($currencies as $currency)
                                                            @php if($model['currency_id']){$curr = explode(',',$model['currency_id']);}else {$curr = [];}  @endphp
                                                            <option @foreach($curr as $k => $v) @if($currency->id == $v ) selected @endif @endforeach value="{{ $currency->id }}">{{ $currency->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.date')</b></label>
                                                    <select name="date_id" class="form-control">
                                                        <option value="">-@lang('lang.choose')-</option>
                                                        @foreach($dates as $date)
                                                        <option @if($model['date_id'] == $date->id) selected @endif value="{{ $date->id }}">{{ $date->name() }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group card p-20">
                                                    <div>
                                                        <label><b>@lang('lang.card_price')</b></label>
                                                        <input name="price"  type="text" value="{{ $model['price'] }}" required class="form-control"/>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><b>Izoh UZ</b></label>
                                                            <input name="price_izoh_uz" type="text" value="{{ $model['price_izoh_uz'] }}" class="form-control"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Izoh RU</b></label>
                                                            <input name="price_izoh_ru" type="text" value="{{ $model['price_izoh_ru'] }}" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.re_issue_card')</b></label>
                                                    <input name="re_issue_card" type="text" value="{{ $model['re_issue_card'] }}" class="form-control"/>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.urgent_opening')</b></label>
                                                    <input name="urgent_opening" type="text" value="{{ $model['urgent_opening'] }}" class="form-control"/>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.open_additional_card')</b></label>
                                                    <input name="qushimcha_karta_ochish" type="text" value="{{ $model['qushimcha_karta_ochish'] }}" class="form-control"/>
                                                </div>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><b>Xizmat(Обслуживание) UZ</b></label>
                                                            <input name="service_uz" type="text" value="{{ $model['service_uz'] }}" class="form-control"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Xizmat(Обслуживание) RU</b></label>
                                                            <input name="service_ru" type="text" value="{{ $model['service_ru'] }}" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.sms_inform')</b> (free -> 0)</label>
                                                    <input name="sms_inform" type="text" value="{{ $model['sms_inform'] }}" class="form-control"/>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.residual_balance')</b></label>
                                                    <input name="kamay_qoldiq" type="text" value="{{ $model['kamay_qoldiq'] }}" required  class="form-control"/>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>Kartani rasmiylashtirish usuli uz</b></label>
                                                    <input name="rasmiy_yuli_uz" type="text" value="{{ $model['rasmiy_yuli_uz'] }}"   class="form-control"/>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>Kartani rasmiylashtirish usuli ru</b></label>
                                                    <input name="rasmiy_yuli_ru" type="text" value="{{ $model['rasmiy_yuli_ru'] }}"   class="form-control"/>
                                                </div>

                                                <div class="form-group card p-20">
                                                    <div>
                                                        <label><b>@lang('lang.payment_commission')(shu bank)</b></label>
                                                        <input name="tuluv_komissia"  type="text" value="{{ $model['tuluv_komissia'] }}" required class="form-control"/>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><b>Izoh UZ</b></label>
                                                            <input name="tuluv_komissia_izoh_uz" type="text" value="{{ $model['tuluv_komissia_izoh_uz'] }}" class="form-control"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Izoh RU</b></label>
                                                            <input name="tuluv_komissia_izoh_ru" type="text" value="{{ $model['tuluv_komissia_izoh_ru'] }}" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group card p-20">
                                                    <div>
                                                        <label><b>@lang('lang.payment_commission')(boshqa bank)</b></label>
                                                        <input name="tuluv_komissia_boshqa"  type="text" value="{{ $model['tuluv_komissia_boshqa'] }}" required class="form-control"/>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><b>Izoh UZ</b></label>
                                                            <input name="tuluv_komissia_boshqa_izoh_uz" type="text" value="{{ $model['tuluv_komissia_boshqa_izoh_uz'] }}" class="form-control"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Izoh RU</b></label>
                                                            <input name="tuluv_komissia_boshqa_izoh_ru" type="text" value="{{ $model['tuluv_komissia_boshqa_izoh_ru'] }}" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group card p-20">
                                                    <div>
                                                        <label><b>@lang('lang.your_banks_terminal')</b></label>
                                                        <input name="naqdlash_komissia_terminal"  type="text" value="{{ $model['naqdlash_komissia_terminal'] }}" class="form-control"/>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><b>Izoh UZ</b></label>
                                                            <input name="naqdlash_komissia_terminal_izoh_uz" type="text" value="{{ $model['naqdlash_komissia_terminal_izoh_uz'] }}" class="form-control"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Izoh RU</b></label>
                                                            <input name="naqdlash_komissia_terminal_izoh_ru" type="text" value="{{ $model['naqdlash_komissia_terminal_izoh_ru'] }}" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group card p-20">
                                                    <div>
                                                        <label><b>@lang('lang.your_banks_bankomat')</b></label>
                                                        <input name="naqdlash_komissia"  type="text" value="{{ $model['naqdlash_komissia'] }}" class="form-control"/>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><b>Izoh UZ</b></label>
                                                            <input name="naqdlash_komissia_izoh_uz" type="text" value="{{ $model['naqdlash_komissia_izoh_uz'] }}" class="form-control"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Izoh RU</b></label>
                                                            <input name="naqdlash_komissia_izoh_ru" type="text" value="{{ $model['naqdlash_komissia_izoh_ru'] }}" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group card p-20">
                                                    <div>
                                                        <label><b>@lang('lang.other_banks_bankomat')</b></label>
                                                        <input name="naqdlash_komissia_boshqa"  type="text" value="{{ $model['naqdlash_komissia_boshqa'] }}" required class="form-control"/>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><b>Izoh UZ</b></label>
                                                            <input name="naqdlash_komissia_boshqa_izoh_uz" type="text" value="{{ $model['naqdlash_komissia_boshqa_izoh_uz'] }}" class="form-control"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Izoh RU</b></label>
                                                            <input name="naqdlash_komissia_boshqa_izoh_ru" type="text" value="{{ $model['naqdlash_komissia_boshqa_izoh_ru'] }}" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.documents') UZ</b></label>
                                                    <textarea name="documents_uz" class="form-control" rows="4">{{ $model['documents_uz'] }}</textarea>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.documents') RU</b></label>
                                                    <textarea name="documents_ru" class="form-control" rows="4">{{ $model['documents_ru'] }}</textarea>
                                                </div>

                                                <div class="form-group">
                                                    <input name="bepul_xizmat" @if( $model['bepul_xizmat']) checked @endif type="checkbox" value="1"/>
                                                    <label><b>@lang('lang.free_service')</b></label>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <input name="bepul_naqdlash" @if( $model['bepul_naqdlash']) checked @endif type="checkbox" value="1" />
                                                    <label><b>@lang('lang.free_money')</b></label>
                                                </div>

                                                <div class="form-group">
                                                    <input name="bepul_tulov" @if( $model['bepul_tulov']) checked @endif type="checkbox" value="1" />
                                                    <label><b>@lang('lang.free_pay')</b></label>
                                                </div>

                                                <div class="form-group">
                                                    <input name="lounge_key" @if( $model['lounge_key']) checked @endif type="checkbox" value="1" />
                                                    <label><b>Lounge Key</b></label>
                                                </div>

                                                <div class="form-group">
                                                    <input name="d_secure" @if( $model['d_secure']) checked @endif type="checkbox" value="1" />
                                                    <label><b>3D Secure</b></label>
                                                </div>

                                                <div class="form-group card p-20">
                                                    <div>
                                                        <input name="kontaktsiz_tulov" @if( $model['kontaktsiz_tulov']) checked @endif type="checkbox" value="1" />
                                                        <label><b>@lang('lang.no_contact_pay')</b></label>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><b>Izoh UZ</b></label>
                                                            <input name="kontaksiz_tolov_izoh_uz" type="text" value="{{ $model['kontaksiz_tolov_izoh_uz'] }}" class="form-control"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Izoh RU</b></label>
                                                            <input name="kontaksiz_tolov_izoh_ru" type="text" value="{{ $model['kontaksiz_tolov_izoh_uz'] }}" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group card p-20">
                                                    <div>
                                                        <input name="cash_back" @if( $model['cash_back']) checked @endif type="checkbox" value="1" />
                                                        <label><b>Cash Back</b></label>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><b>Izoh UZ</b></label>
                                                            <input name="cash_back_izoh_uz" type="text" value="{{ $model['cash_back_izoh_uz'] }}" class="form-control"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Izoh RU</b></label>
                                                            <input name="cash_back_izoh_ru" type="text" value="{{ $model['cash_back_izoh_ru'] }}" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group card p-20">
                                                    <div>
                                                        <input name="uyga_yetkazish" @if($model['uyga_yetkazish']) checked @endif type="checkbox" value="1" />
                                                        <label><b>@lang('lang.delivery_service')</b></label>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><b>Izoh UZ</b></label>
                                                            <input name="uyga_yetkazish_izoh_uz" type="text" value="{{ $model['uyga_yetkazish_izoh_uz'] }}" class="form-control"/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Izoh RU</b></label>
                                                            <input name="uyga_yetkazish_izoh_ru" type="text" value="{{ $model['uyga_yetkazish_izoh_ru'] }}" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group card p-20" style="color: red;">
                                                    <div>
                                                        <input name="aktsiya" @if($model['aktsiya']) checked @endif type="checkbox" value="1" />
                                                        <label><b>Aktsiya</b></label>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label><b>Izoh UZ</b></label>
                                                            <textarea name="aktsiya_izoh_uz" type="text" class="form-control" rows="4">@if($model['id']){{ $model->aktsiya_izoh('uz') }}@endif</textarea>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label><b>Izoh RU</b></label>
                                                            <textarea name="aktsiya_izoh_ru" type="text" class="form-control" rows="4">@if($model['id']){{ $model->aktsiya_izoh('ru') }}@endif</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label><b>Link UZ</b></label>
                                                            <input name="aktsiya_link_uz" type="text" value="@if($model['id']){{ $model->aktsiya_link('uz') }}@endif" class="form-control"/>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <label><b>Link RU</b></label>
                                                            <input name="aktsiya_link_ru" type="text" value="@if($model['id']){{ $model->aktsiya_link('ru') }}@endif" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label><b>@lang('lang.additional_conveniences') uz</b></label>
                                                    <textarea name="qushimcha_qulaylik_uz" class="form-control" rows="4">{{ $model['qushimcha_qulaylik_uz'] }}</textarea>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.additional_conveniences') ru</b></label>
                                                    <textarea name="qushimcha_qulaylik_ru" class="form-control" rows="4">{{ $model['qushimcha_qulaylik_ru'] }}</textarea>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.addition_rules') uz</b></label>
                                                    <textarea name="qushimcha_shart_uz" class="form-control"  rows="4">{{ $model['qushimcha_shart_uz'] }}</textarea>
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.addition_rules') ru</b></label>
                                                    <textarea name="qushimcha_shart_ru" class="form-control" rows="4">{{ $model['qushimcha_shart_ru'] }}</textarea> 
                                                </div>

                                                <div class="form-group">
                                                    <label><b>@lang('lang.link') uz</b></label>
                                                    <input name="link_uz" type="text" value="@if($model['id']){{ $model->link('uz') }}@endif"   class="form-control"/>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label><b>@lang('lang.link') ru</b></label>
                                                    <input name="link_ru" type="text" value="@if($model['id']){{ $model->link('ru') }}@endif"   class="form-control"/>
                                                </div>

                                                <div class="form-group">
                                                    <label> <b>@lang('lang.image')</b></label>
                                                    <input type="file" name="image" class="form-control form-control-lg">
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success">@lang('lang.save')</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function(){

            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });

            $('#card-parent').addClass('active pcoded-trigger');
            $('#card-child1').addClass('active');

           /* $(function () {
                $('#debit-card-table').DataTable({
                    'paging'      : true,
                    'lengthChange': true,
                    'searching'   : true,
                    'ordering'    : true,
                    'info'        : false,
                    'autoWidth'   : true,
                    "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
                })
            });*/
        });

    </script>
@endsection