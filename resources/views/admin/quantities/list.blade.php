@extends('admin.layouts.app')

@section('breadcrumb')
   {{--  <li class="breadcrumb-item">
        <a href="#">
            <i class="fa fa-bank"></i>
        </a>
    </li> --}}
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Miqdorlar</h5>
                                    <div class="card-header-right">
                                        <a id="add-new-btn" data-toggle="modal" data-target="#myModal" class="btn btn-info p-1 text-white pl-2 pr-3" href="">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table id="data-table" class="table-responsive-xl table-borderless table table-hover" style="font-size: 13px !important;">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('lang.name')</th>
                                          
                                            <th>@lang('lang.description')</th>
                                            <th>@lang('lang.quantum')</th>
                                            <th>@lang('lang.status')</th>
                                            
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($model as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>

                                                <td>
                                                    <span  id="name-uz{{ $row->id }}">{{ $row->name_uz }}</span><br>
                                                    <span  id="name-ru{{ $row->id }}">{{ $row->name_ru }}</span>
                                                </td>
                                                <td >
                                                    <span id="description-uz{{ $row->id }}">{{ $row->description_uz }}</span><br>
                                                    <span id="description-ru{{ $row->id }}">{{ $row->description_ru }}</span><br>
                                                </td>
                                                <td>
                                                    <span id="summa{{ $row->id }}">{{ $row->summa }}</span>
                                                    &nbsp;<span  id="currency_id{{ $row->id }}" data-val="{{ $row->currency_id }}">{{ $row->currency->name }}</span>
                                                </td>
                                                <td>
                                                    <span id="status" data-val="{{ $row->status }}">
                                                        @if($row->status == 1)
                                                            <span class="text-success">Aktiv</span> 
                                                        @else
                                                            <span class="text-warning">Bloklangan</span> 
                                                        @endif
                                                    </span>
                                                </td>
                                                
                                                <td class="text-right">
                                                    <a data-toggle="modal" data-target="#myModal" data-id="{{ $row->id }}" data-service-id="" class="update btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('min-quantity-delete',[$row->id])}}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog " style="max-width: 860px">

            <!-- Modal content-->
            <form action="{{ route('min-quantity-edit') }}" method="POST">
                @csrf
                <input type="hidden" id="id" name="id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                       <div class="col-md-12">
                            <div class="form-group">
                                <label>@lang('lang.name') UZ</label>
                                <input type="text" autocomplete="off" id="name-uz" name="name_uz" required class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>@lang('lang.name') RU</label>
                                <input type="text" autocomplete="off" id="name-ru" name="name_ru" required class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                             <div class="form-group">
                                <label>@lang('lang.description') UZ</label>
                                <input type="text" autocomplete="off" id="description-uz" name="description_uz" required class="form-control">
                            </div>
                        </div>
                         <div class="col-md-12">
                             <div class="form-group">
                                <label>@lang('lang.description') RU</label>
                                <input type="text" autocomplete="off" id="description-ru" name="description_ru" required class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                             <div class="form-group">
                                <label>@lang('lang.quantum') </label>
                                <input type="text" autocomplete="off" id="summa" name="summa" required class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                             <div class="form-group">
                                <label>@lang('lang.currency') </label>
                                <select required="required" name="currency_id" autocomplete="off" id="currency_id" class="form-control">
                                    @foreach($currencies as $currency)   
                                        <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                             <div class="form-group">
                                <label>@lang('lang.status') </label>
                                <select name="status" autocomplete="off" id="status" class="form-control">
                                    <option value="1">Activlashtirish</option>
                                    <option value="0">Bloklash</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning" data-dismiss="modal" >{{ Yt::trans('Yopish','uz') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });

            $('#menu-addition').addClass('active pcoded-trigger');
            $('#min-quantity-list').addClass('active');

            $('#add-new-btn').click(function(){
                $('#id').val('');
                $('#name-uz').val('');
                $('#name-ru').val('');
                $('#description-uz').val('');
                $('#description-ru').val('');
                $('#summa').val();
                $('#curency_id').val(1).trigger('change');
                $('#status').val(1).trigger('change');
                $('#modal-title').text("@lang('lang.add_new')")
            });

            $('.update').click(function(){
                let id = $(this).attr('data-id');

                n_uz = $('#name-uz'+id).text();
                n_ru = $('#name-ru'+id).text();
                d_uz = $('#description-uz'+id).text();
                d_ru = $('#description-ru'+id).text();
                summa = $('#summa'+id).text();
                currency_id = $('#currency_id'+id).attr('data-val');
                status = $('#status'+id).attr('data-val');
                $('#name-uz').val(n_uz);
                $('#name-ru').val(n_ru);
                $('#description-uz').val(d_uz);
                $('#description-ru').val(d_ru);
                $('#summa').val(summa);
                $('#currency_id').val(currency_id).trigger('change');
                $('#status').val(status).trigger('change');
                $('#id').val(id);
                $('#modal-title').text("@lang('lang.edit')")

            });
        });

        $(function () {
            $('#data-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });

    </script>
@endsection
