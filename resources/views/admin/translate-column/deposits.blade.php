@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a >Dedposit Column Translate</a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Dedposit Column Trasnnlate</h5>
                                </div>
                                <div class="card-block">
                                    <span class="text-danger">Maydonga depoist jadvali columi kiritiladi. Birnichi translate tanlandi, sahifa yangilangandan keyin add two column tanladani!</span>
                                    <div class="form-group">
                                           <label for="#translate">Translate</label>
                                            <input type="radio" id="translate" class="action" name="radio" value="{{ route('depsit-column-translate') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="#add-two-column">Add two column</label>
                                            <input type="radio" id="add-two-column" class="action" name="radio" value="{{ route('deposit-add-json-to-column') }}">
                                    </div>
                                    <form id="translate-form" action="" method="GET">
                                       
                                        <div class="form-group">
                                            <div class="col-md-6 p-0"><label for="">Model</label>
                                                <input class="form-control" required name="model">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 p-0"><label for="">Column</label>
                                                <input class="form-control" required name="column">
                                            </div>
                                        </div>

                                        <div class="fomr-group">
                                              <button class="btn waves-effect waves-light all-status-btn hor-grd pl-2 p-1  pr-3 btn-grd-success" type="submit">Translate</button>

                                              <button class="btn waves-effect waves-light all-status-btn hor-grd pl-2 p-1  pr-3 btn-grd-warning" type="submit">Add result column</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <script >
        $('.action').click(function(){
            $('#translate-form').attr('action',$(this).val());
        });
    </script>   
@endsection

