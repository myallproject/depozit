@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="">@lang('lang.news')</a>
    </li>
@endsection
@section('content')
<div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.news')</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-info p-1 text-white pl-2 pr-3" href="{{ route('news-add-get') }}">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table id="news-table" class="table-responsive-xl table-borderless table table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            {{-- <th>User</th> --}}
                                            <th>@lang('lang.created_at')</th>
                                            <th>@lang('lang.name')</th>
                                          
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($news as $row)
                                             <tr>
                                               <td>{{ $row->id }}</td>
                                               {{-- <td>{{ $row->user->getName()}}</td> --}}
                                               <td>{{ $row->created_at }}</td>
                                               <td><b>Uz:</b> {{ $row->title_uz }}<br><b>Ru:</b> {{ $row->title_ru }}</td>
                                              
                                                <td class="text-right">
                                                   
                                                    <a href="{{ route('news-update-get',['id'=>$row->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>

                                                    <a href="{{ route('news-delete',[$row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr> 
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#menu-news').addClass('active pcoded-trigger');

            $(function () {
            $('#news-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });
        });
    </script>
@endsection
