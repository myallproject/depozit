@extends('admin.layouts.app')
@section('content')

 <link rel="stylesheet" href="/plugins/dropify/dist/css/dropify.min.css">
 <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="card">
                        <form action="{{ route('news-add') }}" method="POST" enctype="multipart/form-data">
                            <input  type="hidden" name="id" value='' />
                            @csrf
                            <div class="card-heeder p-3">
                                <h5>@lang('lang.news')</h5>
                            </div>
                            <div class="card-body">
                                @if($errors->any())
                                    <div class="col-md-12">
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                                <div class="">
                                     <div class="form-group  row">
                                        <div class="col-sm-12 mb-3">
                                            <label>@lang('lang.tags')</label>
                                            <select required class="form-control select2" multiple name='new_category_id[]'>
                                                @foreach($categories as $category)
                                                <option 
												<?php $ct = []; if(old('new_category_id')){$ct = old('new_category_id');}; ?>
													<?php if(in_array($category->id, $ct)) : ?>
														selected 
													<?php endif;?>
												value="{{ $category->id }}">{{ $category->name() }}
												</option>
                                                @endforeach
                                            </select>
                                        </div>
                                         <div class="col-sm-12 mb-3">
                                            <label>Nomi</label>
                                            <input  class="form-control" name='title_uz' type="text" value="{{ old('title_uz') }}"/>
                                        </div>
                                        <div class="col-sm-12 mb-3">
                                            <label>Название</label>
                                            <input   class="form-control" name='title_ru' type="text" value="{{ old('title_ru') }}" />
                                        </div>
                                        <div class="col-sm-12 mb-3">
                                            <label>Ta'rif</label>
                                            <input class="form-control" name='description_uz' type="text" value="{{ old('description_uz') }}" />
                                        </div>
                                        <div class="col-sm-12 mb-3">
                                            <label>Описание</label>
                                            <input class="form-control" name='description_ru' type="text" value="{{ old('description_ru') }}" />
                                        </div>
                                        <div class="col-sm-12 mb-3">
                                            <label>Keywords</label>
                                            <input class="form-control" name='keywords_uz' type="text" value="{{ old('keywords_uz') }}" />
                                        </div>
                                        <div class="col-sm-12 mb-3">
                                            <label>Ключевые слова</label>
                                            <input class="form-control" name='keywords_ru' type="text" value="{{ old('keywords_ru') }}" />
                                        </div>

                                        <div class="col-sm-12 mb-3">
                                            <label >Matn</label>
                                            <textarea   name="text_uz" id="editor1" rows="10" cols="80">{{ old('text_uz') }}</textarea>
                                        </div>
                                        <div class="col-sm-12 mb-3">
                                            <label >Текст</label>
                                            <textarea   name="text_ru" id="editor2" rows="10" cols="80">{{ old('text_ru') }}</textarea>
                                        </div>
                                        <div class="col-sm-12 mb-3">
                                            <label>Link UZ</label>
                                            <input   class="form-control" name='link_uz' type="text" value="{{ old('link_uz') }}" />
                                        </div>
                                        <div class="col-sm-12 mb-3">
                                            <label>Link RU</label>
                                            <input   class="form-control" name='link_ru' type="text" value="{{ old('link_ru') }}" />
                                        </div>
                                        <div class="col-sm-12 pr-0 pl-0 mb-3" >
                                            <div class="col-sm-6">
                                                <label >@lang('lang.image')</label>
                                                <input  type="file" id="input-file-now-custom-2" value="{{ old('image') }}" name="image" class="dropify" data-default-file="" />
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div> 
                                    </div> 
                              </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-md-12">
                                    <button type=" submit" class="btn btn-success">@lang('lang.save')</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
   <script src="/plugins/ckeditor/ckeditor.js"></script>
 <script src="/plugins/dropify/dist/js/dropify.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#menu-news').addClass('active pcoded-trigger');

        CKEDITOR.replace('editor1', {
            uiColor: '#CCEAEE',
            height: 250,
            extraPlugins: 'colorbutton',
        });
        CKEDITOR.replace('editor2', {
            uiColor: '#CCEAEE',
            height: 250,
            extraPlugins: 'colorbutton',
        });
           
        // Basic
        $('.dropify').dropify();

         $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

 		
        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })

    });

    </script>
@endsection