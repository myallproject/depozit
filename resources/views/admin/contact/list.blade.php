@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">@lang('lang.contact')</a>
    </li>
@endsection

@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.contact')</h5>
                                    <div class="card-header-right">

                                    </div>
                                </div>
                                <div class="card-block font-size-14">
                                    <table id="credit-table" class="table-responsive-xl table-borderless table table-hover"  style="font-size: 12px !important;">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('lang.user')</th>
                                            <th>@lang('lang.text')</th>
                                            <th>@lang('lang.status')</th>
                                            <th>@lang('lang.date')</th>
                                            <th>@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($contacts as $row)
                                                <tr>
                                                    <td>{{ $row->id }}</td>
                                                    <td>{{ $row->name }}<br>{{ $row->phone }}<br>{{ $row->email }}</td>
                                                    <td>{{ $row->text }}</td>
                                                    <td>{{ $row->status }}</td>
                                                    <td>{{ $row->created_at }}</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-check"></i></a>
                                                        <a href="#" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <script>
        $(document).ready(function () {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#menu-contact').addClass('active');



        });

        $(function () {
            $('#credit-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : false,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });


    </script>
    <style>
        .float-right{
            float:right !important;
        }
    </style>
@endsection
