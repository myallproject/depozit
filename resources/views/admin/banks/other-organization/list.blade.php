@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">
            <i class="fa fa-bank"></i>
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Tashkilotlar</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-warning p-1 text-white pl-2 pr-3" data-toggle="modal" data-target="#myModal" ><i class="fa fa-upload"> </i> Import Filial

                                        </a>  <a href="{{ route('banks-add-form') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table id="bank-table" class="table-responsive-xl table-borderless table table-hover" style="font-size: 13px !important;">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('lang.name')</th>
                                            <th>@lang('lang.image')</th>
                                            <!-- <th>@lang('lang.phone')</th> -->
                                            <th>@lang('lang.services')</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($organizations as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td>{{ $row->name() }}</td>
                                                <td><img src="/{{ $row->image }}" width="70" ></td>
                                               <!--  <td>{{ $row->phone }}</td> -->
                                                <td>
                                                    <a href="{{ route('bank-deposits',[$row->id]) }}" class="btn btn-info p-1 pl-2 text-white">@lang('lang.deposits') &nbsp; {{ count($row->deposits) }}</a>
                                                    &nbsp;
                                                    <a href="" class="btn btn-info p-1 pl-2 text-white">@lang('lang.credits') &nbsp; {{ count($row->credits) }}</a> &nbsp;
                                                    <a href="{{ route('organization-branches',[$row->id]) }}" class="btn btn-success p-1 pl-2 text-white">Filiallar</a>
                                                </td>
                                                <td class="text-right">
                                                    {{--<a href="{{ route('bank-information',[$row->id]) }}" class="btn btn-info p-1 pl-2 text-white"><i class="fa fa-info"></i></a>--}}
                                                    <a href="{{ route('banks-view',[$row->id]) }}" class="btn btn-info p-1 pl-2 text-white"><i class="fa fa-eye"></i></a>
                                                    <a href="{{ route('banks-update-form',[$row->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('bank-delete',[$row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog " style="max-width: 400px">

            <!-- Modal content-->
            <form action="{{ route('organization-branches-import') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                       <div class="col-md-12">
                            <div class="form-group">
                                <label>Excel file</label>
                                <input type="file" autocomplete="off"  name="file" required class="form-control">
                            </div>
                        </div>
                      
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning" data-dismiss="modal" >{{ Yt::trans('Yopish','uz') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')"+"bu bankga tegishli bulgan barcha ma'lumot va xizmatlar ma'lumotlar bazasidan o'chadi!");
            });
            $('#menu-banks').addClass('active');

        });

        $(function () {
            $('#bank-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });
    </script>
@endsection
