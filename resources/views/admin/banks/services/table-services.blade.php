@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">
            <i class="fa fa-bank"></i>
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.banks')</h5>
                                    <div class="card-header-right">
                                        <a data-toggle="modal" data-target="#myModal" data-id="" data-service-id="" class="btn btn-info add p-1 text-white pl-2 pr-3" href=""><i class="fa fa-upload"> </i> @lang('lang.add_new')</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table id="bank-table" class="table-responsive-xl table-borderless table table-hover" style="font-size: 13px !important;">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Path</th>
                                            <th>Komments</th>
                                            <th>Yuklangan vaqt</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($documents as $doc)
                                            <tr>
                                                <td>{{ $doc->id }}</td>
                                                <td>{{ $doc->file }}</td>
                                                <td id="td_comments{{$doc->id}}">{{ $doc->comments }}</td>
                                                <td>{{ $doc->updated_at }}</td>
                                             
                                                <td class="text-right"> 
                                                    <a href="{{ route('admin-bank-services-table-file-download',[$doc->id]) }}" class="btn btn-success download p-1 pl-2 text-white"><i class="fa fa-download"></i></a>
                                                    <a data-toggle="modal" data-target="#myModal" data-id="{{ $doc->id }}"class="btn btn-primary upload p-1 pl-2 text-white"><i class="fa fa-upload"></i></a>
                                                    <a href="{{ route('admin-bank-services-table-delete',[$doc->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
 <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog " >

            <!-- Modal content-->
            <form action="{{ route('admin-bank-services-table-edit') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="id" name="id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                       <div class="col-md-12">
                            <div class="form-group">
                                <label>File (Excel)</label>
                                <input type="file" autocomplete="off" id="file" name="file" required class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Komments</label>
                                <input type="text" autocomplete="off" id="comments" name="comments" required class="form-control">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning" data-dismiss="modal" >{{ Yt::trans('Yopish','uz') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#menu-bank-services-table').addClass('active pcoded-trigger');

        });

        $('.add').click(function(){
            $('#id').val('');
            $('#comments').val('');
          
            $('#modal-title').text("@lang('lang.add_new')")
        });

        $('.upload').click(function(){
            let id = $(this).attr('data-id');
                comments = $('#td_comments'+id).text();

            $('#id').val(id);
            $('#comments').val(comments);
            $('#modal-title').text("@lang('lang.edit')")

        });
    </script>
@endsection
