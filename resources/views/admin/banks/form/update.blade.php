<style>
    .select2-container {
        width: 100% !important;
    }
</style>
<div class="row">
    <div class="col-sm-12 pt-4 pl-5">
        <form action="{{ route('banks-update') }}" method="POST" enctype="multipart/form-data">
           @csrf
            <input type="hidden" name="id" value="{{ $bank->id }}">
            <input type="hidden" name="bank_info_id" value="{{ $bank_info->id }}">
            <input type="hidden" name="slug" value="{{ $bank->slug }}">
            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.name') UZ</b></label>
                    <input type="text" name="name_uz" class="form-control form-control-lg" value="{{ $bank->name_uz}}" required>
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.name') RU</b></label>
                    <input type="text" name="name_ru" class="form-control form-control-lg" value="{{ $bank->name_ru}}" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.name_official') UZ</b></label>
                    <input type="text" name="official_name_uz" class="form-control form-control-lg" value="{{ $bank_info->official_name_uz}}" required>
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.name_official') RU</b></label>
                    <input type="text" name="official_name_ru" class="form-control form-control-lg" value="{{ $bank_info->official_name_ru}}" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>Tashkilot turi</b></label>
                    <select required name="organization_id" class="form-control">
                        @foreach($organization as $org)
                            <option @if($bank->organization_id == $org->id) selected @endif value="{{ $org->id}}">{{ $org->name() }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>Bank turi</b></label>
                    <select required name="type" class="form-control">
                        @foreach($type_bank as $type)
                            <option @if($bank->bank_type_id == $type->id) selected @endif value="{{ $type->id}}" >{{ $type->name() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row"> 
                <div class="col-sm-4 mb-3">
                    <label>* <b>Call center</b></label>
                    <input type="text" name="phone1" class="form-control form-control-lg" value="{{ $bank_info->phone1}}" required>
                </div>
                <div class="col-sm-4 mb-3">
                    <label> <b>Ishonch telefoni(Ixtiyoriy)</b></label>
                    <input type="text" name="phone2" class="form-control form-control-lg" value="{{ $bank_info->phone2}}">
                </div>
                <div class="col-sm-4 mb-3">
                    <label> <b>@lang('lang.phone')(Ixtiyoriy)</b></label>
                    <input type="text" name="phone3" class="form-control form-control-lg" value="{{ $bank_info->phone3}}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.email')</b></label>
                    <input type="email" name="email" class="form-control form-control-lg" value="{{ $bank_info->mail}}" required>
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>Telegram</b></label>
                    <input type="text" name="telegram_channel" class="form-control form-control-lg" value="{{ $bank_info->telegram_channel }}" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.address') UZ</b></label>
                    <input type="text" name="address_uz" class="form-control form-control-lg" value="{{ $bank->address_uz}}" required>
                    {{--<sub class="text-danger">Enterni o'rniga "< br >" ni yozish kerak</sub>--}}
                </div>
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.address') RU</b></label>
                    <input type="text" name="address_ru" class="form-control form-control-lg" value="{{ $bank->address_ru}}" required>
                    <sub class="text-danger">Enterni o'rniga "< br >" ni yozish kerak</sub>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.location')</b></label>
                    <input type="text" name="location" class="form-control form-control-lg" value="{{ $bank->location }}" required>
                </div>
            </div>
            <div class=" form-group row">
                <div class="col-sm-12  mb-3">
                    <label> <b>Ofislar</b></label>
                    <select type="text" name="region[]"  multiple="multiple"  class="select2 form-control form-control-lg" required>
                        @php $r_id_arr = explode(',',$bank->region_id); @endphp
                        @foreach($regions as $reg)
                            <option @if(in_array($reg->id,$r_id_arr)) selected="selected" @endif value="{{ $reg->id }}">{{ $reg->name() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.work_time') UZ</b></label>
                    <input type="text" name="work_time_uz" class="form-control form-control-lg" value="{{ $bank->work_time_uz }}">
                    <sub class="text-danger">Enterni o'rniga "< br >" ni yozish kerak</sub>
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.work_time') RU</b></label>
                    <input type="text" name="work_time_ru" class="form-control form-control-lg" value="{{ $bank->work_time_ru }}">
                    <sub class="text-danger">Enterni o'rniga "< br >" ni yozish kerak</sub>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.description') UZ</b></label>
                    <input type="text" name="description_uz" class="form-control form-control-lg" value="{{ $bank->description_uz}}" required>
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.description') RU</b></label>
                    <input type="text" name="description_ru" class="form-control form-control-lg" value="{{ $bank->description_ru}}" required>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.text') UZ</b></label>
                    <textarea rows="6" name="text_uz" class="form-control form-control-lg" required>{{ $bank->text_uz }}</textarea>
                </div>
                 <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.text') RU</b></label>
                    <textarea rows="6" name="text_ru" class="form-control form-control-lg" required>{{ $bank->text_ru }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.website_link') UZ</b></label>
                    <input type="text" name="link_uz" class="form-control form-control-lg" value="{{ $bank_info->site_link_uz }}" required>
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.website_link') RU</b></label>
                    <input type="text" name="link_ru" class="form-control form-control-lg" value="{{ $bank_info->site_link_ru }}" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.mobile_app_title')</b></label>
                    <input type="text" name="mobile_app" class="form-control form-control-lg" value="{{ $bank_info->mobile_app }}" required>
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>App Store Link</b></label>
                    <input type="text" name="app_store" class="form-control form-control-lg" value="{{ $bank_info->app_store }}" required>
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>Play Market Link</b></label>
                    <input type="text" name="play_market" class="form-control form-control-lg" value="{{ $bank_info->play_market }}" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.bank_chairman') UZ</b></label>
                    <input type="text" name="chairman_uz" class="form-control form-control-lg" value="{{ $bank_info->chairman_uz }}" required>
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.bank_chairman') RU</b></label>
                    <input type="text" name="chairman_ru" class="form-control form-control-lg" value="{{ $bank_info->chairman_ru }}" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.stockholders') UZ</b></label>
                    <input type="text" name="stockholders_uz" class="form-control form-control-lg" value="{{ $bank_info->stockholders_uz }}" required>
                </div>
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.stockholders') RU</b></label>
                    <input type="text" name="stockholders_ru" class="form-control form-control-lg" value="{{ $bank_info->stockholders_ru }}" required>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <label><b>@lang('lang.rekvizits')</b></label>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3">
                            <label> <b>@lang('lang.official_address') UZ</b></label>
                            <input type="text" name="official_address_uz" class="form-control form-control-lg" value="{{ $bank_info->address_uz }}" required>
                        </div>
                        <div class="col-sm-12 mb-3">
                            <label> <b>@lang('lang.official_address') RU</b></label>
                            <input type="text" name="official_address_ru" class="form-control form-control-lg" value="{{ $bank_info->address_ru }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4 mb-3">
                            <label> <b>STIR</b></label>
                            <input type="number" name="stir" class="form-control form-control-lg" value="{{ $bank_info->inn }}" required>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <label> <b>MFO</b></label>
                            <input type="number" name="mfo" class="form-control form-control-lg" value="{{ $bank_info->mfo }}" required>
                        </div>
                        <div class="col-sm-4 mb-3">
                            <label> <b>SWIFT</b></label>
                            <input type="text" name="swift" class="form-control form-control-lg" value="{{ $bank_info->svift }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3">
                            <label> <b>@lang('lang.licence')</b></label>
                            <input type="text" name="licence" class="form-control form-control-lg" value="{{ $bank_info->license }}" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.image')</b></label>
                    <input @if(!$bank->image) required @endif type="file" name="image" class="form-control form-control-lg">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-info ">
                        @lang('lang.save')
                    </button>
                </div>
            </div>
        </form>
    </div>
    @if($errors->any())
        <div class="col-sm-4 pt-4">
            <div class="form-group ">
                <div class="alert alert-danger" role="alert">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>

@section('script')
    <script>
        $(document).ready(function() {
            /* $('.delete').on('click', function() {
                 return confirm();
             });*/
            $('#menu-banks').addClass('active');

        });
    </script>
@endsection