@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">
            <i class="fa fa-bank"></i>
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{{ Yt::trans('Banklar turlari','uz') }}</h5>
                                    <div class="card-header-right">
                                        <a id="add-new-btn" data-toggle="modal" data-target="#myModal" class="btn btn-info p-1 text-white pl-2 pr-3" href="">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table id="bank-table" class="table-responsive-xl table-borderless table table-hover" style="font-size: 13px !important;">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('lang.name') UZ</th>
                                            <th>@lang('lang.name') RU</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($banksType as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td id="banks-type-name-uz{{ $row->id }}">{{ $row->name_uz }}</td>
                                                <td id="banks-type-name-ru{{ $row->id }}">{{ $row->name_ru }}</td>

                                                <td class="text-right">
                                                    <a data-toggle="modal" data-target="#myModal" id="banks-type-update" date-id="{{ $row->id }}" class="banks-type-update btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('banks-type-delete',[$row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form action="{{ route('banks-type-add') }}" method="POST">
                @csrf
                <input type="hidden" id="banks-type-id" name="banks_type_id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>@lang('lang.name') UZ</label>
                                <input type="text" autocomplete="off" id="type-name-uz" name="name_uz" required class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>@lang('lang.name') RU</label>
                                <input type="text" autocomplete="off" id="type-name-ru" name="name_ru" required class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning" data-dismiss="modal" >{{ Yt::trans('Yopish','uz') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#menu-banks-type').addClass('active');

            $('#add-new-btn').click(function(){
                $('#banks-type-id').val('');
                $('#type-name-uz').val('');
                $('#type-name-ru').val('');
                $('#modal-title').text("@lang('lang.add_new')")
            });

            $('.banks-type-update').click(function(){
               let type_id = $(this).attr('date-id');
                type_n_uz = $('#banks-type-name-uz'+type_id).text();
                type_n_ru = $('#banks-type-name-ru'+type_id).text();
                $('#type-name-uz').val(type_n_uz);
                $('#type-name-ru').val(type_n_ru);
                $('#banks-type-id').val(type_id);
            });
        });

/*        $(function () {
            $('#bank-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });*/
    </script>
@endsection
