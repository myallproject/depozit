@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('banks-list') }}">
            <i class="fa fa-bank"></i>
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">
            {{ $bank->name() }}
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{{ $bank->name() }}</h5>
                                    <div class="card-header-right">
                                        <a href="{{ route('banks-add-form') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div >
                                       <table class="table table-responsive-lg table-hover">
                                           <tr>
                                                <th class="text-right">@lang('lang.name') <i class="fa fa-bank"></i></th>
                                                <td> {{ $bank->name() }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-right">@lang('lang.phone') <i class="fa fa-phone"></i></th>
                                                <td>{{ $bank->phone }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-right">@lang('lang.image') <i class="fa fa-image"></i></th>
                                                <td class="p-0"> <img width="100" src="/{{ $bank->image }}" /></td>
                                            </tr>
                                            <tr>
                                                <th class="text-right">@lang('lang.region') <i class="fa fa-address-book"></i></th>
                                                <td>
                                                    @if($bank->region_id)
                                                    @foreach($bank->regions() as $region) {{ $region->name_uz }},  @endforeach
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-right">@lang('lang.address') <i class="fa fa-address-book"></i></th>
                                                <td> {{ $bank->address() }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-right">@lang('lang.location') <i class="fa fa-map-marker"></i></th>
                                                <td> {{ $bank->location ? $bank->location:__('lang.no')}}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-right">@lang('lang.description') <i class="fa fa-file-text"></i></th>
                                                <td> {{ $bank->description() > 0 ?  $bank->description(): __('lang.no')}} </td>
                                            </tr>
                                            <tr>
                                                <th class="text-right">@lang('lang.text') <i class="fa fa-file-text"></i></th>
                                                <td> {{ $bank->text() }}{{ $bank->text() ? $bank->text():__('lang.no') }}</td>
                                            </tr>
                                           <tr>
                                                <th class="text-right">@lang('lang.status') <i class="fa fa-check-square"></i></th>
                                                <td> {{ $bank->status == 0 ? 'Aktive': 'Bloklangan' }}</td>
                                            </tr>
                                           <tr>
                                                <th class="text-right">@lang('lang.work_time') <i class="fa fa-clock-o"></i></th>
                                                <td> {{ $bank->workTime() ? $bank->workTime() :__('lang.no') }}</td>
                                            </tr>
                                           <tr>
                                                <th class="text-right">@lang('lang.website_link') <i class="fa fa-link"></i></th>
                                                <td> <a target="_blank" href="{{ $bank->information->site() }}"></a>{{ $bank->information->site() }}</td>
                                            </tr>
                                           <tr>
                                                <th class="text-right">@lang('lang.views') <i class="fa fa-eye"></i></th>
                                                <td> {{ $bank->views > 0 ? $bank->views: __('lang.no')}}</td>
                                            </tr>
                                           <tr>
                                                <th class="text-right">@lang('lang.slug') <i class="fa fa-key"></i></th>
                                                <td><input disabled="disabled" class="form-control form-control-lg" value="{{ $bank->slug }}"></td>
                                            </tr><tr>
                                                <th class="text-right">
                                                    <a href="{{ route('banks-update-form',[$bank->id]) }}" class="btn btn-info text-white">
                                                        <i class="fa fa-pencil"></i> @lang('lang.edit')
                                                    </a>
                                                </th>
                                                <td></td>
                                            </tr>
                                       </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
           /* $('.delete').on('click', function() {
                return confirm();
            });*/
            $('#menu-banks').addClass('active');

        });
    </script>
@endsection
