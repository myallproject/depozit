@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('banks-list') }}">
            <i class="fa fa-bank"></i>
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">
           
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row" style="font-size: 13px !important">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5><a href="{{ route('banks-list') }}">Orqaga</a></h5>
                                    <div class="card-header-left">
                                    		
                                    </div>
                                </div>

                                <div class="col-md-12">
                                	<form action="{{ route('edit-bank->information') }}" method="POST">
                                		<input type="hidden" name="id" value="{{ $model->id }}">
                                		 @csrf
                                    <div class="form-group row">
						                <div class="col-sm-12 mb-3">
						                    <label> <b>@lang('lang.bank')</b></label>
						                    <input type="text" disabled="" value="{{ $model->bank->name_uz }}"  class="form-control form-control-lg">
						                </div>

						                 <div class="col-sm-12 mb-3">
						                    <label> <b>Bank turi</b></label>
											 <select name="type" class="form-control">
												 @foreach($type_bank as $type)
												 <option @if($type->id == $model->type_bank) selected="selected" @endif value="{{ $type->id }}">{{ $type->name() }}</option>
												 @endforeach
											 </select>
						                </div>
										<div class="col-sm-12 mb-3">
						                    <label> <b>Litsenziya</b></label>
						                    <input type="text" name="license" value="{{ $model->license }}"  class="form-control form-control-lg">
						                </div>
						                <div class="col-sm-12 mb-3">
						                    <label> <b>Telefon</b></label>
						                    <input type="text" name="phone" value="{{ $model->phone }}"  class="form-control form-control-lg">
						                </div>
										<div class="col-sm-12 mb-3">
						                    <label> <b>Ishonch telefoni</b></label>
						                    <input type="text" name="helpline" value="{{ $model->helpline }}"  class="form-control form-control-lg">
						                </div>
						                <div class="col-sm-12 mb-3">
						                    <label> <b>Sayt</b></label>
						                    <input type="text" name="site" value="{{ $model->site }}"  class="form-control form-control-lg">
						                </div>
						                <div class="col-sm-12 mb-3">
						                    <label> <b>E-pochta</b></label>
						                    <input type="email" name="mail" value="{{ $model->mail }}"  class="form-control form-control-lg">
						                </div>
										<div class="col-sm-12 mb-3">
						                    <label> <b>Telegram kanal</b></label>
						                    <input type="text" name="telegram_channel" value="{{ $model->telegram_channel }}"  class="form-control form-control-lg">
						                </div>
						                <div class="col-sm-12 mb-3">
						                    <label> <b>Manzil UZ</b></label>
						                    <input type="text" name="address_uz" value="{{ $model->address_uz }}"  class="form-control form-control-lg">
						                </div>
										<div class="col-sm-12 mb-3">
						                    <label> <b>Manzil RU</b></label>
						                    <input type="text" name="address_ru" value="{{ $model->address_ru }}"  class="form-control form-control-lg">
						                </div>
						                 <div class="col-sm-12 mb-3">
						                    <label> <b>Fililallar</b></label>
						                    <input type="number" name="count_office" value="{{ $model->adcount_officedress }}"  class="form-control form-control-lg">
						                </div>
						                 <div class="col-sm-12 mb-3">
						                    <label> <b>Min-bank</b></label>
						                    <input type="number" name="count_min_office" value="{{ $model->count_min_office }}"  class="form-control form-control-lg">
						                </div>
						                 <div class="col-sm-12 mb-3">
						                    <label> <b>INN</b></label>
						                    <input type="text" name="inn" value="{{ $model->inn }}"  class="form-control form-control-lg">
						                </div>

						                 <div class="col-sm-12 mb-3">
						                    <label> <b>СВИФТ</b></label>
						                    <input type="text" name="svift" value="{{ $model->svift }}"  class="form-control form-control-lg">
						                </div>
						                 <div class="col-sm-12 mb-3">
						                   <button type="submit" class="btn btn-info ">
						                        @lang('lang.save')
						                    </button>
						                </div>
						                
						            </div>
						        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
           /* $('.delete').on('click', function() {
                return confirm();
            });*/
            $('#menu-banks').addClass('active');

        });
    </script>
@endsection
