@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.stock.index') }}">
            <i class="ti-exchange-vertical"></i>
        </a>
    </li>

    <li class="breadcrumb-item">
        <a href="#">
            <i class="ti-pencil"></i>
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-body start -->
                <div class="page-body">
                    <!-- Basic table card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>O'zgartirish</h5>
                        </div>
                        <div class="card-block table-border-style">
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- Basic Form Inputs card start -->
                                    @if (session()->has('success'))
                                        <div class="alert alert-success">Saqlandi</div>
                                    @endif
                                    <div class="card">
                                        <div class="card-block">

                                            <form action={{ route('stock.save', !empty($stock) ? $stock->id : null) }}
                                                method="post">
                                                {{ csrf_field() }}

                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label ">Firma
                                                        nomi</label>
                                                    <div class="col-sm-10">
                                                        <input type="text"
                                                            class="form-control {{ $errors->has('name') ? 'form-control-danger' : null }}"
                                                            placeholder="Firma nomi"
                                                            value="{{ !empty($stock) ? $stock->name : old('name') }}"
                                                            name="name">

                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">Ordinary Stock Price</label>
                                                    <div class="col-sm-10">
                                                        <input type="text"
                                                            class="form-control {{ $errors->has('ordinary_stock_price') ? 'form-control-danger' : null }}"
                                                            placeholder="Ordinary Stock Price" name="ordinary_stock_price"
                                                            value="{{ !empty($stock) ? $stock->ordinary_stock_price : old('ordinary_stock_price') }}">

                                                    </div>
                                                </div>


                                                <div class="form-group row ">
                                                    <label class="col-sm-2 col-form-label">Preferred Stock Price</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="preferred_stock_price"
                                                            class="form-control {{ $errors->has('preferred_stock_price') ? 'form-control-danger' : null }}"
                                                            placeholder="Preferred Stock Price" name="ordinary_stock_price"
                                                            value="{{ !empty($stock) ? $stock->preferred_stock_price : old('preferred_stock_price') }}">


                                                    </div>
                                                </div>


                                                <div
                                                    class="form-group row {{ $errors->has('branch_id    ') ? 'form-danger' : null }}">
                                                    <label class="col-sm-2 col-form-label">Sanoat</label>
                                                    <div class="col-sm-10">
                                                        <select name="branch_id" class="form-control" name="branch_id">
                                                            @foreach ($branches as $key => $v)
                                                                <option value="{{ $key }}"
                                                                    {{ !empty($stock) && $key == $stock->branch_id ? 'selected="selected"' : null }}>
                                                                    {{ $v }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">Xorijiy</label>
                                                    <div class="col-sm-10">
                                                        <select name="foreign" class="form-control" name="branch_id">
                                                            @foreach ([0 => 'Ozbekiston', 1 => 'Xorijiy'] as $key => $v)
                                                                <option value="{{ $key }}"
                                                                    {{ !empty($stock) && $key == $stock->foreign ? 'selected="selected"' : null }}>
                                                                    {{ $v }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">Dividend</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="dividend"
                                                            class="form-control {{ $errors->has('dividend') ? 'form-control-danger' : null }}"
                                                            placeholder="Dividend" name="ordinary_stock_price"
                                                            value="{{ !empty($stock) ? $stock->dividend : old('dividend') }}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <button class="btn btn-success" type="submit">Saqlash</button>
                                                    <a href="{{ route('admin.stock.index') }}"
                                                        class="btn btn-primary">Orqaga</a>
                                                </div>
                                            </form>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @section('script')
        <script type="text/javascript">
            // console.log('HelloWorld');
            $(document).ready(function() {
                $('#dataTable').DataTable({
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': false,
                    'autoWidth': true,
                    "dom": '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
                })
            });

        </script>

    @endsection
