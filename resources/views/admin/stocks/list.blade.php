@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.stock.index') }}">
            <i class="ti-exchange-vertical"></i>
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-body start -->
                <div class="page-body">
                    <!-- Basic table card start -->
                    <div class="card">
                        @if (session()->has('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                        @endif
                        <div class="card-header">
                            <h5>Aksiyalar</h5>
                            <div class="card-header-right">
                                <a class="btn btn-info p-1 text-white pl-2 pr-3" href="{{ route('stock.form') }}">
                                    <i class="fa fa-plus-circle"> </i>
                                    Yangini qo'shish </a>
                            </div>
                        </div>
                        <div class="card-block table-border-style">
                            <table class="table-responsive-xl table-borderless table table-hove" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Firma nomi</th>
                                        <th>Aksiya Bahosi</th>
                                        <th>Dividend</th>
                                        <th>Sanoat</th>
                                        <th>Boshqarish</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($stocks as $stock)
                                        <tr>
                                            <th scope="row">{{ $stock->id }}</th>
                                            <td>{{ $stock->name }}</td>
                                            <td>{{ $stock->ordinary_stock_price }}</td>
                                            <td>{{ $stock->dividend }} %</td>
                                            <td width="50">
                                                <div class="label label-success">
                                                    {{ $stock->branch ? $stock->branch->title_uz : 'Tanlanmagan' }}
                                                </div>
                                            </td>

                                            <td class="text-right">
                                                <a href="{{ route('stock.form', $stock->id) }}"
                                                    class="btn btn-primary p-1 pl-2 text-white"><i
                                                        class="fa fa-pencil"></i></a>
                                                <a href="{{ route('stock.delete', $stock->id) }}"
                                                    class="btn btn-danger delete p-1 pl-2 text-white"><i
                                                        class="fa fa-trash"></i></a>

                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @section('script')
        <script src="{{ asset('js/admin/stocks/index.js') }}"></script>
    @endsection
