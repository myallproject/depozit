@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">@lang('lang.offices')</a>
    </li>
@endsection

@section('content')

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.offices')</h5>
                                    <div class="card-header-right">
                                        <a href="" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                    </div>
                                </div>
                                <div class="card-block font-size-14">
                                    <div class="row ml-3 mr-3">
                                        <div class="col-sm-12 pt-4 ">
                                            <form action="{{ route('organization-branches-edit') }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <input id="org-id" type="hidden" name="org_id" value="{{ $organization->id }}">
                                                <input type="hidden" name="id" value="{{ $branch->id }}">
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Tashkilot</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <input disabled value="{{ $organization->name() }}" class="form-control form-control-lg" required="required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Filial nomi</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <input type="text" name="name_uz" value="{{ $branch->getName('uz') }}" class="form-control form-control-lg" required="required" placeholder="Name UZ">
                                                       <br>
                                                       <input type="text" name="name_ru" value="{{ $branch->getName('ru') }}" class="form-control form-control-lg" required="required" placeholder="Name UZ">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Tekefon raqami</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <input type="text" name="phone" value="{{ $branch->phone }}" class="form-control form-control-lg" required="required" placeholder="+99 999 999 99 99">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>MFO</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <input type="text" name="mfo" value="{{ $branch->getMFO() }}" class="form-control form-control-lg" required="required" placeholder="MFO">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>STIR</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <input type="text" name="stir" value="{{ $branch->stir }}" class="form-control form-control-lg" required="required" placeholder="STIR">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Hudud</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <select name="region_id" class="form-control form-control-lg" required="required" placeholder="Tanlang">
                                                      		@foreach($regions as $region)
							                                    <option class="border-bottom" @if($region->id == $branch->region_id) selected @endif style="font-weight: bold;" value="{{ $region->id }}">{{ $region->name() }}</option>
							                                    @foreach($region->children as $child_r)
							                                        <option @if($child_r->id == $branch->region_id) selected @endif value="{{ $child_r->id }}">&nbsp;&nbsp;{{ $child_r->name() }}</option>
							                                    @endforeach
							                                @endforeach

                                                       </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Manzil</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <input type="text" name="address_uz" value="{{ $branch->getAddress('uz') }}" class="form-control form-control-lg" required="required" placeholder="Manzil UZ">
                                                       <br>
                                                       <input type="text" name="address_ru" value="{{ $branch->getAddress('ru') }}" class="form-control form-control-lg" required="required" placeholder="Manzil UZ">
                                                    </div>
                                                </div> 

                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Xizmatlar</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <textarea  name="services_uz" rows="4" class="form-control form-control-lg" required="required" placeholder="Xizmatlar UZ">{{ $branch->getServices('uz') }}</textarea>
                                                       <br>
                                                       <textarea name="services_ru" rows="4" class="form-control form-control-lg" required="required" placeholder="Xizmatlar UZ">{{ $branch->getServices('ru') }}</textarea>
                                                    </div>
                                                </div>
                                                 <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label><b>Lang & Lat</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                    	<sub><b>Lang</b></sub>
                                                        <input  name="lang" type="text" value="{{ $branch->lang }}" class="form-control form-control-lg">
                                                        <br>
                                                        <sub><b>Lat</b></sub>
                                                        <input name="lat" type="text" value="{{ $branch->lat }}" class="form-control form-control-lg">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3"></div>
                                                    <div class="col-sm-8 mb-3">
                                                        <button type="submit" class="btn btn-info ">
                                                            @lang('lang.save')
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script>
        $(document).ready(function () {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#menu-banks').addClass('active');
      
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <style>
        .float-right{
            float:right !important;
        }
    </style>
@endsection
