@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">
            <i class="fa fa-bank"></i>
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.offices')</h5>
                                    <div class="card-header-right">
                                        <a href="{{ route('organization-branches-add',[$organization->id]) }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table id="org-table" class="table-responsive-xl table-borderless table table-hover" style="font-size: 13px !important;">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('lang.region')</th>
                                            <th>@lang('lang.name')</th>
                                            <th>@lang('lang.phone')</th>
                                            <th>@lang('lang.services')</th>
                                            <th>@lang('lang.mfo')</th>
                                            <th >O'rni</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($branches as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td>{{ $row->region->name() }}</td>
                                                <td>{{ $row->getName() }}</td>
                                               	<td>{{ $row->phone }}</td>
                                               	<td>{{ $row->getServices() }}</td>
                                               	<td>{{ $row->getMFO() }}</td>
                                                <td><input style="width:50px" data-id="{{ $row->id }}" name="positon" value="{{ $row->position }}" class="form-control position-class" id="postion-id-{{ $row->id }}"></td>
                                                <td class="text-right">
                                                    <a href="{{ route('organization-branches-update',[$row->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')"+"bu bankga tegishli bulgan barcha ma'lumot va xizmatlar ma'lumotlar bazasidan o'chadi!");
            });
            $('#menu-banks').addClass('active');

            $('.position-class').on('change',function(){
                $.ajax({
                    method: 'GET',
                    url: "{{ route('row-position') }}",
                    data:{
                        model: 'BankOffices',
                        id: $(this).attr('data-id'),
                        position: $(this).val(),
                    }
                }).done(function(data){
                });
            });
        });

        $(function () {
            $('#org-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });
    </script>
@endsection
