@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{route('admin-telephone-list')}}"><i class="fa fa-bar-chart"></i></a>
    </li>
    <li class="breadcrumb-item">
        <a>Telefondagi ehtimoliy xatolar</a>
    </li>
@endsection

@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">

                                    <h5>Telefondagi magazinlardagi ehtimoliy xatolar</h5>
                                    <div class="card-header-right col-md-3">
                                        <a href="{{route('admin-telephone-list')}}" class="btn btn-primary p-2 text-white pl-2 pr-3 btn-block"><i class="fa fa-cogs"></i>&nbsp;&nbsp;To'g'irlandi</a>
                                    </div>
                                </div>
                                @foreach ($problem_with_shop as $key=>$value)
                                    <div class="card-block font-size-14 pt-2">                                    
                                        <a class="btn waves-effect waves-light all-status-btn hor-grd pl-2 p-1  pr-3 btn-grd-success">{{ $shops_to_send[$key]->name_uz }}</a>                                     
                                    </div>
                                    <div class="card-block font-size-14">
                                        <table  class="table-responsive-xl table-borderless table table-hover"  style="font-size: 12px !important;">
                                            <thead>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" class="translate-all" name="all_credit"/> @lang('lang.all')
                                                </th>
                                                <th>ID </th>
                                                <th>Brend Nomi</th>
                                                <th>Model Nomi</th>
                                                <th>Ichki Xotira</th>
                                                <th>Operativ Xotira</th>
                                                <th>O'lchami</th>
                                                <th>Og'irligi</th>
                                                <th class="text-right">@lang('lang.control')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($value as $problem)
                                                    <form method="GET">
                                                        @php
                                                            $i = 0;
                                                        @endphp
                                                        @foreach ($problem as $index=>$tel)
                                                        @if($i++==0)
                                                            @php
                                                                $shop_tel_id = $tel->id;
                                                            @endphp 
                                                                <tr>
                                                                    <td>
                                                                        <input type="checkbox" data-status="check" class="translate" name="telephones[]" value="{{ $tel->id }}" />
                                                                    </td>
                                                                    <td>{{ $tel->id }}</td>
                                                                    <td>{{ $tel->brand->name }}</td>
                                                                    <td>{{ $tel->smartphone_model}}</td>
                                                                    <td>{{ $tel->smartphone_inner_memory}}</td>
                                                                    <td>{{ $tel->smartphone_operation_memory }}</td>
                                                                    <td>{{ $tel->smartphone_dimension }}</td>
                                                                    <td>{{ $tel->smartphone_weight }} </td>
                                                                     <td class="text-right"> 
                                                                        <a href="{{ route('admin-telephone-add-usingShop',[$tel->id]) }}" class="btn btn-primary p-1 pl-2 text-white">Yangi telefon qo'shish</a>                                                                       
                                                                        <a href="{{ route('telephone-delete',[$tel->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                                    </td>
                                                                </tr>
                                                        @else
                                                            <input type="text" style="display: none" name="id_{{$index}}" value="{{$tel->id}}">
                                                            <tr>
                                                                <td>
                                                                    <input type="checkbox" data-status="check" class="translate" name="telephones[]" value="{{ $tel->id }}" />
                                                                </td>
                                                                <td>{{ $tel->id }}</td>
                                                                <td>{{ $tel->brand->name }}</td>
                                                                <td><input type="text" class="form-control" name="model_{{$index}}" value="{{ $tel->smartphone_model}}"></td>
                                                                <td>{{ $tel->smartphone_inner_memory}}</td>
                                                                <td>{{ $tel->smartphone_operation_memory }}</td>
                                                                <td>{{ $tel->smartphone_dimension }}</td>
                                                                <td>{{ $tel->smartphone_weight }} </td>
                                                            <td class="text-right">
                                                                    <a href="{{ route('telephone-shop-match',['shop_id' => $shop_tel_id,'id' => $tel->id]) }}" class="btn btn-warning p-1 pl-2 text-white">Mos Keladi</a>
                                                                    <a href="{{ route('telephone-update-form',[$tel->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                                    <a href="{{ route('telephone-delete',[$tel->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endif                                                        
                                                        @endforeach    
                                                        @if($i==1)
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><button class="btn btn-block btn-success modelbtn" type="submit">Saqlash</button></td>
                                                                <td></td>
                                                            </tr>
                                                        @endif                                             
                                                        
                                                    </form>                                                
                                                @endforeach
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
    {{-- success model --}}
    @php
        if((isset($errors) && $errors->any()) || session()->has('success')) $status = 1;
        else $status = 0;
    @endphp
    <a id="modalinfo" data-toggle="modal" data-target="#info" data-status="{{$status}}" style="display: none;"></a>
    <div id="info" class="modal fade" role="dialog">
        <div class="modal-dialog " style="max-width: 600px">            
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modal-title" class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        @if(isset($errors) && $errors->any()) 
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}} </li>
                                @endforeach                               
                            </ul>                            
                        </div> 
                        @endif
                        @if(session()->has('success'))
                            <div class="alert alert-info">
                                {{session('success')}}
                            </div>
                        @endif                      
                    </div>                    
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')

    <script>
        $(document).ready(function () {
            //checking modal for errors
            let infoBtn = $('#modalinfo');
            let infoStatus = infoBtn.attr('data-status');
            if(infoStatus == 1) {
                infoBtn.click();
            }            
            //

            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#rassrochka-parent').addClass('active pcoded-trigger');
            $('#rassrochka-child4').addClass('active');
            $('form').on("submit", function(e) {
                let form = $(this);
                e.preventDefault();
                $('.modelbtn').removeClass('btn-success');
                $.ajax({
                    type:form.attr('method'),
                    url: "{{route('admin-telephone-model_edit')}}",
                    data:form.serialize(),
                    success: function(data){
                        if(data.error){
                            $.each(data.error, (index, value) => {
                                console.error(index+") "+value);
                            })
                        } else {
                            $('.modelbtn').addClass('btn-success');
                        }
                    }
                })
            })
        });

    </script>
    <style>
        .float-right{
            float:right !important;
        }
    </style>
@endsection
