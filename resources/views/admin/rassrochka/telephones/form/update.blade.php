@php
    App::setLocale('uz');
    $array = Lang::get('lang.rassrochka.telephone');
@endphp
<div class="row">
    <div class="col-sm-12 pt-4 pl-2">
        <form action="{{ route('telephone-update', ['id' => $telephone->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>{{$array['model']}} (Kiritilishi Shart)</b></label>
                    <input required="required" type="text" name="model" class="form-control form-control-lg" value="{{$telephone->smartphone_model}}">
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>{{$array['brand']}}</b></label>

                    <select type="text" name="brand_id" class="form-control form-control-lg">
                        @if($brand !== null) <option @if($brand->id == $telephone->brand_id) selected @endif value="{{$brand->id}}">{{$brand->name}}</option>@endif
                        @foreach($brands as $b)
                            <option @if($b->id == $telephone->brand_id) selected @endif value="{{ $b->id }}"> {{ $b->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @php
                unset($array['model']);
                unset($array['brand']);
                $i = 1;
            @endphp
            @foreach ($array as $key => $value)
            @php
                $tableValue = 'smartphone_' . $key;
            @endphp
                @if($i++%2 == 1)
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3">
                            <label> <b>{{$value}}</b></label>
                            <input type="text" name="{{$key}}" value="{{ $telephone->$tableValue }}" class="form-control form-control-lg">
                        </div>
                @else 
                        <div class="col-sm-6 mb-3">
                            <label> <b>{{$value}}</b></label>
                            <input type="text" name="{{$key}}" value="{{$telephone->$tableValue}}" class="form-control form-control-lg">
                        </div>
                    </div>
                @endif
            @endforeach
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-info ">
                        @lang('lang.save')
                    </button>
                </div>
            </div>            
        </form>
    </div>
</div>