<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
 

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <link rel="icon" href="{{ asset('admin/assets/images/favicon.ico') }}" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/bootstrap/css/bootstrap.min.css') }}">
    <!-- waves.css -->
    <link rel="stylesheet" href="{{ asset('admin/assets/pages/waves/css/waves.min.css') }}" type="text/css" media="all">
    <!-- feather icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/assets/icon/feather/css/feather.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/icon/themify-icons/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/icon/font-awesome/css/font-awesome.min.css') }}">
    <!-- Style.css -->
    <link href="/plugins/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <style>

    </style>
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/widget.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/Ionicons/css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/plugins/icon.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    @yield('style')
    <style type="text/css">
        .select2-container { 
        width: 100% !important;
         }

        .hide{
            display: none;
        }    
    </style>
</head>
<body>

<div class="loader-bg">
    <div class="loader-bar"></div>
</div>
<!-- [ Pre-loader ] end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        <!-- [ Header ] start -->
        <nav class="navbar header-navbar pcoded-header">
            <div class="navbar-wrapper">
                <div class="navbar-logo" style="text-transform: inherit !important;">
                    <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                        <i class="feather icon-toggle-right"></i>
                    </a>
                    <a href="" style="font-size: 22px !important;">
                        Dashboard
                    </a>
                    <a class="mobile-options waves-effect waves-light">
                        <i class="feather icon-more-horizontal"></i>
                    </a>
                </div>
                <div class="navbar-container container-fluid">
                    <ul class="nav-left">
                        <li class="header-search">
                            <div class="main-search morphsearch-search">
                                <div class="input-group">
                                        <span class="input-group-prepend search-close">
										<i class="feather icon-x input-group-text"></i>
									</span>
                                    <input type="text" class="form-control" placeholder="Enter Keyword">
                                    <span class="input-group-append search-btn">
										<i class="feather icon-search input-group-text"></i>
									</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                <i class="full-screen feather icon-maximize"></i>
                            </a>
                        </li>

                    </ul>

                    <ul class="nav-right">

                        <li class="user-profile header-notification">
                            <div class="dropdown-primary dropdown">
                                <div class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="/admin/assets/images/admin.jpg" class="img-radius" alt="User-Profile-Image">
                                    <span>{{ Auth::user()->name }}</span>
                                    <i class="feather icon-chevron-down"></i>
                                </div>
                                <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                    <li>
                                        <a href="#">
                                            <i class="feather icon-settings"></i> @lang('lang.settings')
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="feather icon-user"></i> @lang('lang.profile')
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="feather icon-log-out"></i> @lang('lang.logout')
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>

                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
        <!-- [ Header ] end -->

        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <!-- [ navigation menu ] start -->
            @include('admin.sidebar')
                <div class="pcoded-content">
                    <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-8">
                                    <div class="page-header-title">
                                        <h4 class="m-b-10">@lang('lang.admin_panel')</h4>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('dashboard') }}">
                                                <i class="feather icon-home"></i>
                                            </a>
                                        </li>
                                       @yield('breadcrumb')
                                    </ul>
                                </div>
                                {{-- <div class="col-md-4 text-right" >
                                    <div class="card-header-right mb-2 mr-2">
                                        <div class="btn-group " role="group" >
                                            <a href="{{ url('locale',['locale'=>'uz']) }}" class="btn btn-outline-success text-white btn-sm waves-effect waves-light {!! App::getLocale() == 'uz' ? "active" : "" !!}">Uzbek</a>
                                            <a href="{{ url('locale',['locale'=>'ru']) }}" class="btn btn-outline-success  text-white btn-sm waves-effect waves-light {!! App::getLocale() == 'ru' ? "active" : "" !!}">Russian</a>
                                        </div>
                                    </div>
                                </div> --}}

                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->
                   @yield('content')
                </div>
                <!-- [ style Customizer ] start -->
                <div id="styleSelector">
                </div>
                <!-- [ style Customizer ] end -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('admin/assets/js/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/js/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/js/popper.js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/assets/js/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- waves js -->
<script src="{{ asset('admin/assets/pages/waves/js/waves.min.js') }}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{ asset('admin/assets/js/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
<!-- Float Chart js -->
<script src="{{ asset('admin/assets/js/float/jquery.flot.js') }}"></script>
<script src="{{ asset('admin/assets/js/float/jquery.flot.categories.js') }}"></script>
<script src="{{ asset('admin/assets/js/float/curvedLines.js') }}"></script>
<script src="{{ asset('admin/assets/js/float/jquery.flot.tooltip.min.js') }}"></script>
<!-- amchart js -->
<script src="{{ asset('admin/assets/pages/widget/amchart/amcharts.js') }}"></script>
<script src="{{ asset('admin/assets/pages/widget/amchart/serial.js') }}"></script>
<script src="{{ asset('admin/assets/pages/widget/amchart/light.js') }}"></script>
<script src="{{ asset('plugins/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Custom js -->
<script src="{{ asset('admin/assets/js/pcoded.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/vertical/vertical-layout.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('admin/assets/pages/dashboard/custom-dashboard.min.js') }}"></script>

<script src="{{ asset('/plugins/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/plugins/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
    $('.select2').select2();
</script>

@yield('script')

<script type="text/javascript" src="{{ asset('admin/assets/js/script.min.js') }}"></script>
</body>
</html>

