@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href={{ route('services-list') }}>
            @lang('lang.services')
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.services')</h5>
                                    <div class="card-header-right">
                                        <a href="{{ route('service-add-form') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table class="table-responsive-xl table-borderless table table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('lang.name')</th>
                                            <th>@lang('lang.parent')</th>
                                            <th>O'rni</th>
                                            <th>@lang('lang.status')</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($services as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td>{{ $row->getName() }}</td>
                                                <td>{{ $row->parent['name_uz'] }}</td>
                                                <td><input style="width:50px" data-id="{{ $row->id }}" name="positon" value="{{ $row->position }}" class="form-control position-class" id="postion-id-{{ $row->id }}"></td>
                                                <td>
                                                    
                                                     <select name="active" data-id="{{ $row->id }}" data-action="{{ route('admin-service-status-change') }}" class="form-control change-status">
                                                       
                                                        <option @if($row->status == 1) selected @endif value="1">Active</option>
                                                        <option @if($row->status == 0) selected @endif value="0">Block</option>
                                                    </select>
                                                    <div id="status-{{ $row->id }}" style="display: none" class="alert alert-success"></div>

                                                </td>
                                                <td class="text-right">
                                                    <a href="{{ route('service-update-form',[$row->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('service-delete',[$row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#menu-addition').addClass('active pcoded-trigger');
            $('#menu-service').addClass('active');

             $('.change-status').on('change',function(){
                $.ajax({
                    method: 'GET',
                    url: $(this).attr('data-action'),
                    data:{
                        'status':$(this).val(),
                        'id': $(this).attr('data-id')
                    }
                }).done(function(data){
                    $('#status-'+data.id).text('Msg: '+data.msg+', Status:'+data.status+', Id:'+data.id).show();
                    setTimeout(function(){$('#status-'+data.id).hide();},5000);
                });
             });

            $('.position-class').on('change',function(){
                $.ajax({
                    method: 'GET',
                    url: "{{ route('row-position') }}",
                    data:{
                        model: 'Service',
                        id: $(this).attr('data-id'),
                        position: $(this).val(),
                    }
                }).done(function(data){
                });
            });
         });
    </script>
@endsection
