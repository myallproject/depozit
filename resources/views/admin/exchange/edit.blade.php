@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('rate-list') }}">
            @lang('lang.bank') @lang('lang.currency_exchange_rate')
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="">
            {!! !empty($exchange) ? trans('lang.edit') : trans('lang.add_new') !!}
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{!! !empty($exchange) ? trans('lang.edit') : trans('lang.add_new') !!}</h5>
                                    @if($exchange)
                                        <div class="card-header-right">
                                            <a href="{{ route('rate-add-form') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-12">
                                    @if($exchange)
                                        @include('admin.exchange.form.update')
                                    @else
                                        @include('admin.exchange.form.add')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#menu-rate').addClass('active ');
            $('.check-type-exchange').each(function(i,v){
                if($(this).attr('data-status') == 'true'){
                    $('#other_take_'+$(this).attr('data-id')).removeAttr('disabled');
                    $('#other_sale_'+$(this).attr('data-id')).removeAttr('disabled');
                }
            });
        });

        $('.check-type-exchange').on('click',function(){
            let data_status = $(this).attr('data-status');
            let data_id = $(this).attr('data-id');
            if(data_status == 'false'){
                $(this).attr('data-status','true');
                $('#other_take_'+data_id).removeAttr('disabled');
                $('#other_sale_'+data_id).removeAttr('disabled');
            } else if(data_status == 'true'){
                $(this).attr('data-status','false');
                $('#other_take_'+data_id).attr('disabled','disabled');
                $('#other_sale_'+data_id).attr('disabled','disabled');
            }
        });
    </script>
@endsection
