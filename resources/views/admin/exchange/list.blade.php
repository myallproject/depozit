@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('rate-list') }}">@lang('lang.bank') @lang('lang.currency_exchange_rate')</a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.currency_exchange_rate')</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-info p-1 text-white pl-2 pr-3" href="{{ route('rate-add-form') }}">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>

                                {{--<div class="card-block">
                                    <table class="table-responsive-xl table-borderless table table-hover" id="exchange-table">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('lang.bank')</th>
                                            <th>@lang('lang.currency')</th>
                                            <th>@lang('lang.take')</th>
                                            <th>@lang('lang.sale')</th>
                                            <th>@lang('lang.time')</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($list as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td>{{ $row->bank->name_uz }}</td>
                                                <td>{{ $row->currencyName->name }}</td>
                                                <td>{{ $row->take }}</td>
                                                <td>{{ $row->sale }}</td>
                                                <td>{{ $row->updated_at }}</td>
                                                <td class="text-right">
                                                    <a href="{{ route('rate-update-form',[$row->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('rate-delete',[$row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>--}}

                                <div class="card-block">
                                    <div>
                                        <input type="search"
                                                class="search-table"
                                                name="search-table"
                                                id="search-table"
                                                results=5 placeholder="Search..."
                                                style="height: 30px;
                                                width: 50%;
                                                border: 1px solid #dedede;
                                                border-radius: 5px;
                                                margin-bottom: 10px;
                                                padding: 5px; 10px">
                                    </div>
                                    <table class="table-responsive-xl table-borderless table table-hover" id="exchange-table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>@lang('lang.bank')</th>
                                                <th>@lang('lang.currency')</th>
                                                <th>@lang('lang.take')</th>
                                                <th>@lang('lang.sale')</th>
                                                <th>@lang('lang.time')</th>
                                                <th class="text-right">@lang('lang.control')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($banks as $bank)
                                            <tr>
                                                <td>{{ $bank->id }}</td>
                                                <td colspan="5">{{ $bank->name_uz }}</td>
                                                <td class="text-right">
                                                    <a href="{{ route('rate-update-form-all',[$bank->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i> @lang('lang.edit')</a>
                                                </td>
                                            </tr>
                                            @foreach($list as $row)
                                                @if($bank->id == $row->bank_id)
                                                <tr>
                                                    <td>{{ $row->id }}</td>
                                                    <td></td>
                                                    <td>{{ $row->currencyName->name }}</td>
                                                    <td>{{ $row->take }}</td>
                                                    <td>{{ $row->sale }}</td>
                                                    <td>{{ $row->updated_at }}</td>
                                                    <td class="text-right">
                                                        <a href="{{ route('rate-delete',[$row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            @foreach($other_rate as $rate)
                                                @if($bank->id == $rate->bank_id)
                                                <tr>
                                                    <td>{{ $rate->id }}</td>
                                                    <td></td>
                                                    <td>{{ $rate->currency->name }}<br>{{ $rate->typeExchange->name() }}</td>
                                                    <td>{{ $rate->take }}</td>
                                                    <td>{{ $rate->sale }}</td>
                                                    <td>{{ $rate->updated_at }}</td>
                                                    <td class="text-right">
                                                        <a href="{{ route('other-rate-delete',[$rate->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endforeach

                                        </tbody>
                                        {{-- <tfoot>
                                        {{ $banks->links() }}
                                        </tfoot> --}}
                                    </table>
                                </div>


                            </div>

                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#menu-rate').addClass('active');
            $("#search-table").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#exchange-table tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });

        $(function () {
            /*$('#exchange-table').DataTable({
                'paging'      : true,
                'searching'   : true,
                'lengthChange': true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            });*/

        });
    </script>
@endsection
