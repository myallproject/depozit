@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('rate-list') }}">
            @lang('lang.bank') @lang('lang.currency_exchange_rate')
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="">
            {!!  trans('lang.edit')  !!}
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{!!  trans('lang.edit') !!}, {{ $bank->name_uz }}</h5>
                                </div>
                                @if(count($model) > 0)
                                   <form action="{{ route('rate-update-edit-all') }}" method="POST" >
                                       <div class="col-md-12 form-group row">
                                       @csrf
                                       <div class="col-sm-6 ">
                                           @foreach($model as $row)
                                               <input type="hidden"  name="ids[]" value="{{ $row->id }}">
                                               <div class="form-group col-md-12 row " style="border-bottom: solid 1px" >
                                                   <div class="col-md-12"><label><b>{{ $row->currencyName->name }}</b></label></div>
                                                   <div class="col-sm-6 mb-3">
                                                       <label> <b>@lang('lang.take')</b></label>
                                                       <input name="take_{{ $row->id }}"  value="{{ $row->take }}" class="form-control form-control-lg" required="required">
                                                   </div>
                                                   <div class="col-sm-6 mb-3">
                                                       <label> <b>@lang('lang.sale')</b></label>
                                                       <input name="sale_{{ $row->id }}" value="{{ $row->sale }}" class="form-control form-control-lg" required="required">
                                                   </div>
                                                   <div class="col-sm-12">
                                                       <label><b>Boshqa kurslar</b></label>
                                                   </div>
                                                   <div class="col-md-12 form-group row ">
                                                    @foreach($otherRateExchange as $oth)
                                                        @if($oth->currency_id == $row->currency )
                                                           <div class="col-sm-12 ">
                                                              <label>{{ $oth->typeExchange->name() }}</label>
                                                           </div>
                                                            <input type="hidden" name="other_ids[]" value="{{ $oth->id }}">
                                                           <div class="col-sm-6">
                                                               <label>@lang('lang.take')</label>
                                                               <input type="text" name="other_take_{{ $oth->id }}"  class="form-control  form-control-lg" value="{{ $oth->take }}">
                                                           </div>
                                                           <div class="col-sm-6">
                                                               <label>@lang('lang.sale')</label>
                                                               <input type="text" name="other_sale_{{ $oth->id }}" class="form-control  form-control-lg" value="{{ $oth->sale }}">
                                                           </div>
                                                           @endif
                                                    @endforeach
                                                   </div>
                                               </div>
                                           @endforeach
                                       </div>



                                </div>
                                       <div class="form-group row">
                                           <div class="col-sm-12 mb-3">
                                               <button type="submit" class="btn btn-info ">
                                                   @lang('lang.save')
                                               </button>
                                           </div>
                                       </div>
                                   </form>

                                @else
                                    @lang('lang.not_found')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#menu-rate').addClass('active ');
        });

        $('.check-type-exchange').on('click',function(){
            let data_status = $(this).attr('data-status');
            let data_id = $(this).attr('data-id');
            if(data_status == 'false'){
                $(this).attr('data-status','true');
                $('#other_take_'+data_id).removeAttr('disabled');
                $('#other_sale_'+data_id).removeAttr('disabled');
            } else if(data_status == 'true'){
                $(this).attr('data-status','false');
                $('#other_take_'+data_id).attr('disabled','disabled');
                $('#other_sale_'+data_id).attr('disabled','disabled');
            }
        });
    </script>
@endsection
