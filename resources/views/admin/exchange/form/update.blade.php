<div class="row ml-5">
    <div class="col-sm-8 pt-4 ">
        <form action="{{ route('rate-update') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ $exchange->id }}">
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.bank')</b></label>
                    <select name="bank" class="form-control form-control-lg" required="required">
                        <option value="">@lang('lang.choose')</option>
                        @foreach($bank as $b)
                            <option @if($b->id == $exchange->bank_id) selected="selected" @endif value="{{ $b->id }}">{{ $b->name() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.currency')</b></label>
                    <select name="currency" class="form-control form-control-lg" required="required">
                        <option value="">@lang('lang.choose')</option>
                        @foreach($currency as $c)
                            <option @if($c->id == $exchange->currency) selected="selected" @endif value="{{ $c->id }}">{{ $c->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.take')</b></label>
                    <input name="take" class="form-control form-control-lg"  value="{{ $exchange->take }}"  required="required">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.sale')</b></label>
                    <input name="sale" class="form-control form-control-lg" value="{{ $exchange->sale }}" required="required">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-info ">
                        @lang('lang.save')
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-sm-4 pt-4">
        <div class="form-group ">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>

