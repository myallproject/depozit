<div class=" col-md-12 ml-5">
        <form action="{{ route('rate-add') }}" method="POST" enctype="multipart/form-data">
            <div class="col-md-12 p-0 row">
                <div class="col-sm-6 pt-4 ">
                    @csrf
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3">
                            <label> <b>@lang('lang.bank')</b></label>
                            <select name="bank" class="form-control form-control-lg" required="required">
                                <option value="">@lang('lang.choose')</option>
                                @foreach($bank as $b)
                                    <option value="{{ $b->id }}">{{ $b->name() }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3">
                            <label> <b>@lang('lang.currency')</b></label>
                            <select name="currency" class="form-control form-control-lg" required="required">
                                <option value="">@lang('lang.choose')</option>
                                @foreach($currency as $c)
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3">
                            <label> <b>@lang('lang.take')</b></label>
                            <input type="text" name="take" class="form-control form-control-lg" required="required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3">
                            <label> <b>@lang('lang.sale')</b></label>
                            <input type="text" name="sale" class="form-control form-control-lg" required="required">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 pt-4 ">
                    <div class="form-group row">
                        <h5>Boshqa kurslar</h5>
                        @foreach($typeRateExchange as $t_rate)
                            <div class="col-md-12 mt-3">
                                <label>{{ $t_rate->name() }}</label>
                                <input class="check-type-exchange" data-id="{{ $t_rate->id }}" type="checkbox" data-status="false" name="type_exchange[]" value="{{ $t_rate->id }}">
                            </div>

                            <div class="col-sm-6">
                                <label> <b>@lang('lang.sale')</b></label>
                                <input id="other_take_{{ $t_rate->id }}" type="text" name="other_sale_{{$t_rate->id}}"  disabled class="form-control form-control-lg">
                            </div>
                            <div class="col-sm-6 mb-3">
                                <label> <b>@lang('lang.take')</b></label>
                                <input id="other_sale_{{ $t_rate->id }}"  type="text" name="other_take_{{$t_rate->id}}" disabled class="form-control form-control-lg">
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-info ">
                        @lang('lang.save')
                    </button>
                </div>
            </div>
        </form>

    <div class="col-sm-4 pt-4">
        <div class="form-group ">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>

