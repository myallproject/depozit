@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">
            <i class="fa fa-map"></i>
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card" style="font-size: 12px !important">
                                <div class="card-header">
                                    <h5>@lang('lang.question')</h5>
                                   {{--  <div class="card-header-right">
                                        <a href="{{ route('region-add-form') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                    </div> --}}
                                </div>
                                <div class="card-block font-size-14">
                                    <div class="row ml-3 mr-3">
                                        <div class="col-sm-12 pt-4 ">
                                            <form action="{{ route('admin-question-edit-post') }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <input id="question-id" type="hidden" name="id" value="{{ $question->id }}">
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Foydalanuvchi</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <input type="text" value="{{ $question->user->getName() }}" disabled class="form-control form-control-lg" required="required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Bank</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <input type="text" value="{{ $question->bank->name() }}" disabled class="form-control form-control-lg" required="required">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Xizmat turi</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                    	<select name="service" class="form-control select2 form-control-lg" required="required">
                                                    			<option value="">@lang('lang.choose')</option>
                                                    		@foreach($services as $service)
                                                    			<option @if($question->service_id == $service->id) selected @endif value="{{ $service->id }}">{{ $service->getName() }}</option>
                                                    		@endforeach
                                                    	</select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Savol</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <textarea rows="6" class="form-control form-control-lg" name="question" required="required">{{ $question->question }}</textarea>
                                                    </div>
                                                </div>
												
												<div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Bank uchun ma'lumot</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <textarea rows="6" class="form-control form-control-lg" name="info_bank" required="required">{{ $question->info_bank }}</textarea>
                                                    </div>
                                                </div>

                                              
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3"></div>
                                                    <div class="col-sm-8 mb-3">
                                                        <button type="submit" class="btn btn-info ">
                                                            @lang('lang.save')
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
  <script>
  	$(document).ready(function() {
        $('#menu-questions').addClass('active');
    });

      $(function () {
            $('#questions-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : false,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });
  </script>
@endsection
