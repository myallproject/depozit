@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">
            <i class="fa fa-map"></i>
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card" style="font-size: 12px !important">
                                <div class="card-header">
                                    <h5>@lang('lang.questions')</h5>
                                   {{--  <div class="card-header-right">
                                        <a href="{{ route('region-add-form') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                    </div> --}}
                                </div>
                                <div class="card-block">
                                    <table  id="questions-table" class="table-responsive-xl table-borderless table table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>User</th>
                                            <th>Bank</th>
                                            <th>Service</th>
                                            <th>Savol</th>
                                            <th>Javob</th>
                                            <th class="text-right" >@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($questions as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td>{{ $row->user->getName() }}</td>
                                                <td>{{ $row->bank->name() }}</td>
                                                <td>{{ $row->service->getName() }}</td>
                                                <td>{{ $row->question }}</td>
                                                <td> <a class="btn btn-success show-answer p-1 pl-2 text-white" data-id="{{ $row->id }}"><i class=" fa fa-angle-down"></i> </a></td>
                                               
                                                <td class="text-right">
                                                   
                                                    <a href="{{ route('admin-question-edit-view',['id'=>$row->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('admin-question-delete',['id'=>$row->id]) }}" class="btn delete btn-danger p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>


                                            </tr>

                                            <tr id="answer-content-{{ $row->id }}" class="hide">
                                                <td colspan="7">
                                                    @foreach($row->answers as $ans)
                                                    <div style="display: block; font-size: 12px !important">
                                                         <p style="display: block font-size: 12px !important"> <span id="answer-text-{{ $ans->id }}">{{ $ans->answer_text }}</span>
                                                            <a href="" class="edit-answer" data-toggle="modal" data-target="#answer-edit-modal"  data-id="{{ $ans->id }}" style="color: blue"><i class="fa fa-pencil"></i></a>
                                                            <a href="{{ route('admin-question-answer-delete',['id'=>$ans->id]) }}" class="delete" style="color: red"><i class="fa fa-trash"></i></a>
                                               
                                                            <span style="display: block"> <b>{{ $ans->user->getName() }}</b></span>
                                                         </p>
                                                    </div>
                                                    @endforeach
                                                </td>
                                                <td style="display: none"></td>
                                                <td style="display: none"></td>
                                                <td style="display: none"></td>
                                                <td style="display: none"></td>
                                                <td style="display: none"></td>
                                                <td style="display: none"></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

    <div id="answer-edit-modal" class="modal fade" role="dialog">
        <div class="modal-dialog " style="max-width: 500px">
            <!-- Modal content-->
            <form id="answer-edit-form" action="{{ route('admin-question-answer-edit') }}" method="POST">
                @csrf
                <input type="hidden" id="edit-answer-id" name="id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                       <div class="col-md-12">
                            <div class="form-group">
                                <label>Javob matni</label>
                                <textarea  rows="6" autocomplete="off" id="edit-answer-text" name="answer_text" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success save-comment" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning close-modal" data-dismiss="modal" >Yopish</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection

@section('script')
  <script>
      $(document).ready(function() {
          $('.delete').on('click', function() {
              return confirm("@lang('lang.confirmDel')");
          });
          $('#menu-questions').addClass('active');

          $('.show-answer').click(function(){
            id = $(this).attr('data-id');
            $('#answer-content-'+id).toggleClass('show').toggleClass('hide');
          });
          
          $('.edit-answer').click(function(){
             id = $(this).attr('data-id');
             text =  $('#answer-text-'+id).text();
             $('#edit-answer-id').val(id);
             $('#edit-answer-text').val(text);
          });
      });

      $(function () {
            $('#questions-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : false,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });
  </script>
@endsection
