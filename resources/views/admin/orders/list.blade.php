@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">Orders</a>
    </li>
@endsection

@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Orders</h5>
                                    <div class="card-header-right">
                                        <a href="{{ route('admin-orders-add-view') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                    </div>
                                </div>
                                <div class="card-block font-size-14">
                                    <table id="orders-table" class="table-responsive-xl table-borderless table table-hover"  style="font-size: 12px !important;">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>User Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Bank</th>
                                            <th>Product</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($orders as $order)
                                                <tr>
                                                    <td>{{ $order->id }}</td>
                                                   	<td>{{ $order->getUserName() }}</td>
                                                   	<td>{{ $order->getPhoneNumber() }}</td>
                                                    <td>{{ $order->getEmail() }}</td>
                                                    <td>{{ $order->bank->name_uz }}</td>
                                                    <td>
                                                        @if($order->product_type_id == 1)
                                                            Omonat: &nbsp;
                                                        @elseif($order->product_type_id == 2)
                                                            Credit: &nbsp;
                                                        @elseif($order->product_type_id == 3)
                                                            Debet-Karta: &nbsp;
                                                        @endif
                                                        {{ $order->product->name_uz }}
                                                    </td>
                                                    <td class="text-right" >
                                                        <a href="{{ route('admin-orders-edit-view',[$order->id]) }}" class="btn btn-primary ml-1 p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                        <a href="" class="btn btn-danger ml-1 delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <script>
        $(document).ready(function () {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#menu-review').addClass('active');
        });

        $(function () {
            $('#orders-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : false,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });


    </script>
    <style>
        .float-right{
            float:right !important;
        }
    </style>
@endsection
