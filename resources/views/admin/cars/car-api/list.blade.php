@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{route('admin-telephone-list')}}"><i class="fa fa-bar-chart"></i></a>
    </li>
    <li class="breadcrumb-item">
        <a>Telefon API</a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            @if (session('status'))
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    </div>
                                </div>                                        
                            @endif
                            <div class="card">
                                <div class="card-header">
                                    <h5>Telefon API using Parsehub</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-danger p-1 text-white pl-2 pr-3" href="{{route('admin-telephone-api_runparse')}}">
                                            <i class="fa fa-database"> </i>
                                            Magazindagi narxlarni API orqali yuklash
                                        </a>
                                        <a id="add-new-btn" data-toggle="modal" data-target="#myModal" class="btn btn-info p-1 text-white pl-2 pr-3" href="">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>                                    
                                    
                                </div>
                                <div class="card-block">
                                    @if(count($table_nots) > 0)
                                    <div class="col-md-12">
                                        <h5 class="text-danger">Eslatmalar</h5>
                                        <div style="padding: 10px; border: 1px solid #f3878740;background-color: #f3878740;">
                                            <table class="table-responsive-xl table-bordered table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Api</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($table_nots as $not) 
                                                        <tr>
                                                            <td>1</td>
                                                            <td>{{ $not->row_column_value }}
                                                                <a href="{{ $not->row_column }}" target="_blank" class=" btn btn-success p-1 pl-2 text-white">
                                                                    <i class="fa fa-link"></i> 
                                                                    Tekshirish
                                                                </a>
                                                            <td>
                                                                 <a href="{{ route('check-notification',['id'=>$not->id]) }}" class=" btn btn-success p-1 pl-2 text-white"><i class="fa fa-check"></i> Tekshirildi</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @endif

                                    <table id="data-table" class="table-responsive-xl table-borderless table table-hover" style="font-size: 13px !important;">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Magazin</th>
                                            <th>Project Token</th>
                                            <th>Run Token</th>
                                            <th>Api Key</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($api as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td>{{ $row->organization->name() }}</td>
                                                <td id="project-token-td-{{ $row->id }}">{{ $row->project_token }}</td>
                                                <td id="run-token-td-{{ $row->id }}">{{ $row->run_token }}</td>
                                                <td id="api-key-td-{{ $row->id }}">{{ $row->api_key }}</td>
                                                <td class="text-right">
                                                    <a data-toggle="modal" data-target="#myModal" data-id="{{ $row->id }}" data-org-id="{{ $row->shop_id }}" class="update btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('admin-telephone-api_delete', ['id' => $row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog " style="max-width: 860px">

            <!-- Modal content-->
            <form action="{{ route('admin-telephone-api_edit') }}" method="POST">
                @csrf
                <input type="hidden" id="id" name="id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Online Magazinlar</label>
                                    <select id="org-id-select" name="shop_id" class="form-control"required>
                                       @foreach($organizations as $org)
                                        <option value="{{ $org->id }}">{{ $org->name() }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>                       
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Poject Token</label>
                                    <input type="text" autocomplete="off" id="project-token-input" name="project_token" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Run Token</label>
                                    <input type="text" autocomplete="off" id="run-token-input" name="run_token" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>API key</label>
                                    <input type="text" autocomplete="off" id="api-key-input" name="api_key" required class="form-control">
                                </div>
                            </div>
                        </div>                      
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning" data-dismiss="modal" >Yopish</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });

            $('#rassrochka-parent').addClass('active pcoded-trigger');
            $('#rassrochka-child2').addClass('active');

            $('#add-new-btn').click(function(){
                $('#api-input').val('');
                $('#id').val('');
                $('#modal-title').text("@lang('lang.add_new')")
            });

            $('.update').click(function(){
               id = $(this).attr('data-id');
               project_token = $('#project-token-td-'+id).text();
               run_token = $('#run-token-td-'+id).text();
               api_key = $('#api-key-td-'+id).text();
               org = $(this).attr('data-org-id');
               $('#project-token-input').val(project_token);               
               $('#run-token-input').val(run_token);
               $('#api-key-input').val(api_key);
               $('#id').val(id);
               $('#org-id-select').val(org).trigger('change');
                $('#modal-title').text("@lang('lang.edit')")

            });
        });

        $(function () {
            $('#data-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });

    </script>
@endsection
