@php
    App::setLocale('uz');
    $array = Lang::get('lang.rassrochka.telephone');
@endphp
<div class="row">
    <div class="col-sm-12 pt-4 pl-2">
        <form action="{{ route('telephone-add') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>{{$array['model']}} (Kiritilishi Shart)</b></label>
                    <input required="required" type="text" name="model" class="form-control form-control-lg">
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>{{$array['brand']}}</b></label>

                    <select type="text" name="brand_id" class="form-control form-control-lg">
                        @if($brand !== null) <option value="{{$brand->id}}">{{$brand->name}}</option>@endif
                        @foreach($brands as $b)
                            <option value="{{ $b->id }}"> {{ $b->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @php
                unset($array['model']);
                unset($array['brand']);
                $i = 1;
            @endphp
            @foreach ($array as $key => $value)
                @if($i++%2 == 1)
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3">
                            <label> <b>{{$value}}</b></label>
                            <input type="text" name="{{$key}}"  class="form-control form-control-lg">
                        </div>
                @else 
                        <div class="col-sm-6 mb-3">
                            <label> <b>{{$value}}</b></label>
                            <input type="text" name="{{$key}}"  class="form-control form-control-lg">
                        </div>
                    </div>
                @endif
            @endforeach
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-info ">
                        @lang('lang.save')
                    </button>
                </div>
            </div>            
        </form>
    </div>
</div>