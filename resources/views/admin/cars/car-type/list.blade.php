@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a >@lang('lang.rassrochka.telephones')</a>
    </li>
@endsection

@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">

                                    <h5>@lang('lang.rassrochka.telephones')</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-warning p-1 text-white pl-2 pr-3" data-toggle="modal" data-target="#myModal" ><i class="fa fa-upload"> </i>Telefonni import qilish</a>
                                        <a class="btn btn-info p-1 text-white pl-2 pr-3" href="{{ route('telephone-add-form') }}">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>
                                 <div class="card-block font-size-14 pb-1">
                                     <a href="{{ route('admin-telephone-list') }}" style="margin-bottom: 8px" class="btn waves-effect waves-light all-status-btn hor-grd pl-2 p-1  pr-3 btn-grd-primary">@lang('lang.all')</a> &nbsp;&nbsp;&nbsp;
                                 </div>
                                <div class="card-block font-size-14 pt-0">
                                    @foreach($brands as $brand)
                                    <a href="{{ route('admin-telephone-type',[$brand->id]) }}" class="btn waves-effect waves-light all-status-btn hor-grd pl-2 p-1  pr-3 btn-grd-success">{{ $brand->name }}</a> &nbsp;&nbsp;&nbsp;
                                    @endforeach
                                </div>

                                <form id="translate-form" action="" method="GET">
                                    @csrf
                                <div class="card-block font-size-14">
                                    <table id="credit-table" class="table-responsive-xl table-borderless table table-hover"  style="font-size: 12px !important;">
                                        <thead>
                                        <tr>
                                            <th>
                                                <input type="checkbox" class="translate-all" name="all_credit"/> @lang('lang.all')
                                            </th>
                                            <th>ID </th>
                                            <th>Brend Nomi</th>
                                            <th>Model Nomi</th>
                                            <th>Ichki Xotira</th>
                                            <th>Operativ Xotira</th>
                                            <th>O'lchami</th>
                                            <th>Og'irligi</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($telephones as $tel)
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" data-status="check" class="translate" name="telephones[]" value="{{ $tel->id }}" />
                                                    </td>
                                                    <td>{{ $tel->id }}</td>
                                                    <td>{{ $tel->brand->name}}</td>
                                                    <td>{{ $tel->smartphone_model}}</td>
                                                    <td>{{ $tel->smartphone_inner_memory}}</td>
                                                    <td>{{ $tel->smartphone_operation_memory }}</td>
                                                    <td>{{ $tel->smartphone_dimension }}</td>
                                                    <td>{{ $tel->smartphone_weight }} </td>
                                                <td class="text-right">
                                                        <a href="{{ route('telephone-update-form',[$tel->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                        <a href="{{ route('telephone-delete',[$tel->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="mt-4">
                                       {{-- <input type="radio" name="lang" value="uz"><label>UZ</label>
                                        &nbsp;--}}
                                       {{--  <input type="checkbox" required name="lang" value="ru"><label>RU</label>
                                        <button type="submit" class="btn btn-primary p-1">Translate</button> --}}
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
    {{-- importing telephones --}}
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog " style="max-width: 400px">

            <!-- Modal content-->
            <form action="{{ route('telephones-import') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                       <div class="col-md-12">
                            <div class="form-group">
                                <label>Excel file</label>
                                <input type="file" autocomplete="off"  name="file" required class="form-control">
                            </div>
                        </div>
                      
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning" data-dismiss="modal" >{{ Yt::trans('Yopish','uz') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
    {{-- success model --}}
    @php
        if((isset($errors) && $errors->any()) || session()->has('success')) $status = 1;
        else $status = 0;
    @endphp
    <a id="modalinfo" data-toggle="modal" data-target="#info" data-status="{{$status}}" style="display: none;"></a>
    <div id="info" class="modal fade" role="dialog">
        <div class="modal-dialog " style="max-width: 400px">            
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modal-title" class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        @if(isset($errors) && $errors->any()) 
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}} </li>
                                @endforeach                               
                            </ul>                            
                        </div> 
                        @endif
                        @if(session()->has('success'))
                            <div class="alert alert-info">
                                {{session('success')}}
                            </div>
                        @endif                      
                    </div>                    
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')

    <script>
        $(document).ready(function () {
            //checking modal for errors
            let infoBtn = $('#modalinfo');
            let infoStatus = infoBtn.attr('data-status');
            if(infoStatus == 1) {
                infoBtn.click();
            }            
            //
            $('.translate-all').on('click',function(){

                if($('.translate').attr('checked') == 'checked'){
                    $('.translate').attr('checked',false);
                } else {
                    $('.translate').attr('checked',true);
                }

            });

            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#rassrochka-parent').addClass('active pcoded-trigger');
            $('#rassrochka-child1').addClass('active');
        });

        $(function () {
            $('#credit-table').DataTable({
                'pageLength'  : 50,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });

    </script>
    <style>
        .float-right{
            float:right !important;
        }
    </style>
@endsection
