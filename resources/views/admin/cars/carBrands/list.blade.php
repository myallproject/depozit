@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a >Moshina Brendlari</a>
    </li>
@endsection

@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">

                                    <h5>Moshina Brendlari</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-warning p-1 text-white pl-2 pr-3" data-toggle="modal" data-target="#myModal" id="add-new-btn" ><i class="fa fa-upload"> </i>Yangi moshina brendi qo'shish</a>
                                    </div>
                                </div>
                                <form id="translate-form" action="" method="GET">
                                    @csrf
                                <div class="card-block font-size-14">
                                    <table id="credit-table" class="table-responsive-xl table-borderless table table-hover"  style="font-size: 12px !important;">
                                        <thead>
                                        <tr>
                                            <th>
                                                <input type="checkbox" class="translate-all" name="all_credit"/> @lang('lang.all')
                                            </th>
                                            <th>ID </th>
                                            <th>Brend Nomi (UZ)</th>
                                            <th>Brend Nomi (RU)</th>
                                            <th>Moshinalar Soni</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($car_brands as $brand)
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" data-status="check" class="translate" name="telephones[]" value="{{ $brand->id }}" />
                                                    </td>
                                                    <td>{{ $brand->id }}</td>
                                                    <td id="brand_name_uz-{{$brand->id}}">{{ $brand->name_uz}}</td>
                                                    <td id="brand_name_ru-{{$brand->id}}">{{ $brand->name_ru}}</td>
                                                    <td>{{ $brand->count_model()}}</td>
                                                <td class="text-right">
                                                        <a data-toggle="modal" data-target="#myModal" data-id="{{ $brand->id }}" class="update btn btn-primary p-1 text-white"><i class="fa fa-pencil"></i></a>
                                                        <a href="{{route('admin-car-brand-delete',['id'=>$brand->id])}}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="mt-4">
                                       {{-- <input type="radio" name="lang" value="uz"><label>UZ</label>
                                        &nbsp;--}}
                                       {{--  <input type="checkbox" required name="lang" value="ru"><label>RU</label>
                                        <button type="submit" class="btn btn-primary p-1">Translate</button> --}}
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
    {{-- importing telephones --}}
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog " style="min-width: 800px !important;">

            <!-- Modal content-->
            <form action="{{ route('admin-car-brand-add') }}" method="POST">
                @csrf
                <input type="hidden" id="id" name="id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name UZ</label>
                                    <input type="text" class="form-control" required name="name_uz" id="brand_name_uz-input">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name RU</label>
                                    <input type="text" class="form-control" required name="name_ru" id="brand_name_ru-input">
                                </div>
                            </div>
                        </div>                 
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning" data-dismiss="modal" >{{ Yt::trans('Yopish','uz') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
    {{-- success model --}}
    @php
        if((isset($errors) && $errors->any()) || session()->has('success') ||session()->has('myErrors')) $status = 1;
        else $status = 0;
    @endphp
    <a id="modalinfo" data-toggle="modal" data-target="#info" data-status="{{$status}}" style="display: none;"></a>
    <div id="info" class="modal fade" role="dialog">
        <div class="modal-dialog " style="max-width: 400px">            
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modal-title" class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        @if(isset($errors) && $errors->any()) 
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}} </li>
                                @endforeach                               
                            </ul>                            
                        </div> 
                        @endif
                        @if(session()->has('success'))
                            <div class="alert alert-info">
                                {{session('success')}}
                            </div>
                        @endif    
                        @if(session()->has('myErrors'))
                            <div class="alert alert-danger">
                                {{session('myErrors')}}                           
                            </div> 
                        @endif                     
                    </div>                    
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')

    <script>
        $(document).ready(function () {
            //checking modal for errors
            let infoBtn = $('#modalinfo');
            let infoStatus = infoBtn.attr('data-status');
            if(infoStatus == 1) {
                infoBtn.click();
            }            
            //
            $('.translate-all').on('click',function(){

                if($('.translate').attr('checked') == 'checked'){
                    $('.translate').attr('checked',false);
                } else {
                    $('.translate').attr('checked',true);
                }

            });

            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#car-parent').addClass('active pcoded-trigger');
            $('#car-child2').addClass('active');

            $('#add-new-btn').click(function(){
                $('#id').val('');
                $('#modal-title').text("@lang('lang.add_new')")
            });

            $('.update').click(function(){
               id = $(this).attr('data-id');
               brand_name_ru = $('#brand_name_ru-'+id).text();
               brand_name_uz = $('#brand_name_uz-'+id).text();
               $('#brand_name_ru-input').val(brand_name_ru);               
               $('#brand_name_uz-input').val(brand_name_uz);
               $('#id').val(id);
               $('#modal-title').text("@lang('lang.edit')");

            });
        });

        $(function () {
            $('#credit-table').DataTable({
                'pageLength'  : 50,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });

    </script>
    <style>
        .float-right{
            float:right !important;
        }
    </style>
@endsection
