@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a >Moshinalar</a>
    </li>
@endsection

@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">

                                    <h5>Moshinalar</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-danger p-1 text-white pl-2 pr-3" href="{{route('admin-car-api_runparse')}}">
                                            <i class="fa fa-database"> </i>
                                            Moshinadagi narxlarni API orqali yuklash
                                        </a>
                                        <a class="btn btn-warning p-1 text-white pl-2 pr-3" data-toggle="modal" data-target="#myModal" id="add-new-btn" ><i class="fa fa-upload"> </i>Yangi mashina qo'shish</a>
                                    </div>
                                </div>
                                 <div class="card-block font-size-14 pb-1">
                                     <a href="{{ route('admin-car-list') }}" style="margin-bottom: 8px" class="btn waves-effect waves-light all-status-btn hor-grd pl-2 p-1  pr-3 btn-grd-primary">@lang('lang.all')</a> &nbsp;&nbsp;&nbsp;
                                 </div>
                                <div class="card-block font-size-14 pt-0">
                                    @foreach($brands as $brand)
                                    <a href="{{ route('admin-car-type',[$brand->id]) }}" class="btn waves-effect waves-light all-status-btn hor-grd pl-2 p-1  pr-3 btn-grd-success">{{ $brand->name() }}</a> &nbsp;&nbsp;&nbsp;
                                    @endforeach
                                </div>

                                <form id="translate-form" action="" method="GET">
                                    @csrf
                                <div class="card-block font-size-14">
                                    <table id="credit-table" class="table-responsive-xl table-borderless table table-hover"  style="font-size: 12px !important;">
                                        <thead>
                                        <tr>
                                            <th>
                                                <input type="checkbox" class="translate-all" name="all_credit"/> @lang('lang.all')
                                            </th>
                                            <th>ID </th>
                                            <th>Brend Nomi</th>
                                            <th>Model Nomi (UZ)</th>
                                            <th>Model Nomi (RU)</th>
                                            <th>Pozitsiya</th>
                                            <th>Narxi</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($cars as $car)
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" data-status="check" class="translate" name="telephones[]" value="{{ $car->id }}" />
                                                    </td>
                                                    <td>{{ $car->id }}</td>
                                                    <td id="car-brand-{{$car->id}}" data-brand-id="{{$car->car_brand_id}}">{{ $car->brand->name()}}</td>
                                                    <td id="car-name-uz-{{$car->id}}">{{ $car->name_uz}}</td>
                                                    <td id="car-name-ru-{{$car->id}}">{{ $car->name_ru}}</td>
                                                    <td id="car-pozition-{{$car->id}}">{{ $car->pozition}}</td>
                                                    <td><span id="car-price-{{$car->id}}">{{ $car->price}}</span> &nbsp;
                                                        <span id="car-currency-{{$car->id}}" data-cur='{{$car->price_category}}'>
                                                            @switch($car->price_category)
                                                                @case(1)
                                                                    UZS
                                                                    @break
                                                                @case(2)
                                                                    USD
                                                                    @break
                                                                @case(3)
                                                                    RUBL
                                                                    @break
                                                                @default
                                                                    Ko'rsatilmagan
                                                            @endswitch
                                                        </span>
                                                        
                                                    </td>
                                                    <td class="text-right">
                                                        <a data-toggle="modal" data-target="#myModal" data-id="{{ $car->id }}" data-org-id="{{ $car->car_brand_id }}" class="update btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                        <a href="{{route('admin-car-delete',['id'=>$car->id])}}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="mt-4">
                                       {{-- <input type="radio" name="lang" value="uz"><label>UZ</label>
                                        &nbsp;--}}
                                       {{--  <input type="checkbox" required name="lang" value="ru"><label>RU</label>
                                        <button type="submit" class="btn btn-primary p-1">Translate</button> --}}
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
    {{-- importing telephones --}}
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog " style="min-width: 800px !important;">

            <!-- Modal content-->
            <form action="{{ route('admin-car-add') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="id" name="id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Moshina Brendi</label>
                                        <select name="brand_id" class="form-control"required id="car_brand-input">
                                           @foreach($brands as $brand)
                                            <option value="{{ $brand->id }}">{{ $brand->name() }}</option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label>Moshina narxi</label>
                                <input type="number" class="form-control" name="price" id="car_price-input" required>
                            </div>
                            <div class="col-md-3">
                                <label>Valyuta</label>
                                <select name="currency" id="car_currency-input" class="form-control" value="1">
                                    <option value="1" selected>UZS</option>
                                    <option value="2">USD</option>
                                    <option value="3">RUBL</option>
                                </select>
                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name UZ</label>
                                    <input type="text" class="form-control" required name="name_uz" id="car_name_uz-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Name RU</label>
                                    <input type="text" class="form-control" required name="name_ru" id="car_name_ru-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Pozitsiya</label>
                                    <select name="pozition" id="car_pozition-input" class="form-control">
                                        <option value="1">1-pozitsiya</option>
                                        <option value="2">2-pozitsiya</option>
                                        <option value="3">3-pozitsiya</option>
                                        <option value="4">4-pozitsiya</option>
                                    </select>
                                </div>
                            </div>
                        </div>     
                        <div class="row">
                            <div class="col-md-12">
                                <label for="img-input">Surat</label>
                                <input type="file" class="form-control" name="car_image" id="car-image-input">    
                            </div>   
                        </div>                 
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning" data-dismiss="modal" >{{ Yt::trans('Yopish','uz') }}</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
    {{-- success model --}}
    @php
        if((isset($errors) && $errors->any()) || session()->has('success') ||session()->has('myErrors')) $status = 1;
        else $status = 0;
    @endphp
    <a id="modalinfo" data-toggle="modal" data-target="#info" data-status="{{$status}}" style="display: none;"></a>
    <div id="info" class="modal fade" role="dialog">
        <div class="modal-dialog " style="max-width: 400px">            
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modal-title" class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        @if(isset($errors) && $errors->any()) 
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}} </li>
                                @endforeach                               
                            </ul>                            
                        </div> 
                        @endif
                        @if(session()->has('success'))
                            <div class="alert alert-info">
                                {{session('success')}}
                            </div>
                        @endif    
                        @if(session()->has('myErrors'))
                            <div class="alert alert-danger">
                                {{session('myErrors')}}                           
                            </div> 
                        @endif                     
                    </div>                    
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')

    <script>
        $(document).ready(function () {
            //checking modal for errors
            let infoBtn = $('#modalinfo');
            let infoStatus = infoBtn.attr('data-status');
            if(infoStatus == 1) {
                infoBtn.click();
            }            
            //
            $('.translate-all').on('click',function(){

                if($('.translate').attr('checked') == 'checked'){
                    $('.translate').attr('checked',false);
                } else {
                    $('.translate').attr('checked',true);
                }

            });

            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#car-parent').addClass('active pcoded-trigger');
            $('#car-child1').addClass('active');

            $('#add-new-btn').click(function(){
                $('#id').val('');
                $('#modal-title').text("@lang('lang.add_new')")
            });

            $('.update').click(function(){
               id = $(this).attr('data-id');
               car_brand = $('#car-brand-'+id).attr('data-brand-id');
               car_name_uz = $('#car-name-uz-'+id).text();
               car_name_ru = $('#car-name-ru-'+id).text();
               car_pozition = $('#car-pozition-'+id).text();
               car_price = $('#car-price-'+id).text();
               car_currency = $('#car-currency-'+id).attr('data-cur');
               $('#car_brand-input').val(car_brand);               
               $('#car_name_uz-input').val(car_name_uz);
               $('#car_name_ru-input').val(car_name_ru);
               $('#car_pozition-input').val(car_pozition);
               $('#car_price-input').val(car_price);
               $('#car_currency-input').val(car_currency);
               $('#id').val(id);
               $('#org-id-select').val(org).trigger('change');
                $('#modal-title').text("@lang('lang.edit')")

            });
        });

        $(function () {
            $('#credit-table').DataTable({
                'pageLength'  : 50,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });

    </script>
    <style>
        .float-right{
            float:right !important;
        }
    </style>
@endsection
