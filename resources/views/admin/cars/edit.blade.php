@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a>
            @lang('lang.rassrochka.telephones')
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="">
            {!! !empty($telephone) ? trans('lang.edit') : trans('lang.add_new') !!}
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card" style="background-color: #ecf0f5 !important;">
                                <div class="card-header">
                                    <h5>{!! !empty($telephone) ? trans('lang.edit') : trans('lang.add_new') !!}</h5>
                                    @if($telephone)
                                        <div class="card-header-right">
                                            <a href="{{ route('telephone-add-form') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-12"  style="font-size: 13px !important;">
                                    @if($telephone)
                                        @include('admin.rassrochka.telephones.form.update')
                                    @else
                                        @include('admin.rassrochka.telephones.form.add')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            
            $('#rassrochka-parent').addClass('active pcoded-trigger');
            $('#rassrochka-child1').addClass('active');            
        });
    </script>
@endsection
