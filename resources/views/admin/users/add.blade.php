@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">Users</a>
    </li>
@endsection

@section('content')
 <link rel="stylesheet" href="/plugins/dropify/dist/css/dropify.min.css">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>User</h5>
                                    <div class="card-header-right">
                                        <a href="{{ route('admin-user-add') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                    </div>
                                </div>
                                <div class="card-block font-size-14">
                                    <div class="row ml-3 mr-3">
                                        <div class="col-sm-12 pt-4 ">
                                            <form action="{{ route('admin-user-edit') }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                                                @csrf
                                                <input id="user-id" type="hidden" name="id" value="">
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>FISH</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <input type="text" name="name" value="" class="form-control form-control-lg" required="required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Email</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                       <input type="text" name="email" value=""  autocomplete="off" class="form-control form-control-lg" required="required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Yangi parol</b></label>
                                                    </div>
                                                    <div class="col-sm-4 mb-3">
                                                       <input type="password" name="password" value="" class="form-control form-control-lg">
                                                    </div>
                                                     <div class="col-sm-4 mb-3">
                                                       
                                                       {{-- <a class="btn btn-success ml-1 p-1 pl-2 text-white" data-toggle="tooltip" data-placement="right" title="Parol yaratish"><i class="fa fa-key"></i></a>
                                                       
                                                       <span  id="password-text" class="ml-2"></span> --}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label> <b>Role</b></label>
                                                    </div>
                                                    <div class="col-sm-8 mb-3">
                                                        <select id='user-role'  name="role" class="form-control form-control-lg">
                                                            @foreach($userrole as $role)
	                                                           	@if(Auth::user()->role_id != 10)
	                                                           		@if($role->id != 10)
	                                                                	<option value="{{ $role->id }}">{{ $role->getName() }}</option>
	                                                           		@endif
	                                                           	@else
	                                                                <option value="{{ $role->id }}">{{ $role->getName() }}</option>
	                                                           	@endif	
                                                            @endforeach
                                                        </select> 
                                                    </div>
                                                </div>
                                                <div id='other-input-content' >
                                                    
                                                </div>
                                                 <div class="form-group row">
                                                    <div class="col-sm-2 mb-3 text-right">
                                                        <label><b>@lang('lang.image') (yangi)</b></label>
                                                    </div>                                                        
                                                    <div class="col-sm-4">    
                                                        <input type="file" id="input-file-now-custom-2" value="" name="image" class="dropify" data-default-file="" />
                                                    </div>
                                                   
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-2 mb-3"></div>
                                                    <div class="col-sm-8 mb-3">
                                                        <button type="submit" class="btn btn-info ">
                                                            @lang('lang.save')
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
<script src="/plugins/dropify/dist/js/dropify.min.js"></script>


    <script>
        $(document).ready(function () {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#menu-user').addClass('active');
        });

         $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
         
         if($('#user-role').val() == 11) {
            $.ajax({
                method: 'GET',
                url: "{{ route('admin-user-other-input') }}",
                data:{
                    'id':$('#user-id').val()
                }
            }).done(function(data){
                $('#other-input-content').html(data);
            });
        } else {
                $('#other-input-content').html('');
        }

        
        $('#user-role').on('change',function(){
            if($(this).val() == 11) {
                $.ajax({
                    method: 'GET',
                    url: "{{ route('admin-user-other-input') }}",
                    data:{
                        'id':$('#user-id').val()
                    }
                }).done(function(data){
                    $('#other-input-content').html(data);
                });
            } else {
                $('#other-input-content').html('');
            }
        });

        $('.dropify').dropify();

         $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        
        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        });
    </script>
    <style>
        .float-right{
            float:right !important;
        }
    </style>
@endsection
