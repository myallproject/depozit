@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a >User Roles</a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>User Roles</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-info p-1 text-white pl-2 pr-3"id="add-new-btn" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table class="table-responsive-xl table-borderless table table-hover">
                                        <thead>
	                                        <tr>
	                                            <th>ID</th>
	                                            <th>@lang('lang.name')</th>
	                                            <th>@lang('lang.status')</th>
	                                            <th class="text-right">@lang('lang.control')</th>
	                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($list as $row)
                                            <tr>
                                                <td id="id-type-{{ $row->id }}">{{ $row->id }}</td>
                                                <td id="name-{{ $row->id }}">{{ $row->name }}</td>
                                                <td id="status-{{ $row->id }}">{{ $row->status }}</td>
                                                <td class="text-right">
                                                    <a data-toggle="modal" data-target="#myModal" data-id="{{ $row->id }}" class="btn btn-primary update p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('user-roles-delete',[$row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form action="{{ route('user-roles-edit') }}" method="POST">
                @csrf
                <input type="hidden" id="id" name="id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>@lang('lang.name') UZ</label>
                                <input type="text" autocomplete="off" id="name" name="name" required class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" id="status" required class="form-control">
                                	@foreach(config('global.status') as $k => $v)
                                		<option value="{{ $k }}">{{ $v }}</option>
                               		@endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning" data-dismiss="modal" >@lang('lang.close')</a>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });

            $('#deposits-parent').addClass('active pcoded-trigger');

            $('#add-new-btn').click(function(){
                $('#id').val('');
                $('#name').val('');
                $('#status').val('').trigger('change');

                $('#modal-title').text("@lang('lang.add_new')")
            });

            $('.update').click(function(){
                let id = $(this).attr('data-id');
                name = $('#name-'+id).text();
                status = $('#status-'+id).text();
                $('#name').val(name);
                $('#status').val(status).trigger('change');
          
                $('#id').val(id);
                $('#modal-title').text("@lang('lang.edit')")
            });
        });
    </script>
@endsection
