@if($user)

	<div class="form-group row">
		<div class="col-sm-2 mb-3 text-right">
		    <label> <b>Tashkilot turi</b></label>
		</div>
		<div class="col-sm-8 mb-3">
		    <select id="organization-select" name="organization" class="form-control form-control-lg">
		    	<option>@lang('lang.choose')</option>
			    @foreach($organizations as $org)  
			      	<option @if($user->organization['organization_id'] == $org->id) selected @endif value="{{ $org->id }}">{{ $org->name() }}</option>
			    @endforeach
		    </select> 
		</div>
	</div>

	<div class="form-group row">
		<div class="col-sm-2 mb-3 text-right">
		    <label> <b>Tashkilot</b></label>
		</div>
		<div class="col-sm-8 mb-3">
		    <select id="organization-child"  name="child_oranization"  class="form-control form-control-lg">
		    	@foreach($banks as $bank)  
		      		<option @if($user->organization['child_org_id'] == $bank->id) selected @endif value="{{ $bank->id }}">{{ $bank->name() }}</option>
		    	@endforeach
		    </select> 
		</div>
	</div>

	<div class="form-group row">
		<div class="col-sm-2 mb-3 text-right">
		    <label> <b>Onlayn muloqotga qo'shish</b></label>
		</div>
		<div class="col-sm-8 mb-3">
			<?php $hotLine = App\Models\HotLineUser::where('user_id', $user->id)->first(); ?>
			@if($hotLine)
		    	<input checked type="checkbox" name="hot_line" value="1">
		    @else
		    	<input type="checkbox" name="hot_line" value="1">
		    @endif
		</div>
	</div>

	<div class="form-group row">
		<div class="col-sm-2 mb-3 text-right">
		    <label> <b>Lavozimi Uz</b></label>
		</div>
		<div class="col-sm-8 mb-3">
		    <input type="text" name="position_uz" @if($user->organization) value="{{ $user->organization->position('uz') }}" @endif class="form-control form-control-lg">
		</div>
	</div>

	<div class="form-group row">
		<div class="col-sm-2 mb-3 text-right">
		    <label> <b>Lavozimi Ru</b></label>
		</div>
		<div class="col-sm-8 mb-3">
		   <input type="text" name="position_ru" @if($user->organization)  value="{{ $user->organization->position('ru') }}" @endif class="form-control form-control-lg" >
		    
		</div>
	</div>

	<script>
	$(document).ready(function(){
	 	$.ajax({
	        method:'GET',
	        url:"{{ route('admin-select-oranization') }}",
	        data:{
	            'org_type': $('#organization-select').val(),
	          	'child_org': "{{ $user->organization['child_org_id'] }}"
	        }
		    }).done(function(data){
		        $('#organization-child').html(data)
		});

		$('#organization-select').on('change',function(){
	            $.ajax({
	                method:'GET',
	                url:"{{ route('admin-select-oranization') }}",
	                data:{
	                    'org_type': $(this).val(),
	                    'child_org': "{{ $user->organization['child_org_id'] }}"
	                }
	            }).done(function(data){
	                $('#organization-child').html('');
	                $('#organization-child').html(data)
	            });

        });
	});
		
</script>
@else 

<div class="form-group row">
	<div class="col-sm-2 mb-3 text-right">
	    <label> <b>Tashkilot turi</b></label>
	</div>
	<div class="col-sm-8 mb-3">
	    <select id="organization-select" name="organization" class="form-control form-control-lg">
	    	<option>@lang('lang.choose')</option>
		    @foreach($organizations as $org)  
		      	<option value="{{ $org->id }}">{{ $org->name() }}</option>
		    @endforeach
	    </select> 
	</div>
</div>

<div class="form-group row">
	<div class="col-sm-2 mb-3 text-right">
	    <label> <b>Tashkilot</b></label>
	</div>
	<div class="col-sm-8 mb-3">
	    <select id="organization-child"  name="child_oranization"  class="form-control form-control-lg">
	    	@foreach($banks as $bank)  
	      		<option value="{{ $bank->id }}">{{ $bank->name() }}</option>
	    	@endforeach
	    </select> 
	</div>
</div>

	<div class="form-group row">
		<div class="col-sm-2 mb-3 text-right">
		    <label> <b>Onlayn muloqotga qo'shish</b></label>
		</div>
		<div class="col-sm-8 mb-3">
		    <input type="chexkbox" name="hot_line" value="1">
		</div>
	</div>

	<div class="form-group row">
		<div class="col-sm-2 mb-3 text-right">
		    <label> <b>Lavozimi Uz</b></label>
		</div>
		<div class="col-sm-8 mb-3">
		    <input type="text" name="position_uz" value="" class="form-control form-control-lg" >
		</div>
	</div>

	<div class="form-group row">
		<div class="col-sm-2 mb-3 text-right">
		    <label> <b>Lavozimi Ru</b></label>
		</div>
		<div class="col-sm-8 mb-3">
		   <input type="text" name="position_ru" value="" class="form-control form-control-lg" >
		</div>
	</div>


<script>
	$(document).ready(function(){
	 	$.ajax({
	        method:'GET',
	        url:"{{ route('admin-select-oranization') }}",
	        data:{
	            'org_type': $('#organization-select').val(),
	          	'child_org': ""
	        }
		    }).done(function(data){
		        $('#organization-child').html(data)
		});

		$('#organization-select').on('change',function(){
	            $.ajax({
	                method:'GET',
	                url:"{{ route('admin-select-oranization') }}",
	                data:{
	                    'org_type': $(this).val(),
	                    'child_org': ""
	                }
	            }).done(function(data){
	                $('#organization-child').html('');
	                $('#organization-child').html(data)
	            });

        });
	});
		
</script>
@endif




