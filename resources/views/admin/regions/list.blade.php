@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">
            <i class="fa fa-map"></i>
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.regions')</h5>
                                    <div class="card-header-right">
                                        <a href="{{ route('region-add-form') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table  id="region-table" class="table-responsive-xl table-borderless table table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('lang.city')</th>
                                            <th>@lang('lang.region')</th>

                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($regions as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td>{{ $row->name() }}</td>
                                                <td>{{ $row->parent_id > 0 ? $row->parent->name() : "NO" }}</td>
                                                <td class="text-right">
                                                    <a href="{{ route('region-update-form',[$row->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('region-delete',[$row->id]) }}" class="btn delete btn-danger p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
  <script>
      $(document).ready(function() {
          $('.delete').on('click', function() {
              return confirm("@lang('lang.confirmDel')");
          });
          $('#menu-region').addClass('active');
      });

      $(function () {
            $('#region-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : false,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });
  </script>
@endsection
