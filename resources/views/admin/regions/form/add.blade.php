<div class="row ml-5">
    <div class="col-sm-8 pt-4 ">
        <form action="{{ route('region-add') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.region')</b></label>
                    <select type="text" name="parent" class="form-control form-control-lg">
                        <option value="">-- @lang('lang.region') --</option>
                        @foreach($parent as $row)
                            <option value="{{ $row->id }}"> {{ $row->name() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
           <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.city') name uz</b></label>
                   <input required type="text" name="name_uz" class="form-control form-control-lg">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.city') name ru</b></label>
                    <input required type="text" name="name_ru" class="form-control form-control-lg">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-info ">
                        @lang('lang.save')
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-sm-4 pt-4">
        <div class="form-group ">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>

