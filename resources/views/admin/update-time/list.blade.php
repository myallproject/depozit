@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">
            <i class="fa fa-bank"></i>
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{{ Yt::trans('Kredit garovi','uz') }}</h5>
                                    <div class="card-header-right">
                                        <a id="add-new-btn" data-toggle="modal" data-target="#myModal" class="btn btn-info p-1 text-white pl-2 pr-3" href="">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table id="data-table" class="table-responsive-xl table-borderless table table-hover" style="font-size: 13px !important;">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>@lang('lang.update_time')</th>
                                            <th>@lang('lang.last_update_time')</th>
                                            <th>Kredit turi</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($model as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td id="update-time-{{ $row->id }}">{{ $row->data_update_time }}</td>
                                                <td >{{ $row->last_data_update_time }}</td>
                                                <td >{{ $row->service->name() }}</td>
                                                <td class="text-right">
                                                    <a data-toggle="modal" data-target="#myModal" data-id="{{ $row->id }}" data-service-id="{{ $row->service_id }}" class="update btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('data-update-time-delete',[$row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form action="{{ route('data-update-time-edit') }}" method="POST">
                @csrf
                <input type="hidden" id="id" name="id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Sayt xizmatlari</label>
                                <select required="required" class="form-control form-control-lg" name="service_id" id="service-id-select">
                                    <option value="">@lang('lang.choose')</option>
                                    @foreach($services as $service)
                                        <option value="{{ $service->id }}">{{ $service->name() }}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>@lang('lang.time')</label>
                                <input type="datetime-local" id="update_time" value="" name="update_time"  required class="form-control">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning" data-dismiss="modal" >@lang('lang.close')</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });

            $('#menu-credit-parent').addClass('active pcoded-trigger');

            $('#add-new-btn').on('click',function(){
                $('#id').val('');
                $('#service-id-select').val('').trigger('change');
                $('#update_time').val('');
                $('#modal-title').text("@lang('lang.add_new')")
            });

            $('.update').on('click',function(){
                let id = $(this).attr('data-id');
                id_type = $(this).attr('data-service-id');

                $('#service-id-select').val(id_type).trigger('change');

                $('#id').val(id);
                $('#modal-title').text("@lang('lang.edit')");
            });
        });

        $(function () {
            $('#data-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });

    </script>
@endsection
