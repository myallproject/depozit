@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('currency-list') }}">
            @lang('lang.currency')
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="">
            {!! !empty($currency) ? trans('lang.edit') : trans('lang.add_new') !!}
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{!! !empty($currency) ? trans('lang.edit') : trans('lang.add_new') !!}</h5>
                                    @if($currency)
                                        <div class="card-header-right">
                                            <a href="{{ route('currency-add-view') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-12">
                                    @if($currency)
                                        @include('admin.currency.form.update')
                                    @else
                                        @include('admin.currency.form.add')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#menu-addition').addClass('active pcoded-trigger');
            $('#menu-currency').addClass('active');
        });
    </script>
@endsection
