<style type="text/css">
    .pcoded-submenu>li>a {
        font-size: 13px !important;
    }

</style>
<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <div class="">
            <div class="main-menu-header">
                <img class="img-menu-user img-radius" src="/admin/assets/images/admin.jpg" alt="User-Profile-Image">
                <div class="user-details">
                    <p id="more-details">{{ Auth::user()->name }}<i class="feather icon-chevron-down m-l-10"></i></p>
                </div>
            </div>
            <div class="main-menu-content">
                <ul>
                    <li class="more-details">
                        <a href="#">
                            <i class="feather icon-user"></i>@lang('lang.profile')
                        </a>
                        <a href="#">
                            <i class="feather icon-settings"></i>@lang('lang.settings')
                        </a>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form-log').submit();">
                            <i class="feather icon-log-out"></i> @lang('lang.logout')
                        </a>
                        <form id="logout-form-log" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
        <div class="pcoded-navigation-label">@lang('lang.settings')</div>
        <ul class="pcoded-item pcoded-left-item">
            <li>
                <a href="{{ route('dashboard') }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                    <span class="pcoded-mtext">@lang('lang.home_page')</span>
                </a>
            </li>
            <li>
                <a target="_blank" href="{{ route('home', App::getLocale()) }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-sign-out"></i></span>
                    <span class="pcoded-mtext">@lang('lang.go_site')</span>
                </a>
            </li>
            <li id="menu-user">
                <a href="{{ route('users-list') }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-users"></i></span>
                    <span class="pcoded-mtext">Users</span>
                </a>
            </li>
            <li>
                <a href="{{ route('user-roles-list') }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-users"></i></span>
                    <span class="pcoded-mtext">User roles</span>
                    @if (Session::has('msg-root-validate'))
                        <br><span class="text-danger">{{ Session::get('msg-root-validate') }}</span>
                    @endif
                </a>
            </li>
        </ul>

        <ul class="pcoded-item pcoded-left-item">

            <li class="pcoded-hasmenu " id="menu-banks">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-bank"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.all_partner')</span>
                </a>

                <ul class="pcoded-submenu">
                    <li class="" id="">
                        <a href="{{ route('banks-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.banks')</span>
                        </a>
                    </li>

                    <li class="" id="">
                        <a href="{{ route('other-organization-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.other_organization')</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="pcoded-hasmenu " id="menu-credit-parent">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-credit-card"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.credits')</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="" id="menu-credit-first">
                        <a href="{{ route('credits-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.credits')</span>
                        </a>
                    </li>
                    <li class="" id="menu-credit-second">
                        <a href="{{ route('credit-type-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.credit_types')</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('credit-borrower-category-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.borrower_cat')</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('credit-goal-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Kredit maqsadi</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('credit-issuing-form-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Kredit berish shakli</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('credit-pledge-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Kredit garovi</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('credit-proof-income-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Daromadni tasdiqlovchi hujjat</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('credit-review-period-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Kreditni ko'rib chiqish vaqti</span>
                        </a>
                    </li>
                    <li class="border-bottom">
                        <a href="{{ route('credit-surety-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Kredit uchun kafillik</span>
                        </a>
                    </li>
                    <li class="border-bottom">
                        <a href="{{ route('credit-provision-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Kredit ta'minot turi</span>
                        </a>
                    </li>
                    <li class="border-bottom">
                        <a href="{{ route('credit-compensation-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Boshlang'ich badal</span>
                        </a>
                    </li>
                </ul>

            </li>

            <li class="pcoded-hasmenu " id="deposits-parent">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-money"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.deposit')</span>
                </a>

                <ul class="pcoded-submenu">
                    <li class="" id="deposits-child1">
                        <a href="{{ route('deposit-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.deposits')</span>
                        </a>
                    </li>
                    <li class="" id="deposits-child2">
                        <a href="{{ route('deposit-type-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.deposit_types')</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('deposit-open-type-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Omanatni ochish turi</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('deposit-period-percent-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Foiz to'lash davrlari</span>
                        </a>
                    </li>
                    @if (Auth::user()->role_id == 10)
                        <li class="">
                            <a href="{{ route('depsit-column-translate-page') }}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Translate Column</span>
                            </a>
                        </li>
                    @endif
                </ul>

            </li>

            <li class="pcoded-hasmenu " id="card-parent">

                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-credit-card"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.cards')</span>
                </a>

                <ul class="pcoded-submenu">
                    <li class="" id="card-child1">
                        <a href="{{ route('debit-cards-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.debit_cards')</span>
                        </a>
                    </li>
                    <li class="" id="card-child2">
                        <a href="{{ route('credit-card-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.credit_cards')</span>
                        </a>
                    </li>
                    <li class="" id="card-child3">
                        <a href="{{ route('debit-card-type-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Debit karta turlari</span>
                        </a>
                    </li>
                </ul>

            </li>
            {{-- Rassrochka --}}
            <li class="pcoded-hasmenu " id="rassrochka-parent">

                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-bar-chart"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.rassrochka.rassrochka')</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="" id="rassrochka-child4">
                        <a href="{{ route('admin-rassrochka-brand-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Brendlar</span>
                        </a>
                    </li>
                </ul>

                <ul class="pcoded-submenu">
                    <li class="" id="rassrochka-child1">
                        <a href="{{ route('admin-telephone-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.rassrochka.telephones')</span>
                        </a>
                    </li>
                </ul>
                <ul class="pcoded-submenu">
                    <li class="" id="rassrochka-child2">
                        <a href="{{ route('admin-telephone-api_list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Telefon API</span>
                        </a>
                    </li>
                </ul>
                <ul class="pcoded-submenu">
                    <li class="" id="rassrochka-child3">
                        <a href="{{ route('admin-telephone-api_problems') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Telefondani xatolar *</span>
                        </a>
                    </li>
                </ul>
                <ul class="pcoded-submenu">
                    <li class="" id="rassrochka-child4">
                        <a href="{{ route('admin-telephone-shop-api_problems') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Magazinlardagi xatolar *</span>
                        </a>
                    </li>
                </ul>

            </li>

            {{-- Stock --}}
            <li class="pcoded-hasmenu " id="rassrochka-parent">

                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="ti-exchange-vertical"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.investments')</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="{{ route('admin.stock.index') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('stock.shares')</span>
                        </a>
                    </li>

                    <li class="">
                        <a href="{{ route('admin.stock.index') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Sohalar</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('admin.stock.index') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">So'rovnomalar</span>
                        </a>
                    </li>
                </ul>
            </li>

            {{-- Cars module --}}
            <li class="pcoded-hasmenu " id="card-parent">

                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-car"></i>
                    </span>
                    <span class="pcoded-mtext">Avtomabillar</span>
                </a>

                <ul class="pcoded-submenu">
                    <li class="" id="car-child1">
                        <a href="{{ route('admin-car-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Avtomabillar</span>
                        </a>
                    </li>
                    <li class="" id="car-child2">
                        <a href="{{ route('admin-car-brand-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Avtomabil Brendlari</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="" id="menu-questions">
                <a href="{{ route('admin-question-list') }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-question-circle-o"></i>
                    </span>
                    <span class="pcoded-mtext">Savol-javob</span>
                </a>
            </li>

            {{-- Orders --}}
            <li class="" id="menu-orders">
                <a href="{{ route('admin-orders-list') }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-shopping-cart"></i>
                    </span>
                    <span class="pcoded-mtext">Buyurtmalar <span
                            style="border-radius: 50% 50%; padding: 1px 6px;background-color: red;color: white;"> 0
                        </span></span>
                </a>
            </li>
            {{-- End Orders --}}
        </ul>

        <div class="pcoded-navigation-label">@lang('lang.others')</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="" id="menu-region">
                <a href="{{ route('region-list') }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="feather icon-map"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.regions')</span>
                </a>
            </li>
            <li class="" id="menu-rate">
                <a href="{{ route('rate-list') }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-money"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.bank') @lang('lang.currency_exchange_rate')</span>
                </a>
            </li>

            <li class="pcoded-hasmenu " id="menu-addition">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.additions')</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="" id="menu-service">
                        <a href="{{ route('services-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.services')</span>
                        </a>
                    </li>
                    <li class="" id="menu-date-type">
                        <a href="{{ route('date-type-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.date_services')</span>
                        </a>
                    </li>
                    <li class="" id="menu-currency">
                        <a href="{{ route('currency-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.currency')</span>
                        </a>
                    </li>
                    <li class="" id="menu-baks-type">
                        <a href="{{ route('banks-type-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">{{ Yt::trans('Banklar turlari', 'uz') }}</span>
                        </a>
                    </li>

                    <li class="" id="menu-update-time">
                        <a href="{{ route('data-update-time-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.update_time')</span>
                        </a>
                    </li>

                    <li class="" id="menu-type-exchange">
                        <a href="{{ route('type-exchange-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Valyuta ayriboshlash turi</span>
                        </a>
                    </li>

                    <li class="" id="menu-type-payment">
                        <a href="{{ route('payment-type-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">To'lov tizimlari</span>
                        </a>
                    </li>

                    <li class="" id="min-quantity-list">
                        <a href="{{ route('min-quantity-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">BHM</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="pcoded-hasmenu " id="menu-setting">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-wrench"></i>
                    </span>
                    <span class="pcoded-mtext">Настройка</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="" id="meta-tags">
                        <a href="{{ route('meta-tags-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Метаданные</span>
                        </a>
                    </li>
                    <li class="" id="menu-currency-api">
                        <a href="{{ route('setting-currency-api') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Valyuta API</span>
                        </a>
                    </li>
                    <li class="" id="menu-site-cache">
                        <a href="{{ route('setting-caching') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Caching & Cron</span>
                        </a>
                    </li>
                </ul>
            </li>
            {{-- <li class="pcoded-hasmenu " id="pages-parent">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-money"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.pages')</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="" id="pages-child1">
                        <a href="{{ route('home-page') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.home')</span>
                        </a>
                    </li>
                    <li class="" id="pages-child2">
                        <a href="{{ route('deposit-type-list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">@lang('lang.deposit_types')</span>
                        </a>
                    </li>
                </ul>
            </li>--}}
            <li class="" id="menu-contact">
                <a href="{{ route('contact-list') }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-user"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.contact')</span>
                </a>
            </li>
            <li class="" id="review-contact">
                <a href="{{ route('admin-review-list') }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-commenting-o"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.v2.review') <span
                            style="border-radius: 50% 50%; padding: 1px 6px;background-color: red;color: white;">{{ CNotification::notificationCount('reviews') }}</span></span>
                </a>
            </li>

            <li class="" id="menu-news">
                <a href="{{ route('news-list') }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-pencil-square-o"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.news')</span>
                </a>
            </li>
            <li class="" id="menu-category">
                <a href="{{ route('news-category-list') }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-tags"></i>
                    </span>
                    <span class="pcoded-mtext">@lang('lang.tags')</span>
                </a>
            </li>
            <li class="" id="menu-bank-services-table">
                <a href="{{ route('admin-bank-services-table') }}" class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="fa fa-check"></i>
                    </span>
                    <span class="pcoded-mtext">Bank xizmatlari jadvali</span>
                </a>
            </li>
        </ul>
        {{-- <div class="container py-4 bg-primary text-light">
            --}}
            {{-- <img src="/admin/assets/images/orion.png" width="100" alt="">
            --}}
            {{-- <div>@lang('lang.site_users')</div>
            <div>
                @lang('lang.this_year'): <strong>@php echo number_format(SiteVisitors::count()) @endphp</strong>
            </div>
            <div>
                @lang('lang.this_month'): <strong>@php echo number_format(SiteVisitors::monthly()) @endphp</strong>
            </div>
            <div>
                @lang('lang.today'): <strong>@php echo number_format(SiteVisitors::daily()) @endphp</strong>
            </div>
        </div> --}}
    </div>

</nav>
