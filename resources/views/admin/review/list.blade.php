@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">@lang('lang.v2.review')</a>
    </li>
@endsection

@section('content')
<style type="text/css" media="screen">
    .hide{
        display: none;
    }    
</style>
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.v2.review')</h5>
                                    <div class="card-header-right">
                                        <a href="{{ route('admin-review-add') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                    </div>
                                </div>
                                <div class="card-block font-size-14">
                               {{--  <div class="row">
                                    <div class="col-md-12 mb-2 ">
                                        <form action="{{ route('admin-review-list') }}" method="GET">
                                            <div class="form-group row">
                                                <div class="col-sm-8">
                                                    <input type="text" name="search" class="form-control" value="{{ $search }}" placeholder="Search">
                                                </div>
                                                <div class="col-sm-4">
                                                    <button type="submit" style="font-size: 17px" class=" pt-1 pb-1 btn btn-warning">Search</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div> --}}
                                    <table id="review-table" class="table-responsive-xl table-borderless table table-hover"  style="font-size: 12px !important;">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>User</th>
                                            <th>@lang('lang.bank')</th>
                                            <th>@lang('lang.assessment')</th>
                                            <th>Title</th>
                                            
                                            <th>@lang('lang.service')</th>
                                            <th>@lang('lang.region')</th>
                                            <th>@lang('lang.status')</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($list as $row)
                                                <tr @if(count($row->notifications()) > 0) style="background-color: {{ config('global.review.answer_status_color.'.$row->answer_status) }}" @endif>
                                                    <td>{{ $row->id }}</td>
                                                    <td>{{ $row->user->getName() }}</td>
                                                    <td>{{ $row->bank->name() }}</td>
                                                    <td>{{ $row->assessment }}</td>
                                                    <td>{{ $row->title }}</td>
                                                    
                                                    <td>@if($row->service_id) {{ $row->service->getName() }} @endif</td>
                                                    <td>@if($row->region_id) {{ $row->region->name() }} @endif</td>
                                                    <td>@lang('lang.v2.'.$row->statusText())</td>
                                                    
                                                    <td class="text-right" style="display: flex;">
                                                        <a href="#{{$row->id}}" data-id="{{ $row->id }}" class="btn btn-success p-1 pl-2 text-white ml-1 comments-content" data-toggle="tooltip" data-placement="right"  title="Comments"><i class="fa fa-commenting-o"></i><i class="fa fa-arrow-down"></i></a>
                                                        <a href="{{ route('admin-review-update',['id'=>$row->id]) }}" class="btn btn-primary ml-1 p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                        <a href="{{ route('admin-review-delete',['id'=>$row->id]) }}" class="btn btn-danger ml-1 delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                    </td>

                                                </tr>
                                                <tr id="review-comment-{{$row->id}}" class="hide">
                                                    <td></td>
                                                    <td  colspan="9" id="review-td-comment-{{ $row->id }}">
                                                    <?php $comments = \App\Models\Comment::commentsTree($row); ?>
                                                        @if(count($comments) > 0)
                                                            @include('admin.review.comments')
                                                        @endif
                                                    </td>
                                                    <td style="display: none"></td>
                                                    <td style="display: none"></td>
                                                    <td style="display: none"></td>
                                                    <td style="display: none"></td>
                                                    <td style="display: none"></td>
                                                    <td style="display: none"></td>
                                                    <td style="display: none"></td>
                                                    <td style="display: none"></td>
                                                </tr>
                                                @if(count($row->notifications()) > 0)
                                                    <tr @if(count($row->notifications()) > 0) style="background-color: {{ config('global.review.answer_status_color.'.$row->answer_status) }}" @endif>
                                                        <td colspan="10">
                                                        @foreach($row->notifications() as $not)
                                                        {{-- <span>Ustun: </span> {{ $not->row_column }};&nbsp;&nbsp; --}}
                                                        <span>O'zgartirish: </span> @lang('lang.v2.'.$not->row_column.'.'.config('global.review.'.$not->row_column)[$not->row_column_value]);&nbsp;&nbsp;
                                                        <a class="btn btn-success" href="{{route('del-notification',['id'=>$not->id])}}"  >Ko'rilganga qo'shish</a>
                                                        @endforeach
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <td colspan="10"></td>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

<div id="comment-edit-modal" class="modal fade" role="dialog">
        <div class="modal-dialog " style="max-width: 500px">
            <!-- Modal content-->
            <form id="comment-edit-form" action="{{ route('admin-review-comment-edit') }}" method="POST">
                @csrf
                <input type="hidden" id="comment-id" name="id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                       <div class="col-md-12">
                            <div class="form-group">
                                <label>Text</label>
                                <textarea  rows="4" autocomplete="off" id="commet-text-input" name="text" class="form-control"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success save-comment" >
                            @lang('lang.save')
                        </button>
                        <a class="btn btn-warning close-modal" data-dismiss="modal" >Yopish</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection

@section('script')


    <script>
        $(document).ready(function () {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#menu-review').addClass('active');

            $('.comments-content').click(function(){
                $('#review-comment-'+$(this).attr('data-id')).toggleClass('show').toggleClass('hide');
            });


            $('[data-toggle="tooltip"]').tooltip();

            $('.edit').click(function(){
               id = $(this).attr('data-id');
               $('#commet-text-input').text($('#comment-text-span-'+id).text());
               $('#comment-id').val(id);
            });

            $('.save-comment').click(function(e){
                e.preventDefault();
                let form = $('#comment-edit-form');
                $.ajax({
                    method:'POST',
                    url: form.attr('action'),
                    data:form.serialize(),
                }).done(function(data){
                    $('.close-modal').click();
                    if(data.text){
                        $('#comment-text-span-'+data.id).text(data.text);
                    }
                });
            });

        });

        $(function () {
            $('#review-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : false,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });


    </script>
    <style>
        .float-right{
            float:right !important;
        }
    </style>
@endsection
