@foreach($comments as $comment)
	<div class="col-md-12 ">
		<div class="mb-2">
			<span><i class="fa fa-circle-o"></i> <span id="comment-text-span-{{ $comment->id }}" >{{ $comment->text }} </span>(<b style="font-size: 10px">{{ $comment->user->getName() }}</b>) </span>
			<a style="display: inline-flex;" data-toggle="modal" data-target="#comment-edit-modal" data-id="{{ $comment->id }}" class="btn btn-primary ml-1 p-0 pl-1 edit text-white"><i class="fa fa-pencil"></i></a>
			<a  href="{{ route('admin-review-comment-delete',[$comment->id]) }}" style="display: inline-flex;" class="btn btn-danger ml-1 p-0 pl-1 delete text-white"><i class="fa fa-trash"></i></a>
		</div>
		 @if($comment->sub->count())
		    <div class="comment-item__child ">@include('admin.review.comments',['comments' => $comment->sub])</div>
		@endif
	</div>
@endforeach