@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="#">@lang('lang.v2.review')</a>
    </li>
@endsection

@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.v2.review') | @lang('lang.update')</h5>
                                    <div class="card-header-right">
                                    </div>
                                </div>
                                <div class="card-block font-size-14">
                                    <div class="row ml-3 mr-3">
                                        <div class="col-sm-12 pt-4 ">
                                            <form action="{{ route('admin-review-edit') }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $model->id }}">
                                                <div class="form-group row">
                                                    <div class="col-sm-4 mb-3">
                                                        <label> <b>Status</b></label>
                                                        <select  name="status" class="form-control form-control-lg review-status" required="required">
                                                            @foreach(config('global.review.status') as $k => $v)    
                                                                <option @if($model->status == $k) selected @endif value="{{ $k }}">@lang('lang.v2.'.$v)</option>
                                                            @endforeach
                                                        </select> 
                                                    </div>

                                                    <div class="col-sm-4 mb-3">
                                                        <label> <b>Baho</b></label>
                                                        <select  name="assessment" class="form-control form-control-lg" required="required">
                                                             @foreach(config('global.review.assessment') as $k => $v)    
                                                                <option @if($model->assessment == $k) selected @endif value="{{ $k }}">{{ $v }}</option>
                                                            @endforeach
                                                        </select> 
                                                    </div>

                                                    <div class="col-sm-4 mb-3">
                                                        <label> <b>Javob statusi</b></label>
                                                        <select name="answer_status" class="form-control form-control-lg answer-status">
                                                            @foreach(config('global.review.answer_status') as $st_k => $st_v)
                                                                <option @if($model->answer_status == $st_k) selected @endif value="{{ $st_k }}">@lang('lang.v2.answer_status.'.$st_v)</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                
                                                </div>

                                                  <div class="form-group row">
                                                    <div class="col-sm-12 mb-3">
                                                        <label> <b>Adminstratsiya komentariyasi</b></label>
                                                        <input type="hidden" name="comment_site_id"  value="{{ $model->moderatorComment($model->id)['id'] }}">
                                                        <textarea type="text" name="comment_site" class="form-control form-control-lg admin-comment" rows="6">{{ $model->moderatorComment($model->id)['text'] }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12 mb-3">
                                                        <label> <b>User</b></label>
                                                        <input type="text"  disabled value="{{ $model->user->getName() }}" class="form-control form-control-lg">
                                                        <input type="hidden"  name="user"  value="{{ $model->user->id }}" class="form-control form-control-lg">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <div class="col-sm-4 mb-3">
                                                        <label> <b>Region</b></label>
                                                        <select  name="region" class="form-control form-control-lg" required="required">
                                                            @foreach($regions as $region)
                                                                <option @if($model->region_id == $region->id) selected @endif value="{{ $region->id }}">{{ $region->name()}}</option>
                                                            @endforeach
                                                        </select> 
                                                    </div>
                                                
                                                    <div class="col-sm-4 mb-3">
                                                        <label> <b>Bank</b></label>
                                                        <select  name="bank" class="form-control form-control-lg" required="required">
                                                             @foreach($banks as $bank)
                                                                <option @if($model->bank_id == $bank->id) selected @endif value="{{ $bank->id }}">{{ $bank->name()}}</option>
                                                            @endforeach
                                                        </select> 
                                                    </div>
                                               
                                                    <div class="col-sm-4 mb-3">
                                                        <label> <b>Service</b></label>
                                                        <select  name="service" class="form-control form-control-lg">
                                                            <option value="">@lang('lang.choose')</option>
                                                            @foreach($services as $service)
                                                                @if(count($service->children()) > 0)
                                                                    <option @if($service->id  == $model->service_id) selected @endif value="{{ $service->id }}">{{ $service->getName() }}</option>
                                                                    @foreach($service->children() as $child)
                                                                        <option @if($child->id  == $model->service_id) selected @endif value="{{ $child->id }}">&nbsp;&nbsp;{{ $child->getName() }}</option>
                                                                    @endforeach
                                                                @else
                                                                    <option @if($service->id  == $model->service_id) selected @endif value="{{ $service->id }}">{{ $service->getName() }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select> 
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-12 mb-3">
                                                        <label> <b>Title</b></label>
                                                        <input type="text" name="title" value="{{ $model->title }}" class="form-control form-control-lg" required="required">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12 mb-3">
                                                        <label> <b>Text</b></label>
                                                        <textarea type="text" name="fulltext" class="form-control form-control-lg" required="required" rows="6">{{ $model->fulltext }}</textarea>
                                                    </div>
                                                </div> 

                                                <div class="form-group row">
                                                    <div class="col-sm-12 mb-3">
                                                        <label> <b>Bank uchun info</b></label>
                                                        <textarea type="text" name="info_bank" class="form-control form-control-lg" rows="6">{{ $model->info_bank }}</textarea>
                                                    </div>
                                                </div>
                                               
                                                <div class="form-group row">
                                                    <div class="col-sm-12 mb-3">
                                                        <button type="submit" class="btn btn-info send-btn">
                                                            @lang('lang.save')
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <style>
        .float-right{
            float:right !important;
        }
    </style>

    <script>
        
            $('.review-status').on('change',function(){
                val = $(this).val();
                if(val == 0 || val == 2){
                   $('.answer-status').val(3).trigger('change');
                }
            });

            val = $('.review-status').val();
            if(val == 0 || val == 2){
               $('.answer-status').val(3).trigger('change');
            }


    </script>

@endsection
