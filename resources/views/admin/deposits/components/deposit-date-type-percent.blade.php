@if($dates)
	@foreach($dates as $date)
	<div class=" col-md-12 row">
		<div class="col-sm-2 mb-3">
			<label> <b>@lang('lang.date')</b></label>
			<input disabled value="{{ $date->name() }}" class="form-control form-control-lg">	
		</div>
		<div class="col-sm-2 mb-3">
			<label> <b>@lang('lang.percent')</b></label>
			<input required="required" type="text" name="deposit_date_type_percet_{{ $date->id }}" class="form-control form-control-lg">
		</div>
		<div class="col-sm-2 mb-3">
			<label> <b>@lang('lang.type')</b></label>
			<select required name="deposit_date_type_{{ $date->id }}" class="form-control">
				@foreach($date_type_id as $type)
					<option value="{{ $type->id }}">{{ $type->name() }}</option>
				@endforeach
			</select>
		</div>
		<div class="col-sm-2 mb-3">
			<label> <b>@lang('lang.date')</b></label>
			<input required  type="text" name="deposit_date_{{ $date->id }}" class="form-control form-control-lg"/>
		</div>
		<div class="col-sm-4 mb-3">
			<label> <b>@lang('lang.date') (saytda ko'rsatilgan)</b></label>
			<input required="required" type="text" name="date_name_for_site_{{ $date->id }}" class="form-control form-control-lg">
		</div>
	</div>
	@endforeach
@endif

@if($model)
	@foreach($model as $row)
		<div class=" col-md-12 row">
			<div class="col-sm-1 mb-3">
				<label>Delete</label>
				<a data-del-id="{{ $row->id }}" class="btn btn-danger child-delete text-white"><i class="fa fa-trash"></i></a>
			</div>
			<div class="col-sm-2 mb-3">
			    <label> <b>@lang('lang.date')</b></label>
			    <input disabled value="{{ $row->dateType->name() }}" class="form-control form-control-lg">
			</div>
			<div class="col-sm-2 mb-3">
			    <label> <b>@lang('lang.percent')</b></label>
			    <input required="required" type="text" value="{{ $row->percent }}" name="deposit_date_type_percet_{{ $row->dateType->id }}" class="form-control form-control-lg">
			</div>
			<div class="col-sm-1 mb-3">
			    <label> <b>@lang('lang.type')</b></label>
			    <select required name="deposit_date_type_{{ $row->dateType->id }}" class="form-control">
			    	@foreach($date_type_id as $type)
			    		<option @if($type->id == $row->date_type) selected="selected"  @endif value="{{ $type->id }}">{{ $type->name() }}</option>
			    	@endforeach
			    </select>
			</div>
			<div class="col-sm-2 mb-3">
				<label> <b>@lang('lang.date')</b></label>
				<input required  type="text" name="deposit_date_{{ $row->dateType->id }}" value="{{ $row->date }}" class="form-control form-control-lg"/>
			</div>
			<div class="col-sm-4 mb-3">
			    <label> <b>@lang('lang.date')(saytda ko'rsatilgan)</b></label>
			    <input required="required" type="text" value="{{ $row->date_name_for_site }}" name="date_name_for_site_{{ $row->dateType->id }}" class="form-control form-control-lg">
			</div>
		</div>
	@endforeach
@endif

<script>
	let date_type = $('#deposit-date-type-list').val();
	$('.child-delete').click(function(){
        $.ajax({
            type:'GET',
            url: "{{ route('deposit-date-type-percent-input') }}",
            data:{
                'date_type_id': date_type,
                'id_deposit' : $('#id-deposit').val(),
                'delete_id': $(this).attr('data-del-id')
            },success:function(data){
                $('#content-date-type-percent').html(data);
            }
        });
    });	
</script>