@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('deposit-list') }}">@lang('lang.deposits')</a>
    </li>
@endsection

@section('content')
      <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>@lang('lang.deposits')</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-info p-1 text-white pl-2 pr-3" href="{{ route('deposit-add-view') }}">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table class="table-responsive-xl table-bordered table">
                                        <thead>
                                            <tr>
                                                <th>@lang('lang.options')</th>
                                                <th>{{ Yt::trans('Qiymat va Izohlar','uz') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th>@lang('lang.account_fill')</th>
                                                <td>@if($deposit->account_fill_rules){{ Yt::trans($deposit->account_fill_rules,'uz') }}@else @lang('lang.no')@endif</td>
                                            </tr> 
                                            <tr>
                                                <th>@lang('lang.partly_take')</th>
                                                <td>@if($deposit->partly_take_rules){{ Yt::trans($deposit->partly_take_rules,'uz')  }}@else @lang('lang.no')@endif</td>
                                            </tr>
                                             <tr>
                                                <th>@lang('lang.percents_capitalization')</th>
                                                <td>@if($deposit->percents_capitalization_rules) {{ Yt::trans($deposit->percents_capitalization_rules,'uz') }}@else @lang('lang.no') @endif</td>
                                            </tr>
                                             <tr>
                                                <th>@lang('lang.privilege_take')</th>
                                                <td>@if($deposit->privilege_take_text){{ Yt::trans($deposit->privilege_take_text,'uz') }}@else @lang('lang.no') @endif</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <script>
        $(document).ready(function () {
           
            $('#deposits-parent').addClass('active pcoded-trigger');
            $('#deposits-child1').addClass('active');

        });
    </script>
<style>
.float-right{
    float:right !important;
}
</style>
@endsection
