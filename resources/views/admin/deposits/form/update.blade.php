<div class="row">
    <div class="col-sm-12 pt-4 pl-2">
        <form action="{{ route('deposit-update') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" id="id-deposit" name="id" value="{{ $deposit->id }}" >
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.bank')</b></label>
                    <select required="required" type="text" name="bank" class="form-control form-control-lg">
                        <option value="">- @lang('lang.choose') -</option>
                        @foreach($banks as $bank)
                            <option @if($deposit->bank_id == $bank->id) selected='selected'@endif value="{{ $bank->id }}"> {{ $bank->name_uz }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.name') UZ</b></label>
                    <input required="required" value="{{ $deposit->name_uz }}" type="text" name="name_uz" class="form-control form-control-lg">
                </div>

                <div class="col-sm-6 mb-3">
                    <label> <b>Номи УЗ</b></label>
                    <input required="required" value="{{ $deposit->name_ru }}" type="text" name="name_ru" class="form-control form-control-lg">
                </div>

            </div>
            <div class="form-group row">
                <div class="col-sm-4 mb-3">
                    <label> <b>@lang('lang.deposit_type')</b></label>
                    <select required="required" type="text" name="deposit_type" class="form-control form-control-lg">
                        <option value="">- @lang('lang.choose') -</option>
                        @foreach($deposit_type as $dep_type)
                            <option @if($deposit->deposit_type_id == $dep_type->id) selected='selected'@endif value="{{ $dep_type->id }}"> {{ $dep_type->name() }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-4 mb-3">
                    <label> <b>@lang('lang.type_sum')</b></label>
                    <select  type="text" name="type_sum" class="form-control form-control-lg">
                        <option value="">- @lang('lang.choose') -</option>
                        @foreach($sumType as $k => $v)
                            <option  @if($deposit->type_sum == $v) selected='selected'@endif  value="{{ $v }}"> @lang('lang.'.$k)</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-4 mb-3">
                    <label> <b>@lang('lang.currency')</b></label>
                    <select required="required" type="text" name="currency" class="form-control form-control-lg">
                        @foreach($currency as $cur)
                            <option @if($deposit->currency == $cur->id) selected='selected'@endif value="{{ $cur->id }}"> {{ $cur->name }}</option>
                        @endforeach
                    </select>
                </div>

            </div>

            <div class="form-group">
                 <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.deposit_date')</b></label>
                    <select required="required" type="text" id="deposit-date-type-list" name="date_type[]" multiple class="date-type-deposit select2 form-control form-control-lg">
                        <option value="">- @lang('lang.choose') -</option>
                        @foreach($date_type as $d_type)
                            <option @if(in_array($d_type->id, explode(',', $deposit->deposit_date_type))) selected='selected'@endif value="{{ $d_type->id }}"> {{ $d_type->name() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-gropu row" id='content-date-type-percent'></div>
            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.deposit_date_string') uz</b></label>
                    <input required="required" value="{{ $deposit->dateStringLang('uz') }}" type="text" name="deposit_date_string_uz" class="form-control form-control-lg">
                </div>
                 <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.deposit_date_string') ru</b></label>
                    <input required="required" value="{{ $deposit->dateStringLang('ru') }}" type="text" name="deposit_date_string_ru" class="form-control form-control-lg">
                </div>
               
                 <div class="col-sm-3 mb-3">
                    <label> <b>@lang('lang.type')</b></label>
                    <select required="required" type="text" name="date_type_id" class="form-control form-control-lg">
                    <option value="">@lang('lang.choose')</option>
                       @foreach($date_type_id as $type_id)
                        <option @if( $deposit->date_type_id == $type_id->id) selected @endif  value="{{ $type_id->id }}"> {{ $type_id->name() }}</option>
                       @endforeach
                    </select>
                </div>
                <div class="col-sm-4 mb-3">
                    <label> <b>@lang('lang.deposit_date')</b></label>
                    <input required="required" value="{{ $deposit->deposit_date }}" type="text" name="deposit_date" class="form-control form-control-lg">
                </div>
               

            </div>
            <div class="form-group row">
                <div class="col-sm-2 mb-3">
                <label for="">Boshqa foizlar</label><br>
                    @if($deposit->other_percent)
                    <input name="other_percent" value="1" type="checkbox" checked>
                    @else
                    <input name="other_percent" value="1" type="checkbox">
                    @endif
                </div>
                <div class="col-sm-4 mb-3">
                    <label> <b>@lang('lang.percent')</b></label>
                    <input required="required" value="{{ $deposit->deposit_percent }}" type="text" name="percent" class="form-control form-control-lg">
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.min_sum')</b></label>
                    <input type="text" value="{{ $deposit->min_sum }}" name="min_sum" class="form-control form-control-lg">
                </div>
            </div>

            <div id="deposit_date" class="form-group row">
                <div class="col-sm-12 mb-3 " id="add-btn-form" >
                    <a id="add-new-diff-date-input" class="btn btn-success text-white"><i class="fa fa-plus"></i></a>
                    <input id="data-amount-input" type="hidden" name="diff_date_amount_input" value=" {{ count($deposit->depositPercentDate) }}">
                    <label >Differensial muddat qo'shish qo'shish</label>
                        @if($deposit->depositPercentDate)
                             @include('admin.deposits.form.deposit-percent-date')
                        @endif
                </div>
            </div>

            <div class="form-group row" style="border-top:1px solid black; color:red;">
                <div class="col-md-3">
                    <div class=" col-sm-12 mb-3">
                        <label for="aktsiya"> <b>Aktsiya</b></label>
                        <input name="aktsiya" value="1" type="checkbox" @if($deposit->aktsiya) checked @endif>
                     </div>
                </div>
                
                 <div class="col-md-9">
                    <div class="col-md-12">
                        <label><b>Izoh UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="aktsiya_izoh_uz">{{ $deposit->aktsiya_izoh('uz') }}</textarea>
                    </div>
                     <div class="col-md-12">
                        <label><b>Izoh RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="aktsiya_izoh_ru">{{ $deposit->aktsiya_izoh('ru')}}</textarea>
                    </div>
                    <div class="col-md-12">
                        <label><b>Link UZ</b></label>
                        <input type="text" class="form-control form-control-lg" name="aktsiya_link_uz" value="{{ $deposit->aktsiya_link('uz') }}"/>
                    </div>
                     <div class="col-md-12">
                        <label><b>Link RU</b></label>
                        <input type="text" class="form-control form-control-lg" name="aktsiya_link_ru" value="{{ $deposit->aktsiya_link('ru') }}"/>
                    </div>  
                 </div>
            </div>

            <div class="form-group row" style="border-top:1px solid">
                <div class="col-sm-4">
                    <div class=" col-sm-12 mb-3">
                        <label> <b>@lang('lang.account_fill')</b></label>
                        <select required="required" type="text" name="account_fill" class="form-control form-control-lg">
                            @foreach(config('global.yes_or_no') as $k => $v)
                                <option @if($deposit->account_fill == $v) selected="selected" @endif value="{{ $v }}"> @lang('lang.'.$k)</option>
                            @endforeach
                        </select>
                     </div>
                </div>
                
                 <div class="col-md-8">
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="account_fill_rules_uz">{{ $deposit->accountFillRules('uz') }}</textarea>
                    </div>
                     <div class="col-md-12">
                        <label><b>@lang('lang.note') RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="account_fill_rules_ru">{{ $deposit->accountFillRules('ru') }}</textarea>
                    </div>                     
                 </div>

            </div>
            <div class="form-group row" style="border-top:1px solid">
                <div class="col-sm-4">
                    <div class="col-sm-12 mb-3">
                        <label> <b>@lang('lang.partly_take')</b></label>
                        <select required="required" type="text" name="partly_take" class="form-control form-control-lg">
                            @foreach(config('global.yes_or_no') as $k => $v)
                                <option @if($deposit->partly_take == $v) selected="selected" @endif value="{{ $v }}"> @lang('lang.'.$k)</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="partly_take_rules_uz">{{ $deposit->partlyTakeRules('uz') }}</textarea>
                    </div>
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="partly_take_rules_ru">{{ $deposit->partlyTakeRules('ru') }}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row" style="border-top:1px solid">
                <div class="col-sm-4 ">
                    <div class="col-sm-12 mb-3">
                        <label> <b>@lang('lang.percents_capitalization')</b></label>
                        <select required="required" type="text" name="percents_capitalization" class="form-control form-control-lg">
                            @foreach(config('global.yes_or_no') as $k => $v)
                                <option @if($deposit->percents_capitalization == $v) selected="selected" @endif value="{{ $v }}"> @lang('lang.'.$k)</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-8 ">
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="percents_capitalization_rules_uz">{{ $deposit->percentsCapitalizationRules('uz') }}</textarea>
                    </div>
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="percents_capitalization_rules_ru">{{ $deposit->percentsCapitalizationRules('ru') }}</textarea>
                    </div>
                </div>
            </div>

            <div class="form-group row" style="border-top:1px solid">
                <div class="col-sm-4 mb-3">
                    <label> <b>@lang('lang.percent_paid_period')</b></label>
                    <select required="required" type="text" name="percent_paid_period" class="form-control form-control-lg">
                        @foreach($paid_period as $period)
                            <option @if($deposit->percent_paid_period == $period->id) selected @endif value="{{ $period->id }}"> {{ $period->name() }}</option>
                        @endforeach
                    </select>
                </div>

                {{--<div class="col-sm-8 mb-3">--}}
                    {{--<label> <b>@lang('lang.close_before_date')</b></label>--}}
                    {{--<input type="text" value="{{ $deposit->close_before_date }}" name="close_before_date" class="form-control form-control-lg">--}}
                {{--</div>--}}

            </div>
            <div class="form-group row" style="border-top:1px solid">
                <div class="col-sm-4">
                    <div class="col-sm-12 mb-3">
                        <label> <b>@lang('lang.privilege_take')</b></label>
                        <select required="required" type="text" name="privilege_take" class="form-control form-control-lg">
                            @foreach(config('global.yes_or_no') as $k => $v)
                                <option @if($deposit->privilege_take == $v) selected="selected" @endif value="{{ $v }}"> @lang('lang.'.$k)</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="privilege_take_text_uz">{{ $deposit->privilegeTakeText('uz') }}</textarea>
                    </div>
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="privilege_take_text_ru">{{ $deposit->privilegeTakeText('ru') }}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group row" style="border-top:1px solid">
                <div  class="col-sm-4 ">
                    <div class="col-sm-12 mb-3">
                        <label> <b>@lang('lang.dp_open_type')</b></label>
                        <select required="required" type="text" name="type_open[]" multiple="" class="form-control select2 form-control-lg">
                            @php $openT = explode(',',$deposit->type_open);  @endphp
                            @foreach($type_open as $type_o)
                                @foreach($openT as $kT => $vT)
                                <option @if($type_o->id  == $vT)selected @endif value="{{ $type_o->id }}"> {{ $type_o->name() }}</option>
                                @endforeach
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="open_type_text_uz">{{ $deposit->openTypeText('uz') }}</textarea>
                    </div>
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="open_type_text_ru">{{ $deposit->openTypeText('ru') }}</textarea>
                    </div>
               </div>
            </div>
            <div  class="form-group row">
                 <div class="col-sm-8 mb-3">
                    <label> <b>@lang('lang.link') uz</b></label>
                    <input type="text" value="{{ $deposit->link('uz') }}" name="link_uz" class="form-control form-control-lg">
                </div>
            </div>
             <div  class="form-group row">
                 <div class="col-sm-8 mb-3">
                    <label> <b>@lang('lang.link') ru</b></label>
                    <input type="text" value="{{ $deposit->link('ru') }}" name="link_ru" class="form-control form-control-lg">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Boshqa @lang('lang.rules') UZ</b></label>
                    <textarea  name="text_uz" rows="3" class="form-control form-control-lg">{{ $deposit->text_uz }}</textarea>
                </div>

            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b> Boshqa  @lang('lang.rules') RU</b></label>
                    <textarea  name="text_ru" rows="3" class="form-control form-control-lg">{{ $deposit->text_ru }}</textarea>
                </div>

            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-info ">
                        @lang('lang.save')
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
