@php $i = 0; @endphp
@foreach($deposit->depositPercentDate as $row_percent_date)
    @php $ii = ++$i;  @endphp
{{--
     $small = strstr($row_percent_date->date,'>');
    if($small){
        $r1 =  str_replace('>','',$row_percent_date->date);
        print_r( explode('-',$r1));
    } $big = strstr($row_percent_date->date,'<');
    if($big){
        $r2 = str_replace('<','',$row_percent_date->date);
        print_r( explode('-',$r2));

    }
--}}
<div class="col-sm-12 pr-0 pl-0 remove-div" id="diff_form_{{ $ii }}" style="display: flex">
    <input type="hidden" class="input_dp_base_date" name="diff_deposit_date_type_input_{{$ii}}" value="{{ $row_percent_date->id }}" >
    <div class="col-sm-1"><label> O'chirish</label>
        <a onclick="remove({{ $ii  }},{{ $row_percent_date->id }})" data-del-id="{{ $row_percent_date->id }}" class="btn btn-danger text-white" data-div-id="{{$ii}}">
        <i class="fa fa-trash"></i></a>
        </div>
    <div class="col-sm-3 mb-3"><label> Foiz hisoblash</label>
        <select required="required"  name="diff_percent_consider_input_{{ $ii}}" class="form-control input_percent_consider form-control-lg">
            <option value="">-Tanlang-</option>
            @foreach(config('global.percent_consider') as $k => $v )
            <option @if($row_percent_date->percent_consider == $v['k']) selected="selected" @endif value="{{$v['k']}}">{{$v['name_uz']}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-sm-2 mb-3"><label> Muddat turi</label>
        <select required="required"  name="diff_date_type_input_{{ $ii}}" class="form-control input_data_type form-control-lg">
            @foreach(config('global.date_type') as $k => $v)
             <option @if($row_percent_date->date_type == $v['k']) selected="selected" @endif value="{{ $v['k'] }}">{{ $v['name'] }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-sm-3 mb-3"><label> Muddat</label>
        <input required="required" type="text" name="diff_date_input_{{ $ii}}" class="form-control input_date form-control-lg" value="{{ $row_percent_date->date }}">
    </div>
    <div class="col-sm-3 mb-3"><label> Foiz</label>
        <input required="required" type="text" name="diff_percent_input_{{ $ii}}" class="form-control input_percent form-control-lg" value="{{ $row_percent_date->percent }}">
    </div>
</div>
@endforeach
