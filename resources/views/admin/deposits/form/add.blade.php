<div class="row">
    <div class="col-sm-12 pt-4 pl-2">
        <form action="{{ route('deposit-add') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" id="id-deposit" name="id" value="" >
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.bank')</b></label>
                    <select required="required" type="text" name="bank" class="form-control form-control-lg">
                        <option value="">- @lang('lang.choose') -</option>
                        @foreach($banks as $bank)
                            <option value="{{ $bank->id }}"> {{ $bank->name_uz }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.name')UZ</b></label>
                    <input required="required" type="text" name="name_uz" class="form-control form-control-lg">
                </div>

                <div class="col-sm-6 mb-3">
                    <label> <b>Номи УЗ</b></label>
                    <input required="required" type="text" name="name_ru" class="form-control form-control-lg">
                </div>

            </div>
            <div class="form-group row">
                    <div class="col-sm-4 mb-3">
                        <label> <b>@lang('lang.deposit_type')</b></label>
                        <select required="required" type="text" name="deposit_type" class="form-control form-control-lg">
                            <option value="">- @lang('lang.choose') -</option>
                            @foreach($deposit_type as $dep_type)
                                <option value="{{ $dep_type->id }}"> {{ $dep_type->name() }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-4 mb-3">
                        <label> <b>@lang('lang.type_sum')</b></label>
                        <select type="text" name="type_sum" class="form-control form-control-lg">
                            <option value="">- @lang('lang.choose') -</option>
                            @foreach($sumType as $k => $v)
                                <option value="{{ $v }}"> @lang('lang.'.$k)</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-4 mb-3">
                        <label> <b>@lang('lang.currency')</b></label>
                        <select required="required" type="text" name="currency" class="form-control form-control-lg">
                            @foreach($currency as $cur)
                                <option value="{{ $cur->id }}"> {{ $cur->name }}</option>
                            @endforeach
                        </select>
                    </div>


            </div>

            <div class="form-group">
                <div class="col-sm-12  mb-3">
                    <label> <b>@lang('lang.deposit_date')</b></label>
                    <select required="required" type="text" id="deposit-date-type-list"  name="date_type[]" multiple class="date-type-deposit select2 form-control form-control-lg">
                        @foreach($date_type as $d_type)
                            <option value="{{ $d_type->id }}"> {{ $d_type->name() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class=" row" id='content-date-type-percent'></div>

            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.deposit_date_string') uz</b></label>
                    <input required="required" type="text" name="deposit_date_string_uz" class="form-control form-control-lg">
                </div>
                 <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.deposit_date_string') ru</b></label>
                    <input required="required" type="text" name="deposit_date_string_ru" class="form-control form-control-lg">
                </div>

                
                 <div class="col-sm-3 mb-3">
                    <label> <b>@lang('lang.type')</b></label>
                    <select required="required" type="text" name="date_type_id" class="form-control form-control-lg">
                        <option value="">@lang('lang.choose')</option>
                           @foreach($date_type_id as $type_id)
                            <option  value="{{ $type_id->id }}"> {{ $type_id->name() }}</option>
                           @endforeach
                    </select>
                </div>
                <div class="col-sm-4 mb-3">
                    <label> <b>@lang('lang.deposit_date') </b></label>
                    <input required="required" type="text" name="deposit_date" class="form-control form-control-lg">
                </div>

            </div>


            <div class="form-group row">
                <div class="col-sm-2 mb-3">
                
                <label for="">Boshqa foizlar</label><br>
                    <input name="other_percent" value="1" type="checkbox">
                </div>
                <div class="col-sm-4 mb-3">
                    <label> <b>@lang('lang.percent')</b></label>
                    <input required="required" type="text" name="percent" class="form-control form-control-lg">
                </div>

                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.min_sum')</b></label>
                    <input type="text" name="min_sum" class="form-control form-control-lg">
                </div>


            </div>


            <div id="deposit_date" class="form-group row">
                <div class="col-sm-12 mb-3 " id="add-btn-form" >
                    <a id="add-new-diff-date-input" class="btn btn-success text-white"><i class="fa fa-plus"></i></a>
                    <input id="data-amount-input" type="hidden" name="diff_date_amount_input" value="0">
                    <label >Differensial muddat qo'shish qo'shish</label>
                </div>
            </div>

            <div class="form-group row" style="border-top:1px solid black; color:red;">
                <div class="col-md-3">
                    <div class=" col-sm-12 mb-3">
                        <input name="aktsiya" value="1" type="checkbox">
                        <label for="aktsiya"> <b>Aktsiya</b></label>
                     </div>
                </div>
                
                <div class="col-md-9">
                    <div class="col-md-12">
                        <label><b>Izoh UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="aktsiya_izoh_uz"></textarea>
                    </div>
                     <div class="col-md-12">
                        <label><b>Izoh RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="aktsiya_izoh_ru"></textarea>
                    </div>  
                    <div class="col-md-12">
                        <label><b>Link UZ</b></label>
                        <input type="text" class="form-control form-control-lg" name="aktsiya_link_uz"/>
                    </div>
                     <div class="col-md-12">
                        <label><b>Link RU</b></label>
                        <input type="text" class="form-control form-control-lg" name="aktsiya_link_ru"/>
                    </div>
                 </div>
            </div>

            <div class="form-group row" style="border-top:1px solid">
                <div class="col-sm-4">
                    <div class=" col-sm-12 mb-3">
                        <label> <b>@lang('lang.account_fill')</b></label>
                        <select required="required" type="text" name="account_fill" class="form-control form-control-lg">
                            @foreach(config('global.yes_or_no') as $k => $v)
                                <option value="{{ $v }}"> @lang('lang.'.$k)</option>
                            @endforeach
                        </select>
                     </div>
                </div>
                
                 <div class="col-md-8">
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="account_fill_rules_uz"></textarea>
                    </div>
                     <div class="col-md-12">
                        <label><b>@lang('lang.note') RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="account_fill_rules_ru"></textarea>
                    </div>                     
                 </div>
            </div>

            <div class="form-group row" style="border-top:1px solid">
                <div class="col-sm-4">
                    <div class="col-sm-12 mb-3">
                        <label> <b>@lang('lang.partly_take')</b></label>
                        <select required="required" type="text" name="partly_take" class="form-control form-control-lg">
                            @foreach(config('global.yes_or_no') as $k => $v)
                                <option value="{{ $v }}"> @lang('lang.'.$k)</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="partly_take_rules_uz"></textarea>
                    </div>
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="partly_take_rules_ru"></textarea>
                    </div>
                </div>
            </div>

            <div class="form-group row" style="border-top:1px solid">
                <div class="col-sm-4 ">
                    <div class="col-sm-12 mb-3">
                        <label> <b>@lang('lang.percents_capitalization')</b></label>
                        <select required="required" type="text" name="percents_capitalization" class="form-control form-control-lg">
                            @foreach(config('global.yes_or_no') as $k => $v)
                                <option value="{{ $v }}"> @lang('lang.'.$k)</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-8 ">
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="percents_capitalization_rules_uz"></textarea>
                    </div>
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="percents_capitalization_rules_ru"></textarea>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-4 mb-3">
                    <label> <b>@lang('lang.percent_paid_period')</b></label>
                    <select required="required" type="text" name="percent_paid_period" class="form-control form-control-lg">
                        @foreach($paid_period as $period)
                            <option value="{{ $period->id }}"> {{ $period->name() }}</option>
                        @endforeach
                    </select>
                </div>

                {{--<div class="col-sm-8 mb-3">
                    <label> <b>@lang('lang.close_before_date')</b></label>
                    <input type="text" name="close_before_date" class="form-control form-control-lg">
                </div>--}}

             </div>
            <div class="form-group row" style="border-top:1px solid">
                <div class="col-sm-4">
                    <div class="col-sm-12 mb-3">
                        <label> <b>@lang('lang.privilege_take')</b></label>
                        <select required="required" type="text" name="privilege_take" class="form-control form-control-lg">
                            @foreach(config('global.yes_or_no') as $k => $v)
                                <option value="{{ $v }}"> @lang('lang.'.$k)</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="privilege_take_text_uz"></textarea>
                    </div>
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="privilege_take_text_ru"></textarea>
                    </div>
                </div>
            </div>
             <div class="form-group row" style="border-top:1px solid">
                <div  class="col-sm-4 ">
                    <div class="col-sm-12 mb-3">
                        <label> <b>@lang('lang.dp_open_type')</b></label>
                        <select required="required" type="text" name="type_open[]" multiple="" class="form-control select2 form-control-lg">
                            @foreach($type_open as $type_o)
                                <option value="{{ $type_o->id }}"> {{ $type_o->name() }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="open_type_text_uz"></textarea>
                    </div>
                    <div class="col-md-12">
                        <label><b>@lang('lang.note') RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="open_type_text_ru"></textarea>
                    </div>
               </div>
            </div>
            <div class="form-group row">
                   <div class="col-sm-8 mb-3">
                    <label> <b>@lang('lang.link') uz</b></label>
                    <input type="text" name="link_uz" class="form-control form-control-lg">
                </div>  
            </div> 
            <div class="form-group row">
                   <div class="col-sm-8 mb-3">
                    <label> <b>@lang('lang.link') ru</b></label>
                    <input type="text" name="link_ru" class="form-control form-control-lg">
                </div>  
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Boshqa  @lang('lang.rules') UZ</b></label>
                    <textarea  name="text_uz" rows="3" class="form-control form-control-lg"></textarea>
                </div>

             </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Boshqa @lang('lang.rules') RU</b></label>
                    <textarea  name="text_ru" rows="3" class="form-control form-control-lg"></textarea>
                </div>

             </div>


            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-info ">
                        @lang('lang.save')
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
