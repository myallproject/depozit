@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('deposit-list') }}">@lang('lang.deposits')</a>
    </li>
    <li class="breadcrumb-item">
        <a href="">
            {!! !empty($deposit) ? __('lang.edit') : __('lang.add_new') !!}
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{!! !empty($deposit) ? __('lang.edit') : __('lang.add_new') !!}</h5>
                                    @if($deposit)
                                        <div class="card-header-right">
                                            <a href="{{ route('deposit-add-view') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                        </div>
                                    @endif
                                </div>
                                <div class="card-body p-0">
                                    <div class="col-md-12">
                                        @if($deposit)
                                            @include('admin.deposits.form.update')
                                        @else
                                            @include('admin.deposits.form.add')
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>

        $(document).ready(function () {
            $('.btn-delete-d-date').click(function(){

            });
            
            let date_type = $('#deposit-date-type-list').val();
            if(date_type != ''){
                 $.ajax({
                    type:'GET',
                    url: "{{ route('deposit-date-type-percent-input') }}",
                    data:{
                        'date_type_id': date_type,
                        'id_deposit' : $('#id-deposit').val()
                    },success:function(data){
                        $('#content-date-type-percent').html(data);
                    }
                });
            }
            
            
           
            $('.date-type-deposit').on('change',function(){
                let date_type = $('#deposit-date-type-list').val();
                $.ajax({
                    type:'GET',
                    url: "{{ route('deposit-date-type-percent-input') }}",
                    data:{
                        'date_type_id': date_type,
                        'id_deposit' : $('#id-deposit').val()
                    },success:function(data){
                        $('#content-date-type-percent').html(data);
                    }
                });
            });

            remove = function(id,del_id = false){
                if(confirm("Siz maydonni o'chirasizmi?")){

                    $('#data-amount-input').val(parseInt($('#data-amount-input').val()-1));

                    if(del_id){
                        if($('.remove-div').find('.input_dp_base_date').val()){

                            $.ajax({
                                type:'POST',
                                url:"{{ route('delete-deposit-diff-date') }}",
                                data: {
                                    'd_date_id': del_id,
                                    '_token': $('input[name=_token]').val(),

                                },success:function (data) {

                                },error:function () {
                                    alert('error');
                                }
                            });
                        }
                    }

                    $('#diff_form_'+id).remove();

                    $('.remove-div').each(function(i,v){
                        a_id = $(this).find('a').attr('data-div-id');
                        if(a_id > id){
                            if($(this).find('.input_dp_base_date').val()){

                                $(this).find('.input_dp_base_date').attr('name','diff_deposit_date_type_input_'+(a_id-1));
                            }
                            $(this).attr('id','diff_form_'+((parseInt(a_id)-1)));
                            $(this).find('a').attr('data-div-id',(parseInt(a_id)-1));
                            $(this).find('a').attr('onclick','remove('+(parseInt(a_id)-1)+')');
                            $(this).attr('id','diff_form_'+(parseInt(a_id)-1));
                            $(this).find('a').attr('onclick','remove('+(a_id-1)+')');
                            $(this).find('.input_date').attr('name','diff_date_input_'+(a_id-1));
                            $(this).find('.input_percent').attr('name','diff_percent_input_'+(a_id-1));
                            $(this).find('.input_data_type').attr('name','diff_date_type_input_'+(a_id-1));
                            $(this).find('.input_percent_consider').attr('name','diff_percent_consider_input_'+(a_id-1));

                        }
                    });

                }

            };

            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });

            $('#deposits-parent').addClass('active pcoded-trigger');
            $('#deposits-child1').addClass('active');

            $('#add-new-diff-date-input').click(function(){
                $('#data-amount-input').val(parseInt($('#data-amount-input').val())+1);
                $('#add-btn-form').append(
                    '<div class="col-sm-12 pr-0 pl-0 remove-div" id="diff_form_'+$("#data-amount-input").val()+'" style="display: flex">\n' +
                        '<input type="hidden" class="input_dp_base_date" name="diff_deposit_date_type_input_'+$("#data-amount-input").val()+'" value="" >\n'+
                        '<div class="col-sm-1"><label> O\'chirish</label><a onclick="remove('+$("#data-amount-input").val()+')" class=" btn btn-danger text-white" data-div-id="'+$("#data-amount-input").val()+'">' +
                            '<i class="fa fa-trash"></i></a>'+
                        '</div>\n'+
                        '<div class="col-sm-3 mb-3"><label> Foiz hisoblash</label>\n' +
                            '<select required="required"  name="diff_percent_consider_input_'+parseInt($("#data-amount-input").val())+'" class="form-control input_percent_consider form-control-lg">' +
                                '<option value="">-Tanlang-</option>'+
                                '<option value="1">Turli muddatda va foizda</option>'+
                                '<option value="2">Differensial</option>'+
                                '<option value="3">Bank orqali ochish</option>'+
                                '<option value="4">Mobil ilova orqali ochish</option>'+
                            '</select>\n'+
                        '</div>\n'+
                        '<div class="col-sm-2 mb-3"><label> Muddat turi</label>\n' +
                            '<select required="required"  name="diff_date_type_input_'+parseInt($("#data-amount-input").val())+'" class="form-control input_data_type form-control-lg">' +
                            '<option value="1">Kun</option>'+
                            '<option value="2">Oy</option>'+
                            '<option value="3">Yil</option>'+
                            '</select>\n'+
                        '</div>\n' +

                        '<div class="col-sm-3 mb-3"><label> Muddat</label>\n' +
                            '<input required="required" type="text" name="diff_date_input_'+parseInt($("#data-amount-input").val())+'" class="form-control input_date form-control-lg">\n' +
                        '</div>\n' +
                        '<div class="col-sm-3 mb-3"><label> Foiz</label>\n' +
                            '<input required="required" type="text" name="diff_percent_input_'+parseInt($("#data-amount-input").val())+'" class="form-control input_percent form-control-lg">\n' +
                        '</div>\n' +
                    '</div>'
                );
            });
        });


    </script>
@endsection
