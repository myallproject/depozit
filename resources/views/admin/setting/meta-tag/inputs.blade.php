@if(!$model)
    <input id="input-amount" type="hidden" name="input_amount" value="1">
     <div class="col-md-12">
        <div class="form-group">
            <label>
                <b> Meta </b> <input type="radio" required name="tag" value="meta">
            </label>
            <br>
            <label>
                <b> Title </b> <input type="radio" required name="tag" value="title|title">
            </label> 
            <br>
            {{--<label>
                <b> h1 </b> <input type="radio" required name="tag" value="h1|h1">
            </label> <br>
            <label>
                <b> h2 </b> <input type="radio" required name="tag" value="h2|h2">
            </label> <br>
            <label>
               <b> h3 </b><input type="radio" required name="tag" value="h3|h3">
            </label><br>
            <label>
               <b> h4 </b><input type="radio" required name="tag" value="h4|h4">
            </label> --}}
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label>Страница</label>
            <select name="page" id='page' class="form-control select2 form-control-lg" required>
                @foreach(config('global.pages') as $k => $v)
                    <option value="{{ $v }}">@lang('lang.site_pages.'.$v)</option>
                @endforeach
            </select>
        </div>
    </div>
    {{-- <div class="col-md-12 pl-0 pr-0 float-left parent-div" style="border-bottom: solid 1px;"  id="meta-attr-1">
        <div class="col-md-4 float-left">
            <div class="form-group">
                <label>Мета атрибут</label>
                <input type="text" id="attribute" name="attribute_1" required class="form-control">
            </div>
            <a class="btn btn-danger"  id="first" style="display:none" data-id="" data-attr="1" onclick="remove(1)">Delete</a>
        </div>
        <div class="col-md-8 pl-0 pr-0 float-left">
            <div class="col-md-12">
                 <div class="form-group">
                    <label>Метаданные UZ</label>
                    <input type="text" id="data-uz" name="data_uz_1" required class="form-control">
                </div>
            </div>
             <div class="col-md-12">
                 <div class="form-group">
                    <label>Метаданные RU</label>
                    <input type="text" id="data-ru" name="data_ru_1" required class="form-control">
                </div>
            </div>
        </div>
    </div> --}}
@else
@php $i = 0; $par = strpos($parent->tag,'|'); @endphp

<input id="input-amount" type="hidden" name="input_amount" value="{{ count($model) }}">

@if(count($model) <= 0)
     @if($par)
        <div class="col-md-12 pl-0 pr-0 float-left parent-div" style="border-bottom: solid 1px;"  >
            <div class="col-md-12 float-left">
                <div class="col-md-12">
                     <div class="form-group">
                        <label>Текст UZ</label>
                        <input type="text" id="text-uz" name="text_uz" value="{{ $parent->text('uz') }}" required class="form-control">
                    </div>
                </div>
                 <div class="col-md-12">
                     <div class="form-group">
                        <label>Текст RU</label>
                        <input type="text" id="text-ru" name="text_ru" value="{{ $parent->text('ru') }}" required class="form-control">
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif
@foreach($model as $row)
    @php $i += 1; @endphp
    <input name="child_id_{{ $i }}" type="hidden" value="{{ $row->id }}">
    @if($par)
        <div class="col-md-12 pl-0 pr-0 float-left parent-div" style="border-bottom: solid 1px;"  >
            <div class="col-md-12 float-left">
                <div class="col-md-12">
                     <div class="form-group">
                        <label>Текст UZ</label>
                        <input type="text" id="text-uz" name="text_uz" value="{{ $parent->text('uz') }}" required class="form-control">
                    </div>
                </div>
                 <div class="col-md-12">
                     <div class="form-group">
                        <label>Текст RU</label>
                        <input type="text" id="text-ru" name="text_ru" value="{{ $parent->text('ru') }}" required class="form-control">
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="col-md-12 pl-0 pr-0 float-left parent-div" style="border-bottom: solid 1px;"  id="meta-attr-{{ $i }}">
        <div class="col-md-4 float-left">
            <div class="form-group">
                <label>Атрибут</label>
                <input type="text" id="attribute" name="attribute_{{ $i }}" required class="form-control" value="{{ $row->attribute }}" >
            </div>
            <a class="btn btn-danger"  id="first" style="display: inherit;" data-id="" data-attr="{{ $i }}" onclick="remove({{ $i}}, {{ $row->id }})">Delete</a>
        </div>
        <div class="col-md-8 pl-0 pr-0 float-left">
            <div class="col-md-12">
                 <div class="form-group">
                    <label>Данные UZ</label>
                    <input type="text" id="data-uz" name="data_uz_{{ $i }}" value="{{ $row->metaData('uz') }}" required class="form-control">
                </div>
            </div>
             <div class="col-md-12">
                 <div class="form-group">
                    <label>Данные RU</label>
                    <input type="text" id="data-ru" name="data_ru_{{ $i }}" value="{{ $row->metaData('ru') }}" required class="form-control">
                </div>
            </div>
        </div>
    </div>
@endforeach 
@endif