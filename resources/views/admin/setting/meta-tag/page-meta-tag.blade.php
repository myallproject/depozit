@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        
         <a href="#">
             Метаданные
         </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Мета</h5>
                                    <div class="card-header-right">
                                        
                                        <a id="add-new-btn" data-toggle="modal" data-target="#myModal" class="btn btn-info p-1 text-white pl-2 pr-3" href="">
                                            <i class="fa fa-plus-circle"> </i>
                                            Добавить
                                        </a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <table id="data-table" class="table-responsive-xl table-borderless table table-hover" style="font-size: 13px !important;">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Страница</th>
                                                <th>Метаданные</th>
                                                <th class="text-right">Управление</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($model as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td id="page_{{ $row->id }}">@lang('lang.site_pages.'.$row->page)</td>
                                                <td>
                                                  <b>UZ- </b>
                                                  <?php echo htmlentities('<').$row->tagName()['open'];?> 
                                                   @foreach($row->attribute() as $child)
                                                      <span> <span><b>{{ $child->attribute }}</b></span>="<span>{{ $child->metaData('uz') }}</span>"
                                                   @endforeach
                                                   @if(!$row->tagName()['close'])
                                                        <?php echo htmlentities('/>');?>
                                                   @else 
                                                        <?php echo htmlentities('>');?>{{ $row->text('uz') }}<?php echo htmlentities('</').$row->tagName()['close'].htmlentities('>');?>
                                                   @endif
                                                   <br>
                                                   <b>RU- </b>
                                                   <?php echo htmlentities('<').$row->tagName()['open'];?> 
                                                   @foreach($row->attribute() as $child)
                                                      <span> <span><b>{{ $child->attribute }}</b></span>="<span>{{ $child->metaData('ru') }}</span>"
                                                   @endforeach
                                                   @if(!$row->tagName()['close'])
                                                        <?php echo htmlentities('/>');?><br>
                                                   @else 
                                                        <?php echo htmlentities('>');?>{{ $row->text('ru') }}<?php echo htmlentities('</').$row->tagName()['close'].htmlentities('>');?>
                                                   @endif
                                                </td>
                                               
                                                <td class="text-right">
                                                    <a data-toggle="modal" data-target="#myModal" data-id="{{ $row->id }}" data-service-id="" class="update btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                    <a href="{{ route('meta-tag-delete',['id' => $row->id ]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog " style="max-width: 1200px">

            <!-- Modal content-->
            <form action="{{ route('meta-tag-edit') }}" method="POST">
                @csrf
                <input type="hidden" id="this_id" name="id" value="">

                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="modal-title" class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="form-modal">
                    
                    </div>

                    <div class="modal-footer" style="display: initial !important;">
                        <a class="btn btn-info float-left add-new-input" id="btn-add-attr" style="display: none">Добавить новый атрибут</a>

                        <button type="submit" class="btn btn-success" >
                            Сохранить
                        </button>
                        <a class="btn btn-warning float-right" data-dismiss="modal" >Закрыть</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });

            $('#menu-setting').addClass('active pcoded-trigger');

            $('#add-new-btn').click(function(){
                 $('#this_id').val('');
                 $('#btn-add-attr').hide();
                $.ajax({
                    url:"{{ route('meta-tag-inputs') }}",
                    data:{
                        'id': ''
                    },success:function(data){
                        $('#form-modal').html(data)
                    }
                });
                $('#modal-title').text("Добавить тег")
            });

            $('.update').click(function(){
                let id = $(this).attr('data-id');
                
                $('#this_id').val(id);
                $('#btn-add-attr').show();
                 $.ajax({
                        url:"{{ route('meta-tag-inputs') }}",
                        data:{
                            'id': id
                        },success:function(data){
                            $('#form-modal').html(data)
                        }
                    });

                $('#modal-title').text("Редактировать")
            });
            $('.delete').click(function(){
                id = $(this).attr('data-attr');
                $('#meta-attr-'+id).remove();
            });

            $('.add-new-input').click(function(){
                $('#input-amount').val(parseInt($('#input-amount').val())+1);
                

                $('#form-modal').append(
                    '<div class="col-md-12 pl-0 pr-0 parent-div float-left" style="border-bottom: solid 1px;" id="meta-attr-'+$("#input-amount").val()+'">'+
                        '<div class="col-md-4 float-left">'+
                            '<div class="form-group">'+
                                '<label>Атрибут</label>'+
                                '<input type="text"  name="attribute_'+$('#input-amount').val()+'" required class="form-control attribute">'+
                            '</div>'+
                            '<a class="btn btn-danger" style="display: inherit;" data-id=""  onclick="remove('+$('#input-amount').val()+')" data-attr="'+$('#input-amount').val()+'">Delete</a>'+
                        '</div>'+
                        '<div class="col-md-8 pl-0 pr-0 float-left">'+
                            '<div class="col-md-12">'+
                                 '<div class="form-group">'+
                                    '<label>Данные UZ</label>'+
                                    '<input type="text" name="data_uz_'+$('#input-amount').val()+'" required class="form-control data_uz">'+
                                '</div>'+
                            '</div>'+
                             '<div class="col-md-12">'+
                                 '<div class="form-group">'+
                                    '<label>Данные RU</label>'+
                                    '<input type="text" name="data_ru_'+$('#input-amount').val()+'" required class="form-control data_ru">'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'
                );
            });


          remove = function(id,del_id = false){
             $('#meta-attr-'+id).remove();
             $('#input-amount').val(parseInt($('#input-amount').val())-1);

             $('.parent-div').each(function(i,v){
                a_attr = $(this).find('a').attr('data-attr');
                $(this).find('a').attr('data-attr',(parseInt(a_attr)-1));
                if(a_attr > id){
                    $(this).attr('id','meta-attr-'+((parseInt(a_attr)-1))+'');
                    $(this).find('a').attr('onclick','remove('+(parseInt(a_attr)-1)+')');
                    $(this).find('.data_uz').attr('name','data_uz_'+(a_attr-1));
                    $(this).find('.data_ru').attr('name','data_ru_'+(a_attr-1));
                    $(this).find('.attribute').attr('name','attribute_'+(a_attr-1));
                }
             });

             if(del_id){
                $.ajax({
                    method: 'get',
                    url: "{{ route('meta-tag-delete-one')}}",
                    data:{
                        id: del_id
                    }
                })
             }
          };
          
        });

        $(function () {
            $('#data-table').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });

    </script>
@endsection