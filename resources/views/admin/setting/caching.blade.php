@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        
         <a href="#">
             Caching & Cron
         </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Caching & Cron</h5>
                                    <div class="card-header-right">
                                    </div>
                                </div>
                                <div class="card-block">
                                	<table class="table-responsive-xl table-bordered table table-hover">
                                		<thead>
                                			<tr>
                                				<th>Run</th>
                                				<th>Description</th>
                                			</tr>
                                		</thead>
	                                    <tbody>
	                                    	<tr>
	                                    		<td><a class="btn btn-success text-white" href="{{ route('artisan-command',['command' => 'cache:clear']) }}">Cache clear</a></td>
	                                    		<td>Sayt cache ni tozalash</td>
	                                    	</tr>
	                                    	<tr>
	                                    		<td><a class="btn btn-success text-white" href="{{ route('artisan-command',['command' => 'view:clear']) }}" >View clear</a></td>
	                                    		<td>View ni tozalash</td>
	                                    	</tr>
	                                    	<tr>
	                                    		<td><a class="btn btn-success text-white" href="{{ route('artisan-command',['command' => 'route:clear']) }}" >Route clear</a></td>
	                                    		<td>Route ni tozalash</td>
	                                    	</tr>
	                                    	<tr>
	                                    		<td><a class="btn btn-success text-white" href="{{ route('artisan-command',['command' => 'config:cache']) }}" >Config cache</a></td>
	                                    		<td>Configni tozalash & cache qilish</td>
	                                    	</tr>
	                                    	<tr>
	                                    		<td><a class="btn btn-success text-white" href="{{ route('artisan-command',['command' => 'rate:api']) }}" >Run cron Api</a></td>
	                                    		<td>API Cron ni qo'lda ishga tushirish (avtomatik bajariladigan kommandalarni qo'lda ishga tushirish)</td>
	                                    	</tr>
                                            <tr>
                                                <td><a class="btn btn-success text-white" href="{{ route('artisan-command',['command' => 'cron:rate']) }}" >Run cron CB</a></td>
                                                <td>Cb Cron ni qo'lda ishga tushirish (avtomatik bajariladigan kommandalarni qo'lda ishga tushirish)</td>
                                            </tr>
	                                    	<tr>
	                                    		<td><a class="btn btn-success text-white" href="{{ route('download-cron-result') }}">Cron statistika <i class="fa fa-download"></i></a></td>
	                                    		<td>Cron natijalashini yuklab olish (avtomatik bajariladigan kommandalar statistikasi)</td>
	                                    	</tr>
	                                    </tbody>
                                	</table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    

@endsection