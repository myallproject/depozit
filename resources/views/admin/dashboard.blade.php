@extends('admin.layouts.app')
@section('content')
<style type="text/css" media="screen">
    th,td {
        padding:2px !important;
    }    
</style>
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-body">
                <!-- [ page content ] start -->
                <div class="row">
                     <div class="col-lg-6 col-md-12">
                        <div class="card">
                            <div class="card-header bg-c-blue borderless">
                                <h5><i class="fa fa-telegram"></i> Telegram uchun valyuta kurslari jadvali</h5>
                               
                                <div class="card-header-right ">
                                  <a style="padding: 3px 8px 3px 8px" href="{{ route('best-rate-for-telegram',['locale'=>App::getLocale()])}}" target="_blank" class="btn btn-info text-white">Eng yaxshilar. <i class='fa text-white fa-table'></i>&nbsp;Jadval</a>
                                </div>
                            </div>
                            <div class="card-body table-responsive">
                                <table  class="table">
                                    @foreach($currencies as $currency )
                                    <tr>
                                        <td>{{ $currency->id }}</td>
                                        <td>{{ $currency->name }}</td>
                                        <td><a style="padding: 5px" href="{{ route('rate-for-telegram',['locale'=>App::getLocale(),'currency' => $currency->id])}}" target="_blank" class="btn btn-success text-white"><i class='fa fa-table'></i>&nbsp;Jadval</a></td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>

                         <div class="card">
                            <div class="card-block">
                                <div class="row align-items-center">
                                    
                                    <div class="col-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th><i class="fa fa-users "></i>&nbsp;Sayt foydalanuvchilari</th>
                                                    <th><i class="fa fa-user-circle-o"></i>&nbsp;Role</th>
                                                    <th><i class="fa fa-check"></i>&nbsp;Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($users as $user)
                                                <tr>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->role_id }}</td>
                                                    <td>
                                                        @if(Cache::has('user-is-online-'.$user->id))
                                                            <span class="text-success"><i class="fa fa-circle "></i>&nbsp;Online</span>
                                                        @else 
                                                            <span class="text-danger"><i class="fa fa-circle "></i>&nbsp;Offline</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer bg-c-green">
                                <div class="row align-items-center">
                                    <div class="col-9">
                                        <p class="text-white m-b-0"></p>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                    </div>
                      <div class="col-md-6 align-right">
                        <div class="card">
                            <div class="card-block">
                                <div class="row align-items-center">
                                    <div class="col-8">
                                       <a href="{{ route('cron-rate') }}" class="btn btn-danger cron-rate text-white"><i class="fa fa-tasks"></i>&nbsp;cron:rate</a>
                                    </div>
                                    <div class="col-4 text-right">
                                        <i class="fa fa-tasks f-28"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer bg-c-green">
                                <div class="row align-items-center">
                                    <div class="col-9">
                                        <p class="text-white m-b-0">php artisan cron:rate kommandasi orqali MB valyuta kurslarini o'zgartirish</p>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                         <div class="card">
                            <div class="card-block">
                                <div class="row align-items-center">
                                    <div class="col-12">
                                       <table class="table">
                                           <thead>
                                               <tr>
                                                   <th>Kuraslarni yangilash vaqti</th>
                                                   <th> <i class="fa fa-clock-o f-28"></i></th>
                                               </tr>
                                           </thead>
                                           <tbody>
                                            @foreach($banks as $bank)
                                               <tr>
                                                   <td>{{ $bank->name_uz}}</td>
                                                   <td>{{ $bank->rateUpdateTime() }}</td>
                                               </tr>
                                            @endforeach
                                           </tbody>
                                       </table>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="card-footer bg-c-green">
                                <div class="row align-items-center">
                                    <div class="col-9">
                                        <p class="text-white m-b-0">api orqali valyuta kurslarini o'zgartirish</p>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ page content ] end -->
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
  <script>
      $(document).ready(function() {
          $('.cron-rate').on('click', function() {
              return confirm("Do you really want to get started?");
          });
      });
  </script>
@endsection
