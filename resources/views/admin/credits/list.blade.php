@extends('admin.layouts.app')
@section('breadcrumb')
    <li class="breadcrumb-item">
        <a >@lang('lang.credits')</a>
    </li>
@endsection

@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <!-- [ page content ] start -->
                    <div class="">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">

                                    <h5>@lang('lang.credits')</h5>
                                    <div class="card-header-right">
                                        <a class="btn btn-info p-1 text-white pl-2 pr-3" href="{{ route('credit-add-form') }}">
                                            <i class="fa fa-plus-circle"> </i>
                                            @lang('lang.add_new')
                                        </a>
                                    </div>
                                </div>
                                 <div class="card-block font-size-14 pb-1">
                                     <a href="{{ route('credits-list') }}" style="margin-bottom: 8px" class="btn waves-effect waves-light all-status-btn hor-grd pl-2 p-1  pr-3 btn-grd-primary">@lang('lang.all')</a> &nbsp;&nbsp;&nbsp;
                                 </div>
                                <div class="card-block font-size-14 pt-0">
                                    @foreach($credit_types as $type)
                                    <a href="{{ route('credits-type-view',[$type->slug]) }}" class="btn waves-effect waves-light all-status-btn hor-grd pl-2 p-1  pr-3 btn-grd-success">{{ $type->name() }}</a> &nbsp;&nbsp;&nbsp;
                                    @endforeach
                                </div>

                                <form id="translate-form" action="" method="GET">
                                    @csrf
                                <div class="card-block font-size-14">
                                    <table id="credit-table" class="table-responsive-xl table-borderless table table-hover"  style="font-size: 12px !important;">
                                        <thead>
                                        <tr>
                                            <th>
                                                <input type="checkbox" class="translate-all" name="all_credit"/> @lang('lang.all')
                                            </th>
                                            <th>ID </th>
                                            <th>{{ Yt::trans('Nomi','uz') }}</th>
                                            <th>@lang('lang.bank')</th>
                                            <th>@lang('lang.type')</th>
                                            <th>@lang('lang.percent')</th>
                                            <th>@lang('lang.date')</th>
                                            <th>@lang('lang.status')</th>
                                            <th class="text-right">@lang('lang.control')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($credits as $row)
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" data-status="check" class="translate" name="credits[]" value="{{ $row->id }}" />
                                                    </td>
                                                    <td>{{ $row->id }}</td>
                                                    <td>{{ $row->name_uz}}</td>
                                                    <td>{{ $row->parent->name()}}</td>
                                                    <td>{{ $row->bank->name_uz }}</td>
                                                    <td>{{ $row->percent }} %</td>
                                                    <td>{{ $row->date_credit." ".$row->creditDate->name() }} </td>
                                                    <td>

                                                        <?php
                                                        if($row->status == 1){
                                                            $statusClass = 'alert-success';
                                                            $statusText = 'Active';
                                                        }elseif($row->status == 0){
                                                            $statusClass = 'alert-warning';
                                                            $statusText = 'Block';
                                                        }
                                                        ?>
                                                        <div id="alert-status-{{ $row->id }}" class="alert {{ $statusClass }} p-1 text-center" role="alert">
                                                            {{ $statusText }}
                                                        </div>
                                                    </td>
                                                    <td class="text-right">
                                                        <a
                                                            data-active-id='{{ $row->id }}'
                                                            data-status="{{ $row->status }}"
                                                            id="active-{{ $row->id }}"
                                                            class="active-dep btn @php if($row->status == 1){ echo 'btn-success';} if($row->status == 0){ echo 'btn-warning';} @endphp  p-1 pl-2 text-white "
                                                        ><i id="status-icon-{{ $row->id }}" class="fa @php if($row->status == 1){ echo 'fa-check';} if($row->status == 0){ echo 'fa-close';} @endphp "></i></a>

                                                        <a href="{{ route('credit-update-form',[$row->id]) }}" class="btn btn-primary p-1 pl-2 text-white"><i class="fa fa-pencil"></i></a>
                                                        <a href="{{ route('credit-delete',[$row->id]) }}" class="btn btn-danger delete p-1 pl-2 text-white"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="mt-4">
                                       {{-- <input type="radio" name="lang" value="uz"><label>UZ</label>
                                        &nbsp;--}}
                                       {{--  <input type="checkbox" required name="lang" value="ru"><label>RU</label>
                                        <button type="submit" class="btn btn-primary p-1">Translate</button> --}}
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- [ page content ] end -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script>
        $(document).ready(function () {
            $('.translate-all').on('click',function(){

                if($('.translate').attr('checked') == 'checked'){
                    $('.translate').attr('checked',false);
                } else {
                    $('.translate').attr('checked',true);
                }

            });

            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            $('#menu-credit-parent').addClass('active pcoded-trigger');
            $('#menu-credit-first').addClass('active');

            $('.active-dep').on('click',function(){
                let status = $(this).attr('data-status');
                let id = $(this).attr('data-active-id');
                if(status == 1){
                    $.ajax({
                        type:'GET',
                        url:'{{ route("credit-status") }}',
                        data:{
                            'id':id,
                            '_token': $('meta[name=csrf-token]').val(),
                            'status':0
                        },success:function(){
                            $('#active-'+id).removeClass('btn-success').addClass('btn-warning');
                            $('#active-'+id).attr('data-status',0);
                            $('#status-icon-'+id).removeClass('fa-check').addClass('fa-close');
                            $('#alert-status-'+id).removeClass('alert-success').addClass('alert-warning').text('Block');
                        },error:function(){
                            alert('error')
                        }
                    });

                } else if (status == 0){
                    $.ajax({
                        type:'GET',
                        url:'{{ route("credit-status") }}',
                        data:{
                            'id':id,
                            '_token': $('meta[name=csrf-token]').val(),
                            'status':1
                        },success:function(){
                            $('#active-'+id).removeClass('btn-warning').addClass('btn-success');
                            $('#active-'+id).attr('data-status',1);
                            $('#status-icon-'+id).removeClass('fa-close').addClass('fa-check');
                            $('#alert-status-'+id).removeClass('alert-warning').addClass('alert-success').text('Active');
                        },error:function(){
                            alert('error')
                        }
                    });
                }
            });

        });

        $(function () {
            $('#credit-table').DataTable({
                'pageLength'  : 50,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true,
                "dom"         : '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
            })
        });

    </script>
    <style>
        .float-right{
            float:right !important;
        }
    </style>
@endsection
