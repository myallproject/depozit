@extends('admin.layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a>
            @lang('lang.credits')
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="">
            {!! !empty($credit) ? trans('lang.edit') : trans('lang.add_new') !!}
        </a>
    </li>
@endsection
@section('content')
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card" style="background-color: #ecf0f5 !important;">
                                <div class="card-header">
                                    <h5>{!! !empty($credit) ? trans('lang.edit') : trans('lang.add_new') !!}</h5>
                                    @if($credit)
                                        <div class="card-header-right">
                                            <a href="{{ route('credit-add-form') }}" class="btn btn-info p-1 text-white pl-2 pr-3" href=""><i class="fa fa-plus-circle"> </i> @lang('lang.add_new')</a>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-12"  style="font-size: 13px !important;">
                                    @if($credit)
                                        @include('admin.credits.form.update')
                                    @else
                                        @include('admin.credits.form.add')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('.delete').on('click', function() {
                return confirm("@lang('lang.confirmDel')");
            });
            
            $('#menu-credit-parent').addClass('active pcoded-trigger');
            $('#menu-credit-first').addClass('active');

           

            $('#credit-date-type').on('change',function(){
                selectCreditChildren();
            });
            $('#credit-compensation-select').on('change',function(){
                selectCreditCompensationChildren();
            });
            
            function selectCreditCompensationChildren(){
                $.ajax({
                    type: 'GET',
                    url: "{{ route('credit-child-compensation-form') }}",
                    data:{
                        'credit_id' : $('#credit_id').val(),
                        'compensation_ids' : $('#credit-compensation-select').val(),
                        'cr_type_id': $('#credit-type-id').val(),
                        'date_id': $('#credit-date-type').val(),
                    }
                }).done(function (data){
                    $('#credit-children-compensation-form').html(data)
                });
            }
            function selectCreditChildren(){
                $.ajax({
                    type: 'GET',
                    url: "{{ route('credit-child-form') }}",
                    data:{
                        'credit_id' : $('#credit_id').val(),
                        'date_ids' : $('#credit-date-type').val(),
                        'cr_type_id': $('#credit-type-id').val(),
                    }
                }).done(function (data){
                    $('#credit-children-date-form').html(data)
                });
            }

            $('.type-credit-class').on('change',function(){
                $.ajax({
                    type:'GET',
                    url: "{{ route('credit-review-period-response-list') }}",
                    data:{
                        'id':$(this).val(),
                        'review_id':$('#credit-review').attr('data-id'),
                    },success:function(data){
                        $('#credit-review').html(data)
                    }
                });

                $.ajax({
                    type:'GET',
                    url: "{{ route('credit-issuing-form-response-list') }}",
                    data:{
                        'id':$(this).val(),
                        'review_id':$('#credit-issuing-form').attr('data-id'),
                    },success:function(data){
                        $('#credit-issuing-form').html(data)
                    }
                });
                $.ajax({
                    type:'GET',
                    url: "{{ route('credit-goal-credit-response-list') }}",
                    data:{
                        'id':$(this).val(),
                        'credit_id': $('#credit_id').val(),
                        'review_id':$('#goal-credit-select').attr('data-id'),
                    },success:function(data){
                        $('#goal-credit-select').html(data)
                    }
                });
                $.ajax({
                    type:'GET',
                    url: "{{ route('credit-compensation-select-list') }}",
                    data:{
                        'id':$(this).val(),
                        'review_id':$('#credit-compensation-select').attr('data-id'),
                    },success:function(data){
                        $('#credit-compensation-select').html(data)
                    }
                });
                $.ajax({
                    type:'GET',
                    url: "{{ route('credit-date-type-select-list') }}",
                    data:{
                        'id':$(this).val(),
                        'credit_id': $('#credit_id').val(),
                        'review_id':$('#credit-date-type').attr('data-id'),
                    },success:function(data){
                        $('#credit-date-type').html(data);
                    }
                });

                $.ajax({
                    type:'GET',
                    url: "{{ route('credit-provision-type-select-list') }}",
                    data:{
                        'id':$(this).val(),
                        'review_id':$('#credit-provision').attr('data-id'),
                    },success:function(data){
                        $('#credit-provision').html(data)
                    }
                });

                cat_data = {
                    'id':$(to_cr_class).val(),
                    'review_id':$('#credit-borrower-category').attr('data-id'),
                }
                sendAjax(cat_data, "{{ route('credit-borrow-category-select-list') }}",  $('#credit-borrower-category'));
            });
            
           let to_cr_class =  $('.type-credit-class');
            $.ajax({
                type:'GET',
                url: "{{ route('credit-review-period-response-list') }}",
                data:{
                    'id':$(to_cr_class).val(),
                    'review_id':$('#credit-review').attr('data-id'),
                },success:function(data){
                    $('#credit-review').html(data)
                }
            });

            $.ajax({
                type:'GET',
                url: "{{ route('credit-issuing-form-response-list') }}",
                data:{
                    'id':$(to_cr_class).val(),
                    'review_id':$('#credit-issuing-form').attr('data-id'),
                },success:function(data){
                    $('#credit-issuing-form').html(data)
                }
            });
            
            $.ajax({
                type:'GET',
                url: "{{ route('credit-goal-credit-response-list') }}",
                data:{
                    'id':$(to_cr_class).val(),
                    'credit_id': $('#credit_id').val(),
                    'review_id':$('#goal-credit-select').attr('data-id'),
                },success:function(data){
                    $('#goal-credit-select').html(data)
                }
            });

            $.ajax({
                type:'GET',
                url: "{{ route('credit-date-type-select-list') }}",
                data:{
                    'id':$(to_cr_class).val(),
                    'credit_id':$('#credit_id').val(),
                    'review_id':$('#credit-date-type').attr('data-id'),
                },success:function(data){
                    $('#credit-date-type').html(data);
                    selectCreditChildren();
                }
            });
            
            $.ajax({
                type:'GET',
                url: "{{ route('credit-compensation-select-list') }}",
                data:{
                    'id':$(to_cr_class).val(),
                    'credit_id':$('#credit_id').val(),
                    'review_id':$('#credit-compensation-select').attr('data-id'),
                },success:function(data){
                    $('#credit-compensation-select').html(data);
                    selectCreditCompensationChildren();
                }
            });


            $.ajax({
                type:'GET',
                url: "{{ route('credit-provision-type-select-list') }}",
                data:{
                    'id':$(to_cr_class).val(),
                    'review_id':$('#credit-provision').attr('data-id'),
                },success:function(data){
                    $('#credit-provision').html(data)
                }
            });

           
            

            cat_data = {
                'id':$(to_cr_class).val(),
                'review_id':$('#credit-borrower-category').attr('data-id'),
            }

            sendAjax(cat_data, "{{ route('credit-borrow-category-select-list') }}",  $('#credit-borrower-category'));

            function sendAjax(dataAttr, url, content){
                $.ajax({
                    type:'GET',
                    url: url,
                    data: dataAttr
                }).done(function(data){
                    $(content).html(data)
                })
            }
        });
    </script>
@endsection
