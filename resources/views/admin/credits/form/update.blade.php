<div class="row">
    <div class="col-sm-12 pt-4 pl-2">
        <form action="{{ route('credit-update') }}" method="POST" enctype="multipart/form-data">
            @csrf
        <input id="credit_id" type="hidden" name="id" value="{{ $credit->id }}">
            <div class="form-group row">
                <div class="col-sm-6 pl-0 pr-0">
                    <div class="col-sm-12 mb-3">
                        <label> <b>@lang('lang.bank')</b></label>
                        <select required="required" type="text" name="bank" class="form-control form-control-lg">
                            <option value="">- @lang('lang.choose') -</option>
                            @foreach($banks as $bank)
                                <option @if($bank->id == $credit->bank_id) selected="selected" @endif value="{{ $bank->id }}"> {{ $bank->name()}}</option>
                            @endforeach
                        </select>
                    </div>
                
                    <div class="col-sm-12 mb-3">
                        <label> <b>Bank turi</b></label>
                        <select required="required" type="text" name="type_bank" class="form-control form-control-lg">
                            <option value="">- @lang('lang.choose') -</option>
                            @foreach($banks_type as $type_b)
                                <option @if($credit->bank->information->type_bank == $type_b->id) selected @endif value="{{ $type_b->id }}"> {{ $type_b->name() }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 border" style="border-color: #4081c2 !important">
                    <strong >Kredit sozlamasi</strong>
                    <div class="form-group row">
                        <div class="col-md-4">      
                            <label for="status">Status</label>
                            <select id="status" name="status" class="form-control">
                                @foreach(config('global.credits.status') as $ks=>$vs)
                                <option @if($credit->status == $ks) selected @endif value="{{ $ks }}">{{ $vs }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="activity">Faolligi</label>
                            <select name="status_suspended" id="activity" class="form-control">
                                @foreach(config('global.credits.activity') as $ka=>$va)
                                <option @if($credit->status_suspended == $ka) selected @endif value="{{ $ka }}">@lang('lang.'.$va)</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.name') UZ</b></label>
                    <input required="required" type="text" name="name_uz" value="{{ $credit->name_uz }}" class="form-control form-control-lg">
                </div>
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.name') RU</b></label>
                    <input required="required" type="text" name="name_ru" value="{{ $credit->name_ru }}" class="form-control form-control-lg">
                </div>

            </div>
            <div class="form-group row">
                <div class="col-sm-3 mb-3">
                    <label> <b>@lang('lang.credit_types')</b></label>
                    <select required="required" type="text" name="type_credit" id="credit-type-id" class="form-control type-credit-class form-control-lg">
                        <option value="">- @lang('lang.choose') -</option>
                        @foreach($credit_types as $cr_type)
                            <option @if($cr_type->id == $credit->type_credit_id) selected="selected" @endif value="{{ $cr_type->id }}"> {{ $cr_type->name() }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-sm-3 mb-3">
                    <label> <b>@lang('lang.currency')</b></label>
                    <select required="required" type="text" name="currency" class="form-control form-control-lg">
                        @foreach($currency as $cur)
                            <option @if($cur->id == $credit->currency) selected="selected" @endif value="{{ $cur->id }}"> {{ $cur->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>Kredit muddati(saytda.ko'rsatilgan)</b></label>
                    <input required="required" type="text" name="credit_data_string" class="form-control form-control-lg" value="{{ $credit->credit_data_string }}">
                </div>

                <div class="col-sm-3 mb-3">
                    <label> <b>@lang('lang.credit_date_type')</b></label>
                    <select required="required" type="text" name="type_date" class="form-control form-control-lg">
                        <option value="">- @lang('lang.choose') -</option>
                        @foreach($date_type as $d_type)
                            <option @if($d_type->id == $credit->type_date ) selected="selected" @endif value="{{ $d_type->id }}"> {{ $d_type->name() }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-3 mb-3">
                    <label> <b>@lang('lang.credit_date') (raqamlarda)</b></label>
                    <input required="required" type="text" name="date" value="{{ $credit->date_credit }}" class="form-control form-control-lg">
                </div>

            </div>

            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.credit_date_type')(filter uchun)</b></label>
                   <select required="required" type="text" name="type_date_filter[]" id="credit-date-type" data-id="" multiple class="form-control select2 form-control-lg">

                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.first_compensation')</b></label>
                    <select type="text" name="compensation_status[]" multiple class="form-control select2 form-control-lg" id="credit-compensation-select" data-id="{{ $credit->compensation_status }}">
                        <option value="">- @lang('lang.choose') -</option>
                    </select>
                </div>
                </div>
            <div class="" id="credit-children-date-form"></div>
            <div class="" id="credit-children-compensation-form"></div>


            <div class="form-group row">
                <div class="col-sm-6 mb-3">
                    <label> <b>@lang('lang.amount') (Raqamlarda)</b></label>
                    <input type="text" name="amount" value="{{ $credit->amount }}" class="form-control form-control-lg">
                </div>
            </div>
                <div class="form-group row">
                    <div class="col-sm-12 mb-3">
                        <label> <b>@lang('lang.text_amount') uz (Matn shaklida)</b></label>
                        <input type="text" name="text_amount_uz" value="{{ $credit->text_amount_uz }} " class="form-control form-control-lg">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 mb-3">
                        <label> <b>@lang('lang.text_amount') ru (Matn shaklida)</b></label>
                        <input type="text" name="text_amount_ru" value="{{ $credit->text_amount_ru }} " class="form-control form-control-lg">
                    </div>
                </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.percent')</b></label>
                    <input required="required" type="text" name="percent" value="{{ $credit->percent }}" class="form-control form-control-lg">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Foizlar jadvali</b></label><br>
                    <span style="color:darkred">*Kiritish usuli: vaqti; summasi; foiz; qo'shimcha shartlari;... | vaqti; summasi; foiz; qo'shimcha shartlari;...)</span>
                    <textarea required rows="6" name="percent_table" class="form-control form-control-lg">{{ $credit->percent_table }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.date_privilege') uz (Maydon faqat mavjud bulsa to'ldirilishi kerak)</b></label>
                    <input  type="text" name="date_privilege_uz" value="{{ $credit->date_privilege_uz }}" class="form-control form-control-lg">
                </div>

                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.date_privilege') ru (Поля должны быть заполнены, только если они доступны)</b></label>
                    <input  type="text" name="date_privilege_ru" value="{{ $credit->date_privilege_ru }}" class="form-control form-control-lg">
                </div>

            </div>            
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.first_compensation') uz (matn shaklida)</b></label>
                    <input  type="text" name="first_compensation_uz" value="{{ $credit->first_compensation_uz }}" class="form-control form-control-lg">
                </div>
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.first_compensation') ru (в виде текста)</b></label>
                    <input  type="text" name="first_compensation_ru" value="{{ $credit->first_compensation_ru }}" class="form-control form-control-lg">
                </div>
            </div>
            <div class="form-group row">

                <div class="col-sm-12 mb-3">
                    <label> <b>Ko'rib chiqish muddati</b></label>
                    <select required="required" type="text" name="review_period" id="credit-review" data-id="{{ $credit->credit_review_period_id }}" class="form-control form-control-lg">

                    </select>
                </div>

            </div>

            <div class="form-group row">

                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.type_provision')</b></label>
                    <select class="select2" required="required" type="text" name="provision[]" multiple id="credit-provision" data-id="{{ $credit->provision_id }}"  class="form-control form-control-lg">

                    </select>
                </div>

                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.type_provision') uz (matn shaklida)</b></label>
                    <input  type="text" name="type_provision_uz" value="{{ $credit->type_provision_uz }}" class="form-control form-control-lg">
                </div>
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.type_provision') ru (в виде текста)</b></label>
                    <input  type="text" name="type_provision_ru" value="{{ $credit->type_provision_ru }}" class="form-control form-control-lg">
                </div>

            </div>

            <div class="form-group row">

                <div class="col-sm-12 mb-3">
                    <label> <b>Garov ta'minoti</b></label>
                    <input type="text" name="garov_taminoti" class="form-control form-control-lg" value="{{ $credit->garov }}">
                </div>

            </div>
            <div class="form-group row">

                <div class="col-sm-12 mb-3">
                    <label> <b>Kafillik ta'minoti</b></label>
                    <input type="text" name="kafillik" class="form-control form-control-lg" value="{{ $credit->kafillik }}">
                </div>

            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Sug'urta polisi</b></label>
                    <input type="text" name="sugurta_polisi" class="form-control form-control-lg" value="{{ $credit->sugurta_polisi }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Kredit rasmiylashtirish yo'li UZ</b></label>
                    <input type="text" name="rasmiylashtirish_uz" class="form-control form-control-lg" value="{{ $credit->legitimation('uz') }}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Kredit rasmiylashtirish yo'li RU</b></label>
                    <input type="text" name="rasmiylashtirish_ru" class="form-control form-control-lg" value="{{ $credit->legitimation('ru') }}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Kredit ajratish tartibi</b></label>
                    <select name="credit_issuing_form" id="credit-issuing-form"  data-id="{{ $credit->credit_issuing_form_id }}"  class="form-control form-control-lg">

                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Kredit ajratish tartibi(matn)</b></label>
                    <input type="text" name="ajratish_tartibi" class="form-control form-control-lg" value="{{ $credit->ajratish_tartibi }}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Kredit so'ndirish tartibi</b></label>
                    <input type="text" name="sundirish_tartibi" class="form-control form-control-lg" value="{{ $credit->sundirish_tartibi }}">
                </div>
            </div>
            {{-- //talablar --}}
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Kredit oluvchinig yoshi(matn) UZ</b></label>
                    <input type="text" name="credit_borrower_age_uz" class="form-control form-control-lg" value="{{ $credit->credit_borrower_age }}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Kredit oluvchinig yoshi(matn) RU</b></label>
                    <input type="text" name="credit_borrower_age_ru" class="form-control form-control-lg" value="{{ $credit->credit_borrower_age_ru }}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.borrower_cat') </b></label>
                    <select name="credit_borrower_category" id="credit-borrower-category" required data-id="{{ $credit->credit_borrower_category_id }}"  class="form-control form-control-lg">
                        <option>@lang('lang.choose')</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Kreditga layoqatlilik bo'yicha talablar(matn) UZ</b></label>
                    <textarea rows="6" name="credit_borrower_possible_uz" class="form-control form-control-lg">{{ $credit->credit_borrower_possible_uz }}</textarea>
                </div>
                <div class="col-sm-12 mb-3">
                    <label> <b>Kreditga layoqatlilik bo'yicha talablar(matn) RU</b></label>
                    <textarea rows="6" name="credit_borrower_possible_ru" class="form-control form-control-lg">{{ $credit->credit_borrower_possible_ru }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Kredit oluvchiga boshqa talablar(matn) UZ</b></label>
                    <textarea rows="6" name="credit_borrower_additional_uz" class="form-control form-control-lg">{{ $credit->credit_borrower_additional_uz }}</textarea>
                </div>
                <div class="col-sm-12 mb-3">
                    <label> <b>Kredit oluvchiga boshqa talablar(matn) RU</b></label>
                    <textarea rows="6" name="credit_borrower_additional_ru" class="form-control form-control-lg">{{ $credit->credit_borrower_additional_ru }}</textarea>
                </div>
            </div>
            {{-- //talablar --}}
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Kredit maqsadi</b></label>
                    <select name="credit_goal_select[]"  multiple id="goal-credit-select"  data-id=""  class="form-control form-control-lg select2">

                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>Faqat ayrim toifadagi marka uchun (Faqat Avtokreditlar uchun)</b></label>
                    <select name="car_brand" class="form-control form-control-lg">
                        <option value="0">Mavjud emas</option>
                        @foreach($car_brands as $car_brand)
                            <option value="{{ $car_brand->id }}" @if($car_brand_id == $car_brand->id) selected @endif> {{ $car_brand->name() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.goal_credit')(matn) UZ</b></label>
                    <textarea rows="6" name="goal_credit_uz"  class="form-control form-control-lg">{{ $credit->goal_credit_uz }}</textarea>
                </div>
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.goal_credit')(matn) RU</b></label>
                    <textarea rows="6" name="goal_credit_ru"  class="form-control form-control-lg">{{ $credit->goal_credit_ru }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.documents') UZ</b></label>
                    <textarea rows="6" name="documents_uz" class="form-control form-control-lg">{{ $credit->documents_uz }}</textarea>
                </div>

                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.documents') RU</b></label>
                    <textarea rows="6" name="documents_ru" class="form-control form-control-lg">{{ $credit->documents_ru }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.addition_rules') UZ</b></label>
                    <textarea rows="6" name="addition_rules_uz" class="form-control form-control-lg">{{ $credit->addition_rules_uz }}</textarea>
                </div>
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.addition_rules') RU</b></label>
                    <textarea rows="6" name="addition_rules_ru" class="form-control form-control-lg">{{ $credit->addition_rules_ru }}</textarea>
                </div>
            </div>
            <div class="form-group row" style="color:red;">
                <div class="col-md-3">
                    <div class=" col-sm-12 mb-3">
                        <label for="aktsiya"> <b>Aktsiya</b></label>
                        <input name="aktsiya" value="1" type="checkbox" @if($credit->aktsiya) checked @endif>
                     </div>
                </div>
                
                 <div class="col-md-9">
                    <div class="col-md-12">
                        <label><b>Izoh UZ</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="aktsiya_izoh_uz">{{ $credit->aktsiya_izoh('uz') }}</textarea>
                    </div>
                     <div class="col-md-12">
                        <label><b>Izoh RU</b></label>
                        <textarea class="form-control form-control-lg" rows="3" name="aktsiya_izoh_ru">{{ $credit->aktsiya_izoh('ru')}}</textarea>
                    </div>
                    <div class="col-md-12">
                        <label><b>Link UZ</b></label>
                        <input type="text" class="form-control form-control-lg" name="aktsiya_link_uz" value="{{ $credit->aktsiya_link('uz') }}"/>
                    </div>
                     <div class="col-md-12">
                        <label><b>Link RU</b></label>
                        <input type="text" class="form-control form-control-lg" name="aktsiya_link_ru" value="{{ $credit->aktsiya_link('ru') }}"/>
                    </div>  
                 </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.link') uz</b></label>
                    <input required type="text" name="link_uz" class="form-control form-control-lg" value="{{ $credit->link('uz') }}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.link') ru</b></label>
                    <input required type="text" name="link_ru" class="form-control form-control-lg" value="{{ $credit->link('ru') }}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-info ">
                        @lang('lang.save')
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>