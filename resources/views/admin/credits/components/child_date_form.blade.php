@if($isDate)
	@if($dates)
		@foreach($dates as $date)
			<div class="col-md-12 row mb-3" style="font-size: 12px !important">
				<div class="col-sm-1">
					<label><b>@lang('lang.date')</b></label>
					<input disabled value="{{ $date->name() }}"  class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">	
				</div>
				<div class="col-sm-1 ">
					<label><b>@lang('lang.percent')</b></label>
					<input required="required" type="text" name="percent_{{ $date->id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
				</div> 

				<div class="col-sm-1">
					<label> <b>@lang('lang.type')</b></label>
					<select required name="date_type_{{ $date->id }}" class="form-control" style="font-size: 12px !important; padding: 2px 4px;">
						@foreach($date_type_id as $type)
							<option value="{{ $type->id }}">{{ $type->name() }}</option>
						@endforeach
					</select>
				</div>

				<div class="col-sm-1 ">
					<label> <b>@lang('lang.date')</b></label>
					<input required  type="text" name="date_{{ $date->id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;"/>
				</div>

				<div class="col-sm-3 ">
					<label> <b>@lang('lang.date') (Text UZ)</b></label>
					<input required="required" type="text" name="date_name_uz_{{ $date->id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
				</div>
				<div class="col-sm-2 ">
					<label> <b>@lang('lang.date') (Text RU)</b></label>
					<input required="required" type="text" name="date_name_ru_{{ $date->id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
				</div>
				<div class="col-sm-2">
					<label> <b>@lang('lang.first_compensation')</b></label>
					<select name="first_compensation_{{ $date->id }}" class="form-control" style="font-size: 12px !important; padding: 2px 4px;">
						@foreach($compensation as $row_com)
							<option value="{{ $row_com->id }}">{{ $row_com->name() }}</option>
						@endforeach
					</select>
				</div>
			</div>
		@endforeach
	@endif

	@if($model)		
	
		@foreach($model as $row)
			<div class="col-md-12 row mb-3" style="font-size: 12px !important">
				<div class="col-sm-1 mb-3">
					<label>Delete</label>
					<a data-del-id="{{ $row->id }}" style="padding: 5px 15px;" class="btn btn-danger child-delete text-white"><i class="fa fa-trash"></i></a>
				</div>
				<div class="col-sm-1">
					<label><b>@lang('lang.date')</b></label>
					<input disabled value="{{ $row->dateId->name() }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">	
				</div>
				<div class="col-sm-1 ">
					<label><b>@lang('lang.percent')</b></label>
					<input required="required" type="text" value="{{ $row->percent }}" name="percent_{{ $row->dateId->id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
				</div> 

				<div class="col-sm-1">
					<label> <b>@lang('lang.type')</b></label>
					<select required name="date_type_{{ $row->dateId->id }}" class="form-control" style="font-size: 12px !important; padding: 2px 4px;">
						@foreach($date_type_id as $type)
							<option @if($type->id == $row->date_type) selected="selected"  @endif value="{{ $type->id }}">{{ $type->name() }}</option>
						@endforeach
					</select>
				</div>

				<div class="col-sm-1 ">
					<label> <b>@lang('lang.date')</b></label>
					<input required  type="text" value="{{ $row->date }}" name="date_{{ $row->dateId->id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;"/>
				</div>

				<div class="col-sm-3">
					<label> <b>@lang('lang.date') (Text UZ)</b></label>
					<input required="required" type="text" value="{{ $row->date_name_uz }}" name="date_name_uz_{{ $row->dateId->id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
				</div>
				<div class="col-sm-2 ">
					<label> <b>@lang('lang.date') (Text RU)</b></label>
					<input required="required" type="text" value="{{ $row->date_name_ru }}" name="date_name_ru_{{ $row->dateId->id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
				</div>

				<div class="col-sm-2">
					<label> <b>@lang('lang.first_compensation')</b></label>
					<select  name="first_compensation_{{ $row->dateId->id }}" class="form-control" style="font-size: 12px !important; padding: 2px 4px;">
						@foreach($compensation as $row_com)
							<option @if($row_com->id == $row->compensation_id) selected="selected"  @endif value="{{ $row_com->id }}">{{ $row_com->name() }}</option>
						@endforeach
					</select>
				</div>
			</div>
		@endforeach	
	@endif
	<script>
		$('.child-delete').click(function(){
			$.ajax({
				type:'GET',
				url: "{{ route('credit-child-form') }}",
				data:{
					'credit_id' : $('#credit_id').val(),
					'date_ids' : $('#credit-date-type').val(),
					'delete_id': $(this).attr('data-del-id')
				}
			}) .done(function (data){
					$('#credit-children-date-form').html(data)
			});
		});	
	</script>
@else 
	@if($compensations)
	@foreach($compensations as $compensation)
		<div class="col-md-12 row mb-3" style="font-size: 12px !important">
			<div class="col-sm-1">
				<label><b>@lang('lang.date')</b></label>
				<input disabled value="{{ $date->name() }}"  class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">	
			</div>
			<div class="col-sm-1 ">
				<label><b>@lang('lang.percent')</b></label>
				<input required="required" type="text" name="percent_{{ $compensation->id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
			</div> 

			<div class="col-sm-1">
				<label> <b>@lang('lang.type')</b></label>
				<select required name="date_type_{{ $compensation->id }}" class="form-control" style="font-size: 12px !important; padding: 2px 4px;">
					@foreach($date_type_id as $type)
						<option value="{{ $type->id }}">{{ $type->name() }}</option>
					@endforeach
				</select>
			</div>

			<div class="col-sm-1 ">
				<label> <b>@lang('lang.date')</b></label>
				<input required  type="text" name="date_{{ $compensation->id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;"/>
			</div>

			<div class="col-sm-3 ">
				<label> <b>@lang('lang.date') (Text UZ)</b></label>
				<input required="required" type="text" name="date_name_uz_{{ $compensation->id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
			</div>
			<div class="col-sm-2 ">
				<label> <b>@lang('lang.date') (Text RU)</b></label>
				<input required="required" type="text" name="date_name_ru_{{ $compensation->id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
			</div>
			<div class="col-sm-2">
				<label> <b>@lang('lang.first_compensation')</b></label>				
				<input disabled value="{{ $compensation->name() }}"  class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">	
			</div>
		</div>
	@endforeach
	@endif

	@if($model)
	@foreach($model as $row)		
		<div class="col-md-12 row mb-3" style="font-size: 12px !important">
			<div class="col-sm-1 mb-3">
				<label>Delete</label>
				<a data-delcomp-id="{{ $row->id }}" style="padding: 5px 15px;" class="btn btn-danger childcomp-delete text-white"><i class="fa fa-trash"></i></a>
			</div>
			<div class="col-sm-1">
				<label><b>@lang('lang.date')</b></label>
				<input disabled value="{{ $row->dateId->name() }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">	
			</div>
			<div class="col-sm-1 ">
				<label><b>@lang('lang.percent')</b></label>
				<input required="required" type="text" value="{{ $row->percent }}" name="percent_{{ $row->compensation_id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
			</div> 

			<div class="col-sm-1">
				<label> <b>@lang('lang.type')</b></label>
				<select required name="date_type_{{ $row->compensation_id }}" class="form-control" style="font-size: 12px !important; padding: 2px 4px;">
					@foreach($date_type_id as $type)
						<option @if($type->id == $row->date_type) selected="selected"  @endif value="{{ $type->id }}">{{ $type->name() }}</option>
					@endforeach
				</select>
			</div>

			<div class="col-sm-1 ">
				<label> <b>@lang('lang.date')</b></label>
				<input required  type="text" value="{{ $row->date }}" name="date_{{ $row->compensation_id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;"/>
			</div>

			<div class="col-sm-3">
				<label> <b>@lang('lang.date') (Text UZ)</b></label>
				<input required="required" type="text" value="{{ $row->date_name_uz }}" name="date_name_uz_{{ $row->compensation_id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
			</div>
			<div class="col-sm-2 ">
				<label> <b>@lang('lang.date') (Text RU)</b></label>
				<input required="required" type="text" value="{{ $row->date_name_ru }}" name="date_name_ru_{{ $row->compensation_id }}" class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
			</div>

			<div class="col-sm-2">
				<label> <b>@lang('lang.first_compensation')</b></label>
				<input disabled value="{{ $row->compensation->name() }}"  class="form-control form-control-lg" style="font-size: 12px !important; padding: 8px 2px;">
			</div>
		</div>
	@endforeach	
	@endif
	<script>
		$('.childcomp-delete').click(function(){
			$.ajax({
				type:'GET',
				url: "{{ route('credit-child-compensation-form') }}",
				data:{
					'credit_id' : $('#credit_id').val(),
					'compensation_ids' : $('#credit-compensation-select').val(),
					'date_id': $('#credit-date-type').val(),
					'delete_id': $(this).attr('data-delcomp-id')
				}
			}) .done(function (data){
					$('#credit-children-compensation-form').html(data)
			});
		});	
	</script>
@endif