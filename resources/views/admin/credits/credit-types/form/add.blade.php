<div class="row ml-5">
    <div class="col-sm-8 pt-4 ">
        <form action="{{ route('credit-type-add') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.name')</b></label>
                    <input type="text" name="name" class="form-control form-control-lg" required="required">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <label> <b>@lang('lang.text')</b></label>
                    <textarea type="text" name="text" class="form-control form-control-lg"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12 mb-3">
                    <button type="submit" class="btn btn-info ">
                        @lang('lang.save')
                    </button>
                </div>
            </div>
        </form>
    </div>

</div>

