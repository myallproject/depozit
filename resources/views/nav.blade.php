<nav class="main-menu">
    <div class="navbar-header">
        <!-- Toggle Button -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>

    <div class="navbar-collapse collapse clearfix">
        <ul class="navigation clearfix">

            <li class="menu-home"><a  href="{{ route('home') }}">{{ Yt::trans('Bosh sahifa','uz') }}</a></li>

            <li class="menu-deposit"><a  href="{{ route('deposits') }}"> {{ Yt::trans('Omonatlar','uz') }}</a></li>

            <li  class="dropdown menu-credit"><a href="#">{{ Yt::trans('Kreditlar','uz') }}</a>
                <ul>

                    <li class="menu-consume">
                        <a href="{{ route('credit-consumer') }}">@lang('lang.eat_credits')</a>
                    </li>
                    <li class="menu-consume">
                        <a href="{{ route('credit-auto') }}">@lang('lang.auto_credit')</a>
                    </li>
                    <li class="menu-consume">
                        <a href="{{ route('credit-education') }}">@lang('lang.education_credit')</a>
                    </li>
                    <li class="menu-consume">
                        <a href="{{ route('credit-microcredit') }}">@lang('lang.micro_debt')</a>
                    </li>
                    <li class="menu-consume">
                        <a href="{{ route('credit-mortgage') }}">@lang('lang.credit_mortgage')</a>
                    </li>

                </ul>
            </li>
        {{-- <li class="dropdown"><a href="#">Kartalar</a>
                 <ul>
                     <li><a href="#">Debit kartalar</a></li>
                     <li><a href="#">Kredit kartalar</a></li>
                 </ul>
             </li>

             <li><a href="">Ipoteka</a></li>

            <li><a href="">Investitsiya</a></li>

            <li class="dropdown"><a href="#">Sug'urta</a>
                <ul>
                    <li><a href="#">Страхование туристов</a></li>
                    <li><a href="#">ОСАГО и КАСКО</a></li>
                </ul>
            </li>

            <li><a href="">Maxsus taklif</a></li>--}}


            <li  class="dropdown menu-currency"><a style="text-transform: initial !important;"  href="#">@lang('lang.currency_exchange_rate')</a>
                <ul>
                    <li class="menu-consume">
                        <a href="{{ route('exchange-rate') }}">{{ Yt::trans('Markaziy Bank','uz') }}</a>
                    </li>
                    <li class="menu-consume">
                        <a href="{{ route('on-bank-exchange-rate') }}">{{ Yt::trans('Boshqa banklar','uz') }}</a>
                    </li>
                </ul>
            </li>

            <li class="menu-connection"><a style="text-transform: initial !important;" href="{{ route('contact-us') }}">{{ Yt::trans("Biz bilan bog'lanish",'uz') }}</a></li>


        </ul>
    </div>
</nav><!-- Main Menu End-->
