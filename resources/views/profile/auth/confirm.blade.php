@extends('layouts.app')

@section('content')
    <div class="login-page page-gap">
        <div class="medium-container">
            <div class="login-page__wrap">
                <div class="title">
                    Код подтверждения
                </div>
                <div class="subtitle">
                    Введите код подтверждения который получили по смс
                </div>
                <div class="form">
                    <form action="" method="post">
                        @csrf
                        <div class="form-head">
                            <span class="active" style="width: 100%">+{{ auth()->user()->getPhone() }}</span>
                        </div>
                        <div class="form-body">
                            <div class="form-group-xn mb-20">
                                <label for="codeLabel">Код подтверждение</label>
                                <input type="text" name="code" id="codeLabel" title="" required>
                            </div>
                        </div>
                        <div class="form-footer">
                            <button class="btn-xn btn-xn-primary">Подтвердить</button>
                            <div class="text-center mt-20 font-400">
                                <a href="{{ route('profile.confirm', App::getLocale()) }}">Отправить заново</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="login-page__bg">
                <img src="/temp/images/content/login-bg.svg" alt="">
            </div>
        </div>
    </div>
@endsection
