@extends('layouts.app')
@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('content')
    <div class="login-page page-gap">
        <div class="medium-container">
            <div class="login-page__wrap">
                <div class="title">@lang('lang.pls_authorize')</div>
                <div class="subtitle">@lang('lang.auth_subtitle')</div>
                <div class="form">
                    <form action="{{ route('personal.login.form', ['locale'=>App::getLocale()])}}" method="post">
                        @csrf
                        <div class="form-head">
                            <span class="active">@lang('lang.signin')</span>
                            <a href="{{ route('profile.register', App::getLocale()) }}">@lang('lang.signup')</a>
                        </div>
                        <div class="form-body">
                            <div class="form-group-xn mb-20">
                                <label for="loginLabel">@lang('lang.phone_number') @lang('lang.or') @lang('lang.e-mail')</label>
                                <input type="text" name="login" id="loginLabel" placeholder="998 XX XXX XX XX @lang('lang.or') you@mail.com" required>
                            </div>
                            <div class="form-group-xn">
                                <label for="passwordLabel">@lang('lang.password')</label>
                                <input type="password" name="password" id="passwordLabel" placeholder="******" required>
                            </div>
                        </div>
                        <div class="form-footer">
                            <button class="btn-xn btn-xn-primary">@lang('lang.signin')</button>
                            <div class="social-login-container">
                                <a href="{{ route('signin-google') }}" class="social-icon">
                                    <img src="/temp/images/icons/google.svg" alt="google-icon">
                                </a>
                                <a href="{{ route('signin-facebook') }}" class="social-icon">
                                    <img src="/temp/images/icons/facebook-login.svg" alt="facebook-icon">
                                </a>
                            </div>
                            <div class="text-center mt-20 font-400"><a href="{{ route('password.reset', App::getLocale()) }}">@lang('lang.forgot_password')</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="login-page__bg">
                <img src="/temp/images/content/login-bg.svg" alt="">
            </div>
        </div>
    </div>
@endsection
