@extends('layouts.app')

@section('content')
    <div class="login-page page-gap">
        <div class="medium-container">
            <div class="login-page__wrap">
                <div class="title">@lang('lang.pls_authorize')</div>
                <div class="subtitle">@lang('lang.auth_subtitle')</div>
                <div class="form">
                    <form action="" method="post">
                        @csrf
                        <div class="form-head text-center">
                            <span class="active" style="width: 100%;">@lang('lang.reset_pwd_text')</span>
                        </div>
                        <div class="form-body">
                            <div class="form-group-xn mb-20">
                                <label for="phoneLabel">@lang('lang.phone_number')</label>
                                <input type="text" name="phone_number" id="phoneLabel" placeholder="998 XX XXX XX XX" required >
                            </div>
                            <div class="form-group-xn">
                                <label for="passwordLabel">@lang('lang.new_password')</label>
                                <input type="password" name="new_password" id="passwordLabel" placeholder="******" required>
                            </div>
                        </div>
                        <div class="form-footer">
                            <button class="btn-xn btn-xn-primary">@lang('lang.reset_pwd')</button>
                            <div class="text-center mt-20 font-400">
                                <a href="{{ route('profile.login', App::getLocale()) }}">@lang('lang.signin')</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="login-page__bg">
                <img src="/temp/images/content/login-bg.svg" alt="">
            </div>
        </div>
    </div>
@endsection
