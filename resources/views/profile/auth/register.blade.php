@extends('layouts.app')

@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection

@section('content')
    <div class="login-page page-gap">
        <div class="medium-container">
            <div class="login-page__wrap">
                <div class="title">@lang('lang.pls_authorize')</div>
                <div class="subtitle">@lang('lang.auth_subtitle')</div>
                <div class="form">
                    <form action="" method="post">
                        @csrf
                        <div class="form-head">
                            <a href="{{ route('profile.login', App::getLocale()) }}">@lang('lang.signin')</a>
                            <span class="active">@lang('lang.signup')</span>
                        </div>
                        <div class="form-body">
                            <div class="form-group-xn mb-20">
                                <label for="nameLabel">@lang('lang.your_fullname')</label>
                                <input type="text"
                                       name="username"
                                       id="nameLabel"
                                       placeholder="Azizov Aziz"
                                       value="{{ old('username') }}"
                                       required>
                            </div>
                            <div class="form-group-xn mb-20">
                                <label for="phoneLabel">@lang('lang.phone_number')</label>
                                <input type="text"
                                       name="phone"
                                       id="phoneLabel"
                                       placeholder="998 XX XXX XX XX"
                                       value="{{ old('phone') }}"
                                       required>
                            </div>
                            <div class="form-group-xn mb-20">
                                <label for="email">@lang('lang.email') (@lang('lang.optional'))</label>
                                <input type="email"
                                       name="email"
                                       id="email"
                                       placeholder="you@mail.com"
                                       value="{{ old('email') }}">
                            </div>
                            <div class="form-group-xn mb-20">
                                <label for="passwordLabel">@lang('lang.password')</label>
                                <input type="password" name="password" id="passwordLabel" placeholder="******" required>
                            </div>
                            <div class="form-group-xn">
                                <label for="confirmPasswordLabel">@lang('lang.confirm_password')</label>
                                <input type="password" name="confirm_password" id="confirmPasswordLabel" placeholder="******" required>
                            </div>
                        </div>
                        <div class="form-footer">
                            <button class="btn-xn btn-xn-primary">@lang('lang.signup')</button>
                            <div class="social-login-container">
                                <a href="{{ route('signin-google') }}" class="social-icon">
                                    <img src="/temp/images/icons/google.svg" alt="google-icon">
                                </a>
                                <a href="{{ route('signin-facebook') }}" class="social-icon">
                                    <img src="/temp/images/icons/facebook-login.svg" alt="facebook-icon">
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="login-page__bg">
                <img src="/temp/images/content/login-bg.svg" alt="">
            </div>
        </div>
    </div>
@endsection
