@extends('layouts.app')

@section('title')
    <title>@lang('lang.v2.my_activity')</title>
@endsection

@section('content')
<style>
    .active-btn{
        background-color: #024c67 !important;
        color: white !important;
    }
    .place-center{
        place-self: center;
    }
    .tooltip:hover:before {
    top: -4px;
    left: 47px;
    transform: rotate(0deg);
    content: none;
    }
    .tooltip:hover:after {
        top: -4px;
        left: 0px;
    }
    .tooltip-t:hover:after {
        top: -30px !important;
        left: 0px;
    }
    .modal-region-span:hover{
        color: #0296ce;
    }
    .review-card.rejected {
        border: 1px solid #dedede;
    }
    .btn-st{
        text-transform: initial; 
        padding: 6px 15px 34px 15px;
        background-color: aliceblue; 
    }
</style>
    <div class="profile-page">
        <div class="profile-page__head">
            <div class="sm-container pl-30 pr-30">
                @lang('lang.welcome'), {{ auth()->user()->getName() }}
            </div>
        </div>
    </div>
    <div class="page-menu">
        <div class="sm-container pl-30 pr-30">
            <ul>
                <li><a href="{{ route('profile.reviews', App::getLocale()) }}" class="{{ request()->is('profile/reviews*') ? 'active' : '' }}">@lang('lang.v2.my_activity')</a></li>
                <li><a href="{{ route('profile.index', App::getLocale()) }}" class="{{ request()->is('profile') ? 'active' : '' }}">@lang('lang.v2.my_page')</a></li>
            </ul>
        </div>
    </div>
    <div class="grey-section">
        <div class="sm-container">
            <div class="profile-card">

                <div class="review-list mb-0">
                    @foreach($reviews as $review)
                        <div class="review-card card-border {{ $review->statusClass() }}">
                            <div class="review-card__meta">
                                <div class="d-flex align-items-center">
                                    {{-- <div class="review-card__point {{ $review->assessmentClass() }}" data-tooltip="@lang('lang.v2.'.$review->statusText())">
                                        {{ $review->assessment }}
                                    </div> --}}
                                     @if($review->status != 0)
                                        <div class="{{ $review->starClass() }} --green mr-10" style="display: flex;" data-tooltip="@lang('lang.v2.review_waiting')">
                                            @for($i=1; $i <= 5; $i++)
                                                <i class="fa @if($i  <= $review->assessment) fa-star @else fa-star-o @endif"> </i>
                                            @endfor
                                        </div>
                                        @endif
                                    <div class="review-card__date">
                                        {{ $review->created_at->format('d.m.Y') }}

                                         <span class="pl-10">
                                            <i class="fa fa-eye" style="font-size: 24px; color: #9a9a9a;"></i>
                                            <span >{{ $review->views }}</span>
                                         </span>
                                    </div>
                                </div>
                                <div class="review-card__bank">
                                    <img src="/{{ $review->bank->image }}" alt="">
                                
                                 </div>
                            </div>
                            <a href="{{ route('review.view', ['locale' => App::getLocale(), 'id'=>$review->id]) }}" class="review-card__title">
                                {{ $review->title }}
                            </a>
                            <div class="m-description" style="text-align: justify;">
                                {{ $review->limitedFulltext() }}
                            </div>
                            <div class="review-card__footer d-flex flex-wrap">
                                <div class="col-md-9 mb-10 d-flex flex-wrap">
                                    @if($review->service)
                                        <div class="review-card__service mr-20 place-center">
                                            {{ $review->service->getName() }}
                                        </div>
                                    @endif
                                    <a href="{{ route('review.view', ['locale' => App::getLocale(), 'id'=>$review->id]) }}#comments" class="review-card__comments">
                                        <img src="/temp/images/xn/chat.svg" alt="">
                                        <span>{{ (int)$review->countComments() }} @lang('lang.a_comment')</span>
                                    </a>
                                </div>
                                @if($review->status == 2)
                                    @if(in_array($review->assessment,[1,2,3]))
                                        <div class="col-md-3 text-center">
                                            <a 
                                                data-id="{{ $review->id }}" 
                                                data-column="answer_status" 
                                                data-table-name="{{ $review->getTable() }}"  
                                                data-url="{{ route('site-notifications') }}" 
                                                column_value="{{ $review->answer_status }}"
                                                class="btn btn--medium btn-st problem-solved @if($review->answer_status == 4) active-btn @endif">
                                                @lang('lang.v2.answer_status.problem_solved_2')
                                            </a>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                {{ $reviews->links('pagination::bootstrap-4') }}

                @if(count($reviews) <= 0)
                    <div class="empty-box">
                        <div class="empty-box__icon"></div>
                        <div class="empty-box__message">
                            Faoliyatingiz ushbu bo'limda <br> paydo bo'ladi.
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('.problem-solved').click(function(){
                $(this).toggleClass('active-btn');
                let column_value =  $(this).attr('column_value');
                if(column_value == 4){
                    column_value = 1;
                    $(this).attr('column_value',column_value);
                } else {
                    column_value = 4;
                    $(this).attr('column_value',column_value);
                }

                $.ajax({
                    method:'GET',
                    url:$(this).attr('data-url'),
                    data:{
                        table_name:$(this).attr('data-table-name'),
                        id:$(this).attr('data-id'),
                        column:$(this).attr('data-column'),
                        column_value:column_value,
                        class: "Review",
                        table_column_name: 'answer_status',
                    }
                }).done(function(data){

                });
            });
        });
    </script>
@endsection