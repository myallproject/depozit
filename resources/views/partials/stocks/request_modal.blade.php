<!-- Modal -->
<div class="modal fade" id="requestModal" tabindex="-1" role="dialog" aria-labelledby="requestModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('stock.buy')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" id="request_stock">
                    {{ csrf_field() }}
                    <input type="hidden" name="stock_id">
                    <div class="container">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="@lang('stock.name')" name="name">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="@lang('stock.surname')" name="surname">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="@lang('stock.email')" name="email">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="@lang('stock.phone')" name="phone">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('stock.cancel')</button>
                <button type="submit" class="btn btn-info">@lang('stock.request')</button>
            </div>
            </form>
        </div>
    </div>
</div>
