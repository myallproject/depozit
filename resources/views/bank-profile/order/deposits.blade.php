@extends('layouts.app')

@section('title')
    <title>@lang('lang.v2.order.orders')</title>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/orders.css">
@endsection

@section('content')
    <div class="profile-page pl-10 pr-10">
        <div class="profile-page__head">
            <div class="sm-container">
                @lang('lang.welcome'), {{ auth()->user()->getName() }}
            </div>
        </div>
    </div>
    <div class="page-menu pl-10 pr-10">
        <div class="sm-container">
            <ul>
                <li>
                    <a href="{{ route('bank-profile', ['local'=>App::getLocale()]) }}" class="">
                        @lang('lang.v2.bank_profile.presonal_info')
                    </a>
                </li>
                <li>
                    <a href="{{ route('review.bp_list', ['local'=>App::getLocale(), 'id' => Auth::user()->organization->child_org_id ]) }}" class="">
                        @lang('lang.v2.bpreview_bank')
                    </a>
                </li>
                <li>
                    <a href="{{ route('profile-question-answer',['local'=>App::getLocale()]) }}">
                        @lang('lang.v2.bank_profile.question_answer')
                    </a>
                </li>
                <li>
                    <a href="{{ route('bank-orders-deposits',['local'=>App::getLocale()]) }}" class="active">
                        @lang('lang.v2.order.orders')
                    </a>
                </li> 
            </ul>
        </div>
    </div>
    <div class="grey-section">
        <div class="sm-container">
            <div class="profile-card">
                <div class="card-border">
                    <div class="profile-form__head page-menu ">
                        <div class="profile-links ">
                            <a class="profile-link-active" href="{{ route('bank-orders-deposits',['local'=>App::getLocale()]) }}">@lang('lang.deposits')</a>
                            <a href="{{ route('bank-orders-credits',['local'=>App::getLocale()]) }}">@lang('lang.credits')</a>
                            <a href="{{ route('bank-orders-debet',['local'=>App::getLocale()]) }}">@lang('lang.debit_cards')</a>
                       </div>
                    </div>
                    @include('bank-profile.order.components.orders-list', ['product'=>'deposit'])
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/js/orders.js"></script>
@endsection