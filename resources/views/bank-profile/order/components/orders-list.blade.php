@if (count($orders_unread) || count($orders_done))
    <div class="col-md-12 mb-20 orders-table">
        <div class="col-md-12 mt-10 pl-0 pr-0">
            <form action="{{ route('mark-orders-done', ['local'=>App::getLocale()]) }}" method="GET" id="orders-done-form">
                <button class="btn-xn btn-xn-primary" id="done" disabled>Bajarildi</button>
            </form>
        </div>
        <div class="hl-divider mt-25 mb-25"></div>
        <table id="orders-table">
            <thead>
                <tr>
                    <th></th>
                    <th>@lang('lang.full_name')</th>
                    <th>@lang('lang.phone_number')</th>
                    <th>@lang('lang.email')</th>
                    <th>@lang('lang.region')</th>
                    <th>@lang('lang.v2.order.type_of_'.$product)</th>
                </tr>
            </thead>
            <tbody class="unread">
                @foreach ($orders_unread as $order)
                    <tr>
                        <td>
                            <input type="checkbox" value="{{ $order->id }}">
                        </td>
                        <td>{{ $order->user_name }}</td>
                        <td>{{ $order->phone_number }}</td>
                        <td>{{ $order->email }}</td>
                        <td>{{ $order->region->name() }}</td>
                        <td><a href="{{ $order->product->link() }}">{{ $order->product->name() }}</a></td>
                    </tr>
                @endforeach
            </tbody>
            <tbody class="done">
                @foreach ($orders_done as $order)
                    <tr>
                        <td></td>
                        <td>{{ $order->user_name }}</td>
                        <td>{{ $order->phone_number }}</td>
                        <td>{{ $order->email }}</td>
                        <td>{{ $order->region->name() }}</td>
                        <td><a href="{{ $order->product->link() }}">{{ $order->product->name() }}</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@else
    <div class="col-md-12 pl-5 pr-5 mb-20" style="display: inline-block;">
        <div class="col-md-12 text-center">
            <img src="/temp/images/icons/search_data.svg" style="width:80px;">
        </div>
        <div class="col-md-12 text-center">
            @lang('lang.not_found')
        </div>
    </div>
@endif