@extends('layouts.app')

@section('title')
    <title>@lang('lang.profile')</title>
@endsection

@section('content')

    <div class="profile-page pl-10 pr-10">
        <div class="profile-page__head">
            <div class="sm-container">
                @lang('lang.welcome'), {{ auth()->user()->getName() }}
            </div>
        </div>
    </div>
    <div class="page-menu pl-10 pr-10">
        <div class="sm-container">
            <ul>
                <li>
                    <a href="{{ route('bank-profile', ['local'=>App::getLocale()]) }}" class="active">
                        @lang('lang.v2.bank_profile.presonal_info')
                    </a>
                </li>
                <li>
                    <a href="{{ route('review.bp_list', ['local'=>App::getLocale(), 'id' => Auth::user()->organization->child_org_id ]) }}">
                        @lang('lang.v2.bpreview_bank')
                    </a>
                </li>
                <li>
                    <a href="{{ route('profile-question-answer',['local'=>App::getLocale()]) }}">
                        @lang('lang.v2.bank_profile.question_answer')
                    </a>
                </li>
                <li>
                    <a href="{{ route('bank-orders-deposits',['local'=>App::getLocale()]) }}">
                        @lang('lang.v2.order.orders')
                    </a>
                </li> 
            </ul>
        </div>
    </div>
    <div class="grey-section">
        <div class="sm-container">
            <div class="profile-card">
                <div class="card-border">
                    <div class="profile-form__head">
                        <div class="profile-name">{{ auth()->user()->getName() }}</div>
                        <div>
                            <button class="btn-xn btn-xn-default update-form-slide" disabled><i class="fa fa-pencil"></i> @lang('lang.edit')</button>
                        </div>
                    </div>
                    <div class="hl-divider mt-25 mb-30"></div>
                    <div class="profile-form profile-form-view mb-5">
                        <div class="row row-xn">
                            <div class="col-md-6">
                                <div class="form-group-xn mb-20">
                                    <label>@lang('lang.v2.bank_profile.fio')</label>
                                    <div class="profile-form__value">{{ auth()->user()->getName() }}</div>
                                </div>
                                <div class="form-group-xn  mb-20">
                                    <label>@lang('lang.v2.bank_profile.your_phone')</label>
                                    <div class="profile-form__value">{{ auth()->user()->phone }}</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                               {{--  <div class="form-group-xn mb-30">
                                    <label>@lang('lang.v2.bank_profile.gender')</label>
                                    <div class="profile-form__value"> <i class="fa {{ auth()->user()->getGender() }}"></i></div>
                                </div> --}}
                                <div class="form-group-xn mb-20">
                                    <label>@lang('lang.v2.bank_profile.email')</label>
                                    <div class="profile-form__value">{{ auth()->user()->email }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-form" style="@if(count($errors) > 0) @else display: none @endif" id="profile-update-form"> 
                        <form action="{{ route('profile.edit.user',App::getLocale()) }}" method="post">
                            <input type="hidden" name="id" value="{{ auth()->user()->id() }}" />
                            @csrf
                             <div class="row row-xn mb-10">
                                <div class="col-md-6">
                                    <div class="form-group profile mb-10">
                                        <label for="nameLabel">@lang('lang.v2.bank_profile.fio')</label>
                                        <input type="text" name="name" id="nameLabel" value="{{ auth()->user()->getName() }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group profile mb-10">
                                        <label for="phoneLabel">@lang('lang.v2.bank_profile.your_phone')</label>
                                        <input type="phone" name="phone" id="phoneLabel" maxlength="13" value="{{ auth()->user()->phone }}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group profile mb-10">
                                        <label for="phoneLabel">@lang('lang.v2.bank_profile.email')</label>
                                        <input type="email" name="email" id="phoneLabel"  value="{{  auth()->user()->email  }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group profile mb-10">
                                        <label for="genderLabel">@lang('lang.v2.bank_profile.gender')</label>
                                        <select name="gender" id="genderLabel" class="select2-xn">
                                            <option value="0">@lang('lang.choose')</option>
                                            @foreach(config('global.gender') as $kg => $vg)    
                                                <option @if(auth()->user()->gender == $kg ) selected @endif value="{{ $kg }}">@lang('lang.'.$vg)</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-xn">
                                <div>
                                    <button class="btn-xn btn-xn-primary">@lang('lang.save')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="profile-card">
                <div class="profile-card__title pl-10 pr-10">@lang('lang.password')</div>
                <div class="card-border pl-10 pr-10">
                    <form action="{{ route('profile.change-password', App::getLocale()) }}" method="post">
                        @csrf
                        <div class="row row-xn">
                            <div class="col-md-6">
                                <div class="form-group-xn mb-10">
                                    <label for="currentPasswordLabel">@lang('lang.v2.bank_profile.old_password')</label>
                                    <input type="password" name="current_password" id="currentPasswordLabel">
                                </div>
                            </div>
                        </div>
                        <div class="row row-xn mb-10">
                            <div class="col-md-6">
                                <div class="form-group-xn mb-10">
                                    <label for="newPasswordLabel">@lang('lang.v2.bank_profile.new_password')</label>
                                    <input type="password" name="new_password" id="newPasswordLabel" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group-xn mb-10">
                                    <label for="confirmPasswordLabel">@lang('lang.v2.bank_profile.confirm_password')</label>
                                    <input type="password" name="new_confirm_password" id="confirmPasswordLabel" >
                                </div>
                            </div>
                        </div>
                        <div class="row row-xn">
                            <div>
                                <button class="btn-xn btn-xn-primary">@lang('lang.save')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    {{-- <script>
        $(document).ready(function() {
            $('.update-form-slide').on('click',function(){
                $('#profile-update-form').slideToggle(1000);
            });
        });
    </script> --}}
@endsection