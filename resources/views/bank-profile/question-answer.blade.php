@extends('layouts.app')

@section('title')
    <title>@lang('lang.profile')</title>
@endsection

@section('content')
<link rel="stylesheet" type="text/css" href="/css/questions.css">

    <div class="profile-page pl-10 pr-10">
        <div class="profile-page__head">
            <div class="sm-container">
                @lang('lang.welcome'), {{ auth()->user()->getName() }}
            </div>
        </div>
    </div>
    <div class="page-menu pl-10 pr-10">
        <div class="sm-container">
            <ul>
                <li>
                    <a href="{{ route('bank-profile', ['local'=>App::getLocale()]) }}">
                        @lang('lang.v2.bank_profile.presonal_info')
                    </a>
                </li>
                <li>
                    <a href="{{ route('review.bp_list', ['local'=>App::getLocale(), 'id' => Auth::user()->organization->child_org_id ]) }}" class="">
                        @lang('lang.v2.bpreview_bank')
                    </a>
                </li>
                <li>
                    <a href="{{ route('profile-question-answer',['local'=>App::getLocale()]) }}" class="active">
                        @lang('lang.v2.bank_profile.question_answer')
                    </a>
                </li>
                <li>
                    <a href="{{ route('bank-orders-deposits',['local'=>App::getLocale()]) }}">
                        @lang('lang.v2.order.orders')
                    </a>
                </li> 
            </ul>
        </div>
    </div>
    <div class="grey-section">
        <div class="sm-container">
            <div class="profile-card">
                <div class="card-border">
                    <div class="profile-form__head page-menu ">
                       <div class="profile-links ">
                            <a class="profile-link-active" href="{{ route('profile-question-answer',['local'=>App::getLocale()]) }}" >Yangi savollar</a>
                            <a  href="{{ route('profile-bank-answered-questions',['local'=>App::getLocale()]) }}">Javob berilgan savollar</a>
                       </div>
                    </div>
                     <div class="hl-divider mt-25 mb-30"></div>
                    <div>
                        @foreach($questions as $row_q)
                            <div class="col-md-12 pl-5 pr-5 mb-20 question-card" style="display: inline-block;">
                                <div class="col-md-12 border  card-q" style=" padding: 10px; display: inline-block; border-color: #dedede;">
                                    <div class="col-md-12 pl-0 pr-0 mb-10 mt-10" style="flex-wrap: wrap; display: flex;"> 
                                        <div class="col-md-2 text-center"><h4 style="margin:0px">Savol</h4>
                                            <div class="mt-5 mb-5" style="font-size: 12px">
                                                <span style="display:block">
                                                    <i class="mr-5 fa {{ $user->getGender() }}"></i>{{ $user->getName() }}
                                                </span>
                                                <span style="display:block">
                                                    <i class="mr-5 fa fa-clock"></i> {{ date('d/m/Y',strtotime($row_q->created_at)) }}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-10 text-justify" style="" id="question-text-{{ $row_q->id }}">{{ $row_q->question }}</div>
                                    </div>
                                    <div class="col-md-12 mb-10 mt-10">        
                                        <button class="btn-xn btn-xn-default answer-show-modal" data-question-id="{{ $row_q->id }}"><i class="fa fa-pencil"></i> 
                                            Javob yozish
                                        </button>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                            <div class="col-md-12 pl-5 pr-5 mb-20  news-pagination">
                               {{ $questions->links() }} 
                            </div>

                            @if(count($questions) <= 0)
                                <div class="col-md-12 pl-5 pr-5 mb-20 question-card" style="display: inline-block;">
                                    <div class="col-md-12 text-center">
                                        <img src="/temp/images/icons/search_data.svg" style="width:80px;">
                                    </div>
                                    <div class="col-md-12 text-center">
                                        @lang('lang.not_found')
                                    </div>
                                </div>
                            @endif
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
<a style="display:none" class="answer-modal-btn" data-modal="answer-modal"></a>
<div class="modal-ui" data-modal-name="answer-modal">
    <div class="modal-ui__dialog detailed-search">
        <div class="modal-ui__content">
            <div class="modal-ui__close-btn"></div>
            <h2 class="b-title">Javob yozish</h2>
            <form action="{{ route('profile-write-answer',['local'=>App::getLocale()]) }}" method="POST" multiple>
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Savol</label>
                            <div class="border" style="border-radius: 5px; padding: 15px" id="content-question"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Javob</label>
                            <input type="hidden" name="id" id="question-id" value="">
                           <textarea rows="6" style="border-radius: 5px" id="answer-text"  name="answer_text"></textarea>
                        </div>
                    </div>
                </div>
                <div class="buttons">
                    <div class="text-center mb-20">
                        <button class="btn-xn btn-xn-primary">Javob berish</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection

@section('script')
    <script>
        $('.answer-show-modal').click(function(){
            $('#question-id').val($(this).attr('data-question-id'));
            $('#content-question').text($('#question-text-'+$(this).attr('data-question-id')).text());
            $('#answer-text').val('');
            $('.answer-modal-btn').click();

        });
    </script>
@endsection