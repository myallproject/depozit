@extends('layouts.app')

@section('meta')
    <meta name="viewport" content="width=1400">
     @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection

@section('css')
    <link rel="stylesheet" href="/css/ranking.css">
@endsection

@section('content')
    <div class="md-container page-gap">
        <h2 class="m-title">@lang('lang.citizen_ranking_full')</h2>
        <div class="row">
            <div class="mb-20 col-md-9 pl-15 pr-15">
                <div class="card-border mb-20 " style="padding-bottom: 17px">
                <form id="ranking-filter-form" action="{{ route('ranking-citizen-filter',['locale'=>App::getLocale()]) }}" method="GET">
                    <div class="row" style="padding: 0px; margin:0px;">
                        <div class="col-sm-6" style="margin-bottom: 8px; padding:0px !important; padding-left:10px !important">
                            <div class="form-group">
                                <label for="date">
                                    <img src="/temp/images/ranking/calendar.svg" alt="date">@lang('lang.reyting_condition')
                                </label>
                                <select name="date" class="select2__js ranking-ajax-filter" id="select-date">
                                    <option value="{{$date[0]}}">@lang('lang.todays')</option>
                                    <option value="{{$date[1]}}">@lang('lang.1_week')</option>
                                    <option value="{{$date[2]}}">@lang('lang.1_month')</option>
                                    <option value="{{$date[3]}}">@lang('lang.3_month')</option>
                                    <option value="{{$date[4]}}">@lang('lang.6_month')</option>
                                    <option value="{{$date[5]}}">@lang('lang.1_year')</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6" style="margin-bottom: 8px; padding:0px !important; padding-left:10px !important">
                            <div class="form-group">
                                <label for="services">
                                    <img src="/temp/images/ranking/services.svg" alt="services">@lang('lang.all_services')
                                </label>
                                <select name="services" class="select2__js ranking-ajax-filter" id="select-services">
                                    <option value="0">@lang('lang.services')</option>
                                    @foreach($services as $service)
                                        @if(count($service->children()) > 0)
                                            <option value="{{ $service->id }}">{{ $service->getName() }}</option>
                                            @foreach($service->children() as $child)
                                                <option value="{{ $child->id }}">&nbsp;&nbsp;{{ $child->getName() }}</option>
                                            @endforeach
                                        @else
                                            <option value="{{ $service->id }}">{{ $service->getName() }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
                <div class="ranking-list">
                    <div class="review-card card-border">
                        <div class="col-md-12 d-flex" style="justify-content:flex-end;align-items:flex-end; padding:0px;">
                            <div class="col-sm-4" style="padding:0px !important;">
                                <input type="search" class="bank-search" name="search-bank" id="search-bank" results="5" placeholder="@lang('lang.search')...">
                                <button class="bank-search-icon"></button>
                            </div>
                        </div>
                        <div id="ranking-result-content">
                            @include('frontend.ranking.citizen-ranking-ajax',['data' => $data])
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3 pl-15 pr-15">
                <div class="card-border mb-20">
                    <span class="d-block m-description" style="line-height: 17px; margin-bottom: 8px">@lang('lang.v2.index_page.text_right')</span>
                    <a href="{{ route('review.add',['locale' => App::getLocale()]) }}" class="btn-xn btn-xn-primary btn-xn-justify">
                        @lang('lang.v2.index_page.text_btn')
                    </a>
                </div>
                <div class="rating-reviews card-border">
                    <h3 class="sm-title">@lang('lang.top_services_ranking')</h3>
                    <table style="width: 100%">
                        <tbody>
                            @if ($top_services)
                                @foreach ($top_services as $service)
                                    <tr>
                                        <td style="width: 70%;">
                                            <a href="{{ route('info-bank',['locale'=>App::getLocale(),'slug'=>$service['bank_slug']]) }}" class="rating-bank" target="_blank">
                                                {{ $service['bank_name'] }}
                                            </a>
                                            <span class="rating-specification"> {{ $service['service_name'] }} </span>
                                        </td>
                                        <td style="width: 30%;"> {{ $service['mark'] }} </td>
                                    </tr>
                                @endforeach
                            @else
                                This ranking is unaviable for now!
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $("#search-bank").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#ranking-table tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $('.ranking-ajax-filter').on('change',function(e){
                e.preventDefault();
                form = $('#ranking-filter-form');
                $.ajax({
                    url:form.attr('action'),
                    method:form.attr('method'),
                    data:form.serialize()
                }).done(function(data){
                    $('#ranking-result-content').html(data)
                });
            });
        });
    </script>
@endsection