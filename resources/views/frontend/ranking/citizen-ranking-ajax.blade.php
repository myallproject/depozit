@if (count($data)>0)
    <div class="col-md-12" style="padding: 0px;">
        <table class="text-left">
            <thead>
                <tr>
                    <th style="width: 7%;">@lang('lang.position')</th>
                    <th style="width:27%;">@lang('lang.bank')</th>
                    <th class="small-table-cell">@lang('lang.ranking')</th>
                    <th class="medium-table-cell">@lang('lang.avg_score')</th>
                    <th class="small-table-cell">@lang('lang.all_reviews')</th>
                    <th class="small-table-cell">@lang('lang.answered_reviews')</th>
                    <th class="small-table-cell">@lang('lang.solved_reviews')</th>
                </tr>
            </thead>
            <tbody id="ranking-table">
                @php $rank=1 @endphp
                @foreach ($data as $bank)
                    <tr>
                        <td style="width: 7%;">{{$rank}}</td>
                        <td style="width:27%;">
                            <div style="display:flex; align-items:center;">
                                <img src="/{{$bank['bank']->image}}" alt="bank logo" style="width: 40px; height:auto;">
                                <a href="{{ route('info-bank',['locale'=>App::getLocale(),'slug'=>$bank['bank']->slug]) }}" target="_blank" style="padding-left: 5px;">{{$bank['bank']->name()}}</a>
                            </div>
                        </td>
                        <td class="small-table-cell">{{$bank['bank']->ranking($filter_date, $service_id)}}</td>
                        <td class="medium-table-cell">
                            <div class="average-rank-container">
                                <span>{{$bank['bank']->formatedAvgMark($filter_date, $service_id)}}</span>
                                <span class="stars">
                                    {{$bank['bank']->stars($filter_date, $service_id)}}
                                </span>
                            </div>
                        </td>
                        <td class="small-table-cell">{{$bank['bank']->countAllReviews($filter_date, $service_id)}}</td>
                        <td class="small-table-cell">{{$bank['bank']->countAnsweredReviews($filter_date, $service_id)}}</td>
                        <td class="small-table-cell">{{$bank['bank']->countSolvedReviews($filter_date, $service_id)}}</td>
                        @php $rank++ @endphp
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@else
    <div class="col-md-12 text-center">
        <img src="/temp/images/icons/search_data.svg" style="width:80px;">
    </div>
    <div class="col-md-12 text-center">
        @lang('lang.not_found')
    </div>
@endif