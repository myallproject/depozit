@foreach($questions as $row_q)
    <div class="col-md-8 mb-20 review-card card-border mt-0" style="padding:0!important;">
        <div style="padding: 10px 15px;">
            <div class="review-card__meta">
                <div>
                    <div style="margin-block: 2px;">
                        <b>@lang('lang.question')</b>
                    </div>
                    <div class="d-flex align-items-center flex-wrap">
                        <div class="review-card__date mr-10" style="font-size: 15px">{{ date('d.m.Y',strtotime($row_q->created_at)) }}</div>
                        <div class="review-card__date"><i class="fa fa-user-o"></i><span class="ml-10">{{ $row_q->user->getName() }}</span></div>
                    </div>
                </div>
                <div class="review-card__bank">
                    <img src="/{{ $row_q->bank->image }}" alt="">
                </div>
            </div>
            <div id="question-text-{{ $row_q->id }}" class="review-card__title">
                {{ $row_q->question }}
            </div>
            <div class="review-card__footer d-flex flex-wrap">
                @if($row_q->service)
                    <div class="review-card__service mr-20">
                        {{ $row_q->service->getName() }}
                    </div>
                @endif    
            </div>
        </div>
        
        <div style="border-top: 1px solid #dedede;"></div>

        @foreach($row_q->answers as $answer)
        <div style="padding: 10px 15px; background-color:#fafafa">
            <div class="review-card__meta">
                <div>
                    <div style="margin-block: 2px;">
                        <b>@lang('lang.answer')</b>
                    </div>
                    <div class="d-flex align-items-center flex-wrap">
                        <div class="review-card__date mr-10" style="font-size: 15px">{{ date('d.m.Y',strtotime($answer->created_at)) }}</div>
                        <div class="review-card__date"><i class="fa fa-user-o"></i><span class="ml-10">{{ $answer->user->getLocalName() }}</span></div>
                    </div>
                </div>
                <div class="review-card__bank">
                    <img src="/{{ $row_q->bank->image }}" alt="">
                </div>
            </div>
            <div id="answer-text-id-{{ $answer->id }}" class="review-card__title">
                {{ $answer->answer_text }}
            </div>
        </div>
        @endforeach
    </div>
@endforeach
<div class="col-md-12 pl-5 pr-5 mb-20  news-pagination">
   {{ $questions->links() }} 
</div>


@if(count($questions) <= 0)
<div class="col-md-12 pl-5 pr-5 mb-20 question-card" style="display: inline-block;">
    <div class="col-md-12 text-center">
        <img src="/temp/images/icons/search_data.svg" style="width:80px;">
    </div>
    <div class="col-md-12 text-center">
        @lang('lang.not_found')
    </div>
</div>
@endif