<div class="col-md-12 text-left pl-0 pr-0 ">
    <form action="{{ route('bank-add-question',['locale'=>App::getLocale()]) }}" method="post">
    	@csrf
        <input type="hidden" name='bank_id' value="{{ $bank->id }}">
        <div class="form-group mt-10 mb-30">
            <label for="#select-services">@lang('lang.questions_page.service_type') <sub class="text-danger" style="font-size: 30px">*</sub></label>
            <select id="select-services" class="select2__js" name="service_id">
            	<option value="">@lang('lang.choose')</option>}
            	@foreach($services as $service)
                    @if(count($service->children()) > 0)
                        <option  @if(old('service_id') == $service->id) selected @endif  value="{{ $service->id }}">{{ $service->getName() }}</option>
                        @foreach($service->children() as $child)
                            <option  @if(old('service_id') == $service->id) selected @endif value="{{ $child->id }}">&nbsp;&nbsp;{{ $child->getName() }}</option>
                        @endforeach
                    @else
                        <option  @if(old('service_id') == $service->id) selected @endif value="{{ $service->id }}">{{ $service->getName() }}</option>
                    @endif
                @endforeach
            </select> 
            @if($errors->has('service_id'))
            	<span class="text-danger">{{ $errors->first('service_id') }}</span>
            @endif                                       
        </div>
        <div class="form-group mt-10 mb-30">
            <label for="#question-text">@lang('lang.questions_page.your_question') <sub class="text-danger" style="font-size: 30px">*</sub></label>
            <textarea id="question-text" class="textarea mb-5" name="question">{{ old('question')}}</textarea>
            @if($errors->has('question'))    
            	<span class="text-danger">{{ $errors->first('question') }}</span>
            @endif
            <span style="text-align: justify !important ;display: -webkit-box;">@lang('lang.questions_page.question_description')</span>
        </div>
        <div class="form-group mt-10 mb-30">
            <label for="#info_bank-text">@lang('lang.v2.add_form.info_bank')</label>
            <textarea id="info_bank-text" class="textarea mb-5" name="info_bank">{{ old('info_bank') }}</textarea>
        </div>

        <div class="form-group mt-10 mb-30">
            <i><sub class="text-danger" style="font-size: 30px">*</sub><span> - @lang('lang.required_field')</span></i>
        </div>
        <div class="form-group mt-10 mb-30">
            <button type="submit" class="btn btn--medium btn--blue btn--medium-new" style="height: auto; line-height: 40px;">@lang('lang.questions_page.send_question')</button>
        </div>
    </form>
</div>