@extends('layouts.app')

@section('meta')
 @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection

@section('content')
<link href="/css/banks.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="/css/questions.css">
    <div class="section">
        <div class="section-gap--small" style="margin-top: 0px;">
            <div class="section-gap-bank">
                <div class="medium-container">

                        <div class=" col-md-12 text-center pl-5 pr-5 " style="display: flex">
                             <a href="{{ route('bank-question-list',['locale' => App::getLocale()]) }}" class="tab-question-list "  >@lang('lang.questions_page.question_to_bank')</a>
                             <a href="{{ route('bank-question-type-list',['locale' => App::getLocale()]) }}" class="tab-question-list active-tab">@lang('lang.questions_page.question_type_service')</a>
                        </div>
                        
                    <div class="filter-result">
                        <div class="col-md-12 pl-0 pr-0 question-category mb-20" style="display: inline-block;">
                                <a class="" href="{{ route('bank-question-type-list',['locale' => App::getLocale()]) }}">@lang('lang.questions_page.all')</a>   
                                @foreach($services as $row_s)
                                <a class="" href="{{ route('bank-question-type-list',['locale' => App::getLocale(), 'tagid' => $row_s->id, 'tag' => $row_s->getName()]) }}">{{ $row_s->getName() }}</a>   
                                @endforeach
                            </div>
                    <div class="table filter-table">
                        <div class="tbody bank-name" id="result-search-bank">
                            @foreach($questions as $row_q)
                                <div class="col-md-8 mb-20 review-card card-border mt-0" style="padding:0!important;">
                                    <div style="padding: 10px 15px;">
                                        <div class="review-card__meta">
                                            <div>
                                                <div style="margin-block: 2px;">
                                                    <b>@lang('lang.question')</b>
                                                </div>
                                                <div class="d-flex align-items-center flex-wrap">
                                                    <div class="review-card__date mr-10" style="font-size: 15px">{{ date('d.m.Y',strtotime($row_q->created_at)) }}</div>
                                                    <div class="review-card__date"><i class="fa fa-user-o"></i><span class="ml-10">{{ $row_q->user->getName() }}</span></div>
                                                </div>
                                            </div>
                                            <div class="review-card__bank">
                                                <img src="/{{ $row_q->bank->image }}" alt="">
                                            </div>
                                        </div>
                                        <div id="question-text-{{ $row_q->id }}" class="review-card__title">
                                            {{ $row_q->question }}
                                        </div>
                                        <div class="review-card__footer d-flex flex-wrap">
                                            @if($row_q->service)
                                                <div class="review-card__service mr-20">
                                                    {{ $row_q->service->getName() }}
                                                </div>
                                            @endif    
                                        </div>
                                    </div>
                                    
                                    <div style="border-top: 1px solid #dedede;"></div>

                                    @foreach($row_q->answers as $answer)
                                    <div style="padding: 10px 15px; background-color:#fafafa">
                                        <div class="review-card__meta">
                                            <div>
                                                <div style="margin-block: 2px;">
                                                    <b>@lang('lang.answer')</b>
                                                </div>
                                                <div class="d-flex align-items-center flex-wrap">
                                                    <div class="review-card__date mr-10" style="font-size: 15px">{{ date('d.m.Y',strtotime($answer->created_at)) }}</div>
                                                    <div class="review-card__date"><i class="fa fa-user-o"></i><span class="ml-10">{{ $answer->user->getLocalName() }}</span></div>
                                                </div>
                                            </div>
                                            <div class="review-card__bank">
                                                <img src="/{{ $row_q->bank->image }}" alt="">
                                            </div>
                                        </div>
                                        <div id="answer-text-id-{{ $answer->id }}" class="review-card__title">
                                            {{ $answer->answer_text }}
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            @endforeach
                            <div class="col-md-12 pl-5 pr-5 mb-20  news-pagination">
                               {{ $questions->links() }} 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection