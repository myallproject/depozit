@extends('layouts.app')

@section('title')
  <title>@lang('lang.questions_page.question_answer') - @lang('lang.questions_page.questions_banks',['bank'=>$bank->name()])</title>
@endsection
@section('meta')
    <meta name="description" content="@lang('pages.bank.description')"/>
    <meta name="keywords" content="@lang('pages.bank.keywords')" />
@endsection

@section('content')
<link rel="stylesheet" type="text/css" href="/css/questions.css">
    <div class="section">
        <div class="section-gap--small" style="margin-top: 0px;">
            <div class="medium-container">
                <div class="credit-info-main" style="padding-bottom: 0px;">
                    <div class="row mx-auto" style="width: 100%;">
                        <div class="col-12 col-md-3 text-center pt-10 pl-5 pr-5" style="padding-left: 0px; padding-right: 0px;">
                            <img src="/{{ $bank->image }}" alt="" style="width: auto; height: 150px;">
                        </div>
                        <div class="col-12 col-md-9 pl-5 pr-5 question-p-title" >
                            <h2 class="font-30">@lang('lang.questions_page.questions_banks',['bank'=>$bank->name()])</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="divider-ui--full"></div>

            <div class="section-gap-bank">
                <div class="medium-container">
                    <div class="col-md-12 mobile-centered">
                        <a href="{{ route('bank-give-question',['locale' => App::getLocale(),'slug' => $bank->slug]) }}" class="btn btn--medium btn--blue btn--medium-new" style="height: auto; line-height: 40px;">@lang('lang.questions_page.give_question')</a>
                    </div>
                    <div class="col-md-12 text-center pl-5 pr-5 " style="display: flex">
                        <a href="{{ route('bank-list-answers',['locale' => App::getLocale(),'slug' => $bank->slug]) }}" class="tab-question-list" >@lang('lang.questions_page.users_questions')</a>
                        <a href="{{ route('faq-question-list',['locale' => App::getLocale(),'slug' => $bank->slug]) }}" class="tab-question-list active-tab ">@lang('lang.questions_page.faq')</a>
                    </div>
                     <div class="col-md-12 pl-5 pr-5 ">
					    <h2 class="font-25">@lang('lang.questions_page.question_categories')</h2>
						<div class="col-md-12 pl-0 pr-0 question-category mb-20" style="display: inline-block;">
					        <a class="" href="{{ route('faq-question-list',['locale' => App::getLocale(),'slug' => $bank->slug]) }}">@lang('lang.questions_page.all')</a>   
					        @foreach($services as $row_s)
					        <a class="" href="{{ route('faq-question-list',['locale' => App::getLocale(),'slug' => $bank->slug, 'tagid' => $row_s->id, 'tag' => $row_s->getName()]) }}">{{ $row_s->getName() }}</a>   
					        @endforeach
					    </div>
					</div>
                    @include('frontend.questions.blocks.question-list')   
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection