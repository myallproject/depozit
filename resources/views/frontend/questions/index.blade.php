@extends('layouts.app')

@section('meta')
  @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection

@section('content')
<link href="/css/banks.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="/css/questions.css">
    <div class="section">
        <div class="section-gap--small" style="margin-top: 0px;">
            <div class="section-gap-bank">
                <div class="medium-container">

                        <div class=" col-md-12 text-center pl-5 pr-5 " style="display: flex">
                             <a href="{{ route('bank-question-list',['locale' => App::getLocale()]) }}" class="tab-question-list active-tab"  >@lang('lang.questions_page.question_to_bank')</a>
                             <a href="{{ route('bank-question-type-list',['locale' => App::getLocale()]) }}" class="tab-question-list">@lang('lang.questions_page.question_type_service')</a>
                        </div>
                        

                    <div class="filter-result">
                    <div class="col-md-12 pl-0 pr-0">
                        <div class="form-group">
                            <input type="text" id="search-bank" name="search_bank" results="5" placeholder="@lang('lang.search')...">
                            <i id="search-icon" class="fa fa-search btn-search search-banks"></i>
                        </div>
                    </div>
                    <div class="table filter-table">
                       
                        <div class="tbody bank-name" id="result-search-bank">
                           
                            @foreach($banks as $bank)
                            @php $faq = $bank['bank']->questions->first(); @endphp

                            <div class="question-card row">
                                <div class="col-12 question-card-title">
                                    <h3 class="mb-5 mt-5"><a href="{{ route('info-bank', ['local'=>App::getLocale(), 'slug'=>$bank['bank']->slug])}}" target="_blank">{{ $bank['bank']->name() }}</a> : @lang('lang.hots_line')</h3>
                                </div>
                                <div class="col-12 row pt-10 pb-5 banker-container">
                                    <div class="col-2 text-center">
                                        <a>
                                            <span class="banker-img" style=
                                                @if($bank['bank']->hotLineUserOne && $bank['bank']->hotLineUserOne->user->user_image)
                                                    "background-image: url(/{{ $bank['bank']->hotLineUserOne->user->user_image }})"
                                                @else 
                                                    "background-image: url(/temp/images/user.svg)"
                                                @endif>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-10">
                                        @if($bank['bank']->hotLineUserOne)
                                            <div class="banker-info">
                                                <h3>@lang('lang.who_answers', ['banker'=>$bank['bank']->hotLineUserOne->user->getLocalName()])</h3>
                                                <p >{{ $bank['bank']->hotLineUserOne->user->organization->position() }}</p>
                                            </div>
                                        @endif
                                        <div class="btn-container">
                                            <a href="{{ route('bank-give-question',['locale' => App::getLocale(),'slug' => $bank['bank']->slug]) }}" class="btn btn--medium btn--blue btn-outline-dark-blue">@lang('lang.questions_page.give_question')</a>
                                            <a href="{{ route('bank-list-answers',['locale' => App::getLocale(),'slug' => $bank['bank']->slug]) }}" class="btn btn--medium btn--blue btn-outline-dark-blue">@lang('lang.questions_page.see_answer')</a>
                                            <a href="{{ route('faq-question-list',['locale' => App::getLocale(),'slug' => $bank['bank']->slug]) }}" class="btn btn--medium btn--blue btn-outline-dark-blue">@lang('lang.questions_page.faq')</a>
                                        </div>
                                    </div>
                                </div>
                                @if($faq)
                                    <div class="col-12 faq mt-10 pb-10">
                                        <h4>@lang('lang.question')</h4>
                                        <p>
                                            {{ $faq->question }}
                                        </p>
                                        <a href="{{ route('bank-list-answers',['locale' => App::getLocale(),'slug' => $bank['bank']->slug]) }}">@lang('lang.detailed') <i class="fa fa-angle-down"></i></a>
                                    </div>
                                @endif
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $("#search-bank").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#result-search-bank .question-card").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
@endsection