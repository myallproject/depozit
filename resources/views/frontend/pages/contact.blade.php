@extends('layouts.app')

@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('content')
    <div class="medium-container section-gap mb-10 mt-30">
        <div class="m-title text-center">
            <h2>@lang('lang.contact_with_uz')</h2>
        </div>
    </div>
    <div class="divider-ui--full"></div>
    <div class="medium-container section-gap text-center mt-30">
        <div class="col-md-12">
            @if(Session::has('success'))
                <div class="success-msg">
                    <span>
                        {{ Session::get('success') }}
                    </span>
                </div>
            @endif
        </div>
        <h2 class="b-title">@lang('lang.send_message')</h2><span class="sub-title">@lang('lang.contact_text')</span>
        <div class="contact-form">
            <form method="POST" action="{{ route('contact') }}" id="contact-form">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="name" value="{{ old('name')}}" placeholder="@lang('lang.full_name')" >
                            @if($errors->has('name'))    
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="phone" value="{{ old('phone') }}" placeholder="@lang('lang.phone')" >
                            @if($errors->has('phone'))    
                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="@lang('lang.email')">
                    @if($errors->has('email'))    
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                </div>
                <div class="form-group">
                    <textarea name="text" placeholder="@lang('lang.message')" >{{ old('text') }}</textarea>
                     @if($errors->has('text'))    
                        <span class="text-danger">{{ $errors->first('text') }}</span>
                    @endif
                </div>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group"><input type='text' name="captcha">
                        @if($errors->has('captcha'))    
                            <span class="text-danger">{{ $errors->first('captcha') }}</span>
                        @endif
                    </div>
                </div>
                    <div class="col-md-6">
                        <div class="form-group d-flex">
                           <div class="captcha captcha-content text-center">
                               <span style="width: 122px; height: 40px;">{!! captcha_img() !!}</span>
                           </div> 

                           <div class="captcha-btn-form">
                               <button type="button" class="btn btn-captcha btn-captcha-refresh btn--blue"><i class="fa fa-refresh"></i></button>
                           </div>
                        </div>
                    </div>
                </div><button type="submit" class="btn btn--medium btn--blue">@lang('lang.send')</button>
            </form>
        </div>
        <div class="contact-info"><span>@lang('lang.our_address')</span>  <a href="tel:998993688711">+998 99 368 87 11</a>  <a href="mailto:info@depozit.uz">info@depozit.uz</a>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
           $('.btn-captcha-refresh').on('click',function(){
                $('.captcha-content span').html('<i style="font-size:36px" class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                $.ajax({
                    method:'GET',
                    url:"{{ route('captcha-refresh') }}"
                }).done(function(data){
                    $('.captcha-content span').html(data.captcha);
                });
           });
           setTimeout(function(){$('.success-msg').hide();},5000);
        });
    </script>
@endsection