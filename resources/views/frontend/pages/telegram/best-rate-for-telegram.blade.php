@extends('layouts.app-telegram')

@section('content')
<style type="text/css" media="screen">
    .medium-container {
        max-width: 978px !important;
    }    
</style>
	<div class="section-gap--small mt-20 mb-10">
        <div class="medium-container">
            <img src="/temp/images/logo.svg" style="position: absolute;" width="300px" alt="Depozit.uz">
            <h2 class="b-title mb-20" style="padding-top: 15px">@lang('lang.great_chance')</h2>
            <div class="col-md-12 text-right " style="margin-bottom: 5px; margin-top: -35px">
                <div class="" style="display:-webkit-inline-box; font-size: 15px; color: #3aa5d0">
                    <div style="margin-right: 8px">
                        @php
                            $time =  $best_rate_update_time['data_update_time'];
                            if(!$time){
                                $time = date('Y-m-d h:m');
                            }
                        @endphp 
                        <span >{{ date('d/M/Y H:i',strtotime($time)) }}</span><br>
                        <span >@lang('lang.update_time')</span>
                    </div>
                    <img src="/temp/images/update_time.png" width="35" />
                </div>
            </div>
           <style>
                .new-width .td:nth-child(1),  .new-width .th:nth-child(1){
                    width: 15% !important;
                }
                .new-width .td:nth-child(2),  .new-width .th:nth-child(2){
                    width: 30% !important;
                }
                 .new-width .td:nth-child(3),  .new-width .th:nth-child(3){
                    width: 30% !important;
                }
                 .new-width .td:nth-child(4),  .new-width .th:nth-child(4){
                    width: 20% !important;
                }
                .uzs-size{
                    font-size: 50%;
                }
                .td{
                    padding-top: 8px !important;
                    padding-bottom: 8px !important;
                }
            </style>
<div class="table best-currency-table">
    <div class="thead">
        <div class="tr new-width">
            <div class="th">@lang('lang.currency')</div>
            <div class="th">@lang('lang.cb_course')</div>
            <div class="th">@lang('lang.sale')</div>
            <div class="th">@lang('lang.take')</div>
        </div>
    </div>
    <div class="tbody">
        <div class="tr new-width">
            <div class="td">
                <div class="font-800">USD</div>
            </div>
            <div class="td">
                <div> {{ number_format($cd_rate['usd']['rate'],2)}} <span class="uzs-size">UZS</span></div>
                <div class="small">{{ $cd_rate['usd']['diff'] }} <span class="uzs-size">UZS</span></div>
            </div>
            <div class="td" style="display: flex">
                <div>
                    @if($usd_take['bank']['image'])
                        <img class="widget-bank-logo" src="/{{ $usd_take['bank']['image'] }}" />
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($usd_take['take'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $usd_take['bank']['name_uz'] }}</div>
                </div>


            </div>
            <div class="td" style="display: flex">
                <div >
                    @if($usd_sale['bank']['image'])
                        <img class="widget-bank-logo" src="/{{ $usd_sale['bank']['image'] }}" />
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($usd_sale['sale'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $usd_sale['bank']['name_uz'] }}</div>
                </div>

            </div>
        </div>
        
        <div class="tr new-width">
            <div class="td">
                <div class="font-800">EUR</div>
            </div>
            <div class="td">
                <div>{{ number_format($cd_rate['eur']['rate'],2) }} <span class="uzs-size">UZS</span></div>
                <div class="small">{{ $cd_rate['eur']['diff'] }} <span class="uzs-size">UZS</span></div>
            </div>
            <div class="td" style="display: flex">
                <div >
                    @if($eur_take['bank']['image'])
                        <img class="widget-bank-logo" src="/{{ $eur_take['bank']['image'] }}" />
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($eur_take['take'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $eur_take['bank']['name_uz'] }}</div>
                </div>

            </div>
            <div class="td" style="display: flex">
                <div >
                    @if($eur_sale['bank']['image'])
                        <img class="widget-bank-logo" src="/{{ $eur_sale['bank']['image'] }}" />
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($eur_sale['sale'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $eur_sale['bank']['name_uz'] }}</div>
                </div>

            </div>
        </div>

        <div class="tr new-width">
            <div class="td">
                <div class="font-800">RUB</div>
            </div>
            <div class="td">
                <div>{{ number_format($cd_rate['rub']['rate'],2) }} <span class="uzs-size">UZS</span></div>
                <div class="small">{{ $cd_rate['rub']['diff'] }} <span class="uzs-size">UZS</span></div>
            </div>
            <div class="td" style="display: flex">
                <div >
                    @if($rub_take['bank']['image'])
                        <img class="widget-bank-logo" src="/{{ $rub_take['bank']['image'] }}" />
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($rub_take['take'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $rub_take['bank']['name_uz'] }}</div>
                </div>

            </div>
            <div class="td" style="display: flex">
                <div >
                    @if($rub_sale['bank']['image'])
                        <img class="widget-bank-logo" src="/{{ $rub_sale['bank']['image'] }}" />
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($rub_sale['sale'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $rub_sale['bank']['name_uz'] }}</div>
                </div>

            </div>
        </div>
       
        <div class="tr new-width">
            <div class="td">
                <div class="font-800">GBP</div>
            </div>
            <div class="td">
                <div>{{ number_format($cd_rate['gbp']['rate'],2) }} <span class="uzs-size">UZS</span></div>
                <div class="small">{{ $cd_rate['gbp']['diff'] }} <span class="uzs-size">UZS</span></div>
            </div>
            <div class="td" style="display: flex">
                <div >
                    @if($eur_take['bank']['image'])
                        <img class="widget-bank-logo" src="/{{ $gbp_take['bank']['image'] }}" />
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($gbp_take['take'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $gbp_take['bank']['name_uz'] }}</div>
                </div>

            </div>
            <div class="td" style="display: flex">
                <div >
                    @if($eur_sale['bank']['image'])
                        <img class="widget-bank-logo" src="/{{ $gbp_sale['bank']['image'] }}" />
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($gbp_sale['sale'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $gbp_sale['bank']['name_uz'] }}</div>
                </div>

            </div>
        </div>
        
        <div class="tr new-width">
                <div class="td">
                    <div class="font-800">CHF</div>
                </div>
                <div class="td">
                    <div>{{ number_format($cd_rate['chf']['rate'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $cd_rate['chf']['diff'] }} <span class="uzs-size">UZS</span></div>
                </div>
                <div class="td" style="display: flex">
                    <div >
                        @if($eur_take['bank']['image'])
                            <img class="widget-bank-logo" src="/{{ $chf_take['bank']['image'] }}" />
                        @else
                            <img class="widget-bank-logo" src="/temp/images/bank.png" />
                        @endif
                    </div>
                    <div>
                        <div>{{ number_format($chf_take['take'],2) }} <span class="uzs-size">UZS</span></div>
                        <div class="small">{{ $chf_take['bank']['name_uz'] }}</div>
                    </div>

                </div>
                <div class="td" style="display: flex">
                    <div >
                        @if($eur_sale['bank']['image'])
                            <img class="widget-bank-logo" src="/{{ $chf_sale['bank']['image'] }}" />
                        @else
                            <img class="widget-bank-logo" src="/temp/images/bank.png" />
                        @endif
                    </div>
                    <div>
                        <div>{{ number_format($chf_sale['sale'],2) }} <span class="uzs-size">UZS</span></div>
                        <div class="small">{{ $chf_sale['bank']['name_uz'] }}</div>
                    </div>

                </div>
        </div>

        <div class="tr new-width">
            <div class="td">
                <div class="font-800">JPY</div>
            </div>
            <div class="td">
                <div>{{ number_format($cd_rate['jpy']['rate'],2) }} <span class="uzs-size">UZS</span></div>
                <div class="small">{{ $cd_rate['jpy']['diff'] }} <span class="uzs-size">UZS</span></div>
            </div>
            <div class="td" style="display: flex">
                <div >
                    @if($eur_take['bank']['image'])
                        <img class="widget-bank-logo" src="/{{ $jpy_take['bank']['image'] }}" />
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($jpy_take['take'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $jpy_take['bank']['name_uz'] }}</div>
                </div>

            </div>
            <div class="td" style="display: flex">
                <div >
                    @if($eur_sale['bank']['image'])
                        <img class="widget-bank-logo" src="/{{ $jpy_sale['bank']['image'] }}" />
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($jpy_sale['sale'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $jpy_sale['bank']['name_uz'] }}</div>
                </div>
            </div>
        </div>

        <div class="tr new-width">
            <div class="td">
                <div class="font-800">KZT</div>
            </div>
            <div class="td">
                <div>{{ number_format($cd_rate['kzt']['rate'],2) }} <span class="uzs-size">UZS</span></div>
                <div class="small">{{ $cd_rate['kzt']['diff'] }} <span class="uzs-size">UZS</span></div>
            </div>
            <div class="td" style="display: flex">
                <div >
                    @if($eur_take['bank']['image'])
                        <img class="widget-bank-logo" src="/{{ $kzt_take['bank']['image'] }}" />
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($kzt_take['take'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $kzt_take['bank']['name_uz'] }}</div>
                </div>

            </div>
            <div class="td" style="display: flex">
                <div >
                    @if($eur_sale['bank']['image'])
                        <img class="widget-bank-logo" src="/{{ $kzt_sale['bank']['image'] }}" />
                    @else
                        <img class="widget-bank-logo" src="/temp/images/bank.png" />
                    @endif
                </div>
                <div>
                    <div>{{ number_format($kzt_sale['sale'],2) }} <span class="uzs-size">UZS</span></div>
                    <div class="small">{{ $kzt_sale['bank']['name_uz'] }}</div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
@endsection