@extends('layouts.app-telegram')
@section('title')
    @lang('lang.currency_exchange_rate')
@endsection
@section('metaKeyWords')
   <meta name="keywords" content="valyuta, valyuta kurslari, курсы валют, в Узбекистане, Tashkent" />
@endsection
@section('content')
<style type="text/css" media="screen">
    .medium-container {
        max-width: 1300px !important;
    }
    .p-0{
        padding:0px !important;
    }
    .td .logo{
        max-width: 13% !important;
    }
</style>
    {{-- <div class="section-gap--small mt-20 mb-10">
        <div class="medium-container">
            
            <h2 class="b-title mb-20">@lang('lang.great_chance')</h2>
            <div class="col-md-12 text-right " style="margin-bottom: 5px">
                <div class="" style="display:-webkit-inline-box; font-size: 12px; color: #3aa5d0">
                    <div style="margin-right: 8px">
                        <span >{{ date('d/M/Y',strtotime(date('Y-m-d'))) }}</span><br>
                        <span >@lang('lang.update_time')</span>
                    </div>
                    <img src="/temp/images/update_time.png" width="25" />
                </div>
            </div>
            @widget("currency")
        </div>
    </div> --}}

    <div class="section-gap--small mt-20">
        <div class="medium-container section-gap--small mt-20 mb-20">
            <div class="col-md-12 d-flex p-0">
                <div class="col-md-4 p-0">
                    <img src="/temp/images/logo.svg" style="" width="270px" alt="Depozit.uz">
                </div>
                <div class="col-md-4 p-0">
                     <h2 class="b-title" style="padding-top: 15px">@lang('lang.title_rate_telegram_table')&nbsp;{{ $currencyName->name }}/UZS</h2>
                </div>
                <div class="col-md-4 p-0 text-right">
                     <div class="" style="display:-webkit-inline-box; font-size: 15px; color: #3aa5d0">
                    <div style="margin-right: 8px">
                        @php
                            $time =  $best_rate_update_time['data_update_time'];
                            if(!$time){
                                $time = date('Y-m-d h:m');
                            }
                        @endphp 
                        <span >{{ date('d/M/Y H:i',strtotime($time)) }}</span><br>
                        <span >@lang('lang.update_time')</span>
                    </div>
                    <img src="/temp/images/update_time.png" width="35" />
                </div>
                </div>
            </div>
             
           
            {{--  <div class="col-md-12 text-right " style="right: 39px;
                    margin-bottom: 5px;
                    position: absolute; top: 28px;">
               
            </div> --}}
        </div>
        <div class="medium-container p-0">
        <div class="col-md-12 pl-0 pr-0 d-flex">
            <div class=" col-md-6">
                <div class="table calculator-result">
                    <div class="thead">
                        <div class="tr" style="font-size: 20px">
                            <div class="th" style="width: 70%">@lang('lang.banks')</div>
                            <div class="th" style="width: 30%; font-weight: bold;">@lang('lang.sale')</div>
                          
                        </div>
                    </div>
                    <div class="tbody" id="exchange-ajax-content">
                        <style>
                            .style-type-exchange-1 span:nth-child(1){
                                margin-left: 5px;
                            }.style-type-exchange span:nth-child(1){
                                margin-left: 7px;
                            }
                           
                        </style>
                      
                            @foreach($exchange_take as $row)
                                <div class="tr pt-0 pb-0">
                                    <div class="td d-flex align-items-center currency-img-bank" style="width: 68%">
                                        <div class="logo"  style="height: 40px !important">
                                            <img  style="max-height: 40px !important;" src="/{{ $row->bank->image }}">
                                        </div>
                                        <span class="name pl-0 currency-name-bank" ><h1 class="b-title" style="font-size: 28px; font-weight: 500; margin-bottom: 0px">{{ $row->bank->name_uz }}</h1></span>
                                    </div>
                                    
                                    <div class="td d-flex" style="width: 32% !important">
                                        <div style="font-size: 28px"> {{ number_format($row->take) }}&nbsp;<small style="font-size: 50%;">UZS</small></div>
                                       {{--  <div class="small">@lang('lang.course_c') {{ $currencyName->name }}</div> --}}
                                        <div class="small" style="padding-left: 5px">
                                            @if(count($row->otherRates()) > 0)
                                                @php $star = ''; $after = '*';@endphp
                                                <div class="other-rate style-type-exchange-1 pt-0" style="font-size: 14px">
                                                    @foreach($row->otherRates() as $other)
                                                        @php $star.=$after; @endphp
                                                        @if($other->take > 0)
                                                            <span>{{ $star }} {{ number_format($other->take) }}&nbsp;<small style="font-size: 50%;">UZS</small></span><br>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="tr">
                                <div class="style-type-exchange">
                                    @php $stars = ' '; $afters = '*';@endphp
                                    @foreach($otherExchangeType as $type)
                                        <span>{{ $stars.= $afters }}{{ $type->name() }}; &nbsp;</span><br>
                                    @endforeach
                                </div>

                            </div>
                        </div>
                </div>
            </div>

            <div class="col-md-6">
                 <div class="table calculator-result">
                    <div class="thead">
                        <div class="tr " style="font-size: 20px">
                            <div class="th" style="width: 70%">@lang('lang.banks')</div>
                          
                            <div class="th" style="width: 30%; font-weight: bold;">@lang('lang.take')</div>
                            
                        </div>
                    </div>
                    <div class="tbody" id="exchange-ajax-content">
                        <style>
                            .style-type-exchange-1 span:nth-child(1){
                                margin-left: 5px;
                            }.style-type-exchange span:nth-child(1){
                                margin-left: 7px;
                            }
                            .logo img{
                                margin-right: 5px !important;
                                margin-left: 5px !important;
                            }
                           
                        </style>
                      
                            @foreach($exchange_sale as $row)
                                <div class="tr pt-0 pb-0">
                                    <div class="td d-flex align-items-center currency-img-bank" style="width: 68%">
                                        <div class="logo"  style="height: 40px !important">
                                            <img  style="max-height: 40px !important;" src="/{{ $row->bank->image }}">
                                        </div>
                                        <span class="name pl-0 currency-name-bank" ><h1 class="b-title" style="font-size: 28px;font-weight: 500; margin-bottom: 0px">{{ $row->bank->name_uz }}</h1></span>
                                    </div>
                                 
                                    <div class="td d-flex" style="width: 32% !important">
                                        <div style="font-size: 28px"> {{ number_format($row->sale) }}&nbsp;<small style="font-size: 50%;">UZS</small></div>
                                       {{--  <div class="small">@lang('lang.course_c') {{ $currencyName->name }}</div> --}}
                                        <div class="small" style="padding-left: 5px">
                                            @if(count($row->otherRates()) > 0)
                                                @php $star = ''; $after = '*';@endphp
                                                <div class="other-rate style-type-exchange-1 pt-0" style="font-size: 14px">
                                                    @foreach($row->otherRates() as $other)
                                                        @php $star.=$after; @endphp
                                                        @if($other->sale  > 0)
                                                            <span>{{ $star }} {{ number_format($other->sale) }}&nbsp;<small style="font-size: 50%;">UZS</small></span><br>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                  
                                 
                                </div>
                            @endforeach 
                            <div class="tr"></div>

                            {{-- <div class="tr">
                                <div class="style-type-exchange">
                                    @php $stars = ' '; $afters = '*';@endphp
                                    @foreach($otherExchangeType as $type)
                                        <span>{{ $stars.= $afters }}{{ $type->name() }}; &nbsp;</span><br>
                                    @endforeach
                                </div>

                            </div> --}}
                        </div>
                </div>
            </div>
        </div>

        </div>
    </div>
@endsection

@section('script')
   
@endsection

