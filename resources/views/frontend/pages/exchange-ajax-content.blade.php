<style>
    .style-type-exchange-1 span:nth-child(1){
        margin-left: 5px;
    }.style-type-exchange span:nth-child(1){
        margin-left: 7px;
    }
</style>
@if(count($data['exchange'] ) > 0)
    @foreach($data['exchange'] as $row)
        <div class="tr pb-0 pt-0">
            <div class="td d-flex align-items-center currency-img-bank" >
                <div class="logo d-flex">
                    <img src="/{{ $row->bank->image }}">
                </div>
                <span class="name pl-0 currency-name-bank" ><h1 class="b-title">{{ $row->bank->name_uz }}</h1></span>
            </div>
            <div class="td" style="width: 24.3%; padding-top: 7px; padding-bottom: 7px;">
                <div> {{ number_format($row->$action,2) }} <span style="font-size: 50%">UZS</span></div>
                <div class="small">@lang('lang.course_c') {{ $currencyName->name }}</div>
                <div class="small">
                    @if(count($row->otherRates()) > 0)
                        @php $star = ''; $after = '*';@endphp
                        <div class="other-rate style-type-exchange-1" style="font-size: 14px">
                            @foreach($row->otherRates() as $other)
                            <span>{{ $star.=$after }} {{ number_format($other->$action,2) }} UZS</span><br>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="td" style="width: 24.3%">
                <div> {{ number_format($row->calc($action,$quantum),2)}} <span style="font-size: 50%">UZS</span></div>
                <div class="small">@lang('lang.quantum')</div>
            </div>
            {{-- <div class="td">
                <div>{{ date('d.m.Y',strtotime($row->updated_at)) }}</div>
                <div class="small">@lang('lang.time')</div>
            </div> --}}
            <div class="td"><a target="_blank" href="{{ $row->bank->information->site() }}" class="btn btn--small btn--blue">@lang('lang.detailed')</a>
            </div>
        </div>
    @endforeach
    <span class="count-page-items" style="display: none">{{ $data['exchange']->count() }}</span>
@else
<tr class="show-rate text-center" data-id="">
    <td colspan="5" class="text-center">
        <div class="card-body text-center pb-3 col-md-12  mb-25 mt-25">
            <h3 class="mt-3 mb-3">@lang('lang.not_found')</h3>
        </div>
    </td>
</tr>
<span class="count-page-items" style="display: none">0</span>
@endif