@extends('layouts.app')
@section('meta')

<title>{{ $new->title() }}</title>
<meta name="description" content="{{ $new->getDescription() }}"/>

<meta name="keywords" content="{{ $new->getKeywords() }}"/>

  {{--   @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection --}}
@section('content')
<link rel="stylesheet" type="text/css" href="/css/news-section.css">
 <div class="section">
 	<div class="section-gap mt-50">
 		<div class="medium-container pr-0 pl-0">
 			<div class="col-md-12 mr-0 ml-0 pr-0 pl-0 row">
 				<div class="col-md-12 pr-0 pl-0 mb-20">
 					<div class="col-md-12"> 
 						{{-- <span><i class="fa fa-user-o"></i> </span>
 						<span>{{ $new->user->name }}@lang('lang.news_admin')</span>&nbsp;|&nbsp; --}}
 						<span><i class="fa fa-clock-o"></i></span>
 						<span>{{ date('d M Y',strtotime($new->created_at)) }}</span>&nbsp;|&nbsp;
 						<span><i class="fa fa-eye"></i></span>
 						<span>{{ number_format($new->views)}}</span>
 					</div>
 				</div>
	 			<div class="col-md-8 mb-40 pr-0 pl-0">
		 			<div class="col-md-12"><h1 class="b-title text-left b-new-title">{{ $new->title() }}</h1></div>
		 			<div class="col-md-12">
		 				<div class="news-img-large"><img src="/{{ $new->image }}" alt=""></div>
		 			</div>
		 			<div class="col-md-12">
		 				<div class="news-text">
		 					<p>
		 						@php echo html_entity_decode($new->text()); @endphp
		 					</p>
		 				</div>
		 			</div>

		 			<div class="col-md-12 mt-20">
		 				<a target="_blank" href="{{ $new->link()}}" class="read-news" href="">@lang('lang.read_detailed')&nbsp;<i class="fa fa-external-link"></i></a>
		 			</div>
					@if($new->new_category_id)
		 				<div class="col-md-12 mt-20">
		 					<h3 class="mb-10"><i class="fa fa-tags"></i>&nbsp;@lang('lang.tags')</h3>
		 						@foreach($new->categories() as $row_cat)	
		 						<a href="{{ route('news-all-page',['locale'=>App::getLocale(),'category'=>$row_cat->id]) }}" class="category" style="padding:10px;font-size:16px">{{ $row_cat->name() }}</a>
		 						@endforeach
		 				</div>
 					@endif
		 		</div>

		 		<div class="col-md-4 pr-0 pl-0">
		 			@if(count($news) > 0)
		 			@foreach($news as $row)
			 			<div class="col-md-12 mb-30">
							<div class="news-img"><img src="/{{ $row->image }}" alt=""></div>
							<div class="news-title"><a href="{{ route('news-page',['locale'=>App::getLocale(),'slug'=>$row->slug,'id'=>$row->id]) }}">{{ $row->title() }}</a></div>
						</div>
					@endforeach
					<div class="col-md-12 mb-30 text-right news-pagination">
						{{ $news->links() }}
					</div>
					@endif

					<div class="col-md-12 mb-5 mt-30">
						<h4><i class="fa fa-tags"></i>&nbsp;@lang('lang.tags')</h4>
					</div>

					<div class="col-md-12 " >
						<div class="col-md-12 category-content" >
							<a  href="{{ route('news-all-page',['locale'=>App::getLocale()])}}" class="category">@lang('lang.all')</a>
							@if(count($categories) > 0)
								@foreach($categories as $category)
								<a  href="{{ route('news-page',['locale'=>App::getLocale(),'category'=>$category->id]) }}" class="category">
									{{ $category->name()}}
								</a>
								@endforeach
							@endif
						</div>
					</div>
		 		</div>

		 	</div>
 		</div>
 	</div>
</div>
@endsection