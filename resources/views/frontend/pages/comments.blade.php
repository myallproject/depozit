@extends('layouts.app')
@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('content')
    <style>
        .pagination>li>a,.pagination>li>span {
            //background-color: unset !important;
            border: none !important;
        }
    </style>
    <section class="page-title">
        <div class="image-layer" ></div>
        <div class="auto-container">
            <h1>@lang('lang.users_comments')</h1>
            <div class="bread-crumb">
                <ul class="clearfix">
                    <li><a href="{{ route('home') }}">@lang('lang.home')</a></li>
                    <li class="active">@lang('lang.leave_comment')</li>
                </ul>
            </div>
        </div>
    </section>


    <section class="testimonials-three pt-0 pb-2">
        <div class="auto-container">
            {{--<div class="sec-title centered">
                <h1>@lang('lang.what_say_user')</h1>
                <div class="text">Lorem ipsum dolor sit amet consectetur adipiscing elit</div>
            </div>--}}
            <div class="outer-box mr-0 ml-0 ">
                <div class="clearfix">
                    @if(count($comments) <= 0)
                        <div class="text-center">
                            <h1>@lang('lang.not_found')</h1>
                        </div>
                    @else
                        @foreach($comments as $comment)
                        <div class="col-md-6 mb-4 col-sm-12 col-xs-12 mb-5">
                            <!--Testimonial Block-->
                            <div class="testimonial-block">
                                <div class="inner">
                                    <div class="text mb-2">{{ $comment->text }}</div>
                                    <div class="info">
                                        <figure class="image img-circle"><img class="img-circle" src="images/noavatar2.png" alt=""></figure>
                                        <h4>{{ $comment->name }}</h4>
                                        <div class="designation">@lang('lang.user_site')</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @endif
                </div>

            </div>
            <div class="load-more mb-5" style="font-family:Arial, Helvetica, sans-serif !important;">
                {{ $comments->links() }}
            </div>

{{--
            <div class="load-more mb-5">
                <a  id="more-comment">
                    <span class="fa fa-angle-double-down"></span> &nbsp;@lang('lang.see_more_comment')
                </a>
            </div>


            <div class="outer-box mt-5">
                <div class="clearfix" id="more-comment-data">

                </div>

            </div>
--}}

        </div>
    </section>


    <div class="callback-box">
        <div class="callback-header">
            <h2>@lang('lang.send_your_comment')</h2>
            <div class="text"></div>
        </div>
        <div class="form-box">
            <!--Callback Form-->
            <div class="form-style-two callback-form">
                <form method="post" action="{{ route('comment') }}">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="name" value="" placeholder="@lang('lang.username')" required>
                    </div>

                    <div class="form-group">
                        <textarea rows="5" name="text" value="" placeholder="@lang('lang.your_comment')" required></textarea>
                    </div>

                    <div class="form-group">
                        <div class="text-center">
                            <button type="submit" class="theme-btn btn-style-two">@lang('lang.send')</button>
                        </div>
                    </div>

                </form>
            </div><!--End Callback Form-->
        </div>
    </div>



    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content  b-r-0">
                <div class="modal-header  bg-info text-white" >
                    <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body  text-center ">
                    <p><h2>
                        @lang('lang.thanks_your_comment')
                    </h2>
                    </p>
                </div>

            </div>
        </div>
    </div>

    <button    style="display:none !important; "  type="button"  data-toggle="modal" data-target="#myModal" class="btn btn-danger show-modal-btn border-0 pt-2 bg-color-red pl-4 shadow btn-hover pr-4 text-white text-uppercase " style="border-radius: 0px!important">
    </button>

@endsection

@section('script')
    @if(Session::has('success'))
        <script>
            $(document).ready(function(){
                $('.show-modal-btn').click();
            });
        </script>
    @endif

    <script>
        $(document).ready(function() {
           /* $('#more-comment').on('click',function () {

               $.ajax({
                    method: "GET",
                    url : "",
                    success:function (data) {
                    $('#more-comment-data').html(data);
                 }
               });
            });*/
        });
    </script>
@endsection
