@extends('layouts.app')

@section('meta')
     @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('css')
    <style>
        .min-banner__img{
            min-height: 320px;
        }
        @media (max-width: 768px){
            .min-banner__img#goal{
                background-image:url(/temp/images/banner/digital_tree_mobile.jpg);
            }
        }
        @media (min-width: 769px){
            .min-banner__img#goal{
                background-image:url(/temp/images/banner/digital_tree_lg.jpg);
            }
        } 
    </style>
@endsection
@section('content')
    <div class="min-banner">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="min-banner__img" id="goal">
                        <div class="medium-container text-uppercase">
                            <div class="col-md-8 head-banner">
                                <h2 class="slide2_text">@lang('lang.slide2_text')</h2>
                                <div class="banner-footer text-lowercase">
                                    <a href="https://t.me/DepozitUzb" class="btn-banner-social-link" target="_blank"><i class="fa fa-paper-plane fa-lg"></i><span>t.me/depozituzb</span></a>
                                    <a href="https://www.facebook.com/depozit.uz/" class="btn-banner-social-link" target="_blank"><i class="fa fa-facebook-official fa-lg"></i><span>@depozit.uz</span></a>
                                    <a href="https://www.instagram.com/depozit.uz/" class="btn-banner-social-link" target="_blank"><i class="fa fa-instagram fa-lg"></i><span>@depozit.uz</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="min-banner__img" style="background-image:url(/temp/images/content/banner.png)">
                        <div class="medium-container text-uppercase">
                            <h2>@lang('lang.mission_site')</h2>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="min-banner__img" style="background-image:url(/temp/images/content/banner.png)">
                        <div class="medium-container text-uppercase">
                            <h2>@lang('lang.real_economy_text')</h2>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="min-banner__img" style="background-image:url(/temp/images/content/banner.png)">
                        <div class="medium-container text-uppercase">
                            <h2>@lang('lang.about_site')</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="min-banner__wrapper">
                <div class="medium-container">
                    {{-- <div class="swiper-pagination"></div> --}}
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="quote section-gap">
        <div class="quote__container" style="font-weight: 900;">@lang('lang.about_site')</div>
    </div>
    <div class="services section-gap">
        <div class="medium-container">
            <h2 class="b-title">@lang('lang.our_offer')</h2>
            <div class="arrow-card">
                <div class="content">
                    <div class="num">01</div>
                    <div class="text">
                        <h3>@lang('lang.deposits')</h3>
                        <p>@lang('lang.about_us_deposit')</p>
                    </div>
                </div>
            </div>
            <div class="arrow-card arrow-card--reverse">
                <div class="content">
                    <div class="num">02</div>
                    <div class="text">
                        <h3>@lang('lang.eat_credits')</h3>
                        <p>@lang('lang.about_us_consumer_c')</p>
                    </div>
                </div>
            </div>
            <div class="arrow-card">
                <div class="content">
                    <div class="num">03</div>
                    <div class="text">
                        <h3>@lang('lang.auto_credit')</h3>
                        <p>@lang('lang.about_us_auto_c')</p>
                    </div>
                </div>
            </div>
            <div class="arrow-card arrow-card--reverse">
                <div class="content">
                    <div class="num">04</div>
                    <div class="text">
                        <h3>@lang('lang.credit_mortgage')</h3>
                        <p>@lang('lang.about_us_mortgage_c')</p>
                    </div>
                </div>
            </div>
            <div class="arrow-card">
                <div class="content">
                    <div class="num">05</div>
                    <div class="text">
                        <h3>@lang('lang.micro_debt')</h3>
                        <p>@lang('lang.about_us_micor_c')</p>
                    </div>
                </div>
            </div>
            <div class="arrow-card arrow-card--reverse">
                <div class="content">
                    <div class="num">06</div>
                    <div class="text">
                        <h3>@lang('lang.education_credit')</h3>
                        <p>@lang('lang.about_us_edu_c')</p>
                    </div>
                </div>
            </div>
            <div class="arrow-card">
                <div class="content">
                    <div class="num">07</div>
                    <div class="text">
                        <h3>@lang('lang.currency_exchange_rate')</h3>
                        <p>@lang('lang.about_us_currancy')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection