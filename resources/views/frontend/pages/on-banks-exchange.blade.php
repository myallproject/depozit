@extends('layouts.app')

@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('content')

    <div class="section-gap--small mt-20 mb-10">
        <div class="medium-container">
            <h2 class="b-title mb-30">@lang('lang.great_chance')</h2>
            @widget("currency")
        </div>
    </div>

    <div class="section-gap--small mt-20">
        <div class="medium-container section-gap--small mt-20 mb-20">
            <h2 class="b-title">@lang('lang.currency_exchange_rate')</h2>
            <div class="breadcrumb_ breadcrumb_--center">
                <a href="{{ route('on-bank-exchange-rate',['locale'=>App::getLocale()]) }}" class="active">@lang('lang.calc_currency')</a>
                <a href="{{ route('cb-exchange-rate',['locale'=>App::getLocale()]) }}" style="text-transform: uppercase" >@lang('lang.cbu')</a>
            </div>
        </div>
        <div class="medium-container">
            <div class="calculator-form mb-30">
                <form action="{{ route('exchange-ajax-content',['locale'=>App::getLocale()]) }}" id="exchange-form" method="GET">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>@lang('lang.process')</label>
                                <select name="action" id="action-exchange" class="select2__js exchange-filter select-action" >
                                    <option value="take">@lang('lang.take')</option>
                                    <option value="sale">@lang('lang.sale')</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>@lang('lang.quantum')</label>
                                <input class="exchange-filter" id="sum-exchange"  type="text" name="quantum" value="100" placeholder="@lang('lang.quantum')">
                                <span id="quantum-error" class="text-danger" style="display: none"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>@lang('lang.currency')</label>
                                <select name="currency" id="currency-form" class="exchange-filter select2__js">
                                    @foreach( $currencies as $currency)
                                        @if($currency->name != 'UZS')
                                            <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="table calculator-result">
                <div class="thead">
                    <div class="tr">
                        <div class="th">@lang('lang.banks')</div>
                        <div class="th" id="action"></div>
                        <div class="th">
                            {{--<div class="form-group">
                                <input type="text" id='search-bank' placeholder="@lang('lang.search_bank')" class="form-control small">
                            </div>--}}
                        </div>
                    </div>
                </div>
                <div class="tbody" id="exchange-ajax-content">
                    @include('frontend.pages.exchange-ajax-content')
                </div>
                <div class="tfoot text-center ">
                    <div class="tr">
                        <div class="style-type-exchange">
                            @php $stars = ' '; $afters = '*';@endphp
                            @foreach($otherExchangeType as $type)
                                <span>{{ $stars.= $afters }}{{ $type->name() }}; &nbsp;</span><br>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
            <div class="text-center">
                  <a class="btn-xn btn-xn-default show-more-item" style="display: inline-block;" data-status="true" data-page-next="2" data-page-current="1" data-href="{{ route('exchange-ajax-content',['locale'=>App::getLocale()]) }}">@lang('lang.other_banks')&nbsp;<i class="fa fa-angle-down"></i></a>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).on('click', '.show-more-item', function(e) {
            if($(this).attr('data-status') == 'true'){
                serch_form = $('#exchange-form');
                next = $(this).attr('data-page-next');
                current = $(this).attr('data-page-current');
                page_url = $(this).attr('data-href');
                e.preventDefault();
                paginate(page_url,next,serch_form.serialize());
            }
        });

        function paginate(page_url,page,search) {
             $('.show-more-item').attr('data-status', false);
            $.ajax({
                url: page_url+'&page=' + page,
                dataType: 'json',
                data:search,
            }).done(function(data){
                next = $('.show-more-item').attr('data-page-next');
                $('.show-more-item').attr('data-page-current',next);
                $('.show-more-item').attr('data-page-next', (Number(next)+1));
                $('.show-more-item').attr('data-status', true);
                //$('#exchange-ajax-content').html('');
                $('#exchange-ajax-content').append(data);
                count = $('.count-page-items').last().text();
                if(Number(count) < 3 ){
                    $('.show-more-item').hide();
                }
                location.hash = page;
            });
        }

        $('#exchange-form').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $('.select-action').change(function(){
            if($(this).val() == 'take'){
                action = "@lang('lang.take')";
            }if($(this).val() == 'sale'){
                action = "@lang('lang.sale')";
            }
            $('#action').text(action);
        });

        let  select_action = $('.select-action').val();
        if(select_action == 'take'){
            action = "@lang('lang.take')";
        }if(select_action == 'sale'){
            action = "@lang('lang.sale')";
        }
        $('#action').text(action);

        $(document).ready(function(){
            let currency_form = $('#exchange-form');
            $('.exchange-filter').change(function(e){
                e.preventDefault();
                $.ajax({
                    type:currency_form.attr('method'),
                    url: currency_form.attr('action'),
                    data:currency_form.serialize(),
                    success:function(data){
                        if(data.error){
                            $.each(data.error,function(i,v){
                                  $('#'+i+'-error').text(v).show()
                            });
                        } else {
                            $('#quantum-error').hide();
                            $('#exchange-ajax-content').html(data);
                        }
                        $('.show-more-item').attr('data-page-current',1);
                        $('.show-more-item').attr('data-page-next', 2);
                            count= $('.count-page-items').last().text();
                            if(Number(count) < 3 ){
                                $('.show-more-item').hide();
                            } else {
                                $('.show-more-item').show();
                            }
                    }
                });
            });


            $('.menu-currency').addClass('current');
        });
    </script>
@endsection

