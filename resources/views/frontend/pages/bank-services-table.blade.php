@extends('layouts.app')
@section('title')
    @lang('lang.banks_serices_table')
@endsection
@section('meta')
     @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('content')
<style type="text/css" media="screen">
	.class-th-0{
		width: 30px;
	}
	.class-th-1{
		width: 180px;
	}
	.class-th-2{
		width: 300px;
	}
	.class-th-3{
		width: 350px;
	}
	.class-th-4{
		width: 180px;
	}
	.class-th-5{
		width: 130px;
	}
	.class-th-6{
		width:165px;
	}
	.class-th-7{
		width:200px;
	}
	.class-th-8{
		width: 160px;		
	}
	.head-tr td{
		border-top:none;
	}
	.body-tr:hover{
		background-color: aliceblue;
	}
	.class-td-2,.class-td-3, .class-td-4, .class-td-5, .class-td-6, .class-td-7, .class-td-8, .class-th-2,.class-th-3,.class-th-4,.class-th-5,.class-th-6,.class-th-7,.class-th-8{
		text-align: left;
	}
	@media (max-width: 1400px){
		.medium-container {
	    	max-width: 1315px;
		}		
	}

	
</style>
	<div class="section">
	    <div class="partners section-gap mt-10">
	        <div class="medium-container">
	            <div class="filter-result mt-30">
	                <div class="filter-result__title pb-10">
	                    <h1 class="b-title mb-10">@lang('lang.banks_serices_table')</h1>
	                </div>
	                <div class="col-md-12 table-responsive">
	                	<table class="table-service">
	                		<tbody>

								<tr style="font-weight: 700" class="head-tr">
									@php $head = $service_table[0][0]; @endphp
									
									@foreach($head as $hk => $hv)
										@if($hv)
											<td class="class-th-{{ $hk }}">{{ $hv }}</td>
										@endif
									@endforeach
								</tr>

									@php unset($service_table[0][0]);$body = $service_table[0]; @endphp
									
									@foreach($body as $ktr => $vtr)
										<tr class="body-tr">
											@foreach($vtr as $ktd => $vtd)
												@if($vtd)
													<td class="class-td-{{ $ktd }}">
														@php echo html_entity_decode($vtd); @endphp
													</td>
												@endif
											@endforeach
										</tr>
									@endforeach
								
	                		</tbody>
	                	</table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection