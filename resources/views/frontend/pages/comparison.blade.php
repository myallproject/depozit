@extends('layouts.app')
@section('meta')
     @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('content')
    <div class="section">
        <div class="exchange-list">
            <div class="medium-container section-gap--small">
            <div class="m-title text-center">
                <h2>@lang('lang.comparison')</h2>
            </div>
            <br>
            <div class="" style="overflow-x: auto">
                <table id="comparison-table">
                    <thead>
                    <tr>
                        @if(count($deposits) > 0)
                            <th>#</th>
                            <th>@lang('lang.name')</th>
                            <th>@lang('lang.min_sum')</th>
                            <th>@lang('lang.currency')</th>
                            <th>@lang('lang.percent')</th>
                            <th>@lang('lang.date')</th>
                            <th>@lang('lang.bank')</th>
                            <th>@lang('lang.delete')</th>
                        @elseif(count($credits) > 0)
                            <th>#</th>
                            <th>@lang('lang.name')</th>
                            <th>@lang('lang.min_sum')</th>
                            <th>@lang('lang.currency')</th>
                            <th>@lang('lang.percent')</th>
                            <th>@lang('lang.type')</th>
                            <th>@lang('lang.bank')</th>
                            <th>@lang('lang.delete')</th>
                        @elseif(count($credit_cards) > 0)
                            <th>#</th>
                            <th>@lang('lang.name')</th>
                            <th>@lang('lang.card_limit')</th>
                            <th>@lang('lang.date')</th>
                            <th>@lang('lang.percent_rate')</th>
                            <th>@lang('lang.date_privilege')</th>
                            <th>@lang('lang.bank')</th>
                            <th>@lang('lang.delete')</th>
                        @elseif(count($debit_cards) > 0)
                            <th>#</th>
                            <th>@lang('lang.name')</th>
                            <th>@lang('lang.type')</th>
                            <th>@lang('lang.card_price')</th>
                            <th>@lang('lang.currency')</th>
                            <th>@lang('lang.payment')</th>
                            <th>@lang('lang.bank')</th>
                            <th>@lang('lang.delete')</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody id="body-tr">
                    @if(count($deposits) > 0)
                        @php $i = 0; @endphp
                        @foreach($deposits as $deposit)
                            <tr id="remove-d-{{ $deposit->id }}">
                                <td>@php echo $i += 1; @endphp</td>
                                <td><a href="{{ route('view-deposit',['locale' => App::getLocale(),'slug' => $deposit->slug]) }}">{{ $deposit->name() }}</a></td>
                                <td>{{ number_format($deposit->min_sum) }}</td>
                                <td>{{ $deposit->priceCurrency->name }}</td>
                                <td>{{ $deposit->deposit_percent }}%</td>
                                <td>
                                    @if(!$deposit->deposit_date_string)
                                        {{ $deposit->deposit_date." ".__('lang.day') }}
                                    @else
                                        {{ $deposit->dateStringLang(App::getlocale()) }}
                                    @endif
                                </td>
                                <td><a href="{{ route('info-bank',['locale' => App::getLocale(),'slug'=>$deposit->bank->slug]) }}">{{ $deposit->bank->name() }}</a></td>
                                <td class="">
                                    <a style="cursor: pointer" class="remove-deposit" data-id="{{ $deposit->id }}">
                                        <i class="fa fa-close"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @elseif(count($credits) > 0)
                        @php $i = 0; @endphp
                        @foreach($credits as $credit)
                            <tr id="remove-c-{{ $credit->id }}">
                                <td>@php echo $i += 1; @endphp</td>
                                <td><a href="{{ route('view-credit',['locale' => App::getLocale(),'slug'=>$credit->slug]) }}">{{ $credit->name() }}</a></td>
                                <td>{{ $credit->amount }}</td>
                                <td>{{ $credit->currencyName->name }}</td>
                                <td>{{ $credit->percent }}%</td>
                                <td>{{ $credit->parent->name() }}</td>
                                <td><a href="{{ route('info-bank',['locale' => App::getLocale(),'slug'=>$credit->bank->slug]) }}">{{ $credit->bank->name() }}</a></td>
                                <td class="text-right">
                                    <a style="cursor: pointer" class="remove-credit" data-id="{{ $credit->id }}">
                                        <i class="fa fa-close"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @elseif(count($credit_cards) > 0)
                        @php $i = 0; @endphp
                        @foreach($credit_cards as $cr_card)
                           {{-- <tr id="remove-c-c-{{ $credit->id }}">
                                <td>@php echo $i += 1; @endphp</td>
                                <td><a href="{{ route('view-credit',[$credit->slug]) }}">{{ $credit->name_uz }}</a></td>
                                <td>{{ $credit->amount }}</td>
                                <td>{{ $credit->currencyName->name }}</td>
                                <td>{{ $credit->percent }}%</td>
                                <td>{{ $credit->parent->name() }}</td>
                                <td><a href="{{ $credit->bank->slug }}">{{ $credit->bank->name_uz }}</a></td>
                                <td class="text-right">
                                    <a style="cursor: pointer" class="remove-credit-card" data-id="{{ $credit->id }}">
                                        <i class="fa fa-close"></i>
                                    </a>
                                </td>
                            </tr>--}}
                        @endforeach
                    @else
                        <tr class="text-center" style="background-color: #ffffff !important;">
                            <td colspan="8">@lang('lang.not_found')</td>
                        </tr>
                    @endif

                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $('.remove-deposit').on('click',function(){
            let id = $(this).data('id');
            $.ajax({
                url: "{{ route('remove-comparison') }}",
                data: {
                    id: id,
                    url_type: '{{ $url }}',
                },
                success:function(data){
                    $('#remove-d-'+id).remove();
                    $(".comparison-amount").text(data);
                    if(!data){
                        $('#body-tr').html("<tr class='text-center'><td colspan='8'>@lang('lang.not_found')</td></tr>")
                    }
                }
            });
        });

        $('.remove-credit').on('click',function(){
            let id = $(this).data('id');
            $.ajax({
                url: "{{ route('remove-comparison') }}",
                data: {
                    id: id,
                    url_type: '{{ $url }}',
                },
                success:function(data){
                    $('#remove-c-'+id).remove();
                    $(".comparison-amount").text(data);
                    if(!data){
                        $('#body-tr').html("<tr class='text-center'><td colspan='8'>@lang('lang.not_found')</td></tr>")
                    }
                }
            });
        });

        $('.remove-credit-card').on('click',function(){
            let id = $(this).data('id');
            $.ajax({
                url: "{{ route('remove-comparison') }}",
                data: {
                    id: id,
                    url_type: '{{ $url }}',
                },
                success:function(data){
                    $('#remove-c-c-'+id).remove();
                    $(".comparison-amount").text(data);
                    if(!data){
                        $('#body-tr').html("<tr class='text-center'><td colspan='8'>@lang('lang.not_found')</td></tr>")
                    }
                }
            });
        });

       /* $(function () {
            $('#comparison-table').DataTable({
                'paging'      : false,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : true
            })
        });*/
    </script>
@endsection
