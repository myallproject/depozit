@extends('layouts.app')
@section('meta')
       @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('content')
<link rel="stylesheet" type="text/css" href="/css/news-section.css">
 <div class="section">
 	<div class="section-gap mt-50">
 		<div class="medium-container pr-0 pl-0">
 			<div class="col-md-12 ">
 				<h1 class="b-title mt-20 mb-30">@lang('lang.news')</h1>
 			</div>
 			<div class="col-md-12 mr-0 ml-0 pr-0 pl-0 row">
 				<div class="col-md-9 news-all-content pr-0 pl-0 row mr-0 ml-0  news-items">
 					@if(count($news) > 0)
					@foreach($news as $new)
						<div class="col-md-4 mb-30">
							<div class="hover-box-shadow">
								<div class="news-img"><img src="/{{ $new->image }}" alt=""></div>
								<div class="news-title p-8"><a href="{{ route('news-page',['locale' => App::getLocale(),'slug'=>$new->slug,'id'=>$new->id]) }}">{{ mb_substr($new->title(), '0',60,'UTF-8') }}...</a></div>
							</div>
						</div>
					@endforeach

					<div class="col-md-12 mb-30 text-right news-pagination">
						{{ $news->links() }}
					</div>
					@else
						<div class="col-md-12 mb-30 text-center">
							<div class="col-md-12">
								<img src="/temp/images/data_not_found.png" width="200" alt="">
							</div>
							<div class="col-md-12 ">
								<h3 class="mt-0">@lang('lang.news_not_found')</h3>
							</div>
						</div>

					@endif
 				</div>
 				
				<div class="col-md-3 ">
					<h3 class="mb-10 mt-0"><i class="fa fa-tags"></i>&nbsp;@lang('lang.tags')</h3>
					<div class="col-md-12 pr-10 pl-10 category-content" >
					<a  href="{{ route('news-all-page',['locale' => App::getLocale()])}}" class="category">@lang('lang.all')</a>
						@if(count($categories) > 0)
							@foreach($categories as $category)
							<a  href="{{ route('news-all-page',['locale' => App::getLocale(),'category'=>$category->id]) }}" class="category">
								{{ $category->name()}}
							</a>
							@endforeach
						@endif
					</div>
				</div>	
		 	</div>

		 	<div class="col-md-12 mr-0 ml-0 pr-0 pl-0 row "></div>
 		</div>
 	</div>
</div>
@endsection