@extends('layouts.app')

@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('content')

    <div class="section-gap--small mt-20 mb-30">
        <div class="medium-container">
            <h2 class="b-title mb-30">@lang('lang.great_chance')</h2>
            {{-- <div class="col-md-12 text-right " style="margin-bottom: 5px">
                <div class="" style="display:-webkit-inline-box; font-size: 15px; color: #3aa5d0">
                    <div style="margin-right: 8px">
                       @php
                            $time = '';
                            if(!$time){
                                $time = date('Y-m-d H:m');
                            }
                         @endphp 
                            <span >{{ DateMonthName::updateDateTime($time) }}</span><br>
                        <span >@lang('lang.update_time')</span>
                    </div>
                    <img  src="/temp/images/update_time.png" width="32" />
                </div>
            </div> --}}
            @widget("currency")
        </div>
    </div>


    <div class="section-gap--small mt-20 mb-20">
        <div class="medium-container section-gap--small mt-30  mb-20">
            <h2 class="b-title">@lang('lang.currency_exchange_rate')</h2>
            <div class="breadcrumb_ breadcrumb_--center">

                <a href="{{ route('on-bank-exchange-rate',['locale'=>App::getLocale()]) }}" >@lang('lang.calc_currency')</a>
                <a href="{{ route('cb-exchange-rate',['locale'=>App::getLocale()]) }}" class="active" style="text-transform: uppercase">@lang('lang.cbu')</a>
            </div>
        </div>
    </div>
    <div class="exchange-list">
        <div class="medium-container section-gap--small mt-30">
            <table>
                <thead>
                    <tr>
                        <th>@lang('lang.currency_name')</th>
                        <th>@lang('lang.symbol_name')</th>
                        <th style="text-transform: capitalize;" width="200">@lang('lang.course') (UZS)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="empty">
                        <td></td>
                    </tr>
                @foreach($cb_rate as $row)
                    <tr>
                        <td>{{ $row->nominal }} {{ $row->name }}</td>
                        <td>#{{ $row->key }}</td>
                        <td>#{{ number_format($row->rate,2) }}
                         @php
                             $diff_class = '';
                             if((float)$row->diff < 0){
                                $diff_class = "bottom"; 
                             } else {
                                $diff_class = "top"; 
                             }
                          @endphp
                         <sup  class="{{ $diff_class }}" >{{ $row->diff }}</sup>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>

    </script>
@endsection