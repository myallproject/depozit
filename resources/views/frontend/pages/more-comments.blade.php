@foreach($comments as $comment)
    <div class="testi-column col-md-6 mb-4 col-sm-12 col-xs-12">
        <!--Testimonial Block-->
        <div class="testimonial-block">
            <div class="inner">
                <div class="text">{{ $comment->text }}</div>
                <div class="info">
                    <figure class="image img-circle"><img class="img-circle" src="images/noavatar2.png" alt=""></figure>
                    <h4>{{ $comment->name }}</h4>
                    <div class="designation">@lang('lang.user_site')</div>
                </div>
            </div>
        </div>
    </div>
@endforeach
