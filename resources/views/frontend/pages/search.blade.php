@extends('layouts.app')
@section('css')
    <link href="/css/banks.css" type="text/css" rel="stylesheet" />
    <link href="/plugins/rangeslider.js-2.3.0/rangeslider.css" type="text/css" rel="stylesheet" />
@endsection
@section('meta')
         @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('content')
    <div class="section">
        <div class="partners section-gap mt-10">
            <div class="medium-container">
                <div class="filter-result">
                    <div class="filter-result__title">
                        <h1 class="b-title mb-2">@lang('lang.search_info_in_site')</h1>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <form action="{{ route('search',['locale'=>App::getLocale()]) }}" method="GET">
                                <input class="" id="search-input" name="variable" value="" >
                                <button type="submit" class="search-icon search-icon--submit btn-search-form"><i class="fa fa-search  search-banks"></i></button>
                            </form>
                        </div>
                    </div>
                    
                   {{--  <input type="range" min="10" max="1000" step="10" value="300" data-orientation="vertical" > --}}

                    <div class="table filter-table">
                        <div class="thead">
                            <div class="col-md-12 row">
                                <div class="col-md-12 align-items-center">
                                    <div class="big"><h2>@lang('lang.search_result')</h2></div>
                                </div>
                            </div>
                        </div>
                        <div class="tbody ">
                            @if(count($resultSearch) > 0)
                                @foreach($resultSearch as $result)
                                    <div class="col-md-12 border-bottom row pt-25">
                                        <div class="col-md-12 pb-15">
                                            <div class="big d-block mb-5">
                                                <h3><a href="{{ $result->url }}">{{ $result->title }}</a></h3>
                                            </div>
                                             <div class="small d-block">

                                             </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="col-md-12 border-bottom row pt-25">
                                    <div class="col-md-12 pb-15">
                                        <div class="big d-block mb-5">
                                            <h3>@lang('lang.not_found')</h3>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="/plugins/rangeslider.js-2.3.0/rangeslider.js"></script>
    <script src="/plugins/rangeslider.js-2.3.0/rangeslider.min.js"></script>
    <script>
         $('input[type="range"]').rangeslider();
    </script>
@endsection