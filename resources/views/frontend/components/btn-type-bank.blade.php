
<style type="text/css" media="screen">
    .d-i-block{
        display: inline-block;
    }
    .b-r-l{
        border-radius: 8px 0px 0px 8px;
    }
    .b-r-r{
        border-radius: 0px 8px 8px 0px;
    }
    .b-l-none{
        border-left: none !important;
    }
    .b-r-none{
        border-right: none !important;
    }
    .btn-filter-bank{

        border: 1px solid #024c67;
        padding: 5px;
        display: inherit;
        color: #444;
        text-align: center !important;

       
    }.btn-filter-bank:hover{
        -webkit-animation: pulse-shadow-blue .5s forwards;
        -moz-animation: pulse-shadow-blue .5s forwards;
        -o-animation: pulse-shadow-blue .5s forwards;
        animation: pulse-shadow-blue .5s forwards;
        background-color: #0b6080;
        color:white;
    }
    .btn-filter-bank-bg{
        background-color: #0b6080;
        color:white;   
    }

    
</style>

<div class="col-lg-12 col-md-12 col-sm-12 mt-0" style="display: flex;">
    <input type="hidden" data-btn-type-id="" id="bank_type_input" name="type_banks_btn" value="">
   <div class="">
        <div class="form-group">
            <a class="btn btn-filter-bank b-r-l " id="all_banks_btn" data-status="0" data-bank-type="all_banks">@lang('lang.all_banks')</a>
        </div>
   </div>
   <div class="">
        <div class="form-group">
            <a class="btn btn-filter-bank b-l-none" id="state_bank_btn" data-status="0" data-bank-type="state_bank">@lang('lang.state_bank')</a>
        </div>
   </div>
   <div class="">
     <div class="form-group">
            <a class="btn btn-filter-bank b-l-none" id="private_bank_btn" data-status="0" data-bank-type="private_bank">@lang('lang.private_bank')</a>
        </div>
   </div>
   <div class="">
        <div class="form-group">
            <a class="btn btn-filter-bank b-r-r b-l-none" id="foreign_bank_btn" data-status="0" data-bank-type="foreign_bank">@lang('lang.foreign_bank')</a>
        </div>
   </div>
</div>