<select class='select2__js filter-deposit-ajax' data-validate="true" name="currency" id="f-el-currency" data-empty="@lang('lang.choose')">
    @foreach($currency as $cur)
        <option @if($cur->id == $id) selected @endif value="{{ $cur->id }}">{{ $cur->name }}</option>
    @endforeach
</select>