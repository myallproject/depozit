<div class="col-md-8 mx-auto order-container">
    <h1>
        @lang('lang.'.$title)
    </h1>
    <form action="{{ route('order.'.$type, ['local'=>App::getLocale()]) }}" method="post" id="order-form">
        @csrf
        <div class="row">
            <div class="col-md-6 pb-20">
                <div class="form-group">
                    <label>@lang('lang.v2.bank_profile.fio')</label>
                    <input type="text" class="form-control" name="user_name" placeholder="@lang('lang.v2.bank_profile.fio')" required minlength="5" >
                </div>
            </div>
            <div class="col-md-6 pb-20">
                <div class="form-group">
                    <label>@lang('lang.v2.bank_profile.your_phone')</label>
                    <input type="text" class="form-control" name="phone" placeholder="998 XX XXX XX XX" required minlength="12" maxlength="12" >
                </div>
            </div>
            <div class="col-md-6 pb-20">
                <div class="form-group">
                    <label>@lang('lang.v2.bank_profile.email')</label>
                    <input type="email" class="form-control" name="email" required placeholder="you@mail.com" >
                </div>
            </div>
            <div class="col-md-6 pb-20">
                <div class="form-group">
                    <label>@lang('lang.region')</label>
                    <select class="select2__js" name="city" required>
                        <option value="" disabled selected>@lang('lang.choose')</option>
                        @foreach ($regions as $region)
                            <option value="{{ $region->id }}">{{ $region->name() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12 pb-20">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="agree" required>
                    <label for="agree">@lang('lang.v2.order.agreement')</label>
                </div>
            </div>
            <div class="col-md-6 col-6 order-btn-container">
                <div class="form-group">
                    <button type="submit" class="btn btn--blue btn--medium">@lang('lang.v2.order.apply')</button>
                </div>
            </div>
        </div>
        <input type="hidden" name="product_id" value="{{ $product_id }}">
    </form>
</div>

{{-- @section('script')
    <script>
        $(document).ready(function(){
            $('#order-form').on('submit',function(e){
                e.preventDefault();
                form = $(this);
                $.ajax({
                    url:form.attr('action'),
                    method:form.attr('method'),
                    data:form.serialize()
                }).done(function(data){
                    $('#ranking-result-content').html(data)
                });
            });
        });
    </script>
@endsection --}}