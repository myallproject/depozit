@extends('layouts.app')

@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection


@section('content')
<style type="text/css">
.select2-search {
    display: inherit !important;
}
.select2-container--default .select2-results__option {
    padding-left: 15px !important;
}
.select2-container--default .select2-results__option::before{
    content: none !important;
}
.select2-container--default .select2-results__option[role=group] {
    padding-left: 10px !important;
}
.input-loader{
    font-size: 20px;
    position: absolute;
    display: block;
    top: 35px;
    left: 180px;
}
.choose-star{
    font-size: 30px
}
.star-rating > span[data-status="true"] > i::before{
    content : "\f005";
}
</style>
    <div class="xs-container page-gap " style="padding-right: 22px; padding-left: 22px">
        <h2 class="m-title text-left">
           @lang('lang.v2.add_form.add_review')
        </h2>
        <form action="{{ route('review.add.form',['locale' => App::getLocale()]) }}" method="post">
            {{ csrf_field() }}
            <div class="row row-xn">
               {{--  <div class="col-md-6">
                    <div class="form-group-xn mb-20">
                        <label for="territoryLabel">@lang('lang.region') <sub class="text-danger choose-star">*</sub></label> 
                        <select name="region_id" class="select2-xn" id="territoryLabel" data-child-id="#bankLabel" data-childred-url="{{ route('select-region-banks',['locale' => App::getLocale()]) }}" required>
                            <option value="0">@lang('lang.choose')</option>
                            @foreach($cities as $city)
                                <option class="border-bottom" @if(old('region_id') == $city->id ) selected  @endif  style="font-weight: bold;" value="{{ $city->id }}">{{ $city->name() }}</option>
                                @foreach($city->children as $child_r)
                                    <option @if(old('region_id') == $child_r->id ) selected  @endif value="{{ $child_r->id }}">&nbsp;&nbsp;{{ $child_r->name() }}</option>
                                @endforeach
                            @endforeach
                        </select>
                    </div>
                </div>  --}}
                <div class="col-md-6 " id="bank-content">
                    <div class="form-group-xn mb-20">
                        <label for="bankLabel">@lang('lang.bank') <sub class="text-danger choose-star">*</sub></label>
                        <select name="bank_id" class="select2-xn children" id="bankLabel" data-child-id="#branchLabel" data-childred-url="{{ route('select-bank-branches',['locale' => App::getLocale()]) }}" required>
                                <option value="0">@lang('lang.choose')</option>
                                @foreach($banks as $bank)
                                    <option @if(old('bank_id') == $bank->id ) selected  @endif  value="{{ $bank->id }}">{{ $bank->name() }}</option>
                                @endforeach
                        </select>
                    </div>
                    <i class="input-loader"></i>
                </div>
                <div class="col-md-6 " id="bank-branches-content">
                    <div class="form-group-xn mb-20">
                        <label for="branchLabel">@lang('lang.v2.add_form.bank_branches')</label>
                        <select name="branch_id" class="select2-xn" id="branchLabel" disabled data-childred-url="">
                            <option value="0">@lang('lang.choose')</option>
                        </select>
                    </div>
                    <i class="input-loader"></i>
                </div>
                <div class="col-md-6">
                    <div class="form-group-xn mb-20">
                        <label for="serviceTypeLabel">@lang('lang.v2.add_form.type_services') <sub class="text-danger choose-star">*</sub></label>
                        <select name="service_id" class="select2-xn" id="serviceTypeLabel" required>
                            <option value="0">@lang('lang.choose')</option>
                            @foreach($services as $service)
                                @if(count($service->children()) > 0)
                                    <option value="{{ $service->id }}">{{ $service->getName() }}</option>
                                    @foreach($service->children() as $child)
                                        <option value="{{ $child->id }}">&nbsp;&nbsp;{{ $child->getName() }}</option>
                                    @endforeach
                                @else
                                    <option value="{{ $service->id }}">{{ $service->getName() }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group-xn mb-20">
                        <label>@lang('lang.v2.add_form.appreciation') <sub class="text-danger choose-star">*</sub></label>
                        <div style="font-size:40px; display: flex;" class="star-rating">
                             <input type="hidden" name="assessment" id="assessment-input" value="" required>
                            <span data-status="" data-id="1"><i class="fa fa-star-o star-rating-i"></i></span>
                            <span data-status="" data-id="2"><i class="fa fa-star-o star-rating-i"></i></span>
                            <span data-status="" data-id="3"><i class="fa fa-star-o star-rating-i"></i></span>
                            <span data-status="" data-id="4"><i class="fa fa-star-o star-rating-i"></i></span>
                            <span data-status="" data-id="5"><i class="fa fa-star-o star-rating-i"></i></span>
                        </div>
                        {{-- <div class="assessment-smiles">
                            <label class="assessment-smile">
                                <input type="radio" name="assessment" value="1" required>
                                <img src="/temp/images/xn/smiley/1.svg" alt="" >
                            </label>
                            <label class="assessment-smile">
                                <input type="radio" name="assessment" value="2" required>
                                <img src="/temp/images/xn/smiley/2.svg" alt="">
                            </label>
                            <label class="assessment-smile">
                                <input type="radio" name="assessment" value="3" required>
                                <img src="/temp/images/xn/smiley/3.svg" alt="" required>
                            </label>
                            <label class="assessment-smile">
                                <input type="radio" name="assessment" value="4" required>
                                <img src="/temp/images/xn/smiley/4.svg" alt="">
                            </label>
                            <label class="assessment-smile">
                                <input type="radio" name="assessment" value="5" required>
                                <img src="/temp/images/xn/smiley/5.svg" alt="">
                            </label>
                        </div> --}}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group-xn mb-20">
                        <label for="titleLabel">@lang('lang.v2.add_form.title') <sub class="text-danger choose-star">*</sub></label>
                        <input type="text" name="title" id="titleLabel" required value="{{ old('title') }}">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group-xn mb-20">
                        <label for="reviewLabel">@lang('lang.v2.add_form.review') <sub class="text-danger choose-star">*</sub> (@lang('lang.v2.add_form.textarea_left_text'))</label>
                        <textarea name="fulltext" id="reviewLabel" minlength="100"  maxlength="3000" required>{{ old('fulltext') }}</textarea>
                        {{-- <div class="r-add-form-left-text">@lang('lang.v2.add_form.textarea_left_text')</div> --}}
                        
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group-xn mb-3">
                        <label for="infoBank">@lang('lang.v2.add_form.info_bank') <sub class="text-danger choose-star"></sub></label>
                        <textarea name="info_bank" id="infoBank" >{{ old('info_bank') }}</textarea>
                    </div>
                </div>
                {{-- <div style="display: -webkit-box; position: relative;">
                    <span style="color:#fb9302; font-size:12px; margin-left: -21px; position: absolute;">
                        <i class="fa fa-exclamation-triangle fa-cs " style="padding-top: 0px" ></i>
                    </span>
                    <span style="text-align: justify; display: inherit; padding-top: 3px">@lang('lang.v2.review_add_page.text_3')</span>
                </div> --}}
                <div>
                    <span style="color:#fb9302;">
                        <i class="fa fa-exclamation-triangle fa-lg"></i>
                    </span>
                    <span>@lang('lang.v2.review_add_page.text_3')</span>
                </div>
               
                <div class="col-md-12 mb-20 mt-20" style="font-size: 15px;">
                    <div style="display: -webkit-box; position: relative;">
                        <span style="text-align: justify; display: inline-grid; padding-top: 4px">@lang('lang.v2.review_add_page.text_1') <span style="margin-top: 10px">@lang('lang.v2.review_add_page.text_2')</span></span>
                    </div>    
                </div>

                 <div class="col-md-12 mb-20 " style="font-size: 15px;">
                    <i><sub class="text-danger choose-star">*</sub><span> - @lang('lang.required_field')</span></i>
                </div>

                <div class="col-md-12 mb-20">
                    <span ><a class="download" id="download" >@lang('lang.communication_rules') &nbsp;<i class="fas fa-file-download fa-lg"></i></a></span>
                </div>
            </div>
            
            <button type="submit" class="btn-xn btn-xn-primary">@lang('lang.v2.add_form.btn_review')</button>
        </form>
    </div>
@endsection
@section('script')
 <script type="text/javascript">
     
   $(document).ready(function() {
        $('#territoryLabel').on('change',function(){
             $('#bank-content').find('i').addClass('fa fa-refresh fa-spin fa-3x fa-fw');
            $.ajax({
                method:'GET',
                url:$(this).attr('data-childred-url'),
                data:{
                    region_id: $(this).val()
                }
            }).done(function(data,){
                $('#bank-content').find('i').removeClass('fa fa-refresh fa-spin fa-3x fa-fw');
                $('#bankLabel').html(data);
                $('#branchLabel').html('');
            });
        });

        $('.children').on('change',function(){
            $('#bank-branches-content').find('i').addClass('fa fa-refresh fa-spin fa-3x fa-fw');
            $.ajax({
                method:'GET',
                url:$(this).attr('data-childred-url'),
                data:{
                    id: $(this).val(),
                    region_id: $('#territoryLabel').val()
                }
            }).done(function(data,){
                $('#bank-branches-content').find('i').removeClass('fa fa-refresh fa-spin fa-3x fa-fw');
                $('#branchLabel').removeAttr('disabled');
                $('#branchLabel').html(data);
            });
        });
        
        if($('#bankLabel').val() != 0){
           $('#bank-branches-content').find('i').addClass('fa fa-refresh fa-spin fa-3x fa-fw');
            $.ajax({
                method:'GET',
                url:"{{ route('select-bank-branches',['locale' => App::getLocale()]) }}",
                data:{
                    id: $('#bankLabel').val(),
                    region_id: $('#territoryLabel').val()
                }
            }).done(function(data,){
                $('#bank-branches-content').find('i').removeClass('fa fa-refresh fa-spin fa-3x fa-fw');
                $('#branchLabel').removeAttr('disabled');
                $('#branchLabel').html(data);
            });  
        }

        $('.download').on('click',function(){
             $.ajax({
                method:'GET',
                url:"{{ route('download-communication-rules',['lang' => App::getLocale()]) }}"
            }).done(function(){
                window.open("{{ route('download-communication-rules',['lang' => App::getLocale()]) }}",'_blank');
            });
        });

        $('.star-rating-i').hover(function(){
            $.each($('.star-rating-i'),function(){
                $(this).parent('span').toggleAttr('data-status','');
            });

            parent = $(this).parent('span').attr('data-id');
            for (i = 1 ; i <= parent; i++) {
                $('span[data-id="'+i+'"]').find('i').toggleClass('fa-star').toggleClass('fa-star-o');
                $('span[data-id="'+i+'"]').toggleAttr('data-status','true');
            }
            $('#assessment-input').val(parent);
            //$(this).toggleClass('fa-star-o').toggleClass('fa-star');
        });

        $('.star-rating-i').click(function(){
            parent = $(this).parent('span').attr('data-id');
            for (i = 1 ; i <= parent; i++) {
                 $('span[data-id="'+i+'"]').find('i').addClass('fa-star').removeClass('fa-star-o');
                 $('span[data-id="'+i+'"]').attr('data-status','true');
            }
            $('#assessment-input').val(parent);
            //$(this).toggleClass('fa-star-o').toggleClass('fa-star');
        });

         $.fn.toggleAttr = function(attr, val) {
            var test = $(this).attr(attr);
            if ( test ) { 
              // if attrib exists with ANY value, still remove it
              $(this).removeAttr(attr);
            } else {
              $(this).attr(attr, val);
            }
            return this;
          };
        });

 </script>
    
@endsection