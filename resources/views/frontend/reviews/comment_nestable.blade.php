@foreach($comments as $comment)
    <div class="comment-item">
        @if(in_array($comment->user->role_id, config('global.admins')))
            <div class="comment-item__avatar" style="background-image:url(/temp/images/dp-logo.png)"></div>
        @elseif($comment->user->role_id == 11)
            <div class="comment-item__avatar" style="background-image:url(/{{ $comment->user->organization->organisation->image }}); background-size: 170%;"></div>
		@else
            <div class="comment-item__avatar" style="background-image:url(images/content/bank/agroexport.png)"></div>
        @endif
        <div class="comment-item__meta">
            @if(in_array($comment->user->role_id, config('global.admins')))
                <div class="comment-item__username">@lang('lang.moderator')</div>
			@elseif($comment->user->role_id == 11)
                <div class="comment-item__username">{{ $comment->user->organization->organisation->name() }}</div>
            @else
                <div class="comment-item__username">{{ $comment->user->name }}</div>
            @endif
            <div class="comment-item__date">{{ $comment->created_at->format('d.m.Y') }}</div>
        </div>
        <div class="m-description text-justify">{{ $comment->text }}</div>
        <div><div class="to-comment" data-parent-id="{{ $comment->id }}">@lang('lang.add_comment')</div></div>
        @if($comment->sub->count())
            <div class="comment-item__child">
                @include('frontend.reviews.comment_nestable', ['comments' => $comment->sub])
            </div>
        @endif
    </div>
@endforeach
