@extends('layouts.app')

@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection

@section('css')
<style type="text/css">
    .select2-search {
        display: inherit !important;
    }
    .select2-container--default .select2-results__option {
        padding-left: 15px !important;
    }
    .select2-container--default .select2-results__option::before{
        content: none !important;
    }
    .select2-container--default .select2-results__option[role=group] {
        padding-left: 10px !important;
    }
    .news-pagination ul{
        display: flex;
    }
    .news-pagination ul li {
        padding-right: 4px;
        padding-left: 4px;
        font-size: 18px
    }
    .input-loader{
        font-size: 20px;
        position: absolute;
        display: block;
        top: 12px;
        left: 100px;
    }
    .text-justify{
        text-align: justify;
    }
    .card-border {
        padding: 25px 20px;
    }
    .flex-wrap{
        flex-wrap: wrap;
    }
    /*.container-with-sidebar__content, .container-with-sidebar__sidebar{
        padding-right: 0px;
        padding-left: 0px;
    }*/

    .tooltip:hover:before {
        top: -4px;
        left: 47px;
        transform: rotate(0deg);
        content: none;
    }
    .tooltip:hover:after {
        top: -4px;
        left: 0px;
    }
    .tooltip-t:hover:after {
        top: -30px !important;
        left: 0px;
    }
    .modal-region-span:hover{
        color: #0296ce;
    }
    .review-card.rejected {
        border: 1px solid #dedede;
    }
    .filter-span{
        display: block;
    }
    .share-links{
        font-size: 40px;
        display: contents;
    }
    .share-links > div {
        margin: 15px 15px 15px 15px;
        display: -webkit-inline-box;
    }
    .mt-8{
        margin-top: 8px !important;
    }
</style>
<link rel="stylesheet" href="/css/ranking.css">
@endsection

@section('content')
    <div class="md-container page-gap">
        <h2 class="m-title">
            @if($single_bank)
                @lang('lang.v2.reviews_bpTitle',['bankname'=>$single_bank[1]])
            @else
                @lang('lang.v2.reviews_pageTitle')
            @endif
        </h2>
        <div class="container-with-sidebar col-md-12 pl-0 pr-0">
            <div class="container-with-sidebar__content mb-20 col-md-9 pl-15 pr-15" style="flex: inherit;">
                <div class="card-border mb-20 " style="padding-bottom: 20px; height:100%">
                <form id="review-filter-form" action="{{ route('review.list',['locale'=>App::getLocale()]) }}" method="GET">
                    <div class="row">
                        <div class="col-md-12" style="flex-wrap: wrap; display: contents;">
                             <div class="col-sm-12 pl-5 pr-5 mb-15" style="margin-bottom: 8px;">
                                <a class="tooltip tooltip-t" data-tooltip="Hududni tanlang" data-modal="region-list-modal" ><span id="region-name-selector">@lang('lang.republic_uzb')</span><i class="fa fa-list ml-10"></i></a>
                                <input class="" type="hidden" name="region" value="" id="region-id-input"/>
                                {{-- <div class="form-group">
                                     <select name="region" class="select2__js review-ajax-filter" id="select-region">
                                        <option value="0">@lang('lang.regions')</option>
                                            @foreach($regions as $region)
                                                <option class="border-bottom" style="font-weight: bold;" value="{{ $region->id }}">{{ $region->name() }}</option>
                                                @foreach($region->children as $child_r)
                                                    <option value="{{ $child_r->id }}">&nbsp;&nbsp;{{ $child_r->name() }}</option>
                                                @endforeach
                                            @endforeach
                                    </select>
                                </div> --}}
                            </div>  
                            <div class="col-sm-3 pl-5 pr-5" style="margin-bottom: 8px;" id="banks-input-content">
                                <div class="form-group">
                                     <select name="bank" class="select2__js review-ajax-filter" id="select-organization">
                                        <option value="0">@lang('lang.banks')</option>
                                        @if($single_bank)
                                            @foreach($banks as $bank)
                                                <option value="{{ $bank->id }}" @if($bank->id == $single_bank[0]) selected @endif>{{ $bank->name() }}</option>
                                            @endforeach
                                        @else
                                            @foreach($banks as $bank)
                                                <option value="{{ $bank->id }}">{{ $bank->name() }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <i class="input-loader "></i>
                            </div> 
                            <div class="col-sm-3 pl-5 pr-5" style="margin-bottom: 8px;">
                                <div class="form-group">
                                     <select name="services" class="select2__js review-ajax-filter" id="select-services">
                                        <option value="0">@lang('lang.services')</option>
                                       @foreach($services as $service)
                                            @if(count($service->children()) > 0)
                                                <option value="{{ $service->id }}">{{ $service->getName() }}</option>
                                                @foreach($service->children() as $child)
                                                    <option value="{{ $child->id }}">&nbsp;&nbsp;{{ $child->getName() }}</option>
                                                @endforeach
                                            @else
                                                <option value="{{ $service->id }}">{{ $service->getName() }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3 pl-5 pr-5" style="margin-bottom: 8px;">
                                <div class="form-group">
                                     <select name="assessment" class="select2__js review-ajax-filter" id="select-assessment">
                                        <option value="0">@lang('lang.assessment')</option>
                                       @foreach(config("global.review.assessment_text")  as $at)
                                        <option value="{{ $at }}">@lang('lang.v2.assessment_text.'.$at)</option>
                                       @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 pl-5 pr-5" style="margin-bottom: 8px;">
                                <div class="form-group ">
                                     <select name="answer_status" class="select2__js review-ajax-filter" id="select-status">
                                        <option value="0">@lang('lang.v2.review')</option>
                                        @foreach(config('global.review.answer_status') as $st_k => $st_v)
                                            <option value="{{ $st_k }}">@lang('lang.v2.answer_status.'.$st_v)</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                   </form>
                </div>
             </div>
             <div class="container-with-sidebar__sidebar col-md-3 pl-15 pr-15" style="width: auto; flex: inherit;">
                @if(!(Auth::check() && Auth::user()->role_id == 11))
                <div class="card-border mb-20">
                    <span class="d-block m-description" style="line-height: 25px; margin-bottom: 8px">@lang('lang.v2.index_page.text_right')</span>
                    <a href="{{ route('review.add',['locale' => App::getLocale()]) }}" class="btn-xn btn-xn-primary btn-xn-justify">
                        @lang('lang.v2.index_page.text_btn')
                    </a>
                </div>
                @else 
                <div class="card-border mb-20">
                    <span class="d-block m-description" style="line-height: 25px; margin-bottom: 8px">@lang('lang.v2.index_page.bptext_right')</span>
                    <a href="{{ route('review.bp_list',['locale' => App::getLocale(), 'id' => Auth::user()->organization->child_org_id]) }}" class="btn-xn btn-xn-primary btn-xn-justify">
                        @lang('lang.v2.bpreview_bank')
                    </a>
                </div>
                @endif
            </div>
            
            <div class="container-with-sidebar__content mb-20 col-md-9 pl-15 pr-15" style="flex: inherit;">
                <div class="review-list" id="review-result-content">
                    @include('frontend.reviews.reviews-content',['reviews' => $reviews])
                </div>
            </div>
            <div class="container-with-sidebar__sidebar col-md-3 pl-15 pr-15" style="width: auto;flex: inherit;">
                <div class="rating-reviews card-border">
                    <h3 class="sm-title">@lang('lang.top_services_ranking')</h3>
                    <table style="width: 100%">
                        <tbody>
                            @if ($top_services)
                                @foreach ($top_services as $service)
                                    <tr>
                                        <td style="width: 70%;">
                                            <a href="{{ route('info-bank',['locale'=>App::getLocale(),'slug'=>$service['bank_slug']]) }}" class="rating-bank" target="_blank">
                                                {{ $service['bank_name'] }}
                                            </a>
                                            <span class="rating-specification"> {{ $service['service_name'] }} </span>
                                        </td>
                                        <td style="width: 30%;"> {{ $service['mark'] }} </td>
                                    </tr>
                                @endforeach
                            @else
                                This ranking is unaviable for now!
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <?php //echo  htmlspecialchars_decode(Share::currentPage()->facebook()); ?> 
    
    <?php /* echo  htmlspecialchars_decode(Share::page('http://jorenvanhocht.be', 'Share title')
    ->facebook()
    ->twitter()
    ->linkedin('Extra linkedin summary can be passed here')
    ->whatsapp()); */?>

<div class="modal-ui" data-modal-name="region-list-modal">
    <div class="modal-ui__dialog detailed-search">
        <div class="modal-ui__content" style="border-radius: 8px;">
            <div class="modal-ui__close-btn"></div>
            <h2 class="b-title">@lang('lang.city_uzb')</h2>
            <div class="col-md-12">
                <div  class="form-group">
                    <input id="region-data-list-input"  class="form-control" placeholder="@lang('lang.input_region_name')">
                </div>
            </div>

            <div class="col-md-12 pl-0 pr-0">
                    <div class="col-md-12" style="display: flex;">
                         <span class="modal-region-span select-region-span mb-15" style="display: inherit;cursor: pointer;" data-id="">@lang('lang.republic_uzb')</span>
                        <span class="modal-region-span select-region-span ml-15 mb-15" style="display: inherit;cursor: pointer;" data-id="2">@lang('lang.city_tashkent')</span>
                    </div>
                   

                    <div id="filter-content" style="max-height: 350px; overflow-y: auto;" >
                        <div class="col-md-12" style=" flex-wrap: wrap;display: flex;">
                            <div class="col-sm-3 list-parent-div">
                                @foreach($regions_list as $regionk => $regionv)
                                    @if(in_array($regionk, ["A", "B", "C", "D", "E", "F", "G", "I", "H", "А", "Б", "В", "Г"]))
                                        <span class="filter-span">
                                        <b class="mt-15 parent-lt" style="display: grid;">{{ $regionk }}</b>
                                        @foreach($regionv as $rk => $rv)
                                        <span class="modal-region-span select-region-span" style="display: inherit;cursor: pointer;" data-id="{{ $rv['id'] }}" >{{ $rv['name'] }}</span>
                                        @endforeach
                                        </span>
                                    @endif
                                @endforeach
                            </div>

                            <div class="col-sm-3 list-parent-div">

                                 @foreach($regions_list as $regionk => $regionv)
                                    @if(in_array($regionk, ["J", "K", "L", "M", "N", "O", "Д", 'Е', "Ё", "Ж","P", "З", "И", "Й", "К", "Л", "М", "Н", "О"]))
                                        <span class="filter-span">
                                        <b class="mt-15 parent-lt" style="display: grid;">{{ $regionk }}</b>
                                        @foreach($regionv as $rk => $rv)
                                        <span class="modal-region-span select-region-span" style="display: inherit;cursor: pointer;" data-id="{{ $rv['id'] }}" >{{ $rv['name'] }}</span>
                                        @endforeach
                                        <span>
                                    @endif
                                @endforeach
                            </div>

                            <div class="col-sm-3 list-parent-div">
                                 @foreach($regions_list as $regionk => $regionv)
                                    @if(in_array($regionk, ["Q", "R", "S", "T", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц","Ч"]))
                                        <span class="filter-span">
                                        <b class="mt-15 parent-lt" style="display: grid;">{{ $regionk }}</b>
                                        @foreach($regionv as $rk => $rv)
                                        <span class="modal-region-span select-region-span" style="display: inherit;cursor: pointer;" data-id="{{ $rv['id'] }}" >{{ $rv['name'] }}</span>
                                        @endforeach
                                    </span>
                                    @endif
                                @endforeach
                            </div>
                            
                            <div class="col-sm-3 list-parent-div">
                                @foreach($regions_list as $regionk => $regionv)
                                    @if(in_array($regionk, ["U", "V", "W", "X","Y", "Z", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я"]))
                                        <span class="filter-span">
                                        <b class="mt-15 parent-lt" style="display: grid;">{{ $regionk }}</b>
                                        @foreach($regionv as $rk => $rv)
                                            <span class="modal-region-span select-region-span" style="display: inherit;cursor: pointer;" data-id="{{ $rv['id'] }}" >{{ $rv['name'] }}</span>
                                        @endforeach
                                         </span>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<a data-modal="share-modal" class="show-social-modal" style="display: none"></a>
<div class="modal-ui" data-modal-name="share-modal">
    <div class="modal-ui__dialog detailed-search" style="top: 20%;">
        <div class="modal-ui__content" style="max-width: 500px !important; border-radius: 8px;">
            <div class="modal-ui__close-btn"></div>
            <h2 class="b-title">@lang('lang.review_modal_title')</h2>
            <div class="col-md-12 text-center">
               <div class="col-md-12 share-links mt-8 pl-0" >
                    @if(session()->has('newReview'))
                    <?php $session = session()->get('newReview'); ?>
                        <?php echo  htmlspecialchars_decode(Share::page(route('review.view',['locale' => App::getLocale(),'id'=>$session['review_id']]).'&quote='.$session['text'])->facebook()); ?>
                        <?php echo  htmlspecialchars_decode(Share::page(route('review.view',['locale' => App::getLocale(),'id'=>$session['review_id']]),$session['text'])->twitter()); ?>
                        <?php echo  htmlspecialchars_decode(Share::page(route('review.view',['locale' => App::getLocale(),'id'=>$session['review_id']]),$session['text'])->telegram()); ?>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
    <script>
        $(document).ready(function(){

            $(document).on('click', '.pagination a', function(e) {
                serch_form = $('#review-filter-form');
                e.preventDefault();

                paginate($(this).attr('href').split('page=')[1],serch_form.serialize());
            });


           $('.review-ajax-filter').on('change',function(e){
                form = $('#review-filter-form');
                e.preventDefault();
                $.ajax({
                    url:form.attr('action'),
                    method:form.attr('method'),
                    data:form.serialize()
                }).done(function(data){
                    $('#review-result-content').html(data)
                });
            });

           $('.select-region-span').on('click',function(e){
                id = $(this).attr('data-id');
                $('#banks-input-content').find('i').addClass('fa fa-refresh fa-spin fa-3x fa-fw');
                $.ajax({
                    url:"{{ route('select-region-banks',['locale'=>App::getLocale()]) }}",
                    method:"GET",
                    data:{
                        region_id: id
                    }
                }).done(function(data){
                    $('#banks-input-content').find('i').removeClass('fa fa-refresh fa-spin fa-3x fa-fw');
                    $('#select-organization').html(data)
                });

                region_name = $(this).text();
                $('.modal-ui__close-btn').click();
                $('#region-name-selector').text(region_name);
                $('#region-id-input').val(id);

                form = $('#review-filter-form');
                e.preventDefault();
                $.ajax({
                    url:form.attr('action'),
                    method:form.attr('method'),
                    data:form.serialize()
                }).done(function(data){
                    $('#review-result-content').html(data)
                });

           });

              $("#region-data-list-input").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#filter-content").find('.col-sm-3').filter(function() {
                  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
                });
                $("#filter-content").find('.filter-span').filter(function() {
                  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
                });
                $("#filter-content").find('.select-region-span').filter(function() {
                  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
              });
        });

        function paginate(page,search) {
            $.ajax({
                url: '?page=' + page,
                dataType: 'json',
                data:search,
            }).done(function(data) {
                $('#review-result-content').html('');
                $('#review-result-content').html(data);
                location.hash = page;
            });
        }
    </script>

    @if(session()->has('newReview'))
        <script> 
            $(document).ready(function(){
               setTimeout(function(){$('.show-social-modal').click();},5000); 
            });
        </script>
    @endif
@endsection