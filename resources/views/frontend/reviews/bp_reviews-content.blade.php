@if(count($reviews) <= 0)
	 <div class="review-card card-border">
		<div class="col-md-12 text-center">
			<img src="/temp/images/icons/search_data.svg" style="width:80px;">
		</div>
		<div class="col-md-12 text-center">
			@lang('lang.not_found')
		</div>
	</div>

@endif

@foreach($reviews as $review)
@php
    $isRead = $review->bp_reviewStatus->readstatus;
@endphp
    <div class="review-card card-border {{ $review->statusClass() }}">
        <div class="toogle-icon">
            
            <div class="toggleicontext">
                <a 
                    data-text="@if(!$isRead) @lang('lang.yesread') @else @lang('lang.noread') @endif" 
                    data-href="{{route('review.bp_readlist',['id' => $review->id,'locale' => App::getLocale()])}}" 
                    class="toggleable btn-xn btn-xn-justify  @if($isRead) {{'readed btn-xn-default'}} @else {{ 'btn-xn-primary' }}@endif">
                    @if($isRead) @lang('lang.yesread') 
                    @else @lang('lang.noread')
                    @endif 
                </a>
                
            </div>
        </div>

        <div class="review-card__meta">
            <div class="d-flex align-items-center flex-wrap">
               {{--  <div class="review-card__point {{ $review->assessmentClass() }} "   style="margin-bottom: 8px;" data-tooltip="@lang('lang.v2.'.$review->statusText())">@if($review->status != 0){{ $review->assessment }} @endif</div> --}}
                @if($review->status != 0)
                <div class="{{ $review->starClass() }} --green mr-10" style="display: flex;" data-tooltip="@lang('lang.v2.review_waiting')">
                    @for($i=1; $i <= 5; $i++)
                        <i class="fa @if($i  <= $review->assessment) fa-star @else fa-star-o @endif"> </i>
                    @endfor
                </div>
                @endif
                <div class="review-card__date mr-10" style="font-size: 15px">{{ $review->created_at->format('d.m.Y') }}</div>
                <div class="review-card__date"><i class="fa fa-user-o"></i><span class="ml-10">{{ $review->user->getName() }}</span></div>
            </div>
            <div class="review-card__bank">
                @if($review->bank_office !== null)<div class="mr-10 mt-10"><h3>{{$review->bank_office->getName()}}</h3></div>@endif
                <img src="/{{ $review->bank->image }}" alt="">
            </div>
        </div>
        
        <a href="{{ route('review.view', ['id' => $review->id,'locale' => App::getLocale()]) }}" 
        class="review-card__title nextpage">
            {{ $review->title }}
        </a>
        <div class="m-description text-justify">
            {{ $review->limitedFulltext() }}
        </div>
        @if($review->info_bank != null) 
            <div class="info-bank">
                <span class="info-bank-heading">
                    @lang('lang.v2.add_form.info_bank'):
                </span>
                <span>{{$review->info_bank}}</span>
            </div>
        @endif
        <div class="review-card__footer d-flex flex-wrap">
            @if($review->service)
                <div class="review-card__service mr-20">
                    {{ $review->service->getName() }}
                </div>
            @endif
            <a href="{{ route('review.view', ['id' => $review->id, 'locale' => App::getLocale()]) }}#comments" class="review-card__comments">
                <img src="/temp/images/xn/chat.svg" alt="">
                <span>{{ (int)$review->comments_count }} @lang('lang.a_comment')</span>
            </a>      
        </div>
        <div class="btn-container">
            @if($review->answer_status == 2 || $review->answer_status == 4)
                <div class="buttonforesponse">
                    <a href="{{ route('review.bp_edit_response_page', ['id' => $review->id,'locale' => App::getLocale()]) }}" class="btn-xn btn-xn-justify btn-xn-secondary" >@lang('lang.v2.bank_profile.edit_response')</a>
                </div>
            @endif
            <div class="buttonforesponse">
                <a href="{{ route('review.view', ['id' => $review->id,'locale' => App::getLocale()]) }}" class="btn-xn btn-xn-justify btn-xn-primary">@lang('lang.v2.bank_profile.answer_response')</a>
            </div>          
        </div>
    </div>
@endforeach
<div class="col-md-12  news-pagination">{{ $reviews->links() }}</div>
