@if(count($reviews) <= 0)
	 <div class="review-card card-border">
		<div class="col-md-12 text-center">
			<img src="/temp/images/icons/search_data.svg" style="width:80px;">
		</div>
		<div class="col-md-12 text-center">
			@lang('lang.not_found')
		</div>
	</div>

@endif

@foreach($reviews as $review)
    <div class="review-card card-border {{ $review->statusClass() }} mt-0" >
        <div class="review-card__meta">
            <div class="d-flex align-items-center flex-wrap">
               {{--  <div class="review-card__point {{ $review->assessmentClass() }} "   style="margin-bottom: 8px;" data-tooltip="@lang('lang.v2.'.$review->statusText())">@if($review->status != 0){{ $review->assessment }} @endif</div> --}}
                @if($review->status != 0)
                <div class="{{ $review->starClass() }} {{ $review->assessmentColor() }} mr-10 " style="display: flex;" data-tooltip="@lang('lang.v2.review_waiting')">
                    @for($i=1; $i <= 5; $i++)
                        <i class="fa @if($i  <= $review->assessment) fa-star @else fa-star-o @endif"> </i>
                    @endfor
                </div>
                 @else
                    <div style="color:red" class="mr-10">
                        <span class="tooltip tooltip-r" data-tooltip=" {{ $review->moderatorComment($review->id)['text'] }}" >@lang('lang.v2.review_rejected')</span>
                    </div>
                @endif
                <div class="review-card__date mr-10" style="font-size: 15px">{{ $review->created_at->format('d.m.Y') }}</div>
                <div class="review-card__date"><i class="fa fa-user-o"></i><span class="ml-10">{{ $review->user->getName() }}</span></div>
            </div>
            <div class="review-card__bank">
                <img src="/{{ $review->bank->image }}" alt="">
            </div>
        </div>
        
        <a href="{{ route('review.view', ['id' => $review->id,'locale' => App::getLocale()]) }}" class="review-card__title">
            {{ $review->title }}
        </a>
        <div class="m-description text-justify">
            {{ $review->limitedFulltext() }}
        </div>
        <div class="review-card__footer d-flex flex-wrap">
            @if($review->service)
                <div class="review-card__service mr-20">
                    {{ $review->service->getName() }}
                </div>
            @endif
            <a href="{{ route('review.view', ['id' => $review->id, 'locale' => App::getLocale()]) }}#comments" class="review-card__comments">
                <img src="/temp/images/xn/chat.svg" alt="">
                <span>{{ (int)$review->comments_count }} @lang('lang.a_comment')</span>
            </a>      
        </div>
    </div>
@endforeach
<div class="col-md-12  news-pagination">{{ $reviews->links() }}</div>
