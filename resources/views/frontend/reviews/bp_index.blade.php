@extends('layouts.app')

@section('title')
    <title>@lang('lang.v2.reviews_metaTitle')</title>
@endsection

@section('content')
<style type="text/css">
.select2-search {
    display: inherit !important;
}
.select2-container--default .select2-results__option {
    padding-left: 15px !important;
}
.select2-container--default .select2-results__option::before{
    content: none !important;
}
.select2-container--default .select2-results__option[role=group] {
    padding-left: 10px !important;
}
.news-pagination ul{
    display: flex;
}
.news-pagination ul li {
    padding-right: 4px;
    padding-left: 4px;
    font-size: 18px
}
.input-loader{
    font-size: 20px;
    position: absolute;
    display: block;
    top: 12px;
    left: 100px;
}
.text-justify{
    text-align: justify;
}
.card-border {
    padding: 25px 20px;
}
.flex-wrap{
    flex-wrap: wrap;
}
/*.container-with-sidebar__content, .container-with-sidebar__sidebar{
    padding-right: 0px;
    padding-left: 0px;
}*/

.tooltip:hover:before {
    top: -4px;
    left: 47px;
    transform: rotate(0deg);
    content: none;
}
.tooltip:hover:after {
    top: -4px;
    left: 0px;
}
.tooltip-t:hover:after {
    top: -30px !important;
    left: 0px;
}
.modal-region-span:hover{
    color: #0296ce;
}
.review-card.rejected {
    border: 1px solid #dedede;
}
/*toggle icon*/
.toggleicontext {
    display: inline-block;
}
.readed {
   color: rgb(94, 91, 91);
}
.info-title {
    display: block;
    text-align: center;
    font-weight: 500;
}
.buttonforesponse {
    margin-right: 10px;
}   
.btn-container{
    margin-left: 50%;
    width: 50%;
    display: flex;
    justify-content: flex-end;
}
.btn-container a{
    
}
span.stars i{
    color: #0dd149 !important;
}
.info-bank {
    margin-top: 20px;
}
.info-bank-heading {
    font-size: 18px;
    color: rgb(179, 13, 13);
}
/*toggle icon end*/
</style>
    <div class="md-container page-gap">
        <h2 class="m-title">
            @lang('lang.v2.reviews_bpTitle', ['bankname' => $bank->name()])
        </h2>
        <h3 class="info-title">@lang('lang.position'): {{$reyting[1]}}, @lang('lang.ranking'): {{$reyting[0]}}, @lang('lang.avg_score'): 
            <span class="stars">
                {{$bank->stars()}}
            </span>
            (<span>{{$bank->formatedAvgMark()}}</span>)
        </h3>
        <div class="container-with-sidebar col-md-12 pl-0 pr-0">
            <div class="container-with-sidebar__content mb-20 col-md-9 pl-15 pr-15">
                <div class="card-border mb-20 " style="padding-bottom: 17px">
                <form id="review-filter-form" action="{{ route('review.bp_list',['id' => $bank->id, 'locale'=>App::getLocale()]) }}" method="GET">
                    <div class="row">
                        <div class="col-md-12" style="flex-wrap: wrap; display: contents;">
                             <div class="col-sm-12 pl-5 pr-5 mb-15" style="margin-bottom: 8px;">
                                <a class="tooltip tooltip-t" data-tooltip="Hududni tanlang" data-modal="region-list-modal" ><span id="region-name-selector">O'zbekiston Respublikasi</span><i class="fa fa-list ml-10"></i></a>
                                <input class="" type="hidden" name="region" value="" id="region-id-input"/>
                                {{-- <div class="form-group">
                                     <select name="region" class="select2__js review-ajax-filter" id="select-region">
                                        <option value="0">@lang('lang.regions')</option>
                                            @foreach($regions as $region)
                                                <option class="border-bottom" style="font-weight: bold;" value="{{ $region->id }}">{{ $region->name() }}</option>
                                                @foreach($region->children as $child_r)
                                                    <option value="{{ $child_r->id }}">&nbsp;&nbsp;{{ $child_r->name() }}</option>
                                                @endforeach
                                            @endforeach
                                    </select>
                                </div> --}}
                            </div>  
                            <div class="col-sm-3 pl-5 pr-5" style="margin-bottom: 8px;" id="banks-input-content">
                                <div class="form-group">
                                     <select name="branch" class="select2__js review-ajax-filter" id="select-organization">
                                        <option value="0">@lang('lang.v2.add_form.bank_branches')</option>
                                        @foreach($bank_offices as $office)
                                            <option value="{{ $office->id }}">{{ $office->getName() }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <i class="input-loader "></i>
                            </div> 
                            <div class="col-sm-3 pl-5 pr-5" style="margin-bottom: 8px;">
                                <div class="form-group">
                                     <select name="services" class="select2__js review-ajax-filter" id="select-services">
                                        <option value="0">@lang('lang.services')</option>
                                       @foreach($services as $service)
                                            @if(count($service->children()) > 0)
                                                <option value="{{ $service->id }}">{{ $service->getName() }}</option>
                                                @foreach($service->children() as $child)
                                                    <option value="{{ $child->id }}">&nbsp;&nbsp;{{ $child->getName() }}</option>
                                                @endforeach
                                            @else
                                                <option value="{{ $service->id }}">{{ $service->getName() }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3 pl-5 pr-5" style="margin-bottom: 8px;">
                                <div class="form-group">
                                     <select name="assessment" class="select2__js review-ajax-filter" id="select-assessment">
                                        <option value="0">@lang('lang.assessment')</option>
                                       @foreach(config("global.review.assessment_text")  as $at)
                                        <option value="{{ $at }}">@lang('lang.v2.assessment_text.'.$at)</option>
                                       @endforeach
                                    </select>
                                </div>
                            </div>
                            @php
                                $i = 1;
                            @endphp
                            <div class="col-sm-3 pl-5 pr-5" style="margin-bottom: 8px;">
                                <div class="form-group">
                                     <select name="answer_status" class="select2__js review-ajax-filter" id="select-status">
                                        <option value="0">@lang('lang.v2.review')</option>
                                        @foreach(config('global.review.answer_status') as $st_k => $st_v)                                            
                                            <option value="{{ $i++ }}">
                                                {{trans_choice('lang.v2.answer_status_for_banker.'.$st_v, 1)}} 
                                            </option>
                                            <option value="{{ $i++ }}">
                                                {{trans_choice('lang.v2.answer_status_for_banker.'.$st_v, 2)}} 
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                   </form>
                </div>
                <div class="review-list" id="review-result-content">
                    @include('frontend.reviews.bp_reviews-content',['reviews' => $reviews])
                </div>
            </div>
            
            <div class="container-with-sidebar__sidebar col-md-3 pl-15 pr-15" style="width: auto;">
                <div class="card-border mb-20 rating-reviews">
                    <h3 class="sm-title">@lang('lang.v2.bank_profile.text_right')</h3>
                    @php
                        $i = 0;
                    @endphp
                    <table style="width: 100%">
                        <tr id="0"><td style="display: none;"></td><td class="rating-bank" style="width: 13rem">@lang('lang.total')</td><td>{{$counter[$i++]}}</td></tr>
                        @foreach(config('global.review.answer_status') as $st_k => $st_v)                                            
                            <tr><td style="display: none;"></td><td class="rating-bank">{{trans_choice('lang.v2.answer_status_for_banker.'.$st_v, 1)}}</td><td id="trinfo{{$i}}">{{$counter[$i++]}}</td></tr>
                            <tr><td style="display: none;"></td><td class="rating-bank">{{trans_choice('lang.v2.answer_status_for_banker.'.$st_v, 2)}}</td><td id="trinfo{{$i}}">{{$counter[$i++]}}</td></tr>                                                        
                        @endforeach
                    </table>
                </div>
                <div class="rating-reviews card-border">
                    <h3 class="sm-title">@lang('lang.top_services_ranking_name', ["name" => $bank->name()])</h3>
                    @php
                        $count = 1;    
                    @endphp
                    <table style="width: 100%">
                        @foreach ($top_services_for_single_bank as $element)
                        <tr>
                            <td style="width: 10%;">{{$count++}}</td>
                            <td style="width: 70%;">
                                <a href="{{ route('info-bank',['locale'=>App::getLocale(),'slug'=>$bank->slug]) }}" class="rating-bank" target="_blank">{{$bank->name()}}</a>
                                <span class="rating-specification">{{$element['service_name']}}</span>
                            </td>
                            <td style="width: 20%;">{{$element['rank']}}</td>
                        </tr>
                        @endforeach                     
                    </table>
                </div>
            </div>

        </div>
    </div>

    <?php //echo  htmlspecialchars_decode(Share::currentPage()->facebook()); ?> 
    
    <?php /* echo  htmlspecialchars_decode(Share::page('http://jorenvanhocht.be', 'Share title')
    ->facebook()
    ->twitter()
    ->linkedin('Extra linkedin summary can be passed here')
    ->whatsapp()); */?>

    <div class="modal-ui" data-modal-name="region-list-modal">
    <div class="modal-ui__dialog detailed-search">
        <div class="modal-ui__content">
            <div class="modal-ui__close-btn"></div>
            <h2 class="b-title">@lang('lang.city_uzb')</h2>
           {{--  <div class="col-md-12">
                <div  class="form-group">
                    <input id="region-data-list-input"  class="form-control" placeholder="Hudud nomini kiriting">
                </div>
            </div> --}}

            <div class="col-md-12 pl-0 pr-0">
                    <span class="modal-region-span select-region-span mb-15" style="display: inherit;cursor: pointer;" data-id="">@lang('lang.whole_republic')</span>
                    <div style="max-height: 350px; overflow-y: auto;" >
                    <div class="col-md-12" style=" flex-wrap: wrap;display: flex;">
                    <div class="col-sm-3">
                        @foreach($regions_list as $regionk => $regionv)
                            @if(in_array($regionk, ["A", "B", "C", "D", "E", "F", "G", "I", "H", "А", "Б", "В", "Г"]))
                                <b class="mt-15" style="display: grid;">{{ $regionk }}</b>
                                @foreach($regionv as $rk => $rv)
                                <span class="modal-region-span select-region-span" style="display: inherit;cursor: pointer;" data-id="{{ $rv['id'] }}" >{{ $rv['name'] }}</span>
                                @endforeach
                            @endif
                        @endforeach
                    </div>

                    <div class="col-sm-3">

                         @foreach($regions_list as $regionk => $regionv)
                            @if(in_array($regionk, ["J", "K", "L", "M", "N", "O", "Д", 'Е', "Ё", "Ж","P", "З", "И", "Й", "К", "Л", "М", "Н", "О"]))
                                <b class="mt-15" style="display: grid;">{{ $regionk }}</b>
                                @foreach($regionv as $rk => $rv)
                                <span class="modal-region-span select-region-span" style="display: inherit;cursor: pointer;" data-id="{{ $rv['id'] }}" >{{ $rv['name'] }}</span>
                                @endforeach
                            @endif
                        @endforeach
                    </div>

                    <div class="col-sm-3">
                         @foreach($regions_list as $regionk => $regionv)
                            @if(in_array($regionk, ["Q", "R", "S", "T", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц","Ч"]))
                                <b class="mt-15" style="display: grid;">{{ $regionk }}</b>
                                @foreach($regionv as $rk => $rv)
                                <span class="modal-region-span select-region-span" style="display: inherit;cursor: pointer;" data-id="{{ $rv['id'] }}" >{{ $rv['name'] }}</span>
                                @endforeach
                            @endif
                        @endforeach
                    </div>
                    
                    <div class="col-sm-3">
                        @foreach($regions_list as $regionk => $regionv)
                            @if(in_array($regionk, ["U", "V", "W", "X","Y", "Z", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я"]))
                                <b class="mt-15" style="display: grid;">{{ $regionk }}</b>
                                @foreach($regionv as $rk => $rv)
                                    <span class="modal-region-span select-region-span" style="display: inherit;cursor: pointer;" data-id="{{ $rv['id'] }}" >{{ $rv['name'] }}</span>
                                @endforeach
                            @endif
                        @endforeach
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
    <script>
        $(document).ready(function(){
            if(performance.navigation.type == 2){
                location.reload();
            }
            $(document).on('click', '.pagination a', function(e) {
                serch_form = $('#review-filter-form');
                e.preventDefault();

                paginate($(this).attr('href').split('page=')[1],serch_form.serialize());
            });



           $('.review-ajax-filter').on('change',function(e){
                form = $('#review-filter-form');
                e.preventDefault();
                $.ajax({
                    url:form.attr('action'),
                    method:form.attr('method'),
                    data:form.serialize()
                }).done(function(data){
                    $('#review-result-content').html(data)
                });
            });

           $('.select-region-span').on('click',function(e){
                id = $(this).attr('data-id');
                $('#banks-input-content').find('i').addClass('fa fa-refresh fa-spin fa-3x fa-fw');
                $.ajax({
                    url:"{{ route('select-bank-branches',['locale'=>App::getLocale()]) }}",
                    method:"GET",
                    data:{
                        id: {{$bank->id}},
                        region_id: id
                    }
                }).done(function(data){
                    $('#banks-input-content').find('i').removeClass('fa fa-refresh fa-spin fa-3x fa-fw');
                    $('#select-organization').html(data)
                });

                region_name = $(this).text();
                $('.modal-ui__close-btn').click();
                $('#region-name-selector').text(region_name);
                $('#region-id-input').val(id);

                form = $('#review-filter-form');
                e.preventDefault();
                $.ajax({
                    url:form.attr('action'),
                    method:form.attr('method'),
                    data:form.serialize()
                }).done(function(data){
                    $('#review-result-content').html(data)
                });

           });
        });

        function paginate(page,search) {
            $.ajax({
                url: '?page=' + page,
                dataType: 'json',
                data:search,
            }).done(function(data) {
                $('#review-result-content').html('');
                $('#review-result-content').html(data);
                location.hash = page;
            });
        }
        $('.toggleable').on('click', function() {
            let toggleText =$(this);   
            let td1 = $('#trinfo1');
            let td2 = $('#trinfo2'); 
            let n1 = parseInt(td1.text());
            let n2 = parseInt(td2.text());
            $.ajax({
                method: 'GET',
                url: $(this).attr('data-href'),
            }).done(function() {
                if(toggleText.hasClass("readed")) {
                toggleText.removeClass("readed");
                toggleText.removeClass("btn-xn-default");
                toggleText.addClass("btn-xn-primary");
                n1--;
                n2++;   
                } else {                                  
                    toggleText.addClass("readed");
                    toggleText.removeClass("btn-xn-primary");  
                    toggleText.addClass("btn-xn-default");
                    n1++;
                    n2--; 
                }
                td1.text(n1);
                td2.text(n2);
                let old = toggleText.text();
                toggleText.text(toggleText.attr('data-text'));
                toggleText.attr('data-text',old);
            });                        
        });
        // $('.nextpage').on('click', function() {
        //     let toggleText =$(this).parent().find('.toggleable');   
        //     let td1 = $('#trinfo1');
        //     let td2 = $('#trinfo2'); 
        //     let n1 = parseInt(td1.text());
        //     let n2 = parseInt(td2.text());
        //     if(!toggleText.hasClass("readed")) {                                  
        //         toggleText.addClass("readed");
        //         toggleText.removeClass("btn-xn-primary",100);  
        //         toggleText.addClass("btn-xn-default");
        //         n1++;
        //         n2--; 
        //         td1.text(n1);
        //         td2.text(n2);
        //         let old = toggleText.text();
        //         toggleText.text(toggleText.attr('data-text'));
        //         toggleText.attr('data-text',old);
        //     }
        // });
    </script>
@endsection