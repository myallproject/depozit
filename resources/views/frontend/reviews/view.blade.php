@extends('layouts.app')

@section('meta')
   <title>{{ $review->title }}</title>
@endsection

@section('content')
<style>
.share-links{
    font-size: 25px;
    display: flex;
}
.share-links > div {
    margin: 0px 5px 0px 5px;
}
.mt-8{
    margin-top: 8px !important;
}
.text-justify{
    text-align: justify;
}
.tooltip:hover:before {
    top: -4px;
    left: 47px;
    transform: rotate(0deg);
    content: none;
}
.tooltip:hover:after {
    top: -4px;
    left: 0px;
}
.tooltip-t:hover:after {
    top: -30px !important;
    left: 0px;
}
.modal-region-span:hover{
    color: #0296ce;
}
.review-card.rejected {
    border: 1px solid #dedede;
}
.responseCard {
    background-color: rgb(245, 242, 242); 
}
.comment-item__avatar{
    background-size: 70%;
}
</style>
    <div class="sm-container page-gap">
        <div class="container-with-sidebar p-0" >
            <div class="container-with-sidebar__content pl-20 mb-20">
                <div class="review-card review-view pr-0  mt-0" {{ $review->status == 1 ? 'waiting' : '' }}>
                    <div class="review-card__meta">
                        <div class="d-flex align-items-center flex-wrap">
                           {{--  <div class="review-card__point {{ $review->assessmentClass() }}" data-tooltip="Baho tekshirilmoqda">
                                {{ $review->assessment }}
                            </div> --}}
                            @if($review->status != 0)
                                <div class="{{ $review->starClass() }} {{ $review->assessmentColor() }} mr-10" style="display: flex;" data-tooltip="@lang('lang.v2.review_waiting')">
                                    @for($i=1; $i <= 5; $i++)
                                        <i class="fa @if($i  <= $review->assessment) fa-star @else fa-star-o @endif"> </i>
                                    @endfor
                                </div>
                            @else
                                <div style="color:red" class="mr-10">@lang('lang.v2.review_rejected')</div>
                            @endif
                             <div class="review-card__date mr-10" style="font-size: 15px">{{ $review->created_at->format('d.m.Y') }}
                            </div>
                            <div class="review-card__date"><i class="fa fa-user-o"></i><span class="ml-10">{{ $review->user->getName() }}</span>
                            </div>
                            <div class="review-card__date">
                                 <span class="pl-20">
                                    <i class="fa fa-eye" style="font-size: 24px; color: #9a9a9a;"></i>
                                    <span >{{ $review->views }}</span>
                                 </span>
                            </div>
                        </div>
                        <div class="review-card__bank">
                            <img src="/{{ $review->bank->image }}" alt="">
                        </div>
                    </div>
                    <div class="review-view__title">{{ $review->title }}</div>
                    <div class="m-description text-justify">{{ $review->fulltext }}</div>
                    @if($review->service)
                        <div class="review-card__footer d-flex flex-wrap">
                            <div class="review-card__service mr-20">
                                {{ $review->service->getName() }}
                            </div>
                        </div>
                    @endif
                </div>

                <div class="col-md-12 share-links mt-8 pl-0" >
                    <?php echo  htmlspecialchars_decode(Share::page(route('review.view',['locale' => App::getLocale(),'id'=>$review->id]).'&quote='.$review->fulltext)->facebook()); ?>
                    <?php echo  htmlspecialchars_decode(Share::page(route('review.view',['locale' => App::getLocale(),'id'=>$review->id]),$review->fulltext)->twitter()); ?>
                    <?php echo  htmlspecialchars_decode(Share::page(route('review.view',['locale' => App::getLocale(),'id'=>$review->id]),$review->fulltext)->telegram()); ?>
                </div>

                <!--bank response-->
                @if(Auth::check() && Auth::user()->role_id == 11 && $review->comment_site !== null)
                    <div class="review-card card-border mt-20 responseCard" >
                        <div class="m-description text-justify">
                            <p style="margin: 0px;"><span style="font-weight: 600">@lang('lang.v2.add_form.info_bank'):</span>{{$review->comment_site}}</p>
                        </div>
                    </div>
                @endif  
                @if(!is_null($response))
                <div class="review-card card-border mt-20 responseCard" >
                    <div class="review-card__meta">
                        <div class="review-card__bank">
                            <img src="/{{ $review->bank->image }}" alt="">
                        </div>                          
                    </div>
                    
                    <p href="" class="review-card__title">
                        {{$review->bank->name()}}, <span class="review-card__date" style="font-size: 0.9em;"> {{ $response->created_at->format('d.m.Y') }}</span>
                    </p>
                    
                    <div class="m-description text-justify">
                        {{$response->text}}
                    </div>
                </div>
                @endif
                <!--Комментарии-->
                <div class="mb-60" id="comments"></div>
                <h2 class="sm-title mb-40">@lang('lang.comments'):
                    @if(!is_null($response)) {{ (int)$review->comments_count - 1 }}
                    @else {{ (int)$review->comments_count}}
                    @endif
                </h2>
                <div class="comments-list">
                    @if(count($comments) > 0)
                        @include('frontend.reviews.comment_nestable')
                    @endif

                    <div class="comment-form mt-20">
                        <form action="{{ route('comment.add.form',['locale' => App::getLocale()]) }}" method="post">
                            @csrf
                            <input type="hidden" name="review_id" value="{{ $review->id }}">
                            <input type="hidden" name="parent_id" value="">
                            <div class="form-group-xn">
                                <label for="commentLabel"></label>
                                <textarea name="text" id="commentLabel" placeholder="@lang('lang.text_comment')"></textarea>
                            </div>
                            <div class="d-flex flex-wrap mt-10">
                                <button class="btn-xn btn-xn-primary mr-10" type="submit">@lang('lang.add_comment')</button>
                                <button class="btn-xn btn-xn-default" data-show="false" id="cancelComment" type="button">@lang('lang.cancel')</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
            @if(!(Auth::check() && Auth::user()->role_id == 11))
                <div class="container-with-sidebar__sidebar pl-20">
                    <div class="card-border mb-20">
                        <span class="d-block m-description" style="line-height: 17px; margin-bottom: 8px">@lang('lang.v2.index_page.text_right')</span>
                        <a href="{{ route('review.add',['locale' => App::getLocale()]) }}" class="btn-xn btn-xn-primary btn-xn-justify">
                            @lang('lang.v2.index_page.text_btn')
                        </a>
                    </div>
                </div>
            @endif        
        </div>
    </div>
@endsection


@section('script')
    <script>
        const body = $('body');

        function getForm(review_id = '', parent_id = '', showCancelBtn = true) {
            return '<div class="comment-form mt-20">\n' +
                '          <form action="{{ route('comment.add.form',['locale' => App::getLocale()]) }}" method="post">\n' +
                '            @csrf' +
                '            <input type="hidden" name="review_id" value=' + review_id + '>' +
                '            <input type="hidden" name="parent_id" value=' + parent_id + '>' +
                '            <div class="form-group-xn">\n' +
                '              <label for="commentLabel"></label>\n' +
                '              <textarea name="text" id="commentLabel" placeholder="@lang('lang.text_comment')"></textarea>\n' +
                '            </div>\n' +
                '            <div class="d-flex flex-wrap mt-10">\n' +
                '              <button class="btn-xn btn-xn-primary mr-10" type="submit">@lang('lang.add_comment')</button>\n' +
                '              <button class="btn-xn btn-xn-default" data-show='+showCancelBtn+' id="cancelComment" type="button">@lang('lang.cancel')</button>\n' +
                '            </div>\n' +
                '          </form>\n' +
                '        </div>'
        }

        $('.to-comment').on('click', function (e) {
            e.preventDefault();
            $('.comment-form').remove();
            let parent_id =  $(this).attr('data-parent-id');
            $(this).parent().append(getForm({{ $review->id }}, parent_id, true));
        });

        body.on('click', '#cancelComment', function (e) {
            e.preventDefault();
            $('.comment-form').remove();
            $('.comments-list').append(getForm({{ $review->id }}, null, false));
        });
    </script>
@endsection
