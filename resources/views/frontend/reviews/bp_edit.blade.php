@extends('layouts.app')

@section('title')
   <title> @lang('lang.v2.reviews_metaTitle')</title>
@endsection

@section('content')
<style>
.share-links{
    font-size: 25px;
    display: flex;
}
.share-links > div {
    margin: 0px 5px 0px 5px;
}
.mt-8{
    margin-top: 8px !important;
}
.text-justify{
    text-align: justify;
}
.tooltip:hover:before {
    top: -4px;
    left: 47px;
    transform: rotate(0deg);
    content: none;
}
.tooltip:hover:after {
    top: -4px;
    left: 0px;
}
.tooltip-t:hover:after {
    top: -30px !important;
    left: 0px;
}
.modal-region-span:hover{
    color: #0296ce;
}
.review-card.rejected {
    border: 1px solid #dedede;
}
.responseCard {
    background-color: rgb(245, 242, 242); 
}
.comment-item__avatar{
    background-size: 70%;
}
</style>
    <div class="sm-container page-gap">
        <div class="container-with-sidebar p-0" >
            <div class="container-with-sidebar__content pl-20 mb-20">
                <div class="comments-list">
                    <div class="comment-form mt-20">
                        <form action="{{ route('review.bp_edit_response',['locale' => App::getLocale()]) }}" method="post">
                            @csrf
                            <input type="hidden" name="response_id" value="{{$response->id}}">
                            <input type="hidden" name="bank_id" value="{{$bank_id}}">
                            <div class="form-group-xn">
                                <label for="commentLabel"></label>
                                <textarea name="text" placeholder="@lang('lang.text_comment')">{{$response->text}}</textarea>
                            </div>
                            <div class="d-flex flex-wrap mt-10">
                                <button class="btn-xn btn-xn-primary mr-10" type="submit">@lang('lang.v2.bank_profile.edit_response')</button>
                            </div>
                        </form>
                    </div>                    
                </div>
            </div>      
        </div>
    </div>
@endsection
