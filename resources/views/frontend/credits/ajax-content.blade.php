@if(count($data['max_cr_id'])  <= 0)
    <div class="card-body text-center pb-3 pt-3 col-md-12 border-bottom border-r-8 shadow-lg--hover  bg-white mb-2">
        <h3 class="mt-3 mb-3">@lang('lang.not_found')</h3>
    </div>
@endif
@php
    $count_sub_row_dp = 0;
@endphp
@foreach($data['max_cr_id'] as $row)
    @php
        $count_sub_db = 0;
        $count_sub_row_dp += 1;
    @endphp
    @foreach($data['data'] as $key =>  $sub_row)
        @if($row->bank_id == $key)
            @foreach($sub_row as $row_s)
                @if($row_s->id != $row->id)
                    @php $count_sub_db += 1; @endphp
                @endif
            @endforeach
        @endif
    @endforeach
<div class="filter-item" style="border: none">
        <div class="tr">
            <div class="td">
                <div class="logo-wrap" style="text-align: center !important; border-radius: 30px !important;">
                    
                    <div class="logo">
                        <img src="/{{ $row->bank->image }}" alt="Image Bank">
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="label" >
                            <span class="name" style="margin-bottom: 8px"></span>
                            <span class="o">
                                <a data-bank-id="{{ $row->bank->id }}" data-this-id="{{ $row->id }}"
                                   data-click-status="false" data-filter-type="{{ $filter_type }}"
                                   data-cr-list-url="" class="btn-others-credit text-white">
                                    @lang('lang.others') {{ $count_sub_db }} <i class="fa fa-angle-down"></i>
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="td d-flex flex-column justify-content-center">
                <span class="name" style="width: -webkit-fill-available">
                    <a  target="_blank" href="{{ route('view-credit',[$row->slug]) }}">
                       "{{ $row->name_uz }}"
                    </a><br>
                </span>
                <div class="big">{{ $row->percent }}%</div>
            </div>
            <div class="td d-flex flex-column justify-content-center" style="padding-right: 10px !important;">
                <div class="big">{{ $row->date_credit." ".$row->creditDate->name() }}</div>
            </div>
             <div class="td d-flex flex-column justify-content-center" style="padding-right: 10px !important;">
                <div class="big">{{ number_format($row->amount)." ".$row->currencyName->name }}</div>
                <div class="small">@lang('lang.quantum')</div>
            </div>
            <div class="td d-flex align-items-center">
                <div class="big" style="font-size: 18px">
                    @if($row->provision_id)
                    {{ $row->provision->name()}}
                    @else
                    --
                    @endif
                </div>
            </div>
            <div class="td d-flex flex-column align-items-lg-end align-items-sm-center justify-content-center mt-20 mb-20">
                 <a
                   data-add-route="{{ route('add-comparison') }}"
                   data-remove-route="{{ route('remove-comparison') }}"
                   data-name="{{ $row->name_uz}}" id="btn-id-{{ $row->id }}"
                   data-status="" data-id="{{ $row->id }}" class="add-to-balance add-comparison"
                   style="cursor:pointer; border: none; background-size: 20px; width: 32px; height: 32px;">
                </a>
                <a href="#" class="btn btn--medium btn--blue mb-10">@lang('lang.send')</a>
                <a href="#" class="btn btn--medium btn--blue btn--arrow">@lang('lang.detailed')</a>
            </div>
        </div>
        <div class="tr-more">
           @include('frontend.credits.detailed-tab')
        </div>
    </div>
    <div class="why">
        <div class="arrow-card__divider" style="width: 90vw"> </div>
    </div>
    <div id="others-credit-content-{{$row->id}}" style="display: none; margin-bottom: 25px;" class="min-content-father"> </div>

    @php $count_sub_row_dp += $count_sub_db; @endphp
@endforeach

<input id="count-credit" type="hidden" value="{{ $count_sub_row_dp }}">


<script src="{{ asset('/js/credit.ajax.content.js') }}"></script>

@php
    $sess = Session::get('comparison-credit');
@endphp
@if($sess and count($sess) > 0)
    @foreach($sess as $k => $v)
       <script>
            $('#btn-id-'+'{{ $v }}').attr('data-status',true).addClass('added-to-balance');
        </script>
    @endforeach
@endif
