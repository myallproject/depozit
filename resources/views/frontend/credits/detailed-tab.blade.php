<div class="head">
    <div class="divider"></div>
</div>
<div class="content">
    <div class="tab-nav">
        <a href="#" class="active">@lang('lang.rules')</a>
        <a href="#">@lang('lang.table_rates')</a>
        <a href="#">@lang('lang.requirement')</a>
        <a href="#">@lang('lang.documents')</a>
        <a href="#">@lang('lang.about_d_bank')</a>
    </div>
    <div class="tab-caption">
        <div class="tab-pane active">
            <div class="row">
                <div class="col-md-6">
                    <ul>
                        <li><span class="sub font-16">@lang('lang.date'):</span>  <span>{{ $row->date_credit." ".$row->creditDate->name() }}</span>
                        </li>
                        <li><span class="sub font-16">@lang('lang.quantum'):</span>  <span>{{ number_format($row->amount)." ".$row->currencyName->name }}</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li>- talab qilinadigan daromadni tasdiqlovchi hujjat</li>
                        <li>-</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-pane">
            <div class="table percentage-table">
                <div class="thead">
                    <div class="tr">
                        <div class="th">@lang('lang.quantum')</div>
                        <div class="th text-lowercase" >{{ $row->date_credit." ".$row->creditDate->name() }}</div>
                        <div class="th"></div>
                        <div class="th"></div>
                    </div>
                </div>
                <div class="tbody">
                    <div class="tr">
                        <div class="td new-td">
                            <div>{{ number_format($row->amount)." ".$row->currencyName->name }}</div>
                        </div> 
                        <div class="td new-td">
                            <div>{{ $row->percent }}%</div>
                        </div>
                        <div class="td new-td"></div>
                        <div class="td new-td"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane">
            <div class="row">
                <div class="col-md-4">
                    <div class="col-title">@lang('lang.borrower_cat')</div>
                    <ul>
                        <li>- {{ $row->credit_borrower_category_id }}</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="col-title">@lang('lang.borrower_age')</div>
                    <ul>
                        <li>- {{ $row->credit_borrower_age }} </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="col-title">@lang('lang.work_experience')</div>
                    <ul>
                        <li>-</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-pane">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        @if($row->documents())
                            {{ mb_substr ($row->documents(),'0',200, 'UTF-8')  }}...
                        @else
                            -
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-pane">
            <div class="row">
                <div class="col-md-5">
                    <div class="col-title">@lang('lang.bank_information')</div>
                    <ul>
                        <li>@lang('lang.phone'): &nbsp; {{ $row->bank->phone }}</li>
                        <li>@lang('lang.address'): &nbsp; {{ $row->bank->information->address() }}</li>
                        <li>@lang('lang.site'):  &nbsp; <a href="{{ $row->bank->information->site() }}" target="_blank">{{ $row->bank->information['site'] }}</a></li>
                        <li>@lang('lang.telegram_channel'): &nbsp; {{ $row_f->bank->information->telegram_channel }}</li>
                        {{--<li>@lang('lang.general_office'):</li>--}}
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="col-title">@lang('lang.rating')</div>
                    <ul>
                        <li>
                            <span class="sub">@lang('lang.financial_rating'):</span>
                            <span>@if($row->bank->ratings){{ $row->bank->ratings->moliyaviy }}@else - @endif</span>
                        </li>
                        <li><span class="sub">@lang('lang.popular_rating'):</span>
                            <span>@if($row->bank->ratings){{ $row->bank->ratings->ommabop }}@else - @endif</span>
                        </li>
                        <li><span class="sub">@lang('lang.average_rating'):</span>
                            <span>@if($row->bank->ratings){{ $row->bank->ratings->urtacha }}@else - @endif</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="col-title">@lang('lang.ownership')</div>
                    <ul>
                        @foreach($type_bank as $tp_bk)
                            @if($row->bank->bank_type_id == $tp_bk->id)
                                <li>
                                    <span><i class="fa fa-check"></i></span>
                                    <span>{{ $tp_bk->name() }}</span>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="foot">
    <div class="divider"></div>
    <a href="{{ route('view-credit',[$row->slug]) }}" class="btn btn--medium btn--blue">@lang('lang.more_detailed')</a>
    <a href="{{ $row->link(App::getLocale()) }}" target="_blank" class="btn btn--medium btn--light_blue">@lang('lang.go_site')</a>
</div>