<form action="{{ route('filter-new-auto-credits',['locale'=>App::getLocale()]) }}" id="credit-filter-form-small" method="GET">
    <input type="hidden" name="credit_type_id" value="{{ $credit_type_id }}">
    <div class="row">
        <div class="col-md-5 pr-0 pl-0 form-display">
            <div class="col-md-12">
                <div class="form-group mt-20 no-tick">
                    <label for="car-brand-small">Avtomobil markasi</label>
                    <select class="select2__js filter-credit on-change-filter" name="car_brand" id="car-brand-small"  data-empty="@lang('lang.any')" data-focus="false" data-childred-url="{{ route('select-car-brands',['locale' => App::getLocale()]) }}">
                        <option value="0">@lang('lang.any')</option>
                        @foreach($car_brands as $car_brand)
                            <option value="{{ $car_brand->id }}">{{ $car_brand->name() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12" id="car-small-div">
                <div class="form-group mt-20 no-tick">
                    <label for="car-small">Avtomobil modeli</label>
                    <select class="select2__js filter-credit on-change-filter" disabled name="car" id="car-small"  data-empty="@lang('lang.any')" data-focus="false">
                        <option value="0">@lang('lang.any')</option>
                    </select>
                </div>
                <i class="input-loader"></i>
            </div>
            <div class="col-md-12">
                <div class="form-group mt-20">
                    <label>@lang('lang.first_compensation')</label>
                    <select class="select2__js filter-credit on-change-filter" name="compensation" id="first-compensation-small"  data-empty="@lang('lang.any')" data-focus="false">
                        <option value="0">@lang('lang.any')</option>
                        @foreach($bosh_badal as $badal)
                            <option value="{{ $badal->id }}">{{ $badal->name() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group mt-20">
                    <label>@lang('lang.date')</label>
                    <select class="select2__js filter-credit on-change-filter clear" name="date" id="f-el-date-small"  data-empty="@lang('lang.any')" data-focus="false">
                        <option value="0">@lang('lang.any')</option>
                        @foreach($dates as $date)
                            <option value="{{ $date->id }}">{{ $date->name() }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group mb-15 mt-10">
                    <input type="checkbox" id="online-credit-input" class="minimal" name="online_credit"  data-form-id="#credit-filter-form-small">
                    <label class="label clear-checkbox" style="display: initial">
                        @lang('lang.online_credits')
                    </label>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="car-img">
                <img src="/uploads/cars/defaultcar.webp" alt="default car image" id="car-image">
            </div>
            <div class="col-md-12 credit-filter-buttons">
                <button class="btn btn--medium btn--blue filter-credit-btn-small">@lang('lang.selection')</button>
                <button class="btn btn--medium btn--blue btn--arrow" data-modal="detailed-credit-modal">@lang('lang.addition_rules')</button>
            </div>
        </div>



        
    </div>
    <input type="hidden" name="single_bank_id" value="{{$single_bank_id}}">
</form>
