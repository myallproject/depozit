<form action="{{ route('filter-micro-credits',['locale'=>App::getLocale()]) }}" id="credit-filter-form-small" method="GET">
    <input type="hidden" name="credit_type_id" value="{{ $credit_type_id }}">
    <div class="row">
         <div class="col-md-9 pr-0 pl-0 form-display">
            <div class="col-md-6">
                <div class="form-group mt-20">
                    <label >@lang('lang.sum_credit')</label>
                    <input type="text" name="amount" id="f-el-amount" data-validate="true" data-error-text="@lang('validation.numeric',['attribute' => trans('lang.value')])" class='filter-credit' placeholder="1 000 000" />
                    <div class="slidecontainer">
                      <input type="range" min="" step="100000" max="50000000" value="0" class="slider" id="myRange">
                    </div>
                    <span id="amount-error" class="text-danger" style="display: none"></span>
                </div>
            </div>

        <div class="col-md-6">
            <div class="form-group mt-20" >
                <label>@lang('lang.credit_date')</label>
                <select class="select2__js clear border-r-3 on-change-filter" data-focus="false" id="f-el-date-type" name="date"  data-empty="@lang('lang.any')" >
                    <option value="0">@lang('lang.whatever')</option>
                    @foreach($dates as $date)
                        <option value="{{ $date->id }}">{{ $date->name() }}</option>
                    @endforeach
                </select>
            </div>
            {{--<div class="form-group">
                <label >@lang('lang.type_currency')</label>
                <select  class='select2__js filter-credit' name="currency" id="f-el-currency"  data-empty="@lang('lang.any')">
                    @foreach($currency as $cur)
                        <option value="{{ $cur->id }}">{{ $cur->name }}</option>
                    @endforeach
                </select>
            </div>--}}
        </div>

       {{-- <div class="col-md-3" >
            <div class="form-group">
                <label>@lang('lang.first_compensation')</label>
                <select class="select2__js filter-credit" name="compensation"  data-empty="@lang('lang.any')">
                    <option value="0">@lang('lang.any')</option>
                    <option value="1">@lang('lang.required')</option>
                    <option value="2">@lang('lang.not_required')</option>
                </select>
            </div>
        </div>--}}
            <div class="col-md-12">
                <div class="form-group mb-15 mt-10">
                    <input type="checkbox" id="online-credit-input" class="minimal" name="online_credit"  data-form-id="#credit-filter-form-small">
                    <label class="label clear-checkbox" style="display: initial">
                        @lang('lang.online_credits')
                    </label>
                </div>
            </div>

        </div>



        <div class="col-md-3 credit-filter-buttons">
            <button class="btn btn--medium btn--blue filter-credit-btn-small">@lang('lang.selection')</button>
            <button class="btn btn--medium btn--blue btn--arrow" data-modal="detailed-credit-modal">@lang('lang.addition_rules')</button>
        </div>
    </div>
    <input type="hidden" name="single_bank_id" value="{{$single_bank_id}}">
</form>
