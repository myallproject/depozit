@extends('layouts.app')
@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('content')
<link rel="stylesheet" href="/plugins/bower_components/icheck/all.css" />
<link rel="stylesheet" type="text/css" href="/css/credits.css">
<link rel="stylesheet" type="text/css" href="/css/slider-range.css">
    <style>
        
    </style>
    <div class="section">
        <div class="section-gap--small"  style="margin-top: 20px">
            <div class="medium-container">
                <h1 class="b-title mb-20">@lang('lang.education_credit')
                    <span  style="font-size: 80%"> <span id="count-result" data-text="@lang('lang.select_credit')"></span></span>
                    {{-- <div class="position-relative">
                        <span class="update-time">
                             <span >{{ DateMonthName::updateDateName($update_time['data_update_time']) }}</span>
                             <span >@lang('lang.update_time')</span>
                            <img src="/temp/images/update_time.png" width="25" />
                        </span>
                    </div> --}}
                </h1>
                <div class="filter-form">
                    @include('frontend.credits.filters.filter-education')
                </div>
               <div  id="progress-bar" style="width: 100%; background-color: rgba(58,165,208,.4); display: none"><div id="bar" style="width: 1%; height: 3px; background-color: #024c67;"></div></div>
                <div class="filter-result" style="margin-top: 0px !important;">
                    <div class="filter-result__title"> </div>
                    <div class="table filter-table">
                        <div class="thead">
                            <!-- <div class="tr">
                                <div class="th">@lang('lang.bank')</div>
                                <div class="th">@lang('lang.effect_percent')</div>
                                <div class="th">@lang('lang.amount')</div>
                                <div class="th">@lang('lang.requirements')</div>
                                <div class="th"></div>
                            </div> -->
                            
                            <div class="tr"  style="font-size: 16px">
                                <div class="th" style="width: 16%">@lang('lang.bank')</div>
                                <div class="th" style="white-space: nowrap;width: 16%">@lang('lang.effect_percent') <i style="cursor:pointer;" data-sort="asc" class="fa fa-sort-amount-asc sort-percent" id="sort-percent"></i></div>
                                <div class="th" style="width: 16%">@lang('lang.date')</div>
                                <div class="th" style="width: 16%">@lang('lang.quantum')</div>
                                <div class="th" style="width: 16%">@lang('lang.provision')</div>
                                <div class="th" style="width: 16%"></div>
                            </div>

                        </div>
                        <div class="tbody" id="credit-result">

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @include('frontend.credits.modals.modal-education')
@endsection
@section('script')
     <script type="text/javascript" src="/plugins/bower_components/icheck/icheck.min.js"></script>
    <script>
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        });
        $('#content-result-filter').html('@include("frontend.components.loader")');
        
    </script>
    <script src="{{ asset('/js/credits/education.js') }}"></script>
    <script src="{{ asset('/js/slider-range.js') }}"></script>

    <script type="text/javascript">
        $('.filter-credit-btn-large').on('click',function(){
            $('#f-el-amount').val($('#large-search-amount-id').val());
            $('#f-el-date').val($('#large-f-el-date').val()).trigger('change');
            $('#f-el-currency-small').val($('#f-el-currency-large').val()).trigger('change');
        });
    </script>

     <script>

       $('.sort-percent').on('click',function(){
            if($(this).attr('data-sort') == 'asc'){
                $('#credit-result .item-section').sort(sort_div_desc).appendTo('#credit-result');
                $(this).attr('data-sort','desc');
                $(this).toggleClass('fa-sort-amount-asc').toggleClass('fa-sort-amount-desc');
            }else if($(this).attr('data-sort') == 'desc'){
                $('#credit-result .item-section').sort(sort_div_asc).appendTo('#credit-result');
                $(this).attr('data-sort','asc');
                $(this).toggleClass('fa-sort-amount-desc').toggleClass('fa-sort-amount-asc');
            }
       });

       function sort_div_desc(a,b){ 
            return ($(b).data('percent')) > ($(a).data('percent')) ? 1 : -1;
       }
       function sort_div_asc(a,b){
            return ($(b).data('percent')) < ($(a).data('percent')) ? 1 : -1;
       }
    </script>
@endsection