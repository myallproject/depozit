<div class="head">
    <div class="divider"></div>
</div>
<div class="content">
    <div class="tab-nav">
        <a href="#" class="active main-tab">@lang('lang.rules')</a>
        <a href="#">@lang('lang.percent_rates') @if($row->otherCreditList()) * @endif</a>
        <a href="#">@lang('lang.documents')</a>
        <a href="#">@lang('lang.requirement')</a>
        @if($row->aktsiya)
            <a href="#" class="aktsiya-tab">*@lang('lang.promotion')</a>
        @endif
        <a href="#">@lang('lang.about_d_bank')</a>
    </div>
    <div class="tab-caption">
        <div class="tab-pane active main-tab">
            <div class="row">
                <div class="col-md-6">
                    <ul>
                        <li>
                            <span class="sub font-16">@lang('lang.date'):</span>&nbsp;<span class="text-lowercase" style="text-transform: none !important;">
                            @if(!$row->otherCreditList())
                                {{ $row->date_credit." ".$row->creditDate->name($row->date_credit) }}
                            @else
                            @php
                                $len = count($row->otherCreditList());
                                $arr = [];
                            @endphp
                                @foreach($row->otherCreditList() as $key => $other_date)
                                    @php
                                        array_push($arr, $other_date->datename());
                                    @endphp
                                @endforeach
                                @php
                                $result = array_unique($arr);
                                    echo(end($result));
                                @endphp
                            @endif
                            </span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.date_privilege'):</span>  <span>@if($row->date_privilege_uz or $row->date_privilege_ru){{ $row->privilege() }}@else @lang('lang.does_not_exist') @endif</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.quantum'):</span>  <span>@if($row->text_amount_uz or $row->text_amount_ru) {{ $row->textAmount() }}  @else -- @endif</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.way_legalization'):</span>  <span>@if($row->rasmiylashtirish) {{ $row->legitimation(App::getlocale()) }}  @else -- @endif</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li>
                            <span class="sub font-16">@lang('lang.first_compensation'):</span>  <span> @if($row->compensation_status) @if($row->compensation() == "Отсутствует") Не требуется @else {{ $row->compensation() }} @endif @else @lang("lang.not_required") @endif</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.type_provision'):</span>  <span> @if($row->provision_id) {{ $row->typeProvision() }} @else -- @endif</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.goal_credit'):</span>  <span> @if($row->credit_goal_id) {{ $row->creditGoalString() }} @else -- @endif</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-pane">
            <div class="table percentage-table table-3x">
                  <div class="thead">
                    <div class="tr">
                        <div class="th">@lang('lang.credit_date')</div>
                        <div class="th">@lang('lang.percent_rate')</div>
                        <div class="th">@lang('lang.first_compensation')</div>
                    </div>
                </div>
                <div class="tbody">
                    @if(!$row->otherCreditList())
                        <div class="tr">
                            <div class="td">
                                <div class="text-lowercase text-left">{{ $row->date_credit." ".$row->creditDate->name($row->date_credit) }}</div>
                            </div>
                            <div class="td">
                                <div>{{ $row->percent }}%</div>
                            </div>
                            <div class="td">
                                <div>@if($row->creditBoshBadal->name() == 'Отсутствует') Не требуется @else{{ $row->creditBoshBadal->name() }}@endif</div>
                            </div>
                        </div>
                    @else 
                        @foreach($row->otherCreditList() as $other_row)
                            <div class="tr">
                                <div class="td">
                                    <div class="text-lowercase text-left" style="text-transform: none !important;">{{ $other_row->datename() }}</div>
                                </div>
                                <div class="td">
                                    <div>{{ $other_row->percent }}%</div>
                                </div>
                                <div class="td">
                                    <div>@if($other_row->compensation_id) @if($other_row->compensation->name() == 'Отсутствует') Не требуется @else{{ $other_row->compensation->name() }} @endif @else @lang("lang.not_required") @endif</div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="tab-pane">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        @if($row->documents())
                            @php $docs = explode(';',$row->documents()); @endphp
                            @foreach($docs as $kdoc => $vdoc)
                                @if($vdoc)- <span class="doc-text">{{ $vdoc }};</span><br>@endif
                            @endforeach
                        @else
                            -
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-pane">
            <div class="row">
                <div class="col-md-6">
                    <ul>
                        <li>
                            <span class="sub font-16">@lang('lang.borrower_age'):</span>
                            <span>
                                @if(App::getLocale() == 'uz')
                                    @if($row->credit_borrower_age)
                                        {{$row->credit_borrower_age}}
                                    @else
                                        @lang('lang.does_not_important_2')
                                    @endif
                                @else 
                                    @if($row->credit_borrower_age_ru)
                                        {{$row->credit_borrower_age_ru}}
                                    @else
                                        @lang('lang.does_not_important_2')
                                    @endif
                                @endif
                            </span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.borrower_possible'):</span>
                            <span>
                                @if($row->credit_borrower_possible())
                                    {{$row->credit_borrower_possible()}}
                                @else
                                    @lang('lang.does_not_important')
                                @endif

                            </span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li>
                            <span class="sub font-16">@lang('lang.credit_citizenship'):</span>
                            <span>@if($row->credit_borrower_category_id){{ $row->citizenship->name() }}@else @lang('lang.does_not_important_2') @endif</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.borrower_additional'):</span>
                            <span>
                                @if($row->credit_borrower_additional())
                                    {{$row->credit_borrower_additional()}}
                                @else
                                    @lang('lang.does_not_important')
                                @endif
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        @if($row->aktsiya)
            <div class="tab-pane aktsiya-tab">
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li>
                                <span class="sub font-16">@lang('lang.detailed_info'):</span>
                                <span>{{ $row->aktsiya_izoh(App::getLocale()) }}</span>
                            </li>
                            <li>
                                <span class="sub font-16">@lang('lang.link'):</span>
                                <a href="{{ $row->aktsiya_link(App::getLocale()) }}" target="_blank">{{ $row->aktsiya_link(App::getLocale()) }}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="tab-pane">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-title">@lang('lang.bank_information')</div>
                    <ul>
                        <li>
                            <span class="sub font-16">@lang('lang.ownership'):</span>
                            <span>
                                @foreach($type_bank as $tp_bk)
                                    @if($row->bank->information->type_bank == $tp_bk->id)
                                        {{ $tp_bk->name() }}
                                    @endif
                                @endforeach
                            </span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.phone'):</span>
                            <span>{{ $row->bank->phone }}</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.address'):</span>
                            <span>{{ $row->bank->information->address() }}</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.site'):</span>
                            <span><a href="{{ $row->bank->information->site() }}" target="_blank">{{ $row->bank->information->site() }}</a></span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.telegram_channel'):</span>
                            <span><a href="https://t.me/{{ substr($row->bank->information->telegram_channel,1) }}" target="_blank"> {{ $row->bank->information->telegram_channel }}</a></span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.mobile_app'):</span>
                            <span>{{ $row->bank->information->mobile_app }} 
                                <a href="{{$row->bank->information->app_store}}" target="_blank"><i class="fab fa-app-store-ios fa-lg" style="color: #3aa5d0"></i></a> &nbsp; 
                                <a href="{{$row->bank->information->play_market}}" target="_blank"><i class="fab fa-google-play fa-lg"></i></a>
                            </span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="col-title">@lang('lang.rating')</div>
                    <ul>
                        <li>
                            <span class="sub font-16">@lang('lang.citizen_ranking'):</span>
                            <span><a href="{{route('ranking-citizen', ['locale' => App::getLocale()])}}" class="btn-outline-dark-blue btn-badge ex-sm-block" target="_blank">@lang('lang.ranking_position',['rank'=> cache($row->bank->citizenRankingKey()) ]) <i class="fas fa-external-link-alt"></i></a></span>
                        </li>
                        <li style="padding-top: 10px; padding-bottom: 10px;">
                            <a href="{{ route('review.add',['locale' => App::getLocale()]) }}" class="btn-xn btn-xn-primary btn-xn-justify" style="max-width: 200px; color: white !important;">
                                @lang('lang.v2.index_page.text_btn')
                            </a>
                        </li>
                        {{-- <li><span class="sub">@lang('lang.popular_rating'):</span>
                            <span>@if($row->bank->ratings){{ $row->bank->ratings->ommabop }}@else - @endif</span>
                        </li>
                        <li><span class="sub">@lang('lang.average_rating'):</span>
                            <span>@if($row->bank->ratings){{ $row->bank->ratings->urtacha }}@else - @endif</span>
                        </li> --}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="foot">
    <div class="divider"></div>
    <a href="{{ route('view-credit',['locale' => App::getLocale(),'slug'=>$row->slug]) }}" target="_blank" class="btn btn--medium btn--blue">@lang('lang.more_detailed')</a>
    <a href="{{ $row->link(App::getLocale()) }}" target="_blank" class="btn btn--medium btn--light_blue">@lang('lang.go_site')</a>
</div>