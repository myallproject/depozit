<div class="modal-ui" data-modal-name="detailed-credit-modal">
    <div class="modal-ui__dialog detailed-search">
        <div class="modal-ui__content">
            <div class="modal-ui__close-btn"></div>
            <h2 class="b-title">@lang('lang.addition_rules_search')</h2>
            <form action="{{ route('filter-large-education-credit',['locale'=>App::getLocale()]) }}" id="credit-filter-form-large" method="GET">
                <input type="hidden" name="credit_type_id" value="{{ $credit_type_id }}">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>@lang('lang.sum_credit')</label>
                            <input class="clear" id="large-search-amount-id" name="amount" data-validate="true" data-error-text="@lang('validation.numeric',['attribute' => __('lang.value')])">
                            <div class="slidecontainer">
                              <input type="range" min="" step="100000" max="100000000" value="0" class="slider clear" id="myRange2">
                            </div>
                            <span id="large-amount-error" class="text-danger" style="display: none"></span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="d-flex justify-content-between">
                            <div class="form-group" style="width:48%">
                                <label>@lang('lang.currency')</label>
                                <select class="select2__js" name="currency" id="f-el-currency-large" data-empty="@lang('lang.any')">
                                    @foreach($currency as $cur)
                                        <option value="{{ $cur->id }}">{{ $cur->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" style="width:48%">
                                <label>@lang('lang.date')</label>
                                <select class="select2__js clear" name="date" data-empty="@lang('lang.any')" id="large-f-el-date">
                                    <option value="0">@lang('lang.any')</option>
                                    @foreach($dates as $date)
                                        <option value="{{ $date->id }}">{{ $date->name() }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>@lang('lang.region')</label>
                            <select class="select2__js clear" name="region" id="region" data-empty="@lang('lang.any')">
                                <option value="0">@lang('lang.any')</option>
                                @foreach($cities as $city)
                                    <option class="border-bottom" style="font-weight: bold;" value="{{ $city->id }}">{{ $city->name() }}</option>
                                    @foreach($city->children as $child_r)
                                        <option value="{{ $child_r->id }}">&nbsp;&nbsp;{{ $child_r->name() }}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>@lang('lang.goal_credit')</label>
                            <select class="select2__js clear" name="goal" id="edu-cr-goal" data-empty="@lang('lang.any')">
                                <option value="0">@lang('lang.any')</option>
                                @foreach($goals as $goal)
                                    <option value="{{ $goal->id }}">{{ $goal->name() }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>@lang('lang.type_provision')</label>
                            <select class="select2__js clear" name="provision" id="edu-cr-provision" data-empty="@lang('lang.any')">
                                <option value="0">@lang('lang.any')</option>
                               @foreach($provisions as $provision)
                                <option value="{{ $provision->id }}">{{ $provision->name() }}</option>
                               @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>@lang('lang.first_compensation')</label>
                            <select class="select2__js clear" name="compensation" id="edu-cr-compensation" data-empty="@lang('lang.any')">
                                <option value="0">@lang('lang.any')</option>
                                <option value="1">@lang('lang.required')</option>
                                <option value="2">@lang('lang.not_required')</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 d-none">
                        <div class="form-group">
                            <label>@lang('lang.period_review')</label>
                            <select class="select2__js clear" name="review_period" data-empty="@lang('lang.any')">
                                <option value="0">@lang('lang.any')</option>
                                @foreach($review_period as $review)
                                    <option value="{{ $review->id }}">{{ $review->name() }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>@lang('lang.issuing_form')</label>
                            <select class="select2__js clear" name="credit_issuing_form" id="edu-cr-issuing-form" data-empty="@lang('lang.any')">
                                <option value="0">@lang('lang.any')</option>
                                @foreach($issuing_forms as $is_form)
                                    <option value="{{ $is_form->id }}">{{ $is_form->name() }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>@lang('lang.bank')</label>
                            <select class="select2__js clear" name="type_banks[]" multiple id="select-banks" data-empty="@lang('lang.any')" data-dropdown="open">
                                <option value="any">@lang('lang.any')</option>
                                <option value="state_bank">@lang('lang.state_bank')</option>
                                <option value="private_bank">@lang('lang.private_bank')</option>
                                <option value="foreign_bank">@lang('lang.foreign_bank')</option>
                                @foreach($banks as $bank)
                                    @if($bank->id == $single_bank_id)
                                        <option value="{{ $bank->id }}" selected> {{ $bank->name_uz }}</option>
                                    @else
                                        <option value="{{ $bank->id }}"> {{ $bank->name_uz }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                     @include('frontend.components.btn-type-bank')
                </div>
                <div class="buttons">
                    <div class="text-center mb-20">
                        <button type="submit" class="btn btn--medium btn--blue filter-credit-btn-large">@lang('lang.selection')</button>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn--medium btn--blue clear-filter">@lang('lang.clear_filter')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
