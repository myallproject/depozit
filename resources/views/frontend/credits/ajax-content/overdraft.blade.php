@if($single_bank_id<1)
    @if(count($data['max_cr_id'])  <= 0)
        <div class="card-body text-center pb-3 pt-3 col-md-12 border-bottom border-r-8 shadow-lg--hover  bg-white mb-2">
            <h3 class="mt-3 mb-3">@lang('lang.not_found')</h3>
        </div>
    @endif
    @php
        $count_sub_row_dp = 0;
    @endphp
    @foreach($data['max_cr_id'] as $row)
        @php
            $count_sub_db = 0;
            $count_sub_row_dp += 1;
        @endphp
        @foreach($data['data'] as $key =>  $sub_row)
            @if($row->bank_id == $key)
                @foreach($sub_row as $row_s)
                    @if($row_s->id != $row->id)
                        @php $count_sub_db += 1; @endphp
                    @endif
                @endforeach
            @endif
        @endforeach
         <?php $datepercent = $row->otherCreditDatePercent($credit_date); ?>
         <section class="item-section" data-date="" data-percent="{{ $datepercent['percent'] }}" data-date-type="">
            <div class="filter-item credits">
                <div class="tr">
                    <div class="td">
                        <div class="logo-wrap" style="text-align: center !important;">
                            <div class="logo">
                                <img src="/{{ $row->bank->image }}" alt="Image Bank">
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="label" >
                                    <span class="o">
                                        <a id="other-content-{{ $row->id }}" data-bank-id="{{ $row->bank->id }}" data-this-id="{{ $row->id }}"
                                           data-click-status="false" data-filter-type="{{ $filter_type }}"
                                           data-cr-list-url="{{ route('overdraft-credit-other-list',['locale'=>App::getLocale()]) }}" class="btn-others-credit">
                                            @lang('lang.others') {{ $count_sub_db }} <i class="fa fa-angle-down"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="td d-flex cr-name-div flex-column justify-content-center">
                        @if($row->status_suspended == 2)
                            <div class="tooltip" data-tooltip="@lang('lang.'.config('global.credits.activity')[$row->status_suspended].'')">
                                <div class="suspended">
                                    <span>
                                        <i class="fa fa-exclamation-triangle fa-cs"></i>
                                    </span>
                                </div>
                            </div>
                        @endif
                        <div class="aktsiya">
                            <span class="name">
                                <a target="_blank" href="{{ route('view-credit',['locale'=>App::getLocale(),'slug'=>$row->slug]) }}">
                                   "{{ $row->name()}}"
                                </a>
                            </span>
                            @if($row->aktsiya)
                                <div class="promotion">
                                    <img src="/temp/images/aktsiya.svg" alt="promotion image">
                                </div>
                            @endif
                        </div>
                        <div class="big">
                            @if(!$row->otherCreditList() || !$singlePrint)                                
                            {{ $datepercent['percent'] . '%'}}
                            @else 
                                {{$row->otherCreditUnique()['percent']}}
                            @endif
                        </div>
                    </div>
                    <div class="mobile-row-divider"></div>
                    <div class="td d-flex flex-column justify-content-center">
                        <div class="big text-lowercase" style="text-transform: none !important;">
                            @if(!$row->otherCreditList() || !$singlePrint)
                            @php
                                $printDate = explode(" ",  $datepercent['date']);
                                if(is_numeric($printDate[0]) || sizeof($printDate) == 1) echo($printDate[0] ." ".$row->creditDate->name($printDate[0]) );
                                else echo($printDate[1] ." ".$row->creditDate->name($printDate[1]) ); 
                            @endphp
                            @else 
                                {{$row->otherCreditUnique()['date']}}
                            @endif
                            <div class="small date" style="display: none;">@lang('lang.date')</div>
                        </div>
                    </div>
                   {{--  <div class="td d-flex flex-column justify-content-center">
                        <div class="big">
                           {{ $row->textAmount() }}
                        </div>
                        <div class="small">@lang('lang.quantum')</div>
                    </div> --}}
                    <div class="td d-flex align-items-center">
                        <div class="big" style="font-size: 18px">
                            @if($row->provision_id)
                                <?php echo html_entity_decode($row->provisionName()); ?>
                            @else
                                --
                            @endif
                            <div class="small provision" style="display: none;">@lang('lang.provision')</div>
                        </div>
                    </div>
                    <div class="mobile-row-divider"></div>
                    <div class="td d-flex flex-column mobile-btn-container">
                        <a id="other-content-{{ $row->id }}" data-bank-id="{{ $row->bank->id }}" data-this-id="{{ $row->id }}"
                            data-click-status="false" data-filter-type="{{ $filter_type }}"
                            data-cr-list-url="{{ route('overdraft-credit-other-list',['locale'=>App::getLocale()]) }}" class="btn--medium mobile-others btn-others-credit">
                             @lang('lang.others') {{ $count_sub_db }} <i class="fa fa-angle-down fa-lg"></i>
                        </a>
                    </div>
                    <div class="td d-flex flex-column align-items-lg-end justify-content-center mobile-50">
                        <a
                            data-add-route="{{ route('add-comparison') }}"
                            data-remove-route="{{ route('remove-comparison') }}"
                            data-name="{{ $row->name()}}" id="btn-id-{{ $row->id }}"
                            data-status="" data-id="{{ $row->id }}" class="add-to-balance add-comparison">
                        </a>
                        <a href="#" class="btn btn--medium btn--blue btn--arrow detailed-btn">@lang('lang.detailed_btn')</a>
                    </div>
                </div>
                <div class="tr-more">
                    @include('frontend.credits.detailed-tabs.overdraft')
                </div>
            </div>
            <div class="why">
                <div class="arrow-card__divider" style="width: 90vw"> </div>
            </div>
            <div id="others-credit-content-{{$row->id}}" style="display: none;" class="min-content-father"> </div>

            @php $count_sub_row_dp += $count_sub_db; @endphp

         </section>
    @endforeach

    <input id="count-credit" type="hidden" data-text="(@lang('lang.select_credit',['count' => $count_sub_row_dp]))" value="{{ $count_sub_row_dp }}">
@else
    @include('frontend.credits.ajax-content.overdraft-single-bank')
@endif

<script src="{{ asset('/js/credit.ajax.content.js') }}"></script>

@php
    $sess = Session::get('comparison-credit');
@endphp
@if($sess and count($sess) > 0)
    @foreach($sess as $k => $v)
        <script>
            $('#btn-id-'+'{{ $v }}').attr('data-status',true).addClass('added-to-balance');
        </script>
    @endforeach
@endif
