@if($single_bank_id<=0)
    @if(count($data['max_cr_id'])  <= 0)
        <div class="card-body text-center pb-3 pt-3 col-md-12 border-bottom border-r-8 shadow-lg--hover  bg-white mb-2">
            <h3 class="mt-3 mb-3">@lang('lang.not_found')</h3>
        </div>
    @endif
    @php
        $count_sub_row_dp = 0;
        $beginning = $singlePrint;
    @endphp
    @foreach($data['max_cr_id'] as $row)
        @php
            $singlePrint = $beginning;
            $count_sub_db = 0;
            $count_sub_row_dp += 1;
            if($border_case == 1) {
                if($row->type_of_output($border_case, $compensation)) {
                    $singlePrint = true;
                }
            } else if($border_case == 2) {
                if($row->type_of_output($border_case, $credit_date)) {
                    $singlePrint = true;
                }
            }
        @endphp
        @foreach($data['data'] as $key =>  $sub_row)
            @if($row->bank_id == $key)
                @foreach($sub_row as $row_s)
                    @if($row_s->id != $row->id)
                        @php $count_sub_db += 1; @endphp
                    @endif
                @endforeach
            @endif
        @endforeach
        <?php $datepercent = $row->otherCreditDatePercent($credit_date, $compensation); ?>
        <section class="item-section" data-date="{{ $datepercent['date'] }}" data-percent="{{ $datepercent['percent'] }}" data-date-type="">
        <div class="filter-item credits">
            <div class="tr" style="width: 100%">
                <div class="td" style="width: 16%">
                    <div class="logo-wrap" style="text-align: center !important;">
                        <div class="logo">
                            <img src="/{{ $row->bank->image }}" alt="Image Bank">
                        </div>
                        <div class="d-flex justify-content-center">
                            <div class="label" >
                                <span class="o">
                                    <a id="other-content-{{ $row->id }}" data-bank-id="{{ $row->bank->id }}" data-this-id="{{ $row->id }}"
                                       data-click-status="false" data-filter-type="{{ $filter_type }}"
                                       data-cr-list-url="{{ route('auto-credit-other-list',['locale'=>App::getLocale()]) }}" class="btn-others-credit">
                                        @lang('lang.others') {{ $count_sub_db }} <i class="fa fa-angle-down"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="td d-flex cr-name-div flex-column justify-content-center" style="width: 16%">
                    @if($row->status_suspended == 2)
                        <div class="tooltip" data-tooltip="@lang('lang.'.config('global.credits.activity')[$row->status_suspended].'')">
                            <div class="suspended">
                                <span>
                                    <i class="fa fa-exclamation-triangle fa-cs"></i>
                                </span>
                            </div>
                        </div>
                    @endif
                    <div class="aktsiya">
                        <span class="name">
                            <a target="_blank" href="{{ route('view-credit',['locale'=>App::getLocale(),'slug'=>$row->slug]) }}">
                               "{{ $row->name()}}"
                            </a>
                        </span>
                        @if($row->aktsiya)
                            <div class="promotion">
                                <img src="/temp/images/aktsiya.svg" alt="promotion image">
                            </div>
                        @endif
                    </div>
                    <div class="big">
                        @if(!$row->otherCreditList() || !$singlePrint)                                
                        {{ $datepercent['percent'] . '%'}}
                        @else 
                            {{$row->otherCreditUnique()['percent']}}
                        @endif
                    </div>
                </div>
                <div class="mobile-row-divider"></div>
                <div class="td d-flex flex-column justify-content-center" style="width: 14%">
                     <div class="big text-lowercase"  style="text-transform: none !important;">
                        @if(!$row->otherCreditList() || !$singlePrint)
                            @php
                                $printDate = explode(" ",  $datepercent['date']);
                                if(is_numeric($printDate[0]) || sizeof($printDate) == 1) echo($printDate[0] ." ".$row->creditDate->name($printDate[0]) );
                                else echo($printDate[1] ." ".$row->creditDate->name($printDate[1]) ); 
                            @endphp
                        @else 
                            {{$row->otherCreditUnique()['date']}}
                        @endif
                         <div class="small date" style="display: none;">@lang('lang.date')</div>
                    </div>
                </div>

                <div class="td d-flex flex-column justify-content-center" style="width: 18%">
                    @if ($amount == 0)  
                        <div class="big">                                                    
                            {{ $row->textAmount() }}                            
                        </div>
                        <div class="small">@lang('lang.quantum')</div>
                    @else
                        @if(!$row->otherCreditList() || !$singlePrint || $border_case == 1)
                            <div class="big" style="font-size: 18px;">
                                {{$row->otherCreditUniqueSumma($amount, true)['total']}}
                            </div>
                            <div class="small" style="margin-bottom: 10px;">@lang('lang.credit_quantum')</div>
                            
                        @else
                            <div class="big" style="font-size: 18px;">
                                {{$row->otherCreditUniqueSumma($amount)['total']}}
                            </div>
                            <div class="small" style="margin-bottom: 10px;">@lang('lang.credit_quantum')</div>  
                        @endif 
                        @if(!$row->otherCreditList() || !$singlePrint)
                            <div class="big" style="font-size: 18px">
                                {{$row->otherCreditUniqueSumma($amount, true)['per_month']}}
                            </div>
                            <div class="small">@lang('lang.per_month_quantum')</div> 
                        @else
                            <div class="big" style="font-size: 18px">
                                {{$row->otherCreditUniqueSumma($amount)['per_month']}}
                            </div>
                            <div class="small">@lang('lang.per_month_quantum')</div>   
                        @endif                          
                    @endif
                </div>
                <div class="td d-flex align-items-center mobile-width-100" style="width: 16%">
                    <div class="big" style="font-size: 18px">
                        @if ($amount != 0)
                            @if($row->compensation_status)
                                @if(!$row->otherCreditList() || !$singlePrint || $border_case == 1)
                                    {{ number_format((int)$datepercent['compensation'] * $amount / 100) . ' ' . $row->currencyName->name() }}
                                @else 
                                    {{$row->otherCreditUnique($amount)['compensation']}}
                                @endif
                            @else 
                            -- 
                            @endif
                        @else
                            @if($row->compensation_status)
                                @if(!$row->otherCreditList() || !$singlePrint)
                                    {{ $datepercent['compensation']}}
                                @else 
                                    {{$row->otherCreditUnique()['compensation']}}
                                @endif
                            @else 
                            -- 
                            @endif
                        @endif
                        <div class="small compensation" style="display: none;">@lang('lang.first_compensation')</div>
                    </div>
                </div>
                
                <div class="mobile-row-divider"></div>
                <div class="td d-flex flex-column mobile-btn-container">
                    <a id="other-content-{{ $row->id }}" data-bank-id="{{ $row->bank->id }}" data-this-id="{{ $row->id }}"
                        data-click-status="false" data-filter-type="{{ $filter_type }}"
                        data-cr-list-url="{{ route('auto-credit-other-list',['locale'=>App::getLocale()]) }}" class="btn--medium mobile-others btn-others-credit">
                         @lang('lang.others') {{ $count_sub_db }} <i class="fa fa-angle-down fa-lg"></i>
                     </a>
                </div>
                <div class="td d-flex flex-column align-items-lg-end justify-content-center mt-20 mb-20 mobile-50">
                    <a
                        data-add-route="{{ route('add-comparison') }}"
                        data-remove-route="{{ route('remove-comparison') }}"
                        data-name="{{ $row->name()}}" id="btn-id-{{ $row->id }}"
                        data-status="" data-id="{{ $row->id }}" class="add-to-balance add-comparison">
                    </a>
                    <a href="#" class="btn btn--medium btn--blue btn--arrow detailed-btn">@lang('lang.detailed_btn')</a>
                </div>
            </div>
            <div class="tr-more">
                @include('frontend.credits.detailed-tabs.auto')
            </div>
        </div>
        <div class="why">
            <div class="arrow-card__divider" style="width: 90vw"> </div>
        </div>
        <div id="others-credit-content-{{$row->id}}" style="display: none;" class="min-content-father"> </div>

        @php $count_sub_row_dp += $count_sub_db; @endphp
        </section>
    @endforeach

    <input id="count-credit" type="hidden" data-text="@lang('lang.select_credit',['count' => $count_sub_row_dp])" value="{{ $count_sub_row_dp }}">
@else
    @include('frontend.credits.ajax-content.auto-single-bank')
@endif
<script src="{{ asset('/js/credit.ajax.content.js') }}"></script>

@php
    $sess = Session::get('comparison-credit');
@endphp
@if($sess and count($sess) > 0)
    @foreach($sess as $k => $v)
        <script>
            $('#btn-id-'+'{{ $v }}').attr('data-status',true).addClass('added-to-balance');
        </script>
    @endforeach
@endif
