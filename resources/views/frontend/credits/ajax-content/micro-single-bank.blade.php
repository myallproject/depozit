@if(count($data) <= 0)
    <div class="card-body text-center pb-3 pt-3 col-md-12 border-bottom border-r-8 shadow-lg--hover  bg-white mb-2">
        <h3 class="mt-3 mb-3">@lang('lang.not_found')</h3>
    </div>
@else
    @php
        $count_sub_row_dp = 0;
    @endphp
    @foreach($data as $row)
        @php
            $count_sub_row_dp += 1;
        @endphp
        <?php $datepercent = $row->otherCreditDatePercent($credit_date); ?>
         <section class="item-section" data-date="" data-percent="{{ $datepercent['percent'] }}" data-date-type="">
        <div class="filter-item">
            <div class="tr">
                <div class="td">
                    <div class="logo-wrap" style="text-align: center !important;">
                        <div class="logo">
                            <img src="/{{ $row->bank->image }}" alt="Image Bank">
                        </div>
                    </div>
                </div>
                <div class="td d-flex cr-name-div flex-column justify-content-center">
                    @if($row->status_suspended == 2)
                        <div class="tooltip" data-tooltip="@lang('lang.'.config('global.credits.activity')[$row->status_suspended].'')">
                            <div class="suspended">
                                <span>
                                    <i class="fa fa-exclamation-triangle fa-cs"></i>
                                </span>
                            </div>
                        </div>
                    @endif
                    <div class="aktsiya">
                        <span class="name">
                            <a target="_blank" href="{{ route('view-credit',['locale'=>App::getLocale(),'slug'=>$row->slug]) }}">
                               "{{ $row->name()}}"
                            </a>
                        </span>
                        @if($row->aktsiya)
                            <div class="promotion">
                                <img src="/temp/images/aktsiya.svg" alt="promotion image">
                            </div>
                        @endif
                    </div>
                    <div class="big">{{ $datepercent['percent'] }}%</div>
                </div>
                <div class="mobile-row-divider"></div>
                <div class="td d-flex flex-column justify-content-center">
                    <div class="big text-lowercase">
                        {{ $datepercent['date'] }}
                        <div class="small date" style="display: none;">@lang('lang.date')</div>
                    </div>
                </div>
                <div class="td d-flex flex-column justify-content-center">
                    <div class="big">
                      {{ $row->textAmount() }}
                    </div>
                    <div class="small">@lang('lang.quantum')</div>
                </div>
                <div class="td d-flex align-items-center mobile-width-100">
                    <div class="big" style="font-size: 18px">
                        @if($row->provision_id)
                           <?php echo html_entity_decode($row->provisionName()); ?>
                        @else
                            --
                        @endif
                        <div class="small provision" style="display: none;">@lang('lang.provision')</div>
                    </div>
                </div>
                <div class="mobile-row-divider"></div>
                <div class="td mobile-show"></div>
                <div class="td d-flex flex-column align-items-lg-end align-items-sm-flex-end justify-content-center">
                    <a
                        data-add-route="{{ route('add-comparison') }}"
                        data-remove-route="{{ route('remove-comparison') }}"
                        data-name="{{ $row->name()}}" id="btn-id-{{ $row->id }}"
                        data-status="" data-id="{{ $row->id }}" class="add-to-balance add-comparison">
                    </a>
                    <a href="#" class="btn btn--medium btn--blue btn--arrow">@lang('lang.detailed_btn')</a>
                </div>
            </div>
            <div class="tr-more">
                @include('frontend.credits.detailed-tabs.micro')
            </div>
        </div>
        <div class="why">
            <div class="arrow-card__divider" style="width: 90vw"> </div>
        </div>
        <div id="others-credit-content-{{$row->id}}" style="display: none;" class="min-content-father"> </div>
        
    </section>
    @endforeach

    <input id="count-credit" type="hidden" data-text="(@lang('lang.select_credit',['count' => $count_sub_row_dp]))" value="{{ $count_sub_row_dp }}">
@endif
