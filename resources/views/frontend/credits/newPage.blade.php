@extends('layouts.app')
@section('meta')
      @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection

@section('content')
<link rel="stylesheet" href="/plugins/bower_components/icheck/all.css" />
<link rel="stylesheet" type="text/css" href="/css/credits.css">
    <style>
       .input-loader{
            font-size: 20px;
            position: absolute;
            display: block;
            top: 55px;
            left: 230px;
        }
        .car-img {
            text-align: center;
        }
        .select2-container--default .select2-results__option::before{
            content: none !important;
        }
        .select2-container--default .select2-results__option{
            padding: 6px 6px 6px 10px;
        }
        .num-match {
            color: #024c67;
            margin-top: 20px;
            text-transform: uppercase;
            letter-spacing: .5px;
            font-size: 14px;   
            margin-bottom: 0px;
        }
    </style>
    <div class="section">
        <div class="section-gap--small"  style="margin-top: 20px">
            <div class="medium-container">
                <h1 class="b-title mb-20">@lang('lang.auto_credit')
                    {{-- <div class="position-relative">
                        <span class="update-time">
                             <span >{{ DateMonthName::updateDateName($update_time['data_update_time']) }}</span>
                             <span >@lang('lang.update_time')</span>
                            <img src="/temp/images/update_time.png" width="25" />
                        </span>
                    </div> --}}
                </h1>
                <div class="filter-form">
                    @include('frontend.credits.filters.filter-newPage')
                </div>
                <h1 class="num-match">
                    <span> <span id="count-result" data-text="@lang('lang.select_credit')"></span></span>
                </h1>                
               <div  id="progress-bar" style="width: 100%; background-color: rgba(58,165,208,.4); display: none"><div id="bar" style="width: 1%; height: 3px; background-color: #024c67;"></div></div>
                <div class="filter-result" style="margin-top: 0px !important;">
                    <div class="filter-result__title" style="padding-top: 0px !important; padding-bottom: 10px !important;"> </div>
                    <div class="table filter-table">
                        <div class="thead">                            
                            <div class="tr"  style="font-size: 16px">
                                <div class="th" style="width: 16%">@lang('lang.bank')</div>
                                <div class="th" style="white-space: nowrap;width: 16%">@lang('lang.percent_rate') <i style="cursor:pointer;" data-sort="asc" class="fa fa-sort-amount-asc sort-percent" id="sort-percent"></i></div>
                                <div class="th" style="width: 14%">@lang('lang.date')</div>
                                <div class="th" style="width: 18%">@lang('lang.quantum')</div>
                                <div class="th" style="width: 16%">@lang('lang.first_compensation')</div>
                                <div class="th" style="width: 16%"></div>
                            </div>

                        </div>
                        <div class="tbody" id="credit-result">

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @include('frontend.credits.modals.modal-auto')
@endsection
@section('script')
    <script type="text/javascript" src="/plugins/bower_components/icheck/icheck.min.js"></script>
    <script src='/js/credits/newPage.js'></script>
    <script type="text/javascript">

        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        });
        $('#content-result-filter').html('@include("frontend.components.loader")');

        $('.filter-credit-btn-large').on('click',function(){
                $('#f-el-amount').val($('#large-search-amount-id').val());
                $('#f-el-date-small').val($('#f-el-date').val()).trigger('change');

                $('#first-compensation-small').val($('#first-compensation').val()).trigger('change')
        });
       $('.sort-percent').on('click',function(){
            if($(this).attr('data-sort') == 'asc'){
                $('#credit-result .item-section').sort(sort_div_desc).appendTo('#credit-result');
                $(this).attr('data-sort','desc');
                $(this).toggleClass('fa-sort-amount-asc').toggleClass('fa-sort-amount-desc');
            }else if($(this).attr('data-sort') == 'desc'){
                $('#credit-result .item-section').sort(sort_div_asc).appendTo('#credit-result');
                $(this).attr('data-sort','asc');
                $(this).toggleClass('fa-sort-amount-desc').toggleClass('fa-sort-amount-asc');
            }
       });

       function sort_div_desc(a,b){ 
            return ($(b).data('percent')) > ($(a).data('percent')) ? 1 : -1;
       }
       function sort_div_asc(a,b){
            return ($(b).data('percent')) < ($(a).data('percent')) ? 1 : -1;
       }
    </script>
@endsection