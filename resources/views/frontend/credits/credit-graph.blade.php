<div class="exchange-list" style="width: -webkit-fill-available;">
    <div class="medium-container section-gap--small" style="overflow-x: auto;">
        <table >
            <thead>
                <tr>
                    <th style="text-transform: capitalize">@choice('lang.month', 0)</th>
                    <th>@lang('lang.left_to_pay') </th>
                    <th>@lang('lang.pay_credit') </th>
                    <th>@lang('lang.interest_payment') </th>
                    <th>@lang('lang.total_pay') </th>                    
                </tr>
            </thead>
            <tbody>
            @if($data)
                @php $credit = $pay_percent = $pay_all =(float)0;@endphp
                @foreach($data['data'] as $k => $v)
                    <tr class="">
                        <td>{{ $k }}</td>
                        <td>{{ number_format(ceil($v['residue'])) }}</td>
                        <td>{{ number_format(ceil($v['per_month'])) }}</td>
                        <td>{{ number_format(ceil($v['pay_percent'])) }}</td>
                        <td>{{ number_format(ceil($v['pay_all'])) }}</td>
                    </tr>
                    @php $credit += (float) $v['per_month']; $pay_percent += (float)ceil($v['pay_percent']);$pay_all +=(float)ceil($v['pay_all'])  @endphp
                @endforeach
                    <tr>
                        <td></td>
                        <td>
                            0,00
                        </td>
                        <td><b>{{ number_format($credit) }} UZS</b></td>
                        <td><b>{{ number_format($pay_percent) }} UZS</b></td>
                        <td><b>{{ number_format($pay_all) }} UZS</b></td>
                        
                    </tr>
            @else
                <tr>
                    <td colspan="5" style="text-align: center">@lang('lang.not_found')</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>