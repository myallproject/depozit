@if(count($data)  <= 0)
    <div class="card-body text-center pb-3 pt-3 col-md-12 border-bottom border-r-8 shadow-lg--hover  bg-white mb-2">
        <h3 class="mt-3 mb-3">@lang('lang.not_found')</h3>
    </div>
@endif

@foreach($data as $row)
     <?php $datepercent = $row->otherCreditDatePercent($credit_date); ?>
     <section class="item-section" data-date="" data-percent="{{ $datepercent['percent'] }}" data-date-type="">
    <div class="filter-item">
        <div class="tr">
            <div class="td">
                <div class="logo-wrap" style="text-align: center !important;">
                    <div class="logo">
                        <img src="/{{ $row->bank->image }}" alt="Image Bank">
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="label" >
                            <span class="name" style="margin-bottom: 8px"></span>

                        </div>
                    </div>
                </div>
            </div>
            <div class="td d-flex cr-name-div flex-column justify-content-center">
                @if($row->status_suspended == 2)
                    <div class="tooltip" data-tooltip="@lang('lang.'.config('global.credits.activity')[$row->status_suspended].'')">
                        <div class="suspended">
                            <span>
                                <i class="fa fa-exclamation-triangle fa-cs"></i>
                            </span>
                        </div>
                    </div>
                @endif
                <div class="aktsiya">
                    <span class="name">
                        <a target="_blank" href="{{ route('view-credit',['locale'=>App::getLocale(),'slug'=>$row->slug]) }}">
                           "{{ $row->name()}}"
                        </a>
                    </span>
                    @if($row->aktsiya)
                        <div class="promotion">
                            <img src="/temp/images/aktsiya.svg" alt="promotion image">
                        </div>
                    @endif
                </div>
                <div class="big">
                    @if(!$row->otherCreditList() || !$singlePrint)                                
                        {{ $datepercent['percent'] . '%'}}
                    @else 
                        {{$row->otherCreditUnique()['percent']}}
                    @endif
                </div>
            </div>
            <div class="mobile-row-divider"></div>
            <div class="td d-flex flex-column justify-content-center" style="padding-right: 10px !important;">
                <div class="big text-lowercase" style="text-transform: none !important;">
                    @if(!$row->otherCreditList() || !$singlePrint)
                    @php
                        $printDate = explode(" ",  $datepercent['date']);
                        if(is_numeric($printDate[0]) || sizeof($printDate) == 1) echo($printDate[0] ." ".$row->creditDate->name($printDate[0]) );
                        else echo($printDate[1] ." ".$row->creditDate->name($printDate[1]) ); 
                    @endphp
                    @else 
                        {{$row->otherCreditUnique()['date']}}
                    @endif
                     <div class="small date" style="display: none;">@lang('lang.date')</div>
                </div>
            </div>
            {{-- <div class="td d-flex flex-column justify-content-center" style="padding-right: 10px !important;">
                <div class="big">
                    {{ $row->textAmount() }}
                </div>
                <div class="small">@lang('lang.quantum')</div>
            </div> --}}
            <div class="td d-flex align-items-center">
                <div class="big" style="font-size: 18px">
                    @if($row->provision_id)
                         <?php echo html_entity_decode($row->provisionName()); ?>
                    @else
                        --
                    @endif
                    <div class="small provision" style="display: none;">@lang('lang.provision')</div>
                </div>
            </div>
            <div class="mobile-row-divider"></div>
            <div class="td mobile-show"></div>
            <div class="td d-flex flex-column align-items-lg-end align-items-sm-flex-end justify-content-center">
                <a
                    data-add-route="{{ route('add-comparison') }}"
                    data-remove-route="{{ route('remove-comparison') }}"
                    data-name="{{ $row->name()}}" id="btn-id-{{ $row->id }}"
                    data-status="" data-id="{{ $row->id }}" class="add-to-balance add-comparison-other">
                </a>
                <a href="#" class="btn btn--medium btn--blue btn--arrow min-202">@lang('lang.detailed_btn')</a>
            </div>
        </div>
        <div class="tr-more">
            @include('frontend.credits.detailed-tabs.consumer')
        </div>
    </div>
    <div class="why">
        <div class="arrow-card__divider" style="width: 90vw"> </div>
    </div>
</section>
@endforeach

<script src="{{ asset('/js/other-credit.js') }}"></script>

@php
    $sess = Session::get('comparison-credit');
@endphp
@if($sess and count($sess) > 0)
    @foreach($sess as $k => $v)
        <script>
            $('#btn-id-'+'{{ $v }}').attr('data-status',true).addClass('added-to-balance');
        </script>
    @endforeach
@endif
