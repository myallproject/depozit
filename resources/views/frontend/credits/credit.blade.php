@extends('layouts.app')
@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale())." - ".$credit->name() }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('css')
    <style>
        @media (max-width: 576px){
            body{
                font-size: 14px;
            }
        }
    </style>
@endsection
@section('content')
    <div class="section">
        <div class="section-gap--small mb-0 mt-0 pb-0">
            <div class="medium-container">
                <div class="saving-head__wrap row mt-5" style="border-bottom: 0px">
                    <div class="col-md-12 d-flex credit-heading" style="align-items: center">
                        <div class="col-4 col-md-4 logo mb-0 pl-0 pr-0">
                             <a href="{{ route('info-bank',['locale'=>App::getLocale(),'slug'=>$credit->bank->slug]) }}">
                                <img style="float:left; padding-right: 10px;" src="/{{ $credit->bank->image }}" alt="SQB">
                            </a>
                        </div>
                        <div class="col-8 col-md-8 title mb-0 pl-0 pr-0">
                            @lang('lang.credit_from_bank',['name'=>$credit->name(),'bank' => $credit->bank->name(), 'type_cr'=>$type])
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divider-ui--full"></div>
        <div class="credit-info-how section-gap mb-50 mt-20">
            <div class="medium-container">
                <div class="tab-nav tab-nav--large tab-nav--center">
                    <a href="#" ><div class="b-title">@lang('lang.rules')</div></a>
                    <a href="#" ><div class="b-title">@lang('lang.percent_rates')</div></a>
                    <a href="#" ><div class="b-title">@lang('lang.documents')</div></a>
                    <a href="#" ><div class="b-title">@lang('lang.requirements')</div></a>

                   {{-- <a href="#">@lang('lang.period_review')</a>--}}
                </div>
                <div class="tab-caption">
                    <div class="tab-pane">
                        <div class="col-md-12 how-to-repay pl-0 pr-0 doc-text">
                            <div class="list-style-none col-md-6 pl-0 pr-0 font-dp d-contents befor-li">
                                <ul>
                                    <li class="d-flex" style="margin-bottom:5px !important">
                                        <div>
                                            <span class="icon" ><i style="font-size: 20px;" class="fa fa-database"></i></span>
                                        </div>
                                        <div>
                                            <span style="line-height:  1.4rem">
                                                <b>@lang('lang.max_sum'):</b>&nbsp;@if(!$credit->amount || $credit->textAmount()) {{ $credit->textAmount() }} @else {{ number_format($credit->amount) }} {{ $credit->currencyName->name() }} @endif
                                            </span>
                                        </div>
                                    </li>
                                    <li class="d-flex" style="margin-bottom:5px !important">
                                       <div>
                                            <span class="icon" ><i style="font-size: 20px;" class="fa fa-clock-o"></i></span>
                                       </div>
                                        <div>
                                            <span style="line-height:  1.4rem">
                                                <b>@lang('lang.date'):</b>&nbsp;
                                                <span>
                                                    @if(!$credit->otherCreditList())
                                                        {{ $credit->date_credit." ".$credit->creditDate->name($credit->date_credit) }}
                                                    @else
                                                    @php
                                                        $len = count($credit->otherCreditList());
                                                        $arr = [];
                                                    @endphp
                                                        @foreach($credit->otherCreditList() as $key => $other_date)
                                                            @php
                                                                array_push($arr, $other_date->datename());
                                                            @endphp
                                                        @endforeach
                                                        @php
                                                        $result = array_unique($arr);
                                                            echo(end($result));
                                                        @endphp
                                                    @endif
                                                </span>
                                            </span>
                                        </div>
                                    </li>
                                    <li class="d-flex" style="margin-bottom:5px !important">
                                        <div>
                                            <span class="icon" ><i style="font-size: 20px;" class="fa fa-clock-o"></i></span>
                                        </div>
                                        <div>
                                            <span style="line-height:  1.4rem">
                                                <b>@lang('lang.date_privilege'):</b>&nbsp;@if($credit->date_privilege_uz){{ $credit->privilege() }} @else  @lang('lang.does_not_exist') @endif
                                            </span>
                                        </div>
                                    </li>
                                    <li class="d-flex" style="margin-bottom:5px !important">
                                        <div>
                                            <span class="icon"><i style="font-size: 20px;" class="fa fa-check-circle-o"></i></span>
                                        </div>
                                        <div>
                                            <span style="line-height:  1.4rem">
                                                <b>@lang('lang.other_requirements'):</b>&nbsp;
                                                @if($credit->additionRules())
                                                    @php
                                                        echo str_replace(';','<br>',$credit->additionRules());
                                                    @endphp 
                                                @else
                                                    @lang('lang.does_not_exist') 
                                                @endif
                                            </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="list-style-none col-md-6 pl-0 pr-0 font-dp d-contents befor-li">
                                <ul>
                                    <li class="d-flex" style="margin-bottom:5px !important">
                                        <div>
                                            <span class="icon"><i style="font-size: 20px; margin-left: 1px; margin-right: 9px !important;" class="fa fa-check-circle-o"></i></span>
                                        </div>
                                        <div>
                                            <span style="line-height:  1.4rem">
                                                <b>@lang('lang.first_compensation'):</b>&nbsp;@if($credit->compensation_status){{ $credit->compensation() }} @else  @lang('lang.does_not_exist') @endif
                                            </span>
                                        </div>
                                    </li> 
                                    <li class="d-flex" style="margin-bottom:5px !important">
                                        <div>
                                            <span class="icon"><i style="font-size: 20px; margin-left: 1px; margin-right: 9px !important;" class="fa fa-shield"></i></span>
                                        </div>
                                        <div>
                                            <span style="line-height: 1.4rem;"><b>@lang('lang.type_provision'):</b>&nbsp;@if($credit->provision_id) @if($credit->typeProvision() == "")@lang("lang.not_required")@else{{$credit->typeProvision()}} @endif @else  @lang('lang.not_required') @endif</span>
                                        </div>
                                    </li>
                                    <li class="d-flex" style="margin-bottom:5px !important ">
                                        <div>
                                            <span class="icon"><i style="font-size: 20px;" class="fa fa-check-circle-o"></i></span>
                                        </div>
                                        <div>
                                            <span style="line-height:  1.4rem">
                                                <b>@lang('lang.goal_credit'):</b>&nbsp;
                                                @if($credit->credit_goal_id)
                                                <?php $goals = $credit->goal();  ?>
                                                   @if( count($goals) > 3 )
                                                    @foreach($goals as $kg => $vg)
                                                        @if($kg == 0 or $kg == 1 or $kg == 2 )
                                                            @if($vg)
                                                                {{ $vg }};<br>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                    {{-- <span class="three-dots">...</span> --}}
                                                    <span class='more-text close' >
                                                        @foreach($credit->goal() as $goalk => $gv)
                                                            @if($goalk != 0  and  $goalk != 1 and $goalk != 2 )
                                                                @if($gv)
                                                                    {{ $gv }};<br>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                     </span>
                                                    <a class="btn-more-text" style=""><span data-text='more'>@lang('lang.more')</span>&nbsp;<i class='fa fa-angle-down'></i></a>
                                                    @else
                                                        @foreach($credit->goal() as $goal)
                                                            @if($goal)
                                                                {{ $goal }};<br>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @else
                                                    @lang('lang.does_not_exist')
                                                @endif
                                            </span>
                                        </div>
                                    </li>
                                    <li class="d-flex" style="margin-bottom:5px !important;">
                                        <div>
                                            <span class="icon"><i style="font-size: 17px;" class="fa fa-university"></i></span>
                                        </div>
                                        <div>
                                            <span style="line-height:  1.4rem">
                                                <b>@lang('lang.way_legalization'):</b>&nbsp;
                                                @if($credit->rasmiylashtirish)
                                                    {{ $credit->legitimation(App::getlocale()) }}                                                
                                                @else
                                                    @lang('lang.does_not_exist') 
                                                @endif
                                            </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane">
                        <p class="md-font-18">
                            @lang('lang.text_percent_rate')
                            <br>
                            <br>
                        </p>
                        <div class="row">
                            <div class="col-md-12 pr-0 pl-0">
                                <div class="table percentage-table table-3x md-font-18">
                                    <div class="thead">
                                        <div class="tr">
                                            <div class="th"><strong>@lang('lang.credit_date')</strong></div>
                                            <div class="th"><strong>@lang('lang.percent_rate')</strong></div>
                                            @if ($credit->type_credit_id != 4)
                                                <div class="th"><strong>@lang('lang.first_compensation')</strong></div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="tbody">
                                        @if(!$credit->otherCreditList())
                                            <div class="tr">
                                                <div class="td">
                                                    <div class="text-lowercase text-left">{{ $credit->date_credit." ".$credit->creditDate->name($credit->date_credit) }}</div>
                                                </div>
                                                <div class="td">
                                                    <div>{{ $credit->percent }}%</div>
                                                </div>
                                                @if ($credit->type_credit_id != 4)
                                                    <div class="td">
                                                        <div>@if($credit->creditBoshBadal->name() == 'Отсутствует') Не требуется @else{{ $credit->creditBoshBadal->name() }}@endif</div>
                                                    </div>
                                                @endif
                                            </div>
                                        @else 
                                            @foreach($credit->otherCreditList() as $other_row)
                                                 <div class="tr">
                                                    <div class="td">
                                                        <div class="text-left">{{ $other_row->datename() }}</div>
                                                    </div>
                                                    <div class="td">
                                                        <div>{{ $other_row->percent }}%</div>
                                                    </div>
                                                    @if ($credit->type_credit_id != 4)
                                                        <div class="td">
                                                            <div>@if($other_row->compensation_id){{ $other_row->compensation->name() }}@else @lang('lang.not_required') @endif</div>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane">
                        <p class="md-font-18" style="line-height:30px;">
                            @if($credit->documents())
                                @php $docs = explode(';',$credit->documents()); @endphp
                                @foreach($docs as $kdoc => $vdoc)
                                    @if($vdoc)- <span class="doc-text">{{ $vdoc }};</span><br>@endif
                                @endforeach
                            @else
                                -
                            @endif
                           
                        </p>
                    </div>
                    <div class="tab-pane">
                        <div class="credit-info-extra befor-li">
                           <ul>
                               <li style="align-items: stretch;">
                                    <div style="padding-right: 6px !important;">
                                        <span class="icon" >
                                            <i style="font-size: 20px" class="fa fa-user-circle-o"></i>
                                        </span>
                                    </div>                                
                                    <div><b>@lang('lang.borrower_age'):</b> 
                                        @if(App::getLocale() == 'uz')
                                            @if($credit->credit_borrower_age)
                                                {{$credit->credit_borrower_age}}
                                            @else
                                                @lang('lang.does_not_important_2')
                                            @endif
                                        @else 
                                            @if($credit->credit_borrower_age_ru)
                                                {{$credit->credit_borrower_age_ru}}
                                            @else
                                                @lang('lang.does_not_important_2')
                                            @endif
                                        @endif
                                    </div>
                                </li>

                                <li style="align-items: stretch;">
                                    <div style="padding-right: 6px !important;">
                                        <span class="icon" >
                                            <i style="font-size: 20px" class="fas fa-passport"></i>
                                        </span>
                                    </div>                                    
                                    <div><b>@lang('lang.credit_citizenship'):</b> 
                                        @if($credit->credit_borrower_category_id){{ $credit->citizenship->name() }}@else @lang('lang.does_not_important_2') @endif
                                    </div>
                                </li>

                                <li style="align-items: stretch;">                                  
                                    <div>
                                        <span class="icon" >
                                            <i style="font-size: 20px" class="fas fa-user-cog"></i>
                                        </span>
                                    </div>
                                    <div><b>@lang('lang.borrower_possible'):</b> 
                                        @if($credit->credit_borrower_possible())
                                            {{$credit->credit_borrower_possible()}}
                                        @else
                                            @lang('lang.does_not_important')
                                        @endif
                                    </div>
                                </li>
                                <li style="align-items: stretch;">
                                    <div>
                                        <span class="icon" >
                                            <i style="font-size: 20px" class="fas fa-file-signature"></i>
                                        </span>
                                    </div>
                                    <div><b>@lang('lang.borrower_additional'):</b> 
                                        @if($credit->credit_borrower_additional())
                                            {{$credit->credit_borrower_additional()}}
                                        @else
                                            @lang('lang.does_not_important')
                                        @endif
                                    </div>
                                </li>
                               {{--  <li style="align-items: end; margin-top: 5px !important"><span class="icon" ><i style="font-size: 27px" class="fa fa-check-circle-o"></i></span>
                                    <div style="padding-top: 0px"><b>@lang('lang.other_requirements'):</b><br>
                                    @if($credit->additionRules())
                                        @php $rules = explode(';',$credit->additionRules()); @endphp
                                        @foreach($rules as $krule => $vrule)
                                            @if($vrule)- <span class="doc-text">{{ $vrule }};</span><br>@endif
                                        @endforeach
                                    @else
                                         -
                                     @endif</div>
                                </li> --}}
                           </ul>
                        </div>
                    </div>
                   
                   {{-- <div class="tab-pane">
                        <ul>
                            @if($credit->credit_review_period_id) {{ $credit->reviewPeriod->name() }} @else -- @endif
                        </ul>
                    </div>--}}
                </div>
            </div>
        </div>

        <!--  <div class="section-gap--small mt-10 pb-0">
            <div class="medium-container">
                <div class="credit-info-buttons">
                    <a href="{{ $credit->link }}" target="_blank" class="btn btn--medium btn--light_blue">@lang('lang.go_site')</a>
                    <a href="#" id="calc-btn" class="btn btn--medium btn--blue">@lang('lang.calc_credit')</a>
                </div>
            </div>
        </div> -->

        <div class="divider-ui--full"></div>
        <div class="calculate-credit mt-50 mb-70 pb-30" id="credit_calc_section">
            <div class="medium-container">
                <div class="row">
                    <div class="col-md-8">
                        <form id="calc-credit-form" action="{{ route('credit-calc',['locale' => App::getLocale()]) }}" method="GET">

                        <div class="row">
                                <input type="hidden" name="credit_id" value="{{ $credit->id }}">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('lang.cr_percent_rate')</label>
                                    <input @if($credit->percent != 0) readonly @endif  required="required" data-id="one" class="f-el-price form-control" name="percent" value="{{ $credit->percent }}" data-validate="true"   data-error-text="@lang('validation.numeric',['attribute' => Yt::trans('qiymat','uz')])" >
                                    <span class="error text-danger" id="error-percent" style="display: none;"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('lang.sum_credit')</label>
                                    <input required="required" data-id="two" class="f-el-price form-control" name="quantum" data-validate="true"  data-error-text="@lang('validation.numeric',['attribute' => Yt::trans('qiymat','uz')])" placeholder="1 000 000">
                                    <span class="error text-danger" id="error-quantum" style="display: none;"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('lang.credit_date')</label>
                                    <select class="select2__js" name="date">
                                    @foreach($dates as $row_d)
                                        <option value="{{ $row_d->name_uz }}">{{ $row_d->name() }}</option>
                                    @endforeach
                                    </select>
                                    <span class="error text-danger" id="error-date" style="display: none;"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('lang.percent_calc_type')</label>
                                    <select class="select2__js" name="type_calc">
                                        <option selected="selected" value="simple">@lang('lang.usual')</option>
                                        <option  value="anuitet">@lang('lang.anuitet')</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 row credit-btns" style="display: contents;">
                                <div class="col-12 col-sm-4 col-md-4">
                                    <button class="btn btn--medium calc-credit-btn btn--blue">@lang('lang.calculation')</button>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">
                                     <button class="btn btn--medium btn--blue btn-graph">@lang('lang.pay_graph')</button>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">
                                  <a href="{{ $credit->link(App::getLocale()) }}" target="_blank" class="btn mt-10 btn--medium btn--light_blue">@lang('lang.go_site')</a>
                              </div>
                            </div>

                        </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <div class="calculate-credit__info"><span>@lang('lang.percent')</span>
                            <div id="result-percent">{{ $credit->percent }} %</div>
                        </div>
                        <div class="calculate-credit__info"><span>@lang('lang.calc_percent')</span>
                            <div id="result-sum-percent">--</div>
                        </div>
                        <div class="calculate-credit__info"><span>@lang('lang.pay_first_month')</span>
                            <div id="result-pay-first-month">--</div>
                        </div>
                        <div class="calculate-credit__info"><span>@lang('lang.all_sum')</span>
                            <div id="result-all-sum">--</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="divider-ui--full"></div>
        <div class="{{--info-bank__offices--}} section-gap--small mt-20">
            <div class="medium-container border-bottom text-center saving-head__wrap pb-50">
                <div class="title pb-30" >
                    @lang('lang.other_services_of_bank', ['bank'=>$credit->bank->name()])
                </div>
                <div class="advantages__grid other-offers">
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-deposits', ['locale'=>App::getLocale(), 'slug'=> $credit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/1.svg">
                            </div>
                            <span class="more">@lang('lang.home_header.deposit')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credits-consumer',['locale'=>App::getLocale(), 'slug'=> $credit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/3.svg">
                            </div>
                            <span class="more">@lang('lang.home_header.consumer_credit')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credits-auto',['locale'=>App::getLocale(), 'slug'=> $credit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/4.svg">
                            </div>
                            <span class="more">@lang('lang.home_header.avto_credit')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credits-mortgage',['locale'=>App::getLocale(), 'slug'=> $credit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/2.svg">
                            </div>
                            <span class="more">@lang('lang.home_header.mortgage_credit')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credits-education',['locale'=>App::getLocale(), 'slug'=> $credit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/6.svg">
                            </div>
                            <span class="more">@lang('lang.home_header.edu_credit')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credits-overdraft',['locale'=>App::getLocale(), 'slug'=> $credit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/overdraft.svg">
                            </div>
                            <span class="more">@lang('lang.home_header.overdraft')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credits-microcredit',['locale'=>App::getLocale(), 'slug'=> $credit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/5.svg">
                            </div>
                            <span class="more">@lang('lang.home_header.micro_credit')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-debit-cards', ['local'=>App::getLocale(), 'slug'=>$credit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/debit-card.svg">
                            </div>
                            <span class="more">@lang('lang.home_header.debit_cards')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credit-cards', ['local'=>App::getLocale(), 'slug'=>$credit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/credit-card.svg">
                            </div>
                            <span class="more">@lang('lang.home_header.credit_cards')</span>
                        </a>
                    </h1>
                </div>
            </div>
            <div class="divider-ui--full"></div>
            <div class="col-md-12 medium-container border-bottom row pt-25">

                <div class="col-md-4 justify-content-center pb-15">
                    <div class="big  d-min">
                        
                        <h1 class="color-a mb-10 mt-0"><a target="_blank" href="{{ route('info-bank',['locale'=>App::getLocale(),'slug'=>$credit->bank->slug]) }}">{{ $credit->bank->name() }}</a></h1>
                    </div>
                    <div class="small mt-5 mb-5 licence">{{ $credit->bank->information->licenseReplace() }}</div>
                    <div class="small">@lang('lang.ownership'): {{ $credit->bank->ownership->name() }} </div>
                </div>
                <div class="col-md-4 align-items-center font-18 pb-15">
                    <div class="big d-flex mb-10">
                        <div class="div-icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="pl-10">
                            {{ $credit->bank->information->phone1 }}
                        </div>
                    </div>
                    <div class="big d-flex mb-10">
                        <div class="div-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <div class="pl-10 color-a">
                            <a href="{{ $credit->bank->information->site() }}" target="_blank">{{ $credit->bank->information->site() }}</a>
                        </div>
                    </div>
                    <div class="big d-flex mb-10">
                        <div class="div-icon">
                            <i class="fa fa-telegram"></i>
                        </div>
                        <div class="pl-10">
                            @if($credit->bank->information->telegram_channel)
                            <a href="https://t.me/{{ substr($credit->bank->information->telegram_channel,1) }}" target="_blank"> {{ $credit->bank->information->telegram_channel }}</a>
                            @else
                                --
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-4 align-items-center font-18 pb-15">
                    <div class="big d-flex mb-10">
                        <div class="div-icon">
                            <i class="fa fa-volume-control-phone"></i>
                        </div>
                        <div class="pl-10">
                            @if($credit->bank->information->phone2)
                                {{ $credit->bank->information->phone2 }}
                            @else
                                {{ $credit->bank->information->phone1 }}
                            @endif
                        </div>
                    </div>
                    <div class="big d-flex mb-10">
                        <div class="div-icon">
                            <i class="fa fa-envelope "></i>
                        </div>
                        <div class="pl-10 color-a">
                            <a href="mailto:{{ $credit->bank->information->mail }}">{{ $credit->bank->information->mail }}</a>
                        </div>
                    </div>
                    <div class="big d-flex mb-10 ">
                        <div class="div-icon font-20">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="pl-10">
                            {{ $credit->bank->information->address() }}
                        </div>
                    </div>
                </div>
            </div>

                {{--<div class="medium-container">
                    <div class="b-title">{{ Yt::trans('Bank filiallari','uz') }}</div>
                </div>
                <div id="map2"></div>--}}
        </div>
      {{--  <div class="credit-info-how section-gap">
            <div class="medium-container">
                <div class="tab-nav tab-nav--center">
                    <a href="#">
                        <div class="b-title">КАК ПОЛУЧИТЬ</div>
                    </a>
                    <a href="#">
                        <div class="b-title">КАК ПОГАСИТЬ</div>
                    </a>
                </div>
                <div class="tab-caption">
                    <div class="tab-pane">
                        <div class="how-to-get">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="item">
                                        <div class="icon">
                                            <img src="/temp/images/icons/steps/how_1.svg">
                                        </div>
                                        <div class="title">1. Отправьте заявку</div>
                                        <div class="subtitle">Заполните онлайн-анкету и отправьте заявку в банк в течение 5 минут</div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="item">
                                        <div class="icon">
                                            <img src="/temp/images/icons/steps/how_2.svg">
                                        </div>
                                        <div class="title">1. Отправьте заявку</div>
                                        <div class="subtitle">Заполните онлайн-анкету и отправьте заявку в банк в течение 5 минут</div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="item">
                                        <div class="icon">
                                            <img src="/temp/images/icons/steps/how_3.svg">
                                        </div>
                                        <div class="title">1. Отправьте заявку</div>
                                        <div class="subtitle">Заполните онлайн-анкету и отправьте заявку в банк в течение 5 минут</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane">
                        <div class="how-to-repay">
                            <div class="item">
                                <ul>
                                    <li>В любых отделениях банка</li>
                                    <li>В банкоматах банка</li>
                                    <li>Через интернет - банк</li>
                                    <li>Через мобильное приложение</li>
                                </ul>
                            </div>
                            <div class="item">
                                <ul>
                                    <li>Через банкоматы банков партнеров</li>
                                    <li>В любом отделении "Почты России"</li>
                                    <li>Через терминалы "QIWI"</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center"><a href="#" class="btn btn--medium btn--blue btn--mt">Yuborish</a>
                </div>
            </div>
        </div>--}}

    </div>

    <div class="modal-ui" data-modal-name="detailed-credit-modal">
        <div class="modal-ui__dialog detailed-search">
            <div class="modal-ui__content">
                <div class="modal-ui__close-btn"></div>
                <h2 class="b-title">@lang('lang.credit_pay_graph')</h2>
                <div class="row" id="result-graph-data" style="margin-top: 0px;"></div>
            </div>
        </div>
    </div>
    <button style="display: none !important;" class="btn btn--medium btn-graph-modal btn--blue" data-modal="detailed-credit-modal"></button>
@endsection
@section('script')
    <script>
        $(document).ready(function() {

            $('.f-el-price').on('keyup',function(){
                let v = $(this).val();
                if($(this).val().length != 0){
                    if(!$.isNumeric(v)){
                        $(this).attr('data-validate',false);
                        $('#price-error-'+$(this).attr('data-id')).text($(this).attr('data-error-text')).show();
                    }
                    if($.isNumeric(v)){
                        $(this).attr('data-validate',true);
                        $('#price-error-'+$(this).attr('data-id')).hide();
                    }
                }
                if($(this).val().length == 0){
                    $(this).attr('data-validate',true);
                    $('#price-error-'+$(this).attr('data-id')).hide();
                }
            });

            let form = $('#calc-credit-form');

            $('.calc-credit-btn').on('click',function(e){
            
                e.preventDefault();

                $.ajax({
                    type:form.attr('method'),
                    url: "{{ route('credit-calc',['locale'=>App::getLocale()]) }}",
                    data:form.serialize(),
                    success:function(data){
                        if(data.error){
                            $.each(data.error,function(i,v){
                                $('#error-'+i).show().text(v);
                            });
                        } else {
                            if(!data.percent){
                                data.percent = '';
                            }
                            $('#result-percent').text(data.percent+'%');
                            $('#result-pay-first-month').text(data.pay_first_month+' '+data.currency);
                            $('#result-sum-percent').text(data.month_sum+' '+data.currency);
                            $('#result-all-sum').text(data.all_sum+' '+data.currency);
                            $('.error').hide();
                        }
                    },error:function () {
                        alert('error');
                    }
                });
            });

            $('.btn-graph').on('click',function(e){
               
                e.preventDefault();
                $.ajax({
                    type:form.attr('method'),
                    url:"{{ route('credit-pay-graph',['locale'=>App::getLocale()]) }}",
                    data:form.serialize(),
                    success:function(data){
                        if(data.error){
                             $.each(data.error,function(i,v){
                                $('#error-'+i).show().text(v);
                            });
                        }else{
                            $('#price-error-one_one').hide();
                            $('.btn-graph-modal').click();
                            $('#result-graph-data').html(data);
                        }
                    },error:function(){
                        alert('error');
                    }
                });
            });

            $('#calc-btn').click(function(){
                $('html, body').animate({
                    scrollTop: $("#credit_calc_section").offset().top-100
                }, 1500);
            });

            $('[data-toggle="tooltip"]').tooltip({
                track: true
            });

            $('.btn-more-text').on('click',function(){
               // $('.three-dots').toggleClass('close');
                $('.more-text').slideToggle(600);
                $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
                let more = $(this).find('span').attr('data-text');
                if(more == 'more'){
                    $(this).find('span').text("@lang('lang.close')");
                    $(this).find('span').attr('data-text','close');
                } else if(more == 'close') {
                    $(this).find('span').text("@lang('lang.more')");
                    $(this).find('span').attr('data-text','more');
                }
            });
        });
    </script>
@endsection
