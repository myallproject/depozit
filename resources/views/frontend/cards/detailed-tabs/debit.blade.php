<div class="head">
    <div class="divider"></div>
</div>

<div class="content">
    <div class="tab-nav">
        <a href="#" class="main-tab active">@lang('lang.rules')</a>
        <a href="#">@lang('lang.payments')</a>
        <a href="#">@lang('lang.cashing')</a>
        @if($row->aktsiya)
            <a href="#" class="aktsiya-tab">*@lang('lang.promotion')</a>
        @endif
        <a href="#">@lang('lang.additional_conveniences')</a>
        <a href="#">@lang('lang.info_bank',['name'=>$row->bank->name_uz])</a>
    </div>
    <div class="tab-caption">
        <div class="tab-pane main-tab active">
            <div class="row">
                <div class="col-md-6">
                    <ul>
                        @if($row->price_izoh())
                            <li>
                                <span class="sub font-16">@lang('lang.card_price'):</span>
                                <span>{{ $row->price_izoh() }}</span>
                            </li>
                        @endif
                        @if($row->urgent_opening)
                            <li>
                                <span class="sub font-16">@lang('lang.urgent_opening'):</span>
                                <span>{{ $row->urgent_opening }}</span>
                            </li>
                        @endif
                        @if($row->re_issue_card)
                            <li>
                                <span class="sub font-16">@lang('lang.re_issue_card'):</span>
                                <span>{{ $row->re_issue_card }}</span>
                            </li>
                        @endif
                        <li>
                            <span class="sub font-16">@lang('lang.servicing'):</span>
                            <span>{{ $row->service() }}</span>
                        </li>
                        @if($row->rasmiy_yuli())
                            <li>
                                <span class="sub font-16">@lang('lang.card_legalization'):</span>
                                <span>{{ $row->rasmiy_yuli() }}</span>
                            </li>
                        @endif
                        @if($row->date_id)
                            <li>
                                <span class="sub font-16">@lang('lang.card_period'):</span>
                                <span>{{ $row->dateCard->name() }}</span>
                            </li>
                        @endif
                        @if($row->sms_inform)
                            <li>
                                <span class="sub font-16">@lang('lang.sms_inform'):</span>
                                <span>{{ $row->sms_inform }}</span>
                            </li>
                        @elseif($row->sms_inform == 0)
                            <li>
                                <span class="sub font-16">@lang('lang.sms_inform'):</span>
                                <span>@lang('lang.free')</span>
                            </li>
                        @endif
                        @if($row->qushimcha_shartlar())
                            <li>
                                <span class="sub font-16">@lang('lang.addition_rules'):</span>
                                <span>{{ $row->qushimcha_shartlar() }}</span>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-pane">
            <div class="row">
                <div class="col-md-6">
                    <ul>
                        <li>
                            <span class="sub font-16">@lang('lang.payment_commision_card'):</span>
                            <span>
                                {{ $row->tuluv_komissia }}
                                @if ($row->tuluv_komissia_izoh())
                                    {{ '. '.$row->tuluv_komissia_izoh() }}
                                @endif
                            </span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.other_payments'):</span>
                            <span>
                                {{ $row->tuluv_komissia_boshqa }}
                                @if ($row->tuluv_komissia_boshqa_izoh())
                                    {{ '. '.$row->tuluv_komissia_boshqa_izoh() }}
                                @endif
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-pane">
            <div class="row">
                <div class="col-md-6">
                    <ul>
                        <li>
                            <span class="sub font-16">@lang('lang.your_banks_terminal'):</span>
                            <span>{{ $row->naqdlash_komissia_terminal }}</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.your_banks_bankomat'):</span>
                            <span>{{ $row->naqdlash_komissia }}</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.other_banks_bankomat'):</span>
                            <span>{{ $row->naqdlash_komissia_boshqa }}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        @if($row->aktsiya)
            <div class="tab-pane aktsiya-tab">
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li>
                                <span class="sub font-16">@lang('lang.detailed_info'):</span>
                                <span>{{ $row->aktsiya_izoh(App::getLocale()) }}</span>
                            </li>
                            <li>
                                <span class="sub font-16">@lang('lang.link'):</span>
                                <a href="{{ $row->aktsiya_link(App::getLocale()) }}" target="_blank">{{ $row->aktsiya_link(App::getLocale()) }}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="tab-pane">
            <div class="row">
                <div class="col-md-6">
                    <ul>
                        <li>
                            <span class="sub font-16">@lang('lang.additional_conveniences'):</span>
                            <span>{{ $row->qushimcha_qulaylik() }}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-pane">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-title">@lang('lang.bank_information')</div>
                    <ul>
                        {{-- <li>
                            <span class="sub font-16">@lang('lang.ownership'):</span>
                            <span>
                                @foreach($type_bank as $tp_bk)
                                    @if($row->bank->information->type_bank == $tp_bk->id)
                                        {{ $tp_bk->name() }}
                                    @endif
                                @endforeach
                            </span>
                        </li> --}}
                        <li>
                            <span class="sub font-16">@lang('lang.phone'):</span>
                            <span>{{ $row->bank->phone }}</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.address'):</span>
                            <span>{{ $row->bank->information->address() }}</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.site'):</span>
                            <span><a href="{{ $row->bank->information->site() }}" target="_blank">{{ $row->bank->information->site() }}</a></span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.telegram_channel'):</span>
                            <span><a href="https://t.me/{{ substr($row->bank->information->telegram_channel,1) }}" target="_blank"> {{ $row->bank->information->telegram_channel }}</a></span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.mobile_app'):</span>
                            <span>{{ $row->bank->information->mobile_app }} 
                                <a href="{{$row->bank->information->app_store}}" target="_blank"><i class="fab fa-app-store-ios fa-lg" style="color: #3aa5d0"></i></a> &nbsp; 
                                <a href="{{$row->bank->information->play_market}}" target="_blank"><i class="fab fa-google-play fa-lg"></i></a>
                            </span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="col-title">@lang('lang.rating')</div>
                    <ul>
                        <li>
                            <span class="sub font-16">@lang('lang.citizen_ranking'):</span>
                            <span><a href="{{route('ranking-citizen', ['locale' => App::getLocale()])}}" class="btn-outline-dark-blue btn-badge ex-sm-block" target="_blank">@lang('lang.ranking_position',['rank'=> cache($row->bank->citizenRankingKey()) ]) <i class="fas fa-external-link-alt"></i></a></span>
                        </li>
                        <li style="padding-top: 10px; padding-bottom: 10px;">
                            <a href="{{ route('review.add',['locale' => App::getLocale()]) }}" class="btn-xn btn-xn-primary btn-xn-justify" style="max-width: 200px; color: white !important;">
                                @lang('lang.v2.index_page.text_btn')
                            </a>
                        </li>
                        {{-- <li><span class="sub">@lang('lang.popular_rating'):</span>
                            <span>@if($row->bank->ratings){{ $row->bank->ratings->ommabop }}@else - @endif</span>
                        </li>
                        <li><span class="sub">@lang('lang.average_rating'):</span>
                            <span>@if($row->bank->ratings){{ $row->bank->ratings->urtacha }}@else - @endif</span>
                        </li> --}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="foot">
    <div class="divider"></div>
    {{-- <a href="" target="_blank" class="btn btn--medium btn--blue">@lang('lang.more_detailed')</a> --}}
    <a href="{{ $row->link(App::getLocale()) }}" target="_blank" class="btn btn--medium btn--light_blue">@lang('lang.go_site')</a>
</div>