<div class="modal-ui" data-modal-name="detailed-card-modal">
    <div class="modal-ui__dialog detailed-search">
        <div class="modal-ui__content">
            <div class="modal-ui__close-btn"></div>
            <h2 class="b-title">@lang('lang.addition_rules_search')</h2>
            <form action="{{ route('debit-cards-large-filter',['locale'=>App::getLocale()]) }}" id="large-search-form" method="GET">
                <input type="hidden" name="filter_type" value="large" />
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group mb-15">
                                <label>@lang('lang.type_currency')</label>
                                <select  class='select2__js ' data-validate="true" name="currency" id="f-el-currency-large" data-empty="@lang('lang.choose')">
                                    @foreach($currencies as $currency)
                                        <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                                    @endforeach
                                        <option value="multi">@lang('lang.multi_currency')</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group mb-15">
                                <label>@lang('lang.payment')</label>
                                <select  class='select2__js clear' data-validate="true" name="payment[]"  id="f-el-payment-large"  multiple data-dropdown="open" data-empty="@lang('lang.choose')">
                                    <option value="any"> @lang('lang.any')</option>
                                    @foreach($payments as $payment)
                                        <option value="{{ $payment->id }}">{{ $payment->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group mb-15">
                                <label>@lang('lang.type_card')</label>
                                <select class="select2__js clear" data-validate="true" id="f-el-type-card-large" name="type_card" data-dropdown="open" data-empty="@lang('lang.choose')">
                                    <option value="0"> @lang('lang.any')</option>
                                    @foreach($card_types as $card_type)
                                        <option value="{{ $card_type->id }}">{{ $card_type->name() }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group ">
                                <label>@lang('lang.banks')</label>
                                <select class="select2__js clear" data-validate="true" name="type_banks[]" multiple="multiple" data-dropdown="open" id="select-banks" data-empty="@lang('lang.choose')">
                                    <option value="any"> @lang('lang.any')</option>
                                    <option value="state_bank">@lang('lang.state_bank')</option>
                                    <option value="private_bank">@lang('lang.private_bank')</option>
                                    <option value="foreign_bank">@lang('lang.foreign_bank')</option>
                                    @foreach($banks as $bank)
                                        <option value="{{ $bank->id }}"> {{ $bank->name_uz }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding-top: 20px;">
                        <div class="form-group mb-15">
                            <label class="label clear-checkbox" style="display: initial">
                                <input type="checkbox" class="minimal " name="bepul_xizmat">
                                &nbsp;<i class="far fa-handshake"></i> &nbsp;@lang('lang.free_service')
                            </label>
                        </div>
                        <div class="form-group mb-15">
                            <label class="label clear-checkbox" style="display: initial">
                                <input type="checkbox" class="minimal " name="bepul_naqdlash">
                                &nbsp;<i class="fas fa-hand-holding-usd"></i> &nbsp;@lang('lang.free_money')
                            </label>
                        </div>
                        <div class="form-group mb-15">
                            <label class="label clear-checkbox" style="display: initial">
                                <input type="checkbox" class="minimal"  name="bepul_tulov">
                                &nbsp;<i class="far fa-credit-card"></i> &nbsp;@lang('lang.free_pay')
                            </label>
                        </div>
                        <div class="form-group mb-15">
                            <label class="label clear-checkbox" style="display: initial">
                                <input type="checkbox" class="minimal" name="kontaktsiz_tulov">
                                &nbsp;<i class="fas fa-rss"></i> &nbsp;@lang('lang.no_contact_pay')
                            </label>
                        </div>
                        <div class="form-group mb-10">
                            <label class="label clear-checkbox" style="display: initial">
                                <input type="checkbox" class="minimal" name="cash_back">
                                &nbsp;<i class="fas fa-undo"></i> &nbsp;Cashback
                            </label>
                        </div>
                        <div class="form-group mb-10">
                            <label class="label clear-checkbox" style="display: initial">
                                <input type="checkbox" class="minimal" name="delivery_service">
                                &nbsp;<i class="fas fa-car-alt"></i> &nbsp;@lang('lang.delivery_service')
                            </label>
                        </div>
                        <div class="form-group mb-10">
                            <label class="label clear-checkbox" style="display: initial">
                                <input type="checkbox" class="minimal" name="aktsiya">
                                &nbsp;<i class="fas fa-tag"></i> &nbsp;@lang('lang.promotion')
                            </label>
                        </div>
                        <div class="form-group mb-10">
                            <label class="label clear-checkbox" style="display: initial">
                                <input type="checkbox" class="minimal" name="d_secure">
                                &nbsp;<i class="fas fa-fingerprint"></i> &nbsp;3D Secure
                            </label>
                        </div>
                        <div class="form-group mb-10">
                            <label class="label clear-checkbox" style="display: initial">
                                <input type="checkbox" class="minimal" name="lounge_key">
                                &nbsp;<i class="fas fa-plane"></i> &nbsp;Lounge Key
                            </label>
                        </div>
                    </div>
                </div>

                <div class="buttons">
                    <div class="text-center mb-20">
                        <button type="submit" class="btn btn--medium btn--blue btn-filter-large-card-ajax">@lang('lang.selection')</button>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn--medium btn--blue clear-filter-card">@lang('lang.clear_filter')</button>
                    </div>
                 </div>
            </form>
        </div>
    </div>
</div>
