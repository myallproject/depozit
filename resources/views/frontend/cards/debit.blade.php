@extends('layouts.app')
@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection

@section('css')
    <link rel="stylesheet" href="/plugins/bower_components/icheck/all.css" />
    <link rel="stylesheet" href="/css/cards.css"/>
@endsection

@section('content')
    <div class="section">
        <div class="section-gap--small" style="margin-top: 20px">
            <div class="medium-container">
                <h2 class="b-title mb-20">@lang('lang.debit_cards')
                    <span style="font-size: 80%"> <span id="count-result" data-text="(@lang('lang.select_d_card'))" > </span></span>
                </h2>
                <div class="filter-form">
                    <form id="small-search-form" action="{{ route('debit-cards-small-filter',['locale'=>App::getLocale()]) }}" method="GET">
                        <input type="hidden" name="filter_type" value="small" />
                        <div class="row">
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group mt-20">
                                            <label>@lang('lang.type_currency')</label>
                                            <select  class='select2__js ajax-filter-debit-card' data-focus="false" data-validate="true" name="currency" id="f-el-currency"  data-empty="@lang('lang.choose')">
                                                @foreach($currencies as $currency)
                                                    <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                                                @endforeach
                                                    <option value="multi">@lang('lang.multi_currency')</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group mt-20">
                                            <label>@lang('lang.payment')</label>
                                            <select  class='select2__js ajax-filter-debit-card select2_update' data-validate="true" data-focus="false" name="payment[]" multiple id="f-el-payment" data-dropdown="open" data-empty="@lang('lang.choose')">
                                                <option value="any"> @lang('lang.any')</option>
                                                @foreach($payments as $payment)
                                                <option value="{{ $payment->id }}">{{ $payment->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group mt-20">
                                            <label>@lang('lang.type_card')</label>
                                            <select class="select2__js ajax-filter-debit-card" data-validate="true" data-focus="false" id="f-el-type-card" name="type_card" data-dropdown="open" data-empty="@lang('lang.choose')">
                                                <option value="0"> @lang('lang.any')</option>
                                                @foreach($card_types as $card_type)
                                                <option value="{{ $card_type->id }}">{{ $card_type->name() }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group mt-10">
                                            <input type="checkbox" id="only_delivered_cards" class="minimal" name="only_delivered_cards"  data-form-id="#small-search-form">
                                            <label class="label clear-checkbox" style="display: initial">
                                                @lang('lang.delivery_service')
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 debit-filter-buttons">
                                <div class="form-group mt-20">
                                    <button class="btn btn--medium btn--blue btn-filter-small-card-ajax">@lang('lang.selection')</button>
                                    <button class="btn btn--medium btn--blue btn--arrow" data-modal="detailed-card-modal">@lang('lang.addition_rules')</button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="single_bank_id" value="{{$single_bank_id}}">
                    </form>
                </div>
                <!-- end filter!-->
                <div class="filter-result" style="margin-top: 0px !important;">
                    <div class="filter-result__title"> </div>
                    <div class="table filter-table debit-cards">
                        <div class="thead">
                            <div class="tr" style="font-size: 14px">
                                <div class="th">@lang('lang.card')</div>
                                <div class="th card-img">@lang('lang.card_name')</div>
                                <div class="th" style="display: flex;">
                                    <span style="padding-right: 5px">@lang('lang.payment_commission')</span>
                                    <span><i style="cursor:pointer;" data-sort="desc" class="fa fa-sort-amount-desc sort-payment" id="sort-payment" data-active='true'></i></span>
                                </div>
                                <div class="th" style="display: flex;">
                                    <span style="padding-right: 5px; width:110px;">@lang('lang.commission_for_cash')</span> 
                                    <span><i style="cursor:pointer;" data-sort="desc" class="fa fa-sort-amount-desc sort-cash" id="sort-cash" data-active='true'></i></span>
                                </div>
                                <div class="th" style="display: flex;">
                                    <span style="padding-right: 5px">@lang('lang.card_price')</span> 
                                    <span><i style="cursor:pointer;" data-sort="desc" class="fa fa-sort-amount-desc sort-price" id="sort-price" data-active='true'></i></span>
                                </div>
                                <div class="th"></div>
                            </div>
                        </div>
                        <div class="tbody"  id="content-result-filter" >
                            @include('frontend.components.loader')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('frontend.cards.modals.debit-modal')
@endsection
@section('script')
    <script type="text/javascript" src="/js/cards.js"></script>
    <script type="text/javascript" src="/plugins/bower_components/icheck/icheck.min.js"></script>
    <script>
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        });

        $('.clear-filter-card').on('click',function(){
            //$('.modal-ui__close-btn').click();
            $('.clear').val('').trigger('change');
            $('.select2-selection__placeholder').text('@lang("lang.any")');
            $('.select2-search__field').attr('placeholder','@lang("lang.any")');
            $('.select2-selection--multiple').find('.select2-search__field').attr('placeholder','@lang("lang.any")');

            let checked = $('.clear-checkbox').find('.checked');

            if(checked){
                $(checked).click();
                /*$(checked).attr('aria-checked',false);
                $('.clear-checkbox').find('div').removeClass('checked');
                $('.clear-checkbox').find('input').attr('checked',false);*/
            }
        });

        $('.sort-cash').on('click',function(){
            if($(this).attr('data-active') == 'false'){
                $(this).removeClass('color-not-active').attr('data-active','true');
                if($(this).data('sort') == 'desc'){
                    $('#content-result-filter .item-section').sort(sort_cash_desc).appendTo('#content-result-filter');
                } else if($(this).data('sort') == 'asc'){
                    $('#content-result-filter .item-section').sort(sort_cash_asc).appendTo('#content-result-filter');
                }
                $('#sort-date').attr('data-active','false').addClass('color-not-active');
            } else {
                if($(this).data('sort') == 'asc'){
                    $('#content-result-filter .item-section').sort(sort_cash_desc).appendTo('#content-result-filter');
                    $(this).data('sort','desc');
                }else if($(this).data('sort') == 'desc'){
                    $('#content-result-filter .item-section').sort(sort_cash_asc).appendTo('#content-result-filter');
                    $(this).data('sort','asc');
                }
                $(this).toggleClass('fa-sort-amount-asc').toggleClass('fa-sort-amount-desc');
            }
        });
        $('.sort-price').on('click',function(){
            if($(this).attr('data-active') == 'false'){
                $(this).removeClass('color-not-active').attr('data-active','true');
                if($(this).data('sort') == 'desc'){
                    $('#content-result-filter .item-section').sort(sort_price_desc).appendTo('#content-result-filter');
                } else if($(this).data('sort') == 'asc'){
                    $('#content-result-filter .item-section').sort(sort_price_asc).appendTo('#content-result-filter');
                }
                $('#sort-date').attr('data-active','false').addClass('color-not-active');
            } else {
                if($(this).data('sort') == 'asc'){
                    $('#content-result-filter .item-section').sort(sort_price_desc).appendTo('#content-result-filter');
                    $(this).data('sort','desc');
                }else if($(this).data('sort') == 'desc'){
                    $('#content-result-filter .item-section').sort(sort_price_asc).appendTo('#content-result-filter');
                    $(this).data('sort','asc');
                }
                $(this).toggleClass('fa-sort-amount-asc').toggleClass('fa-sort-amount-desc');
            }
        });
        $('.sort-payment').on('click',function(){
            if($(this).attr('data-active') == 'false'){
                $(this).removeClass('color-not-active').attr('data-active','true');
                if($(this).data('sort') == 'desc'){
                    $('#content-result-filter .item-section').sort(sort_payment_desc).appendTo('#content-result-filter');
                } else if($(this).data('sort') == 'asc'){
                    $('#content-result-filter .item-section').sort(sort_payment_asc).appendTo('#content-result-filter');
                }
                $('#sort-date').attr('data-active','false').addClass('color-not-active');
            } else {
                if($(this).data('sort') == 'asc'){
                    $('#content-result-filter .item-section').sort(sort_payment_desc).appendTo('#content-result-filter');
                    $(this).data('sort','desc');
                }else if($(this).data('sort') == 'desc'){
                    $('#content-result-filter .item-section').sort(sort_payment_asc).appendTo('#content-result-filter');
                    $(this).data('sort','asc');
                }
                $(this).toggleClass('fa-sort-amount-asc').toggleClass('fa-sort-amount-desc');
            }
        });
        function sort_cash_desc(a,b){ 
            return ($(b).data('cash')) > ($(a).data('cash')) ? 1 : -1;
        }
        function sort_cash_asc(a,b){
            return ($(b).data('cash')) < ($(a).data('cash')) ? 1 : -1;
        }
        function sort_price_desc(a,b){ 
            return ($(b).data('price')) > ($(a).data('price')) ? 1 : -1;
        }
        function sort_price_asc(a,b){
            return ($(b).data('price')) < ($(a).data('price')) ? 1 : -1;
        }
        function sort_payment_desc(a,b){ 
            return ($(b).data('payment')) > ($(a).data('payment')) ? 1 : -1;
        }
        function sort_payment_asc(a,b){
            return ($(b).data('payment')) < ($(a).data('payment')) ? 1 : -1;
        }
    </script>
@endsection