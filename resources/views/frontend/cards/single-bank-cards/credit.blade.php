@if(count($data) <= 0)
    <div class="card-body text-center pb-3 pt-3 col-md-12 border-bottom border-r-8 shadow-lg--hover  bg-white mb-2">
        <h3 class="mt-3 mb-3">@lang('lang.not_found')</h3>
    </div>
@else
    @php
        $count_sub_row_dp = 0;
    @endphp
    @foreach($data as $row)
        @php
            $count_sub_row_dp += 1;
        @endphp
        <div class="filter-item cards">
            <div class="tr">
                <div class="td">
                    <div class="logo-wrap" style="padding: 0px !important;">
                        <div class="logo">
                            <img src="/{{ $row->bank->image }}" alt="Image Bank">
                        </div>
                    </div>
                </div>
                <div class="td d-flex flex-column justify-content-center ">
                    <div class="big">{{ $row->imtoyozli_davr() }}</div>
                    <div class="small hide-text">@lang('lang.date_privilege')</div>
                </div>
                <div class="mobile-row-divider"></div>
                <div class="td d-flex flex-column justify-content-center">
                    <div class="big">{{ $row->percent }}%</div>
                    <div class="small hide-text">@lang('lang.percent_rate')</div>
                </div>
                <div class="td d-flex flex-column justify-content-center">
                    <div class="big">{{ number_format(($row->max_sum)) }} {{ $row->currency->name }}</div>
                    <div class="small hide-text">@lang('lang.card_limit')</div>
                </div>
                <div class="mobile-row-divider"></div>
                <div class="td mobile-show"></div>
                <div class="td d-flex flex-column align-items-lg-end align-items-sm-flex-end justify-content-center">
                    <a
                        data-add-route="{{ route('add-comparison') }}"
                        data-remove-route="{{ route('remove-comparison') }}"
                        data-name="{{ $row->name() }}" id="btn-id-{{ $row->id }}"
                        data-type="card_credit"
                        data-status="" data-id="{{ $row->id }}" class="add-to-balance add-comparison">
                    </a>
                    <a href="#" class="btn btn--medium btn--blue btn--arrow">@lang('lang.detailed_btn')</a>
                </div>
            </div>

            <div class="tr-more">
                <div class="head">
                    <div class="divider"></div>
                </div>

                <div class="content">
                    <div class="tab-nav">
                        <a href="#" class="active">@lang('lang.info_bank',['name'=>$row->bank->name_uz])</a>
                    </div>
                    <div class="tab-caption">
                        <div class="tab-pane active">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-title">@lang('lang.bank_information')</div>
                                    <ul>
                                        <li>@lang('lang.phone'): &nbsp; {{ $row->bank->phone }}</li>
                                        <li>@lang('lang.address'): &nbsp; {{ $row->bank->information->address() }}</li>
                                        <li>@lang('lang.site'):  &nbsp; <a href="{{ $row->bank->information->site() }}" target="_blank">{{ $row->bank->information->site() }}</a></li>
                                        <li>@lang('lang.telegram_channel'): &nbsp; {{ $row->bank->information->telegram_channel }}</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-title">@lang('lang.rating')</div>
                                    <ul>
                                        <li>
                                            <span class="sub">@lang('lang.financial_rating'):</span>
                                            <span>@if($row->bank->ratings){{ $row->bank->ratings->moliyaviy }}@else - @endif</span>
                                        </li>
                                        <li><span class="sub">@lang('lang.popular_rating'):</span>
                                            <span>@if($row->bank->ratings){{ $row->bank->ratings->ommabop }}@else - @endif</span>
                                        </li>
                                        <li><span class="sub">@lang('lang.average_rating'):</span>
                                            <span>@if($row->bank->ratings){{ $row->bank->ratings->urtacha }}@else - @endif</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="foot">
                    <div class="divider"></div>
                    {{-- <a href="" target="_blank" class="btn btn--medium btn--blue">@lang('lang.more_detailed')</a> --}}
                    <a href="{{ $row->link(App::getLocale()) }}" target="_blank" class="btn btn--medium btn--light_blue">@lang('lang.go_site')</a></a>
                </div>
            </div>
        </div>

        <div class="why">
            <div class="arrow-card__divider" style="width: 90vw"> </div>
        </div>
        <div id="others-card-content-{{$row->id}}" style="display: none; margin-bottom: 25px;" class="min-content-father"></div>
    @endforeach

    <input id="count-card" type="hidden" data-text="@lang('lang.select_credit',['count' => $count_sub_row_dp])" value="{{ $count_sub_row_dp }}">
@endif