@if(count($data) <= 0)
    <div class="card-body text-center pb-3 pt-3 col-md-12 border-bottom border-r-8 shadow-lg--hover  bg-white mb-2">
        <h3 class="mt-3 mb-3">@lang('lang.not_found')</h3>
    </div>
@else
    @php
        $count_sub_row_dp = 0;
    @endphp
    @foreach($data as $row)
        @php
            $count_sub_row_dp += 1;
        @endphp
    <div class="filter-item cards debit-card">
        <div class="tr">
            <div class="td">
                <div class="logo-wrap">
                    <div class="logo p-y-5">
                        @if($row->image)
                            <img src="/{{ $row->image }}" alt="Card image">
                        @else
                            <img src="/{{ $row->bank->image }}" alt="Bank image">
                        @endif
                    </div>
                </div>
            </div>
            <div class="td d-flex flex-column justify-content-center">
                <a href="{{ route('info-bank',['locale'=>App::getLocale(),'slug'=>$row->bank->slug]) }}" class="small p-b" style="display: block" target="_blank">
                    {{$row->bank->name()}}
                </a>
                <div class="card-name">
                    <a class="big p-b" style="color: #3aa5d0; display: inline-block;">
                        {{ $row->name() }}
                    </a>
                    @if($row->aktsiya)
                        <div class="promotion">
                            <img src="/temp/images/aktsiya.svg" alt="promotion image">
                        </div>
                    @endif
                </div>
                <div class="small p-b" style="display: inline-block;">
                    @if($row->cardType)
                        {{ $row->payments().' '.$row->cardType->name() }}
                    @else
                        {{ $row->payments() }}
                    @endif
                </div>
                <div class="mobile-hidden" style="position: absolute; bottom:8px;">
                    @if($row->uyga_yetkazish)<span class="info-label"><i class="fas fa-car-alt"></i> &nbsp; @lang('lang.delivery_service')</span>@endif
                </div>
            </div>
            <div class="mobile-row-divider"></div>
            @if($row->cash_back || $row->uyga_yetkazish)
                <div class="td mobile-show label-container">
                    <div style="display:inline-block;width:50%;padding-right:5px;">
                        @if($row->cash_back)
                            <span class="info-label"><i class="fas fa-undo"></i> &nbsp; Cashback</span>
                        @elseif($row->uyga_yetkazish)
                            <span class="info-label"><i class="fas fa-car-alt"></i> &nbsp; @lang('lang.delivery_service')</span>
                        @endif
                    </div>
                    <div style="display:inline-block;width:50%;padding-left:5px;">
                        @if($row->cash_back && $row->uyga_yetkazish)
                            <span class="info-label"><i class="fas fa-car-alt"></i> &nbsp; @lang('lang.delivery_service')</span>
                        @endif
                    </div>
                </div>
            @endif
            <div class="td d-flex flex-column justify-content-center ">
                <div class="big">{{ $row->tuluv_komissia }}</div>
                <div class="small hide-text">@lang('lang.payment_commission')</div>
            </div>
            <div class="td d-flex flex-column justify-content-center">
                <div class="big">{{ $row->naqdlash_komissia }}</div>
                <div class="small hide-text">@lang('lang.commission_for_cash')</div>
            </div>
            <div class="td d-flex flex-column justify-content-center">
               <div class="big">{{ $row->price }}</div>
                <div class="small hide-text">@lang('lang.card_price')</div>
            </div>
            <div class="mobile-row-divider"></div>
            <div class="td mobile-show"></div>
            <div class="td d-flex flex-column">
                <div class="mobile-hidden other-labels">
                    @if($row->bepul_xizmat)<span class="others-label"><i class="far fa-handshake"></i> &nbsp;@lang('lang.free_service')</span>@endif
                    @if($row->aktsiya)<span class="others-label"><i class="fas fa-tag"></i> &nbsp;@lang('lang.promotion')</span>@endif
                    @if($row->bepul_tulov)<span class="others-label"><i class="far fa-credit-card"></i> &nbsp;@lang('lang.free_pay')</span>@endif
                    @if($row->cash_back)<span class="others-label"><i class="fas fa-undo"></i> &nbsp;Cashback</span>@endif
                    @if($row->d_secure)<span class="others-label"><i class="fas fa-fingerprint"></i> &nbsp;3D Secure</span>@endif
                    @if($row->bepul_naqdlash)<span class="others-label"><i class="fas fa-hand-holding-usd"></i> &nbsp;@lang('lang.free_money')</span>@endif
                    @if($row->kontaktsiz_tulov)<span class="others-label"><i class="fas fa-rss"></i> &nbsp;@lang('lang.no_contact_pay')</span>@endif
                    @if($row->lounge_key)<span class="others-label"><i class="fas fa-plane"></i> &nbsp;Lounge Key</span>@endif
                </div>
                <a
                    data-add-route="{{ route('add-comparison') }}"
                    data-remove-route="{{ route('remove-comparison') }}"
                    data-name="{{ $row->name()}}" id="btn-id-{{ $row->id }}"
                    data-status="" data-id="{{ $row->id }}" class="add-to-balance add-comparison-debit">
                </a>
                <a href="#" class="btn btn--medium btn--blue btn--arrow min-202">@lang('lang.detailed_btn')</a>
            </div>


        </div>

        <div class="tr-more">
            @include('frontend.cards.detailed-tabs.debit')
        </div>
    </div>

        <div class="why">
            <div class="arrow-card__divider" style="width: 90vw"> </div>
        </div>
        <div id="others-card-content-{{$row->id}}" style="display: none; margin-bottom: 25px;" class="min-content-father"></div>
    @endforeach
    <input id="count-card" type="hidden" data-text="(@lang('lang.select_d_card',['count' => $count_sub_row_dp]))" value="{{ $count_sub_row_dp }}">
@endif
