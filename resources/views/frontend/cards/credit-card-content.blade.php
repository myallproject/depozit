@if($single_bank_id<=0)
    @if(count($data['max_cr_id'])  <= 0)
        <div class="card-body text-center pb-3 pt-3 col-md-12 border-bottom border-r-8 shadow-lg--hover  bg-white mb-2">
            <h3 class="mt-3 mb-3">@lang('lang.not_found')</h3>
        </div>
    @endif
    @php
        $count_sub_row_dp = 0;
    @endphp
    @foreach($data['max_cr_id'] as $row)
        @php
            $count_sub_db = 0;
            $count_sub_row_dp += 1;
        @endphp
        @foreach($data['data'] as $key =>  $sub_row)
            @if($row->bank_id == $key)
                @foreach($sub_row as $row_s)
                    @if($row_s->id != $row->id)
                        @php $count_sub_db += 1; @endphp
                    @endif
                @endforeach
            @endif
        @endforeach
        <div class="filter-item cards">
            <div class="tr">
                <div class="td">
                    <div class="logo-wrap">
                        <div class="logo">
                            <img src="/{{ $row->bank->image }}" alt="Image Bank">
                        </div>
                        <div class="d-flex justify-content-center" style="text-align: center !important;">
                            <div class="label">
                            <span class="o">
                                 <a id="other-content-{{ $row->id }}" data-bank-id="{{ $row->bank_id }}" data-this-id="{{ $row->id }}"
                                    data-click-status="false" data-filter-type="{{ $filter_type }}"
                                    data-count="{{ $count_sub_db }}"
                                    data-list-url="{{ route('other-credit-cards-list',['locale'=>App::getLocale()]) }}" class="btn-others-cards">
                                    @lang('lang.others') {{ $count_sub_db }}
                                    <i class="fa fa-angle-down" style=""></i>
                                 </a>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="td d-flex flex-column justify-content-center ">
                    <div class="big">{{ $row->imtoyozli_davr() }}</div>
                    <div class="small hide-text">@lang('lang.date_privilege')</div>
                </div>
                <div class="mobile-row-divider"></div>
                <div class="td d-flex flex-column justify-content-center">
                    <div class="big">{{ $row->percent }}%</div>
                    <div class="small hide-text">@lang('lang.percent_rate')</div>
                </div>
                <div class="td d-flex flex-column justify-content-center">
                    <div class="big">{{ number_format(($row->max_sum)) }} {{ $row->currency->name }}</div>
                    <div class="small hide-text">@lang('lang.card_limit')</div>
                </div>
                <div class="mobile-row-divider"></div>
                <div class="td d-flex flex-column mobile-btn-container">
                    <a id="other-content-{{ $row->id }}" data-bank-id="{{ $row->bank_id }}" data-this-id="{{ $row->id }}"
                        data-click-status="false" data-filter-type="{{ $filter_type }}"
                        data-count="{{ $count_sub_db }}"
                        data-list-url="{{ route('other-credit-cards-list',['locale'=>App::getLocale()]) }}" class="btn--medium mobile-others btn-others-cards">
                        @lang('lang.others') {{ $count_sub_db }}
                        <i class="fa fa-angle-down fa-lg" style=""></i>
                    </a>
                </div>
                <div class="td d-flex flex-column align-items-lg-end justify-content-center mobile-50">
                    <a
                        data-add-route="{{ route('add-comparison') }}"
                        data-remove-route="{{ route('remove-comparison') }}"
                        data-name="{{ $row->name() }}" id="btn-id-{{ $row->id }}"
                        data-type="card_credit"
                        data-status="" data-id="{{ $row->id }}" class="add-to-balance add-comparison">
                    </a>
                    {{-- <a href="#" class="add-to-balance added-to-balance"></a>--}}
                    <a href="#" class="btn btn--medium btn--blue btn--arrow detailed-btn">@lang('lang.detailed_btn')</a>
                </div>
            </div>
            <div class="tr-more">
                <div class="head">
                    <div class="divider"></div>
                </div>

                <div class="content">
                    <div class="tab-nav">
                        {{-- <a href="#" class="active">Параметры</a>--}}
                        {{-- <a href="#">Требования</a>
                         <a href="#">Документы</a>--}}
                        <a href="#" class="active">@lang('lang.info_bank',['name'=>$row->bank->name_uz])</a>

                    </div>
                    <div class="tab-caption">
                        {{--<div class="tab-pane active">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul>
                                        <li><span class="sub">Срок:</span>  <span>1095 дн</span>
                                        </li>
                                        <li><span class="sub">Сумма:</span>  <span>от 1 000 000 ₽</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul>
                                        <li>- требуется подтверждение дохода</li>
                                        <li>- срок рассмотрения от 1 дня</li>
                                        <li>- обеспечение не требуется</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="col-title">Категория заемщика</div>
                                    <ul>
                                        <li>- работники по найму</li>
                                        <li>- с положительной кредитной историей</li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-title">Возраст</div>
                                    <ul>
                                        <li>- для мужчин от 20 лет на дату получения кредита</li>
                                        <li>- для женщин от 20 лет на дату получения кредита</li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-title">Стаж работы</div>
                                    <ul>
                                        <li>- на последнем месте не менее 6 месяцев</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul>
                                        <li>- страховое свидетельство гос-го пенсионного страхования</li>
                                        <li>- паспорт</li>
                                        <li>- заявление-анкета</li>
                                        <li>- при необходимости банк может запросить копию трудовой книжки, заверенную компанией – работодателем</li>
                                    </ul>
                                </div>
                            </div>
                        </div>--}}
                        <div class="tab-pane active">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-title">@lang('lang.bank_information')</div>
                                    <ul>
                                        <li>@lang('lang.phone'): &nbsp; {{ $row->bank->phone }}</li>
                                        <li>@lang('lang.address'): &nbsp; {{ $row->bank->information->address() }}</li>
                                        <li>@lang('lang.site'):  &nbsp; <a href="{{ $row->bank->information->site() }}" target="_blank">{{ $row->bank->information->site() }}</a></li>
                                        <li>@lang('lang.telegram_channel'): &nbsp; {{ $row->bank->information->telegram_channel }}</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-title">@lang('lang.rating')</div>
                                    <ul>
                                        <li>
                                            <span class="sub">@lang('lang.financial_rating'):</span>
                                            <span>@if($row->bank->ratings){{ $row->bank->ratings->moliyaviy }}@else - @endif</span>
                                        </li>
                                        <li><span class="sub">@lang('lang.popular_rating'):</span>
                                            <span>@if($row->bank->ratings){{ $row->bank->ratings->ommabop }}@else - @endif</span>
                                        </li>
                                        <li><span class="sub">@lang('lang.average_rating'):</span>
                                            <span>@if($row->bank->ratings){{ $row->bank->ratings->urtacha }}@else - @endif</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="foot">
                    <div class="divider"></div>
                    {{-- <a href="" target="_blank" class="btn btn--medium btn--blue">@lang('lang.more_detailed')</a> --}}
                    <a href="{{ $row->link(App::getLocale()) }}" target="_blank" class="btn btn--medium btn--light_blue">@lang('lang.go_site')</a></a>
                </div>
            </div>
        </div>

        <div class="why">
            <div class="arrow-card__divider" style="width: 90vw"> </div>
        </div>
        <div id="others-card-content-{{$row->id}}" style="display: none; margin-bottom: 25px;" class="min-content-father"></div>
        @php $count_sub_row_dp += $count_sub_db; @endphp
    @endforeach

    <input id="count-card" type="hidden" data-text="@lang('lang.select_credit',['count' => $count_sub_row_dp])" value="{{ $count_sub_row_dp }}">
@else
    @include('frontend.cards.single-bank-cards.credit')
@endif

<script  src="/js/ajax-content-card.js"></script>
@php
    $sess = Session::get('comparison-card-credit');
@endphp
@if($sess and count($sess) > 0)
    @foreach($sess as $k => $v)
        <script>
            $('#btn-id-'+'{{ $v }}').attr('data-status',true).addClass('added-to-balance');
        </script>
    @endforeach
@endif
{{--
<script>
    $(".add-comparison").click(function(){
           $(this).toggleClass('added-to-balance');
    });

</script> --}}
