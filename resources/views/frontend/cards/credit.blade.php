@extends('layouts.app')
@section('meta')
     @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="/css/slider-range.css">
@endsection

@section('content')
    <link rel="stylesheet" href="/css/credit-card.css" />
    <div class="section">
        <div class="section-gap--small" style="margin-top: 20px">
            <div class="medium-container">
                <h2 class="b-title mb-20">@lang('lang.credit_cards')
                    <span style="font-size: 80%">(
                        <span id="count-result" data-text="(@lang('lang.select_deposit'))"></span>)
                    </span>
                </h2>
                <div class="filter-form">
                    <form id="small-search-form" action="{{ route('credit-cards-small-filter',['locale'=>App::getLocale()]) }}" method="GET">
                         <input type="hidden" name="filter_type" value="small" />
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group mt-20">
                                    <label>@lang('lang.card_limit')</label>
                                    <input type="text" data-validate="true" name="max_sum" id="f-el-price" data-error-text="@lang('validation.numeric',['attribute' => Yt::trans('qiymat','uz')])" class='filter-deposit-ajax' placeholder="1 000 000" />
                                    <div class="slidecontainer">
                                        <input type="range" min="" step="100000" max="1000000000" value="0" class="slider" id="myRange">
                                    </div>
                                    <span id="max_sum-error" class="text-danger" style="display: none"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-20">
                                    <label>@lang('lang.date')</label>
                                    <select  class='select2__js ' data-validate="true" name="date" id="f-el-date" data-empty="@lang('lang.choose')">
                                        <option value="0"> @lang('lang.any')</option>
                                        @foreach($dates as $date)
                                            <option value="{{ $date->id }}">{{ $date->name() }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group mt-20">
                                    <label>@lang('lang.provision')</label>
                                    <select class="select2__js " data-validate="true" id="f-el-provision" name="provision" data-empty="@lang('lang.choose')">
                                        <option value="0"> @lang('lang.any')</option>
                                        @foreach($provisions as $provision)
                                            <option value="{{ $provision->id }}">{{ $provision->name() }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3" style="padding-top: 24px">
                                <div class="form-group mt-20">
                                    <button class="btn btn--medium btn--blue btn-filter-small-card-ajax" style="height: 45px;">@lang('lang.selection')</button>
                                    {{-- <button class="btn btn--medium btn--blue" data-modal="detailed-card-modal">@lang('lang.addition_rules')</button>--}}
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="single_bank_id" value="{{$single_bank_id}}">
                    </form>
                </div>
                <!-- end filter!-->

                <div class="filter-result" style="margin-top: 0px !important;">
                    <div class="filter-result__title"> </div>
                    <div class="table filter-table">
                        <div class="thead">
                            <div class="tr" style="font-size: 14px">
                                <div class="th">@lang('lang.bank')</div>
                                <div class="th">Imtiyozli davr</div>
                                <div class="th">Foiz stavkasi</div>
                                <div class="th">Kredit karta limiti</div>
                                <div class="th"></div>
                            </div>
                        </div>
                        <div class="tbody"  id="content-result-filter" >

                            @include('frontend.components.loader')

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="/js/cards.js"></script>
    <script src="{{ asset('/js/credit-card.slader-range.js') }}"></script>
    <script>
        $('.clear-filter-card').on('click',function(){
            //$('.modal-ui__close-btn').click();
            $('.clear').val('').trigger('change');
            $('.select2-selection__placeholder').text('@lang("lang.any")');
            $('.select2-search__field').attr('placeholder','@lang("lang.any")');
            $('.select2-selection--multiple').find('.select2-search__field').attr('placeholder','@lang("lang.any")');
        });
    </script>
@endsection