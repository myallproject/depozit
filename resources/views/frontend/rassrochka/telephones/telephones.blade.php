@extends('layouts.app')
@section('meta')
    {{-- @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach --}}
@endsection
@section('content')
<link rel="stylesheet" href="/plugins/bower_components/icheck/all.css" />
<link href="/css/deposits.css" type="text/css" rel="stylesheet" />
{{-- <link rel="stylesheet" type="text/css" href="/css/slider-range.css"> --}}

    <div class="section">
        <div class="section-gap--small" style="margin-top: 20px">
            <div class="medium-container">
                <h1 class="b-title mb-30">@lang('lang.deposits')
                   <span style="font-size: 80%"> <span id="count-result" data-text="(@lang('lang.select_deposit'))" > </span></span>
                    <div class="position-relative mt-10 mobile-hidden">
                        {{-- <span class="update-time">
                             <span >{{ DateMonthName::updateDateName($update_time['data_update_time']) }}</span>
                             <span >@lang('lang.update_time')</span>
                            <img src="/temp/images/update_time.png" width="25" />
                        </span> --}}
                    </div>
                </h1>

                <div class="filter-form">
                    <form id="small-search-form" action="{{ route('filter-deposit',App::getLocale()) }}" method="GET">
                        <div class="row">
                            <div class="col-md-9 pr-0 pl-0 form-display deposits-small-filter">
                                <div class="col-9 col-md-4">
                                    <div class="form-group mt-20">
                                        <label>Brand nomi</label>
                                        <select class='select2__js filter-focus-ajax select2-f-el-currency-container' data-focus="false" data-validate="true" name="currency" id="f-el-currency" data-empty="@lang('lang.choose')">                                                
                                            @foreach($brands as $brand)
                                                <option value="{{ $brand->id }}">{{ $brand->name_uz }}</option>
                                            @endforeach
                                            <option value="0"> @lang('lang.all')</option>
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="col-3 col-md-4">
                                    <div class="form-group mt-20">
                                        <label class="mobile-hidden">@lang('lang.type_currency')</label>
                                        <label class="mobile-show">&nbsp;</label>
                                        <div id="content-currency-div" class="custom-input-group">
                                            <select class='select2__js filter-focus-ajax select2-f-el-currency-container' data-focus="false" data-validate="true" name="currency" id="f-el-currency" data-empty="@lang('lang.choose')">                                                
                                                @foreach($brands as $brand)
                                                    <option value="{{ $brand->id }}">{{ $brand->name_uz }}</option>
                                                @endforeach
                                                <option value="0"> @lang('lang.all')</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group mt-20">
                                        <label>@lang('lang.date')</label>
                                        <select class="select2__js filter-focus-ajax select2-f-el-date-type-container" data-focus="false" data-validate="true"  name="date_deposit_type" id="f-el-date-type" data-empty="@lang('lang.choose')">
                                            <option value="0"> @lang('lang.all')</option>
                                            @foreach($type_date as $type)
                                                <option  value="{{ $type->id }}">{{ $type->name() }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group mb-15 mt-10">
                                        <input type="checkbox" id="online-deposit-input" class="minimal" name="online_deposit"  data-form-id="#small-search-form">
                                        <label class="label clear-checkbox" style="display: initial">
                                            @lang('lang.online_deposits')
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 filter-additional-buttons">
                                <button class="btn btn--medium btn--blue btn-filter-deposit-ajax">@lang('lang.selection')</button>
                                <button class="btn btn--medium btn--blue btn--arrow" data-modal="detailed-deposit-modal">@lang('lang.addition_rules')</button>
                            </div>
                        </div>
                        {{-- <input type="hidden" name="single_bank_id" value="{{$single_bank_id}}"> --}}
                    </form>

                </div>
                <!-- end filter!-->
                <div  id="progress-bar" style="width: 100%; background-color: rgba(58,165,208,.4); display: none"><div id="bar" style="width: 1%; height: 3px; background-color: #024c67;"></div></div>
                <div class="filter-result" style="margin-top: 0px !important;">
                      <div class="filter-result__title"> </div>
                    <!-- <div class="filter-result__title">
                        <span id="count-result" data-text="@lang('lang.select_deposit')" style="font-size: 18px"></span> <span></span>
                    </div> -->
                    <div class="table filter-table">
                        <div class="thead">
                            <div class="tr" style="font-size: 16px">
                                <div class="th">@lang('lang.bank')</div>
                                <div class="th">@lang('lang.effect_percent') <i style="cursor:pointer;" data-sort="desc" class="fa fa-sort-amount-desc sort-percent" id="sort-percent" data-active='true'></i></div>
                                <div class="th" id="text-table-td" data-text-sum="@lang('lang.min_sum')" data-text-benefit="@lang('lang.benefit')">
                                    @lang('lang.min_sum')
                                </div>
                                <div class="th">@lang('lang.date') <i style="cursor:pointer;" data-sort="desc" class="fa fa-sort-amount-desc color-not-active sort-date" id="sort-date" data-active='false'></i></div>
                                <div class="th"></div>
                            </div>
                        </div>
                        <div class="tbody"  id="content-result-filter" ></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- @include('frontend.deposits.addition_rules_modal') --}}

@endsection
@section('script')

    <script type="text/javascript" src="/plugins/bower_components/icheck/icheck.min.js"></script>
    <script>
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        });
        $('#content-result-filter').html('@include("frontend.components.loader")');

    </script>

    <script src="{{ asset('/js/deposit.deposits.js') }}"></script>
    {{-- <script src="{{ asset('/js/deposit.slider-range.js') }}"></script> --}}
<script>

   $('.sort-percent').on('click',function(){

        if($(this).attr('data-active') == 'false'){
            $(this).removeClass('color-not-active').attr('data-active','true');
            if($(this).data('sort') == 'desc'){
                $('#content-result-filter .item-section').sort(sort_div_desc).appendTo('#content-result-filter');
            } else if($(this).data('sort') == 'asc'){
                $('#content-result-filter .item-section').sort(sort_div_asc).appendTo('#content-result-filter');
            }
            $('#sort-date').attr('data-active','false').addClass('color-not-active');
        } else {
            if($(this).data('sort') == 'asc'){
                $('#content-result-filter .item-section').sort(sort_div_desc).appendTo('#content-result-filter');
                $(this).data('sort','desc');
            }else if($(this).data('sort') == 'desc'){
                $('#content-result-filter .item-section').sort(sort_div_asc).appendTo('#content-result-filter');
                $(this).data('sort','asc');
            }
             $(this).toggleClass('fa-sort-amount-asc').toggleClass('fa-sort-amount-desc');
        }
        
   });

   $('.sort-date').on('click',function(){

        if($(this).attr('data-active') == 'false'){
            $(this).removeClass('color-not-active').attr('data-active','true');
            if($(this).data('sort') == 'desc'){
                $('#content-result-filter .item-section').sort(sort_div_desc_date).appendTo('#content-result-filter');
            } else if($(this).data('sort') == 'asc'){
                $('#content-result-filter .item-section').sort(sort_div_asc_date).appendTo('#content-result-filter');
            }
            $('#sort-percent').attr('data-active','false').addClass('color-not-active');
        } else {
            if($(this).data('sort') == 'desc'){
                $('#content-result-filter .item-section').sort(sort_div_asc_date).appendTo('#content-result-filter');
                $(this).data('sort','asc');
            } else if($(this).data('sort') == 'asc'){
                $('#content-result-filter .item-section').sort(sort_div_desc_date).appendTo('#content-result-filter');
                $(this).data('sort','desc');
            }
            $(this).toggleClass('fa-sort-amount-desc').toggleClass('fa-sort-amount-asc');
        }

   });

   function sort_div_desc(a,b){ 
        return ($(b).data('percent')) > ($(a).data('percent')) ? 1 : -1;
   }
   function sort_div_asc(a,b){
        return ($(b).data('percent')) < ($(a).data('percent')) ? 1 : -1;
   }
   function sort_div_desc_date(a,b){ 
        return ($(b).data('date')) > ($(a).data('date')) ? 1 : -1;
   }
   function sort_div_asc_date(a,b){
        return ($(b).data('date')) < ($(a).data('date')) ? 1 : -1;
   }
</script>
@endsection