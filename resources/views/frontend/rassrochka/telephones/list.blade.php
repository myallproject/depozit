@extends('layouts.app')

@section('title')
<title>Rassrochka telefones</title>
@endsection
@section('meta')
<meta name="description" content="@lang('pages.home.description')" />
{{-- <meta name="keywords" content="@lang('pages.home.keywords')" />

@foreach($meta_tags as $meta)
@if(!strpos($meta->tag,'|'))
<{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }}
    ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
    @else
    <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }}
        ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}
    </{{ $meta->tagName()['close'] }}>
    @endif
    @endforeach --}}


    @endsection

    @section('content')
    <link rel="stylesheet" type="text/css" href="/css/news-section.css">
    <style>
        .select2-container--default .select2-results__option::before {
            content: none !important;
        }

        .select2-container--default .select2-results__option {
            padding-left: 10px !important;
        }

        .b-title {
            color: #3aa5d0;
        }

        .min-banner__img {
            min-height: 500px !important;
        }
        .characteristics {
            min-height: 500px;
        }
        .characteristics > p {
            font-size: 18px;
            line-height: 200%;
            width: 80%;
            margin: 0px auto;
        }
        .th,.td {
            width: 16.666666% !important;
        }
        .th {
            font-size: 20px;
        }
        .triple-th {
            width: 50% !important;
            line-height: 40px;
        }
        .th-top {
            text-align: center;
            width: 100% !important;
            border-bottom: 2px solid rgba(0,0,0,0.2);
        }
        .th-unique {
            display: inline-block;
            width: 32.5% !important;
        }
        .th-double {
            line-height: 80px;
        }
    </style>
    <div class="section">
        {{-- slider  --}}
       
        <div class="medium-container">
            <div class="section-gap mt-50 mb-70">
            <form action="" method="GET">
                <div class="form-group row">
                    <label class="col-md-4 pt-15 b-title">Telefon Brendi</label>
                    <div class="col-md-8">
                        <select class="select2__js" name="brand" data-empty="@lang('lang.all')">
                            <option value="0">@lang('lang.all')</option>
                            <option value="1" selected>Apple</option>
                            <option value="2">Xiaomi</option>
                            <option value="3">Samsung</option>
                            <option value="4">Huawei</option>
                            <option value="5">Nokia</option>
                            <option value="6">Motorolla</option>
                        </select>
                        <span class="error text-danger" id="" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group row mt-30">
                    <label class="col-md-4 pt-15 b-title">Bo'lib to'lash muddati</label>
                    <div class="col-md-8">
                        <select class="select2__js" name="" data-empty="@lang('lang.all')">
                            <option value="0">@lang('lang.all')</option>
                            <option value="1">1 oy</option>
                            <option value="2">2 oy</option>
                            <option value="3">3 oy</option>
                            <option value="4">4 oy</option>
                            <option value="5">5 oy</option>
                            <option value="6">6 oy</option>
                        </select>
                        <span class="error text-danger" id="" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group row mt-30">
                    <label class="col-md-4 pt-15 b-title">Telefon Modeli</label>
                    <div class="col-md-8">
                        <select class="select2__js" name="" data-empty="@lang('lang.all')">
                            <option value="0">@lang('lang.all')</option>
                            <option value="1" selected>{{$telephones->smartphone_model}}</option>
                            <option value="2">Xiaomi, Redmi 9</option>
                            <option value="3">Samsung Galaxy S20</option>
                            <option value="4">Huawei 17s</option>
                            <option value="5">Nokia 1202</option>
                            <option value="6">Motorolla 3Kps</option>
                        </select>
                        <span class="error text-danger" id="" style="display: none;"></span>
                    </div>
                </div>               
            </form>
                <div class="row mt-30">
                    <div class="col-md-4">
                        <div class="min-banner">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="min-banner__img "
                                            style="background-image:url(/temp/images/telephones/apple_1.jpg)">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="min-banner__img "
                                            style="background-image:url(/temp/images/telephones/apple_2.jpg)">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="min-banner__img "
                                            style="background-image:url(/temp/images/telephones/apple_3.jpg)">
                                        </div>
                                    </div>
                                </div>
                                <div class="min-banner__wrapper">
                                    <div class="medium-container">
                                        <div class="swiper-pagination"></div>
                                        <div class="swiper-button-next"></div>
                                        <div class="swiper-button-prev"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-8 characteristics">
                        <h2 class="b-title d-block">Xarakteristika</h2>
                    </div>                   
                </div>
                <div class="row mt-30">
                    <div class="col-md-12">
                        <div class="filter-result" style="margin-top: 0px !important;">
                            <div class="filter-result__title"> </div>
                            <!-- <div class="filter-result__title">
                          <span id="count-result" data-text=":count ta omonat mos keldi" style="font-size: 18px"></span> <span></span>
                      </div> -->
                            <div class="table filter-table">
                                <div class="thead">
                                    <div class="tr" style="font-size: 16px">
                                        <div class="th th-double">Kompaniya nomi</div>
                                        <div class="th th-double">Telefon Baxosi<i style="cursor:pointer;" data-sort="desc"
                                                class="fa fa-sort-amount-desc sort-percent" id="sort-percent"
                                                data-active="true"></i></div>
                                        <div class="triple-th">
                                            <div class="th th-top">Bo'lib to'lash shartlari</div>
                                            <div class="th th-unique">Muddati<i style="cursor:pointer;" data-sort="desc"
                                                class="fa fa-sort-amount-desc color-not-active sort-date" id="sort-date"
                                                data-active="false"></i></div>
                                            <div class="th th-unique" id="text-table-td" data-text-sum="Min summa"
                                                data-text-benefit="Foyda">Oylik to'lov</div>
                                            
                                            <div class="th th-unique">Jami to'lov</div>
                                        </div>
                                        <div class="th"></div>
                                    </div>
                                </div>
                                <div class="tbody" id="content-result-filter">
                                    <section class="item-section" data-date="25" data-percent="18" data-date-type="">
                                        <div class="filter-item">
                                            <div class="tr">
                                                <div class="td bank-logo">
                                                    <div class="logo-wrap">

                                                        <div class="logo">
                                                            <img src="/uploads/banks/1585830609.png" alt="Image Bank">
                                                        </div>
                                                        <div class="d-flex justify-content-center">
                                                            <div class="label">
                                                                <span class="o">
                                                                    <a id="other-content-484" data-bank-id="23"
                                                                        data-this-id="485" data-click-status="false"
                                                                        data-filter-type="small" data-count="2"
                                                                        data-dp-list-url="http://localhost:8000/uz/deposits/filter-other-dp"
                                                                        class=" btn-others-deposits">
                                                                        Boshqalar 2
                                                                        <i class="fa fa-angle-down"></i>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="td d-flex flex-column justify-content-center">
                                                    <span class="name" style="width: -webkit-fill-available">
                                                        <a href="http://localhost:8000/uz/deposits/view/33629">
                                                            "Poytaxt - 25"
                                                        </a><br>
                                                    </span>
                                                    <div class="big ">
                                                        18% </div>
                                                </div>
                                                <div class="mobile-row-divider"></div>
                                                <div class="td d-flex flex-column justify-content-center">
                                                    <div class="big">1,000,000 so'm</div>
                                                    <div class="small">Min summa</div>
                                                </div>
                                                <div class="td d-flex align-items-center">
                                                    <div class="big text-lowercase">
                                                        25 oy
                                                        <div class="small date" style="display: none;">Muddat</div>
                                                    </div>
                                                </div>
                                                <div class="td d-flex align-items-center">
                                                    <div class="big text-lowercase">
                                                        3,000,000 so'm
                                                        <div class="small date" style="display: none;">Muddat</div>
                                                    </div>
                                                </div>
                                                <div class="mobile-row-divider"></div>
                                                <div class="td d-flex flex-column mobile-btn-container">
                                                    <a id="other-content-484" data-bank-id="23" data-this-id="485"
                                                        data-click-status="false" data-filter-type="small"
                                                        data-count="2"
                                                        data-dp-list-url="http://localhost:8000/uz/deposits/filter-other-dp"
                                                        class="btn--medium mobile-others btn-others-deposits">
                                                        Boshqalar 2
                                                        <i class="fa fa-angle-down fa-lg"></i>
                                                    </a>
                                                </div>
                                                <div class="td d-flex flex-column align-items-lg-end justify-content-center mt-10 mb-10 mobile-50"
                                                    style="padding-bottom: 0px;">
                                                    <a data-add-route="http://localhost:8000/add/comparison"
                                                        data-remove-route="http://localhost:8000/remove/comparison"
                                                        data-name="Poytaxt - 25" id="btn-id-485" data-status=""
                                                        data-id="485" class="add-to-balance add-comparison"
                                                        style="cursor:pointer; border: none; background-size: 20px; width: 32px; height: 32px;">
                                                    </a>

                                                    <a href="#"
                                                        class="btn btn--medium btn--blue btn--arrow detailed-btn">Ma'lumot</a>
                                                </div>
                                            </div>
                                            <div class="tr-more">
                                                <div class="head">
                                                    <div class="divider"></div>
                                                </div>
                                                <div class="content">
                                                    <div class="tab-nav">
                                                        <a href="#" class="active">Shartlar</a>
                                                        <a href="#">Foizlar jadvali</a>
                                                        <a href="#">Poytaxt Bank haqida ma'lumot</a>

                                                    </div>
                                                    <div class="tab-caption">
                                                        <div class="tab-pane active">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <ul>
                                                                        <li><span class="sub font-16">Muddat:</span>
                                                                            <span>
                                                                                25 oy
                                                                            </span>
                                                                        </li>
                                                                        <li><span class="sub font-16">Min summa:</span>
                                                                            <span>
                                                                                1,000,000 so'm </span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="sub font-16">Omonatni ochish
                                                                                usuli:</span>
                                                                            <span>
                                                                                Bank ofislari orqali </span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <ul>
                                                                        <li>
                                                                            <span class=" cl-not-activa ">
                                                                                <i class="fa fa-minus"></i>
                                                                                Qo'shimcha mablag' kiritish </span>
                                                                        </li>
                                                                        <li>
                                                                            <span class=" cl-not-activa ">
                                                                                <i class="fa fa-minus"></i>
                                                                                Mablag'larni qisman yechish</span>
                                                                            <div class="dp-initial p-relative"><a
                                                                                    class="tooltip-down color-blue-light"
                                                                                    data-tooltip="Omonat shartlariga asosan belgilangan summadan yuqori summani yechish mumkin. Bunda hisoblangan foizlar kuymaydi.”">&nbsp;<i
                                                                                        class="fa fa-info-circle "></i></a>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <span class="">
                                                                                <i class="fa fa-check"></i>
                                                                                Har oy foiz to’lash</span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="">
                                                                                <i class="fa fa-check"></i>
                                                                                Imtiyozli yechish</span>
                                                                            <div class="dp-initial p-relative"><a
                                                                                    class="tooltip-down color-blue-light"
                                                                                    data-tooltip="Omonat muddati tugagunga qadar omonat qaytarib olinganda hisoblangan foizlar kuyib ketmaydi. Omonat xaqiqatda turgan muddatga ma’lum miqdorda foiz to’lanadi.">&nbsp;<i
                                                                                        class="fa fa-info-circle"></i></a>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <span class=" cl-not-activa ">
                                                                                <i class="fa fa-minus"></i>
                                                                                Foizlar kapitalizatsiyasi</span>
                                                                            <div class="dp-initial p-relative"><a
                                                                                    class="tooltip-down color-blue-light"
                                                                                    data-tooltip="Hisoblangan foizlar omonat summasiga qo’shilgan holda ularning ustiga foiz hisoblanadi.">&nbsp;<i
                                                                                        class="fa fa-info-circle"></i></a>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="tab-pane">
                                                            <div class="table percentage-table">
                                                                <div class="thead">
                                                                    <div class="tr">
                                                                        <div class="th">Omonat muddati</div>
                                                                        <div class="th">
                                                                            Foiz stavkasi </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tbody">
                                                                    <div class="tr">
                                                                        <div class="td new-td text-lowercase">
                                                                            <div>
                                                                                25 oy
                                                                                <!-- 1,000,000 so&#039;m -->
                                                                            </div>
                                                                        </div>
                                                                        <div class="td new-td">
                                                                            <div>18%</div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="tab-pane">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="col-title">Bank ma'lumotlari</div>
                                                                    <ul>
                                                                        <li>Telefon: &nbsp; +99871-214-20-00</li>
                                                                        <li>Manzil: &nbsp; 100063, Toshkent shahri,
                                                                            Chilonzor tumani, Islom Karimov ko‘chasi,
                                                                            55-uy</li>
                                                                        <li>Sayt: &nbsp; <a
                                                                                href="https://poytaxtbank.uz"
                                                                                target="_blank">https://poytaxtbank.uz</a>
                                                                        </li>
                                                                        <li>Telegram kanali: &nbsp; <a
                                                                                href="https://t.me/bank_poytaxt"
                                                                                target="_blank"> @bank_poytaxt</a></li>

                                                                    </ul>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="col-title">Egalik shakli</div>
                                                                    <ul>
                                                                        <li>
                                                                            <span>Davlat banki</span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                </div>
                                                <div class="foot">
                                                    <div class="divider"></div>
                                                    <a href="http://localhost:8000/uz/deposits/view/33629"
                                                        target="_blank" class="btn btn--medium btn--blue ">Batafsil
                                                        shartlar </a>
                                                    <a href="https://poytaxtbank.uz/oz/individuals/12" target="_blank"
                                                        class="btn btn--medium btn--light_blue">Saytga o'tish</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="why">
                                            <div class="arrow-card__divider" style="width: 90vw"> </div>
                                        </div>
                                        <div id="others-deposits-content-485" style="display: none;"
                                            class="min-content-father"></div>

                                    </section>
                                    <section class="item-section" data-date="25" data-percent="18" data-date-type="">
                                        <div class="filter-item">
                                            <div class="tr">
                                                <div class="td bank-logo">
                                                    <div class="logo-wrap">

                                                        <div class="logo">
                                                            <img src="/uploads/banks/1553352508.png" alt="Image Bank">
                                                        </div>
                                                        <div class="d-flex justify-content-center">
                                                            <div class="label">
                                                                <span class="o">
                                                                    <a id="other-content-86" data-bank-id="12"
                                                                        data-this-id="88" data-click-status="false"
                                                                        data-filter-type="small" data-count="6"
                                                                        data-dp-list-url="http://localhost:8000/uz/deposits/filter-other-dp"
                                                                        class=" btn-others-deposits">
                                                                        Boshqalar 6
                                                                        <i class="fa fa-angle-down"></i>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="td d-flex flex-column justify-content-center">
                                                    <span class="name" style="width: -webkit-fill-available">
                                                        <a
                                                            href="http://localhost:8000/uz/deposits/view/madadkorim-36941">
                                                            "Madadkor"
                                                        </a><br>
                                                    </span>
                                                    <div class="big ">
                                                        18% * </div>
                                                </div>
                                                <div class="mobile-row-divider"></div>
                                                <div class="td d-flex flex-column justify-content-center">
                                                    <div class="big">
                                                        Cheklanmagan </div>
                                                    <div class="small">Min summa</div>
                                                </div>
                                                <div class="td d-flex align-items-center">
                                                    <div class="big text-lowercase">
                                                        25 oygacha
                                                        <div class="small date" style="display: none;">Muddat</div>
                                                    </div>
                                                </div>
                                                <div class="td d-flex align-items-center">
                                                    <div class="big text-lowercase">
                                                        3,000,000 so'm
                                                        <div class="small date" style="display: none;">Muddat</div>
                                                    </div>
                                                </div>
                                                <div class="mobile-row-divider"></div>
                                                <div class="td d-flex flex-column mobile-btn-container">
                                                    <a id="other-content-86" data-bank-id="12" data-this-id="88"
                                                        data-click-status="false" data-filter-type="small"
                                                        data-count="6"
                                                        data-dp-list-url="http://localhost:8000/uz/deposits/filter-other-dp"
                                                        class="btn--medium mobile-others btn-others-deposits">
                                                        Boshqalar 6
                                                        <i class="fa fa-angle-down fa-lg"></i>
                                                    </a>
                                                </div>
                                                <div class="td d-flex flex-column align-items-lg-end justify-content-center mt-10 mb-10 mobile-50"
                                                    style="padding-bottom: 0px;">
                                                    <a data-add-route="http://localhost:8000/add/comparison"
                                                        data-remove-route="http://localhost:8000/remove/comparison"
                                                        data-name="Madadkor" id="btn-id-88" data-status="" data-id="88"
                                                        class="add-to-balance add-comparison"
                                                        style="cursor:pointer; border: none; background-size: 20px; width: 32px; height: 32px;">
                                                    </a>

                                                    <a href="#"
                                                        class="btn btn--medium btn--blue btn--arrow detailed-btn">Ma'lumot</a>
                                                </div>
                                            </div>
                                            <div class="tr-more">
                                                <div class="head">
                                                    <div class="divider"></div>
                                                </div>
                                                <div class="content">
                                                    <div class="tab-nav">
                                                        <a href="#" class="active">Shartlar</a>
                                                        <a href="#">Foizlar jadvali</a>
                                                        <a href="#">Madad Invest Bank haqida ma'lumot</a>

                                                    </div>
                                                    <div class="tab-caption">
                                                        <div class="tab-pane active">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <ul>
                                                                        <li><span class="sub font-16">Muddat:</span>
                                                                            <span>
                                                                                25 oygacha
                                                                            </span>
                                                                        </li>
                                                                        <li><span class="sub font-16">Min summa:</span>
                                                                            <span>
                                                                                Cheklanmagan </span>
                                                                        </li>
                                                                        <li>
                                                                            <span class="sub font-16">Omonatni ochish
                                                                                usuli:</span>
                                                                            <span>
                                                                                Bank ofislari orqali </span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <ul>
                                                                        <li>
                                                                            <span class=" cl-not-activa ">
                                                                                <i class="fa fa-minus"></i>
                                                                                Qo'shimcha mablag' kiritish </span>
                                                                        </li>
                                                                        <li>
                                                                            <span class=" cl-not-activa ">
                                                                                <i class="fa fa-minus"></i>
                                                                                Mablag'larni qisman yechish</span>
                                                                            <div class="dp-initial p-relative"><a
                                                                                    class="tooltip-down color-blue-light"
                                                                                    data-tooltip="Omonat shartlariga asosan belgilangan summadan yuqori summani yechish mumkin. Bunda hisoblangan foizlar kuymaydi.”">&nbsp;<i
                                                                                        class="fa fa-info-circle "></i></a>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <span class="">
                                                                                <i class="fa fa-check"></i>
                                                                                Har oy foiz to’lash</span>
                                                                        </li>
                                                                        <li>
                                                                            <span class=" cl-not-activa ">
                                                                                <i class="fa fa-minus"></i>
                                                                                Imtiyozli yechish</span>
                                                                            <div class="dp-initial p-relative"><a
                                                                                    class="tooltip-down color-blue-light"
                                                                                    data-tooltip="Omonat muddati tugagunga qadar omonat qaytarib olinganda hisoblangan foizlar kuyib ketmaydi. Omonat xaqiqatda turgan muddatga ma’lum miqdorda foiz to’lanadi.">&nbsp;<i
                                                                                        class="fa fa-info-circle"></i></a>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <span class=" cl-not-activa ">
                                                                                <i class="fa fa-minus"></i>
                                                                                Foizlar kapitalizatsiyasi</span>
                                                                            <div class="dp-initial p-relative"><a
                                                                                    class="tooltip-down color-blue-light"
                                                                                    data-tooltip="Hisoblangan foizlar omonat summasiga qo’shilgan holda ularning ustiga foiz hisoblanadi.">&nbsp;<i
                                                                                        class="fa fa-info-circle"></i></a>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="tab-pane">
                                                            <div class="table percentage-table">
                                                                <div class="thead">
                                                                    <div class="tr">
                                                                        <div class="th">Omonat muddati</div>
                                                                        <div class="th">
                                                                            Foiz stavkasi </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tbody">
                                                                    <div class="tr">
                                                                        <div class="td new-td text-lowercase">
                                                                            12
                                                                            Oy

                                                                        </div>
                                                                        <div class="td new-td">
                                                                            16% </div>

                                                                    </div>
                                                                    <div class="tr">
                                                                        <div class="td new-td text-lowercase">
                                                                            18
                                                                            Oy

                                                                        </div>
                                                                        <div class="td new-td">
                                                                            17% </div>

                                                                    </div>
                                                                    <div class="tr">
                                                                        <div class="td new-td text-lowercase">
                                                                            25
                                                                            Oy

                                                                        </div>
                                                                        <div class="td new-td">
                                                                            18% </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="tab-pane">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="col-title">Bank ma'lumotlari</div>
                                                                    <ul>
                                                                        <li>Telefon: &nbsp; +99873-241-70-31</li>
                                                                        <li>Manzil: &nbsp; 150118, O'zbekiston
                                                                            Respublikasi Farg'ona sh, Mustaqillik 312-uy
                                                                        </li>
                                                                        <li>Sayt: &nbsp; <a
                                                                                href="http://madadinvestbank.uz"
                                                                                target="_blank">http://madadinvestbank.uz</a>
                                                                        </li>
                                                                        <li>Telegram kanali: &nbsp; <a
                                                                                href="https://t.me/madadinvest_bank"
                                                                                target="_blank"> @madadinvest_bank</a>
                                                                        </li>

                                                                    </ul>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="col-title">Egalik shakli</div>
                                                                    <ul>
                                                                        <li>
                                                                            <span>Xususiy bank</span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                </div>
                                                <div class="foot">
                                                    <div class="divider"></div>
                                                    <a href="http://localhost:8000/uz/deposits/view/madadkorim-36941"
                                                        target="_blank" class="btn btn--medium btn--blue ">Batafsil
                                                        shartlar </a>
                                                    <a href="https://www.madadinvestbank.uz/vklad/3" target="_blank"
                                                        class="btn btn--medium btn--light_blue">Saytga o'tish</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="why">
                                            <div class="arrow-card__divider" style="width: 90vw"> </div>
                                        </div>
                                        <div id="others-deposits-content-88" style="display: none;"
                                            class="min-content-father"></div>

                                    </section>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
