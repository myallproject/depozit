@foreach($data as $row_f)
    @if($row_f->id != $this_id)
    @if($date_dp)
        @php
          $percent = $row_f->otherPercent($date_dp);
          $date_type = $row_f->otherDateType($date_dp);
          $date_type_date = $row_f->otherDateTypeDate($date_dp);
        @endphp
    @else
        @php
          $percent = $row_f->deposit_percent;
          $date_type = $row_f->date_type_id;
          $date_type_date = $row_f->deposit_date;
        @endphp
    @endif
    <section class="item-section deposits" data-date="" data-percent="{{ $percent }}" data-date-type="">
        
    <div class="filter-item"> 
        <div class="tr"> 
            <div class="td bank-logo others-boshqalar">
                <div class="logo-wrap" style="margin-top: 0px;">
                    <div class="logo">
                        <img src="/{{ $row_f->bank->image }}" alt="Image Bank">
                    </div>
                </div>
            </div>
            <div class="td d-flex flex-column justify-content-center">
                <div class="aktsiya">
                    <a href="{{ route('view-deposit',['locale'=> App::getLocale(),'slug'=> $row_f->slug]) }}">
                        "{{ $row_f->name() }}"
                   </a>
                    @if($row_f->aktsiya)
                        <div class="promotion">
                            <img src="/temp/images/aktsiya.svg" alt="promotion image">
                        </div>
                    @endif
                </div>
                <div class="big ">
                    {{ $percent }}%
                </div>
            </div>
            <div class="mobile-row-divider"></div>
            <div class="td d-flex flex-column justify-content-center">
                @if(!$r_price and !$date_dp)
                    @if($row_f->min_sum)
                        <div class="big">{{ number_format($row_f->min_sum)}} @if($row_f->priceCurrency->name()){{ $row_f->priceCurrency->name() }}@endif</div>
                    @else
                        <div class="big">
                            @foreach(config('global.type_sum') as $k => $v)
                                @if($row_f->type_sum)
                                    @if($v == $row_f->type_sum) @lang('lang.'.$k) @endif
                                @endif
                            @endforeach
                        </div>
                    @endif
                    <div class="small">@lang('lang.min_sum')</div>
                @else
                    @if(!empty($r_price))
                        <div class="big ">{{ number_format(Benefit::benefit($row_f->id,$percent,$date_type,$date_type_date,$r_price))}} @if($row_f->priceCurrency->name()){{ $row_f->priceCurrency->name() }}@endif</div>
                        @if(!$row_f->otherDate($date_dp))
                           @if(!$row_f->deposit_date_string)
                                <?php $b_date = $row_f->deposit_date." ".__('lang.day'); ?>
                            @else
                                <?php $b_date = $row_f->dateStringLang(App::getLocale()); ?>
                            @endif
                        @else   
                           <?php $b_date = $row_f->otherDate($date_dp); ?>
                        @endif
                        <div class="small" style="text-transform: lowercase;">@lang('lang.benefit_date',['date'=>$b_date])</div>
                    @elseif($row_f->min_sum)
                        <div class="big">{{ number_format($row_f->min_sum) }} @if($row_f->priceCurrency->name()){{ $row_f->priceCurrency->name() }}@endif</div>
                        <div class="small">@lang('lang.quantum')</div>
                    @endif
                @endif
            </div>
            <div class="td d-flex align-items-center">
                <div class="big text-lowercase">
                    @if($date_dp)
                        @if(!$row_f->otherDate($date_dp))
                           @if(!$row_f->deposit_date_string)
                                {{ $row_f->deposit_date." ".__('lang.day') }}
                            @else
                                {{ $row_f->dateStringLang(App::getLocale()) }}
                            @endif
                        @else   
                            {{ $row_f->otherDate($date_dp) }}
                        @endif
                    @else
                        @if(!$row_f->deposit_date_string)
                            {{ $row_f->deposit_date." ".__('lang.day') }}
                        @else
                            {{ $row_f->dateStringLang(App::getLocale()) }}
                        @endif
                    @endif
                    <div class="small date" style="display: none;">@lang('lang.date')</div>
                </div>
            </div>
            <div class="mobile-row-divider"></div>
            <div class="td mobile-show">
                <a href="{{ route('go-to-order-deposit', ['local'=>App::getLocale(), 'slug'=>$row_f->slug]) }}" class="btn btn--medium btn--blue">@lang('lang.v2.order.apply')</a>
            </div>
            <div class="td d-flex flex-column align-items-lg-end align-items-sm-flex-end justify-content-center">
                <a
                   data-add-route="{{ route('add-comparison') }}"
                   data-remove-route="{{ route('remove-comparison') }}"
                   data-name="{{ $row_f->name_uz}}" id="btn-id-{{ $row_f->id }}"
                   data-status="" data-id="{{ $row_f->id }}" class="add-to-balance add-comparison-other">
                </a>
                {{--<a href="#" class="add-to-balance added-to-balance"></a>--}}
                <a href="{{ route('go-to-order-deposit', ['local'=>App::getLocale(), 'slug'=>$row_f->slug]) }}" class="btn btn--medium btn--blue mobile-hidden" style="margin-bottom: 10px">@lang('lang.v2.order.apply')</a>
                <a href="#" class="btn btn--medium btn--blue btn--arrow">@lang('lang.detailed_btn')</a>
            </div>
        </div>
        <div class="tr-more">
            <div class="head">
                <div class="divider"></div>
            </div>

             @include('frontend.deposits.detailed-tabs')

            <div class="foot">
                <div class="divider"></div>
                <a href="{{ route('view-deposit',['locale'=>App::getLocale(),'slug'=>$row_f->slug]) }}" class="btn btn--medium btn--blue">@lang('lang.more_detailed')</a>
                <a href="{{ $row_f->link(App::getLocale()) }}" target="_blank" class="btn btn--medium btn--light_blue">@lang('lang.go_site')</a>
            </div>
        </div>
    </div>
      <div class="why">
        <div class="arrow-card__divider" style="width: 85vw;"> </div>
    </div>
</section>
    @endif
@endforeach

<script type="text/javascript">
    
$('.add-comparison-other').on('click',function(){
    $(this).toggleClass('added-to-balance');
    let status = $(this).attr('data-status');
    let id = $(this).data('id');

    if(status == false || status == 'false'){
        $(this).attr('data-status',true);
        $.ajax({
            url:  $(this).attr('data-add-route'),
            data: {
                id: id,
                url_type: 'deposit'
            },
            success:function(data){
                $(".comparison-amount").text(data);
            }
        });
    }
    if(status == "true") {
        $(this).attr('data-status', false);
        $.ajax({
            url: $(this).attr('data-remove-route'),
            data: {
                id: id,
                url_type: 'deposit'
            },
            success:function(data){
                $(".comparison-amount").text(data);
            }
        });
    }
});


</script>
@php
    $sess = Session::get('comparison-deposit');
@endphp
@if($sess and count($sess) > 0)
    @foreach($sess as $k => $v)
        <script>
            $('#btn-id-'+'{{ $v }}').attr('data-status',true).addClass('added-to-balance');
        </script>
    @endforeach
@endif