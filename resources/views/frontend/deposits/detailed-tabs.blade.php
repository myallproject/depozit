<div class="content"> 
   <div class="tab-nav">
        <a href="#" class="active main-tab">@lang('lang.rules')</a> 
        <a href="#">@lang('lang.table_percent')</a>
        @if($row_f->aktsiya)
            <a href="#" class="aktsiya-tab">*@lang('lang.promotion')</a>
        @endif        
        <a href="#">@lang('lang.info_bank',['name'=>$row_f->bank->name()])</a> 
    </div>
    <div class="tab-caption">
        <div class="tab-pane active main-tab">
            <div class="row">
                <div class="col-md-6">
                  <ul>
                        <li><span class="sub font-16">@lang('lang.date'):</span>
                            <span>
                                @if(!$row_f->deposit_date_string)
                                    {{ $row_f->deposit_date." ".__('lang.day') }}
                                @else
                                    {{ $row_f->dateStringLang(App::getLocale()) }}
                                @endif
                            </span>
                        </li>
                        <li><span class="sub font-16">@lang('lang.min_sum'):</span>
                            <span>
                              @if($row_f->min_sum)
                                  {{ number_format($row_f->min_sum)}} @if($row_f->priceCurrency->name()){{ $row_f->priceCurrency->name() }}@endif
                              @else
                                  @foreach(config('global.type_sum') as $k => $v)
                                      @if($row_f->type_sum)
                                          @if($v == $row_f->type_sum) @lang('lang.'.$k) @endif
                                      @endif
                                  @endforeach
                              @endif
                            </span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.dp_open_type'):</span>
                            <span>
                                @php $countT = count($row_f->openType()); $i=0; $and = __('lang.and')@endphp
                                @foreach($row_f->openType() as $openT)
                                     @php $i +=1 @endphp
                                    {{ $openT->name()}} @if($i < $countT ) {{ $and }} @endif
                                @endforeach
                            </span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li>
                            <span class="@if(!$row_f->account_fill) cl-not-activa @endif" >
                            @if(!$row_f->account_fill)
                              <i class="fa fa-minus"></i>
                            @else
                              <i class="fa fa-check"></i>
                            @endif
                            @lang('lang.account_fill')
                          </span>
                        </li>
                        <li>
                            <span  class="@if(!$row_f->partly_take) cl-not-activa @endif" >
                            @if(!$row_f->partly_take)
                              <i class="fa fa-minus"></i>
                            @else
                              <i class="fa fa-check"></i>
                            @endif
                            @lang('lang.partly_take')</span><div class="dp-initial p-relative" ><a class='tooltip-down color-blue-light' data-tooltip="@lang('lang.info_partly_take')">&nbsp;<i class="fa fa-info-circle " ></i></a ></div>
                        </li>
                        <li>
                            <span  class="@if(!$row_f->percent_paid_period) cl-not-activa @endif">
                            @if(!$row_f->percent_paid_period)
                              <i class="fa fa-minus"></i>
                            @else
                              <i class="fa fa-check"></i>
                            @endif
                            @lang('lang.paid_month')</span>
                        </li>
                        <li>
                            <span  class="@if(!$row_f->privilege_take) cl-not-activa @endif" >
                            @if(!$row_f->privilege_take)
                              <i class="fa fa-minus"></i>
                            @else
                              <i class="fa fa-check"></i>
                            @endif
                            @lang('lang.privilege_take')</span><div  class="dp-initial p-relative" ><a class='tooltip-down color-blue-light' data-tooltip="@lang('lang.info_privilege_take')">&nbsp;<i class="fa fa-info-circle" ></i></a ></div>
                        </li>
                        <li>
                            <span  class="@if(!$row_f->percents_capitalization) cl-not-activa @endif" >
                            @if(!$row_f->percents_capitalization)
                              <i class="fa fa-minus"></i>
                            @else
                              <i class="fa fa-check"></i>
                            @endif
                            @lang('lang.percents_capitalization')</span><div class="dp-initial p-relative" ><a class='tooltip-down color-blue-light' data-tooltip="@lang('lang.info_percents_capitalization')">&nbsp;<i class="fa fa-info-circle" ></i></a ></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-pane">
            <div class="table percentage-table">
              <div class="thead">
                    <div class="tr">
                        <div class="th">@lang('lang.deposit_date')</div>
                        <div class="th">
                           @lang('lang.percent_rate')
                        </div>
                        @if(count($row_f->onMobileApp()) > 0) <div class="th">@lang('lang.input_mobile_app') </div> @endif
                    </div>
                </div>
                <div class="tbody">
                  @if(count($row_f->deposit_percent_date()) <= 0)
                        <div class="tr">
                            <div class="td new-td text-lowercase">
                                <div>
                                     @if(!$row_f->deposit_date_string)
                                        {{ $row_f->deposit_date." ".__('lang.day') }}
                                    @else
                                        {{ $row_f->dateStringLang(App::getLocale()) }}
                                    @endif
                                    <!-- {{ number_format($row_f->min_sum) }} {{ $row_f->priceCurrency->name() }} -->
                                </div>
                            </div>
                            <div class="td new-td">
                                <div>{{ $row_f->deposit_percent }}%</div>
                            </div>    
                        </div>
                    @endif
                    @php 
                          $sum_percent = 0; $i_c = 0; $d=[]; $s_percent = 0; $all_m = 0;
                          $sum_percent2 = 0; $i_c2 = 0; $d2=[]; $s_percent2 = 0; $all_m2 = 0;
                    @endphp
                    @foreach($row_f->deposit_percent_date() as $dp_percents)
                      @if($dp_percents->percent_consider == config('global.percent_consider_id.id.differensial_date'))
                              @php $i_c +=1; 
                                $dates = explode('-', $dp_percents->date);
                                 if($dp_percents->date){
                                    if(strpos($dp_percents->date,'-')){
                                          $date1 = $dates[0]; 
                                           
                                          $date2 = $dates[1]; 
                                          $dd  = (float)$date2-(float)$date1 + 1;

                                          $all_m += $dd;

                                          $per_m = (float)$dp_percents->percent / 12;

                                          $sum_pr = $dd * $per_m;

                                          $s_percent += $sum_pr;
                                     } else {
                                          $per_m = (float)$dp_percents->percent / 12;

                                          $all_m += 1;
                                          $sum_pr = 1 * $per_m;

                                          $s_percent += $sum_pr;
                                     }
                                 }
        
                                @endphp
                          @endif
                           <div class="tr">
                             <div class="td new-td text-lowercase">
                              @if($dp_percents->date)
                                {{ str_replace(['>','<'],'',$dp_percents->date) }}                                
                                  {{ $dp_percents->dateName(str_replace(['>','<'],'',$dp_percents->date))}}
                               @endif 
                             </div>
                            <div class="td new-td">
                              @if($dp_percents->percent){{ $dp_percents->percent }}%@endif
                            </div>
                            @if(count($row_f->onMobileApp()) > 0)
                              @php $onMobileApp = $dp_percents->onMobileApp($dp_percents->id,$row_f->id,$dp_percents->date,4); @endphp
                                @if($onMobileApp['date'] == $dp_percents->date )
                                    @if($dp_percents->percent_consider == config('global.percent_consider_id.id.differensial_date'))
                                     @php $i_c2 +=1; 
                                        $dates2 = explode('-', $onMobileApp['date']);
                                           if($onMobileApp['date']){
                                             if(strpos($onMobileApp['date'],'-')){
                                                $date1 = $dates[0]; 
                                                $date2 = $dates[1]; 
                                                $dd2  = (float)$date2-(float)$date1 + 1;

                                                $all_m2 += $dd2;

                                                $per_m2 = (float)$onMobileApp['percent'] / 12;

                                                $sum_pr2 = $dd2 * $per_m2;

                                                $s_percent2 += $sum_pr2;
                                             } else {
                                                $per_m2 = (float)$onMobileApp['percent'] / 12;
                                                $all_m2 += 1;
                                                $sum_pr2 = 1 * $per_m2;

                                                $s_percent2 += $sum_pr2;
                                             }
                                           }
                                        @endphp
                                    @endif
                                <div class="td new-td">{{ $onMobileApp['percent'] }}%</div>
                                @else
                                    <div class="td new-td"></div>
                                @endif
                            @endif

                           </div>
                    @endforeach
                    @if($i_c > 0)
                        <div class="tr">
                                <div class="td new-td">@lang('lang.effect_percent_year')</div>
                            @if($s_percent and $all_m != 0)
                                <div class="td new-td">{{ number_format(($s_percent / $all_m )*12,2)}}%</div>
                            @endif
                            @if( $i_c2 > 0) 
                                @if($s_percent2 and $all_m2 != 0)                        
                                <div class="td new-td">{{ number_format(($s_percent2 / $all_m2 )*12,2)}}%</div>
                                @endif
                            @else
                                
                            @endif
                        </div>
                    @endif  
                </div>
            </div>
        </div>

        @if($row_f->aktsiya)
            <div class="tab-pane aktsiya-tab">
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li>
                                <span class="sub font-16">@lang('lang.detailed_info'):</span>
                                <span>{{ $row_f->aktsiya_izoh(App::getLocale()) }}</span>
                            </li>
                            <li>
                                <span class="sub font-16">@lang('lang.link'):</span>
                                <a href="{{ $row_f->aktsiya_link(App::getLocale()) }}" target="_blank">{{ $row_f->aktsiya_link(App::getLocale()) }}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        <div class="tab-pane">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-title">@lang('lang.bank_information')</div>
                    <ul>
                        <li>
                            <span class="sub font-16">@lang('lang.ownership'):</span>
                            <span>
                                @foreach($type_bank as $tp_bk)
                                    @if($row_f->bank->information['type_bank'] == $tp_bk->id)
                                        {{ $tp_bk->name() }}
                                    @endif
                                @endforeach
                            </span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.phone'):</span>
                            <span>{{ $row_f->bank->phone }}</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.address'):</span>
                            <span>{{ $row_f->bank->information->address() }}</span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.site'):</span>
                            <span><a href="{{ $row_f->bank->information->site() }}" target="_blank">{{ $row_f->bank->information->site() }}</a></span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.telegram_channel'):</span>
                            <span><a href="https://t.me/{{ substr($row_f->bank->information->telegram_channel,1) }}" target="_blank"> {{ $row_f->bank->information->telegram_channel }}</a></span>
                        </li>
                        <li>
                            <span class="sub font-16">@lang('lang.mobile_app'):</span>
                            <span>{{ $row_f->bank->information->mobile_app }} 
                                <a href="{{$row_f->bank->information->app_store}}" target="_blank"><i class="fab fa-app-store-ios fa-lg" style="color: #3aa5d0"></i></a> &nbsp; 
                                <a href="{{$row_f->bank->information->play_market}}" target="_blank"><i class="fab fa-google-play fa-lg"></i></a>
                            </span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="col-title">@lang('lang.rating')</div>
                    <ul>
                        <li>
                            <span class="sub font-16">@lang('lang.citizen_ranking'):</span>
                            <span><a href="{{route('ranking-citizen', ['locale' => App::getLocale()])}}" class="btn-outline-dark-blue btn-badge ex-sm-block" target="_blank">@lang('lang.ranking_position',['rank'=> cache($row_f->bank->citizenRankingKey()) ]) <i class="fas fa-external-link-alt"></i></a></span>
                        </li>
                        <li style="padding-top: 10px; padding-bottom: 10px;">
                            <a href="{{ route('review.add',['locale' => App::getLocale()]) }}" class="btn-xn btn-xn-primary btn-xn-justify" style="max-width: 200px; color: white !important;">
                                @lang('lang.v2.index_page.text_btn')
                            </a>
                        </li>
                        {{-- <li><span class="sub">@lang('lang.popular_rating'):</span>
                            <span>@if($row_f->bank->ratings){{ $row_f->bank->ratings->ommabop }}@else - @endif</span>
                        </li>
                        <li><span class="sub">@lang('lang.average_rating'):</span>
                            <span>@if($row_f->bank->ratings){{ $row_f->bank->ratings->urtacha }}@else - @endif</span>
                        </li> --}}
                    </ul>
                </div>
            </div>
        </div>


    </div>

</div>