@if($single_bank_id < 1)
    @if(count($data['max_dp_id']) <= 0)
        <div class="card-body text-center pb-3 col-md-12 border-bottom border-r-8 shadow-lg--hover  bg-white mb-2">
            <h3 class="mt-3 mb-3">@lang('lang.not_found')</h3>
        </div>
    @endif
    @php
        $count_sub_row_dp = 0;
    @endphp
    @if($data['max_dp_id'])
    @foreach($data['max_dp_id'] as $row_f)

        @php
            $count_sub_db = 0;
            $count_sub_row_dp += 1;
        @endphp
        @foreach($data['data'] as $key =>  $sub_row)
            @if($row_f->bank_id == $key)
                @foreach($sub_row as $row)
                    @if($row->id != $row_f->id)
                        @php $count_sub_db += 1; @endphp
                    @endif
                @endforeach
            @endif
        @endforeach 

        @if($date_dp)
            @php
              $percent = $row_f->otherPercent($date_dp);
              $date_type = $row_f->otherDateType($date_dp);
              $date_type_date = $row_f->otherDateTypeDate($date_dp);
              $dateMonth = $row_f->dateMonth($date_dp);
            @endphp
        @else
            @php
              $percent = $row_f->deposit_percent;
              $date_type = $row_f->date_type_id;
              $date_type_date = $row_f->deposit_date;
              $dateMonth = $row_f->dateMonth($row_f->date_type_id);
            @endphp
        @endif
        <section class="item-section" data-date="{{ $dateMonth }}" data-percent="{{ $percent }}" data-date-type="">
            <div class="filter-item"> 
                <div class="tr">
                    <div class="td bank-logo">
                        <div class="logo-wrap">
                            {{-- <div class="d-flex justify-content-center" style=" text-transform: uppercase; font-size:12px; text-align: center !important;">
                                <span class="s">
                                     <a class="text-white" href="{{ route('info-bank',['locale'=>App::getLocale(),'slug'=>$row_f->bank->slug]) }}">
                                         {{ $row_f->bank->name_uz }}
                                     </a>
                                </span>
                            </div> --}}
                            <div class="logo">
                                <img src="/{{ $row_f->bank->image }}" alt="Image Bank">
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="label">
                                    <span class="o">
                                         <a id="other-content-{{ $row->id }}" data-bank-id="{{ $row_f->bank->id }}" data-this-id="{{ $row_f->id }}"
                                            data-click-status="false" data-filter-type="{{ $filter_type }}"
                                            data-count="{{ $count_sub_db }}"
                                            data-dp-list-url="{{ route('filter-other-dp',App::getLocale()) }}" class=" btn-others-deposits">
                                            @lang('lang.others') {{ $count_sub_db }}
                                            <i class="fa fa-angle-down"></i>
                                         </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="td d-flex flex-column justify-content-center">
                        <div class="aktsiya">
                            <a href="{{ route('view-deposit',['locale'=> App::getLocale(),'slug'=> $row_f->slug]) }}">
                                "{{ $row_f->name() }}"
                           </a>
                            @if($row_f->aktsiya)
                                <div class="promotion">
                                    <img src="/temp/images/aktsiya.svg" alt="promotion image">
                                </div>
                            @endif
                        </div>
                        <div class="big ">
                           {{ $percent }}% @if( count($row_f->onMobileApp()) > 0 or count($row_f->deposit_percent_date()) > 0 )*@endif
                        </div>
                    </div>
                    <div class="mobile-row-divider"></div>
                    <div class="td d-flex flex-column justify-content-center">
                        @if(!$r_price and !$date_dp)
                            @if($row_f->min_sum)
                                <div class="big">{{ number_format($row_f->min_sum)}} @if($row_f->priceCurrency->name()){{ $row_f->priceCurrency->name() }}@endif</div>
                            @else
                                <div class="big">
                                    @foreach(config('global.type_sum') as $k => $v)
                                        @if($row_f->type_sum)
                                            @if($v == $row_f->type_sum) @lang('lang.'.$k) @endif
                                        @endif
                                    @endforeach
                                </div>
                            @endif
                            <div class="small">@lang('lang.min_sum')</div>
                        @else
                            @if(!empty($r_price))
                                <div class="big">{{ number_format(Benefit::benefit($row_f->id,$percent,$date_type,$date_type_date,$r_price))}} @if($row_f->priceCurrency->name()){{ $row_f->priceCurrency->name() }}@endif</div>
                                @if(!$row_f->otherDate($date_dp))
                                   @if(!$row_f->deposit_date_string)
                                        <?php $b_date = $row_f->deposit_date." ".__('lang.day'); ?>
                                    @else
                                        <?php $b_date = $row_f->dateStringLang(App::getLocale()); ?>
                                    @endif
                                @else   
                                   <?php $b_date = $row_f->otherDate($date_dp); ?>
                                @endif
                                <div class="small" style="text-transform: lowercase;">@lang('lang.benefit_date',['date'=>$b_date])</div>
                            @elseif($row_f->min_sum)
                                <div class="big">{{ number_format($row_f->min_sum)}} @if($row_f->priceCurrency->name()){{ $row_f->priceCurrency->name() }}@endif</div>
                                <div class="small">@lang('lang.quantum')</div>
                            @endif
                        @endif
                    </div>
                    <div class="td d-flex align-items-center">
                        <div class="big text-lowercase">
                            @if($date_dp)
                                @if(!$row_f->otherDate($date_dp))
                                   @if(!$row_f->deposit_date_string)
                                        {{ $row_f->deposit_date." ".__('lang.day') }}
                                    @else
                                        {{ $row_f->dateStringLang(App::getLocale()) }}
                                    @endif
                                @else   
                                    {{ $row_f->otherDate($date_dp) }}
                                @endif
                            @else
                                @if(!$row_f->deposit_date_string)
                                    {{ $row_f->deposit_date." ".__('lang.day') }}
                                @else
                                    {{ $row_f->dateStringLang(App::getLocale()) }}
                                @endif
                            @endif
                            <div class="small date" style="display: none;">@lang('lang.date')</div>
                        </div>
                    </div>
                    <div class="mobile-row-divider"></div>
                    <div class="td d-flex flex-column mobile-btn-container">
                        <a id="other-content-{{ $row->id }}" data-bank-id="{{ $row_f->bank->id }}" data-this-id="{{ $row_f->id }}"
                            data-click-status="false" data-filter-type="{{ $filter_type }}"
                            data-count="{{ $count_sub_db }}"
                            data-dp-list-url="{{ route('filter-other-dp',App::getLocale()) }}" class="btn--medium mobile-others btn-others-deposits">
                            @lang('lang.others') {{ $count_sub_db }}
                            <i class="fa fa-angle-down fa-lg"></i>
                         </a>
                    </div>
                    <div class="td d-flex flex-column align-items-lg-end justify-content-center mobile-50">
                        <a
                           data-add-route="{{ route('add-comparison') }}"
                           data-remove-route="{{ route('remove-comparison') }}"
                           data-name="{{ $row_f->name_uz }}" id="btn-id-{{ $row_f->id }}"
                           data-status="" data-id="{{ $row_f->id }}" class="add-to-balance add-comparison">
                        </a>
                        {{-- <a href="#" class="add-to-balance added-to-balance"></a>--}}
                        <a href="{{ route('go-to-order-deposit', ['local'=>App::getLocale(), 'slug'=>$row_f->slug]) }}" class="btn btn--medium btn--blue mobile-hidden" style="margin-bottom: 10px">@lang('lang.v2.order.apply')</a>
                        <a href="#" class="btn btn--medium btn--blue btn--arrow detailed-btn">@lang('lang.detailed_btn')</a>
                    </div>
                    <div class="td d-flex flex-column mobile-show" style="width: 100%">
                        <a href="{{ route('go-to-order-deposit', ['local'=>App::getLocale(), 'slug'=>$row_f->slug]) }}" class="btn btn--medium btn--blue">@lang('lang.v2.order.apply')</a>
                    </div>
                </div>
                <div class="tr-more">
                    <div class="head">
                        <div class="divider"></div>
                    </div>
                    @include('frontend.deposits.detailed-tabs')
                    <div class="foot">
                        <div class="divider"></div>
                        <a href="{{ route('view-deposit',['locale'=>App::getLocale(), 'slug' => $row_f->slug]) }}" class="btn btn--medium btn--blue ">@lang('lang.more_detailed')</a>
                        <a href="{{ $row->link(App::getLocale()) }}" target="_blank" class="btn btn--medium btn--light_blue">@lang('lang.go_site')</a>
                    </div>
                </div>
            </div>
            <div class="why">
                <div class="arrow-card__divider" style="width: 90vw"> </div>
            </div>
            <div id="others-deposits-content-{{$row_f->id}}" style="display: none;" class="min-content-father"></div>

        @php $count_sub_row_dp += $count_sub_db; @endphp
        </section>
    @endforeach
    @endif
    <input id="count-depozit" type="hidden" data-text="(@lang('lang.select_deposit',['count' => $count_sub_row_dp]))" value="{{ $count_sub_row_dp }}">
@elseif($single_bank_id>0)
    @include('frontend.deposits.ajax-content-for-single-bank')
@endif

<script src="{{ asset('/js/deposit.ajax.content.js') }}"></script>
@php
    $sess = Session::get('comparison-deposit');
@endphp
@if($sess and count($sess) > 0)
    @foreach($sess as $k => $v)
        <script>
            $('#btn-id-'+'{{ $v }}').attr('data-status',true).addClass('added-to-balance');
        </script>
    @endforeach
@endif