@extends('layouts.app')
@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
            <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale())." - ".$deposit->name() }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection
@section('css')
    <style>
        .list-style-none > ul > li:before{
            content: none;
        }
        .select2-container--default .select2-results__option::before {
            content: none !important;
        }
        .select2-container--default .select2-results__option {
            padding-left: 10px !important;
        }
        .col-6.col-md-3 a,
        .col-6.col-md-3 button{
            width: 100%;
            min-width: auto;
        }
    </style>
@endsection
@section('content')
    <div class="section ">
        <div class="saving-head section-gap--small mt-3 mb-3">
            <div class="medium-container">
               {{-- <div class="saving-nav"><a href="#">Foiz stavkasi</a>  <a href="#">Foizlarni to’lash davriyligi</a>  <a href="#">Omonatni to’ldirish</a>  <a href="#">Qisman yechish</a>  <a href="#">Omonat ochish yo’li</a>
                </div>--}}
                <div class="saving-head__wrap row mt-5 ">

                    <div class="col-md-12 d-flex deposit-heading" style="align-items: center">
                        <div class="col-4 col-md-4 logo pl-0 pr-0 mb-0">
                            <a href="{{ route('info-bank',['locale'=>App::getLocale(),'slug'=>$deposit->bank->slug]) }}">
                                <img style="float:left" src="/{{ $deposit->bank->image }}" alt="Bank logo">
                            </a>
                        </div>
                        <div class="col-md-8 title mb-0 pl-0 pr-0">                           
                            @lang('lang.deposit_from_bank',['name'=>$deposit->name(),'bank' => $deposit->bank->name()])                            
                        </div>
                    </div>

                   {{-- <div class="col-md-4 d-flex align-items-center">
                        <div class="info">
                            <div>
                                <span>@lang('lang.percent_rate'): </span>
                                <span style="color:#3aa5d0">{{ $deposit->deposit_percent }}%</span>
                            </div>
                            <div>
                                <span>@lang('lang.date'): </span>
                                <span style="color:#3aa5d0" >
                                    @if(!$deposit->deposit_date_string)
                                        {{ $deposit->deposit_date." ".__('lang.day') }}
                                    @else
                                        {{ Yt::trans($deposit->deposit_date_string,'uz') }}
                                    @endif
                                </span>
                            </div>
                            <div><span>@lang('lang.min_sum'): </span>
                                <span style="color:#3aa5d0">
                                    {{ number_format($deposit->min_sum)}}
                                    @if($deposit->priceCurrency->name())
                                        {{ $deposit->priceCurrency->name() }}
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>

        <div class="divider-ui--full"></div>

        <div class="credit-info-how section-gap mt-50">
            <div class="medium-container">
                <div class="tab-nav tab-nav--center">
                    <a href="#">
                        <div class="b-title">@lang('lang.rules')</div>
                    </a>
                    <a href="#">
                        <div class="b-title">@lang('lang.table_percent')</div>
                    </a>
                </div>
                <div class="tab-caption">
                    <div class="tab-pane">
                        <div class="how-to-repay col-md-12 pl-0 pr-0">
                            <div class=" list-style-none col-md-6 pl-0 pr-0 font-dp">
                                <ul>
                                    <li class="d-flex">
                                        <div>
                                            <i class="fa fa-database"></i>
                                        </div>
                                        <div>
                                            <span><b>@lang('lang.min_sum'):</b>&nbsp;
                                                @if($deposit->min_sum)
                                                      {{ number_format($deposit->min_sum)}} @if($deposit->priceCurrency->name()){{ $deposit->priceCurrency->name() }}@endif
                                                @else
                                                    @foreach(config('global.type_sum') as $k => $v)
                                                      @if($deposit->type_sum)
                                                          @if($v == $deposit->type_sum) @lang('lang.'.$k) @endif
                                                      @endif
                                                    @endforeach
                                                @endif
                                            </span>
                                        </div>
                                    </li>
                                    <li class="d-flex">
                                        <div>
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                        <div>
                                            <span>
                                                <b>@lang('lang.deposit_date'):</b>&nbsp;@if(!$deposit->deposit_date_string){{ $deposit->deposit_date." ".__('lang.day') }} @else {{ $deposit->dateStringLang(App::getLocale()) }}@endif
                                            </span>
                                        </div>
                                    </li>
                                    <li class="d-flex">
                                        <div>
                                            <i class="fa fa-sign-in"></i>
                                        </div>
                                        <div>
                                            <span>
                                                <b>@lang('lang.account_fill'):</b>&nbsp;
                                                @if($deposit->account_fill)
                                                    @if($deposit->accountFillRules(App::getLocale()))
                                                        {{$deposit->accountFillRules(App::getLocale())}}
                                                    @else
                                                        @lang('lang.existent')
                                                    @endif
                                                @else
                                                    @lang('lang.not_existent')
                                                @endif
                                            </span>
                                        </div>                 
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o"></i><b>@lang('lang.percent_paid_period'):</b>&nbsp;<span>{{ $deposit->payPercentPeriod->name() }}</span>
                                    </li>
                                </ul>
                            </div>
                            <div class=" list-style-none col-md-6 pl-0 pr-0 font-dp">
                                <ul>
                                    <li class="d-flex">
                                        <div>
                                            <i class="fa fa-sign-out"></i>
                                        </div>
                                        <div>
                                            <b>@lang('lang.partly_take'):</b>
                                            <span>
                                                @if($deposit->partly_take)
                                                    @if($deposit->partlyTakeRules(App::getLocale()))
                                                        {{ $deposit->partlyTakeRules(App::getLocale()) }}
                                                    @else
                                                        @lang('lang.exist')
                                                    @endif 
                                                @else
                                                     @lang('lang.does_not_exist') 
                                                @endif
                                            </span>
                                        </div>
                                    </li> 
                                    <li class="d-flex">
                                        <div>
                                            <i class="fa fa-sign-out"></i>
                                        </div>
                                        <div>
                                            <b>@lang('lang.privilege_take'):</b>&nbsp;
                                            <span>
                                            @if($deposit->privilege_take)
                                                @if($deposit->privilegeTakeText(App::getLocale()))
                                                    {{ $deposit->privilegeTakeText(App::getLocale()) }} 
                                                @else 
                                                    @lang('lang.exist') 
                                                @endif 
                                            @else 
                                                @lang('lang.does_not_exist')
                                            @endif</span>
                                        </div>
                                    </li>

                                    <li class="d-flex">
                                        <div>
                                            <i class="fa fa-percent"></i>
                                        </div>
                                        <div>
                                            <b>@lang('lang.percents_capitalization'):</b>&nbsp;
                                            <span>
                                                @if($deposit->percents_capitalization) 
                                                    @if($deposit->percentsCapitalizationRules(App::getLocale())) 
                                                        {{ $deposit->percentsCapitalizationRules(App::getLocale()) }} 
                                                    @else 
                                                        @lang('lang.exist')
                                                    @endif 
                                                @else 
                                                    @lang('lang.does_not_exist') 
                                                @endif</span>
                                        </div>
                                    </li>
                                    <li class="d-flex">
                                        <div class="pl-5">
                                            <i class="fa fa-mobile-phone fa-lg"></i>
                                        </div>
                                        <div>
                                            <b>@lang('lang.dp_open_type'):</b>
                                            &nbsp;<span>
                                                <?php $countT = count($deposit->openType()); $i=0; $and = __('lang.and'); ?>
                                                    @foreach($deposit->openType() as $openT)
                                                        <?php $i +=1; ?> 
                                                            {{ $openT->name() }} 
                                                            @if($i < $countT ) 
                                                                {{ $and }} 
                                                            @endif
                                                    @endforeach
                                                </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="list-style-none col-md-12 mt-20 pl-0 pr-0 font-dp">
                                <ul>
                                    <li>
                                         <i class="fa fa-check-circle-o"></i><b>@lang('lang.other_rules'):</b>
                                         <span>@if($deposit->text_uz)
                                        <pre style='white-space: pre-wrap; font-family: Gilroy, "sans-serif"; margin-top:0px; text-align: justify'>{{ $deposit->text() }}</pre>
                                      @else @lang('lang.does_not_exist')  @endif</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane">
                        <div class="saving-table">
                            <div class="table-default">
                                <div class="thead">
                                    <div class="tr">
                                        <div class="th">@lang('lang.deposit_date')</div>
                                        <div class="th">@lang('lang.percent_rate') </div>
                                        @if(count($deposit->onMobileApp()) > 0) <div class="th">@lang('lang.input_mobile_app') </div> @endif
                                    </div>
                                </div>
                                <div class="tbody">
                                    @if(count($deposit->deposit_percent_date()) <= 0)
                                        <div class="tr " style="border-bottom: solid 1px">
                                            <div class="td" style="text-transform: lowercase;">
                                                @if(!$deposit->deposit_date_string){{ $deposit->deposit_date." ".__('lang.day') }} @else {{ $deposit->dateStringLang(App::getLocale()) }} @endif
                                            </div>
                                            <div class="td">{{ $deposit->deposit_percent }}%</div>
                                             @if(count($deposit->onMobileApp()) > 0)
                                                <div class="td"></div>
                                            @endif
                                        </div>
                                    @endif
                                    @php $sum_percent = 0; $i_c = 0; $d=[]; $s_percent = 0; $all_m = 0;
                                        $sum_percent2 = 0; $i_c2 = 0; $d2=[]; $s_percent2 = 0; $all_m2 = 0;
                                    @endphp
                                    @foreach($deposit->deposit_percent_date() as $dp_percents)
                                        @if($dp_percents->percent_consider == config('global.percent_consider_id.id.differensial_date'))
                                         @php $i_c +=1; @endphp
                                            <?php  $dates = explode('-', $dp_percents->date);
                                             if($dp_percents->date){
                                                if(strpos($dp_percents->date,'-')){
                                                      $date1 = $dates[0]; 
                                                       
                                                      $date2 = $dates[1]; 
                                                      $dd  = (float)$date2-(float)$date1 + 1;

                                                      $all_m += $dd;

                                                      $per_m = (float)$dp_percents->percent / 12;

                                                      $sum_pr = $dd * $per_m;

                                                      $s_percent += $sum_pr;
                                                 } else {
                                                      $all_m += 1;

                                                      $per_m = (float)$dp_percents->percent / 12;

                                                      $sum_pr = 1 * $per_m;

                                                      $s_percent += $sum_pr;
                                                 }
                                             }
                    
                                            ?>
                                        @endif
                                       
                                        <div class=tr>
                                            <div class="td" style="text-transform: lowercase;">{{ $dp_percents->date }} {{ $dp_percents->dateName($dp_percents->date) }}</div>
                                            <div class="td">{{ $dp_percents->percent }}%</div>
                                            @if(count($deposit->onMobileApp()) > 0)
                                            @php $onMobileApp = $dp_percents->onMobileApp($dp_percents->id,$deposit->id,$dp_percents->date,4); @endphp
                                                
                                                @if($onMobileApp['date'] == $dp_percents->date )

                                                    @if($dp_percents->percent_consider == config('global.percent_consider_id.id.differensial_date'))
                                                     @php $i_c2 +=1; @endphp
                                                        <?php $dates2 = explode('-', $onMobileApp['date']);
                                                         if($onMobileApp['date']){
                                                           if(strpos($onMobileApp['date'],'-')){
                                                              $date1 = $dates[0]; 
                                                              $date2 = $dates[1]; 
                                                              $dd2  = (float)$date2-(float)$date1 + 1;

                                                              $all_m2 += $dd2;

                                                              $per_m2 = (float)$onMobileApp['percent'] / 12;

                                                              $sum_pr2 = $dd2 * $per_m2;

                                                              $s_percent2 += $sum_pr2;
                                                           } 
                                                         }
                                                        ?>
                                                    @endif
                                                    <div class="td">{{ $onMobileApp['percent'] }}%</div>
                                                @else
                                                    <div class="td"></div>
                                                @endif
                                            @endif
                                        </div>
                                    @endforeach
                                    @if($i_c > 0)
                                        <div class="tr">
                                            <div class="td">@lang('lang.effect_percent_year')</div>
                                            @if($s_percent and $all_m != 0)
                                                <div class="td">{{ number_format(($s_percent / $all_m )*12,2)}}%</div>
                                            @endif
                                            @if( $i_c2 > 0) 
                                                @if($s_percent2 and $all_m2 != 0)                        
                                                <div class="td">{{ number_format(($s_percent2 / $all_m2 )*12,2)}}%</div>
                                                @endif
                                            @else
                                                
                                            @endif
                                        </div>
                                    @endif  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="divider-ui--full" id="order-deposit"></div>

        <div class="order-deposit section-gap--small">
            <div class="medium-container">
                @include('frontend.components.order-form', ['title'=>'v2.order.deposit', 'type'=>'deposit', 'product_id'=>$deposit->id])
            </div>
        </div>

        <div class="divider-ui--full"></div>

        <div class="calculate-credit section-gap--small  mb-50" id="deposit_calc_section">
            <div class="medium-container">
                <div class="row">
                    <div class="col-md-8">
                        <form id="deposit-calculation" action="{{ route('calculation-deposit-percent',App::getLocale()) }}" method="GET">
                            <input type="hidden" name="deposit_id" value="{{ $deposit->id }}">
                            @if(count($deposit->deposit_percent_date()) <= 0) 
                            <input type="hidden" data-type="0" name="percent" value="{{ $deposit->deposit_percent }}">
                            @else
                            <input type="hidden" data-type="1" name="percent" value="{{ $deposit->deposit_percent_date()[0]->percent }}">
                            @endif
                            
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('lang.sum_deposit')</label>
                                    <input required="required" id="f-el-price" class="form-control" name="sum" data-validate="true"  data-error-text="@lang('validation.numeric',['attribute' => Yt::trans('qiymat','uz')])" placeholder="1 000 000">
                                    <span class="error text-danger" id="price-error-sum" style="display: none;"></span>
                                </div>
                            </div>
                            @php
                                function dateHelper($x) {
                                    if($x == 8 || $x == 3) return "yil";
                                    else if($x == 9 || $x == 2) return "oy";
                                    else return "kun"; 
                                }
                            @endphp
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('lang.deposit_date')</label>
                                    <select class="select2__js" name="date" id="dateselector">
                                        @if(count($deposit->deposit_percent_date()) <= 0)                                            
                                            <option value="{{ $deposit->deposit_date . " " . dateHelper($deposit->date_type_id) }}">{{ $deposit->deposit_date . " " . dateHelper($deposit->date_type_id) }}</option>
                                        @else
                                        @foreach($deposit->deposit_percent_date() as $dp_percents)
                                            <option data-percent="{{$dp_percents->percent}}" value="{{ $dp_percents->date . " " . dateHelper($dp_percents->date_type) }}">{{ $dp_percents->date }} {{ $dp_percents->dateName($dp_percents->date) }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <span class="error text-danger" id="price-error-date" style="display: none;"></span>
                                </div>
                            </div>
                            {{-- <div class="col-md-6">
                                <div class="form-group">
                                    <label>@lang('lang.dp_open_type')</label>
                                    <select class="select2__js" name="type" data-empty="@lang('lang.any')">
                                          <option value="">@lang('lang.choose')</option>
                                        <option value="1">Turi</option>
                                        <option value="2">Turi</option>
                                        <option value="3">Turi</option>
                                    </select>
                                    <span class="error text-danger" id="price-error-type" style="display: none;"></span>
                                </div>
                            </div> --}}
                            {{-- <div class="col-md-6" style="display: none">
                                <div class="form-group">
                                    <label>@lang('lang.percents_capitalization')</label>
                                    <select class="select2__js" name="capitalization" data-empty="@lang('lang.any')">
                                        <option value="0">@lang('lang.no')</option>
                                        <option  selected='selected' value="1">@lang('lang.yes')</option>
                                    </select>
                                    <span class="error text-danger" id="price-error-capitalization" style="display: none;"></span>
                                </div>
                            </div> --}}
                            <div class="col-6 col-md-3">
                                <button class="btn btn--medium btn--blue btn-deposit-calculation">@lang('lang.calculation')</button>
                            </div>
                        <div class="col-6 col-md-3">
                                <a href="@if($deposit->link ) {{ $deposit->link(App::getLocale()) }} @else {{ $deposit->bank->site() }}@endif"  target="_blank" class="btn btn--medium btn--light_blue mt-10">@lang('lang.go_site')</a>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <div class="calculate-credit__info"><span>@lang('lang.percent')</span>
                            <div id="span-percent">
                                @if(count($deposit->deposit_percent_date()) <= 0) 
                                    {{$deposit->deposit_percent . "%"}}
                                @else
                                @php
                                    echo $deposit->deposit_percent_date()[0]->percent . "%";
                                @endphp
                                @endif
                            </div>
                        </div>
                        <div class="calculate-credit__info"><span>@lang('lang.calc_percent')</span>
                            <div id="span-percent-sum">0.00</div>
                        </div>
                        <div class="calculate-credit__info"><span>@lang('lang.amount_end_period')</span>
                            <div id="span-all-sum">0.00</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="divider-ui--full"></div>

        <div class="section-gap--small mt-20">
            <div class="medium-container border-bottom text-center saving-head__wrap pb-50">
                <div class="title pb-30" >
                    @lang('lang.other_services_of_bank', ['bank'=>$deposit->bank->name()])
                </div>
                <div class="advantages__grid other-offers">
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-deposits', ['locale'=>App::getLocale(), 'slug'=> $deposit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/1.svg">
                            </div>
                            <span class="more">@lang('lang.deposits')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credits-consumer',['locale'=>App::getLocale(), 'slug'=> $deposit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/3.svg">
                            </div>
                            <span class="more">@lang('lang.eat_credits')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credits-auto',['locale'=>App::getLocale(), 'slug'=> $deposit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/4.svg">
                            </div>
                            <span class="more">@lang('lang.auto_credit')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credits-mortgage',['locale'=>App::getLocale(), 'slug'=> $deposit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/2.svg">
                            </div>
                            <span class="more">@lang('lang.credit_mortgage')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credits-education',['locale'=>App::getLocale(), 'slug'=> $deposit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/6.svg">
                            </div>
                            <span class="more">@lang('lang.education_credit')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credits-overdraft',['locale'=>App::getLocale(), 'slug'=> $deposit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/overdraft.svg">
                            </div>
                            <span class="more">@lang('lang.overdraft_credit')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credits-microcredit',['locale'=>App::getLocale(), 'slug'=> $deposit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/5.svg">
                            </div>
                            <span class="more">@lang('lang.micro_debt')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-debit-cards', ['local'=>App::getLocale(), 'slug'=>$deposit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/debit-card.svg">
                            </div>
                            <span class="more">@lang('lang.debit_cards')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('single-bank-credit-cards', ['local'=>App::getLocale(), 'slug'=>$deposit->bank->slug]) }}" target="_blank">
                            <div class="icon">
                                <img src="/temp/images/icons/credit-card.svg">
                            </div>
                            <span class="more">@lang('lang.credit_cards')</span>
                        </a>
                    </h1>
                </div>
            </div>
            <div class="divider-ui--full"></div>
            <div class="col-md-12 medium-container border-bottom row pt-25">
                <div class="col-md-4 justify-content-center pb-15">
                    <div class="big  d-min">
                        
                        <h1 class="color-a mb-10 mt-0"><a target="_blank" href="{{ route('info-bank',['locale'=>App::getLocale(),'slug' => $deposit->bank->slug]) }}">@if($deposit->bank->name()){{ $deposit->bank->name() }} @else {{ $deposit->bank->name_uz }} @endif</a></h1>
                    </div>
                    <div class="small mt-5 mb-5 licence">{{ $deposit->bank->information->licenseReplace() }}</div>
                    <div class="small">@lang('lang.ownership'): {{ $deposit->bank->ownership->name() }} </div>
                </div>
                <div class="col-md-4 align-items-center font-18 pb-15">
                    <div class="big d-flex mb-10">
                        <div class="div-icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="pl-10">
                            {{ $deposit->bank->information->phone1 }}
                        </div>
                    </div>
                    <div class="big d-flex mb-10">
                        <div class="div-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <div class="pl-10 color-a">
                            <a href="{{ $deposit->bank->information->site() }}" target="_blank">{{ $deposit->bank->information->site() }}</a>
                        </div>
                    </div>
                    <div class="big d-flex mb-10">
                        <div class="div-icon">
                            <i class="fa fa-telegram"></i>
                        </div>
                        <div class="pl-10">
                            @if($deposit->bank->information->telegram_channel)
                                <a href="https://t.me/{{ substr($deposit->bank->information->telegram_channel,1)}}" target="_blank"> {{ $deposit->bank->information->telegram_channel }}</a>
                            @else
                                --
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-4 align-items-center font-18 pb-15">
                    <div class="big d-flex mb-10">
                        <div class="div-icon">
                            <i class="fa fa-volume-control-phone"></i>
                        </div>
                        <div class="pl-10">
                            @if($deposit->bank->information->phone2)
                                {{ $deposit->bank->information->phone2 }}
                            @else
                                {{ $deposit->bank->information->phone1 }}
                            @endif
                        </div>
                    </div>
                    <div class="big d-flex mb-10">
                        <div class="div-icon">
                            <i class="fa fa-envelope "></i>
                        </div>
                        <div class="pl-10 color-a">
                            <a href="mailto:{{ $deposit->bank->information->mail }}">{{ $deposit->bank->information->mail }}</a>
                        </div>
                    </div>
                    <div class="big d-flex mb-10 ">
                        <div class="div-icon font-20">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="pl-10">
                            {{ $deposit->bank->information->address() }}
                        </div>
                    </div>
                </div>
            </div>

                {{--<div class="medium-container">
                    <div class="b-title">{{ Yt::trans('Bank filiallari','uz') }}</div>
                </div>
                <div id="map2"></div>--}}
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#dateselector').on('change', function() {
                if($("[name='percent'").attr('data-type') == 1) {
                    let percent = $('[name="date"] option:selected').attr('data-percent');
                    $('#span-percent').text(percent + "%");
                    $("[name='percent'").val(percent);
                }
            });
            
            $('#f-el-price').on('keyup',function(){
                let v = $(this).val();
                if($(this).val().length != 0){
                    if(!$.isNumeric(v)){
                        $(this).attr('data-validate',false);
                        $('#price-error-sum').text($(this).attr('data-error-text')).show();
                    }
                    if($.isNumeric(v)){
                        $(this).attr('data-validate',true);
                        $('#price-error-sum').hide();
                    }
                }
                if($(this).val().length == 0){
                    $(this).attr('data-validate',true);
                    $('#price-error-sum').hide();
                }
            });

            $('.menu-deposit').addClass('current');

            $('.btn-deposit-calculation').click(function(e){
                e.preventDefault();
                let calc_attr = $('#deposit-calculation');

                $.ajax({
                    type: $(calc_attr).attr('method'),
                    url: $(calc_attr).attr('action'),
                    data: calc_attr.serialize(),
                    success:function(data){
                        if(data.error){
                            $.each(data.error,function(i,v){
                                $('#price-error-'+i).show().text(v);
                            });
                        } else {
                            $('.error').hide();
                            $('#span-percent-sum').text(data.month_sum+' '+data.currency);
                            $('#span-all-sum').text(data.all_sum+' '+data.currency);
                        }

                    },error:function(){
                        alert('error')
                    }

                });
            });

            $('#calc-btn').click(function(){
                $('html, body').animate({
                    scrollTop: $("#deposit_calc_section").offset().top-70
                }, 1500);
            });

        });


    </script>

    {{-- <script src="https://api-maps.yandex.ru/2.1.74/?lang=ru_RU"></script> --}}
    <script>
        /*function init(){
            let a=[41.294313,69.282831];
            window.myMap=new ymaps.Map("map2",{center:a,zoom:11}),
                myMap.behaviors.disable("scrollZoom"),
                
                myPlacemark1=new ymaps.Placemark(a,{},{
                    overlayFactory:"default#interactiveGraphics",
                    iconLayout:"default#imageWithContent",
                    iconImageHref:"/temp/images/icons/mapMarker.png",
                    iconImageSize:[32,44],
                    iconImageOffset:[-16,-44]}),
                
                myPlacemark2=new ymaps.Placemark([41.312243, 69.314321],{},{
                    overlayFactory:"default#interactiveGraphics",
                    iconLayout:"default#imageWithContent",
                    iconImageHref:"/temp/images/icons/mapMarker.png",
                    iconImageSize:[32,44],
                    iconImageOffset:[-16,-44]}),
                
                myPlacemark3=new ymaps.Placemark([41.328704,69.250901],{},{
                    overlayFactory:"default#interactiveGraphics",
                    iconLayout:"default#imageWithContent",
                    iconImageHref:"/temp/images/icons/mapMarker.png",
                    iconImageSize:[32,44],
                    iconImageOffset:[-16,-44]}),

                myMap.geoObjects.add(myPlacemark1).add(myPlacemark2).add(myPlacemark3)
            } ymaps.ready(init)*/
    </script>
@endsection