<div class="modal-ui" data-modal-name="detailed-deposit-modal">
    <div class="modal-ui__dialog detailed-search">
        <div class="modal-ui__content">
            <div class="modal-ui__close-btn"></div>
            <h2 class="b-title">@lang('lang.addition_rules_search')</h2>
            <form id="large-search-form"  action="{{ route('large-search-deposit',App::getLocale()) }}" method="GET" multiple>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>@lang('lang.sum_deposit')</label>
                            <input type="text" class="clear"  data-validate="true"  name="price"  id="large-f-el-price" data-error-text="@lang('validation.numeric',['attribute' => Yt::trans('qiymat','uz')])" >
                            <div class="slidecontainer">
                                <input type="range" min="" step="100000" max="1000000000" value="0" class="slider" id="myRangeLarge">
                            </div>
                            <span id="large-price-error" class="text-danger" style="display: none"></span>                        
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="d-flex justify-content-between">
                            <div class="form-group" style="width:48%">
                                <label >@lang('lang.currency')</label>
                                <select  class="select2__js" data-validate="true" name="currency" id="large-f-el-currency" data-empty="@lang('lang.all')">
                                    @foreach($currency as $cur)
                                        <option value="{{ $cur->id }}">{{ $cur->name }}</option>
                                    @endforeach
                                    <option value="0"> @lang('lang.all')</option>
                                </select>
                            </div>
                            <div class="form-group" style="width:48%">
                                <label>@lang('lang.date')</label>
                                <select class="select2__js clear" data-validate="true" id="large-f-el-date" name="date_deposit_type" data-empty="@lang('lang.all')">
                                    <option value="0"> @lang('lang.all')</option>
                                    @foreach($type_date as $type)
                                        <option value="{{ $type->id }}">{{ $type->name() }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>@lang('lang.region')</label>
                            <select class="select2__js clear" id='f-el-city'  name="region" data-empty="@lang('lang.all')" data-dropdown="open">
                                <option value="0"> @lang('lang.all')</option>
                                @foreach($cities as $city)
                                    <option class="border-bottom" style="font-weight: bold;" value="{{ $city->id }}">{{ $city->name() }}</option>
                                    @foreach($city->children as $child_r)
                                        <option  value="{{ $child_r->id }}">&nbsp;&nbsp;{{ $child_r->name() }}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>@lang('lang.addition_rules')</label>
                            <select class="select2__js clear" name="additional_rule[]" id="f-el-dp-addition_rules" multiple="multiple" data-empty="@lang('lang.any')" data-dropdown="open">
                                <option value="any"> @lang('lang.any')</option>
                                <option value="account_fill">@lang('lang.account_fill')</option>
                                <option value="partly_take">@lang('lang.partly_take')</option>
                                <option value="privilege_take">@lang('lang.privilege_take')</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>@lang('lang.percents_capitalization')</label>
                            <select class="select2__js clear" name="capitalization" id="f-el-dp-percents_capitalization" data-empty="@lang('lang.all')" >
                                <option value="0"> @lang('lang.all')</option>
                                <option value="1">@lang('lang.yes')</option>
                                <option value="2">@lang('lang.no')</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>@lang('lang.percent_paid_period')</label>
                            <select class="select2__js clear" name="percent_period[]" multiple="multiple" id="dp-percent-period" data-empty="@lang('lang.all')" data-dropdown="open">
                               <option value="any"> @lang('lang.all')</option>
                                @foreach($paid_pariod as $paid)
                                    <option value="{{ $paid->id }}">{{ $paid->name() }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>@lang('lang.dp_open_type')</label>
                            <select class="select2__js clear" name="input_type_dp" id="f-el-dp_open_type" data-empty="@lang('lang.all')">
                                <option value="0"> @lang('lang.all')</option>
                                @foreach($open_type as $open_t)
                                <option value="{{ $open_t->id }}">{{ $open_t->name() }}</option>
                                @endforeach
                                 {{--<!-- <option value="input_in_cash" type='hidden'>@lang('lang.input_in_cash')</option>
                                <option value="input_no_cash" type='hidden'>@lang('lang.input_no_cash')</option> -->
                                <option value="bank_office">@lang('lang.on_bank_office')</option>--}}
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group enabled-search">
                            <label>@lang('lang.banks')</label>
                            <select class="select2__js clear" name="type_banks[]" multiple="multiple" id="select-banks" data-empty="@lang('lang.any')" data-dropdown="open">
                                <option value="any"> @lang('lang.any')</option>
                                <option value="state_bank">@lang('lang.state_bank')</option>
                                <option value="private_bank">@lang('lang.private_bank')</option>
                                <option value="foreign_bank">@lang('lang.foreign_bank')</option>
                                @foreach($banks as $bank)
                                    @if($single_bank_id == $bank->id)
                                        <option value="{{ $bank->id }}" selected> {{ $bank->name_uz }}</option>
                                    @else
                                        <option value="{{ $bank->id }}"> {{ $bank->name_uz }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @include('frontend.components.btn-type-bank')
                </div>
                <div class="buttons">
                    <div class="text-center mb-20">
                        <button type="submit" class="btn btn--medium search-btn-modal btn--blue">@lang('lang.selection')</button>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn--medium btn--blue clear-filter">@lang('lang.clear_filter')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

