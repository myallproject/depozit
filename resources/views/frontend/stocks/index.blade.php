@extends('layouts.app')
@section('content')
@section('meta')
    <meta name="localization" content="{{ app()->getLocale() }}">
    <meta name="request_url" content="/{{ app()->getLocale() }}/stocks/request/send">
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">

    <link rel="stylesheet" href="{{ asset('css/stocks.css') }}">

@endsection
<div class="section">
    <div class="section-gap--small" style="margin-top: 20px">
        <div class="medium-container">
            <h2 class="b-title mb-20">@lang('lang.shares')<span style="font-size: 80%">
            </h2>
        </div>

        <div class="container">

            <div class="card mb-20">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6 col-sm-12">
                            <h6 class="card-title">
                                @lang('stock.shares'):
                            </h6>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a href="?foreign=false"
                                    class="btn btn-info {{ request()->has('foreign') && request()->get('foreign') == 'false' ? 'active' : null }}"
                                    data-value="false">@lang('stock.local')</a>
                                <a href="?foreign=true"
                                    class="btn btn-info {{ request()->has('foreign') && request()->get('foreign') == 'true ' ? 'active' : null }}">@lang('stock.foreign')</a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <h6 class="card-title">
                                @lang('stock.branches'):
                            </h6>
                            <div class="form-group">
                                <form action="{{ \Request::fullUrl() }}" method="get" id="branch">
                                    <input type="hidden" name="foreign"
                                        value="{{ request()->has('foreign') ? request()->get('foreign') : 'false' }}">
                                    <select class="form-control" name="branch" id="">
                                        @foreach ($branches as $key => $val)
                                            <option value="{{ $key }}">
                                                {{ $val }}
                                            </option>
                                        @endforeach
                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <table class="table" id="datatable">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">@lang('stock.name')</th>
                        <th scope="col">@lang('stock.share_cost')</th>
                        <th scope="col">@lang('stock.changes')</th>
                        <th scope="col">@lang('stock.dividend')</th>
                        <th scope="col">@lang('stock.action')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($stocks as $stock)
                        <tr>
                            <th scope="row">{{ $stock->name }}</th>
                            <td>{{ $stock->ordinary_stock_price }} <span class="currency">UZS</span></td>
                            <td>
                                <span class="currency">
                                    {{ $stock->daily_change }} </span>
                            </td>
                            <td>{{ $stock->dividend }} %</td>
                            <td>
                                <button class="btn btn-info buy-button"
                                    data-id=" {{ $stock->id }}">@lang('stock.buy')</button>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

    </div>
</div>
</div>

@include('partials.stocks.request_modal')
@endsection

@section('script')
<script src="{{ asset('js/stocks.js') }}"></script>

@endsection
