@forelse ($banks as $bank)
    @include('frontend.banks.components.search-content')
@empty
    <div class="card-body text-center pb-3 col-md-12  mb-25 mt-25">
        <h3 class="mt-3 mb-3">@lang('lang.not_found')</h3>
    </div>
@endforelse