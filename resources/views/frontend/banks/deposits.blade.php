@extends('layouts.app')

@section('content')

    <section class="page-title pb-3">
        <div class="image-layer" ></div>
        <div class="auto-container">
            <div class="container pl-0 pr-0">
                <div class="col-md-12 bg-white text-center mb-3" style="border: 1px solid; border-color: #dddbd8">
                    <div class="col-md-6 pt-5 media-center text-left m-auto">
                        <h1 style="display:inherit !important;"><a href="{{ route('info-bank',[$bank->slug]) }}">{{ $bank->name_uz }}</a></h1>
                        <sub> SUB TEXT</sub>
                    </div>
                    <div class="col-md-6 text-right media-center"><img alt="Bank Image" width="180" src="/{!!  $bank->image !!} " /></div>
                </div>
                <div class="col-md-12 p-0 mb-3 "  style="font-size: 40px;">
                    <div class="col-md-6 p-0 bg-white shadow-lg--hover hover text-center " style="border: 1px solid; border-color: #dddbd8">
                        <a href="{{ route('info-bank-deposits',[$bank->slug]) }}" class="pt-3 pb-3 d-block "  >
                           <img src="/images/icons/s-icon-5.png">
                            <h3 >@lang('lang.deposits')</h3>
                        </a>

                    </div>
                    <div class="col-md-6 p-0 bg-white shadow-lg--hover hover text-center" style="border: 1px solid; border-color: #dddbd8">
                        <a href="" class="pt-3 pb-3 d-block">
                            <i class="fa fa-credit-card"  style="font-size: 53px; color: #5b96ef"></i>
                            <h3 >@lang('lang.credits')</h3>
                        </a>

                    </div>
                </div>
                <div class="col-md-12 bg-white text-center pb-4 pt-5" style="border: 1px solid; border-color: #dddbd8">
                    <h1 style="display:inherit !important;">@lang('lang.deposits')</h1>
                </div>
            </div>

        </div>
    </section>

    <section class="partners-section pt-0" >
        <div class="auto-container">
            <div class="row clearfix">
                <!--Deposits Block-->
                @foreach($bank->deposits as $deposit)
                <div class="partner-block col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">
                            <div class="link-box">
                                <a href="{{ route('view-deposit',['locale' => App::getLocale(),'slug' => $deposit->slug]) }}">
                                    {{ $deposit->name_uz }}
                                </a>
                            </div>

                            <div class="text">@lang('lang.currency'): {{ $deposit->priceCurrency->name }}</div>
                            <div class="text">@lang('lang.min_sum'): {{ $deposit->min_sum }}</div>
                            <div class="text">@lang('lang.percent'): {{ $deposit->deposit_percent }} %</div>

                            <div class="curve" style="height: 60px !important; "></div>
                        </div>
                        <figure class="image">
                            <a href="{{ route('info-bank',[$bank->slug]) }}">
                                <img width="150" src="/{{ $bank->image }}" alt="">
                            </a>
                        </figure>
                        <a class="add-comparison btn btn-info" data-name="{{ $bank->name_uz}}" id="btn-id-{{ $deposit->id }}"  data-status="" data-id="{{ $deposit->id }}">
                            @lang('lang.comparison')  <i class="fa fa-balance-scale btn-balance-icon " ></i>
                        </a>
                    </div>

                </div>
                @endforeach
            </div>
        </div>
    </section>


@endsection

@section('script')
    <script type="application/javascript">
        $(document).ready(function () {
            $('.add-comparison').on('click',function(){
                $(this).toggleClass('btn-info');
                $(this).toggleClass('btn-warning');
                let status = $(this).attr('data-status');
                let id = $(this).data('id');

                if(status == false || status == 'false'){
                    $(this).attr('data-status',true);
                    $.ajax({
                        url: "{{ route('add-comparison') }}",
                        data: {
                            id: id,
                            url_type: 'deposit'
                        },
                        success:function(data){
                            $(".comparison-amount").text(data);
                        }
                    });
                }
                if(status == "true") {
                    $(this).attr('data-status', false);
                    $.ajax({
                        url: "{{ route('remove-comparison') }}",
                        data: {
                            id: id,
                            url_type: 'deposit'
                        },
                        success:function(data){
                            $(".comparison-amount").text(data);
                        }
                    });
                }
            });
        });
    </script>
    @php
        $sess = Session::get('comparison-deposit');
    @endphp
    @if($sess and count($sess) > 0)
        @foreach($sess as $k => $v)
            <script>
                $('#btn-id-'+'{{ $v }}').attr('data-status',true).removeClass('btn-info');
                $('#btn-id-'+'{{ $v }}').attr('data-status',true).addClass('btn-warning');
            </script>
        @endforeach
    @endif
@endsection
