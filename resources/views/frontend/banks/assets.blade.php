@extends('layouts.app')
@section('css')
    <link href="/css/assets.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="/plugins/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('content')
    <div class="section">
        <div class="partners section-gap mt-10">
            <div class="medium-container">
                <h1 class="b-title mb-50">{{ Yt::trans('BANKLARning MOLIYAVIY FAOLIYATI','uz') }}</h1>
                <div class="filter-form mb-70">
                    <form method="GET">
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class='select2__js' >
                                        <option value="0">Aktivlar bo'yicha</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-8 pl-0 pr-0 d-flex input-daterange" id="date-range">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <div class="date">
                                            <input type="text" class=""  name="start" placeholder="DD/MM/YYYY">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <div class=" date">
                                            <input type="text" class="" name="end" placeholder="DD/MM/YYYY">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="input-daterange input-group" >
                                <input type="text" class="form-control" name="start" />
                                <div class="input-group-append">
                                    <span class="input-group-text bg-info b-0 text-white">TO</span>
                                </div>
                                <input type="text" class="form-control" name="end" />
                            </div>--}}

                        </div>
                    </form>
                </div>
                <div class="table calculator-result">
                    {{-- <div class="thead">
                         <div class="tr">
                             <div class="th">Banklar</div>
                             <div class="th" id="action">Olish</div>
                             <div class="th">
                                 <form action="#">
                                     <div class="form-group">
                                         <input type="text" placeholder="Bank nomini kiriting" class="form-control--small">
                                     </div>
                                 </form>
                             </div>
                         </div>
                     </div>--}}
                    <div class="thead">
                        <div class="tr">
                            <div class="th">@lang('lang.banks')</div>
                            <div class="th" >Сумма активов</div>
                            <div class="th"> Сумма активов</div>
                            <div class="th"> Изменение</div>
                            <div class="th"> Изменение</div>
                        </div>
                    </div>

                    <div class="tbody">
                        @foreach($banks as $bank)
                            <div class="tr">
                                <div class="td">
                                    <span class="name">{{ $bank->name_uz }}</span>
                                </div>
                                <div class="td">
                                    <div>98899 UZS</div>
                                    <div class="small">Valyuta USD</div>
                                </div>
                                <div class="td">
                                    <div>98899 UZS</div>
                                    <div class="small">Summa</div>
                                </div>
                                <div class="td">
                                    <div>22.11.2019</div>
                                    <div class="small">Sana</div>
                                </div>
                                <div class="td">
                                    <div>22.11.2019</div>
                                    <div class="small">Sana</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="/plugins/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script>
        $('#datepicker-1').datepicker({
            autoclose: true
        });
        $('#datepicker-2').datepicker({
            autoclose: true
        });
        jQuery('#date-range').datepicker({
            toggleActive: true
        });
    </script>
@endsection