@extends('layouts.app')
@section('meta')
<title>{{ $bank->information->name() }}</title>
<meta name="description" content="{{ $bank->description() }}" />
    {{--  @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach --}}
@endsection
@section('content')
<style>
    .advantages__grid span {
        font-size: 15px;
        font-weight: 500;
    }
    li>a>div:last-child {
        font-size: 14px;
        font-weight: 500;
    }
    .advantages__item .icon {
        width: 50px;
    }
    a span>span, a div>span{
        border-left: 1px solid #3aa5d0 !important;
        margin-left: 5px !important;
        padding-left: 5px !important; 
    }
</style>
    <div class="section">
        <div class="section-gap--small" style="margin-top: 0px;">
            <div class="medium-container">
                <div class="credit-info-main" style="padding-bottom: 0px;">
                    <div class="row mx-auto" style="width: 100%;">
                        <div class="col-12 col-md-5 text-center" style="padding-left: 0px; padding-right: 0px; padding-top: 10px">
                            <img src="/{{ $bank->image }}" alt="" style="max-width: 100%; max-height: 150px;">
                            <h2 style="margin-top: 0px; font-size: 1.8rem;">{{ $bank->information->name() }}</h2>
                        </div>
                        <div class="col-12 col-md-7 text-center" style="padding: 40px 0px;">
                            <div class="advantages__grid 989-window single-bank-services " style="border: none; width: 100%; padding-left: 10px; padding-right: 10px;">
                                <h1 class="advantages__item mt-0" style="border: none;">
                                    <a href="{{ route('review.list.single', ['locale'=>App::getLocale(), 'slug'=>$bank->slug]) }}" >
                                        <div class="icon">
                                            <img src="/temp/images/icons/message-many.svg">
                                        </div>
                                        <span class="more">@lang('lang.v2.review') <span>{{$reviews}}</span></span>
                                    </a>
                                </h1>
                                <h1 class="advantages__item mt-0" style="border: none;">
                                    <a href="{{ route('single-bank-deposits', ['locale'=>App::getLocale(), 'slug'=> $bank->slug]) }}" >

                                        <div class="icon">
                                            <img src="/temp/images/icons/advantages/1.svg">
                                        </div>
                                        <span class="more">@lang('lang.deposits') <span>{{$deposits}}</span></span>
                                    </a>
                                </h1>
                                <h1 class="advantages__item mt-0" style="border: none;">
                                      <a data-jq-dropdown="#credits-dropdown">
                                        <div class="icon">
                                            <img src="/temp/images/icons/advantages/2.svg">
                                        </div>
                                        <span class="more">@lang('lang.credits') <span>{{$credits}}</span> <i class="fa fa-angle-down fa-lg d-block"></i></span>
                                    </a>
                                    <div id="credits-dropdown" class="jq-dropdown jq-dropdown-tip jq-dropdown-scroll">
                                        <ul class="jq-dropdown-menu"  style="margin-top: 0px !important;">
                                            <li style="display: block; margin-right: 0px !important">
                                                <a href="{{ route('single-bank-credits-consumer',['locale'=>App::getLocale(), 'slug'=> $bank->slug]) }}" class="d-flex align-items-center" >
                                                    <div>
                                                        <img src="/temp/images/icons/advantages/3.svg">
                                                    </div>
                                                    <div>
                                                        @lang('lang.eat_credits') <span>{{$consumer_credits}}</span>
                                                    </div>
                                                </a>
                                            </li>
                                            <li style="display: block; margin-right: 0px !important">
                                                <a href="{{ route('single-bank-credits-auto',['locale'=>App::getLocale(), 'slug'=> $bank->slug]) }}" class="d-flex align-items-center"  >
                                                    <div>
                                                        <img src="/temp/images/icons/advantages/4.svg">
                                                    </div>
                                                    <div>
                                                        @lang('lang.auto_credit') <span>{{$auto_credits}}</span>
                                                    </div>
                                                </a>
                                            </li>
                                            <li style="display: block; margin-right: 0px !important">
                                                <a href="{{ route('single-bank-credits-education',['locale'=>App::getLocale(), 'slug'=> $bank->slug]) }}" class="d-flex align-items-center" >
                                                    <div>
                                                        <img src="/temp/images/icons/advantages/6.svg">
                                                    </div>
                                                    <div>
                                                        @lang('lang.education_credit') <span>{{$education_credits}}</span>
                                                    </div>
                                                </a>
                                            </li>
                                            <li style="display: block; margin-right: 0px !important">
                                                <a href="{{ route('single-bank-credits-microcredit',['locale'=>App::getLocale(), 'slug'=> $bank->slug]) }}" class="d-flex align-items-center" >
                                                    <div>
                                                        <img src="/temp/images/icons/advantages/5.svg">
                                                    </div>
                                                    <div>
                                                        @lang('lang.micro_debt') <span>{{$micro_credits}}</span>
                                                    </div>
                                                </a>
                                            </li>
                                            <li style="display: block; margin-right: 0px !important">
                                                <a href="{{ route('single-bank-credits-mortgage',['locale'=>App::getLocale(), 'slug'=> $bank->slug]) }}" class="d-flex align-items-center" >
                                                    <div>
                                                        <img src="/temp/images/icons/advantages/2.svg">
                                                    </div>
                                                    <div>
                                                        @lang('lang.credit_mortgage') <span>{{$mortgage_credits}}</span>
                                                    </div>
                                                </a>
                                            </li>
                                             <li style="display: block; margin-right: 0px !important">
                                                <a href="{{ route('single-bank-credits-overdraft',['locale'=>App::getLocale(), 'slug'=> $bank->slug]) }}" class="d-flex align-items-center" >
                                                    <div>
                                                        <img src="/temp/images/icons/advantages/overdraft.svg">
                                                    </div>
                                                    <div>
                                                        @lang('lang.overdraft_credit') <span>{{$overdraft_credits}}</span>
                                                    </div>

                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    
                                </h1>
                                <h1 class="advantages__item mt-0" style="border: none;">
                                    <a data-jq-dropdown="#cards-dropdown">
                                        <div class="icon">
                                            <img src="/temp/images/icons/advantages/3.svg">
                                        </div>
                                        <span class="more">@lang('lang.cards') <span>{{$cards}}</span> <i class="fa fa-angle-down fa-lg d-block"></i></span>
                                    </a>
                                    <div id="cards-dropdown" class="jq-dropdown jq-dropdown-tip jq-dropdown-scroll">
                                        <ul class="jq-dropdown-menu" style="margin-top: 0px !important;">
                                            <li style="display: block; margin-right: 0px !important">
                                                <a href="{{ route('single-bank-debit-cards', ['local'=>App::getLocale(), 'slug'=>$bank->slug]) }}" class="d-flex align-items-center">
                                                    <div>
                                                        <img src="/temp/images/icons/debit-card.svg">
                                                    </div>
                                                    <div>
                                                        @lang('lang.debit_cards') <span>{{$debit_cards}}</span>
                                                    </div>
                                                </a>
                                            </li>
                                            <li style="display: block; margin-right: 0px !important">
                                                <a href="{{ route('single-bank-credit-cards', ['local'=>App::getLocale(), 'slug'=>$bank->slug]) }}" class="d-flex align-items-center">
                                                    <div>
                                                        <img src="/temp/images/icons/credit-card.svg">
                                                    </div>
                                                    <div>
                                                        @lang('lang.credit_cards') <span>{{$credit_cards}}</span>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </h1>
                                <h1 class="advantages__item mt-0" style="border: none;">
                                    <a href="{{ route('on-bank-exchange-rate',['locale' => App::getLocale()]) }}" >

                                        <div class="icon">
                                            <img src="/temp/images/icons/exchange.svg">
                                        </div>
                                        <span class="more">@lang('lang.currency_exchange_rate')</span>
                                    </a>
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="divider-ui--full"></div>

            <div class="section-gap-bank" id="about">
                <div class="medium-container">
                    <div class="info-bank__text">
                        <div class="row">
                            <div class="col-md-9 mb-15">
                                <div class="row">
                                    <div class="col-md-3"> 
                                        <h2>@lang('lang.about_bank')</h2>
                                    </div>
                                    <div class="col-md-9">
                                        <p style="margin: 0px !important; text-align: justify;">
                                            @if($bank->text())
                                                @php 
                                                    $sections =  BreakIntoPieces::bisectionTextPhrase($bank->text(),4); 
                                                @endphp
                                                @php
                                                    echo ($sections ['first']);
                                                @endphp
                                                <a id="more-text-open" ><span>@lang('lang.more')</span> <i class="fa fa-angle-down"></i></a>
                                                <span id="second-section" style="display: none">
                                                    @php
                                                        echo ($sections ['second']);
                                                    @endphp
                                                    <a  id="more-text-close" ><span>@lang('lang.close')</span> <i class="fa fa-angle-up"></i></a>
                                                </span>
                                            @else
                                                @lang('lang.not_found')
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" >
                                <div class="hots-line">
                                    <div class="h-l-title">@lang('lang.hots_line')</div>
                                    <div class="h-l-img">
                                        <a title="Banker">
                                            <span style=
                                                @if($bank->hotLineUserOne && $bank->hotLineUserOne->user->user_image)
                                                    "background-image: url(/{{ $bank->hotLineUserOne->user->user_image }})"
                                                @else 
                                                    "background-image: url(/temp/images/user.svg)"
                                                @endif>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="h-l-user-info pl-5 pr-5">
                                        @if($bank->hotLineUserOne)
                                            <a class="name" >{{ $bank->hotLineUserOne->user->getLocalName() }}</a>
                                            <a class="user-position" >{{ $bank->hotLineUserOne->user->organization->position() }}</a>
                                        @else
                                            <a class="name" >{{ $bank->information->name() }}</a>
                                        @endif
                                    </div>
                                    <div class="h-l-btn">
                                        <a href="{{ route('bank-give-question',['locale' => App::getLocale(),'slug' => $bank->slug]) }}">@lang('lang.questions_page.give_question')</a>
                                        <a href="{{ route('bank-list-answers',['locale' => App::getLocale(),'slug' => $bank->slug]) }}">@lang('lang.questions_page.see_answer')</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="divider-ui--full"></div>
            <div class="medium-container">
                <div class="section-gap-bank row" id="information">
                    <div class="col-md-9">
                        <div class="info-bank__text">
                            <h2>@lang('lang.bank_information')</h2>
                            <div class="row">
                                <div class="col-md-3"><strong>@lang('lang.address')</strong>
                                </div>
                                <div class="col-md-9">{{$bank->address()}}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><strong>@lang('lang.bank_chairman')</strong>
                                </div>
                                <div class="col-md-9">{{$bank->information->chairman()}}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><strong>@lang('lang.call_centre')</strong>
                                </div>
                                <div class="col-md-9">{{$bank->information->phone1}}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><strong>@lang('lang.helpline')</strong>
                                </div>
                                <div class="col-md-9">{{$bank->information->phone2}}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><strong>@lang('lang.email')</strong>
                                </div>
                                <div class="col-md-9">{{$bank->information->mail}}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><strong>@lang('lang.site')</strong>
                                </div>
                                <div class="col-md-9"><a href="{{$bank->information->site()}}" target="_blank">{{$bank->information->site()}}</a></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><strong>@lang('lang.telegram_channel')</strong>
                                </div>
                                <div class="col-md-9"><a href="{{$bank->information->telegram_link()}}" target="_blank">{{$bank->information->telegram_channel}}</a></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 d-flex align-items-center"><strong>@lang('lang.mobile_app')</strong>
                                </div>
                                <div class="col-md-9 d-flex align-items-center">
                                    @if($bank->information->mobile_app)
                                        <div style="padding-right: 10px;">{{ $bank->information->mobile_app}}</div>
                                        <div>
                                            <a href="{{$bank->information->app_store}}" class="mobile-app-link">
                                                <img src="/temp/images/icons/app-store.svg" alt="app-store-logo" class="mobile-app-logo">
                                            </a>
                                            <a href="{{$bank->information->play_market}}" class="mobile-app-link">
                                                <img src="/temp/images/icons/google-play.svg" alt="play-market-logo" class="mobile-app-logo">
                                            </a>
                                        </div>
                                    @else
                                        @lang('lang.mobile_app_not_found')
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <strong>@lang('lang.rekvizits')</strong>
                                </div>
                                <div class="col-md-9">
                                    <a class="rekvizit-drop-down">@lang('lang.detailed_info') <i class="fa fa-angle-down"></i></a>
                                </div>
                            </div>
                            <!-- rekvizits tab -->
                            <div id="rekvizits" style="display: none; padding: 15px;">
                                <div style="border: 1px solid #dedede; border-radius: 10px; padding: 10px;">
                                    <div class="row">
                                        <div class="col-md-3"><strong>@lang('lang.official_address')</strong></div>
                                        <div class="col-md-9">{{$bank->information->address()}}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3"><strong>@lang('lang.general_office')</strong></div>
                                        <div class="col-md-9">{{$bank->address()}}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3"><strong>@lang('lang.stir')</strong></div>
                                        <div class="col-md-9">{{$bank->information->inn}}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3"><strong>MFO</strong></div>
                                        <div class="col-md-9">{{$bank->information->mfo}}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3"><strong>SWIFT</strong></div>
                                        <div class="col-md-9">{{$bank->information->svift}}</div>
                                    </div>
                                    <div class="row" style="margin-bottom: 0px !important;">
                                        <div class="col-md-3"><strong>@lang('lang.licence')</strong></div>
                                        <div class="col-md-9">{{$bank->information->license}}</div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-md-3"><strong>@lang('lang.stockholders')</strong>
                                </div>
                                <div class="col-md-9">{{ $bank->information->stockholders()}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card-border mb-20">
                            <span class="d-block m-description" style="line-height: 17px; margin-bottom: 8px">@lang('lang.add_review_for_bank',['bank' => $bank->name()])</span>
                            <a href="{{ route('review.add',['locale' => App::getLocale()]) }}" class="btn-xn btn-xn-primary btn-xn-justify">
                                @lang('lang.v2.index_page.text_btn')
                            </a>
                        </div>
                    </div>
                </div>

                <div class="section-gap-bank">
                    <div class="info-bank__contact">
                        <div class="row">
                            <div class="col-lg-8 col-md-6">
                                <div id="map1">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d23290.788996114967!2d69.23680651858297!3d41.31631112612473!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2s!4v1577036607098!5m2!1sru!2s" width="600"
                                            height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 right">
                                <div>
                                    <div class="icon-t">@lang('lang.address')</div>
                                    <div class="address">
                                        {!! htmlspecialchars_decode($bank->address()) !!}
                                    </div>
                                </div>
                                <div>
                                    <div class="icon-t">@lang('lang.work_time')</div>
                                    <div class="working-time">
                                        <div class="day">@lang('lang.monday')</div>
                                        <div class="time">9:00 - 18:00</div>
                                    </div>
                                    <div class="working-time">
                                        <div class="day">@lang('lang.tuesday')</div>
                                        <div class="time">9:00 - 18:00</div>
                                    </div>
                                    <div class="working-time">
                                        <div class="day">@lang('lang.wednesday')</div>
                                        <div class="time">9:00 - 18:00</div>
                                    </div>
                                    <div class="working-time">
                                        <div class="day">@lang('lang.thursday')</div>
                                        <div class="time">9:00 - 18:00</div>
                                    </div>
                                    <div class="working-time">
                                        <div class="day">@lang('lang.friday')</div>
                                        <div class="time">9:00 - 18:00</div>
                                    </div>
                                    <div class="working-time">
                                        <div class="day">@lang('lang.saturday')</div>
                                        <div class="time">@lang('lang.holiday')</div>
                                    </div>
                                    <div class="working-time">
                                        <div class="day">@lang('lang.sunday')</div>
                                        <div class="time">@lang('lang.holiday')</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>

    $('.go-to-section').click(function(){
        $('html, body').animate({
            scrollTop: $($(this).attr('data-id')).offset().top-30
        }, 1500);
    });

</script>
<script src="https://api-maps.yandex.ru/2.1.74/?lang=ru_RU"></script>
<script>
    $(document).ready(function(){
        $('.rekvizit-drop-down').on('click',function(){
            $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
            $('#rekvizits').slideToggle(300);
        });

        $('#more-text-open').click(function(){
            $('#second-section').show();
            $(this).hide();
            $('#more-text-close').show();
        });
        $('#more-text-close').click(function(){
            $('#second-section').hide();
            $(this).hide();
            $('#more-text-open').show();
        });
    });
    function init(){
        let a=[41.294313,69.282831];
        window.myMap=new ymaps.Map("map2",{center:a,zoom:11}),
            myMap.behaviors.disable("scrollZoom"),
            myPlacemark1=new ymaps.Placemark(a,{},{
                overlayFactory:"default#interactiveGraphics",
                iconLayout:"default#imageWithContent",
                iconImageHref:"/temp/images/icons/mapMarker.png",
                iconImageSize:[32,44],
                iconImageOffset:[-16,-44]}),
            myPlacemark2=new ymaps.Placemark([41.312243,69.314321],{},{
                overlayFactory:"default#interactiveGraphics",
                iconLayout:"default#imageWithContent",
                iconImageHref:"/temp/images/icons/mapMarker.png",
                iconImageSize:[32,44],
                iconImageOffset:[-16,-44]}),
            myPlacemark3=new ymaps.Placemark([41.328704,69.250901],{},{
                overlayFactory:"default#interactiveGraphics",
                iconLayout:"default#imageWithContent",
                iconImageHref:"/temp/images/icons/mapMarker.png",
                iconImageSize:[32,44],
                iconImageOffset:[-16,-44]}),
            myMap.geoObjects.add(myPlacemark1).add(myPlacemark2).add(myPlacemark3)
        }
        ymaps.ready(init);
</script>
@endsection