@extends('layouts.app')
@section('meta')
     @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection

@section('css')
    <link href="/css/banks.css" type="text/css" rel="stylesheet" />
@endsection

@section('content')
      <div class="section">
        <div class="partners section-gap mt-10">
            <div class="medium-container">
                <div class="filter-result">
                    <div class="filter-result__title">
                        <h1 class="b-title mb-2" style="font-size: 20px !important;">@lang('lang.banks_of_uzb')</h1>
                    </div>
                    <div class="col-md-12 pl-0 pr-0">
                        <div class="form-group">
                            <input class="" id="search-input" name="search_bank" value="" placeholder="@lang('lang.search_bank')" data-url="{{ route('search-bank',['locale'=>App::getLocale()]) }}" data-method="GET">
                            <i class="fa fa-search btn-search search-banks"></i>
                        </div>
                    </div>
                    <div class="table filter-table row pt-25">
                        <div class="col-md-3 left-container mobile-show">
                            <div class="card-border mb-15">
                                <span class="d-block m-description" style="line-height: 17px; margin-bottom: 8px">@lang('lang.v2.index_page.text_right')</span>
                                <a href="{{ route('review.add',['locale' => App::getLocale()]) }}" class="btn-xn btn-xn-primary btn-xn-justify">
                                    @lang('lang.v2.index_page.text_btn')
                                </a>
                            </div>
                        </div>
                        <div class="col-md-9 right-container tbody bank-name card-border" id="result-search-bank">
                           @include('frontend.banks.ajax-banks-content')
                        </div>
                        <div class="col-md-3 left-container mobile-hidden">
                            <div class="card-border mb-15">
                                <span class="d-block m-description" style="line-height: 17px; margin-bottom: 8px">@lang('lang.v2.index_page.text_right')</span>
                                <a href="{{ route('review.add',['locale' => App::getLocale()]) }}" class="btn-xn btn-xn-primary btn-xn-justify">
                                    @lang('lang.v2.index_page.text_btn')
                                </a>
                            </div>
                            <div class="card-border advertisement-banner"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="/js/banks.js"></script>
@endsection