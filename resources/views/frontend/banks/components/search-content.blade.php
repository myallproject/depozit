<div class="bank-object">
    <div class="col-md-12 bank-row">
        @php
            $stars = $bank['bank']->returnStars();
        @endphp
        <div class="el-1">
            <div>
                <div class="big">
                    <a href="{{ route('info-bank',['locale'=>App::getLocale(),'slug'=>$bank['bank']->slug]) }}">
                        <img class="bank-img" src="/{{ $bank['bank']->image }}"/>
                    </a>
                </div>
                <div class="small rating-max mobile-hidden">
                    @php
                        echo $stars;
                    @endphp
                </div>
                <div class="mobile-show">
                    <div class="big mb-5 d-min">
                        <h1 class="color-a"><a href="{{ route('info-bank',['locale'=>App::getLocale(),'slug'=>$bank['bank']->slug]) }}">{{ $bank['bank']->name() }}</a></h1>
                    </div>
                    <div class="rating-max">
                        @php
                            echo $stars;
                        @endphp
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-row-divider"></div>
        <div class="el-2">
            <div>
                <div class="big mb-15 d-min">
                    <h1 class="color-a"><a href="{{ route('info-bank',['locale'=>App::getLocale(),'slug'=>$bank['bank']->slug]) }}">{{ $bank['bank']->name() }}</a></h1>
                </div>
                <div class="small">{{ $bank['bank']->information->licenseReplace() }}</div>
                <div class="small mt-5">@lang('lang.ownership'): {{ $bank['bank']->information->ownership->name() }}</div>
            </div>
        </div>
        <div class="el-3">
            <div>
                <div class="big d-flex">
                    <div class="div-icon">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="pl-10">
                        @php
                            echo str_replace(';','<br>',$bank['bank']->information->phone1)
                        @endphp
                    </div>
                </div>
                <div class="big d-flex">
                    <div class="div-icon">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="pl-10 color-a">
                        <a href="{{ $bank['bank']->information->site() }}" target="_blank">{{ $bank['bank']->information->site() }}</a>
                    </div>
                </div>
                <div class="big d-flex">
                    <div class="div-icon">
                        <i class="fa fa-telegram"></i>
                    </div>
                    <div class="pl-10">            
                        @if($bank['bank']->information->telegram_channel)
                            <a href="https://t.me/{{ substr($bank['bank']->information->telegram_channel,1)}}" target="_blank"> {{ $bank['bank']->information->telegram_channel }}</a>           
                        @else
                            --
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-row-divider"></div>
        <div class="el-4">
            <div>
                <div>
                    <a href="{{ route('bank-give-question',['locale' => App::getLocale(),'slug' => $bank['bank']->slug]) }}" class="btn btn--medium btn--blue btn-outline-dark-blue">
                        @lang('lang.questions_page.give_question')
                    </a>
                </div>
                <div>
                    <a href="#" class="btn btn--medium btn--blue btn--arrow">
                        @lang('lang.detailed_btn')
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="tr-more">
        @include('frontend.banks.components.detailed-tabs')
    </div>
</div>