<div class="content"> 
    <div class="tab-nav">
         <a href="#" class="active main-tab">@lang('lang.additional_info')</a> 
    </div>
    <div class="tab-caption">
        <div class="tab-pane active main-tab">
            <div class="big d-flex">
                <div class="div-icon">
                    <i class="fa fa-volume-control-phone"></i>
                </div>
                <div class="pl-10">
                    @if($bank['bank']->information->phone2)
                        {{ $bank['bank']->information->phone2 }}
                    @else
                        {{ $bank['bank']->information->phone1 }}
                    @endif
                </div>
            </div>
            <div class="big d-flex">
                <div class="div-icon">
                    <i class="fa fa-envelope "></i>
                </div>
                <div class="pl-10 color-a">
                    <a href="mailto:{{ $bank['bank']->information->mail }}">{{ $bank['bank']->information->mail }}</a>
                </div>
            </div>
            <div class="big d-flex ">
                <div class="div-icon font-20">
                    <i class="fa fa-map-marker"></i>
                </div>
                <div class="pl-10">
                    {{ $bank['bank']->information->address() }}
                </div>
            </div>
         </div>
     </div>
 </div>