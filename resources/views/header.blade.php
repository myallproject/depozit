@php
    $comparison = Comparison::count();
    $url = $comparison['url']
@endphp
<div class="header">
    <div class="medium-container">
        <div class="d-flex justify-content-between align-items-center flex-wrap">
            <div class="header__left">
                <a href="/" class="header__logo" title="@lang('lang.home')">
                    <img src="/temp/images/logo.svg" alt="Depozit.uz">
                </a>
                <div class="header__nav" id="navbar-toggle">
                    <ul>
                        <li>
                            <span>@lang('lang.menu')</span>
                            <div class="close-icon"></div>
                        </li>
                        <li>
                            <div class="header__search mobile-search">
                                <form action="{{ route('search',App::getLocale()) }}" method="get">
                                    <input type="text" name="variable" placeholder="@lang('lang.search_site')...">
                                    <button type="submit" class="search-icon search-icon--submit"></button>
                                    <div class="search-icon search-icon--toggle"></div>
                                </form>
                            </div>
                        </li>
                        <li>
                            <a href="{{ route('deposits',App::getLocale()) }}">@lang('lang.deposits')</a>
                        </li>
                        <li class="mobile-nav-hidden">
                            <span class="example" data-jq-dropdown="#jq-dropdown-1">
                                <a class="btn-open"> @lang('lang.credits') <i class="fa fa-angle-down"></i></a>
                            </span>
                            <div id="jq-dropdown-1" class="jq-dropdown jq-dropdown-tip jq-dropdown-scroll">
                                <ul class="jq-dropdown-menu">
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('credit-consumer',App::getLocale()) }}">@lang('lang.eat_credits')</a></li>
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('credit-auto',App::getLocale()) }}">@lang('lang.auto_credit')</a></li>
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('credit-education',App::getLocale()) }}">@lang('lang.education_credit')</a></li>
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('credit-microcredit',App::getLocale()) }}">@lang('lang.micro_debt')</a></li>
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('credit-mortgage',App::getLocale()) }}">@lang('lang.credit_mortgage')</a></li>
                                     <li style="display: block; margin-right: 0px !important"><a href="{{ route('credit-overdraft',App::getLocale()) }}">@lang('lang.overdraft_credit')</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="mobile-nav-show">
                            <span class="example">
                                <a class="open-mobile-dropdown"> @lang('lang.credits') <i class="fa fa-angle-down"></i></a>
                            </span>
                            <div style="display: none;" class="mobile-dropdown">
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('credit-consumer',App::getLocale()) }}">@lang('lang.eat_credits')</a></div>
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('credit-auto',App::getLocale()) }}">@lang('lang.auto_credit')</a></div>
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('credit-education',App::getLocale()) }}">@lang('lang.education_credit')</a></div>
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('credit-microcredit',App::getLocale()) }}">@lang('lang.micro_debt')</a></div>
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('credit-mortgage',App::getLocale()) }}">@lang('lang.credit_mortgage')</a></div>
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('credit-overdraft',App::getLocale()) }}">@lang('lang.overdraft_credit')</a></div>
                            </div>
                        </li>
                        <li class="mobile-nav-hidden">
                            <span class="example" data-jq-dropdown="#jq-dropdown-2">
                                <a class="btn-open">@lang('lang.cards') <i class="fa fa-angle-down"></i></a>
                            </span>
                            <div id="jq-dropdown-2" class="jq-dropdown jq-dropdown-tip jq-dropdown-scroll">
                                <ul class="jq-dropdown-menu">
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('debit-cards',App::getLocale()) }}">@lang('lang.debit_cards')</a></li>
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('credit-cards',App::getLocale()) }}">@lang('lang.credit_cards')</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="mobile-nav-show">
                            <span class="example">
                                <a class="open-mobile-dropdown">@lang('lang.cards') <i class="fa fa-angle-down"></i></a>
                            </span>
                            <div class="mobile-dropdown" style="display: none;">
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('debit-cards',App::getLocale()) }}">@lang('lang.debit_cards')</a></div>
                                
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('credit-cards',App::getLocale()) }}">@lang('lang.credit_cards')</a></div>
                            </div>
                        </li>
                        <li>
                            <a href="{{ route('on-bank-exchange-rate',['locale' => App::getLocale()]) }}">@lang('lang.currency_exchange_rate')</a>
                        </li>
                        <li class="mobile-nav-hidden">
                            <span class="example" data-jq-dropdown="#jq-dropdown-3">
                                <a class="btn-open">@lang('lang.partners') <i class="fa fa-angle-down"></i></a>
                            </span>
                            <div id="jq-dropdown-3" class="jq-dropdown jq-dropdown-tip jq-dropdown-scroll">
                                <ul class="jq-dropdown-menu">
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('partners-banks',App::getLocale()) }}">@lang('lang.banks')</a></li>
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('bank-question-list',['locale'=>App::getLocale()]) }}">@lang('lang.questions_page.question_answer')</a></li>
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('ranking-citizen',App::getLocale()) }}">@lang('lang.citizen_ranking')</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="mobile-nav-show">
                            <span class="example">
                                <a class="open-mobile-dropdown">@lang('lang.partners') <i class="fa fa-angle-down"></i></a>
                            </span>
                            <div class="mobile-dropdown" style="display: none;">
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('partners-banks',App::getLocale()) }}">@lang('lang.banks')</a></div>
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('bank-question-list',['locale'=>App::getLocale()]) }}">@lang('lang.questions_page.question_answer')</a></div>
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('ranking-citizen',App::getLocale()) }}">@lang('lang.citizen_ranking')</a></div>
                            </div>
                        </li>
                        <li class="mobile-nav-hidden" style="margin-right: 0px !important;">
                            <span class="example" data-jq-dropdown="#jq-dropdown-4">
                                <a class="btn-open pulse">@lang('lang.v2.review') <i class="fa fa-angle-down"></i></a>
                            </span>
                            <div id="jq-dropdown-4" class="jq-dropdown jq-dropdown-tip jq-dropdown-scroll">
                                <ul class="jq-dropdown-menu">
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('review.list', App::getLocale()) }}">@lang('lang.v2.review')</a></li>
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('bank-question-list',['locale'=>App::getLocale()]) }}">@lang('lang.questions_page.question_answer')</a></li>
                                    <li style="display: block; margin-right: 0px !important"><a href="{{ route('ranking-citizen',App::getLocale()) }}">@lang('lang.citizen_ranking')</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="mobile-nav-show">
                            <span class="example">
                                <a class="open-mobile-dropdown">@lang('lang.v2.review') <i class="fa fa-angle-down"></i></a>
                            </span>
                            <div class="mobile-dropdown" style="display: none;">
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('review.list', App::getLocale()) }}">@lang('lang.v2.review')</a></div>
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('bank-question-list',['locale'=>App::getLocale()]) }}">@lang('lang.questions_page.question_answer')</a></div>
                                <div style="display: block; margin-right: 0px !important"><a href="{{ route('ranking-citizen',App::getLocale()) }}">@lang('lang.citizen_ranking')</a></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="header__right">
                <div class="header__search">
                    <form action="{{ route('search',App::getLocale()) }}" method="get">
                        <input type="text" name="variable" placeholder="@lang('lang.search_site')...">
                        <button type="submit" class="search-icon search-icon--submit"></button>
                        <div class="search-icon search-icon--toggle"></div>
                    </form>
                </div>
                <div class="header__balance">
                    <a href="{{ route('comparison',App::getLocale()) }}">
                        <img src="/temp/images/icons/balance.svg" alt="">
                        <sup style=" top:-15px;" class="comparison-amount text-danger">{{ $comparison['count'] }}</sup>
                    </a>
                </div>
                <div class="header-profile ml-10">
                    @if(\Illuminate\Support\Facades\Auth::check() && Auth::user()->role_id != 11)
                        <div class="header-profile__avatar">
                            <img src="/temp/images/icons/profile.svg" alt="">
                        </div>
                        <div class="header-profile__dropdown">
                            <div class="wrap">
                                <a href="{{ route('profile.reviews', App::getLocale()) }}">@lang('lang.v2.my_activity')</a>
                                <a href="{{ route('profile.index', App::getLocale()) }}">@lang('lang.v2.my_page')</a>
                                <div class="hl-divider"></div>
                                <a href="#"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                   class="profile-logout">
                                    @lang('lang.logout')
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    @elseif(\Illuminate\Support\Facades\Auth::check() && Auth::user()->role_id == 11 )
                        <div class="header-profile__avatar">
                            <img src="/temp/images/icons/profile.svg" alt="">
                        </div>
                        <div class="header-profile__dropdown">
                            <div class="wrap">                                
                                <a href="{{ route('bank-profile', ['local'=>App::getLocale()]) }}">@lang('lang.v2.bank_profile.prolfile')</a>
                                <a href="{{ route('review.bp_list', ['local'=>App::getLocale(), 'id' => Auth::user()->organization->child_org_id ]) }}">@lang('lang.v2.bpreview_bank')</a>
                                <a href="{{ route('profile-question-answer',['local'=>App::getLocale()]) }}">@lang('lang.v2.bank_profile.question_answer')</a>
                                <a href="{{ route('bank-orders-deposits',['local'=>App::getLocale()]) }}">@lang('lang.v2.order.orders')</a>
                                <div class="hl-divider"></div>
                                <a href="#"
                                onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();"
                                class="profile-logout">
                                    @lang('lang.logout')
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    @else
                        <a href="{{ route('profile.login', App::getLocale()) }}" class="header-profile__avatar">
                            <img src="/temp/images/icons/profile.svg" alt="">
                        </a>
                    @endif
                </div>
                <div class="header__lang lang-change ml-10">
                    <?php $parametr = Route::current()->parameters(); ?>
                    @if(App::getLocale() == 'ru')
                        <?php $newparametr = array_merge ($parametr,['locale' => 'uz']); ?>
                        <a href="{{ route(Route::currentRouteName(), $newparametr) }}" class="lang_current" >
                            {{-- <img src="/temp/images/icons/uzb-flag.svg"  style="border: 1px solid;border-radius: 5px;"alt="UZ"> --}} <span>UZ</span>
                        </a>
                    @elseif(App::getLocale() == 'uz')
                        <?php $newparametr = array_merge($parametr,['locale'=>'ru']); ?>
                        <a href="{{ route(Route::currentRouteName(), $newparametr) }}"  class="lang_current" >
                            {{-- <img src="/temp/images/icons/rus-flag.svg" style="border: 1px solid;border-radius: 5px;" alt="RU"> --}} <span>RU</span>
                        </a>
                    @endif
                </div>
                <div class="menu-toggle ml-10"></div>
            </div>
        </div>
    </div>
</div>