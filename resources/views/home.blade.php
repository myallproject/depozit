@extends('layouts.app')

@section('meta')
    @foreach($meta_tags as $meta)
        @if(!strpos($meta->tag,'|'))
           <{{ $meta->tag }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach />
        @else
           <{{ $meta->tagName()['open'] }} @foreach($meta->attribute() as $attr) {{ $attr->attribute }} ="{{ $attr->metaData(App::getLocale()) }}" @endforeach >{{ $meta->text(App::getLocale()) }}</{{ $meta->tagName()['close'] }}>
        @endif
    @endforeach
@endsection

@section('content')
<link rel="stylesheet" type="text/css" href="/css/news-section.css">
<style>
    .advantages__grid > a > span {
        font-size: 15px !important;
    }
    .b-title{
        color:#3aa5d0;
    }
    @media (max-width: 768px){
        .min-banner__img#goal{
            background-image:url(/temp/images/banner/digital_tree_mobile.webp);
        }
        .min-banner__img#review{
            background-image:url(/temp/images/banner/review_banner_mobile.webp);
        }
    }
    @media (min-width: 769px){
        .min-banner__img#goal{
            background-image:url(/temp/images/banner/digital_tree_lg.webp);
        }
        .min-banner__img#review{
            background-image:url(/temp/images/banner/review_banner_lg.webp);
        }
    } 
</style>
 <div class="section">
        <div class="min-banner">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="min-banner__img" id="review">
                            <div class="medium-container text-uppercase">
                                <div class="col-sm-8 head-banner">
                                    <h2 class="slide2_text" style="color: #3aa5d0; max-width:100% !important;">@lang('lang.review_banner')</h2>
                                    <div class="banner-footer text-lowercase">
                                        <a href="{{ route('review.add',['locale' => App::getLocale()]) }}" class="btn-xn btn-xn-secondary btn-xn-justify" style="max-width: 200px">
                                            @lang('lang.v2.index_page.text_btn')
                                        </a>
                                        {{-- <a href="" class="btn-banner-social-link" style="color: #3aa5d0;" target="_blank"></a> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="min-banner__img" id="goal">
                            <div class="medium-container text-uppercase">
                                <div class="col-md-8 head-banner">
                                    <h2 class="slide2_text">@lang('lang.slide2_text')</h2>
                                    <div class="banner-footer text-lowercase">
                                        <a href="https://t.me/DepozitUzb" class="btn-banner-social-link" target="_blank"><i class="fa fa-paper-plane fa-lg"></i><span>t.me/depozituzb</span></a>
                                        <a href="https://www.facebook.com/depozit.uz/" class="btn-banner-social-link" target="_blank"><i class="fa fa-facebook-official fa-lg"></i><span>@depozit.uz</span></a>
                                        <a href="https://www.instagram.com/depozit.uz/" class="btn-banner-social-link" target="_blank"><i class="fa fa-instagram fa-lg"></i><span>@depozit.uz</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="min-banner__img" style="background-image:url(/temp/images/banner/banner-covid.webp)">
                            <div class="medium-container  text-uppercase ">
                                <div class="col-md-8 head-banner covid">
                                    <span>@lang('lang.stay_home')</span>
                                    <h2>@lang('lang.covid_banner_text')</h2>
                                    <div class="banner-footer">
                                        <a href="#" class="btn-banner" data-id="#online_services">@lang('lang.detailed')</a>
                                        <img src="/temp/images/logo-white.svg" width="100px" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="min-banner__wrapper">
                    <div class="medium-container">
                        {{-- <div class="swiper-pagination"></div> --}}
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <a href="https://cbu.uz/" target="_blank" title="Ўзбекистон Республикаси Марказий банки"><img src="https://cbu.uz/oz/informer/?txtclr=212121&brdclr=3aa5d0&bgclr=3aa5d0&r_choose=USD_EUR_RUB_GBP" alt=""></a> --}} 

        <div class="advantages section-gap--small">
            <div class="medium-container">
                <div class="advantages__grid" >
                    <h1 class="advantages__item mt-0">
                        <a href="{{ route('deposits',App::getLocale()) }}" >
                            <div class="icon">
                                <img src="/temp/images/icons/advantages/1.svg">
                            </div>
                            <span>@lang('lang.home_header.deposit')</span>
                        </a>
                    </h1>
                    <h1 class="advantages__item mt-0">
                    <a href="{{ route('credit-microcredit',App::getLocale()) }}">
                        <div class="icon">
                            <img src="/temp/images/icons/advantages/2.svg">
                        </div><span>@lang('lang.home_header.micro_credit')</span>
                    </a>
                     </h1>
                    <h1 class="advantages__item mt-0">
                    <a href="{{ route('credit-consumer',App::getLocale()) }}" >
                        <div class="icon">
                            <img src="/temp/images/icons/advantages/3.svg">
                        </div><span>@lang('lang.home_header.consumer_credit')</span>
                    </a>
                     </h1>
                    <h1 class="advantages__item mt-0">
                    <a href="{{ route('credit-auto',App::getLocale()) }}" >
                        <div class="icon">
                            <img src="/temp/images/icons/advantages/4.svg">
                        </div><span>@lang('lang.home_header.avto_credit')</span>
                    </a>
                     </h1>
                    <h1 class="advantages__item mt-0">
                    <a href="{{ route('credit-mortgage',App::getLocale()) }}" >
                        <div class="icon">
                            <img src="/temp/images/icons/advantages/5.svg">
                        </div><span>@lang('lang.home_header.mortgage_credit')</span>
                    </a>
                     </h1>
                    <h1 class="advantages__item mt-0">
                    <a href="{{ route('credit-education',App::getLocale()) }}" >
                        <div class="icon">
                            <img src="/temp/images/icons/advantages/6.svg">
                        </div><span>@lang('lang.home_header.edu_credit')</span>
                    </a>
                     </h1>
                </div>
            </div>
        </div>

            <div class="opportunities section-gap mt-50 mb-70">
                <div class="medium-container">
                    <h2 class="b-title">@lang('lang.great_chance')</h2>

                    <div class="tab-nav tab-nav--large tab-nav--center" style="text-transform: uppercase !important;">
                        <a href="#" class="text-center">@lang('lang.deposits')</a>
                        <a href="#" class="text-center">USD @lang('lang.deposits')</a>
                        <a href="#" class="text-center">@lang('lang.micro_debt')</a>
                        <a href="#" class="text-center">@lang('lang.eat_credits')</a>
                        <a href="#" class="text-center">@lang('lang.auto_credit')</a>
                        <a href="#" class="text-center">@lang('lang.credit_mortgage')</a>
                        <a href="#" class="text-center">@lang('lang.education_credit')</a>
                        <a href="#" class="text-center">@lang('lang.debit_cards')</a>
                    </div>

                    <div class="opportunities__grid tab-caption">
                        <div class="tab-pane">
                            <div class="row">
                               @foreach($deposits as $deposit)
                                <div class="col-md-4">
                                    <div class="card-1">
                                        <div class="card-1__head">
                                            <div>
                                                <div class="title">{{ $deposit->bank->name_uz }}</div>
                                                <div class="tariff">"{{ $deposit->name_uz }}"</div>
                                            </div>
                                            <div class="img">
                                                <img src="/{{ $deposit->bank->image }}" alt="Bank Image">
                                            </div>
                                        </div>
                                        <div class="card-1__section">
                                            <div class="info"><span>{{ $deposit->deposit_percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                            </div>
                                            <div class="info"><span>
                                                @if(!$deposit->deposit_date_string)
                                                    {{ $deposit->deposit_date." ".__('lang.day') }}
                                                @else
                                                    {{ $deposit->dateStringLang(App::getLocale()) }}
                                                @endif
                                                </span>  <span>@lang('lang.deposit_date')</span>
                                            </div>
                                        </div><a href="{{ route('view-deposit',['locale'=>App::getLocale(),'slug'=>$deposit->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                    </div>
                                </div>
                               @endforeach
                            </div>
                        </div>

                         <div class="tab-pane">
                            <div class="row">
                               @foreach($deposits_usd as $deposit)
                                <div class="col-md-4">
                                    <div class="card-1">
                                        <div class="card-1__head">
                                            <div>
                                                <div class="title">{{ $deposit->bank->name_uz }}</div>
                                                <div class="tariff">"{{ $deposit->name_uz }}"</div>
                                            </div>
                                            <div class="img">
                                                <img src="/{{ $deposit->bank->image }}" alt="Bank Image">
                                            </div>
                                        </div>
                                        <div class="card-1__section">
                                            <div class="info"><span>{{ $deposit->deposit_percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                            </div>
                                            <div class="info"><span>
                                                @if(!$deposit->deposit_date_string)
                                                    {{ $deposit->deposit_date." ".__('lang.day') }}
                                                @else
                                                    {{ $deposit->dateStringLang(App::getLocale()) }}
                                                @endif
                                                </span>  <span>@lang('lang.deposit_date')</span>
                                            </div>
                                        </div><a href="{{ route('view-deposit',['locale'=>App::getLocale(),'slug'=>$deposit->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                    </div>
                                </div>
                               @endforeach
                            </div>
                        </div>

                        <div class="tab-pane">
                            <div class="row">
                                @foreach($credits_micro as $micro)
                                    <div class="col-md-4">
                                        <div class="card-1">
                                            <div class="card-1__head">
                                                <div>
                                                    <div class="title">{{ $micro->bank->name_uz }}</div>
                                                    <div class="tariff">"{{ $micro->name_uz }}"</div>
                                                </div>
                                                <div class="img">
                                                    <img src="/{{ $micro->bank->image }}" alt="Bank Image">
                                                </div>
                                            </div>
                                            <div class="card-1__section">
                                                <div class="info"><span>{{ $micro->percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                                </div>
                                                <div class="info"><span>{{ $micro->date_credit." ".$micro->creditDate->name() }}</span>  <span>@lang('lang.credit_date')</span>
                                                </div>
                                            </div><a href="{{ route('view-credit',['locale' => App::getLocale(),'slug'=>$micro->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="tab-pane">
                            <div class="row">
                                @foreach($credits_istemol as $istemol)
                                    <div class="col-md-4">
                                        <div class="card-1">
                                            <div class="card-1__head">
                                                <div>
                                                    <div class="title">{{ $istemol->bank->name_uz }}</div>
                                                    <div class="tariff">"{{ $istemol->name_uz }}"</div>
                                                </div>
                                                <div class="img">
                                                    <img src="/{{ $istemol->bank->image }}" alt="Bank Image">
                                                </div>
                                            </div>
                                            <div class="card-1__section">
                                                <div class="info"><span>{{ $istemol->percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                                </div>
                                                <div class="info"><span>{{ $istemol->date_credit." ".$istemol->creditDate->name() }}</span>  <span>@lang('lang.credit_date')</span>
                                                </div>
                                            </div>
                                                <a href="{{ route('view-credit',['locale' => App::getLocale(),'slug'=>$istemol->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="tab-pane">
                            <div class="row">
                                @foreach($credits_auto as $auto)
                                    <div class="col-md-4">
                                        <div class="card-1">
                                            <div class="card-1__head">
                                                <div>
                                                    <div class="title">{{ $auto->bank->name_uz }}</div>
                                                    <div class="tariff">"{{ $auto->name_uz }}"</div>
                                                </div>
                                                <div class="img">
                                                    <img src="/{{ $auto->bank->image }}" alt="Bank Image">
                                                </div>
                                            </div>
                                            <div class="card-1__section">
                                                <div class="info"><span>{{ $auto->percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                                </div>
                                                <div class="info"><span>{{ $auto->date_credit." ".$auto->creditDate->name() }}</span>  <span>@lang('lang.credit_date')</span>
                                                </div>
                                            </div><a href="{{ route('view-credit',['locale' => App::getLocale(),'slug'=>$auto->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="tab-pane">
                            <div class="row">
                                @foreach($credits_ipoteka as $ipoteka)
                                    <div class="col-md-4">
                                        <div class="card-1">
                                            <div class="card-1__head">
                                                <div>
                                                    <div class="title">{{ $ipoteka->bank->name_uz }}</div>
                                                    <div class="tariff">"{{ $ipoteka->name_uz }}"</div>
                                                </div>
                                                <div class="img">
                                                    <img src="/{{ $ipoteka->bank->image }}" alt="Bank Image">
                                                </div>
                                            </div>
                                            <div class="card-1__section">
                                                <div class="info"><span>{{ $ipoteka->percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                                </div>
                                                <div class="info"><span>{{ $ipoteka->date_credit." ".$ipoteka->creditDate->name() }}</span>  <span>@lang('lang.credit_date')</span>
                                                </div>
                                            </div><a href="{{ route('view-credit',['locale' => App::getLocale(),'slug'=>$ipoteka->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="tab-pane">
                            <div class="row">
                                @foreach($credits_talim as $talim)
                                    <div class="col-md-4">
                                        <div class="card-1">
                                            <div class="card-1__head">
                                                <div>
                                                    <div class="title">{{ $talim->bank->name_uz }}</div>
                                                    <div class="tariff">"{{ $talim->name_uz }}"</div>
                                                </div>
                                                <div class="img">
                                                    <img src="/{{ $talim->bank->image }}" alt="Bank Image">
                                                </div>
                                            </div>
                                            <div class="card-1__section">
                                                <div class="info"><span>{{ $talim->percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                                </div>
                                                <div class="info"><span>{{ $talim->date_credit." ".$talim->creditDate->name() }}</span>  <span>@lang('lang.credit_date')</span>
                                                </div>
                                            </div><a href="{{ route('view-credit',['locale' => App::getLocale(),'slug'=>$talim->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="tab-pane">
                            <div class="row">
                                @foreach($debet_cards as $debet_card)
                                    <div class="col-md-4">
                                        <div class="card-1">
                                            <div class="card-1__head">
                                                <div>
                                                    <div class="title">{{ $debet_card->bank->name_uz }}</div>
                                                    <div class="tariff">"{{ $debet_card->name_uz }}"</div>
                                                </div>
                                                <div class="img" style="width: 140px;">
                                                    @if($debet_card->image)
                                                        <img src="/{{ $debet_card->image }}" style="width: auto; height:auto" alt="Card Image">
                                                    @else
                                                        <img src="/{{ $debet_card->bank->image }}" alt="Bank Image">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="card-1__section">
                                                <div class="info"><span>{{ $debet_card->tuluv_komissia }}</span>  <span>@lang('lang.payment_commission')</span>
                                                </div>
                                                <div class="info"><span>{{ $debet_card->price }}</span>  <span>@lang('lang.card_price')</span>
                                                </div>
                                            </div><a href="{{ route('debit-cards',App::getLocale()) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="online opportunities section-gap mt-50 mb-70" id="online_services">
                <div class="medium-container">
                    <h2 class="b-title">@lang('lang.great_chance_online')</h2>

                    <div class="tab-nav tab-nav--large tab-nav--center" style="text-transform: uppercase !important;">
                        @if (count($online_deposits)>0)
                        <a href="#" class="text-center">@lang('lang.deposits')</a>
                        @endif

                        @if (count($online_deposits_usd)>0)
                        <a href="#" class="text-center">USD @lang('lang.deposits')</a>
                        @endif

                        @if (count($online_credits_micro)>0)
                        <a href="#" class="text-center">@lang('lang.micro_debt')</a>
                        @endif

                        @if (count($online_credits_istemol)>0)
                        <a href="#" class="text-center">@lang('lang.eat_credits')</a>
                        @endif

                        @if (count($online_credits_auto)>0)
                        <a href="#" class="text-center">@lang('lang.auto_credit')</a>
                        @endif

                        @if (count($online_credits_ipoteka)>0)
                        <a href="#" class="text-center">@lang('lang.credit_mortgage')</a>
                        @endif

                        @if (count($online_credits_talim)>0)
                        <a href="#" class="text-center">@lang('lang.education_credit')</a>
                        @endif

                        @if (count($delivered_debet_cards)>0)
                        <a href="#" class="text-center">@lang('lang.debit_cards')</a>
                        @endif
                    </div>

                    <div class="opportunities__grid tab-caption">
                        @if (count($online_deposits)>0)
                        <div class="tab-pane">
                            <div class="row">
                               @foreach($online_deposits as $deposit)
                                <div class="col-md-4">
                                    <div class="card-1">
                                        <div class="card-1__head">
                                            <div>
                                                <div class="title">{{ $deposit->bank->name_uz }}</div>
                                                <div class="tariff">"{{ $deposit->name_uz }}"</div>
                                            </div>
                                            <div class="img">
                                                <img src="/{{ $deposit->bank->image }}" alt="Bank Image">
                                            </div>
                                        </div>
                                        <div class="card-1__section">
                                            <div class="info"><span>{{ $deposit->deposit_percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                            </div>
                                            <div class="info"><span>
                                                @if(!$deposit->deposit_date_string)
                                                    {{ $deposit->deposit_date." ".__('lang.day') }}
                                                @else
                                                    {{ $deposit->dateStringLang(App::getLocale()) }}
                                                @endif
                                                </span>  <span>@lang('lang.deposit_date')</span>
                                            </div>
                                        </div><a href="{{ route('view-deposit',['locale'=>App::getLocale(),'slug'=>$deposit->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                    </div>
                                </div>
                               @endforeach
                            </div>
                        </div>
                        @endif

                        @if (count($online_deposits_usd)>0)
                        <div class="tab-pane">
                            <div class="row">
                               @foreach($online_deposits_usd as $deposit)
                                <div class="col-md-4">
                                    <div class="card-1">
                                        <div class="card-1__head">
                                            <div>
                                                <div class="title">{{ $deposit->bank->name_uz }}</div>
                                                <div class="tariff">"{{ $deposit->name_uz }}"</div>
                                            </div>
                                            <div class="img">
                                                <img src="/{{ $deposit->bank->image }}" alt="Bank Image">
                                            </div>
                                        </div>
                                        <div class="card-1__section">
                                            <div class="info"><span>{{ $deposit->deposit_percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                            </div>
                                            <div class="info"><span>
                                                @if(!$deposit->deposit_date_string)
                                                    {{ $deposit->deposit_date." ".__('lang.day') }}
                                                @else
                                                    {{ $deposit->dateStringLang(App::getLocale()) }}
                                                @endif
                                                </span>  <span>@lang('lang.deposit_date')</span>
                                            </div>
                                        </div><a href="{{ route('view-deposit',['locale'=>App::getLocale(),'slug'=>$deposit->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                    </div>
                                </div>
                               @endforeach
                            </div>
                        </div>
                        @endif

                        @if (count($online_credits_micro)>0)
                         <div class="tab-pane">
                            <div class="row">
                                @foreach($online_credits_micro as $micro)
                                    <div class="col-md-4">
                                        <div class="card-1">
                                            <div class="card-1__head">
                                                <div>
                                                    <div class="title">{{ $micro->bank->name_uz }}</div>
                                                    <div class="tariff">"{{ $micro->name_uz }}"</div>
                                                </div>
                                                <div class="img">
                                                    <img src="/{{ $micro->bank->image }}" alt="Bank Image">
                                                </div>
                                            </div>
                                            <div class="card-1__section">
                                                <div class="info"><span>{{ $micro->percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                                </div>
                                                <div class="info"><span>{{ $micro->date_credit." ".$micro->creditDate->name() }}</span>  <span>@lang('lang.credit_date')</span>
                                                </div>
                                            </div><a href="{{ route('view-credit',['locale' => App::getLocale(),'slug'=>$micro->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                         @endif

                        @if (count($online_credits_istemol)>0)
                        <div class="tab-pane">
                            <div class="row">
                                @foreach($online_credits_istemol as $istemol)
                                    <div class="col-md-4">
                                        <div class="card-1">
                                            <div class="card-1__head">
                                                <div>
                                                    <div class="title">{{ $istemol->bank->name_uz }}</div>
                                                    <div class="tariff">"{{ $istemol->name_uz }}"</div>
                                                </div>
                                                <div class="img">
                                                    <img src="/{{ $istemol->bank->image }}" alt="Bank Image">
                                                </div>
                                            </div>
                                            <div class="card-1__section">
                                                <div class="info"><span>{{ $istemol->percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                                </div>
                                                <div class="info"><span>{{ $istemol->date_credit." ".$istemol->creditDate->name() }}</span>  <span>@lang('lang.credit_date')</span>
                                                </div>
                                            </div>
                                                <a href="{{ route('view-credit',['locale' => App::getLocale(),'slug'=>$istemol->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @endif

                        @if (count($online_credits_auto)>0)
                        <div class="tab-pane">
                            <div class="row">
                                @foreach($online_credits_auto as $auto)
                                    <div class="col-md-4">
                                        <div class="card-1">
                                            <div class="card-1__head">
                                                <div>
                                                    <div class="title">{{ $auto->bank->name_uz }}</div>
                                                    <div class="tariff">"{{ $auto->name_uz }}"</div>
                                                </div>
                                                <div class="img">
                                                    <img src="/{{ $auto->bank->image }}" alt="Bank Image">
                                                </div>
                                            </div>
                                            <div class="card-1__section">
                                                <div class="info"><span>{{ $auto->percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                                </div>
                                                <div class="info"><span>{{ $auto->date_credit." ".$auto->creditDate->name() }}</span>  <span>@lang('lang.credit_date')</span>
                                                </div>
                                            </div><a href="{{ route('view-credit',['locale' => App::getLocale(),'slug'=>$auto->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @endif

                        @if (count($online_credits_ipoteka)>0)
                        <div class="tab-pane">
                            <div class="row">
                                @foreach($online_credits_ipoteka as $ipoteka)
                                    <div class="col-md-4">
                                        <div class="card-1">
                                            <div class="card-1__head">
                                                <div>
                                                    <div class="title">{{ $ipoteka->bank->name_uz }}</div>
                                                    <div class="tariff">"{{ $ipoteka->name_uz }}"</div>
                                                </div>
                                                <div class="img">
                                                    <img src="/{{ $ipoteka->bank->image }}" alt="Bank Image">
                                                </div>
                                            </div>
                                            <div class="card-1__section">
                                                <div class="info"><span>{{ $ipoteka->percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                                </div>
                                                <div class="info"><span>{{ $ipoteka->date_credit." ".$ipoteka->creditDate->name() }}</span>  <span>@lang('lang.credit_date')</span>
                                                </div>
                                            </div><a href="{{ route('view-credit',['locale' => App::getLocale(),'slug'=>$ipoteka->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @endif

                        @if (count($online_credits_talim)>0)
                        <div class="tab-pane">
                            <div class="row">
                                @foreach($online_credits_talim as $talim)
                                    <div class="col-md-4">
                                        <div class="card-1">
                                            <div class="card-1__head">
                                                <div>
                                                    <div class="title">{{ $talim->bank->name_uz }}</div>
                                                    <div class="tariff">"{{ $talim->name_uz }}"</div>
                                                </div>
                                                <div class="img">
                                                    <img src="/{{ $talim->bank->image }}" alt="Bank Image">
                                                </div>
                                            </div>
                                            <div class="card-1__section">
                                                <div class="info"><span>{{ $talim->percent }}%</span>  <span>@lang('lang.year_percent')</span>
                                                </div>
                                                <div class="info"><span>{{ $talim->date_credit." ".$talim->creditDate->name() }}</span>  <span>@lang('lang.credit_date')</span>
                                                </div>
                                            </div><a href="{{ route('view-credit',['locale' => App::getLocale(),'slug'=>$talim->slug]) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @endif

                        @if (count($delivered_debet_cards))
                        <div class="tab-pane">
                            <div class="row">
                                @foreach($delivered_debet_cards as $debet_card)
                                    <div class="col-md-4">
                                        <div class="card-1">
                                            <div class="card-1__head">
                                                <div>
                                                    <div class="title">{{ $debet_card->bank->name_uz }}</div>
                                                    <div class="tariff">"{{ $debet_card->name_uz }}"</div>
                                                </div>
                                                <div class="img" style="width: 140px;">
                                                    @if($debet_card->image)
                                                        <img src="/{{ $debet_card->image }}" style="width: auto; height:auto" alt="Card Image">
                                                    @else
                                                        <img src="/{{ $debet_card->bank->image }}" alt="Bank Image">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="card-1__section">
                                                <div class="info"><span>{{ $debet_card->tuluv_komissia }}</span>  <span>@lang('lang.payment_commission')</span>
                                                </div>
                                                <div class="info"><span>{{ $debet_card->price }}</span>  <span>@lang('lang.card_price')</span>
                                                </div>
                                            </div><a href="{{ route('debit-cards',App::getLocale()) }}" class="btn btn--small btn--blue btn--arrow">@lang('lang.detailed')</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
            </div>

        <div class="courses section-gap--small">
            <div class="medium-container">
                <h1 class="b-title"><a href='{{ route('on-bank-exchange-rate',['locale' => App::getLocale()]) }}'>@lang('lang.foreign_exchange_rate')</a></h1>
                {{--<span class="divider" style="margin: 15px auto 15px;"></span>
                <div class="col-md-12 text-right">
                    <div class="" style="display:-webkit-inline-box;  font-size: 15px; color: #3aa5d0">
                        <div style="margin-right: 8px">
                            @php
                                $time='';
                                if(isset($best_rate_update_time['data_update_time'])){
                                    $time = $best_rate_update_time['data_update_time'];
                                }
                                if(!$time){
                                    $time = date('Y-m-d H:m');
                                }
                             @endphp 
                            <span >{{ date('d/M/Y H:i',strtotime($time)) }}</span><br>
                            <span >{{ DateMonthName::updateDateTime($time) }}</span><br>
                            <span >@lang('lang.update_time')</span>
                        </div>
                        <img  src="/temp/images/update_time.png" width="32" />
                    </div>

                </div>--}}
                <br>
                @widget("currency")
            </div>
        </div>
            

           @include('home-page-blocks.news-section')

            <div class="partners section-gap mt-50">
                <div class="medium-container">
                    <h1 class="b-title">@lang('lang.our_partner')</h1>
                    <div class="tab-nav tab-nav--center mb-5" style="text-transform: uppercase">
                        <a href="#" >@lang('lang.banks')</a>
                        <a href="#">@lang('lang.other_organization')</a>
                    </div>
                    <div class="tab-caption">

                        <div class="tab-pane">
                            {{--<div class="m-link text-right" style="text-transform: uppercase">
                                <a href="#">@lang('lang.count_bank',['count'=>count($banks)])</a>
                            </div>--}}
                            <div class="partners__grid">
                                @foreach($banks as $bank)
                                    <a href="{{ route('info-bank',['locale' => App::getLocale(),'slug'=>$bank->slug]) }}" class="partners__item">
                                        <img src="/{{ $bank->image }}" alt="{{ $bank->name_uz }}">
                                    </a>
                                @endforeach
                            </div>
                        </div>

                        <div class="tab-pane">
                           {{--  <div class="m-link text-right" style="text-transform: uppercase"><a href="#">@lang('lang.count_insurance',['count'=>123])</a>
                            </div> --}}
                            <div class="partners__grid">
                              @foreach($organizations as $org)
                                    <a href="{{ route('info-bank',['locale' => App::getLocale(),'slug'=>$org->slug]) }}" class="partners__item">
                                        <img src="/{{ $org->image }}" alt="{{ $org->name_uz }}">
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="why section-gap">
                <div class="medium-container">
                    <h2 class="b-title">@lang('lang.why_choose')?</h2>
                    <div class="arrow-card">
                        <div class="icon">
                            <img src="/temp/images/icons/steps/1.svg">
                        </div>
                        <div class="content">
                            <div class="num">01</div>
                            <div class="text">
                                <h3>@lang('lang.real')</h3>
                                <p>@lang('lang.text_real')</p>
                            </div>
                        </div>
                    </div>
                    <div class="arrow-card__divider"></div>
                    <div class="arrow-card arrow-card--reverse">
                        <div class="icon">
                            <img src="/temp/images/icons/steps/2.svg">
                        </div>
                        <div class="content">
                            <div class="num">02</div>
                            <div class="text">
                                <h3>@lang('lang.economical')</h3>
                                <p>@lang('lang.economical_text')</p>
                            </div>
                        </div>
                    </div>
                    <div class="arrow-card__divider"></div>
                    <div class="arrow-card">
                        <div class="icon">
                            <img src="/temp/images/icons/steps/3.svg">
                        </div>
                        <div class="content">
                            <div class="num">03</div>
                            <div class="text">
                                <h3>@lang('lang.reliable')</h3>
                                <p>@lang('lang.reliable_text')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          {{--  <div class="sections-list section-gap">
                <div class="medium-container">
                    <h2 class="b-title">NIMA QIDIRISHMOQDA</h2>
                    <div class="sections-list__grid">
                        <div class="sections-list__item">
                            <ul>
                                <li class="main"><a href="#">КРЕДИТЫ</a>
                                </li>
                                <li><a href="#">Без справок о доходах</a>
                                </li>
                                <li><a href="#">В день обращения</a>
                                </li>
                                <li><a href="#">Самые выгодные</a>
                                </li>
                                <li><a href="#">Наличными</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sections-list__item">
                            <ul>
                                <li class="main"><a href="#">ВКЛАДЫ</a>
                                </li>
                                <li><a href="#">Мультивалютные</a>
                                </li>
                                <li><a href="#">Самые выгодные</a>
                                </li>
                                <li><a href="#">С пополнением</a>
                                </li>
                                <li><a href="#">Пенсионные</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sections-list__item">
                            <ul>
                                <li class="main"><a href="#">ИПОТЕКА</a>
                                </li>
                                <li><a href="#">Без первоначального взноса</a>
                                </li>
                                <li><a href="#">Для молодой семьи</a>
                                </li>
                                <li><a href="#">Государственная</a>
                                </li>
                                <li><a href="#">Самая выгодная</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sections-list__item">
                            <ul>
                                <li class="main"><a href="#">КРЕДИТНЫЕ КАРТЫ</a>
                                </li>
                                <li><a href="#">Без справок о доходах</a>
                                </li>
                                <li><a href="#">С льготным периодом</a>
                                </li>
                                <li><a href="#">Для снятия наличных</a>
                                </li>
                                <li><a href="#">Для студентов</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sections-list__item">
                            <ul>
                                <li class="main"><a href="#">КРЕДИТНЫЕ КАРТЫ</a>
                                </li>
                                <li><a href="#">Без справок о доходах</a>
                                </li>
                                <li><a href="#">С льготным периодом</a>
                                </li>
                                <li><a href="#">Для снятия наличных</a>
                                </li>
                                <li><a href="#">Для студентов</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sections-list__item">
                            <ul>
                                <li class="main"><a href="#">КРЕДИТНЫЕ КАРТЫ</a>
                                </li>
                                <li><a href="#">Без справок о доходах</a>
                                </li>
                                <li><a href="#">С льготным периодом</a>
                                </li>
                                <li><a href="#">Для снятия наличных</a>
                                </li>
                                <li><a href="#">Для студентов</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sections-list__item">
                            <ul>
                                <li class="main"><a href="#">КУРСЫ ВАЛЮТ</a>
                                </li>
                                <li><a href="#">Доллара</a>
                                </li>
                                <li><a href="#">Евро</a>
                                </li>
                                <li><a href="#">Доллара по ЦБ РФ</a>
                                </li>
                                <li><a href="#">Для студентов</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sections-list__item">
                            <ul>
                                <li class="main"><a href="#">ПРОДУКТЫ В БАНКАХ</a>
                                </li>
                                <li><a href="#">Вклады в банках</a>
                                </li>
                                <li><a href="#">Кредиты в банках</a>
                                </li>
                                <li><a href="#">Ипотека в банках</a>
                                </li>
                                <li><a href="#">Кредитные карты в банках</a>
                                </li>
                                <li><a href="#">Дебетовые карты в банках</a>
                                </li>
                                <li><a href="#">Автокредиты в банках</a>
                                </li>
                                <li><a href="#">Займы в МФО</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>--}}

</div>

@endsection