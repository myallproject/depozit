<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta name="google-site-verification" content="_DYAFwoVF6n6mxZ-VFKoDne6oUd67JuItQAAbsgOJzA">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="index, follow" />
    <meta name="yandex-verification" content="c58747e3a4c7cb0f" />
	
	{{--<meta name="description" content="Depozit.uz - bu moliyaviy supermarket. Biz murakkab moliya olamida to‘g‘ri qarorlar qabul qilishga yordam beramiz!"/>--}}

    <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1">

    @yield('meta')
	
    @yield('title')

    <meta property="og:title" content="Depozit.uz - Moliyaviy supermarket" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="https://depozit.uz/temp/images/dp-logo.png" />
    <meta property="og:url" content="https://depozit.uz/" />
    <meta property="og:description" content="Depozit.uz - bu moliyaviy supermarket. Biz murakkab moliya olamida to‘g‘ri qarorlar qabul qilishga yordam beramiz!" />
    <meta property="og:site_name" content="Depozit.uz" />
    
    <meta property="twitter:image" content="https://depozit.uz/temp/images/dp-logo.png" />

    <link rel="shortcut icon" href="/temp/images/logo.png" type="image/x-icon">
    <link rel="icon" href="/temp/images/logo.png" type="image/x-icon">

    <!-- Gilroy Font -->
    <link rel="stylesheet" href="https://cdn.rawgit.com/mfd/09b70eb47474836f25a21660282ce0fd/raw/e06a670afcb2b861ed2ac4a1ef752d062ef6b46b/Gilroy.css">
    <!-- End -->
    <link rel="stylesheet" href="/temp/styles/vendor.css">
    <link rel="stylesheet" href="/temp/styles/main.css">
	<!--xn-css-start-->
    <link rel="stylesheet" href="/temp/styles/xn.css">
    <!--xn-css-end-->
    <link rel="stylesheet" href="/css/new-class.css">
    <link rel="stylesheet" href="/css/loading.css">
    <link rel="stylesheet" href="/css/tooltip.css">
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link type="text/css" rel="stylesheet" href="/plugins/dropdown/jquery.dropdown.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

   
    @yield('css')
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TB94Q7Z');</script>
    <!-- End Google Tag Manager -->

<!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
    
        ym(67665238, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/67665238" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
 <!-- /Yandex.Metrika counter -->
 
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TB94Q7Z"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<style>
.card-1__section .info span:first-child {
    font-size: 20px;
}
.new-td{
    padding-bottom: 8px;
    padding-top: 8px;
}
</style>
<span type="hidden" id="open-content"  data-status="false" data-content-id=""> </span>
     <div class="page-wrapper">

        @include('header')

        {{-- <div class="text-center">
            <span style="font-size: 20px; color: darkred !important;"> {{ Yt::trans('Saytimiz test rejimida ishlayapti','uz') }}! </span>
        </div> --}}

        @yield('content')

        @include('footer')

        {{-- @include('frontend.components.page-loader')--}}

     </div>
     <span hidden="hidden" class="any-text" data-text="@lang('lang.any')"></span>
     <script src="/temp/scripts/jquery.js"></script>
     <script src="/temp/scripts/vendor.js"></script>
     <script src="/temp/scripts/main.js"></script>
     <!--xn-css-start-->
	 <script src="/temp/scripts/xn.js"></script>
	 <!--xn-css-end-->
	 <script src="/js/jquery-ui.js"></script> 
	 <script type="text/javascript" src="/plugins/dropdown/jquery.dropdown.min.js"></script>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.3.4/gsap.min.js"></script>
     <script src="{{ asset('js/share.js') }}"></script>
     
      @yield('script')

      @include('alert')

     <script>
        
        $('.btn-open').on('click',function(){
            $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
        });

        $('.btn-banner').click(function(){
            $('html, body').animate({
                scrollTop: $($(this).attr('data-id')).offset().top-30
            }, 1500);
        });

        $(document).ready(function(){
            $('.open-mobile-dropdown').on('click',function(){
                $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
                // $(this).closest('.mobile-dropdown').slideToggle(300);
                $(this).parent().next().slideToggle(300);
            });
        });

        $('.clear-filter').on('click',function(){
            //$('.modal-ui__close-btn').click();

            $('.clear').val('').trigger('change');
            $('.select2-selection__placeholder').text('@lang("lang.any")');
            $('.select2-search__field').attr('placeholder','@lang("lang.any")');
            $('.select2-selection--multiple').find('.select2-search__field').attr('placeholder','@lang("lang.any")');

            $('#f-el-currency').val(1).trigger('change');
            $('#f-el-currency-large').val(1).trigger('change');

            $('#dp-percent-period').select2({
                closeOnSelect : false,
                placeholder: $('#dp-percent-period').attr('data-empty')
            });
            $('#f-el-dp-percents_capitalization').select2({
                placeholder: $('#f-el-dp-percents_capitalization').attr('data-empty')
            });
            $('#large-filter-provision').select2({
                closeOnSelect : false,
                placeholder: $('#large-filter-provision').attr('data-empty')
                
            });

           
        });
        //$(".select2__js").select2({minimumResultsForSearch: Infinity});
        
       
        $('.select2-search').find('input').attr('disabled','disabled');
        let parent_span =  $('.select2-selection').attr('aria-owns');
        
        $('.select2-container').click(function(){
             if($(this).find('.select2-selection--multiple').attr('aria-owns') === 'select2-select-banks-results'){
                $(this).find('.select2-search').find('input').removeAttr('disabled');
            }

            if($('.select2-search').find('input[aria-controls]').attr('aria-controls') === 'select2-region-results'){
                $('.select2-search').addClass('display-inherit');
            }
        });
        
        $('.btn-filter-bank').on('click',function(){
            let bank_type_input = $('#bank_type_input');
            if($(this).attr('data-status') == 0){
                if($(bank_type_input).attr('data-btn-type-id')){
                    $('#'+$(bank_type_input).attr('data-btn-type-id')).removeClass('btn-filter-bank-bg').attr('data-status',0);
                    $(bank_type_input).attr('data-btn-type-id',$(this).attr('id')).val($(this).attr('data-bank-type'));
                } else {
                    $(bank_type_input).attr('data-btn-type-id',$(this).attr('id')).val($(this).attr('data-bank-type'));
                }
                $(this).attr('data-status',1);
                $(this).addClass('btn-filter-bank-bg');
            } else if ($(this).attr('data-status') == 1) {
                $(this).attr('data-status',0);
                $(this).removeClass('btn-filter-bank-bg');
                $(bank_type_input).attr('data-btn-type-id','').val('');
            }
        });
        /*setTimeout(function(){
            $('.page-loader').hide();aria-controls
        },5000);*/
     </script>
</body>
</html>