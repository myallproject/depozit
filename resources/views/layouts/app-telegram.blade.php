<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta name="google-site-verification" content="_DYAFwoVF6n6mxZ-VFKoDne6oUd67JuItQAAbsgOJzA">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="index, follow" />
    
    @yield('metaKeyWords')

    <meta name="description" content="Depozit.uz - bu moliyaviy supermarket. Biz murakkab moliya olamida to‘g‘ri qarorlar qabul qilishga yordam beramiz!"/>

    <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1">


    <title>{{ config('app.name') }} | @yield('title')</title>

    <meta property="og:title" content="Depozit.uz - Moliyaviy supermarket" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://depozit.uz/" />
    <meta property="og:image" content="https://depozit.uz/temp/images/dp-uz.png" />
    <meta property="og:site_name" content="Depozit.uz" />
    <meta property="og:og:description" content="Depozit.uz - bu moliyaviy supermarket. Biz murakkab moliya olamida to‘g‘ri qarorlar qabul qilishga yordam beramiz!" />

    <link rel="shortcut icon" href="/temp/images/logo.png" type="image/x-icon">
    <link rel="icon" href="/temp/images/logo.png" type="image/x-icon">

    <link rel="stylesheet" href="/temp/styles/vendor.css">
    <link rel="stylesheet" href="/temp/styles/main.css">
    <link rel="stylesheet" href="/css/new-class.css">
    <link rel="stylesheet" href="/css/loading.css">
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link type="text/css" rel="stylesheet" href="/plugins/dropdown/jquery.dropdown.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158920235-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-158920235-1');
    </script>
    @yield('css')
</head>

<style>
.card-1__section .info span:first-child {
    font-size: 20px;
}
</style>
<span type="hidden" id="open-content"  data-status="false" data-content-id=""> </span>
     <div class="page-wrapper">
        
         @yield('content')

      

        {{-- @include('frontend.components.page-loader')--}}

     </div>
     <span hidden="hidden" class="any-text" data-text="@lang('lang.any')"></span>
     <script src="/temp/scripts/jquery.js"></script>
     <script src="/temp/scripts/vendor.js"></script>
     <script src="/temp/scripts/main.js"></script>
     <script src="/js/jquery-ui.js"></script>
     <script type="text/javascript" src="/plugins/dropdown/jquery.dropdown.min.js"></script>

      @yield('script')

     <script>
         /*$('.btn-open').on('click',function(){
             $(this).find('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
         });*/


        $('.clear-filter').on('click',function(){
            //$('.modal-ui__close-btn').click();

            $('.clear').val('').trigger('change');
            $('.select2-selection__placeholder').text('@lang("lang.any")');
            $('.select2-search__field').attr('placeholder','@lang("lang.any")');
            $('.select2-selection--multiple').find('.select2-search__field').attr('placeholder','@lang("lang.any")');

            $('#f-el-currency').val(1).trigger('change');
            $('#f-el-currency-large').val(1).trigger('change');

            $('#dp-percent-period').select2({
                closeOnSelect : false,
                placeholder: $('#dp-percent-period').attr('data-empty')
            });
            $('#f-el-dp-percents_capitalization').select2({
                placeholder: $('#f-el-dp-percents_capitalization').attr('data-empty')
            });
            $('#large-filter-provision').select2({
                closeOnSelect : false,
                placeholder: $('#large-filter-provision').attr('data-empty')
            });
        });
       /* setTimeout(function(){
            $('.page-loader').hide();
        },5000);*/
     </script>
</body>
</html>