<div class="section-gap mt-50 mb-70">
	<div class="medium-container pr-0 pl-0">
		<h1 class="b-title mb-40"><a class=" hover-title" href="{{ route('news-all-page',App::getLocale()) }}">@lang('lang.news')</a></h1>
		<div class="news-container">
			<div class="col-md-12 mr-0 ml-0 pr-0 pl-0 row news-items">
				@if(count($news) > 0)
				@foreach($news as $new)
					<div class="col-md-3 mb-30">
						<div class="hover-box-shadow">
						<div class="news-img"><img src="/{{ $new->image }}" alt=""></div>
						<div class="news-title p-8"><a href="{{ route('news-page',['locale'=>App::getLocale(),'slug'=>$new->slug,'id'=>$new->id]) }}">{{ $new->title() }}</a></div>
						</div>
					</div>
				@endforeach
				@else
					<div class="col-md-12 mb-30 text-center">
						@lang('lang.not_found')
					</div>
				@endif
			</div>
		</div>
	</div>
</div>