<div id="great-deposits" class="col-md-12 pr-0 pl-0">
  @foreach($deposits as $dp_row )
    <div class="col-md-4 col-md-12  pb-3">
        <div class="card-body col-md-12 p-3 pr-4 pl-4 bg-white">
            <div class="col-mb-12 border-bottom pb-4 mb-2 pt-2" style="display: flex">
                <div class=" pr-0 pt-3 pl-0" style="width: 70%">
                    <h4><b>{{ $dp_row->bank->name_uz }}</b></h4>
                    <h5>"{{$dp_row->name_uz}}"</h5>
                </div>
                <div class=" pr-0 pl-0 text-right" style="width: 30%">
                    <img src="/{{ $dp_row->bank->image }}" width="70">
                </div>
            </div>

            <div class="col-md-12 pr-0 pl-0 pt-2 mb-4" style="display: flex">
                <div class=" pr-0 pt-3 pl-0" style="width: 60%">
                    <h2><b>{{ $dp_row->deposit_percent }}%</b></h2>
                    <h5>@lang('lang.year_percent')</h5>
                </div>
                <div class=" pr-0 pt-3 pl-0" style="width: 40%">
                    <h2><b>{{ $dp_row->deposit_date }}</b></h2>
                    <h5>@lang('lang.deposit_date')</h5>
                </div>
            </div>
            <div class="col-md-12 pr-0 pl-0 pt-2 mb-4" >
                <a class="btn btn-default">@lang('lang.detailed')</a>
            </div>
        </div>
    </div>
@endforeach

</div>