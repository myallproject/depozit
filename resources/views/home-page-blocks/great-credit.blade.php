<div id="great-credits" style="display: none" class="col-md-12 pr-0 pl-0">
    @foreach($credits as $cr_row)
    <div class="col-md-4 col-md-12  pb-3">
        <div class="card-body col-md-12 p-3 pr-4 pl-4 bg-white">
            <div class="col-mb-12 border-bottom pb-4 mb-2 pt-2" style="display: flex">
                <div class=" pr-0 pt-3 pl-0" style="width: 70%">
                    <h4><b>{{ $cr_row->bank->name_uz }}</b></h4>
                    <h5>"{{ $cr_row->name_uz }}"</h5>
                </div>
                <div class=" pr-0 pl-0 text-right" style="width: 30%">
                    <img src="/{{ $cr_row->bank->image }}" width="70">
                </div>
            </div>

            <div class="col-md-12 pr-0 pl-0 pt-2 mb-4" style="display: flex">
                <div class=" pr-0 pt-3 pl-0" style="width: 60%">
                    <h2><b>{{ $cr_row->percent }}%</b></h2>
                    <h5>@lang('lang.year_percent')</h5>
                </div>
                <div class=" pr-0 pt-3 pl-0" style="width: 40%">
                    <h2><b>{{ $cr_row->date_credit }}</b></h2>
                    <h5>@lang('lang.credit_date')</h5>
                </div>
            </div>
            <div class="col-md-12 pr-0 pl-0 pt-2 mb-4" >
                <a class="btn btn-default">@lang('lang.detailed')</a>
            </div>
        </div>
    </div>
@endforeach

</div>