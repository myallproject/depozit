<div class="alert">
    @if(count($errors) > 0)
        @foreach($errors->all() as $error)
            <div class="alert-item alert--warning">
                <div class="alert-icon"></div>
                <div class="alert-close"></div>
                <div class="alert-caption">
                    <div class="alert-message">
                        {!! $error !!}
                    </div>
                </div>
            </div>
        @endforeach
    @endif

    @if (session()->has('error'))
        <div class="alert-item alert--warning">
            <div class="alert-icon"></div>
            <div class="alert-close"></div>
            <div class="alert-caption">
                <div class="alert-message">
                    {!! session()->get('error') !!}
                </div>
            </div>
        </div>
    @endif

    @if (session()->has('success'))
        <div class="alert-item alert--success">
            <div class="alert-icon"></div>
            <div class="alert-close"></div>
            <div class="alert-caption">
                <div class="alert-message">
                    {!! session()->get('success') !!}
                </div>
            </div>
        </div>
    @endif
</div>

<script>

    function callAlert() {
        let alertItem = $('.alert-item');
        if (alertItem.length > 0) {
            let tl = new TimelineMax({onReverseComplete:clearItems});
            tl.to($('.alert'), 0.3, {
                autoAlpha: 1, opacity: 1
            });
            tl.staggerTo(alertItem, 0.5, {
                x: 0,
                stagger: .05
            });
            $('.alert-item').click(function () {
                tl.reverse();
                clearTimeout(hideAlert);
            });
            function clearItems() {
                alertItem.remove();
            }
            const hideAlert = setTimeout(function () {
                tl.reverse();
            }, 5000)
        }
    }

    // call notification
    $(document).ready(function () {
        // remove if view from browser cache
        if(performance.navigation.type === 2){
            $('.alert-item').remove();
        }
        callAlert();
    });

    'use script';

    class cAlert {
        constructor(container) {
            this.container = container;
            this.container.find('.alert-item').remove();
        }
        warning(message) {
            this.container.append('<div class="alert-item alert--warning">\n' +
                '                <div class="alert-icon"></div>\n' +
                '                <div class="alert-close"></div>\n' +
                '                <div class="alert-caption">\n' +
                '                    <div class="alert-message">\n' +
                '                        '+message+'\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>')
        }

        success(message) {
            this.container.append('<div class="alert-item alert--success">\n' +
                '                <div class="alert-icon"></div>\n' +
                '                <div class="alert-close"></div>\n' +
                '                <div class="alert-caption">\n' +
                '                    <div class="alert-message">\n' +
                '                        '+message+'\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>')
        }

        init() {
            callAlert();
        }
    }

</script>
