<style>
#error_{
    display:none;
}
</style>
<div class="footer">
    <img src="/temp/images/logo-sm-white.svg" class="footer__decor footer__decor--left">
    <img src="/temp/images/logo-sm-white.svg" class="footer__decor footer__decor--right">
    <div class="medium-container">
        <div class="footer__top">
            <div class="column">
                <a href="/" class="logo">
                    <img src="/temp/images/logo-white.svg" alt="Depozit">
                </a>
                <div class="title text-uppercase">
                    <a href="{{ route('about-us',[app::getLocale()]) }}">@lang('lang.about_project')</a>
                </div>
                <div>@lang('lang.footer_text')</div>
                {{-- <div style="@if(Auth::check() && Auth::user()->role_id == 10) @else display:none; @endif">
                     <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id={{ config('app.counter_auth_id') }}'></script>
                    <script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/{{ config('app.counter_id') }}/t/1"></script>
                </div> --}}
            </div>
            <div class="column">
                <div class="title">@lang('lang.others')</div>
                <ul>
                    <li>
                        <a href="{{ route('contact-us',[app::getLocale()]) }}">@lang('lang.contact')</a>
                    </li>
                    <li>
                        <a href='{{ route('service-page',[app::getLocale()]) }}'>@lang('lang.services')</a>
                    </li>
                    <li>
                        <a href="{{ route('faq',[app::getLocale()]) }}">FAQ</a>
                    </li>
                    <li>
                        <a href="{{ route('search',[app::getLocale()]) }}">@lang('lang.search')</a>
                    </li>

                </ul>
            </div>
          <div class="column">
                <div class="title text-center">@lang('lang.our_office_address')</div>
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7553.053639662165!2d69.27012917302886!3d41.29947998791795!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38ae8adbef8fffff%3A0xd2b92cd442230500!2s12b%20Afrosiab%20Street%2C%20Tashkent%2C%20Uzbekistan!5e0!3m2!1sen!2s!4v1599842245010!5m2!1sen!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
            <div class="column">
                <div class="title">@lang('lang.contacts')</div>
                <ul>
                    <li>@lang('lang.address'): @lang('lang.our_address')</li>
                    <li>@lang('lang.phone'): +998 99 088 69 80</li>
                    <li>@lang('lang.email'): info@depozit.uz</li>
                </ul>
                <div class="socials">
                    <div class="title">@lang('lang.our_social_set'):</div>
                    <div class="d-flex flex-wrap">
                        <a target="_blank" href="https://www.instagram.com/depozit.uz/">
                            <img src="/temp/images/icons/instagram.svg">
                        </a>
                        <a target="_blank" href="https://www.facebook.com/depozit.uz/">
                            <img src="/temp/images/icons/facebook.svg">
                        </a>
                        <a target="_blank" href="https://t.me/DepozitUzb">
                            <img src="/temp/images/icons/telegram.svg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="copyright">© Depozit.uz - 2020 | @lang('lang.all_rights_reserved').</div>
            <div class="menu">
                <a href="{{ route('home',[app::getLocale()]) }}">@lang('lang.home')</a>
                <a href="{{ route('about-us',[app::getLocale()]) }}">@lang('lang.about_us')</a>
                {{--<a href="{{ route('comments') }}">Komentariya</a>--}}
            </div>
        </div>
    </div>
</div> <!-- end footer!-->
