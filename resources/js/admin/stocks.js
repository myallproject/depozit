$(document).ready(function() {
    $("#dataTable").DataTable({
        paging: true,
        lengthChange: true,
        searching: true,
        ordering: true,
        info: false,
        autoWidth: true,
        dom:
            '<"float-left"f><"mr-5 ml-5 d-unset"><"float-right"l>rt<"bottom"i><"d-block"p>'
    });

    $(".delete").on("click", () => {
        if (confirm("Siz ma'lumotni o'chirmoqchisiz!")) {
            return true;
        } else {
            return false;
        }
    });
});
