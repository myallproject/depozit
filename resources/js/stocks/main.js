import $ from "jquery";
import "datatables.net";
import "datatables.net-bs4";
import swal from "sweetalert";

require("bootstrap");

const messages = {
    ru: {
        success: "Успех",
        message: "Вы успешно отправили запрос",
    },
    uz: {
        success: "Muafaqiyat",
        message: "Siz murojatni jonatingiz",
    },
};

$(document).ready(() => {
    const modal = $("#requestModal");
    const app_locale = $('meta[name="localization"]').attr("content");

    const translate = (key) => {
        return messages[app_locale][key];
    };

    modal.on("hidden.bs.modal", function (e) {
        $(this).find(".form-control").val("").removeClass("is-invalid");
    });

    $('select[name="branch"]').on("change", () => {
        $("form#branch").submit();
    });

    $("#datatable").DataTable({
        pageLength: 20,
        language: {
            url: "/js/datatables/" + app_locale + ".json",
        },
    });

    $(".buy-button").click((e) => {
        modal.find('input[name="stock_id"]').val($(e.target).data("id"));
        modal.modal();
    });

    $("form#request_stock").on("submit", (e) => {
        e.preventDefault();
        const data = $(e.target).serialize();

        var isLoading = false;
        if (!isLoading) {
            $.ajax({
                url: $('meta[name="request_url"]').attr("content"),
                method: "post",
                data,
                beforeSend: () => {
                    isLoading = true;
                },
                error: (errors) => {
                    // console.log(errors);
                    $.each(errors.responseJSON.errors, (key, val) => {
                        $(`input[name="${key}"]`)
                            .addClass("is-invalid")
                            .siblings(".invalid-feedback")
                            .html("")
                            .append(val);
                    });
                },
                success: () => {
                    isLoading = false;
                    console.log(data);
                    modal.modal("hide");
                    swal({
                        title: translate("success"),
                        text: translate("message"),
                        icon: "success",
                    });
                },
            });
        }
    });
});
