<?php
/*Test Routes*/
Route::prefix('tests')->middleware('auth')->group(function () {

	Route::get('/deposit', 'Test\TestController@deposits')->name('test-deposits');

	Route::get('/deposit-data-ru-trasnlate-page', 'Admin\DepositsController@translatePage')->name('depsit-column-translate-page');

	Route::get('/deposit-data-ru-trasnlate', 'Admin\DepositsController@dataTranslate')->name('depsit-column-translate');

	Route::get('/deposit-data-ru', 'Admin\DepositsController@dataLang')->name('deposit-add-json-to-column');

	Route::get('/putget', 'Test\TestController@putget')->name('test-putget');

	Route::put('/put', 'Test\TestController@put')->name('test-put');

});
?>