<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/psb/rate','Admin\DashboardController@psbRateApi')->name('api-psb-rate');

Route::get('/update-rate-code','Admin\DashboardController@rateCurrencyName')->name('update-rate-code');


/* body: action = take or sale,  code = USD  or ...*/
Route::post('exchange-rates/json','Api\CurrencyApiController@getCurrency');