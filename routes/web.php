<?php

use App\Models\Bank;
use App\Notifications\ReviewNotification;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Spatie\Sitemap\SitemapGenerator;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/', function () {
	$lang = App::getLocale();
	$default = 'uz';
	if ($lang) {
		$default = $lang;
	}
	return redirect()->route('home', $default);
});

Route::get('/sitemap', function () {
	SitemapGenerator::create(route('home', 'uz'))->writeToFile('sitemap.xml');
});

Route::get('/test-page', 'HomeController@testPage')->middleware(['auth', 'admin'])->name('test-page');
Route::get('/sendToChannel', 'HomeController@sendToChannel')->name('sendtotg');

Route::prefix('cache')->middleware(['auth', 'admin'])->group(function () {
	Route::get('/clear', function () {Artisan::call('cache:clear');return 'Cache cleared!';});
	Route::get('/config', function () {Artisan::call('config:cache');return 'Config cached!';});
	Route::get('/view', function () {Artisan::call('view:clear');return 'Views cleared!';});
	Route::get('/page', function () {Artisan::call('page-cache:clear');return 'Pages cleared!';});
});

Route::prefix('cron')->middleware(['auth', 'admin'])->group(function () {
	Route::get('/cb-rate', function () {Artisan::call('cron:rate');return back()->with(['msg' => 'Successfully']);})->name('cron-rate');
	Route::get('/api-rate', function () {Artisan::call('rate:api');return back()->with(['msg' => 'Successfully']);})->name('rate-api');
});

/* Route::prefix('locale')->middleware('localization')->group(function () {
Route::get('/{locale}', function (string $locale) {return redirect()->back();});
 */

Route::get('visitorJson', 'HomeController@visitorJson')->middleware('visitor')->name('visitor-json');
Auth::routes();

Route::get('/site-notifications', 'HomeController@notifications')->name('site-notifications');
Route::get('/del-notification/{id}', 'HomeController@delNotification')->name('del-notification');
Route::get('/check-notification/{id}', 'HomeController@checkNotification')->name('check-notification');

Route::get('/download-communication-rules/{lang}', 'HomeController@downloadCommunicationRules')->name('download-communication-rules');

Route::group(['prefix' => '{locale}', 'where' => ['locale' => '[a-zA-Z]{2}'], 'middleware' => 'localization'], function () {
	if (in_array(Request::segment(1), Config::get('app.available_locales'))) {
		Session::put('locale', Request::segment(1));
	}

	Route::get('/stocks', 'Frontend\StockController@index')->name('stock.index');
	Route::post('/stocks/request/send', 'Frontend\StockController@submit')->name('stocks.send');

	/*Route::get('change-lang/{route}',function(Request $request){
		           return redirect()->route($request::segment(3), App::getLocale());
	*/

	Route::get('/select-region-banks', 'HomeController@selectRegionBanks')->name('select-region-banks');
	Route::get('/select-bank-branches', 'HomeController@selectBankBranches')->name('select-bank-branches');

	Route::get('/', 'HomeController@index')->middleware('visitor')->name('home');

	//Route::prefix(Config::get('pages.urls.'.Session::get('locale').'.deposits'))->namespace('Frontend')->group(function() {
	Route::prefix('deposits')->namespace('Frontend')->group(function () {
		Route::get('/', 'DepositsController@deposits')->name('deposits');
		Route::get('/filter', 'DepositsController@filter')->name('filter-deposit');
		Route::get('/view/{slug}', 'DepositsController@viewDeposit')->name('view-deposit');
		Route::get('/large-search', 'DepositsController@largeSearchDeposits')->name('large-search-deposit');
		Route::get('/calculation', 'DepositsController@calculationDeposit')->name('calculation-deposit-percent');
		Route::get('/filter-other-dp', 'DepositsController@otherDepositList')->name('filter-other-dp');
		Route::get('/select-currency', 'DepositsController@selectCurrency')->name('deposit-select-currency');
		Route::get('/view/{slug}/#order-deposit', 'DepositsController@viewDeposit')->name('go-to-order-deposit');
	});

	Route::get('/exchange-rate-cb', 'HomeController@exchangeRate')->name('cb-exchange-rate');

	Route::get('/exchange-rate-bank', 'HomeController@bankExchangeRate')->name('on-bank-exchange-rate');

	Route::get('/contact-us', 'HomeController@contactUs')->name('contact-us');

	Route::get('/about-us', 'HomeController@aboutUs')->name('about-us');

	Route::get('/comments', 'HomeController@comments')->name('comments');

	Route::get('/more/comments', 'HomeController@moreComment')->name('more-comment');

	Route::get('/comparison', 'HomeController@listComparison')->name('comparison');

	Route::get('/news', 'HomeController@news')->name('news-page');

	Route::get('/news/all', 'HomeController@newsAll')->name('news-all-page');
	Route::get('/covid-19', 'HomeController@banksServicesTable')->name('banks-services-table');

	Route::get('/rate-for-telegram/{currency}', 'HomeController@rateForTelegram')->middleware(['auth', 'admin'])->name('rate-for-telegram');

	Route::get('/best-rate-for-telegram/', 'HomeController@bestTelegram')->middleware(['auth', 'admin'])->name('best-rate-for-telegram');

	Route::get('/services', function () {
		return abort(404);
	})->name('service-page');

	Route::get('/faq', function () {
		return abort(404);
	})->name('faq');

	Route::get('/search', 'SearchController@search')->name('search');

	//Route::get('/make_zero','Frontend\DepositsController@removeNull')->name('make-zero-deposit');

	Route::prefix('partners')->group(function () {
		Route::get('/banks', 'HomeController@banks')->name('partners-banks');
		Route::get('/banks/{slug}', 'HomeController@infoBank')->name('info-bank');
		Route::get('/search-banks', 'Frontend\BanksController@search')->name('search-bank');
	});
	Route::group(['namespace' => 'Frontend', 'prefix' => '/ranking'], function () {
		Route::get('/citizen', 'RankingController@citizen')->name('ranking-citizen');
		Route::get('/citizen-ajax', 'RankingController@citizenFilter')->name('ranking-citizen-filter');
	});

	// Deposits of one bank
	Route::get('/deposits/{slug}', 'Frontend\DepositsController@depositsOfSingleBank')->name('single-bank-deposits');
	// Credits of one bank
	Route::prefix('/credits')->group(function () {
		Route::get('/consumer/{slug}', 'Frontend\CreditsController@consumerCreditsOfBank')->name('single-bank-credits-consumer');
		Route::get('/auto/{slug}', 'Frontend\CreditsController@autoCreditsOfBank')->name('single-bank-credits-auto');
		Route::get('/education/{slug}', 'Frontend\CreditsController@educationCreditsOfBank')->name('single-bank-credits-education');
		Route::get('/microcredit/{slug}', 'Frontend\CreditsController@microCreditsOfBank')->name('single-bank-credits-microcredit');
		Route::get('/mortgage/{slug}', 'Frontend\CreditsController@mortgageCreditsOfBank')->name('single-bank-credits-mortgage');
		Route::get('/overdraft/{slug}', 'Frontend\CreditsController@overdraftCreditsOfBank')->name('single-bank-credits-overdraft');
	});
	// Cards of one bank
	Route::get('/debit-cards/{slug}', 'Frontend\CardsController@debitCardsOfSingleBank')->name('single-bank-debit-cards');
	Route::get('/credit-cards/{slug}', 'Frontend\CardsController@creditCardsOfSingleBank')->name('single-bank-credit-cards');

	//Route::get('/search-page','SearchController@searchPage')->name('search-page');

	Route::prefix('credits')->namespace('Frontend')->group(function () {
		Route::get('/', 'CreditsController@typeCredit')->name('frontend-credits-type');

		//Route::get('/{slug}','CreditsController@credits')->name('credits');

		/**
		 * other credit filter
		 **/
		Route::get('other-auto-credit', 'CreditsController@otherAutoCredit')->name('auto-credit-other-list');
		Route::get('other-consumer-credit', 'CreditsController@otherConsumerCredit')->name('consumer-credit-other-list');
		Route::get('other-education-credit', 'CreditsController@otherEducationCredit')->name('education-credit-other-list');
		Route::get('other-micro-credit', 'CreditsController@otherMicroCredit')->name('micro-credit-other-list');
		Route::get('other-mortgage-credit', 'CreditsController@otherMortgageCredit')->name('mortgage-credit-other-list');
		Route::get('other-overdraft-credit', 'CreditsController@otherOverdraftCredit')->name('overdraft-credit-other-list');

		//Small Filters
		Route::get('/filter-consumer', 'CreditsController@filterConsumer')->name('filter-consumer-credits');
		Route::get('/filter-auto', 'CreditsController@filterAuto')->name('filter-auto-credits');
		Route::get('/filter-education', 'CreditsController@filterEducation')->name('filter-education-credits');
		Route::get('/filter-micro', 'CreditsController@filterMicro')->name('filter-micro-credits');
		Route::get('/filter-mortgage', 'CreditsController@filterMortgage')->name('filter-mortgage-credits');
		Route::get('/filter-overdraft', 'CreditsController@filterOverdraft')->name('filter-overdraft-credits');
		//End small filters
		//new auto filter
		Route::get('/filter-new-auto', 'CreditsController@filterNewAuto')->name('filter-new-auto-credits');
		Route::get('/select-car-brands', 'CreditsController@selectCar')->name('select-car-brands');
		//end new auto filter

		//Large Filters
		Route::get('/large-consumer-search', 'CreditsController@largeConsumerFilter')->name('filter-large-consumer-credit');
		Route::get('/large-auto-search', 'CreditsController@largeAutoFilter')->name('filter-large-auto-credit');
		Route::get('/large-education-search', 'CreditsController@largeEducationFilter')->name('filter-large-education-credit');
		Route::get('/large-micro-search', 'CreditsController@largeMicroFilter')->name('filter-large-micro-credit');
		Route::get('/large-mortgage-search', 'CreditsController@largeMortgageFilter')->name('filter-large-mortgage-credit');
		Route::get('/large-overdraft-search', 'CreditsController@largeOverdraftFilter')->name('filter-large-overdraft-credit');
		//End large filters

		Route::get('/view/{slug}', 'CreditsController@viewCredit')->name('view-credit');
		Route::get('/consumer', 'CreditsController@creditConsumer')->name('credit-consumer');
		Route::get('/auto', 'CreditsController@creditAuto')->name('credit-auto');
		Route::get('/education', 'CreditsController@creditEducation')->name('credit-education');
		Route::get('/microcredit', 'CreditsController@creditMicroCredit')->name('credit-microcredit');
		Route::get('/mortgage', 'CreditsController@creditMortgage')->name('credit-mortgage');
		Route::get('/overdraft', 'CreditsController@creditOverdraft')->name('credit-overdraft');
		Route::get('/credit-calc', 'CreditsController@calcCreditPercent')->name('credit-calc');
		Route::get('/credit-pay-graph', 'CreditsController@payGraph')->name('credit-pay-graph');
		Route::get('/newauto', 'CreditsController@creditNewPage')->name('credit-new-page');
	});
	//Cards
	Route::prefix('cards')->namespace('Frontend')->group(function () {
		Route::get('/debit', 'CardsController@debitCard')->name('debit-cards');
		Route::get('/debit/small-filter', 'CardsController@debitCardSmallFilter')->name('debit-cards-small-filter');
		Route::get('/debit/large-filter', 'CardsController@debitCardLargeFilter')->name('debit-cards-large-filter');

		Route::get('/credit', 'CardsController@creditCard')->name('credit-cards');
		Route::get('/credit/small-filter', 'CardsController@creditCardSmallFilter')->name('credit-cards-small-filter');
		Route::get('/credit/large-filter', 'CardsController@creditCardLargeFilter')->name('credit-cards-large-filter');

		/**
		 * other cards
		 **/
		Route::get('/other-debit-cards', 'CardsController@otherDebitCards')->name('other-debit-cards-list');
		Route::get('/other-credit-cards', 'CardsController@otherCreditCards')->name('other-credit-cards-list');
	});
	//end cards

	// REVIEWS & REVIEW COMMENTS & PROFILE

	Route::group(['namespace' => 'Profile', 'middleware' => 'guest'], function () {
		/* Profile Auth */
		Route::get('/profile/login', 'AuthController@login')->name('profile.login');
		Route::post('/profile/login', 'AuthController@postLogin')->name('personal.login.form');
		Route::get('/profile/register', 'AuthController@register')->name('profile.register');
		Route::post('/profile/register', 'AuthController@postRegister')->name('personal.register.form');
		Route::get('/profile/reset-password', 'AuthController@resetPassword')->name('password.reset');
		Route::post('/profile/reset-password', 'AuthController@postResetPassword')->name('password.reset.form');
		Route::get('/profile/phone-verification', 'AuthController@verifyPhone')->name('phone.verify');
		Route::post('/profile/phone-verification', 'AuthController@postVerifyPhone')->name('phone.verify.form');
	}
	);

	Route::prefix('reviews')->namespace('Frontend')->group(function () {

		Route::get('/', 'ReviewController@list')->name('review.list');
		//Specific Bank reviews
		Route::get('/bank/{slug}', 'ReviewController@singleBankReviews')->name('review.list.single');
		Route::get('/{id}', 'ReviewController@view')->name('review.view');

		Route::group(['middleware' => ['profile', 'profile.verified']], function () {
			Route::get('/form/add', 'ReviewController@addReview')->name('review.add');
			Route::post('/form/add', 'ReviewController@addReviewForm')->name('review.add.form');
			Route::post('comment/add', 'ReviewController@addCommentForm')->name('comment.add.form');
		});
		Route::group(['middleware' => ['bankprofile']], function () {
			Route::get('/bp/{id}', 'ReviewController@bplist')->name('review.bp_list');
			Route::get('/bp/read/{id}', 'ReviewController@toggleReviewRead')->name('review.bp_readlist');
			Route::get('/bp/edit/{id}', 'ReviewController@bpResponseEditPage')->name('review.bp_edit_response_page');
			Route::post('/bp/edit', 'ReviewController@bpResponseEdit')->name('review.bp_edit_response');
		});
	}
	);

	Route::prefix('/bank/profile')->namespace('Frontend')->group(function () {
		Route::get('', 'BankProfileController@profile')->name('bank-profile');
		Route::get('/new-questions', 'BankProfileController@newQuestions')->name('profile-question-answer');
		Route::get('/answered-questions', 'BankProfileController@answeredQuestions')->name('profile-bank-answered-questions');
		Route::post('/writeAnswer', 'BankProfileController@writeAnswer')->name('profile-write-answer');
		Route::post('/editAnswer', 'BankProfileController@editAnswer')->name('profile-edit-answer');
		Route::post('/edit-bank-user', 'BankProfileController@editUser')->name('profile-edit-bank-user');
		Route::prefix('/orders')->middleware(['auth', 'bankprofile'])->group(function () {
			Route::get('/deposits', 'OrderController@bankDeposits')->name('bank-orders-deposits');
			Route::get('/credits', 'OrderController@bankCredits')->name('bank-orders-credits');
			Route::get('/debet-cards', 'OrderController@bankDebetCards')->name('bank-orders-debet');
			Route::get('/mark-done', 'OrderController@markDone')->name('mark-orders-done');
		});
	});

	Route::group(['namespace' => 'Profile', 'prefix' => '/profile', 'middleware' => 'profile'], function () {
		Route::get('/confirm-phone', 'AuthController@confirm')->name('profile.confirm');
		Route::post('/confirm-phone', 'AuthController@postConfirm')->name('profile.confirm.form');
		Route::group(['middleware' => 'profile.verified'], function () {
			Route::get('/', 'IndexController@index')->name('profile.index');
			Route::get('/reviews', 'IndexController@reviews')->name('profile.reviews');
			Route::post('/change-password', 'IndexController@changePassword')->name('profile.change-password');
			Route::post('/user-edit', 'IndexController@editUser')->name('profile.edit.user');
		});
	});

	Route::prefix('questions')->namespace('Frontend')->group(function () {
		Route::get('/bank', 'QuestionsController@index')->name('bank-question-list');
		Route::get('/type', 'QuestionsController@questionTypeList')->name('bank-question-type-list');
		Route::get('/users/{slug}', 'QuestionsController@giveQuestion')->name('bank-give-question');
		Route::get('/faq/{slug}', 'QuestionsController@fAQuestions')->name('bank-faq-questions');
		Route::post('/add-question', 'QuestionsController@addQuestion')->name('bank-add-question')->middleware('profile');
	});

	Route::prefix('answers')->namespace('Frontend')->group(function () {
		Route::get('/{slug}', 'QuestionsController@listAnswer')->name('bank-list-answers');
		Route::get('/faq/{slug}', 'QuestionsController@listFaqAnswer')->name('faq-question-list');

	});

	//rassrochka new module
	Route::prefix('rassrochka/telephones')->namespace('Frontend')->group(function () {

		Route::get('/', 'RassrochkaController@telephones')->name('rassrochka-telephones');
		Route::get('/list', 'RassrochkaController@list')->name('rassrochka-telephones-list');
	});

	//end rassrochka

	//Orders
	Route::group(['namespace' => 'Frontend', 'prefix' => '/order'], function () {
		Route::post('/deposit', 'OrderController@orderDeposit')->name('order.deposit');
		Route::post('/credit', 'OrderController@orderCredit')->name('order.credit');
	});

});

Route::get('/exchange/ajax/content', 'HomeController@exchangeAjaxContent')->name('exchange-ajax-content');

Route::post('/make-contact', 'HomeController@contact')->name('contact');

Route::post('/make-comment', 'HomeController@comment')->name('comment');

Route::get('/loader', 'HomeController@loader')->name('loader');

Route::get('/add/comparison', 'HomeController@addComparison')->name('add-comparison');

Route::get('/remove/comparison', 'HomeController@removeComparison')->name('remove-comparison');

Route::get('captcha-refresh', 'HomeController@captchaRefresh')->name('captcha-refresh');

//admin
Route::prefix('dashboard')->namespace('Admin')->middleware(['auth', 'admin'])->group(function () {

	Route::get('/', 'DashboardController@dashboard')->name('dashboard');

	Route::get('/users', 'UserController@users')->middleware('rootAdmin')->name('users-list');
	Route::get('/user/update/{id}', 'UserController@userUpdate')->middleware('rootAdmin')->name('admin-user-update');
	Route::get('/user/add', 'UserController@userAdd')->middleware('rootAdmin')->name('admin-user-add');
	Route::get('/user-other-input', 'UserController@userOtherContent')->name('admin-user-other-input');
	Route::get('/user-select-organization', 'UserController@selectOrganization')->name('admin-select-oranization');
	Route::post('/user-edit', 'UserController@userEdit')->name('admin-user-edit');

	Route::get('/user/roles', 'UserController@usersRoles')->middleware('rootAdmin')->name('user-roles-list');
	Route::post('/user/roles-edit', 'UserController@editUserRole')->middleware('rootAdmin')->name('user-roles-edit');
	Route::post('/user/roles-delete/{id}', 'UserController@deleteRole')->middleware('rootAdmin')->name('user-roles-delete');

	Route::prefix('all-organizations')->group(function () {
		Route::get('/banks', 'BanksController@listBanks')->name('banks-list');
		Route::get('/other-other_organization', 'BanksController@listOtherOrganization')->name('other-organization-list');

		Route::get('/view/{id}', 'BanksController@view')->name('banks-view');

		Route::get('/update/view/{id}', 'BanksController@updateView')->name('banks-update-form');

		Route::post('/update', 'BanksController@update')->name('banks-update');

		Route::get('/add/view', 'BanksController@addView')->name('banks-add-form');

		Route::post('/add', 'BanksController@add')->name('banks-add');

		Route::get('/delete/{id}', 'BanksController@delete')->name('bank-delete');

        Route::get('/type','BanksController@typeBanks')->name('banks-type-list');

        Route::post('/type/add','BanksController@typeBanksAdd')->name('banks-type-add');

        Route::get('/type/delete/{id}','BanksController@typeBanksDelete')->name('banks-type-delete');

        Route::get('/information/{id}','BanksController@informations')->name('bank-information');
        Route::post('/edit-information','BanksController@EditInformations')->name('edit-bank->information');

      
    });

      // Stock 
    Route::get('stocks','StockController@index')->name('admin.stock.index');
    Route::get('stocks/form/{id?}','StockController@form')->name('stock.form');
    Route::post('stocks/form/{id?}','StockController@save')->name('stock.save');
    Route::get('stocks/delete/{id?}','StockController@delete')->name('stock.delete');

    Route::get('stocks/requests','StockController@requests');

    Route::prefix('organizations')->group(function(){
        Route::get('/branches/{id}','BanksController@organizationBranches')->name('organization-branches');
        Route::get('/branches/add/{id}','BanksController@branchesAdd')->name('organization-branches-add');
        Route::get('/branches/update/{id}','BanksController@branchesUpdate')->name('organization-branches-update');
        Route::post('/branches/edit','BanksController@branchesEdit')->name('organization-branches-edit');
        Route::post('/branches/import','BanksController@importBranches')->name('organization-branches-import');

		Route::post('/type/add', 'BanksController@typeBanksAdd')->name('banks-type-add');

		Route::get('/type/delete/{id}', 'BanksController@typeBanksDelete')->name('banks-type-delete');

		Route::get('/information/{id}', 'BanksController@informations')->name('bank-information');
		Route::post('/edit-information', 'BanksController@EditInformations')->name('edit-bank->information');
	});

	Route::prefix('organizations')->group(function () {
		Route::get('/branches/{id}', 'BanksController@organizationBranches')->name('organization-branches');
		Route::get('/branches/add/{id}', 'BanksController@branchesAdd')->name('organization-branches-add');
		Route::get('/branches/update/{id}', 'BanksController@branchesUpdate')->name('organization-branches-update');
		Route::post('/branches/edit', 'BanksController@branchesEdit')->name('organization-branches-edit');
		Route::post('/branches/import', 'BanksController@importBranches')->name('organization-branches-import');

	});

	Route::get('/banks/services/table', 'HomeController@bankServicesTable')->name('admin-bank-services-table');
	Route::post('/banks/services/table/edit', 'HomeController@bankServicesTableEdit')->name('admin-bank-services-table-edit');
	Route::get('/banks/services/table/file/download/{id}', 'HomeController@download')->name('admin-bank-services-table-file-download');
	Route::get('/banks/services/table/file/delete/{id}', 'HomeController@deleteFile')->name('admin-bank-services-table-delete');

	Route::prefix('deposits')->group(function () {

		Route::get('/', 'DepositsController@depositList')->name('deposit-list');

		Route::get('/add/view', 'DepositsController@addView')->name('deposit-add-view');

		Route::post('/add', 'DepositsController@add')->name('deposit-add');

		Route::get('/update/view/{id}', 'DepositsController@updateView')->name('deposit-update-view');

		Route::post('/update', 'DepositsController@update')->name('deposit-update');

		Route::get('/delete/{id}', 'DepositsController@delete')->name('deposit-delete');

		Route::get('/status', 'DepositsController@status')->name('deposit-status');

		Route::post('/delete/diff/date', 'DepositsController@depositDifferDate')->name('delete-deposit-diff-date');

		Route::get('/view/{id}', 'DepositsController@depositView')->name('deposit-view-parameter');

		Route::prefix('types')->group(function () {
			Route::get('/', 'DepositsController@depositTypeList')->name('deposit-type-list');

			Route::post('/edit', 'DepositsController@typeEdit')->name('deposit-type-edit');

			Route::get('/delete/{id}', 'DepositsController@typeDelete')->name('deposit-type-delete');
		});

		Route::prefix('open/type')->group(function () {
			Route::get('/', 'DepositsController@openTypeDepositList')->name('deposit-open-type-list');

			Route::post('/edit', 'DepositsController@openTypeDepositEdit')->name('deposit-open-type-edit');

			Route::get('/delete/{id}', 'DepositsController@openTypeDepositDelete')->name('deposit-open-type-delete');
		});

		Route::prefix('period/percent')->group(function () {
			Route::get('/', 'DepositsController@paidPercentDepositList')->name('deposit-period-percent-list');
			Route::post('/edit', 'DepositsController@paidPercentDepositEdit')->name('deposit-period-percent-edit');
			Route::get('/delete/{id}', 'DepositsController@paidPercentDepositDelete')->name('deposit-period-percent-delete');
		});

		Route::get('/deposit-date-type-percent-input', 'DepositsController@deposit_date_type_percent_input')->name('deposit-date-type-percent-input');

	});

	Route::prefix('service/date')->group(function () {
		Route::get('/', 'DateServiceController@dateList')->name('date-type-list');

		Route::get('/add/view', 'DateServiceController@addView')->name('date-type-add-view');

		Route::post('/add', 'DateServiceController@add')->name('date-type-add');

		Route::get('/update/view/{id}', 'DateServiceController@updateView')->name('date-type-update-view');

		Route::post('/update', 'DateServiceController@update')->name('date-type-update');

		Route::get('/delete/{id}', 'DateServiceController@delete')->name('date-type-delete');

		Route::post('/position', 'DateServiceController@position')->name('date-position');

		Route::get('/select-children', 'DateServiceController@creditChildren')->name('select-credit-children');

		Route::get('/change-status', 'DateServiceController@changeStatus')->name('date-service-status-change');
		Route::get('/change-status-all/{status}', 'DateServiceController@changeStatusAll')->name('date-service-status-change-all');
	});

	Route::prefix('region')->group(function () {
		Route::get('/', 'RegionsController@regionList')->name('region-list');

		Route::get('/update/view/{id}', 'RegionsController@updateView')->name('region-update-form');

		Route::post('/update', 'RegionsController@update')->name('region-update');

		Route::get('/add/view', 'RegionsController@addView')->name('region-add-form');

		Route::post('/add', 'RegionsController@add')->name('region-add');

		Route::get('/delete/{id}', 'RegionsController@delete')->name('region-delete');
	});

	Route::prefix('currency')->group(function () {
		Route::get('/', 'CurrencyController@currencyList')->name('currency-list');

		Route::get('/add/view', 'CurrencyController@addView')->name('currency-add-view');

		Route::post('/add', 'CurrencyController@add')->name('currency-add');

		Route::get('/update/view/{id}', 'CurrencyController@updateView')->name('currency-update-view');

		Route::post('/update', 'CurrencyController@update')->name('currency-update');

		Route::get('/delete/{id}', 'CurrencyController@delete')->name('currency-delete');
	});

	Route::prefix('services')->group(function () {
		Route::get('/', 'ServiceController@servicesList')->name('services-list');

		Route::get('/add/view', 'ServiceController@addView')->name('service-add-form');

		Route::post('/add', 'ServiceController@add')->name('service-add');

		Route::get('/update/view/{id}', 'ServiceController@updateView')->name('service-update-form');

		Route::post('/update', 'ServiceController@update')->name('service-update');

		Route::get('/delete/{id}', 'ServiceController@delete')->name('service-delete');

		Route::get('/admin-service-status-change', 'ServiceController@changeStatus')->name('admin-service-status-change');
	});

	Route::prefix('pages')->group(function () {
		Route::get('/home/page', 'HomeController@home')->name('home-page');
	});

	Route::prefix('credits')->group(function () {

		Route::get('/type-list/{slug}', 'CreditsController@creditTypeView')->name('credits-type-view');

		Route::get('/', 'CreditsController@creditsList')->name('credits-list');

		Route::get('/view/{id}', 'CreditsController@creditView')->name('credit-view');

		Route::get('/add/view', 'CreditsController@addCreditView')->name('credit-add-form');

		Route::post('/add', 'CreditsController@addCredit')->name('credit-add');

		Route::get('/update/view/{id}', 'CreditsController@updateCreditView')->name('credit-update-form');

		Route::post('/update', 'CreditsController@updateCredit')->name('credit-update');

		Route::get('/delete/{id}', 'CreditsController@deleteCredit')->name('credit-delete');

		Route::get('status', 'CreditsController@status')->name('credit-status');

		Route::get('/translate', 'CreditsController@translate')->name('credits-translate');

		Route::get('/review-response', 'CreditsController@creditReviewList')->name('credit-review-period-response-list');

		Route::get('/issuing-form', 'CreditsController@creditIssuingForm')->name('credit-issuing-form-response-list');

		Route::get('/goal-response', 'CreditsController@creditGoalList')->name('credit-goal-credit-response-list');

		Route::get('/compensation-response', 'CreditsController@creditCompensationList')->name('credit-compensation-select-list');
		Route::get('/date-type-response', 'CreditsController@creditDateTypeList')->name('credit-date-type-select-list');
		Route::get('/provision-list-response', 'CreditsController@creditProvisionList')->name('credit-provision-type-select-list');
		Route::get('/borrow-category-list-response', 'CreditsController@creditBorrowCategory')->name('credit-borrow-category-select-list');

		Route::get('/credit-child-form', 'CreditsController@credit_children_date')->name('credit-child-form');
		Route::get('/credit-child-compensation-form', 'CreditsController@credit_children_compensation_date')->name('credit-child-compensation-form');

		Route::prefix('/borrower/category')->group(function () {
			Route::get('/', 'CreditParameters@borrowerCategoryList')->name('credit-borrower-category-list');
			Route::post('/edit', 'CreditParameters@borrowerCategoryEdit')->name('credit-borrower-category-edit');
			Route::get('/delete/{id}', 'CreditParameters@borrowerCategoryDelete')->name('credit-borrower-category-delete');
			Route::get('/select/type', 'CreditParameters@borrowerCategorySelectType')->name('credit-select-type');

		});

		Route::prefix('/goal')->group(function () {
			Route::get('/', 'CreditParameters@goalCredit')->name('credit-goal-list');
			Route::post('/edit', 'CreditParameters@goalCreditEdit')->name('credit-goal-edit');
			Route::get('/delete/{id}', 'CreditParameters@goalCreditDelete')->name('credit-goal-delete');
		});

		Route::prefix('/issuing/form')->group(function () {
			Route::get('/', 'CreditParameters@issuingFormCreditList')->name('credit-issuing-form-list');
			Route::post('/edit', 'CreditParameters@issuingFormCreditEdit')->name('credit-issuing-form-edit');
			Route::get('/delete/{id}', 'CreditParameters@issuingFormCreditDelete')->name('credit-issuing-form-delete');
		});

		Route::prefix('/pledge')->group(function () {
			Route::get('/', 'CreditParameters@pledgeCreditList')->name('credit-pledge-list');
			Route::post('/edit', 'CreditParameters@pledgeCreditEdit')->name('credit-pledge-edit');
			Route::get('/delete/{id}', 'CreditParameters@pledgeCreditDelete')->name('credit-pledge-delete');
		});

		Route::prefix('/proof/income')->group(function () {
			Route::get('/', 'CreditParameters@proofIncomeCreditList')->name('credit-proof-income-list');
			Route::post('/edit', 'CreditParameters@proofIncomeCreditEdit')->name('credit-proof-income-edit');
			Route::get('/delete/{id}', 'CreditParameters@proofIncomeCreditDelete')->name('credit-proof-income-delete');
		});

		Route::prefix('/review/period')->group(function () {
			Route::get('/', 'CreditParameters@reviewPeriodCreditList')->name('credit-review-period-list');
			Route::post('/edit', 'CreditParameters@reviewPeriodCreditEdit')->name('credit-review-period-edit');
			Route::get('/delete/{id}', 'CreditParameters@reviewPeriodCreditDelete')->name('credit-review-period-delete');
		});

		Route::prefix('/surety')->group(function () {
			Route::get('/', 'CreditParameters@suretyCreditList')->name('credit-surety-list');
			Route::post('/edit', 'CreditParameters@suretyCreditEdit')->name('credit-surety-edit');
			Route::get('/delete/{id}', 'CreditParameters@suretyCreditDelete')->name('credit-surety-delete');
		});

		Route::prefix('/provision')->group(function () {
			Route::get('/', 'CreditParameters@provisionCreditList')->name('credit-provision-list');
			Route::post('/edit', 'CreditParameters@provisionCreditEdit')->name('credit-provision-edit');
			Route::get('/delete/{id}', 'CreditParameters@provisionCreditDelete')->name('credit-provision-delete');
		});
		Route::prefix('/compensation')->group(function () {
			Route::get('/', 'CreditParameters@compensationCreditList')->name('credit-compensation-list');
			Route::post('/edit', 'CreditParameters@compensationCreditEdit')->name('credit-compensation-edit');
			Route::get('/delete/{id}', 'CreditParameters@compensationCreditDelete')->name('credit-compensation-delete');
		});

	});

	Route::prefix('credit/types')->group(function () {
		Route::get('/', 'CreditsController@creditTypeList')->name('credit-type-list');

		Route::get('/add/view', 'CreditsController@addTypeView')->name('credit-type-add-view');

		Route::post('/add', 'CreditsController@addType')->name('credit-type-add');

		Route::get('/update/{id}', 'CreditsController@updateTypeView')->name('credit-type-update-view');

		Route::post('/update', 'CreditsController@updateType')->name('credit-type-update');

		Route::get('/delete/{id}', 'CreditsController@deleteType')->name('credit-type-delete');
	});

	Route::prefix('/exchange/rate')->group(function () {

		Route::get('/', 'ExchangeRateController@rateList')->name('rate-list');

		Route::get('/add/form', 'ExchangeRateController@addForm')->name('rate-add-form');

		Route::post('/add', 'ExchangeRateController@add')->name('rate-add');

		Route::get('/update/form/{id}', 'ExchangeRateController@updateForm')->name('rate-update-form');

		Route::post('/update', 'ExchangeRateController@update')->name('rate-update');

		Route::get('/delete/{id}', 'ExchangeRateController@delete')->name('rate-delete');
		Route::get('/delete-other/{id}', 'ExchangeRateController@deleteOther')->name('other-rate-delete');

		Route::get('/update/bank-currencies/{id}', 'ExchangeRateController@updateCurrenciesForm')->name('rate-update-form-all');
		Route::post('/update/bank-currencies', 'ExchangeRateController@updateCurrenciesEdit')->name('rate-update-edit-all');

		Route::prefix('/type')->group(function () {
			Route::get('/', 'ExchangeRateController@listTypeExchange')->name('type-exchange-list');
			Route::post('/edit', 'ExchangeRateController@editTypeExchange')->name('type-exchange-edit');
			Route::get('/delete/{id}', 'ExchangeRateController@deleteTypeExchange')->name('type-exchange-delete');
		});

	});

	Route::prefix('contact')->group(function () {
		Route::get('/list', 'HomeController@contact')->name('contact-list');
	});

	Route::prefix('cards/debit')->group(function () {
		Route::get('/', 'DebitCardController@debitCardList')->name('debit-cards-list');

		Route::get('/delete/{id}', 'DebitCardController@debitCardDelete')->name('debit-card-delete');

		Route::get('/add/page', 'DebitCardController@add')->name('debit-cards-add-page');

		Route::post('/add/post', 'DebitCardController@add')->name('debit-cards-add');

		Route::get('/update/page', 'DebitCardController@update')->name('debit-cards-update-page');

		Route::post('/update/post', 'DebitCardController@update')->name('debit-cards-update-add');

		Route::get('/status', 'DebitCardController@cardStatus')->name('debit-card-status');
	});

	Route::prefix('cards/credit')->group(function () {
		Route::get('/', 'CreditCardController@cardList')->name('credit-card-list');

		Route::get('/add/page', 'CreditCardController@addCard')->name('credit-card-add-page');

		Route::post('/add/post', 'CreditCardController@addCard')->name('credit-card-add');

		Route::get('/update/page', 'CreditCardController@updateCard')->name('credit-card-update-page');

		Route::post('/update/post', 'CreditCardController@updateCard')->name('credit-card-update');

		Route::get('/status', 'CreditCardController@cardStatus')->name('credit-card-status');

		Route::get('/delete/{id}', 'CreditCardController@deleteCard')->name('credit-card-delete');
	});

	Route::prefix('update-time')->group(function () {
		Route::get('/', 'DataUpdateTimeController@listDate')->name('data-update-time-list');
		Route::post('/edit', 'DataUpdateTimeController@edit')->name('data-update-time-edit');
		Route::get('/delete/{id}', 'DataUpdateTimeController@delete')->name('data-update-time-delete');
	});

	Route::prefix('type-debit-card')->group(function () {
		Route::get('/', 'DebitCardController@listType')->name('debit-card-type-list');
		Route::post('/edit', 'DebitCardController@editCardType')->name('debit-card-type-edit');
		Route::get('/delete/{id}', 'DebitCardController@deleteCardType')->name('debit-card-type-delete');
	});

	Route::prefix('payment-type')->group(function () {
		Route::get('/', 'ServiceController@paymentTypeList')->name('payment-type-list');
		Route::post('/edit', 'ServiceController@paymentTypeEdit')->name('payment-type-edit');
		Route::get('/delete/{id}', 'ServiceController@paymentTypeDelete')->name('payment-type-delete');
	});

	Route::prefix('quantities')->group(function () {
		Route::get('/', 'MinimalPaymentController@listQuantity')->name('min-quantity-list');
		Route::post('/edit', 'MinimalPaymentController@editQuantity')->name('min-quantity-edit');
		Route::get('/delete/{id}', 'MinimalPaymentController@destroyQuantity')->name('min-quantity-delete');
	});

	Route::prefix('/news')->group(function () {
		Route::get('/', 'NewsController@newsList')->name('news-list');

		Route::get('/add', 'NewsController@add')->name('news-add-get');
		Route::post('/add', 'NewsController@add')->name('news-add');

		Route::get('/update/{id}', 'NewsController@update')->name('news-update-get');
		Route::post('/update', 'NewsController@update')->name('news-update');

		Route::get('/delete/{id}', 'NewsController@delete')->name('news-delete');

		Route::prefix('/category')->group(function () {
			Route::get('/', 'NewsController@categoryList')->name('news-category-list');
			Route::post('/edit', 'NewsController@categoryEdit')->name('news-category-edit');
			Route::get('/delete/{id}', 'NewsController@categoryDelete')->name('news-category-delete');
		});
	});

	Route::prefix('/setting')->group(function () {
		Route::get('/meta-tags-list', 'SettingController@metaTagsList')->name('meta-tags-list');

		Route::get('/meta-tag-inputs', 'SettingController@metaTagInputs')->name('meta-tag-inputs');
		Route::post('/meta-edit', 'SettingController@metaTagEdit')->name('meta-tag-edit');
		Route::get('/meta-delete/{id}', 'SettingController@metaDelete')->name('meta-tag-delete');
		Route::get('/meta-delete-one', 'SettingController@metaDeleteOne')->name('meta-tag-delete-one');

		Route::get('/currency-api', 'SettingController@currencyAPIlist')->name('setting-currency-api');

		Route::post('/currency-api-edit', 'SettingController@currencyAPIEdit')->name('setting-currency-api-edit');

		Route::get('/row-position', 'SettingController@rowPosition')->name('row-position');

		Route::get('/caching-cron', 'SettingController@caching')->name('setting-caching');
		Route::get('/artisan-command/{command}', 'SettingController@artisanCommand')->name('artisan-command');
		Route::get('/download-cron-result', 'SettingController@downloadCronResult')->name('download-cron-result');
	});

	Route::prefix('/review')->group(function () {
		Route::get('/', 'ReviewController@reviewList')->name('admin-review-list');
		Route::get('/add', 'ReviewController@reviewAdd')->name('admin-review-add');
		Route::get('/update/{id}', 'ReviewController@reviewUpdate')->name('admin-review-update');
		Route::post('/edit', 'ReviewController@reviewEdit')->name('admin-review-edit');
		Route::get('/delete/{id}', 'ReviewController@reviewDelete')->name('admin-review-delete');
		Route::post('/comment-edit', 'ReviewController@reviewCommentEdit')->name('admin-review-comment-edit');
		Route::get('/comment-delete/{id}', 'ReviewController@reviewCommentDelete')->name('admin-review-comment-delete');
	});

	//rassrochka new modul
	Route::prefix('/rassrochka/telephones')->group(function () {
		Route::get('/', 'RassrochkaController@telephonesList')->name('admin-telephone-list');
		Route::get('/type-list/{id}', 'RassrochkaController@telephoneTypeView')->name('admin-telephone-type');
		Route::post('/import', 'RassrochkaController@importTelephones')->name('telephones-import');
		Route::get('/delete/{id}', 'RassrochkaController@deleteTelephone')->name('telephone-delete');
		Route::get('/add/view', 'RassrochkaController@addTelephoneView')->name('telephone-add-form');
		Route::post('/add', 'RassrochkaController@addTelephone')->name('telephone-add');
		Route::get('/update/view/{id}', 'RassrochkaController@updateTelephoneView')->name('telephone-update-form');
		Route::post('/update', 'RassrochkaController@updateTelephone')->name('telephone-update');
		Route::get('/telefone-api', 'RassrochkaController@telefoneAPIList')->name('admin-telephone-api_list');
		Route::post('/telefone-api-edit', 'RassrochkaController@telefoneAPIEdit')->name('admin-telephone-api_edit');
		Route::get('/telefone-api-delete/{id}', 'RassrochkaController@telefoneAPIDelete')->name('admin-telephone-api_delete');
		Route::get('/telefone-api-runparse', 'RassrochkaController@telefoneAPIRunParse')->name('admin-telephone-api_runparse');
		Route::get('/telefone-api-problems', 'RassrochkaController@telefonAPIProblems')->name('admin-telephone-api_problems');
		Route::get('/telefone-model-edit', 'RassrochkaController@telefoneModelEdit')->name('admin-telephone-model_edit');
		Route::get('/telefone-shop-api-problems', 'RassrochkaController@telefonShopAPIProblems')->name('admin-telephone-shop-api_problems');
		Route::get('/telefone-char-api-runparse', 'RassrochkaController@telefonCharAPIRunParse')->name('admin-telephone-char-api_runparse');
		Route::get('/add/usingShop/{id}', 'RassrochkaController@updateTelephoneFromShop')->name('admin-telephone-add-usingShop');
		Route::get('/shop/matched/{shop_id}/{id}', 'RassrochkaController@updateTelephoneShopMatch')->name('telephone-shop-match');
	});
	Route::prefix('rassrochka')->group(function () {
		Route::get('/brands', 'RassrochkaController@brandsIndex')->name('admin-rassrochka-brand-list');
		Route::post('/brands', 'RassrochkaController@brandsStore')->name('admin-rassrochka-brand-add');
		Route::get('/brands/delete/{id}', 'RassrochkaController@brandsDestroy')->name('admin-rassrochka-brand-delete');
	});

	//car new modul
	Route::prefix('/cars')->group(function () {
		Route::get('/', 'CarsController@index')->name('admin-car-list');
		Route::post('/add', 'CarsController@store')->name('admin-car-add');
		Route::get('/delete/{id}', 'CarsController@destroy')->name('admin-car-delete');
		Route::get('type-list/{id}', 'CarsController@carTypeList')->name('admin-car-type');

		Route::get('/brands', 'BrandsController@index')->name('admin-car-brand-list');
		Route::post('/brands', 'BrandsController@store')->name('admin-car-brand-add');
		Route::get('/brands/delete/{id}', 'BrandsController@destroy')->name('admin-car-brand-delete');
		Route::get('/parsehub', 'CarsController@carAPIRunParse')->name('admin-car-api_runparse');
	});

	Route::prefix('/questions')->group(function () {
		Route::get('/list', 'QuestionsController@listQuestions')->name('admin-question-list');
		Route::get('/edit-view/{id}', 'QuestionsController@editQuestionView')->name('admin-question-edit-view');
		Route::post('/edit', 'QuestionsController@edit')->name('admin-question-edit-post');
		Route::get('/delete/{id}', 'QuestionsController@deleteQuestion')->name('admin-question-delete');
		Route::post('/edit-answer', 'QuestionsController@editAnswer')->name('admin-question-answer-edit');
		Route::get('/delete-answer/{id}', 'QuestionsController@deleteAnswer')->name('admin-question-answer-delete');
	});

	Route::prefix('/orders')->group(function () {
		Route::get('/list', 'OrderController@index')->name('admin-orders-list');
		Route::get('/add-view', 'OrderController@add')->name('admin-orders-add-view');
		Route::post('/save', 'OrderController@save')->name('admin-orders-save');
		Route::get('/edit-view/{id}', 'OrderController@edit')->name('admin-orders-edit-view');
		Route::post('/update', 'OrderController@update')->name('admin-orders-update');
	});

});

Route::group(

	[
		'namespace' => 'Profile',
		'middleware' => 'guest',
	],
	function () {
		// Social logins
		Route::get('/signin/google', 'AuthController@google')->name('signin-google');
		Route::get('/signin/google/redirect', 'AuthController@googleRedirect')->name('signin-google-redirect');
		Route::get('/signin/facebook', 'AuthController@facebook')->name('signin-facebook');
		Route::get('/signin/facebook/redirect', 'AuthController@facebookRedirect')->name('signin-facebook-redirect');
	});

Route::prefix('/test')->middleware(['auth', 'admin'])->group(function () {
	//testing notification
	Route::get('/notf', function () {
		$user = User::find(106);
		$user->notify(new ReviewNotification('http://example.com', 'Test notification.'));
		echo $user->name . ' notified!';
	});
	//testing sms
	Route::get('/sms', 'Profile\AuthController@testSMS');
	//testing banker
	Route::get('/banker', function () {
		$bank = Bank::find(16);
		echo $bank->bank_hodimi;
	});
	// test session
	Route::get('/session/login', function () {
		return session('intended_url', '/');
	});
	// test ids
	Route::get('/ids', function () {
		return implode(',', [5, 6]);
	});
	// test cyrilic conversion
	Route::get('/cyrilic/{str}', function ($str) {
		$line = iconv("UTF-8", "Windows-1251", $str); // convert to windows-1251
		$line = ucfirst($line);
		$line = iconv("Windows-1251", "UTF-8", $line); // convert back to utf-8

		return $line;
	});
	//test bank questions
	Route::get('/bank/questions', function () {
		$bank = Bank::find(9);
		dd($bank->questions);
	});
});